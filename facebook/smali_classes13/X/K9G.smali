.class public final LX/K9G;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/K9H;


# direct methods
.method public constructor <init>(LX/K9H;)V
    .locals 0

    .prologue
    .line 2776861
    iput-object p1, p0, LX/K9G;->a:LX/K9H;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2776862
    iget-object v0, p0, LX/K9G;->a:LX/K9H;

    const/4 v1, 0x0

    .line 2776863
    iput-boolean v1, v0, LX/K9H;->e:Z

    .line 2776864
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2776865
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2776866
    iget-object v0, p0, LX/K9G;->a:LX/K9H;

    const/4 v1, 0x0

    .line 2776867
    iput-boolean v1, v0, LX/K9H;->e:Z

    .line 2776868
    if-eqz p1, :cond_0

    .line 2776869
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2776870
    if-nez v0, :cond_1

    .line 2776871
    :cond_0
    :goto_0
    return-void

    .line 2776872
    :cond_1
    iget-object v0, p0, LX/K9G;->a:LX/K9H;

    iget-object v1, v0, LX/K9H;->a:LX/K9F;

    .line 2776873
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2776874
    check-cast v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->a()Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    move-result-object v0

    .line 2776875
    invoke-virtual {v0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    .line 2776876
    invoke-virtual {v0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->a()LX/0Px;

    move-result-object v2

    .line 2776877
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2776878
    :cond_3
    :goto_1
    iget-object v0, p0, LX/K9G;->a:LX/K9H;

    iget-object v0, v0, LX/K9H;->g:Lcom/facebook/timeline/videos/TimelineVideosTabFragment;

    .line 2776879
    iget-object v1, v0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->j:LX/1Rq;

    invoke-interface {v1}, LX/1OP;->notifyDataSetChanged()V

    .line 2776880
    goto :goto_0

    .line 2776881
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$EdgesModel;

    .line 2776882
    iget-object p1, v1, LX/K9F;->a:Ljava/util/List;

    invoke-virtual {v2}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2776883
    :cond_5
    iput-object v3, v1, LX/K9F;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    goto :goto_1
.end method
