.class public LX/K1D;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K1D;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2761459
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2761460
    iput-object p2, p0, LX/K1D;->a:Ljava/lang/String;

    .line 2761461
    iput-object p3, p0, LX/K1D;->b:Ljava/lang/String;

    .line 2761462
    iput p4, p0, LX/K1D;->c:I

    .line 2761463
    iput p5, p0, LX/K1D;->d:I

    .line 2761464
    return-void
.end method

.method private j()LX/5pH;
    .locals 6

    .prologue
    .line 2761465
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2761466
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2761467
    const-string v2, "start"

    iget v3, p0, LX/K1D;->c:I

    int-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761468
    const-string v2, "end"

    iget v3, p0, LX/K1D;->d:I

    int-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761469
    const-string v2, "text"

    iget-object v3, p0, LX/K1D;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761470
    const-string v2, "previousText"

    iget-object v3, p0, LX/K1D;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761471
    const-string v2, "range"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2761472
    const-string v1, "target"

    .line 2761473
    iget v2, p0, LX/5r0;->c:I

    move v2, v2

    .line 2761474
    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761475
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2761476
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2761477
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K1D;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2761478
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761479
    const-string v0, "topTextInput"

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2761480
    const/4 v0, 0x0

    return v0
.end method
