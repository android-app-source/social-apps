.class public final LX/JZ4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

.field public final synthetic b:LX/JZ8;

.field public final synthetic c:LX/1Pr;

.field public final synthetic d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;LX/JZ8;LX/1Pr;)V
    .locals 0

    .prologue
    .line 2707835
    iput-object p1, p0, LX/JZ4;->d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    iput-object p2, p0, LX/JZ4;->a:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    iput-object p3, p0, LX/JZ4;->b:LX/JZ8;

    iput-object p4, p0, LX/JZ4;->c:LX/1Pr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x1c0abdc6

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2707836
    const v0, 0x7f0d0078

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2707837
    const v0, 0x7f0d0079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2707838
    iget-object v0, p0, LX/JZ4;->d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    iget-object v3, p0, LX/JZ4;->a:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    iget-object v4, p0, LX/JZ4;->b:LX/JZ8;

    iget-object v5, p0, LX/JZ4;->c:LX/1Pr;

    .line 2707839
    iget-object v8, v4, LX/JZ8;->a:LX/JZ7;

    move-object v8, v8

    .line 2707840
    sget-object v9, LX/JZ7;->SAVING:LX/JZ7;

    if-ne v8, v9, :cond_0

    .line 2707841
    :goto_0
    const v0, 0x1629977a

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2707842
    :cond_0
    sget-object v8, LX/JZ7;->SAVING:LX/JZ7;

    .line 2707843
    iput-object v8, v4, LX/JZ8;->a:LX/JZ7;

    .line 2707844
    iget-object v8, v4, LX/JZ8;->c:Ljava/lang/String;

    move-object v8, v8

    .line 2707845
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->r()Ljava/lang/String;

    move-result-object v8

    .line 2707846
    :goto_1
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2707847
    const-string p0, "submitSurveyResponseParamsKey"

    new-instance p1, Lcom/facebook/api/feed/SubmitSurveyResponseParams;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->k()Ljava/lang/String;

    move-result-object v9

    :goto_2
    invoke-direct {p1, v9, v1, v2, v8}, Lcom/facebook/api/feed/SubmitSurveyResponseParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2707848
    iget-object v8, v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->c:LX/0aG;

    const-string v9, "feed_submit_survey_response"

    const p0, 0x6c841cdb

    invoke-static {v8, v9, v10, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v8

    invoke-interface {v8}, LX/1MF;->start()LX/1ML;

    move-result-object v8

    .line 2707849
    new-instance v9, LX/JZ5;

    invoke-direct {v9, v0, v4, v5, v3}, LX/JZ5;-><init>(Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;LX/JZ8;LX/1Pr;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V

    iget-object v10, v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->e:Ljava/util/concurrent/Executor;

    invoke-static {v8, v9, v10}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2707850
    :cond_1
    iget-object v8, v4, LX/JZ8;->c:Ljava/lang/String;

    move-object v8, v8

    .line 2707851
    goto :goto_1

    .line 2707852
    :cond_2
    const/4 v9, 0x0

    goto :goto_2
.end method
