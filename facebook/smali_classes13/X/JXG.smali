.class public final LX/JXG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JXB;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2704249
    iput-object p1, p0, LX/JXG;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    iput-object p2, p0, LX/JXG;->a:LX/JXB;

    iput-object p3, p0, LX/JXG;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iput-object p4, p0, LX/JXG;->c:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, 0x5bed1e2b

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 2704250
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2704251
    iget-object v1, p0, LX/JXG;->a:LX/JXB;

    .line 2704252
    iget-object v4, v1, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v6, v4

    .line 2704253
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    if-ne v1, v4, :cond_2

    move v4, v3

    .line 2704254
    :goto_0
    if-eqz v4, :cond_0

    .line 2704255
    iget-object v1, p0, LX/JXG;->a:LX/JXB;

    invoke-virtual {v1}, LX/JXB;->d()V

    .line 2704256
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v7

    .line 2704257
    if-nez v4, :cond_1

    iget-object v1, p0, LX/JXG;->a:LX/JXB;

    invoke-virtual {v1, v7}, LX/JXB;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    move v1, v3

    .line 2704258
    :goto_1
    iget-object v4, p0, LX/JXG;->a:LX/JXB;

    .line 2704259
    iget-object v8, v4, LX/JXB;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v8, v7, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2704260
    iget-object v1, p0, LX/JXG;->a:LX/JXB;

    .line 2704261
    iput-object v0, v1, LX/JXB;->f:Ljava/lang/Integer;

    .line 2704262
    iget-object v0, p0, LX/JXG;->a:LX/JXB;

    iget-object v1, p0, LX/JXG;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-static {v0, v1, v6}, LX/JXK;->a(LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 2704263
    iget-object v1, p0, LX/JXG;->a:LX/JXB;

    if-nez v0, :cond_4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    if-ne v0, v4, :cond_4

    iget-object v0, p0, LX/JXG;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2704264
    :goto_2
    iput-boolean v3, v1, LX/JXB;->h:Z

    .line 2704265
    iget-object v0, p0, LX/JXG;->c:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2704266
    const v0, 0x30363dba

    invoke-static {v0, v5}, LX/02F;->a(II)V

    return-void

    :cond_2
    move v4, v2

    .line 2704267
    goto :goto_0

    :cond_3
    move v1, v2

    .line 2704268
    goto :goto_1

    :cond_4
    move v3, v2

    .line 2704269
    goto :goto_2
.end method
