.class public LX/Jbu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fjq;


# instance fields
.field private final a:LX/Jbv;

.field private final b:LX/18Q;


# direct methods
.method public constructor <init>(LX/Jbv;LX/18Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717254
    iput-object p1, p0, LX/Jbu;->a:LX/Jbv;

    .line 2717255
    iput-object p2, p0, LX/Jbu;->b:LX/18Q;

    .line 2717256
    return-void
.end method

.method public static b(LX/0QB;)LX/Jbu;
    .locals 6

    .prologue
    .line 2717257
    new-instance v2, LX/Jbu;

    .line 2717258
    new-instance v3, LX/Jbv;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v1

    check-cast v1, LX/2c4;

    const/16 v4, 0x153a

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x2acf

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct {v3, v0, v1, v4, v5}, LX/Jbv;-><init>(Landroid/content/Context;LX/2c4;LX/0Or;LX/0Or;)V

    .line 2717259
    move-object v0, v3

    .line 2717260
    check-cast v0, LX/Jbv;

    invoke-static {p0}, LX/18Q;->a(LX/0QB;)LX/18Q;

    move-result-object v1

    check-cast v1, LX/18Q;

    invoke-direct {v2, v0, v1}, LX/Jbu;-><init>(LX/Jbv;LX/18Q;)V

    .line 2717261
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2717262
    iget-object v0, p0, LX/Jbu;->a:LX/Jbv;

    invoke-virtual {v0}, LX/Jbv;->a()V

    .line 2717263
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 12

    .prologue
    .line 2717264
    const-string v0, "megaphone"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2717265
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/Jbu;->b:LX/18Q;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 2717266
    iget-object v3, v1, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v1, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2717267
    iget-object v3, v1, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/18R;

    .line 2717268
    iget-object v3, v3, LX/18R;->a:LX/3TD;

    if-eqz v3, :cond_2

    .line 2717269
    const/4 v3, 0x1

    .line 2717270
    :goto_0
    move v1, v3

    .line 2717271
    if-nez v1, :cond_1

    .line 2717272
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 2717273
    iget-object v1, p0, LX/Jbu;->b:LX/18Q;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    new-instance v3, LX/3TD;

    const/4 v4, 0x0

    .line 2717274
    const/4 v5, 0x0

    .line 2717275
    const-string v6, "app_name"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2717276
    const-string v7, "release_notes"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2717277
    const-string v8, "local_uri"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2717278
    if-eqz v0, :cond_0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2717279
    :try_start_1
    const-string v9, "data"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 2717280
    if-eqz v9, :cond_0

    .line 2717281
    new-instance v10, LX/4XF;

    invoke-direct {v10}, LX/4XF;-><init>()V

    const-string v11, "id"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2717282
    iput-object v11, v10, LX/4XF;->c:Ljava/lang/String;

    .line 2717283
    move-object v10, v10

    .line 2717284
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 2717285
    iput-object v11, v10, LX/4XF;->j:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 2717286
    move-object v10, v10

    .line 2717287
    const-string v11, "title"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2717288
    iput-object v11, v10, LX/4XF;->l:Ljava/lang/String;

    .line 2717289
    move-object v10, v10

    .line 2717290
    const-string v11, "content"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    const-string p0, "text"

    invoke-virtual {v11, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    .line 2717291
    iput-object v11, v10, LX/4XF;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2717292
    move-object v10, v10

    .line 2717293
    new-instance v11, LX/4XG;

    invoke-direct {v11}, LX/4XG;-><init>()V

    const-string p0, "action"

    invoke-virtual {v9, p0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    const-string p0, "title"

    invoke-virtual {v9, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2717294
    iput-object v9, v11, LX/4XG;->c:Ljava/lang/String;

    .line 2717295
    move-object v9, v11

    .line 2717296
    new-instance v11, Ljava/lang/StringBuilder;

    const-string p0, "install/?file="

    invoke-direct {v11, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, "&notes="

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&appName="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2717297
    iput-object v6, v9, LX/4XG;->d:Ljava/lang/String;

    .line 2717298
    move-object v6, v9

    .line 2717299
    new-instance v7, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    invoke-direct {v7, v6}, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;-><init>(LX/4XG;)V

    .line 2717300
    move-object v6, v7

    .line 2717301
    iput-object v6, v10, LX/4XF;->b:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    .line 2717302
    move-object v6, v10

    .line 2717303
    const/4 v7, 0x1

    .line 2717304
    iput-boolean v7, v6, LX/4XF;->i:Z

    .line 2717305
    move-object v6, v6

    .line 2717306
    new-instance v7, Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-direct {v7, v6}, Lcom/facebook/graphql/model/GraphQLMegaphone;-><init>(LX/4XF;)V

    .line 2717307
    move-object v5, v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2717308
    :cond_0
    :goto_1
    :try_start_2
    move-object v0, v5

    .line 2717309
    invoke-direct {v3, v4, v0}, LX/3TD;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, LX/18Q;->a(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;LX/3TD;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2717310
    :goto_2
    return-void

    .line 2717311
    :catch_0
    move-exception v0

    .line 2717312
    const-class v1, LX/Jbu;

    const-string v2, "megaphone malformed json:"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2717313
    :cond_1
    iget-object v0, p0, LX/Jbu;->a:LX/Jbv;

    invoke-virtual {v0, p1}, LX/Jbv;->a(Landroid/content/Intent;)V

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    :catch_1
    goto :goto_1
.end method
