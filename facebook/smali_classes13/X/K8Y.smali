.class public final LX/K8Y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 2775316
    const-wide/16 v18, 0x0

    .line 2775317
    const/16 v17, 0x0

    .line 2775318
    const/16 v16, 0x0

    .line 2775319
    const-wide/16 v14, 0x0

    .line 2775320
    const/4 v13, 0x0

    .line 2775321
    const/4 v12, 0x0

    .line 2775322
    const/4 v11, 0x0

    .line 2775323
    const/4 v10, 0x0

    .line 2775324
    const/4 v9, 0x0

    .line 2775325
    const/4 v8, 0x0

    .line 2775326
    const/4 v5, 0x0

    .line 2775327
    const-wide/16 v6, 0x0

    .line 2775328
    const/4 v4, 0x0

    .line 2775329
    const/4 v3, 0x0

    .line 2775330
    const/4 v2, 0x0

    .line 2775331
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_11

    .line 2775332
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2775333
    const/4 v2, 0x0

    .line 2775334
    :goto_0
    return v2

    .line 2775335
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 2775336
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2775337
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2775338
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2775339
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2775340
    const/4 v2, 0x1

    .line 2775341
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2775342
    :cond_1
    const-string v6, "description_font"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2775343
    invoke-static/range {p0 .. p1}, LX/8a1;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 2775344
    :cond_2
    const-string v6, "description_font_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2775345
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 2775346
    :cond_3
    const-string v6, "description_line_height_multiplier"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2775347
    const/4 v2, 0x1

    .line 2775348
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v20, v6

    goto :goto_1

    .line 2775349
    :cond_4
    const-string v6, "digest_cards"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2775350
    invoke-static/range {p0 .. p1}, LX/K8T;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2775351
    :cond_5
    const-string v6, "digest_owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2775352
    invoke-static/range {p0 .. p1}, LX/K8X;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 2775353
    :cond_6
    const-string v6, "digest_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2775354
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2775355
    :cond_7
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2775356
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2775357
    :cond_8
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2775358
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2775359
    :cond_9
    const-string v6, "title_font"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2775360
    invoke-static/range {p0 .. p1}, LX/8a1;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2775361
    :cond_a
    const-string v6, "title_font_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2775362
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2775363
    :cond_b
    const-string v6, "title_line_height_multiplier"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2775364
    const/4 v2, 0x1

    .line 2775365
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto/16 :goto_1

    .line 2775366
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2775367
    :cond_d
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2775368
    if-eqz v3, :cond_e

    .line 2775369
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2775370
    :cond_e
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775371
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775372
    if-eqz v9, :cond_f

    .line 2775373
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2775374
    :cond_f
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775375
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775376
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775377
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2775378
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2775379
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2775380
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2775381
    if-eqz v8, :cond_10

    .line 2775382
    const/16 v3, 0xb

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2775383
    :cond_10
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move-wide/from16 v20, v14

    move/from16 v22, v17

    move v14, v9

    move v15, v10

    move/from16 v17, v12

    move v12, v5

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v18

    move/from16 v18, v13

    move/from16 v19, v16

    move v13, v8

    move/from16 v16, v11

    move-wide v10, v6

    move v8, v2

    goto/16 :goto_1
.end method
