.class public LX/JiS;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725944
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, LX/JiS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725945
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2725939
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725940
    const v0, 0x7f030cf1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725941
    const v0, 0x7f0d204c

    invoke-virtual {p0, v0}, LX/JiS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JiS;->a:Landroid/widget/TextView;

    .line 2725942
    const v0, 0x7f0d204b

    invoke-virtual {p0, v0}, LX/JiS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/JiS;->b:Landroid/widget/ImageView;

    .line 2725943
    return-void
.end method


# virtual methods
.method public setIconResource(I)V
    .locals 1

    .prologue
    .line 2725946
    iget-object v0, p0, LX/JiS;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2725947
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2725935
    iget-object v0, p0, LX/JiS;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725936
    return-void
.end method

.method public setTextResource(I)V
    .locals 2

    .prologue
    .line 2725937
    iget-object v0, p0, LX/JiS;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/JiS;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725938
    return-void
.end method
