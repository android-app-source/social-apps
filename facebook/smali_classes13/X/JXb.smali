.class public final LX/JXb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67q;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;I)V
    .locals 0

    .prologue
    .line 2704778
    iput-object p1, p0, LX/JXb;->b:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iput p2, p0, LX/JXb;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/68u;)V
    .locals 6

    .prologue
    .line 2704779
    iget v0, p1, LX/68u;->C:F

    move v0, v0

    .line 2704780
    iget v1, p0, LX/JXb;->a:I

    int-to-double v2, v1

    float-to-double v0, v0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 2704781
    iget-object v1, p0, LX/JXb;->b:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v1, v1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 2704782
    return-void
.end method
