.class public LX/Jg1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jfy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jfy",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/03V;

.field public d:LX/15i;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2722444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722445
    iput-object p1, p0, LX/Jg1;->a:LX/0tX;

    .line 2722446
    iput-object p2, p0, LX/Jg1;->b:LX/1Ck;

    .line 2722447
    iput-object p3, p0, LX/Jg1;->c:LX/03V;

    .line 2722448
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2722449
    iget-object v0, p0, LX/Jg1;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2722450
    return-void
.end method

.method public final a(Ljava/lang/String;LX/Jfp;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/Jfp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/Jfp",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2722451
    if-nez p1, :cond_0

    .line 2722452
    :goto_0
    return-void

    .line 2722453
    :cond_0
    new-instance v0, LX/CL2;

    invoke-direct {v0}, LX/CL2;-><init>()V

    move-object v0, v0

    .line 2722454
    const-string v1, "station_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2722455
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, LX/Jg1;->e:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 2722456
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, LX/Jg1;->d:LX/15i;

    iget v3, p0, LX/Jg1;->e:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2722457
    const-string v1, "after"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2722458
    :cond_1
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2722459
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2722460
    move-object v0, v0

    .line 2722461
    iget-object v1, p0, LX/Jg1;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2722462
    iget-object v1, p0, LX/Jg1;->b:LX/1Ck;

    const-string v2, "load_substations"

    .line 2722463
    new-instance v3, LX/Jg0;

    invoke-direct {v3, p0, p2}, LX/Jg0;-><init>(LX/Jg1;LX/Jfp;)V

    move-object v3, v3

    .line 2722464
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2722465
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2722466
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2722467
    iget-object v0, p0, LX/Jg1;->b:LX/1Ck;

    const-string v1, "load_substations"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2722468
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, LX/Jg1;->e:I

    monitor-exit v1

    if-nez v2, :cond_0

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, LX/Jg1;->d:LX/15i;

    iget v3, p0, LX/Jg1;->e:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v2, v3, v0}, LX/15i;->h(II)Z

    move-result v0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2722469
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/Jg1;->d:LX/15i;

    const/4 v0, 0x0

    iput v0, p0, LX/Jg1;->e:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
