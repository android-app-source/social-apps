.class public final LX/JjQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventUpdateModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jj7;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/JjT;


# direct methods
.method public constructor <init>(LX/JjT;LX/Jj7;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2727666
    iput-object p1, p0, LX/JjQ;->c:LX/JjT;

    iput-object p2, p0, LX/JjQ;->a:LX/Jj7;

    iput-object p3, p0, LX/JjQ;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2727662
    iget-object v0, p0, LX/JjQ;->c:LX/JjT;

    iget-object v0, v0, LX/JjT;->b:LX/JjG;

    const-string v1, "EventReminderMutator"

    const-string v2, "Failed to update an event reminder."

    invoke-virtual {v0, v1, v2, p1}, LX/JjG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2727663
    iget-object v0, p0, LX/JjQ;->a:LX/Jj7;

    if-eqz v0, :cond_0

    .line 2727664
    iget-object v0, p0, LX/JjQ;->a:LX/Jj7;

    invoke-interface {v0}, LX/Jj7;->a()V

    .line 2727665
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2727659
    iget-object v0, p0, LX/JjQ;->a:LX/Jj7;

    if-eqz v0, :cond_0

    .line 2727660
    iget-object v0, p0, LX/JjQ;->a:LX/Jj7;

    invoke-interface {v0}, LX/Jj7;->b()V

    .line 2727661
    :cond_0
    return-void
.end method
