.class public LX/Jqm;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/Jrb;

.field private final c:LX/2N4;

.field private final d:LX/Jqb;

.field private final e:LX/0SG;

.field private final f:LX/Jrc;

.field public final g:LX/Do7;

.field private final h:LX/Jqf;

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740350
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqm;->j:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/FDs;LX/Jrb;LX/Jqb;LX/0SG;LX/Jrc;LX/Do7;LX/2N4;LX/0Ot;LX/Jqf;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/Jrb;",
            "LX/Jqb;",
            "LX/0SG;",
            "LX/Jrc;",
            "LX/Do7;",
            "LX/2N4;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jqf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2740338
    invoke-direct {p0, p8}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2740339
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740340
    iput-object v0, p0, LX/Jqm;->i:LX/0Ot;

    .line 2740341
    iput-object p1, p0, LX/Jqm;->a:LX/FDs;

    .line 2740342
    iput-object p2, p0, LX/Jqm;->b:LX/Jrb;

    .line 2740343
    iput-object p3, p0, LX/Jqm;->d:LX/Jqb;

    .line 2740344
    iput-object p4, p0, LX/Jqm;->e:LX/0SG;

    .line 2740345
    iput-object p5, p0, LX/Jqm;->f:LX/Jrc;

    .line 2740346
    iput-object p6, p0, LX/Jqm;->g:LX/Do7;

    .line 2740347
    iput-object p7, p0, LX/Jqm;->c:LX/2N4;

    .line 2740348
    iput-object p9, p0, LX/Jqm;->h:LX/Jqf;

    .line 2740349
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2740261
    invoke-virtual {p1}, LX/6kW;->q()LX/6jg;

    move-result-object v0

    .line 2740262
    iget-object v1, p0, LX/Jqm;->f:LX/Jrc;

    iget-object v0, v0, LX/6jg;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2740263
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jqm;
    .locals 7

    .prologue
    .line 2740311
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740312
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740313
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740314
    if-nez v1, :cond_0

    .line 2740315
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740316
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740317
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740318
    sget-object v1, LX/Jqm;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740319
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740320
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740321
    :cond_1
    if-nez v1, :cond_4

    .line 2740322
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740323
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740324
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqm;->b(LX/0QB;)LX/Jqm;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2740325
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740326
    if-nez v1, :cond_2

    .line 2740327
    sget-object v0, LX/Jqm;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqm;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740328
    :goto_1
    if-eqz v0, :cond_3

    .line 2740329
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740330
    :goto_3
    check-cast v0, LX/Jqm;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740331
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740332
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740333
    :catchall_1
    move-exception v0

    .line 2740334
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740335
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740336
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740337
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqm;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqm;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Jqm;
    .locals 10

    .prologue
    .line 2740307
    new-instance v0, LX/Jqm;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {p0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v2

    check-cast v2, LX/Jrb;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v3

    check-cast v3, LX/Jqb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v5

    check-cast v5, LX/Jrc;

    invoke-static {p0}, LX/Do7;->a(LX/0QB;)LX/Do7;

    move-result-object v6

    check-cast v6, LX/Do7;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v7

    check-cast v7, LX/2N4;

    const/16 v8, 0x35bd

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/Jqf;->a(LX/0QB;)LX/Jqf;

    move-result-object v9

    check-cast v9, LX/Jqf;

    invoke-direct/range {v0 .. v9}, LX/Jqm;-><init>(LX/FDs;LX/Jrb;LX/Jqb;LX/0SG;LX/Jrc;LX/Do7;LX/2N4;LX/0Ot;LX/Jqf;)V

    .line 2740308
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2740309
    iput-object v1, v0, LX/Jqm;->i:LX/0Ot;

    .line 2740310
    return-object v0
.end method

.method private b(LX/6kn;)Z
    .locals 1

    .prologue
    .line 2740351
    iget-object v0, p1, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jqm;->g:LX/Do7;

    invoke-virtual {v0}, LX/Do7;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740303
    check-cast p1, LX/6kW;

    .line 2740304
    invoke-virtual {p1}, LX/6kW;->q()LX/6jg;

    move-result-object v0

    iget-object v0, v0, LX/6jg;->messageMetadata:LX/6kn;

    invoke-direct {p0, v0}, LX/Jqm;->b(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2740305
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740306
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, LX/Jqm;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2740275
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->q()LX/6jg;

    move-result-object v8

    .line 2740276
    if-nez p1, :cond_0

    iget-object v0, v8, LX/6jg;->messageMetadata:LX/6kn;

    invoke-direct {p0, v0}, LX/Jqm;->b(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2740277
    iget-object v0, p0, LX/Jqm;->c:LX/2N4;

    iget-object v1, p0, LX/Jqm;->f:LX/Jrc;

    iget-object v2, v8, LX/6jg;->messageMetadata:LX/6kn;

    iget-object v2, v2, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2740278
    iget-object p1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740279
    if-nez p1, :cond_0

    .line 2740280
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2740281
    :goto_0
    return-object v0

    .line 2740282
    :cond_0
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/Jqm;->b:LX/Jrb;

    .line 2740283
    iget-object v3, v8, LX/6jg;->messageMetadata:LX/6kn;

    invoke-static {v0, v3, p1}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v3

    sget-object v5, LX/2uW;->ADMIN:LX/2uW;

    .line 2740284
    iput-object v5, v3, LX/6f7;->l:LX/2uW;

    .line 2740285
    move-object v3, v3

    .line 2740286
    invoke-static {v8}, LX/Jrb;->a(LX/6jg;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v5

    .line 2740287
    iput-object v5, v3, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 2740288
    move-object v3, v3

    .line 2740289
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2740290
    iget-object v5, v0, LX/Jrb;->f:LX/3QS;

    sget-object v6, LX/6fK;->SYNC_PROTOCOL_ADMIN_TEXT_MESSAGE_DELTA:LX/6fK;

    invoke-virtual {v5, v6, v3}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2740291
    move-object v3, v3

    .line 2740292
    iget-object v0, p0, LX/Jqm;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740293
    iget-object v0, p0, LX/Jqm;->g:LX/Do7;

    invoke-virtual {v0}, LX/Do7;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Jqm;->a:LX/FDs;

    iget-wide v2, p2, LX/7GJ;->b:J

    iget-object v4, v8, LX/6jg;->messageMetadata:LX/6kn;

    invoke-static {v4}, LX/JrZ;->a(LX/6kn;)LX/6jT;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2740294
    :goto_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2740295
    const-string v2, "newMessageResult"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2740296
    iget-object v2, v8, LX/6jg;->messageMetadata:LX/6kn;

    .line 2740297
    if-eqz v2, :cond_1

    .line 2740298
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v2, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2740299
    if-eqz v2, :cond_1

    .line 2740300
    iget-object v2, p0, LX/Jqm;->h:LX/Jqf;

    invoke-virtual {v2, v0}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    :cond_1
    move-object v0, v1

    .line 2740301
    goto :goto_0

    .line 2740302
    :cond_2
    iget-object v0, p0, LX/Jqm;->a:LX/FDs;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740265
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2740266
    if-eqz v0, :cond_0

    .line 2740267
    iget-object v1, p0, LX/Jqm;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v4, p2, LX/7GJ;->b:J

    iget-object v2, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v2, LX/6kW;

    invoke-virtual {v2}, LX/6kW;->q()LX/6jg;

    move-result-object v2

    iget-object v2, v2, LX/6jg;->messageMetadata:LX/6kn;

    .line 2740268
    iget-object v3, p0, LX/Jqm;->g:LX/Do7;

    invoke-virtual {v3}, LX/Do7;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, LX/JrZ;->a(LX/6kn;)LX/6jT;

    move-result-object v3

    :goto_0
    move-object v2, v3

    .line 2740269
    invoke-virtual {v1, v0, v4, v5, v2}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)V

    .line 2740270
    iget-object v1, p0, LX/Jqm;->d:LX/Jqb;

    .line 2740271
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2740272
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2740273
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2740274
    :cond_0
    return-void

    :cond_1
    sget-object v3, LX/6jT;->a:LX/6jT;

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740264
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/Jqm;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
