.class public LX/JqH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/ContentResolver;

.field public final c:LX/0ad;

.field private final d:LX/1Ml;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/content/ContentResolver;LX/0ad;LX/1Ml;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Landroid/content/ContentResolver;",
            "LX/0ad;",
            "LX/1Ml;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738615
    iput-object p1, p0, LX/JqH;->a:LX/0Ot;

    .line 2738616
    iput-object p2, p0, LX/JqH;->b:Landroid/content/ContentResolver;

    .line 2738617
    iput-object p3, p0, LX/JqH;->c:LX/0ad;

    .line 2738618
    iput-object p4, p0, LX/JqH;->d:LX/1Ml;

    .line 2738619
    return-void
.end method

.method public static a(LX/JqH;LX/DAB;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2738671
    iget-object v0, p1, LX/DAB;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2738672
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2738673
    iget-object v0, p0, LX/JqH;->b:Landroid/content/ContentResolver;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2738674
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    .line 2738675
    :cond_0
    if-eqz v1, :cond_1

    .line 2738676
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2738677
    :cond_1
    :goto_0
    return-void

    .line 2738678
    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2738679
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2738680
    iput v0, p1, LX/DAB;->e:I

    .line 2738681
    const-string v0, "display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2738682
    iput-object v0, p1, LX/DAB;->b:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2738683
    if-eqz v1, :cond_1

    .line 2738684
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2738685
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 2738686
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2738687
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(LX/JqH;Ljava/lang/String;LX/DAB;)V
    .locals 7

    .prologue
    .line 2738654
    const/4 v6, 0x0

    .line 2738655
    :try_start_0
    iget-object v0, p0, LX/JqH;->b:Landroid/content/ContentResolver;

    sget-object v1, LX/554;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "address"

    aput-object v4, v2, v3

    .line 2738656
    iget-object v3, p0, LX/JqH;->c:LX/0ad;

    sget-short v4, LX/JqA;->b:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "thread_id=? and type=? and date>=?"

    :goto_0
    move-object v3, v3

    .line 2738657
    invoke-static {p0, p1}, LX/JqH;->a(LX/JqH;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-string v5, "date DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2738658
    :try_start_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2738659
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 2738660
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2738661
    :cond_1
    return-void

    .line 2738662
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_2

    .line 2738663
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2738664
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_3
    :try_start_2
    const-string v3, "thread_id=? and type=?"

    goto :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2738665
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 2738666
    iput v0, p2, LX/DAB;->d:I

    .line 2738667
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2738668
    const-string v0, "address"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2738669
    iput-object v0, p2, LX/DAB;->a:Ljava/lang/String;

    .line 2738670
    goto :goto_1
.end method

.method private static a(LX/JqH;Ljava/lang/String;)[Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2738651
    iget-object v0, p0, LX/JqH;->c:LX/0ad;

    sget-short v1, LX/JqA;->b:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v2

    const-string v1, "2"

    aput-object v1, v0, v3

    .line 2738652
    iget-object v5, p0, LX/JqH;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    const-wide v7, 0x9a7ec800L

    sub-long/2addr v5, v7

    move-wide v2, v5

    .line 2738653
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/String;

    aput-object p1, v0, v2

    const-string v1, "2"

    aput-object v1, v0, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "LX/DAC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2738620
    iget-object v0, p0, LX/JqH;->d:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v6

    .line 2738621
    :cond_0
    :goto_0
    return-object v0

    .line 2738622
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/JqH;->b:Landroid/content/ContentResolver;

    sget-object v1, LX/553;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "thread_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "msg_count"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "msg_count DESC LIMIT 20"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2738623
    const/4 v1, 0x0

    .line 2738624
    if-nez v6, :cond_3

    .line 2738625
    const/4 v0, 0x0

    .line 2738626
    :goto_1
    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2738627
    if-eqz v6, :cond_0

    .line 2738628
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2738629
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 2738630
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2738631
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2738632
    const-string v0, "thread_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 2738633
    const-string v0, "msg_count"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move v0, v1

    .line 2738634
    :cond_4
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    const/16 v5, 0xa

    if-ge v0, v5, :cond_5

    .line 2738635
    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2738636
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 2738637
    new-instance v8, LX/DAB;

    invoke-direct {v8}, LX/DAB;-><init>()V

    move-object v8, v8

    .line 2738638
    iput v7, v8, LX/DAB;->c:I

    .line 2738639
    move-object v7, v8

    .line 2738640
    invoke-static {p0, v5, v7}, LX/JqH;->a(LX/JqH;Ljava/lang/String;LX/DAB;)V

    .line 2738641
    iget-object v5, v7, LX/DAB;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2738642
    if-eqz v5, :cond_4

    .line 2738643
    invoke-static {p0, v7}, LX/JqH;->a(LX/JqH;LX/DAB;)V

    .line 2738644
    iget v5, v7, LX/DAB;->e:I

    move v5, v5

    .line 2738645
    const/4 v8, -0x1

    if-eq v5, v8, :cond_4

    .line 2738646
    new-instance v5, LX/DAC;

    invoke-direct {v5, v7}, LX/DAC;-><init>(LX/DAB;)V

    move-object v5, v5

    .line 2738647
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2738648
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2738649
    :cond_5
    new-instance v0, LX/JqG;

    invoke-direct {v0, p0}, LX/JqG;-><init>(LX/JqH;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2738650
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_6

    invoke-interface {v2, v1, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :goto_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v0, v2

    goto :goto_3
.end method
