.class public LX/Jza;
.super LX/Jyw;
.source ""


# instance fields
.field private final e:LX/JzS;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/JzX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2756314
    invoke-direct {p0}, LX/Jyw;-><init>()V

    .line 2756315
    const-string v0, "transforms"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v2

    .line 2756316
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, LX/5pC;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/Jza;->f:Ljava/util/List;

    move v0, v1

    .line 2756317
    :goto_0
    invoke-interface {v2}, LX/5pC;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2756318
    invoke-interface {v2, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v3

    .line 2756319
    const-string v4, "property"

    invoke-interface {v3, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2756320
    const-string v5, "type"

    invoke-interface {v3, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2756321
    const-string v6, "animated"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2756322
    new-instance v5, LX/JzY;

    invoke-direct {v5, p0}, LX/JzY;-><init>(LX/Jza;)V

    .line 2756323
    iput-object v4, v5, LX/JzY;->c:Ljava/lang/String;

    .line 2756324
    const-string v4, "nodeTag"

    invoke-interface {v3, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v5, LX/JzY;->a:I

    .line 2756325
    iget-object v3, p0, LX/Jza;->f:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2756326
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2756327
    :cond_0
    new-instance v5, LX/JzZ;

    invoke-direct {v5, p0}, LX/JzZ;-><init>(LX/Jza;)V

    .line 2756328
    iput-object v4, v5, LX/JzZ;->c:Ljava/lang/String;

    .line 2756329
    const-string v4, "value"

    invoke-interface {v3, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    iput-wide v6, v5, LX/JzZ;->a:D

    .line 2756330
    iget-object v3, p0, LX/Jza;->f:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2756331
    :cond_1
    iput-object p2, p0, LX/Jza;->e:LX/JzS;

    .line 2756332
    return-void
.end method


# virtual methods
.method public final a(LX/5pI;)V
    .locals 7

    .prologue
    .line 2756333
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, LX/Jza;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2756334
    iget-object v0, p0, LX/Jza;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JzX;

    .line 2756335
    instance-of v1, v0, LX/JzY;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 2756336
    check-cast v1, LX/JzY;

    iget v1, v1, LX/JzY;->a:I

    .line 2756337
    iget-object v2, p0, LX/Jza;->e:LX/JzS;

    invoke-virtual {v2, v1}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v1

    .line 2756338
    if-nez v1, :cond_0

    .line 2756339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Mapped style node does not exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756340
    :cond_0
    instance-of v2, v1, LX/Jyx;

    if-eqz v2, :cond_1

    .line 2756341
    check-cast v1, LX/Jyx;

    invoke-virtual {v1}, LX/Jyx;->b()D

    move-result-wide v2

    .line 2756342
    :goto_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, LX/JzX;->c:Ljava/lang/String;

    aput-object v0, v1, v6

    const/4 v0, 0x1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, LX/5pI;->a([Ljava/lang/Object;)LX/5pI;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2756343
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported type of node used as a transform child node "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v1, v0

    .line 2756344
    check-cast v1, LX/JzZ;

    iget-wide v2, v1, LX/JzZ;->a:D

    goto :goto_1

    .line 2756345
    :cond_3
    const-string v0, "transform"

    invoke-static {v4}, LX/5pE;->a(Ljava/util/List;)LX/5pE;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/5pI;->a(Ljava/lang/String;LX/5pD;)V

    .line 2756346
    return-void
.end method
