.class public final LX/Jiu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2727008
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2727009
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2727010
    :goto_0
    return v1

    .line 2727011
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2727012
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2727013
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2727014
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2727015
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2727016
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2727017
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2727018
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2727019
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2727020
    const/4 v3, 0x0

    .line 2727021
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 2727022
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2727023
    :goto_3
    move v2, v3

    .line 2727024
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2727025
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2727026
    goto :goto_1

    .line 2727027
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2727028
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2727029
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2727030
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2727031
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2727032
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2727033
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2727034
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 2727035
    const-string v6, "timeline_context_list_item_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2727036
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_4

    .line 2727037
    :cond_7
    const-string v6, "title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2727038
    const/4 v5, 0x0

    .line 2727039
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 2727040
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2727041
    :goto_5
    move v2, v5

    .line 2727042
    goto :goto_4

    .line 2727043
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2727044
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2727045
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2727046
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_9
    move v2, v3

    move v4, v3

    goto :goto_4

    .line 2727047
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2727048
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 2727049
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2727050
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2727051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 2727052
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2727053
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_6

    .line 2727054
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2727055
    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 2727056
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v2, v5

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2726982
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2726983
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2726984
    if-eqz v0, :cond_4

    .line 2726985
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726986
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2726987
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2726988
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p1, 0x0

    .line 2726989
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2726990
    invoke-virtual {p0, v2, p1}, LX/15i;->g(II)I

    move-result v3

    .line 2726991
    if-eqz v3, :cond_0

    .line 2726992
    const-string v3, "timeline_context_list_item_type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726993
    invoke-virtual {p0, v2, p1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2726994
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2726995
    if-eqz v3, :cond_2

    .line 2726996
    const-string p1, "title"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726997
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2726998
    const/4 p1, 0x0

    invoke-virtual {p0, v3, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 2726999
    if-eqz p1, :cond_1

    .line 2727000
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2727001
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2727002
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2727003
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2727004
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2727005
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2727006
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2727007
    return-void
.end method
