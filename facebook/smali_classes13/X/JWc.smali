.class public final LX/JWc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JWd;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

.field public b:Z

.field public final synthetic c:LX/JWd;


# direct methods
.method public constructor <init>(LX/JWd;)V
    .locals 1

    .prologue
    .line 2703245
    iput-object p1, p0, LX/JWc;->c:LX/JWd;

    .line 2703246
    move-object v0, p1

    .line 2703247
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2703248
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2703249
    const-string v0, "FutureFriendingCredentialComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2703250
    if-ne p0, p1, :cond_1

    .line 2703251
    :cond_0
    :goto_0
    return v0

    .line 2703252
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2703253
    goto :goto_0

    .line 2703254
    :cond_3
    check-cast p1, LX/JWc;

    .line 2703255
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2703256
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2703257
    if-eq v2, v3, :cond_0

    .line 2703258
    iget-object v2, p0, LX/JWc;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JWc;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v3, p1, LX/JWc;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2703259
    goto :goto_0

    .line 2703260
    :cond_5
    iget-object v2, p1, LX/JWc;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    if-nez v2, :cond_4

    .line 2703261
    :cond_6
    iget-boolean v2, p0, LX/JWc;->b:Z

    iget-boolean v3, p1, LX/JWc;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2703262
    goto :goto_0
.end method
