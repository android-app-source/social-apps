.class public LX/K16;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K1A;",
        ">;"
    }
.end annotation


# instance fields
.field private a:F

.field private b:F


# direct methods
.method public constructor <init>(IFF)V
    .locals 0

    .prologue
    .line 2761135
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2761136
    iput p2, p0, LX/K16;->a:F

    .line 2761137
    iput p3, p0, LX/K16;->b:F

    .line 2761138
    return-void
.end method

.method private j()LX/5pH;
    .locals 6

    .prologue
    .line 2761139
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2761140
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2761141
    const-string v2, "width"

    iget v3, p0, LX/K16;->a:F

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761142
    const-string v2, "height"

    iget v3, p0, LX/K16;->b:F

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761143
    const-string v2, "contentSize"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2761144
    const-string v1, "target"

    .line 2761145
    iget v2, p0, LX/5r0;->c:I

    move v2, v2

    .line 2761146
    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761147
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2761148
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2761149
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K16;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2761150
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761151
    const-string v0, "topContentSizeChange"

    return-object v0
.end method
