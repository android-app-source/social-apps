.class public final LX/K1Q;
.super LX/0gG;
.source ""


# instance fields
.field public final synthetic a:LX/K1S;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/K1S;)V
    .locals 1

    .prologue
    .line 2761980
    iput-object p1, p0, LX/K1Q;->a:LX/K1S;

    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2761981
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    .line 2761982
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K1Q;->c:Z

    return-void
.end method

.method public synthetic constructor <init>(LX/K1S;B)V
    .locals 0

    .prologue
    .line 2761979
    invoke-direct {p0, p1}, LX/K1Q;-><init>(LX/K1S;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2761978
    iget-boolean v0, p0, LX/K1Q;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2761975
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2761976
    const/4 v1, 0x0

    iget-object v2, p0, LX/K1Q;->a:LX/K1S;

    invoke-static {v2}, LX/K1S;->a(LX/K1S;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2761977
    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2761971
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2761972
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2761973
    iget-object v0, p0, LX/K1Q;->a:LX/K1S;

    iget-object v1, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2761974
    return-void
.end method

.method public final a(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    .line 2761983
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2761984
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 2761985
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K1Q;->c:Z

    .line 2761986
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2761969
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2761970
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2761964
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2761965
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2761966
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2761967
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K1Q;->c:Z

    .line 2761968
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2761957
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2761963
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2761962
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final b(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2761958
    iget-object v0, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2761959
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2761960
    iget-object v0, p0, LX/K1Q;->a:LX/K1S;

    iget-object v1, p0, LX/K1Q;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2761961
    return-void
.end method
