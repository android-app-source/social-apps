.class public LX/JzT;
.super LX/Jyw;
.source ""


# instance fields
.field public e:I

.field private final f:LX/JzS;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 5

    .prologue
    .line 2756203
    invoke-direct {p0}, LX/Jyw;-><init>()V

    .line 2756204
    const/4 v0, -0x1

    iput v0, p0, LX/JzT;->e:I

    .line 2756205
    const-string v0, "props"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 2756206
    invoke-interface {v0}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v1

    .line 2756207
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, LX/JzT;->g:Ljava/util/Map;

    .line 2756208
    :goto_0
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2756209
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v2

    .line 2756210
    invoke-interface {v0, v2}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2756211
    iget-object v4, p0, LX/JzT;->g:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2756212
    :cond_0
    iput-object p2, p0, LX/JzT;->f:LX/JzS;

    .line 2756213
    return-void
.end method


# virtual methods
.method public final a(LX/5rN;)V
    .locals 6

    .prologue
    .line 2756189
    iget v0, p0, LX/JzT;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2756190
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Node has not been attached to a view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756191
    :cond_0
    new-instance v2, LX/5pI;

    invoke-direct {v2}, LX/5pI;-><init>()V

    .line 2756192
    iget-object v0, p0, LX/JzT;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2756193
    iget-object v4, p0, LX/JzT;->f:LX/JzS;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4, v1}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v1

    .line 2756194
    if-nez v1, :cond_1

    .line 2756195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Mapped property node does not exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756196
    :cond_1
    instance-of v4, v1, LX/JzW;

    if-eqz v4, :cond_2

    move-object v0, v1

    .line 2756197
    check-cast v0, LX/JzW;

    invoke-virtual {v0, v2}, LX/JzW;->a(LX/5pI;)V

    goto :goto_0

    .line 2756198
    :cond_2
    instance-of v4, v1, LX/Jyx;

    if-eqz v4, :cond_3

    .line 2756199
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, LX/Jyx;

    invoke-virtual {v1}, LX/Jyx;->b()D

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, LX/5pI;->putDouble(Ljava/lang/String;D)V

    goto :goto_0

    .line 2756200
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported type of node used in property node "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756201
    :cond_4
    iget v0, p0, LX/JzT;->e:I

    new-instance v1, LX/5rC;

    invoke-direct {v1, v2}, LX/5rC;-><init>(LX/5pG;)V

    invoke-virtual {p1, v0, v1}, LX/5rN;->a(ILX/5rC;)V

    .line 2756202
    return-void
.end method
