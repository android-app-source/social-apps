.class public LX/Jp4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Jp4;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/FKr;


# direct methods
.method public constructor <init>(LX/18V;LX/FKr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2736992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736993
    iput-object p1, p0, LX/Jp4;->a:LX/18V;

    .line 2736994
    iput-object p2, p0, LX/Jp4;->b:LX/FKr;

    .line 2736995
    return-void
.end method

.method public static a(LX/0QB;)LX/Jp4;
    .locals 5

    .prologue
    .line 2736996
    sget-object v0, LX/Jp4;->c:LX/Jp4;

    if-nez v0, :cond_1

    .line 2736997
    const-class v1, LX/Jp4;

    monitor-enter v1

    .line 2736998
    :try_start_0
    sget-object v0, LX/Jp4;->c:LX/Jp4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2736999
    if-eqz v2, :cond_0

    .line 2737000
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2737001
    new-instance p0, LX/Jp4;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static {v0}, LX/FKr;->a(LX/0QB;)LX/FKr;

    move-result-object v4

    check-cast v4, LX/FKr;

    invoke-direct {p0, v3, v4}, LX/Jp4;-><init>(LX/18V;LX/FKr;)V

    .line 2737002
    move-object v0, p0

    .line 2737003
    sput-object v0, LX/Jp4;->c:LX/Jp4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2737004
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2737005
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2737006
    :cond_1
    sget-object v0, LX/Jp4;->c:LX/Jp4;

    return-object v0

    .line 2737007
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2737008
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2737009
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2737010
    const-string v1, "push_trace_confirmation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2737011
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2737012
    const-string v1, "traceInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2737013
    iget-object v1, p0, LX/Jp4;->a:LX/18V;

    iget-object v2, p0, LX/Jp4;->b:LX/FKr;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2737014
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2737015
    move-object v0, v0

    .line 2737016
    return-object v0

    .line 2737017
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
