.class public final LX/JZx;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/JZx;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JZv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/JZy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2709356
    const/4 v0, 0x0

    sput-object v0, LX/JZx;->a:LX/JZx;

    .line 2709357
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JZx;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2709358
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2709359
    new-instance v0, LX/JZy;

    invoke-direct {v0}, LX/JZy;-><init>()V

    iput-object v0, p0, LX/JZx;->c:LX/JZy;

    .line 2709360
    return-void
.end method

.method public static declared-synchronized q()LX/JZx;
    .locals 2

    .prologue
    .line 2709361
    const-class v1, LX/JZx;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/JZx;->a:LX/JZx;

    if-nez v0, :cond_0

    .line 2709362
    new-instance v0, LX/JZx;

    invoke-direct {v0}, LX/JZx;-><init>()V

    sput-object v0, LX/JZx;->a:LX/JZx;

    .line 2709363
    :cond_0
    sget-object v0, LX/JZx;->a:LX/JZx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2709364
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2709365
    check-cast p2, LX/JZw;

    .line 2709366
    iget-object v0, p2, LX/JZw;->a:Ljava/lang/String;

    const/4 p2, 0x1

    .line 2709367
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b02aa

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00bd

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f0b02ab

    invoke-interface {v2, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const p0, 0x7f0a009a

    invoke-interface {v2, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v2, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    const p0, 0x7f0b0060

    invoke-interface {v1, v2, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    const p0, 0x7f0b0060

    invoke-interface {v1, v2, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x3

    const p0, 0x7f0b0060

    invoke-interface {v1, v2, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->d(Z)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2709368
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2709369
    invoke-static {}, LX/1dS;->b()V

    .line 2709370
    const/4 v0, 0x0

    return-object v0
.end method
