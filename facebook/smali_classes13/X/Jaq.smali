.class public LX/Jaq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DVF;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2715999
    iput-object p1, p0, LX/Jaq;->a:LX/0Or;

    .line 2716000
    return-void
.end method

.method public static a(LX/0QB;)LX/Jaq;
    .locals 1

    .prologue
    .line 2715997
    invoke-static {p0}, LX/Jaq;->b(LX/0QB;)LX/Jaq;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2716001
    iget-object v0, p0, LX/Jaq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 2716002
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2716003
    const-string v1, "title"

    const v2, 0x7f083081

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2716004
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_MEMBER_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2716005
    const-string v1, "non_modal_display"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2716006
    const-string v1, "is_show_caspian_style"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2716007
    const-string v1, "is_sticky_header_off"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2716008
    const-string v1, "hide_caspian_send_button"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2716009
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2716010
    if-eqz p2, :cond_0

    .line 2716011
    const-string v1, "group_visibility"

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2716012
    :cond_0
    return-object v0
.end method

.method public static b(LX/0QB;)LX/Jaq;
    .locals 2

    .prologue
    .line 2715995
    new-instance v0, LX/Jaq;

    const/16 v1, 0xc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Jaq;-><init>(LX/0Or;)V

    .line 2715996
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2715994
    invoke-direct {p0, p1, p2}, LX/Jaq;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLandroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2715991
    invoke-direct {p0, p1, p2}, LX/Jaq;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)Landroid/content/Intent;

    move-result-object v0

    .line 2715992
    const-string v1, "enable_email_invite"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2715993
    return-object v0
.end method
