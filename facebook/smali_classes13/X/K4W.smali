.class public LX/K4W;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/33u;

.field public final c:LX/K4Z;

.field public final d:LX/0Sh;

.field public final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

.field public g:LX/33y;

.field public h:LX/K4k;

.field public i:LX/K4V;

.field public j:LX/9mm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768473
    const-class v0, LX/K4W;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K4W;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/33u;LX/K4Z;LX/0Sh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2768474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768475
    iput-object p1, p0, LX/K4W;->b:LX/33u;

    .line 2768476
    iput-object p2, p0, LX/K4W;->c:LX/K4Z;

    .line 2768477
    iput-object p3, p0, LX/K4W;->d:LX/0Sh;

    .line 2768478
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2768479
    return-void
.end method

.method public static a$redex0(LX/K4W;LX/5pX;)V
    .locals 1

    .prologue
    .line 2768480
    iget-object v0, p0, LX/K4W;->h:LX/K4k;

    if-nez v0, :cond_0

    .line 2768481
    :goto_0
    return-void

    .line 2768482
    :cond_0
    const-class v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {p1, v0}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    iput-object v0, p0, LX/K4W;->f:Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    .line 2768483
    iget-object v0, p0, LX/K4W;->h:LX/K4k;

    .line 2768484
    iget-object p0, v0, LX/K4k;->a:LX/K4o;

    const/4 p1, 0x1

    .line 2768485
    iput-boolean p1, p0, LX/K4o;->u:Z

    .line 2768486
    iget-object p0, v0, LX/K4k;->a:LX/K4o;

    invoke-static {p0}, LX/K4o;->s(LX/K4o;)V

    .line 2768487
    goto :goto_0
.end method
