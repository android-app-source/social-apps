.class public LX/JmM;
.super LX/1OM;
.source ""

# interfaces
.implements LX/Dct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U",
        "<",
        "Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;",
        ">;>;",
        "LX/Dct",
        "<",
        "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/JmT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Landroid/view/View$OnClickListener;

.field private final d:Landroid/view/View$OnLongClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2732663
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2732664
    new-instance v0, LX/JmK;

    invoke-direct {v0, p0}, LX/JmK;-><init>(LX/JmM;)V

    iput-object v0, p0, LX/JmM;->c:Landroid/view/View$OnClickListener;

    .line 2732665
    new-instance v0, LX/JmL;

    invoke-direct {v0, p0}, LX/JmL;-><init>(LX/JmM;)V

    iput-object v0, p0, LX/JmM;->d:Landroid/view/View$OnLongClickListener;

    .line 2732666
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2732667
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2732662
    iget-object v0, p0, LX/JmM;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2732658
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030896

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2732659
    iget-object v1, p0, LX/JmM;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2732660
    iget-object v1, p0, LX/JmM;->d:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2732661
    new-instance v1, LX/62U;

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2732651
    check-cast p1, LX/62U;

    .line 2732652
    iget-object v0, p0, LX/JmM;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    .line 2732653
    iget-object v1, p1, LX/62U;->l:Landroid/view/View;

    check-cast v1, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;

    .line 2732654
    invoke-virtual {v1, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)V

    .line 2732655
    invoke-virtual {v1, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->setTag(Ljava/lang/Object;)V

    .line 2732656
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2732657
    iget-object v0, p0, LX/JmM;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
