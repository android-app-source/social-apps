.class public LX/K1S;
.super Landroid/support/v4/view/ViewPager;
.source ""


# instance fields
.field public final a:LX/5s9;

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>(LX/5pX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2762028
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 2762029
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K1S;->c:Z

    .line 2762030
    const-class v0, LX/5rQ;

    invoke-virtual {p1, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2762031
    iget-object p1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, p1

    .line 2762032
    iput-object v0, p0, LX/K1S;->a:LX/5s9;

    .line 2762033
    iput-boolean v1, p0, LX/K1S;->b:Z

    .line 2762034
    new-instance v0, LX/K1R;

    invoke-direct {v0, p0}, LX/K1R;-><init>(LX/K1S;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2762035
    new-instance v0, LX/K1Q;

    invoke-direct {v0, p0}, LX/K1Q;-><init>(LX/K1S;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2762036
    return-void
.end method

.method public static synthetic a(LX/K1S;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2762027
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2762025
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/K1Q;->b(Landroid/view/View;I)V

    .line 2762026
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2762023
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/K1Q;->a(I)V

    .line 2762024
    return-void
.end method

.method public final b(IZ)V
    .locals 1

    .prologue
    .line 2762001
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K1S;->b:Z

    .line 2762002
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2762003
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K1S;->b:Z

    .line 2762004
    return-void
.end method

.method public final c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2762022
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/K1Q;->b(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2762020
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/K1Q;->a(Landroid/support/v4/view/ViewPager;)V

    .line 2762021
    return-void
.end method

.method public bridge synthetic getAdapter()LX/0gG;
    .locals 1

    .prologue
    .line 2762019
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()LX/K1Q;
    .locals 1

    .prologue
    .line 2762018
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    check-cast v0, LX/K1Q;

    return-object v0
.end method

.method public getViewCountInAdapter()I
    .locals 1

    .prologue
    .line 2762017
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    invoke-virtual {v0}, LX/K1Q;->b()I

    move-result v0

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2762012
    iget-boolean v1, p0, LX/K1S;->c:Z

    if-nez v1, :cond_1

    .line 2762013
    :cond_0
    :goto_0
    return v0

    .line 2762014
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2762015
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2762016
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x37db6541

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2762009
    iget-boolean v0, p0, LX/K1S;->c:Z

    if-nez v0, :cond_0

    .line 2762010
    const/4 v0, 0x0

    const v2, 0x7c2680c1

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2762011
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x7b865e0f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setScrollEnabled(Z)V
    .locals 0

    .prologue
    .line 2762007
    iput-boolean p1, p0, LX/K1S;->c:Z

    .line 2762008
    return-void
.end method

.method public setViews(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2762005
    invoke-virtual {p0}, LX/K1S;->getAdapter()LX/K1Q;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/K1Q;->a(Ljava/util/List;)V

    .line 2762006
    return-void
.end method
