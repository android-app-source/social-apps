.class public LX/JXX;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/35q;


# instance fields
.field private final a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/view/View;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2704662
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JXX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2704663
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2704664
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JXX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2704665
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2704666
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2704667
    const v0, 0x7f0311ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2704668
    const v0, 0x7f0d2a0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, LX/JXX;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 2704669
    const v0, 0x7f0d2a0f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JXX;->b:Landroid/widget/TextView;

    .line 2704670
    const v0, 0x7f0d1d00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JXX;->c:Landroid/view/View;

    .line 2704671
    const v0, 0x7f0d1d01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JXX;->d:Landroid/widget/TextView;

    .line 2704672
    const v0, 0x7f0d1d03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JXX;->e:Landroid/widget/TextView;

    .line 2704673
    return-void
.end method


# virtual methods
.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 2704674
    iget-object v0, p0, LX/JXX;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2704675
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2704676
    iget-object v0, p0, LX/JXX;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2704677
    return-void
.end method
