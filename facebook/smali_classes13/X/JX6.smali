.class public LX/JX6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/359;

.field public final d:LX/1DR;


# direct methods
.method public constructor <init>(LX/1nu;LX/359;LX/0Ot;LX/1DR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/359;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;",
            ">;",
            "LX/1DR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703944
    iput-object p1, p0, LX/JX6;->a:LX/1nu;

    .line 2703945
    iput-object p3, p0, LX/JX6;->b:LX/0Ot;

    .line 2703946
    iput-object p2, p0, LX/JX6;->c:LX/359;

    .line 2703947
    iput-object p4, p0, LX/JX6;->d:LX/1DR;

    .line 2703948
    return-void
.end method

.method public static a(LX/0QB;)LX/JX6;
    .locals 7

    .prologue
    .line 2703949
    const-class v1, LX/JX6;

    monitor-enter v1

    .line 2703950
    :try_start_0
    sget-object v0, LX/JX6;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703951
    sput-object v2, LX/JX6;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703952
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703953
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703954
    new-instance v6, LX/JX6;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/359;->a(LX/0QB;)LX/359;

    move-result-object v4

    check-cast v4, LX/359;

    const/16 v5, 0x2130

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-direct {v6, v3, v4, p0, v5}, LX/JX6;-><init>(LX/1nu;LX/359;LX/0Ot;LX/1DR;)V

    .line 2703955
    move-object v0, v6

    .line 2703956
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703957
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JX6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703958
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703959
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
