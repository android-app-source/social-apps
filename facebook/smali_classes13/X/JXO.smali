.class public LX/JXO;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:[Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2704452
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2704453
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JXO;->c:Z

    .line 2704454
    const/4 p1, 0x5

    .line 2704455
    const v0, 0x7f0311d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2704456
    new-array v0, p1, [Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    iput-object v0, p0, LX/JXO;->b:[Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    .line 2704457
    const v0, 0x7f0d29db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JXO;->a:Landroid/widget/TextView;

    .line 2704458
    const v0, 0x7f0d29dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2704459
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2704460
    iget-object v3, p0, LX/JXO;->b:[Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    aput-object v1, v3, v2

    .line 2704461
    iget-object v1, p0, LX/JXO;->b:[Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    aget-object v1, v1, v2

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->setVisibility(I)V

    .line 2704462
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2704463
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2704464
    iget-boolean v0, p0, LX/JXO;->c:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4282e46e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704465
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2704466
    const/4 v1, 0x1

    .line 2704467
    iput-boolean v1, p0, LX/JXO;->c:Z

    .line 2704468
    const/16 v1, 0x2d

    const v2, 0x1951f78f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x66588d85

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704469
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2704470
    const/4 v1, 0x0

    .line 2704471
    iput-boolean v1, p0, LX/JXO;->c:Z

    .line 2704472
    const/16 v1, 0x2d

    const v2, -0x73e21c6b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
