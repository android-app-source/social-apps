.class public final LX/Jh7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/10s;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

.field public final synthetic b:LX/3LT;


# direct methods
.method public constructor <init>(LX/3LT;Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V
    .locals 0

    .prologue
    .line 2724179
    iput-object p1, p0, LX/Jh7;->b:LX/3LT;

    iput-object p2, p0, LX/Jh7;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0gs;)V
    .locals 3

    .prologue
    .line 2724181
    iget-object v0, p0, LX/Jh7;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2724182
    :cond_0
    :goto_0
    return-void

    .line 2724183
    :cond_1
    sget-object v0, LX/0gs;->OPENED:LX/0gs;

    invoke-virtual {p1, v0}, LX/0gs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2724184
    iget-object v0, p0, LX/Jh7;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2724185
    iget-boolean v1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    move v0, v1

    .line 2724186
    if-eqz v0, :cond_2

    .line 2724187
    iget-object v0, p0, LX/Jh7;->b:LX/3LT;

    iget-object v1, p0, LX/Jh7;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2724188
    new-instance v2, LX/31Y;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v2, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const p0, 0x7f080208

    invoke-virtual {v2, p0}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const p0, 0x7f080209

    new-instance p1, LX/Jh9;

    invoke-direct {p1, v0, v1}, LX/Jh9;-><init>(LX/3LT;Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    invoke-virtual {v2, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const p0, 0x7f08020a

    new-instance p1, LX/Jh8;

    invoke-direct {p1, v0, v1}, LX/Jh8;-><init>(LX/3LT;Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    invoke-virtual {v2, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2724189
    goto :goto_0

    .line 2724190
    :cond_2
    iget-object v0, p0, LX/Jh7;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->l()V

    goto :goto_0
.end method

.method public final b(LX/0gs;)V
    .locals 0

    .prologue
    .line 2724180
    return-void
.end method
