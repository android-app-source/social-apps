.class public LX/Jr3;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field public final b:LX/Jqb;

.field private final c:LX/Jrc;

.field private final d:LX/FO4;

.field public e:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2My;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741574
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr3;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDs;LX/Jqb;LX/Jrc;LX/FO4;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2741562
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2741563
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741564
    iput-object v0, p0, LX/Jr3;->e:LX/0Ot;

    .line 2741565
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741566
    iput-object v0, p0, LX/Jr3;->f:LX/0Ot;

    .line 2741567
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741568
    iput-object v0, p0, LX/Jr3;->h:LX/0Ot;

    .line 2741569
    iput-object p1, p0, LX/Jr3;->a:LX/FDs;

    .line 2741570
    iput-object p2, p0, LX/Jr3;->b:LX/Jqb;

    .line 2741571
    iput-object p3, p0, LX/Jr3;->c:LX/Jrc;

    .line 2741572
    iput-object p4, p0, LX/Jr3;->d:LX/FO4;

    .line 2741573
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2741559
    invoke-virtual {p1}, LX/6kW;->g()LX/6jy;

    move-result-object v0

    .line 2741560
    iget-object v1, p0, LX/Jr3;->c:LX/Jrc;

    iget-object v0, v0, LX/6jy;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2741561
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jr3;
    .locals 10

    .prologue
    .line 2741499
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741500
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741501
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741502
    if-nez v1, :cond_0

    .line 2741503
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741504
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741505
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741506
    sget-object v1, LX/Jr3;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741507
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741508
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741509
    :cond_1
    if-nez v1, :cond_4

    .line 2741510
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741511
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741512
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741513
    new-instance p0, LX/Jr3;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v7

    check-cast v7, LX/Jqb;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v8

    check-cast v8, LX/Jrc;

    invoke-static {v0}, LX/FO4;->a(LX/0QB;)LX/FO4;

    move-result-object v9

    check-cast v9, LX/FO4;

    invoke-direct {p0, v1, v7, v8, v9}, LX/Jr3;-><init>(LX/FDs;LX/Jqb;LX/Jrc;LX/FO4;)V

    .line 2741514
    const/16 v1, 0xce5

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v1, 0xd18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v9, 0xd6c

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 2741515
    iput-object v7, p0, LX/Jr3;->e:LX/0Ot;

    iput-object v8, p0, LX/Jr3;->f:LX/0Ot;

    iput-object v1, p0, LX/Jr3;->g:LX/0SG;

    iput-object v9, p0, LX/Jr3;->h:LX/0Ot;

    .line 2741516
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741517
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741518
    if-nez v1, :cond_2

    .line 2741519
    sget-object v0, LX/Jr3;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr3;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741520
    :goto_1
    if-eqz v0, :cond_3

    .line 2741521
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741522
    :goto_3
    check-cast v0, LX/Jr3;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741523
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741524
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741525
    :catchall_1
    move-exception v0

    .line 2741526
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741527
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741528
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741529
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr3;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr3;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741557
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741558
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741575
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->g()LX/6jy;

    move-result-object v0

    .line 2741576
    iget-object v1, v0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 2741577
    :goto_0
    iget-object v1, v0, LX/6jy;->messageIds:Ljava/util/List;

    iget-wide v2, p2, LX/7GJ;->b:J

    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-direct {p0, v0}, LX/Jr3;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v0, v4}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/Jr3;->a(Ljava/util/List;JLcom/facebook/messaging/model/threadkey/ThreadKey;Z)Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    move-result-object v0

    .line 2741578
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2741579
    const-string v2, "deleteMessagesResult"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2741580
    iget-object v2, p0, LX/Jr3;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2My;

    invoke-virtual {v2}, LX/2My;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v2, :cond_2

    .line 2741581
    :cond_0
    :goto_1
    move-object v0, v1

    .line 2741582
    return-object v0

    .line 2741583
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 2741584
    :cond_2
    iget-object v2, p0, LX/Jr3;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2N4;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/2N4;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    .line 2741585
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v3, :cond_0

    .line 2741586
    const-string v3, "updatedInboxThreadForMontage"

    iget-object v2, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;JLcom/facebook/messaging/model/threadkey/ThreadKey;Z)Lcom/facebook/messaging/service/model/DeleteMessagesResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;J",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Z)",
            "Lcom/facebook/messaging/service/model/DeleteMessagesResult;"
        }
    .end annotation

    .prologue
    .line 2741555
    new-instance v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/6hi;->CLIENT_ONLY:LX/6hi;

    invoke-direct {v0, v1, v2, p4}, Lcom/facebook/messaging/service/model/DeleteMessagesParams;-><init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2741556
    iget-object v1, p0, LX/Jr3;->a:LX/FDs;

    invoke-virtual {v1, v0, p2, p3, p5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/DeleteMessagesParams;JZ)Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741545
    const-string v0, "deleteMessagesResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    .line 2741546
    if-eqz v0, :cond_0

    .line 2741547
    invoke-virtual {p0, v0}, LX/Jr3;->a(Lcom/facebook/messaging/service/model/DeleteMessagesResult;)V

    .line 2741548
    const-string v1, "updatedInboxThreadForMontage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741549
    if-nez v1, :cond_1

    .line 2741550
    :cond_0
    :goto_0
    return-void

    .line 2741551
    :cond_1
    iget-object v2, p0, LX/Jr3;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    iget-object v3, p0, LX/Jr3;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    invoke-virtual {v2, v1, v3, v4}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741552
    iget-object v2, p0, LX/Jr3;->b:LX/Jqb;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741553
    invoke-static {v2, v1}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741554
    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/DeleteMessagesResult;)V
    .locals 4

    .prologue
    .line 2741531
    iget-object v0, p0, LX/Jr3;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v0, v1, p1}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/service/model/DeleteMessagesResult;)V

    .line 2741532
    iget-object v0, p0, LX/Jr3;->b:LX/Jqb;

    .line 2741533
    iget-object v1, p1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741534
    if-nez v1, :cond_0

    .line 2741535
    :goto_0
    iget-object v0, p0, LX/Jr3;->d:LX/FO4;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->e:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FO4;->a(LX/0Py;)V

    .line 2741536
    return-void

    .line 2741537
    :cond_0
    iget-object v2, v0, LX/Jqb;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Jqa;

    .line 2741538
    if-nez v2, :cond_1

    .line 2741539
    new-instance v2, LX/Jqa;

    invoke-direct {v2, v1}, LX/Jqa;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2741540
    iget-object v3, v0, LX/Jqb;->f:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2741541
    :cond_1
    move-object v1, v2

    .line 2741542
    iget-object v2, v1, LX/Jqa;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->d:LX/0Rf;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2741543
    iget-object v2, v1, LX/Jqa;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->e:LX/0P1;

    invoke-virtual {v3}, LX/0P1;->values()LX/0Py;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2741544
    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741530
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/Jr3;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
