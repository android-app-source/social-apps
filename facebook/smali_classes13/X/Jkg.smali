.class public LX/Jkg;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U",
        "<",
        "Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field public e:LX/Jkj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/Jke;

.field private final g:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730214
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2730215
    sput-object v0, LX/Jkg;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2730205
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2730206
    sget-object v0, LX/Jkg;->a:LX/0Px;

    iput-object v0, p0, LX/Jkg;->c:LX/0Px;

    .line 2730207
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Jkg;->d:Z

    .line 2730208
    new-instance v0, LX/Jke;

    invoke-direct {v0, p0}, LX/Jke;-><init>(LX/Jkg;)V

    iput-object v0, p0, LX/Jkg;->f:LX/Jke;

    .line 2730209
    new-instance v0, LX/Jkf;

    invoke-direct {v0, p0}, LX/Jkf;-><init>(LX/Jkg;)V

    iput-object v0, p0, LX/Jkg;->g:Landroid/view/View$OnClickListener;

    .line 2730210
    iput-object p1, p0, LX/Jkg;->b:Landroid/content/Context;

    .line 2730211
    iput-boolean p2, p0, LX/Jkg;->d:Z

    .line 2730212
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2730213
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2730193
    iget-object v0, p0, LX/Jkg;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2730194
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2730195
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2730200
    const/4 v3, 0x0

    .line 2730201
    iget-object v0, p0, LX/Jkg;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308e0

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;

    .line 2730202
    iget-object v1, p0, LX/Jkg;->b:Landroid/content/Context;

    const v2, 0x101030e

    invoke-static {v1, v2, v3}, LX/0WH;->e(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2730203
    iget-object v1, p0, LX/Jkg;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2730204
    new-instance v1, LX/62U;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2730197
    check-cast p1, LX/62U;

    .line 2730198
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;

    iget-object v1, p0, LX/Jkg;->c:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    iget-object v2, p0, LX/Jkg;->f:LX/Jke;

    const/4 v3, 0x1

    iget-boolean v4, p0, LX/Jkg;->d:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a(Lcom/facebook/user/model/User;LX/Jke;ZZ)V

    .line 2730199
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2730196
    iget-object v0, p0, LX/Jkg;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
