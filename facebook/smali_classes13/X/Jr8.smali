.class public LX/Jr8;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kU;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jol;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742007
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr8;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2741999
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2742000
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742001
    iput-object v0, p0, LX/Jr8;->a:LX/0Ot;

    .line 2742002
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742003
    iput-object v0, p0, LX/Jr8;->b:LX/0Ot;

    .line 2742004
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742005
    iput-object v0, p0, LX/Jr8;->c:LX/0Ot;

    .line 2742006
    return-void
.end method

.method public static a(LX/0QB;)LX/Jr8;
    .locals 9

    .prologue
    .line 2741968
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741969
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741970
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741971
    if-nez v1, :cond_0

    .line 2741972
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741973
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741974
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741975
    sget-object v1, LX/Jr8;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741976
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741977
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741978
    :cond_1
    if-nez v1, :cond_4

    .line 2741979
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741980
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741981
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741982
    new-instance v1, LX/Jr8;

    invoke-direct {v1}, LX/Jr8;-><init>()V

    .line 2741983
    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2ba

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 p0, 0x284a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2741984
    iput-object v7, v1, LX/Jr8;->a:LX/0Ot;

    iput-object v8, v1, LX/Jr8;->b:LX/0Ot;

    iput-object p0, v1, LX/Jr8;->c:LX/0Ot;

    .line 2741985
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741986
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741987
    if-nez v1, :cond_2

    .line 2741988
    sget-object v0, LX/Jr8;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr8;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741989
    :goto_1
    if-eqz v0, :cond_3

    .line 2741990
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741991
    :goto_3
    check-cast v0, LX/Jr8;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741992
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741993
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741994
    :catchall_1
    move-exception v0

    .line 2741995
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741996
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741997
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741998
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr8;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr8;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741953
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741954
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741958
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kU;

    invoke-virtual {v0}, LX/6kU;->j()LX/6k8;

    move-result-object v0

    .line 2741959
    iget-object v1, v0, LX/6k8;->serialized_directives:Ljava/lang/String;

    .line 2741960
    const/4 v2, 0x0

    .line 2741961
    if-eqz v1, :cond_1

    .line 2741962
    :try_start_0
    iget-object v0, p0, LX/Jr8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 2741963
    :goto_0
    if-eqz v1, :cond_0

    .line 2741964
    iget-object v0, p0, LX/Jr8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jol;

    invoke-virtual {v0, v1}, LX/Jol;->a(LX/0lF;)V

    .line 2741965
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0

    .line 2741966
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2741967
    iget-object v0, p0, LX/Jr8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "DeltaOmniMDirectivesHandler"

    const-string v4, "Error parsing directives"

    invoke-virtual {v0, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741957
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741955
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741956
    return-object v0
.end method
