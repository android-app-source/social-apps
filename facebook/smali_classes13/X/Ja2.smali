.class public final LX/Ja2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/84H;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic b:LX/1Pr;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Ja3;


# direct methods
.method public constructor <init>(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2709473
    iput-object p1, p0, LX/Ja2;->d:LX/Ja3;

    iput-object p2, p0, LX/Ja2;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p3, p0, LX/Ja2;->b:LX/1Pr;

    iput-object p4, p0, LX/Ja2;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2709474
    iget-object v0, p0, LX/Ja2;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v0}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 2709475
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ja2;->d:LX/Ja3;

    iget-object v1, v1, LX/Ja3;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2709476
    iget-object v1, p0, LX/Ja2;->d:LX/Ja3;

    iget-object v1, v1, LX/Ja3;->d:LX/1rj;

    iget-object v2, p0, LX/Ja2;->d:LX/Ja3;

    iget-object v2, v2, LX/Ja3;->f:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    sget-object v3, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2709477
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 2709478
    iget-object v1, p0, LX/Ja2;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p0, LX/Ja2;->b:LX/1Pr;

    iget-object v3, p0, LX/Ja2;->c:Ljava/lang/String;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->PENDING:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2709479
    invoke-static {v1, v2, v3, v4}, LX/Ja3;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    .line 2709480
    return-void
.end method
