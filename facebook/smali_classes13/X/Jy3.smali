.class public LX/Jy3;
.super LX/63W;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5vm;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public volatile h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/Jy8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final k:LX/1De;

.field private final l:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final m:LX/Jxq;

.field public n:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1De;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;LX/Jxq;)V
    .locals 0
    .param p1    # LX/1De;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Jxq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2754114
    invoke-direct {p0}, LX/63W;-><init>()V

    .line 2754115
    iput-object p1, p0, LX/Jy3;->k:LX/1De;

    .line 2754116
    iput-object p2, p0, LX/Jy3;->l:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2754117
    iput-object p3, p0, LX/Jy3;->m:LX/Jxq;

    .line 2754118
    return-void
.end method

.method public static a(LX/Jy3;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/Jy8;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jy3;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Or",
            "<",
            "LX/5vm;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;",
            ">;",
            "LX/Jy8;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2754113
    iput-object p1, p0, LX/Jy3;->a:LX/0Or;

    iput-object p2, p0, LX/Jy3;->b:LX/0Or;

    iput-object p3, p0, LX/Jy3;->c:LX/0Or;

    iput-object p4, p0, LX/Jy3;->d:LX/0Or;

    iput-object p5, p0, LX/Jy3;->e:LX/0Or;

    iput-object p6, p0, LX/Jy3;->f:LX/0Or;

    iput-object p7, p0, LX/Jy3;->g:LX/0Or;

    iput-object p8, p0, LX/Jy3;->h:LX/0Or;

    iput-object p9, p0, LX/Jy3;->i:LX/Jy8;

    iput-object p10, p0, LX/Jy3;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method public static a$redex0(LX/Jy3;LX/0Px;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
            ">;",
            "Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 2754067
    const v0, 0x7f0d0ccc

    invoke-virtual {p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2754068
    const v1, 0x7f0d0ccb

    invoke-virtual {p2, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2754069
    if-nez p3, :cond_6

    .line 2754070
    invoke-virtual {p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2754071
    iput-boolean v6, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2754072
    iget-object v2, p0, LX/Jy3;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 2754073
    new-instance v3, LX/1P0;

    invoke-direct {v3, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2754074
    iget-object v2, p0, LX/Jy3;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    .line 2754075
    new-instance v3, LX/62X;

    const v4, 0x7f0a009f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const v5, 0x7f0b26d0

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v3, v4, v2}, LX/62X;-><init>(II)V

    .line 2754076
    iput-boolean v6, v3, LX/62X;->e:Z

    .line 2754077
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2754078
    new-instance v2, LX/Jy1;

    invoke-direct {v2, p0, p2}, LX/Jy1;-><init>(LX/Jy3;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2754079
    :goto_0
    iget-object v1, p0, LX/Jy3;->i:LX/Jy8;

    iget-object v2, p0, LX/Jy3;->k:LX/1De;

    iget-object v3, p0, LX/Jy3;->m:LX/Jxq;

    .line 2754080
    new-instance p3, LX/Jy7;

    invoke-direct {p3, p1, v2, v3, p0}, LX/Jy7;-><init>(LX/0Px;LX/1De;LX/Jxq;LX/Jy3;)V

    .line 2754081
    invoke-static {v1}, LX/JyD;->a(LX/0QB;)LX/JyD;

    move-result-object v4

    check-cast v4, LX/JyD;

    invoke-static {v1}, LX/JyG;->a(LX/0QB;)LX/JyG;

    move-result-object v5

    check-cast v5, LX/JyG;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p2

    check-cast p2, Landroid/content/res/Resources;

    .line 2754082
    iput-object v4, p3, LX/Jy7;->a:LX/JyD;

    iput-object v5, p3, LX/Jy7;->b:LX/JyG;

    iput-object v6, p3, LX/Jy7;->c:Landroid/view/LayoutInflater;

    iput-object p2, p3, LX/Jy7;->d:Landroid/content/res/Resources;

    .line 2754083
    move-object v1, p3

    .line 2754084
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2754085
    iget-object v0, p0, LX/Jy3;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xb30001

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2754086
    iget-object v0, p0, LX/Jy3;->m:LX/Jxq;

    .line 2754087
    iget-object v1, v0, LX/Jxq;->b:LX/0Zb;

    const-string v2, "profile_discovery_event"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2754088
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2754089
    const-string v2, "bucket_list_impression"

    invoke-static {v0, v1, v2}, LX/Jxq;->c(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2754090
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2754091
    :cond_0
    iget-object v0, p0, LX/Jy3;->m:LX/Jxq;

    const/4 v8, 0x0

    .line 2754092
    move v7, v8

    move v5, v8

    .line 2754093
    :goto_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-ge v7, v1, :cond_5

    .line 2754094
    invoke-virtual {p1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    .line 2754095
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    if-ne v2, v3, :cond_2

    const-string v3, "Highlighted"

    .line 2754096
    :goto_2
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;->a()LX/0Px;

    move-result-object v9

    move v4, v8

    .line 2754097
    :goto_3
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_4

    .line 2754098
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v7, v1, :cond_3

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v4, v1, :cond_3

    const/4 v6, 0x1

    .line 2754099
    :goto_4
    invoke-virtual {v9, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    move-object v1, v0

    .line 2754100
    iget-object p0, v1, LX/Jxq;->b:LX/0Zb;

    const-string p2, "profile_discovery_event"

    const/4 p3, 0x0

    invoke-interface {p0, p2, p3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2754101
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 2754102
    const-string p2, "bucket_list_candidate_impression"

    invoke-static {v1, p0, p2}, LX/Jxq;->c(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2754103
    invoke-static {p0, v3, v4}, LX/Jxq;->a(LX/0oG;Ljava/lang/String;I)V

    .line 2754104
    invoke-static {p0, v2, v3, v5, v6}, LX/Jxq;->a(LX/0oG;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IZ)V

    .line 2754105
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 2754106
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 2754107
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2754108
    :cond_2
    const-string v3, "Normal"

    goto :goto_2

    :cond_3
    move v6, v8

    .line 2754109
    goto :goto_4

    .line 2754110
    :cond_4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    .line 2754111
    :cond_5
    return-void

    .line 2754112
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/Jy3;Ljava/lang/Throwable;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V
    .locals 3

    .prologue
    .line 2753990
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 2753991
    iget-object v0, p0, LX/Jy3;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 2753992
    const-string v1, "discovery_dashboard_load_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2753993
    :cond_0
    if-eqz p3, :cond_1

    .line 2753994
    const v0, 0x7f0d0ccb

    invoke-virtual {p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2753995
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2753996
    :cond_1
    iget-object v0, p0, LX/Jy3;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 2753997
    invoke-static {}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->newBuilder()LX/62Q;

    move-result-object v1

    sget-object v2, LX/1lD;->ERROR:LX/1lD;

    .line 2753998
    iput-object v2, v1, LX/62Q;->a:LX/1lD;

    .line 2753999
    move-object v1, v1

    .line 2754000
    const v2, 0x7f080039

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2754001
    iput-object v0, v1, LX/62Q;->b:Ljava/lang/String;

    .line 2754002
    move-object v0, v1

    .line 2754003
    invoke-virtual {v0}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object v0

    .line 2754004
    new-instance v1, LX/Jy2;

    invoke-direct {v1, p0, p2, p3}, LX/Jy2;-><init>(LX/Jy3;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V

    invoke-virtual {p2, v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    .line 2754005
    iget-object v0, p0, LX/Jy3;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xb30001

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2754006
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2754063
    iget-object v0, p0, LX/Jy3;->n:LX/1Mv;

    if-eqz v0, :cond_0

    .line 2754064
    iget-object v0, p0, LX/Jy3;->n:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 2754065
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jy3;->n:LX/1Mv;

    .line 2754066
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2754050
    iget v0, p2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    move v0, v0

    .line 2754051
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2754052
    iget-object v0, p0, LX/Jy3;->m:LX/Jxq;

    .line 2754053
    iget-object v1, v0, LX/Jxq;->b:LX/0Zb;

    const-string v2, "profile_discovery_event"

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2754054
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2754055
    const-string v2, "profile_discovery_cog_click"

    invoke-static {v0, v1, v2}, LX/Jxq;->c(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2754056
    const-string v2, "selected_type"

    const-string p1, "self_profile"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2754057
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2754058
    :cond_0
    iget-object v0, p0, LX/Jy3;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 2754059
    iget-object v1, p0, LX/Jy3;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5vm;

    .line 2754060
    iget-object v2, p0, LX/Jy3;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 2754061
    invoke-virtual {v1}, LX/5vm;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2754062
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IIZI)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/profile/discovery/DiscoveryCurationAnalyticsLogger$SectionType;
        .end annotation
    .end param

    .prologue
    .line 2754027
    iget-object v0, p0, LX/Jy3;->m:LX/Jxq;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    .line 2754028
    iget-object v7, v0, LX/Jxq;->b:LX/0Zb;

    const-string p3, "profile_discovery_event"

    const/4 p6, 0x0

    invoke-interface {v7, p3, p6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 2754029
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 2754030
    const-string p3, "bucket_list_candidate_click"

    invoke-static {v0, v7, p3}, LX/Jxq;->c(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2754031
    invoke-static {v7, v2, v3}, LX/Jxq;->a(LX/0oG;Ljava/lang/String;I)V

    .line 2754032
    invoke-static {v7, v1, v2, v4, v5}, LX/Jxq;->a(LX/0oG;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IZ)V

    .line 2754033
    const-string p3, "selected_position"

    invoke-virtual {v7, p3, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2754034
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 2754035
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2754036
    const-string v0, "discovery_curation_logging_data"

    iget-object v1, p0, LX/Jy3;->l:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2754037
    const-string v0, "discovery_section_type"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2754038
    const-string v0, "candidate_position"

    invoke-virtual {v3, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2754039
    const-string v0, "candidate_is_last"

    invoke-virtual {v3, v0, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2754040
    iget-object v0, p0, LX/Jy3;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 2754041
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2754042
    iget-object v0, p0, LX/Jy3;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    .line 2754043
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->j()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-class v6, Landroid/app/Activity;

    invoke-static {v1, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    invoke-virtual/range {v0 .. v6}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;ILandroid/app/Activity;)Z

    .line 2754044
    :goto_0
    return-void

    .line 2754045
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;

    invoke-direct {v2, v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2754046
    const-string v0, "discovery_bucket"

    invoke-static {v2, v0, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2754047
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2754048
    iget-object v0, p0, LX/Jy3;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 2754049
    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V
    .locals 7

    .prologue
    .line 2754007
    if-nez p2, :cond_0

    .line 2754008
    invoke-virtual {p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2754009
    :cond_0
    iget-object v0, p0, LX/Jy3;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    .line 2754010
    new-instance v3, LX/FTe;

    invoke-direct {v3}, LX/FTe;-><init>()V

    move-object v3, v3

    .line 2754011
    const-string v4, "image_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2754012
    const-string v4, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2754013
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, v4}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    sget-object v4, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2754014
    iput-object v4, v3, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2754015
    move-object v4, v3

    .line 2754016
    if-eqz p2, :cond_1

    sget-object v3, LX/0zS;->d:LX/0zS;

    :goto_0
    invoke-virtual {v4, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/32 v5, 0x15180

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2754017
    iget-object v4, v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2754018
    new-instance v4, LX/Jxy;

    invoke-direct {v4, v0}, LX/Jxy;-><init>(Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;)V

    .line 2754019
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 2754020
    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 2754021
    new-instance v2, LX/Jy0;

    invoke-direct {v2, p0, p1, p2}, LX/Jy0;-><init>(LX/Jy3;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V

    .line 2754022
    new-instance v0, LX/1Mv;

    invoke-direct {v0, v1, v2}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v0, p0, LX/Jy3;->n:LX/1Mv;

    .line 2754023
    iget-object v0, p0, LX/Jy3;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 2754024
    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2754025
    return-void

    .line 2754026
    :cond_1
    sget-object v3, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method
