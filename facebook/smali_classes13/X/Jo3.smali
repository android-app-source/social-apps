.class public LX/Jo3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Jnw;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/DhA;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/DhG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2734873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734874
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Jo3;->a:Ljava/util/Map;

    .line 2734875
    return-void
.end method

.method private static c(Lcom/facebook/messaging/montage/model/art/EffectItem;)V
    .locals 2

    .prologue
    .line 2734876
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    move-object v0, v0

    .line 2734877
    if-nez v0, :cond_0

    .line 2734878
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The effect is not a style transfer."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734879
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;
    .locals 2

    .prologue
    .line 2734880
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/Jo3;->c(Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734881
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2734882
    iget-object v1, p0, LX/Jo3;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2734883
    iget-object v1, p0, LX/Jo3;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jnw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734884
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2734885
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Jo3;->b:LX/DhA;

    .line 2734886
    iget-object v1, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    move-object v1, v1

    .line 2734887
    invoke-static {v1}, LX/DhA;->a(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/DhA;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-static {v1}, LX/DhA;->b(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/DhA;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    :goto_1
    move v0, p1

    .line 2734888
    if-eqz v0, :cond_1

    .line 2734889
    sget-object v0, LX/Jnw;->COMPLETED:LX/Jnw;

    goto :goto_0

    .line 2734890
    :cond_1
    sget-object v0, LX/Jnw;->NOT_STARTED:LX/Jnw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2734891
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/montage/model/art/EffectItem;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/montage/model/art/EffectItem;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Dh8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2734892
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/Jo3;->c(Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734893
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2734894
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 2734895
    iget-object v2, p0, LX/Jo3;->a:Ljava/util/Map;

    sget-object v3, LX/Jnw;->IN_PROGRESS:LX/Jnw;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734896
    iget-object v2, p0, LX/Jo3;->c:LX/DhG;

    new-instance v3, LX/Jo2;

    invoke-direct {v3, p0, v0, v1}, LX/Jo2;-><init>(LX/Jo3;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(LX/3Mb;)V

    .line 2734897
    iget-object v0, p0, LX/Jo3;->c:LX/DhG;

    .line 2734898
    iget-object v2, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    move-object v2, v2

    .line 2734899
    invoke-virtual {v0, v2}, LX/6Lb;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734900
    monitor-exit p0

    return-object v1

    .line 2734901
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
