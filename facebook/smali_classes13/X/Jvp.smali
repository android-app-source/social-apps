.class public final LX/Jvp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750828
    iput-object p1, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1efb736

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750829
    iget-object v1, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->T:LX/89c;

    iget-object v2, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v2}, LX/89e;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget v3, v3, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->W:I

    invoke-interface {v1, v2, v3}, LX/89c;->a(Ljava/lang/String;I)Z

    move-result v1

    .line 2750830
    if-eqz v1, :cond_0

    .line 2750831
    const v1, 0x4c097401    # 3.6032516E7f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2750832
    :goto_0
    return-void

    .line 2750833
    :cond_0
    iget-object v1, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v2, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    .line 2750834
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/9c4;->CREATIVECAM_OPEN_SIMPLE_PICKER:LX/9c4;

    invoke-virtual {v4}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {v4}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "composer"

    .line 2750835
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2750836
    move-object v3, v3

    .line 2750837
    invoke-static {v1, v3}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2750838
    iget-object v1, p0, LX/Jvp;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v1}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->B(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750839
    const v1, -0x4c4e4750

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
