.class public LX/Jyh;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field public b:Landroid/support/v7/widget/RecyclerView;

.field public c:Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

.field private d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

.field public e:Landroid/view/View$OnClickListener;

.field public f:Z

.field public g:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2755334
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Jyh;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2755335
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2755332
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Jyh;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755333
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2755325
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755326
    invoke-virtual {p0}, LX/Jyh;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03105e

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2755327
    const v0, 0x7f0d2739

    invoke-virtual {p0, v0}, LX/Jyh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Jyh;->a:Landroid/widget/LinearLayout;

    .line 2755328
    const v0, 0x7f0d273b

    invoke-virtual {p0, v0}, LX/Jyh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/Jyh;->b:Landroid/support/v7/widget/RecyclerView;

    .line 2755329
    const v0, 0x7f0d273a

    invoke-virtual {p0, v0}, LX/Jyh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    iput-object v0, p0, LX/Jyh;->c:Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    .line 2755330
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/Jyh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/Jyh;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2755331
    return-void
.end method

.method public static a(LX/Jyh;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2755314
    iget-boolean v0, p0, LX/Jyh;->f:Z

    if-eqz v0, :cond_1

    .line 2755315
    iget-object v0, p0, LX/Jyh;->d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    if-nez v0, :cond_0

    .line 2755316
    const v0, 0x7f0d273c

    invoke-virtual {p0, v0}, LX/Jyh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2755317
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    iput-object v0, p0, LX/Jyh;->d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    .line 2755318
    iget-object v0, p0, LX/Jyh;->d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    iget-object v1, p0, LX/Jyh;->g:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    iget-object v2, p0, LX/Jyh;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->a(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;Landroid/view/View$OnClickListener;)V

    .line 2755319
    :cond_0
    iget-object v0, p0, LX/Jyh;->d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    invoke-virtual {v0, v3}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->setVisibility(I)V

    .line 2755320
    iget-object v0, p0, LX/Jyh;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2755321
    :goto_0
    return-void

    .line 2755322
    :cond_1
    iget-object v0, p0, LX/Jyh;->d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    if-eqz v0, :cond_2

    .line 2755323
    iget-object v0, p0, LX/Jyh;->d:Lcom/facebook/profile/insight/view/ProfileInsightInfoView;

    invoke-virtual {v0, v4}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->setVisibility(I)V

    .line 2755324
    :cond_2
    iget-object v0, p0, LX/Jyh;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
