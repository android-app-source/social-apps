.class public LX/JoE;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/messaging/montage/widget/tile/MontageTileController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2735271
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2735272
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)Lcom/facebook/messaging/montage/widget/tile/MontageTileController;
    .locals 14

    .prologue
    .line 2735273
    new-instance v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;-><init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 2735274
    invoke-static {p0}, LX/2OS;->a(LX/0QB;)LX/2OS;

    move-result-object v1

    check-cast v1, LX/2OS;

    const/16 v2, 0x509

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x358c

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 v5, 0x12cd

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xd4e

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v7

    check-cast v7, LX/2Ly;

    invoke-static {p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v8

    check-cast v8, LX/2My;

    invoke-static {p0}, LX/2OR;->b(LX/0QB;)LX/2OR;

    move-result-object v9

    check-cast v9, LX/2OR;

    .line 2735275
    new-instance v12, LX/JoH;

    invoke-direct {v12}, LX/JoH;-><init>()V

    .line 2735276
    invoke-static {p0}, LX/IiT;->a(LX/0QB;)LX/IiT;

    move-result-object v10

    check-cast v10, LX/IiT;

    invoke-static {p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v11

    check-cast v11, LX/2My;

    .line 2735277
    iput-object v10, v12, LX/JoH;->a:LX/IiT;

    iput-object v11, v12, LX/JoH;->b:LX/2My;

    .line 2735278
    move-object v10, v12

    .line 2735279
    check-cast v10, LX/JoH;

    const/16 v11, 0x1204

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x359a

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1430

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    .line 2735280
    iput-object v1, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->e:LX/2OS;

    iput-object v2, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->f:LX/0Or;

    iput-object v3, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->g:LX/0Ot;

    iput-object v4, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->h:LX/0Xl;

    iput-object v5, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->i:LX/0Or;

    iput-object v6, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j:LX/0Ot;

    iput-object v7, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->k:LX/2Ly;

    iput-object v8, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->l:LX/2My;

    iput-object v9, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->m:LX/2OR;

    iput-object v10, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->n:LX/JoH;

    iput-object v11, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->o:LX/0Ot;

    iput-object v12, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->p:LX/0Ot;

    iput-object v13, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->q:LX/0Ot;

    .line 2735281
    return-object v0
.end method
