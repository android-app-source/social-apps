.class public LX/K4I;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2767815
    const-class v0, LX/K4I;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K4I;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2767816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Cutdown$Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2767817
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2767818
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v2

    .line 2767819
    :try_start_0
    invoke-virtual {v2, p0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2767820
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2767821
    const-class v4, Lcom/facebook/storyline/model/Cutdown$Section;

    invoke-virtual {v2, v0, v4}, LX/0lC;->a(LX/0lG;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2767822
    :catch_0
    move-exception v0

    .line 2767823
    sget-object v2, LX/K4I;->a:Ljava/lang/String;

    const-string v3, "Failed to parse timing data %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2767824
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Px;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Cutdown;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2767825
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2767826
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_0

    invoke-virtual {p0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;

    .line 2767827
    new-instance v0, Lcom/facebook/storyline/model/Cutdown;

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->l()I

    move-result v2

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->o()I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->n()I

    move-result v6

    invoke-virtual {v7}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/K4I;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/storyline/model/Cutdown;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IILX/0Px;)V

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2767828
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 2767829
    :cond_0
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
