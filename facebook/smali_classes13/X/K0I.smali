.class public LX/K0I;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K0I;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:F


# direct methods
.method public constructor <init>(IF)V
    .locals 0

    .prologue
    .line 2758204
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2758205
    iput p2, p0, LX/K0I;->a:F

    .line 2758206
    return-void
.end method

.method private k()LX/5pH;
    .locals 4

    .prologue
    .line 2758199
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2758200
    const-string v1, "offset"

    .line 2758201
    iget v2, p0, LX/K0I;->a:F

    move v2, v2

    .line 2758202
    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2758203
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2758196
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2758197
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K0I;->k()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2758198
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2758194
    const-string v0, "topDrawerSlide"

    return-object v0
.end method

.method public final f()S
    .locals 1

    .prologue
    .line 2758195
    const/4 v0, 0x0

    return v0
.end method
