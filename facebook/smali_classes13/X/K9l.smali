.class public LX/K9l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67g;


# instance fields
.field public a:Ljava/lang/String;

.field public b:D

.field public c:D

.field public d:F

.field public e:F

.field public f:F

.field public g:D

.field public h:D

.field public i:Landroid/net/Uri;

.field public j:Landroid/net/Uri;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/graphql/model/GraphQLStory;

.field public q:LX/K9k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2777833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2777834
    return-void
.end method


# virtual methods
.method public final a([D)V
    .locals 4

    .prologue
    .line 2777830
    const/4 v0, 0x0

    iget-wide v2, p0, LX/K9l;->b:D

    aput-wide v2, p1, v0

    .line 2777831
    const/4 v0, 0x1

    iget-wide v2, p0, LX/K9l;->c:D

    aput-wide v2, p1, v0

    .line 2777832
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2777835
    if-ne p0, p1, :cond_0

    .line 2777836
    const/4 v0, 0x1

    .line 2777837
    :goto_0
    return v0

    .line 2777838
    :cond_0
    instance-of v0, p1, LX/K9l;

    if-nez v0, :cond_1

    .line 2777839
    const/4 v0, 0x0

    goto :goto_0

    .line 2777840
    :cond_1
    check-cast p1, LX/K9l;

    .line 2777841
    iget-object v0, p0, LX/K9l;->a:Ljava/lang/String;

    iget-object v1, p1, LX/K9l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2777829
    iget-object v0, p0, LX/K9l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
