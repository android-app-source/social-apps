.class public LX/JqN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field private final a:LX/0WJ;

.field private final b:LX/2ac;

.field private final c:LX/3gD;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jsq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0WJ;LX/2ac;LX/3gD;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0WJ;",
            "LX/2ac;",
            "LX/3gD;",
            "LX/0Ot",
            "<",
            "LX/Jsq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738843
    iput-object p1, p0, LX/JqN;->a:LX/0WJ;

    .line 2738844
    iput-object p2, p0, LX/JqN;->b:LX/2ac;

    .line 2738845
    iput-object p3, p0, LX/JqN;->c:LX/3gD;

    .line 2738846
    iput-object p4, p0, LX/JqN;->d:LX/0Ot;

    .line 2738847
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2738865
    iget-object v0, p0, LX/JqN;->b:LX/2ac;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "user"

    .line 2738866
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2738867
    move-object v0, v0

    .line 2738868
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    iget-object v1, p0, LX/JqN;->c:LX/3gD;

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v2, "fetchFacebookEmployeeStatus"

    .line 2738869
    iput-object v2, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 2738870
    move-object v1, v1

    .line 2738871
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2738848
    const-string v0, "user"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;

    .line 2738849
    const-string v1, "fetchFacebookEmployeeStatus"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 2738850
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 2738851
    iget-object v3, v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;->a:Lcom/facebook/user/model/User;

    move-object v0, v3

    .line 2738852
    invoke-virtual {v2, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    .line 2738853
    if-eqz v1, :cond_0

    .line 2738854
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2738855
    iput-boolean v0, v2, LX/0XI;->v:Z

    .line 2738856
    :cond_0
    iget-boolean v0, v2, LX/0XI;->af:Z

    move v0, v0

    .line 2738857
    if-eqz v0, :cond_1

    .line 2738858
    iget-object v0, p0, LX/JqN;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jsq;

    .line 2738859
    iget-boolean v1, v2, LX/0XI;->af:Z

    move v1, v1

    .line 2738860
    iget-object v3, v2, LX/0XI;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2738861
    iget-object v4, v2, LX/0XI;->aj:Lcom/facebook/user/model/User;

    move-object v4, v4

    .line 2738862
    invoke-virtual {v0, v1, v3, v4}, LX/Jsq;->a(ZLjava/lang/String;Lcom/facebook/user/model/User;)V

    .line 2738863
    :cond_1
    iget-object v0, p0, LX/JqN;->a:LX/0WJ;

    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 2738864
    return-void
.end method
