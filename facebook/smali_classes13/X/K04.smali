.class public final LX/K04;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/Callback;


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/5pH;

.field public final synthetic c:LX/5pW;

.field public final synthetic d:LX/K05;


# direct methods
.method public constructor <init>(LX/K05;Ljava/util/ArrayList;LX/5pH;LX/5pW;)V
    .locals 0

    .prologue
    .line 2757220
    iput-object p1, p0, LX/K04;->d:LX/K05;

    iput-object p2, p0, LX/K04;->a:Ljava/util/ArrayList;

    iput-object p3, p0, LX/K04;->b:LX/5pH;

    iput-object p4, p0, LX/K04;->c:LX/5pW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2757221
    aget-object v0, p1, v2

    check-cast v0, [I

    check-cast v0, [I

    .line 2757222
    const/4 v1, 0x1

    aget-object v1, p1, v1

    check-cast v1, LX/2lD;

    move v3, v2

    .line 2757223
    :goto_0
    iget-object v2, p0, LX/K04;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 2757224
    iget-object v2, p0, LX/K04;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2757225
    aget v4, v0, v3

    if-nez v4, :cond_0

    .line 2757226
    iget-object v4, p0, LX/K04;->b:LX/5pH;

    const-string v5, "granted"

    invoke-interface {v4, v2, v5}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757227
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2757228
    :cond_0
    invoke-interface {v1, v2}, LX/2lD;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2757229
    iget-object v4, p0, LX/K04;->b:LX/5pH;

    const-string v5, "denied"

    invoke-interface {v4, v2, v5}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2757230
    :cond_1
    iget-object v4, p0, LX/K04;->b:LX/5pH;

    const-string v5, "never_ask_again"

    invoke-interface {v4, v2, v5}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2757231
    :cond_2
    iget-object v0, p0, LX/K04;->c:LX/5pW;

    iget-object v1, p0, LX/K04;->b:LX/5pH;

    invoke-interface {v0, v1}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2757232
    return-void
.end method
