.class public LX/Jqc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GI;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7GI",
        "<",
        "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final H:Ljava/lang/Object;


# instance fields
.field private final A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqk;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jql;",
            ">;"
        }
    .end annotation
.end field

.field private final C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqy;",
            ">;"
        }
    .end annotation
.end field

.field private final D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqn;",
            ">;"
        }
    .end annotation
.end field

.field private final E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqo;",
            ">;"
        }
    .end annotation
.end field

.field private final F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrP;",
            ">;"
        }
    .end annotation
.end field

.field private final G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqt;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr7;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr6;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr1;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr2;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr3;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrI;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrB;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrA;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrO;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrK;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrN;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrH;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrJ;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrC;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrD;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqm;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrG;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqu;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqs;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr9;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrR;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrE;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr5;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqw;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739717
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqc;->H:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Jr7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jr6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jr1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jr2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jr3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jr9;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jr5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jql;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JrP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jqt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739788
    iput-object p1, p0, LX/Jqc;->a:LX/0Ot;

    .line 2739789
    iput-object p2, p0, LX/Jqc;->b:LX/0Ot;

    .line 2739790
    iput-object p3, p0, LX/Jqc;->c:LX/0Ot;

    .line 2739791
    iput-object p4, p0, LX/Jqc;->d:LX/0Ot;

    .line 2739792
    iput-object p5, p0, LX/Jqc;->e:LX/0Ot;

    .line 2739793
    iput-object p6, p0, LX/Jqc;->f:LX/0Ot;

    .line 2739794
    iput-object p9, p0, LX/Jqc;->i:LX/0Ot;

    .line 2739795
    iput-object p10, p0, LX/Jqc;->j:LX/0Ot;

    .line 2739796
    iput-object p11, p0, LX/Jqc;->k:LX/0Ot;

    .line 2739797
    iput-object p7, p0, LX/Jqc;->g:LX/0Ot;

    .line 2739798
    iput-object p8, p0, LX/Jqc;->h:LX/0Ot;

    .line 2739799
    iput-object p12, p0, LX/Jqc;->l:LX/0Ot;

    .line 2739800
    iput-object p13, p0, LX/Jqc;->m:LX/0Ot;

    .line 2739801
    iput-object p14, p0, LX/Jqc;->n:LX/0Ot;

    .line 2739802
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Jqc;->o:LX/0Ot;

    .line 2739803
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Jqc;->p:LX/0Ot;

    .line 2739804
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Jqc;->q:LX/0Ot;

    .line 2739805
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Jqc;->r:LX/0Ot;

    .line 2739806
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Jqc;->s:LX/0Ot;

    .line 2739807
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Jqc;->t:LX/0Ot;

    .line 2739808
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Jqc;->u:LX/0Ot;

    .line 2739809
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Jqc;->v:LX/0Ot;

    .line 2739810
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Jqc;->w:LX/0Ot;

    .line 2739811
    move-object/from16 v0, p24

    iput-object v0, p0, LX/Jqc;->x:LX/0Ot;

    .line 2739812
    move-object/from16 v0, p25

    iput-object v0, p0, LX/Jqc;->y:LX/0Ot;

    .line 2739813
    move-object/from16 v0, p26

    iput-object v0, p0, LX/Jqc;->z:LX/0Ot;

    .line 2739814
    move-object/from16 v0, p27

    iput-object v0, p0, LX/Jqc;->A:LX/0Ot;

    .line 2739815
    move-object/from16 v0, p28

    iput-object v0, p0, LX/Jqc;->B:LX/0Ot;

    .line 2739816
    move-object/from16 v0, p29

    iput-object v0, p0, LX/Jqc;->C:LX/0Ot;

    .line 2739817
    move-object/from16 v0, p30

    iput-object v0, p0, LX/Jqc;->D:LX/0Ot;

    .line 2739818
    move-object/from16 v0, p31

    iput-object v0, p0, LX/Jqc;->E:LX/0Ot;

    .line 2739819
    move-object/from16 v0, p32

    iput-object v0, p0, LX/Jqc;->F:LX/0Ot;

    .line 2739820
    move-object/from16 v0, p33

    iput-object v0, p0, LX/Jqc;->G:LX/0Ot;

    .line 2739821
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqc;
    .locals 7

    .prologue
    .line 2739760
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2739761
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2739762
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2739763
    if-nez v1, :cond_0

    .line 2739764
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739765
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2739766
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2739767
    sget-object v1, LX/Jqc;->H:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2739768
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2739769
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2739770
    :cond_1
    if-nez v1, :cond_4

    .line 2739771
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2739772
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2739773
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqc;->b(LX/0QB;)LX/Jqc;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2739774
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2739775
    if-nez v1, :cond_2

    .line 2739776
    sget-object v0, LX/Jqc;->H:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqc;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2739777
    :goto_1
    if-eqz v0, :cond_3

    .line 2739778
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739779
    :goto_3
    check-cast v0, LX/Jqc;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2739780
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2739781
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2739782
    :catchall_1
    move-exception v0

    .line 2739783
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739784
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2739785
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2739786
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqc;->H:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqc;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Jqc;
    .locals 36

    .prologue
    .line 2739822
    new-instance v2, LX/Jqc;

    const/16 v3, 0x29de

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x29dd

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x29d8

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x29d9

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x29da

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x29e9

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x29e2

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x29e1

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x29ef

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x29eb

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x29ee

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x29e8

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x29ea

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x29e3

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x29e4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x29cc

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x29e7

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x29d1    # 1.5001E-41f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x29cf

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x29e0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x29d2

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x29f2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x29e5

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x29dc

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const/16 v27, 0x29d4

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x29d3

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x29ca

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const/16 v30, 0x29cb

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const/16 v31, 0x29d6

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x29cd

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x29ce

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0x29f0

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v34

    const/16 v35, 0x29d0    # 1.5E-41f

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v35

    invoke-direct/range {v2 .. v35}, LX/Jqc;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2739823
    return-object v2
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)LX/7GG;
    .locals 1

    .prologue
    .line 2739759
    check-cast p1, LX/6kW;

    invoke-virtual {p0, p1}, LX/Jqc;->a(LX/6kW;)LX/Jqi;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6kW;)LX/Jqi;
    .locals 5

    .prologue
    .line 2739718
    iget v0, p1, LX/6kT;->setField_:I

    move v0, v0

    .line 2739719
    packed-switch v0, :pswitch_data_0

    .line 2739720
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported delta type: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2739721
    iget v4, p1, LX/6kT;->setField_:I

    move v4, v4

    .line 2739722
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739723
    :pswitch_1
    iget-object v0, p0, LX/Jqc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    .line 2739724
    :goto_0
    return-object v0

    .line 2739725
    :pswitch_2
    iget-object v0, p0, LX/Jqc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739726
    :pswitch_3
    iget-object v0, p0, LX/Jqc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739727
    :pswitch_4
    iget-object v0, p0, LX/Jqc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739728
    :pswitch_5
    iget-object v0, p0, LX/Jqc;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739729
    :pswitch_6
    iget-object v0, p0, LX/Jqc;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739730
    :pswitch_7
    iget-object v0, p0, LX/Jqc;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739731
    :pswitch_8
    iget-object v0, p0, LX/Jqc;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739732
    :pswitch_9
    iget-object v0, p0, LX/Jqc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739733
    :pswitch_a
    iget-object v0, p0, LX/Jqc;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739734
    :pswitch_b
    iget-object v0, p0, LX/Jqc;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739735
    :pswitch_c
    iget-object v0, p0, LX/Jqc;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739736
    :pswitch_d
    iget-object v0, p0, LX/Jqc;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739737
    :pswitch_e
    iget-object v0, p0, LX/Jqc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739738
    :pswitch_f
    iget-object v0, p0, LX/Jqc;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2739739
    :pswitch_10
    iget-object v0, p0, LX/Jqc;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739740
    :pswitch_11
    iget-object v0, p0, LX/Jqc;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739741
    :pswitch_12
    iget-object v0, p0, LX/Jqc;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739742
    :pswitch_13
    iget-object v0, p0, LX/Jqc;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739743
    :pswitch_14
    iget-object v0, p0, LX/Jqc;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739744
    :pswitch_15
    iget-object v0, p0, LX/Jqc;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739745
    :pswitch_16
    iget-object v0, p0, LX/Jqc;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739746
    :pswitch_17
    iget-object v0, p0, LX/Jqc;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739747
    :pswitch_18
    iget-object v0, p0, LX/Jqc;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739748
    :pswitch_19
    iget-object v0, p0, LX/Jqc;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739749
    :pswitch_1a
    iget-object v0, p0, LX/Jqc;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739750
    :pswitch_1b
    iget-object v0, p0, LX/Jqc;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739751
    :pswitch_1c
    iget-object v0, p0, LX/Jqc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739752
    :pswitch_1d
    iget-object v0, p0, LX/Jqc;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739753
    :pswitch_1e
    iget-object v0, p0, LX/Jqc;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739754
    :pswitch_1f
    iget-object v0, p0, LX/Jqc;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739755
    :pswitch_20
    iget-object v0, p0, LX/Jqc;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739756
    :pswitch_21
    iget-object v0, p0, LX/Jqc;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739757
    :pswitch_22
    iget-object v0, p0, LX/Jqc;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    .line 2739758
    :pswitch_23
    iget-object v0, p0, LX/Jqc;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_0
        :pswitch_12
        :pswitch_10
        :pswitch_11
        :pswitch_15
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_16
        :pswitch_17
        :pswitch_1c
        :pswitch_0
        :pswitch_19
        :pswitch_18
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_22
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_23
    .end packed-switch
.end method
