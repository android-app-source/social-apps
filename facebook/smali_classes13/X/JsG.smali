.class public final LX/JsG;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/users/username/EditUsernameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V
    .locals 0

    .prologue
    .line 2745219
    iput-object p1, p0, LX/JsG;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;B)V
    .locals 0

    .prologue
    .line 2745218
    invoke-direct {p0, p1}, LX/JsG;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2745216
    iget-object v0, p0, LX/JsG;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CMq;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/users/username/EditUsernameFragment;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/CMq;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2745217
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2745213
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2745214
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2745215
    return-void
.end method
