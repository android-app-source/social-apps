.class public LX/JWu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JWv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWu",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JWv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703634
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703635
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWu;->b:LX/0Zi;

    .line 2703636
    iput-object p1, p0, LX/JWu;->a:LX/0Ot;

    .line 2703637
    return-void
.end method

.method public static a(LX/0QB;)LX/JWu;
    .locals 4

    .prologue
    .line 2703638
    const-class v1, LX/JWu;

    monitor-enter v1

    .line 2703639
    :try_start_0
    sget-object v0, LX/JWu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703640
    sput-object v2, LX/JWu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703641
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703642
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703643
    new-instance v3, LX/JWu;

    const/16 p0, 0x2132

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWu;-><init>(LX/0Ot;)V

    .line 2703644
    move-object v0, v3

    .line 2703645
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703646
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703647
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2703649
    check-cast p2, LX/JWt;

    .line 2703650
    iget-object v0, p0, LX/JWu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWv;

    iget-object v1, p2, LX/JWt;->a:LX/1Pb;

    iget-object v2, p2, LX/JWt;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703651
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x1

    .line 2703652
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2703653
    check-cast v4, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2703654
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a015d

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b0050

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f0b08fe

    invoke-interface {v4, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x0

    const/16 p2, 0x10

    invoke-interface {v4, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v4, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 2703655
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JWv;->a:LX/JWy;

    const/4 p0, 0x0

    .line 2703656
    new-instance p2, LX/JWx;

    invoke-direct {p2, v4}, LX/JWx;-><init>(LX/JWy;)V

    .line 2703657
    iget-object v0, v4, LX/JWy;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWw;

    .line 2703658
    if-nez v0, :cond_0

    .line 2703659
    new-instance v0, LX/JWw;

    invoke-direct {v0, v4}, LX/JWw;-><init>(LX/JWy;)V

    .line 2703660
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/JWw;->a$redex0(LX/JWw;LX/1De;IILX/JWx;)V

    .line 2703661
    move-object p2, v0

    .line 2703662
    move-object p0, p2

    .line 2703663
    move-object v4, p0

    .line 2703664
    iget-object p0, v4, LX/JWw;->a:LX/JWx;

    iput-object v1, p0, LX/JWx;->b:LX/1Pb;

    .line 2703665
    iget-object p0, v4, LX/JWw;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2703666
    move-object v4, v4

    .line 2703667
    iget-object p0, v4, LX/JWw;->a:LX/JWx;

    iput-object v2, p0, LX/JWx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703668
    iget-object p0, v4, LX/JWw;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2703669
    move-object v4, v4

    .line 2703670
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2703671
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703672
    invoke-static {}, LX/1dS;->b()V

    .line 2703673
    const/4 v0, 0x0

    return-object v0
.end method
