.class public final LX/Jne;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Jna;

.field public final synthetic c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;ZLX/Jna;)V
    .locals 0

    .prologue
    .line 2734228
    iput-object p1, p0, LX/Jne;->c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    iput-boolean p2, p0, LX/Jne;->a:Z

    iput-object p3, p0, LX/Jne;->b:LX/Jna;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2734244
    iget-object v0, p0, LX/Jne;->b:LX/Jna;

    if-eqz v0, :cond_0

    .line 2734245
    iget-object v0, p0, LX/Jne;->b:LX/Jna;

    .line 2734246
    iget-object v1, v0, LX/Jna;->a:LX/Jnb;

    iget-boolean v1, v1, LX/Jnb;->b:Z

    if-eqz v1, :cond_0

    .line 2734247
    iget-object v1, v0, LX/Jna;->a:LX/Jnb;

    iget-object v1, v1, LX/Jnb;->c:LX/Jnd;

    iget-object v1, v1, LX/Jnd;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kL;

    new-instance p0, LX/27k;

    const p1, 0x7f08003e

    invoke-direct {p0, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v1, p0}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2734248
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2734229
    iget-object v0, p0, LX/Jne;->c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    iget-boolean v1, p0, LX/Jne;->a:Z

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 2734230
    iget-object v2, v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->f:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2734231
    new-instance p0, LX/0XI;

    invoke-direct {p0}, LX/0XI;-><init>()V

    invoke-virtual {p0, v2}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v2

    .line 2734232
    if-eqz v1, :cond_0

    .line 2734233
    sget-object p0, LX/Jnf;->b:[I

    invoke-virtual {v1}, LX/03R;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2734234
    :cond_0
    const/4 p0, 0x0

    :goto_0
    move-object p0, p0

    .line 2734235
    iput-object p0, v2, LX/0XI;->ah:LX/0XN;

    .line 2734236
    iget-object p0, v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->f:LX/0WJ;

    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 2734237
    if-eqz v1, :cond_1

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-eq v1, v2, :cond_1

    .line 2734238
    new-instance v2, Landroid/content/Intent;

    sget-object p0, LX/0aY;->J:Ljava/lang/String;

    invoke-direct {v2, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2734239
    iget-object p0, v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->e:LX/0Xp;

    invoke-virtual {p0, v2}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 2734240
    :cond_1
    return-void

    .line 2734241
    :pswitch_0
    sget-object p0, LX/0XN;->AUTO:LX/0XN;

    goto :goto_0

    .line 2734242
    :pswitch_1
    sget-object p0, LX/0XN;->MANUAL:LX/0XN;

    goto :goto_0

    .line 2734243
    :pswitch_2
    sget-object p0, LX/0XN;->UNSET:LX/0XN;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
