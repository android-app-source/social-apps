.class public final LX/K41;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K40;


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766303
    iput-object p1, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;B)V
    .locals 0

    .prologue
    .line 2766304
    invoke-direct {p0, p1}, LX/K41;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 2766305
    iget-object v0, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0, p1}, LX/K4o;->a(F)V

    .line 2766306
    return-void
.end method

.method public final b(F)V
    .locals 2

    .prologue
    .line 2766307
    iget-object v0, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766308
    iget-object v1, v0, LX/K4o;->p:LX/K4l;

    move-object v0, v1

    .line 2766309
    sget-object v1, LX/K4l;->PLAYING:LX/K4l;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/K41;->b:Z

    .line 2766310
    iget-object v0, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0}, LX/K4o;->d()V

    .line 2766311
    iget-object v0, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0, p1}, LX/K4o;->a(F)V

    .line 2766312
    return-void

    .line 2766313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(F)V
    .locals 1

    .prologue
    .line 2766314
    iget-object v0, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0, p1}, LX/K4o;->a(F)V

    .line 2766315
    iget-boolean v0, p0, LX/K41;->b:Z

    if-eqz v0, :cond_0

    .line 2766316
    iget-object v0, p0, LX/K41;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0}, LX/K4o;->b()V

    .line 2766317
    :cond_0
    return-void
.end method
