.class public final LX/JYU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JYV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/JY4;

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JYV;


# direct methods
.method public constructor <init>(LX/JYV;)V
    .locals 1

    .prologue
    .line 2706373
    iput-object p1, p0, LX/JYU;->c:LX/JYV;

    .line 2706374
    move-object v0, p1

    .line 2706375
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2706376
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2706377
    const-string v0, "SaleGroupsNearYouPageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2706378
    if-ne p0, p1, :cond_1

    .line 2706379
    :cond_0
    :goto_0
    return v0

    .line 2706380
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2706381
    goto :goto_0

    .line 2706382
    :cond_3
    check-cast p1, LX/JYU;

    .line 2706383
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2706384
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2706385
    if-eq v2, v3, :cond_0

    .line 2706386
    iget-object v2, p0, LX/JYU;->a:LX/JY4;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JYU;->a:LX/JY4;

    iget-object v3, p1, LX/JYU;->a:LX/JY4;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2706387
    goto :goto_0

    .line 2706388
    :cond_5
    iget-object v2, p1, LX/JYU;->a:LX/JY4;

    if-nez v2, :cond_4

    .line 2706389
    :cond_6
    iget-object v2, p0, LX/JYU;->b:LX/1Pq;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JYU;->b:LX/1Pq;

    iget-object v3, p1, LX/JYU;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2706390
    goto :goto_0

    .line 2706391
    :cond_7
    iget-object v2, p1, LX/JYU;->b:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
