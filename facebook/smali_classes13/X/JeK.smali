.class public final enum LX/JeK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JeK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JeK;

.field public static final enum facebook:LX/JeK;

.field public static final enum messenger:LX/JeK;

.field public static final enum sms:LX/JeK;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2720545
    new-instance v0, LX/JeK;

    const-string v1, "facebook"

    invoke-direct {v0, v1, v2}, LX/JeK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JeK;->facebook:LX/JeK;

    .line 2720546
    new-instance v0, LX/JeK;

    const-string v1, "messenger"

    invoke-direct {v0, v1, v3}, LX/JeK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JeK;->messenger:LX/JeK;

    .line 2720547
    new-instance v0, LX/JeK;

    const-string v1, "sms"

    invoke-direct {v0, v1, v4}, LX/JeK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JeK;->sms:LX/JeK;

    .line 2720548
    const/4 v0, 0x3

    new-array v0, v0, [LX/JeK;

    sget-object v1, LX/JeK;->facebook:LX/JeK;

    aput-object v1, v0, v2

    sget-object v1, LX/JeK;->messenger:LX/JeK;

    aput-object v1, v0, v3

    sget-object v1, LX/JeK;->sms:LX/JeK;

    aput-object v1, v0, v4

    sput-object v0, LX/JeK;->$VALUES:[LX/JeK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2720549
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JeK;
    .locals 1

    .prologue
    .line 2720550
    const-class v0, LX/JeK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JeK;

    return-object v0
.end method

.method public static values()[LX/JeK;
    .locals 1

    .prologue
    .line 2720551
    sget-object v0, LX/JeK;->$VALUES:[LX/JeK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JeK;

    return-object v0
.end method
