.class public final LX/K9u;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 2778646
    const-wide/16 v20, 0x0

    .line 2778647
    const/16 v18, 0x0

    .line 2778648
    const/16 v17, 0x0

    .line 2778649
    const/16 v16, 0x0

    .line 2778650
    const/4 v15, 0x0

    .line 2778651
    const/4 v14, 0x0

    .line 2778652
    const/4 v13, 0x0

    .line 2778653
    const/4 v12, 0x0

    .line 2778654
    const/4 v11, 0x0

    .line 2778655
    const/4 v10, 0x0

    .line 2778656
    const/4 v9, 0x0

    .line 2778657
    const/4 v8, 0x0

    .line 2778658
    const/4 v7, 0x0

    .line 2778659
    const/4 v6, 0x0

    .line 2778660
    const/4 v5, 0x0

    .line 2778661
    const/4 v4, 0x0

    .line 2778662
    const/4 v3, 0x0

    .line 2778663
    const/4 v2, 0x0

    .line 2778664
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_14

    .line 2778665
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2778666
    const/4 v2, 0x0

    .line 2778667
    :goto_0
    return v2

    .line 2778668
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 2778669
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2778670
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2778671
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2778672
    const-string v6, "created_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2778673
    const/4 v2, 0x1

    .line 2778674
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2778675
    :cond_1
    const-string v6, "creation_story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2778676
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 2778677
    :cond_2
    const-string v6, "height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2778678
    const/4 v2, 0x1

    .line 2778679
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v12, v2

    move/from16 v22, v6

    goto :goto_1

    .line 2778680
    :cond_3
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2778681
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 2778682
    :cond_4
    const-string v6, "is_expired"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2778683
    const/4 v2, 0x1

    .line 2778684
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v20, v6

    goto :goto_1

    .line 2778685
    :cond_5
    const-string v6, "is_playable"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2778686
    const/4 v2, 0x1

    .line 2778687
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v19, v6

    goto :goto_1

    .line 2778688
    :cond_6
    const-string v6, "owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2778689
    invoke-static/range {p0 .. p1}, LX/K9s;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2778690
    :cond_7
    const-string v6, "playable_duration"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2778691
    const/4 v2, 0x1

    .line 2778692
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move/from16 v17, v6

    goto/16 :goto_1

    .line 2778693
    :cond_8
    const-string v6, "playable_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2778694
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2778695
    :cond_9
    const-string v6, "savable_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2778696
    invoke-static/range {p0 .. p1}, LX/K9t;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2778697
    :cond_a
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2778698
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2778699
    :cond_b
    const-string v6, "width"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2778700
    const/4 v2, 0x1

    .line 2778701
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v13, v6

    goto/16 :goto_1

    .line 2778702
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2778703
    :cond_d
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2778704
    if-eqz v3, :cond_e

    .line 2778705
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2778706
    :cond_e
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2778707
    if-eqz v12, :cond_f

    .line 2778708
    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2778709
    :cond_f
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2778710
    if-eqz v11, :cond_10

    .line 2778711
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2778712
    :cond_10
    if-eqz v10, :cond_11

    .line 2778713
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2778714
    :cond_11
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2778715
    if-eqz v9, :cond_12

    .line 2778716
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2778717
    :cond_12
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2778718
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2778719
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2778720
    if-eqz v8, :cond_13

    .line 2778721
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, LX/186;->a(III)V

    .line 2778722
    :cond_13
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_14
    move/from16 v19, v14

    move/from16 v22, v17

    move/from16 v23, v18

    move v14, v9

    move/from16 v17, v12

    move/from16 v18, v13

    move v13, v8

    move v12, v6

    move v9, v3

    move v3, v7

    move v8, v2

    move/from16 v24, v11

    move v11, v5

    move/from16 v25, v15

    move v15, v10

    move v10, v4

    move-wide/from16 v4, v20

    move/from16 v20, v25

    move/from16 v21, v16

    move/from16 v16, v24

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2778590
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2778591
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2778592
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 2778593
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778594
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2778595
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2778596
    if-eqz v0, :cond_1

    .line 2778597
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778598
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2778599
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2778600
    if-eqz v0, :cond_2

    .line 2778601
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778602
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2778603
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778604
    if-eqz v0, :cond_3

    .line 2778605
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778606
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778607
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2778608
    if-eqz v0, :cond_4

    .line 2778609
    const-string v1, "is_expired"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778610
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2778611
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2778612
    if-eqz v0, :cond_5

    .line 2778613
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778614
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2778615
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2778616
    if-eqz v0, :cond_6

    .line 2778617
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778618
    invoke-static {p0, v0, p2}, LX/K9s;->a(LX/15i;ILX/0nX;)V

    .line 2778619
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2778620
    if-eqz v0, :cond_7

    .line 2778621
    const-string v1, "playable_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778622
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2778623
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778624
    if-eqz v0, :cond_8

    .line 2778625
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778626
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778627
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2778628
    if-eqz v0, :cond_a

    .line 2778629
    const-string v1, "savable_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778630
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2778631
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2778632
    if-eqz v1, :cond_9

    .line 2778633
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778634
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778635
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2778636
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778637
    if-eqz v0, :cond_b

    .line 2778638
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778639
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778640
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2778641
    if-eqz v0, :cond_c

    .line 2778642
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778643
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2778644
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2778645
    return-void
.end method
