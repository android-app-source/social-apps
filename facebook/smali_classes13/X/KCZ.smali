.class public final LX/KCZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/KAl;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2wX;Landroid/net/Uri;)LX/2wg;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Landroid/net/Uri;",
            ")",
            "LX/2wg",
            "<",
            "LX/KAq;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_2

    move v1, v2

    :goto_0
    const-string v4, "uri must not be null"

    invoke-static {v1, v4}, LX/1ol;->b(ZLjava/lang/Object;)V

    if-eqz v0, :cond_0

    if-ne v0, v2, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    const-string v1, "invalid filter type"

    invoke-static {v3, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    new-instance v1, LX/KCV;

    invoke-direct {v1, p0, p1, p2, v0}, LX/KCV;-><init>(LX/KCZ;LX/2wX;Landroid/net/Uri;I)V

    invoke-virtual {p1, v1}, LX/2wX;->a(LX/2we;)LX/2we;

    move-result-object v1

    move-object v0, v1

    return-object v0

    :cond_2
    move v1, v3

    goto :goto_0
.end method

.method public final a(LX/2wX;Lcom/google/android/gms/wearable/PutDataRequest;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/wearable/PutDataRequest;",
            ")",
            "LX/2wg",
            "<",
            "LX/KAj;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/KCU;

    invoke-direct {v0, p0, p1, p2}, LX/KCU;-><init>(LX/KCZ;LX/2wX;Lcom/google/android/gms/wearable/PutDataRequest;)V

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2wX;Landroid/net/Uri;)LX/2wg;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Landroid/net/Uri;",
            ")",
            "LX/2wg",
            "<",
            "LX/KAk;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_2

    move v1, v2

    :goto_0
    const-string v4, "uri must not be null"

    invoke-static {v1, v4}, LX/1ol;->b(ZLjava/lang/Object;)V

    if-eqz v0, :cond_0

    if-ne v0, v2, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    const-string v1, "invalid filter type"

    invoke-static {v3, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    new-instance v1, LX/KCW;

    invoke-direct {v1, p0, p1, p2, v0}, LX/KCW;-><init>(LX/KCZ;LX/2wX;Landroid/net/Uri;I)V

    invoke-virtual {p1, v1}, LX/2wX;->a(LX/2we;)LX/2we;

    move-result-object v1

    move-object v0, v1

    return-object v0

    :cond_2
    move v1, v3

    goto :goto_0
.end method
