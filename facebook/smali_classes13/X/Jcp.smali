.class public final LX/Jcp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jcq;


# direct methods
.method public constructor <init>(LX/Jcq;)V
    .locals 0

    .prologue
    .line 2718388
    iput-object p1, p0, LX/Jcp;->a:LX/Jcq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x21669027

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2718389
    iget-object v1, p0, LX/Jcp;->a:LX/Jcq;

    .line 2718390
    iget-object v3, v1, LX/Jcq;->l:LX/JdA;

    if-nez v3, :cond_1

    .line 2718391
    :cond_0
    :goto_0
    const v1, 0x4a35c181    # 2977888.2f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2718392
    :cond_1
    const-string v3, "add_account_view_tag"

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2718393
    iget-object v3, v1, LX/Jcq;->l:LX/JdA;

    .line 2718394
    iget-object v4, v3, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-static {v4}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->n(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2718395
    goto :goto_0

    .line 2718396
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718397
    if-eqz v3, :cond_0

    .line 2718398
    iget-object v4, v1, LX/Jcq;->l:LX/JdA;

    .line 2718399
    iget-object p0, v3, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iget-object v1, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-static {v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->r(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 2718400
    iget-object p0, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object p0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->f:LX/4Ad;

    iget-object v1, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/4Ad;->a(Landroid/content/Context;)LX/3dh;

    move-result-object p0

    .line 2718401
    if-eqz p0, :cond_4

    iget-object v1, v3, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iget-object p1, p0, LX/3dh;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2718402
    iget-object v1, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->a(LX/3dh;)Lcom/facebook/messaging/accountswitch/SsoDialogFragment;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    .line 2718403
    :cond_3
    :goto_1
    goto :goto_0

    .line 2718404
    :cond_4
    iget-object p0, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object p0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->k:LX/2Vu;

    iget-object v1, v3, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/2Vu;->a(Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;

    move-result-object p0

    .line 2718405
    if-eqz p0, :cond_5

    .line 2718406
    iget-object v1, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object p1, v3, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->a(Ljava/lang/String;Lcom/facebook/dbllite/data/DblLiteCredentials;)Lcom/facebook/messaging/accountswitch/DblDialogFragment;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_1

    .line 2718407
    :cond_5
    iget-object p0, v4, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;Z)Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_1
.end method
