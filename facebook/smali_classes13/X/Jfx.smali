.class public final LX/Jfx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jfp;

.field public final synthetic b:LX/Jfz;


# direct methods
.method public constructor <init>(LX/Jfz;LX/Jfp;)V
    .locals 0

    .prologue
    .line 2722383
    iput-object p1, p0, LX/Jfx;->b:LX/Jfz;

    iput-object p2, p0, LX/Jfx;->a:LX/Jfp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2722384
    iget-object v0, p0, LX/Jfx;->a:LX/Jfp;

    if-eqz v0, :cond_0

    .line 2722385
    iget-object v0, p0, LX/Jfx;->a:LX/Jfp;

    invoke-interface {v0}, LX/Jfp;->a()V

    .line 2722386
    :cond_0
    iget-object v0, p0, LX/Jfx;->b:LX/Jfz;

    iget-object v0, v0, LX/Jfz;->c:LX/03V;

    const-string v1, "ManagePublisherSelectionLoader"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2722387
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2722388
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;

    .line 2722389
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2722390
    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, LX/Jfx;->b:LX/Jfz;

    .line 2722391
    iput-object v1, v3, LX/Jfz;->e:LX/15i;

    .line 2722392
    iget-object v1, p0, LX/Jfx;->b:LX/Jfz;

    .line 2722393
    iput v0, v1, LX/Jfz;->f:I

    .line 2722394
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2722395
    :cond_0
    iget-object v0, p0, LX/Jfx;->a:LX/Jfp;

    if-nez v0, :cond_1

    .line 2722396
    :goto_0
    return-void

    .line 2722397
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2722398
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2722399
    iget-object v0, p0, LX/Jfx;->a:LX/Jfp;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Jfp;->a(LX/0Px;)V

    goto :goto_0

    .line 2722400
    :cond_2
    iget-object v0, p0, LX/Jfx;->a:LX/Jfp;

    invoke-interface {v0}, LX/Jfp;->a()V

    goto :goto_0
.end method
