.class public final LX/JtP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<[",
        "Lcom/google/android/gms/wearable/Asset;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JtQ;


# direct methods
.method public constructor <init>(LX/JtQ;)V
    .locals 0

    .prologue
    .line 2746714
    iput-object p1, p0, LX/JtP;->a:LX/JtQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2746715
    iget-object v0, p0, LX/JtP;->a:LX/JtQ;

    iget-object v0, v0, LX/JtQ;->a:LX/JtR;

    iget-object v0, v0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746716
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746717
    check-cast p1, [Lcom/google/android/gms/wearable/Asset;

    .line 2746718
    iget-object v0, p0, LX/JtP;->a:LX/JtQ;

    iget-object v0, v0, LX/JtQ;->a:LX/JtR;

    iget-object v0, v0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/JtT;

    iget-object v2, p0, LX/JtP;->a:LX/JtQ;

    iget-object v2, v2, LX/JtQ;->a:LX/JtR;

    iget-object v2, v2, LX/JtR;->b:LX/JtS;

    iget-object v2, v2, LX/JtS;->b:Ljava/lang/String;

    sget-object v3, LX/Jt0;->STICKER:LX/Jt0;

    invoke-direct {v1, v2, p1, v3}, LX/JtT;-><init>(Ljava/lang/String;[Lcom/google/android/gms/wearable/Asset;LX/Jt0;)V

    const v2, 0x3d184003

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746719
    return-void
.end method
