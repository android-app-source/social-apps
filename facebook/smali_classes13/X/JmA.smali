.class public final LX/JmA;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2732494
    new-instance v0, LX/0su;

    sget-object v1, LX/Jm9;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/JmA;->a:LX/0sv;

    .line 2732495
    sget-object v0, LX/Jm9;->a:LX/0U1;

    sget-object v1, LX/Jm9;->b:LX/0U1;

    sget-object v2, LX/Jm9;->c:LX/0U1;

    sget-object v3, LX/Jm9;->d:LX/0U1;

    sget-object v4, LX/Jm9;->e:LX/0U1;

    sget-object v5, LX/Jm9;->f:LX/0U1;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/JmA;->b:LX/0Px;

    .line 2732496
    const-string v0, "units"

    const-string v1, "units_position_index"

    sget-object v2, LX/Jm9;->c:LX/0U1;

    .line 2732497
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2732498
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->b(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/JmA;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2732499
    const-string v0, "units"

    sget-object v1, LX/JmA;->b:LX/0Px;

    sget-object v2, LX/JmA;->a:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 2732500
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 2732501
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2732502
    sget-object v0, LX/JmA;->c:Ljava/lang/String;

    const v1, -0x7a92ea5c

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x65243dbf

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2732503
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 2732504
    return-void
.end method
