.class public LX/K3K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

.field public c:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

.field public d:LX/3l1;

.field public e:LX/K3I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/ui/DragSortThumbnailListView;Ljava/lang/String;LX/3l1;Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;)V
    .locals 0
    .param p1    # Lcom/facebook/slideshow/ui/DragSortThumbnailListView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2765508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2765509
    iput-object p1, p0, LX/K3K;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    .line 2765510
    iput-object p2, p0, LX/K3K;->f:Ljava/lang/String;

    .line 2765511
    iput-object p3, p0, LX/K3K;->d:LX/3l1;

    .line 2765512
    iput-object p4, p0, LX/K3K;->c:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    .line 2765513
    invoke-direct {p0}, LX/K3K;->b()V

    .line 2765514
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2765515
    iget-object v0, p0, LX/K3K;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    iget-object v1, p0, LX/K3K;->c:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2765516
    new-instance v0, LX/3xm;

    new-instance v1, LX/K3J;

    invoke-direct {v1, p0}, LX/K3J;-><init>(LX/K3K;)V

    invoke-direct {v0, v1}, LX/3xm;-><init>(LX/3xj;)V

    iget-object v1, p0, LX/K3K;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    invoke-virtual {v0, v1}, LX/3xm;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 2765517
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2765518
    iput-object p1, p0, LX/K3K;->a:LX/0Px;

    .line 2765519
    iget-object v0, p0, LX/K3K;->c:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    .line 2765520
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p0, v0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    .line 2765521
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2765522
    return-void
.end method
