.class public LX/JoN;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/JoD;


# instance fields
.field public a:LX/JoE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

.field private d:Landroid/graphics/Paint;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/graphics/Paint;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private f:Landroid/graphics/Paint;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private h:I

.field private i:F

.field private j:F

.field private k:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private l:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2735424
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/JoN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2735425
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2735426
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JoN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2735427
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2735428
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2735429
    const-class v0, LX/JoN;

    invoke-static {v0, p0}, LX/JoN;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2735430
    const v0, 0x7f030b79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2735431
    const v0, 0x7f0d1adb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/JoN;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2735432
    iget-object v0, p0, LX/JoN;->a:LX/JoE;

    iget-object v1, p0, LX/JoN;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, LX/JoE;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    move-result-object v0

    iput-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735433
    iget-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735434
    iput-object p0, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->s:LX/JoD;

    .line 2735435
    invoke-virtual {p0}, LX/JoN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2735436
    const v1, 0x7f0b1f32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/JoN;->h:I

    .line 2735437
    invoke-virtual {p0}, LX/JoN;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, LX/JoN;->setUnreadIndicatorColor(I)V

    .line 2735438
    const v1, 0x7f0b1f30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/JoN;->j:F

    .line 2735439
    const v1, 0x7f0a0821

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/JoN;->k:I

    .line 2735440
    const v1, 0x7f0a0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/JoN;->l:I

    .line 2735441
    const v1, 0x7f0b1f31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/JoN;->i:F

    .line 2735442
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 2735405
    sget-object v0, LX/JoL;->a:[I

    iget-object v1, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735406
    iget-object v2, v1, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    move-object v1, v2

    .line 2735407
    invoke-virtual {v1}, LX/JoM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2735408
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown progress indicator type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2735409
    :pswitch_0
    iget-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    const-wide v2, 0x3fa99999a0000000L    # 0.05000000074505806

    .line 2735410
    iget-object v4, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    sget-object v5, LX/JoM;->PENDING:LX/JoM;

    if-eq v4, v5, :cond_1

    .line 2735411
    const-wide/16 v2, 0x0

    .line 2735412
    :cond_0
    :goto_0
    move-wide v0, v2

    .line 2735413
    invoke-direct {p0, p1, v0, v1}, LX/JoN;->a(Landroid/graphics/Canvas;D)V

    .line 2735414
    :goto_1
    :pswitch_1
    return-void

    .line 2735415
    :pswitch_2
    invoke-direct {p0, p1}, LX/JoN;->b(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 2735416
    :cond_1
    iget-object v4, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v4, :cond_0

    .line 2735417
    iget-object v4, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v5, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v4, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->e(Lcom/facebook/ui/media/attachments/MediaResource;)D

    move-result-wide v6

    .line 2735418
    const-wide v4, 0x3fee666660000000L    # 0.949999988079071

    .line 2735419
    sub-double v8, v4, v2

    mul-double/2addr v8, v6

    add-double/2addr v8, v2

    move-wide v2, v8

    .line 2735420
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/graphics/Canvas;D)V
    .locals 12

    .prologue
    .line 2735443
    invoke-direct {p0}, LX/JoN;->e()V

    .line 2735444
    iget v0, p0, LX/JoN;->j:F

    const/high16 v1, 0x41a00000    # 20.0f

    div-float/2addr v0, v1

    .line 2735445
    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    .line 2735446
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/JoN;->j:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v0

    .line 2735447
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, LX/JoN;->j:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    add-float/2addr v2, v0

    .line 2735448
    iget-object v3, p0, LX/JoN;->e:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2735449
    iget-object v3, p0, LX/JoN;->e:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735450
    iget v3, p0, LX/JoN;->j:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget-object v4, p0, LX/JoN;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2735451
    iget-object v3, p0, LX/JoN;->e:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2735452
    iget-object v3, p0, LX/JoN;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2735453
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v0, v3

    .line 2735454
    new-instance v6, Landroid/graphics/RectF;

    iget v4, p0, LX/JoN;->j:F

    sub-float/2addr v4, v3

    iget v5, p0, LX/JoN;->j:F

    sub-float/2addr v5, v3

    invoke-direct {v6, v3, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2735455
    invoke-virtual {v6, v0, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 2735456
    iget v0, p0, LX/JoN;->j:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    sub-float v0, v1, v0

    iget v1, p0, LX/JoN;->j:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    sub-float v1, v2, v1

    invoke-virtual {v6, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 2735457
    const/high16 v7, 0x43b40000    # 360.0f

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-wide v0, p2

    .line 2735458
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    .line 2735459
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    .line 2735460
    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    .line 2735461
    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    .line 2735462
    move-wide v0, v8

    .line 2735463
    double-to-float v0, v0

    mul-float v3, v7, v0

    .line 2735464
    const-wide/16 v0, 0x0

    cmpl-double v0, p2, v0

    if-lez v0, :cond_0

    .line 2735465
    iget-object v0, p0, LX/JoN;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/JoN;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735466
    const/high16 v2, 0x43870000    # 270.0f

    const/4 v4, 0x0

    iget-object v5, p0, LX/JoN;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, v6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 2735467
    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p2, v0

    if-gez v0, :cond_1

    .line 2735468
    iget-object v0, p0, LX/JoN;->e:Landroid/graphics/Paint;

    iget v1, p0, LX/JoN;->l:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735469
    const/high16 v0, 0x43870000    # 270.0f

    add-float v2, v0, v3

    const/high16 v0, 0x43b40000    # 360.0f

    sub-float v3, v0, v3

    const/4 v4, 0x0

    iget-object v5, p0, LX/JoN;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, v6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 2735470
    :cond_1
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V
    .locals 2

    .prologue
    .line 2735471
    invoke-virtual {p0}, LX/JoN;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/JoN;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 2735472
    :cond_0
    invoke-virtual {p0}, LX/JoN;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/JoJ;

    invoke-direct {v1, p0, p1, p2, p3}, LX/JoJ;-><init>(LX/JoN;Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2735473
    :goto_0
    return-void

    .line 2735474
    :cond_1
    iget-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/JoN;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/JoN;

    const-class v1, LX/JoE;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/JoE;

    iput-object v0, p0, LX/JoN;->a:LX/JoE;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2735475
    iget-object v0, p0, LX/JoN;->f:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 2735476
    :goto_0
    return-void

    .line 2735477
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/JoN;->f:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/high16 v8, 0x40000000    # 2.0f

    .line 2735478
    invoke-direct {p0}, LX/JoN;->b()V

    .line 2735479
    iget v0, p0, LX/JoN;->j:F

    const/high16 v1, 0x41a00000    # 20.0f

    div-float/2addr v0, v1

    .line 2735480
    mul-float v1, v0, v8

    .line 2735481
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, LX/JoN;->j:F

    div-float/2addr v3, v8

    sub-float/2addr v2, v3

    add-float/2addr v2, v1

    .line 2735482
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, LX/JoN;->j:F

    div-float/2addr v4, v8

    sub-float/2addr v3, v4

    add-float/2addr v3, v1

    .line 2735483
    iget v4, p0, LX/JoN;->j:F

    div-float/2addr v4, v8

    sub-float v4, v2, v4

    .line 2735484
    iget v5, p0, LX/JoN;->j:F

    div-float/2addr v5, v8

    sub-float v5, v3, v5

    .line 2735485
    iget-object v6, p0, LX/JoN;->f:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2735486
    iget-object v6, p0, LX/JoN;->f:Landroid/graphics/Paint;

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735487
    iget v6, p0, LX/JoN;->j:F

    div-float/2addr v6, v8

    iget-object v7, p0, LX/JoN;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2735488
    iget-object v6, p0, LX/JoN;->f:Landroid/graphics/Paint;

    iget v7, p0, LX/JoN;->k:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735489
    iget v6, p0, LX/JoN;->j:F

    div-float/2addr v6, v8

    sub-float/2addr v6, v1

    iget-object v7, p0, LX/JoN;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2735490
    new-instance v2, Landroid/graphics/RectF;

    const/high16 v3, 0x41100000    # 9.0f

    mul-float/2addr v3, v0

    const/high16 v6, 0x40a00000    # 5.0f

    mul-float/2addr v6, v0

    const/high16 v7, 0x41300000    # 11.0f

    mul-float/2addr v7, v0

    const/high16 v8, 0x41500000    # 13.0f

    mul-float/2addr v8, v0

    invoke-direct {v2, v3, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2735491
    invoke-virtual {v2, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 2735492
    iget-object v3, p0, LX/JoN;->f:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735493
    iget-object v3, p0, LX/JoN;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v1, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2735494
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    iget-object v2, p0, LX/JoN;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v1, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2735495
    return-void
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 2735496
    iget-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735497
    iget-boolean v1, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->v:Z

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->x:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2735498
    if-nez v0, :cond_0

    .line 2735499
    :goto_1
    return-void

    .line 2735500
    :cond_0
    invoke-direct {p0}, LX/JoN;->f()V

    .line 2735501
    iget-object v0, p0, LX/JoN;->d:Landroid/graphics/Paint;

    iget v1, p0, LX/JoN;->i:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2735502
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/JoN;->h:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 2735503
    iget v1, p0, LX/JoN;->h:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 2735504
    iget-object v2, p0, LX/JoN;->d:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2735505
    iget-object v2, p0, LX/JoN;->d:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735506
    iget v2, p0, LX/JoN;->h:I

    int-to-float v2, v2

    iget-object v3, p0, LX/JoN;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2735507
    iget-object v2, p0, LX/JoN;->d:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2735508
    iget-object v2, p0, LX/JoN;->d:Landroid/graphics/Paint;

    iget v3, p0, LX/JoN;->g:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2735509
    iget v2, p0, LX/JoN;->h:I

    int-to-float v2, v2

    iget-object v3, p0, LX/JoN;->d:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, LX/JoN;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2735421
    iget-object v0, p0, LX/JoN;->e:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 2735422
    :goto_0
    return-void

    .line 2735423
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/JoN;->e:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2735402
    iget-object v0, p0, LX/JoN;->d:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 2735403
    :goto_0
    return-void

    .line 2735404
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/JoN;->d:Landroid/graphics/Paint;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2735400
    invoke-virtual {p0}, LX/JoN;->invalidate()V

    .line 2735401
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2735396
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2735397
    invoke-direct {p0, p1}, LX/JoN;->c(Landroid/graphics/Canvas;)V

    .line 2735398
    invoke-direct {p0, p1}, LX/JoN;->a(Landroid/graphics/Canvas;)V

    .line 2735399
    return-void
.end method

.method public getTileRadius()I
    .locals 2

    .prologue
    .line 2735395
    invoke-virtual {p0}, LX/JoN;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/JoN;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2b4856db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2735390
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2735391
    iget-object v1, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735392
    iget-object v2, v1, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    sget-object p0, LX/JoM;->PENDING:LX/JoM;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v1, v2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Z)V

    .line 2735393
    const/16 v1, 0x2d

    const v2, -0x62b65f5c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2735394
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x62d9b40d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2735386
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2735387
    iget-object v1, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735388
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Z)V

    .line 2735389
    const/16 v1, 0x2d

    const v2, -0x364a8923

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6ddd2247

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2735381
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 2735382
    iget-object v1, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    .line 2735383
    iput p1, v1, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    .line 2735384
    iput p2, v1, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    .line 2735385
    const/16 v1, 0x2d

    const v2, -0x18ec3405

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setMessage(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2735379
    const/4 v0, 0x0

    sget-object v1, LX/JoM;->NONE:LX/JoM;

    invoke-direct {p0, p1, v0, v1}, LX/JoN;->a(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V

    .line 2735380
    return-void
.end method

.method public setSolidColor(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2735375
    invoke-virtual {p0}, LX/JoN;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/JoN;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 2735376
    :cond_0
    invoke-virtual {p0}, LX/JoN;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/JoK;

    invoke-direct {v1, p0, p1}, LX/JoK;-><init>(LX/JoN;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2735377
    :goto_0
    return-void

    .line 2735378
    :cond_1
    iget-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(I)V

    goto :goto_0
.end method

.method public setThreadData(LX/DhQ;)V
    .locals 2

    .prologue
    .line 2735371
    invoke-virtual {p0}, LX/JoN;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/JoN;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 2735372
    :cond_0
    invoke-virtual {p0}, LX/JoN;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/JoI;

    invoke-direct {v1, p0, p1}, LX/JoI;-><init>(LX/JoN;LX/DhQ;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2735373
    :goto_0
    return-void

    .line 2735374
    :cond_1
    iget-object v0, p0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(LX/DhQ;)V

    goto :goto_0
.end method

.method public setUnreadIndicatorColor(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2735368
    iput p1, p0, LX/JoN;->g:I

    .line 2735369
    invoke-virtual {p0}, LX/JoN;->invalidate()V

    .line 2735370
    return-void
.end method
