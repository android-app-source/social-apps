.class public final LX/K0N;
.super LX/1Ah;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ah",
        "<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5s9;

.field public final synthetic b:LX/K0P;


# direct methods
.method public constructor <init>(LX/K0P;LX/5s9;)V
    .locals 0

    .prologue
    .line 2758433
    iput-object p1, p0, LX/K0N;->b:LX/K0P;

    iput-object p2, p0, LX/K0N;->a:LX/5s9;

    invoke-direct {p0}, LX/1Ah;-><init>()V

    return-void
.end method

.method private a(LX/1ln;)V
    .locals 7

    .prologue
    .line 2758434
    if-eqz p1, :cond_0

    .line 2758435
    iget-object v6, p0, LX/K0N;->a:LX/5s9;

    new-instance v0, LX/K0K;

    iget-object v1, p0, LX/K0N;->b:LX/K0P;

    invoke-virtual {v1}, LX/K0P;->getId()I

    move-result v1

    const/4 v2, 0x2

    iget-object v3, p0, LX/K0N;->b:LX/K0P;

    iget-object v3, v3, LX/K0P;->f:LX/9nR;

    .line 2758436
    iget-object v4, v3, LX/9nR;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2758437
    invoke-virtual {p1}, LX/1ln;->g()I

    move-result v4

    invoke-virtual {p1}, LX/1ln;->h()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/K0K;-><init>(IILjava/lang/String;II)V

    invoke-virtual {v6, v0}, LX/5s9;->a(LX/5r0;)V

    .line 2758438
    iget-object v0, p0, LX/K0N;->a:LX/5s9;

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/K0N;->b:LX/K0P;

    invoke-virtual {v2}, LX/K0P;->getId()I

    move-result v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, LX/K0K;-><init>(II)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2758439
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2758440
    iget-object v0, p0, LX/K0N;->a:LX/5s9;

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/K0N;->b:LX/K0P;

    invoke-virtual {v2}, LX/K0P;->getId()I

    move-result v2

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, LX/K0K;-><init>(II)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2758441
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2758442
    check-cast p2, LX/1ln;

    invoke-direct {p0, p2}, LX/K0N;->a(LX/1ln;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2758443
    iget-object v0, p0, LX/K0N;->a:LX/5s9;

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/K0N;->b:LX/K0P;

    invoke-virtual {v2}, LX/K0P;->getId()I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/K0K;-><init>(II)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2758444
    iget-object v0, p0, LX/K0N;->a:LX/5s9;

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/K0N;->b:LX/K0P;

    invoke-virtual {v2}, LX/K0P;->getId()I

    move-result v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, LX/K0K;-><init>(II)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2758445
    return-void
.end method
