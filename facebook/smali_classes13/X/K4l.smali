.class public final enum LX/K4l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K4l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K4l;

.field public static final enum DESTROYED:LX/K4l;

.field public static final enum EXPORTING:LX/K4l;

.field public static final enum INITIALIZED:LX/K4l;

.field public static final enum PLAYING:LX/K4l;

.field public static final enum READY_TO_PLAY:LX/K4l;

.field public static final enum UNINITIALIZED:LX/K4l;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2769273
    new-instance v0, LX/K4l;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v3}, LX/K4l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4l;->UNINITIALIZED:LX/K4l;

    .line 2769274
    new-instance v0, LX/K4l;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v4}, LX/K4l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4l;->INITIALIZED:LX/K4l;

    .line 2769275
    new-instance v0, LX/K4l;

    const-string v1, "READY_TO_PLAY"

    invoke-direct {v0, v1, v5}, LX/K4l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4l;->READY_TO_PLAY:LX/K4l;

    .line 2769276
    new-instance v0, LX/K4l;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v6}, LX/K4l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4l;->PLAYING:LX/K4l;

    .line 2769277
    new-instance v0, LX/K4l;

    const-string v1, "EXPORTING"

    invoke-direct {v0, v1, v7}, LX/K4l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4l;->EXPORTING:LX/K4l;

    .line 2769278
    new-instance v0, LX/K4l;

    const-string v1, "DESTROYED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/K4l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4l;->DESTROYED:LX/K4l;

    .line 2769279
    const/4 v0, 0x6

    new-array v0, v0, [LX/K4l;

    sget-object v1, LX/K4l;->UNINITIALIZED:LX/K4l;

    aput-object v1, v0, v3

    sget-object v1, LX/K4l;->INITIALIZED:LX/K4l;

    aput-object v1, v0, v4

    sget-object v1, LX/K4l;->READY_TO_PLAY:LX/K4l;

    aput-object v1, v0, v5

    sget-object v1, LX/K4l;->PLAYING:LX/K4l;

    aput-object v1, v0, v6

    sget-object v1, LX/K4l;->EXPORTING:LX/K4l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/K4l;->DESTROYED:LX/K4l;

    aput-object v2, v0, v1

    sput-object v0, LX/K4l;->$VALUES:[LX/K4l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2769280
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K4l;
    .locals 1

    .prologue
    .line 2769281
    const-class v0, LX/K4l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K4l;

    return-object v0
.end method

.method public static values()[LX/K4l;
    .locals 1

    .prologue
    .line 2769282
    sget-object v0, LX/K4l;->$VALUES:[LX/K4l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K4l;

    return-object v0
.end method
