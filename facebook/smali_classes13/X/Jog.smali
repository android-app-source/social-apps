.class public final LX/Jog;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "LX/DiN;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic b:LX/DiN;

.field public final synthetic c:LX/Joj;


# direct methods
.method public constructor <init>(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/DiN;)V
    .locals 0

    .prologue
    .line 2735934
    iput-object p1, p0, LX/Jog;->c:LX/Joj;

    iput-object p2, p0, LX/Jog;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object p3, p0, LX/Jog;->b:LX/DiN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2735935
    iget-object v0, p0, LX/Jog;->c:LX/Joj;

    iget-object v1, p0, LX/Jog;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v1}, LX/Joj;->a$redex0(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2735936
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2735937
    check-cast p1, LX/0Px;

    .line 2735938
    if-nez p1, :cond_0

    .line 2735939
    :goto_0
    return-void

    .line 2735940
    :cond_0
    iget-object v0, p0, LX/Jog;->c:LX/Joj;

    iget-object v1, p0, LX/Jog;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v1, v2}, LX/Joj;->a$redex0(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/LinkedList;)V

    .line 2735941
    iget-object v0, p0, LX/Jog;->c:LX/Joj;

    iget-object v1, p0, LX/Jog;->b:LX/DiN;

    invoke-static {v0, v1}, LX/Joj;->c(LX/Joj;LX/DiN;)V

    .line 2735942
    iget-object v0, p0, LX/Jog;->c:LX/Joj;

    iget-object v1, p0, LX/Jog;->b:LX/DiN;

    invoke-static {v0, v1}, LX/Joj;->b$redex0(LX/Joj;LX/DiN;)V

    goto :goto_0
.end method
