.class public final LX/JYr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 2707383
    iput-object p1, p0, LX/JYr;->b:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;

    iput-object p2, p0, LX/JYr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x25e5f044

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2707384
    iget-object v1, p0, LX/JYr;->b:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->f:LX/0Zb;

    iget-object v2, p0, LX/JYr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/JYy;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/BOe;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2707385
    iget-object v1, p0, LX/JYr;->b:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->e:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/JYr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, LX/BOe;->b()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2707386
    const v1, 0xe47ae82

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
