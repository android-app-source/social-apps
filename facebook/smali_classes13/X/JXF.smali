.class public final LX/JXF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JXB;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2704238
    iput-object p1, p0, LX/JXF;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    iput-object p2, p0, LX/JXF;->a:LX/JXB;

    iput-object p3, p0, LX/JXF;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iput-object p4, p0, LX/JXF;->c:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const v3, -0x4f416542

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2704239
    iget-object v0, p0, LX/JXF;->a:LX/JXB;

    .line 2704240
    iput-boolean v1, v0, LX/JXB;->e:Z

    .line 2704241
    iget-object v0, p0, LX/JXF;->a:LX/JXB;

    iget-object v4, p0, LX/JXF;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iget-object v5, p0, LX/JXF;->a:LX/JXB;

    .line 2704242
    iget-object p1, v5, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v5, p1

    .line 2704243
    invoke-static {v0, v4, v5}, LX/JXK;->a(LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 2704244
    iget-object v4, p0, LX/JXF;->a:LX/JXB;

    if-nez v0, :cond_0

    move v0, v1

    .line 2704245
    :goto_0
    iput-boolean v0, v4, LX/JXB;->h:Z

    .line 2704246
    iget-object v0, p0, LX/JXF;->c:LX/1Pq;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/JXF;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    aput-object v4, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2704247
    const v0, 0x511981c9

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_0
    move v0, v2

    .line 2704248
    goto :goto_0
.end method
