.class public LX/K14;
.super LX/K13;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/1Ae;

.field private final c:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:I

.field private f:Landroid/net/Uri;

.field private g:I

.field private h:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;IILandroid/net/Uri;LX/1Ae;Ljava/lang/Object;)V
    .locals 2
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2761078
    invoke-direct {p0}, LX/K13;-><init>()V

    .line 2761079
    new-instance v0, LX/1aX;

    invoke-static {p1}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1aX;-><init>(LX/1aY;)V

    iput-object v0, p0, LX/K14;->c:LX/1aX;

    .line 2761080
    iput-object p5, p0, LX/K14;->b:LX/1Ae;

    .line 2761081
    iput-object p6, p0, LX/K14;->d:Ljava/lang/Object;

    .line 2761082
    iput p2, p0, LX/K14;->e:I

    .line 2761083
    iput p3, p0, LX/K14;->g:I

    .line 2761084
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, LX/K14;->f:Landroid/net/Uri;

    .line 2761085
    return-void

    .line 2761086
    :cond_0
    sget-object p4, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2761119
    iget-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final a(Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 2761117
    iput-object p1, p0, LX/K14;->h:Landroid/widget/TextView;

    .line 2761118
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2761115
    iget-object v0, p0, LX/K14;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2761116
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2761113
    iget-object v0, p0, LX/K14;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2761114
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2761111
    iget-object v0, p0, LX/K14;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2761112
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2761096
    iget-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2761097
    iget-object v0, p0, LX/K14;->f:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2761098
    iget-object v1, p0, LX/K14;->b:LX/1Ae;

    invoke-virtual {v1}, LX/1Ae;->b()LX/1Ae;

    move-result-object v1

    iget-object v2, p0, LX/K14;->c:LX/1aX;

    .line 2761099
    iget-object p2, v2, LX/1aX;->f:LX/1aZ;

    move-object v2, p2

    .line 2761100
    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    iget-object v2, p0, LX/K14;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, LX/1Ae;->b(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ae;->h()LX/1bp;

    move-result-object v0

    .line 2761101
    iget-object v1, p0, LX/K14;->c:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 2761102
    iget-object v0, p0, LX/K14;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    .line 2761103
    iget-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    iget v1, p0, LX/K14;->g:I

    iget v2, p0, LX/K14;->e:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2761104
    iget-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/K14;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2761105
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2761106
    iget-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, p7, v0

    .line 2761107
    int-to-float v0, v0

    invoke-virtual {p1, p5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2761108
    iget-object v0, p0, LX/K14;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2761109
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2761110
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2761094
    iget-object v0, p0, LX/K14;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2761095
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2761093
    iget v0, p0, LX/K14;->e:I

    return v0
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2761087
    if-eqz p5, :cond_0

    .line 2761088
    iget v0, p0, LX/K14;->e:I

    neg-int v0, v0

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 2761089
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 2761090
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 2761091
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 2761092
    :cond_0
    iget v0, p0, LX/K14;->g:I

    return v0
.end method
