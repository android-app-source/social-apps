.class public final LX/Jki;
.super LX/3Mg;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V
    .locals 0

    .prologue
    .line 2730218
    iput-object p1, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    invoke-direct {p0}, LX/3Mg;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    .line 2730219
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-wide v2, v2, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->k:J

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x5

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2730220
    iget-object v0, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v1, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v1, v1, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2730221
    iput-wide v2, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->k:J

    .line 2730222
    iget-object v0, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->g:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->b()V

    .line 2730223
    iget-object v0, p0, LX/Jki;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->g:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2730224
    :cond_0
    monitor-exit p0

    return-void

    .line 2730225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
