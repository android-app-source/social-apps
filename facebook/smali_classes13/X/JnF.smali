.class public final LX/JnF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6e1;


# instance fields
.field public final synthetic a:Lcom/facebook/user/model/User;

.field public final synthetic b:LX/9VZ;

.field public final synthetic c:LX/JnH;


# direct methods
.method public constructor <init>(LX/JnH;Lcom/facebook/user/model/User;LX/9VZ;)V
    .locals 0

    .prologue
    .line 2733884
    iput-object p1, p0, LX/JnF;->c:LX/JnH;

    iput-object p2, p0, LX/JnF;->a:Lcom/facebook/user/model/User;

    iput-object p3, p0, LX/JnF;->b:LX/9VZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/dialog/MenuDialogItem;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2733885
    iget-object v0, p0, LX/JnF;->c:LX/JnH;

    iget-object v0, v0, LX/JnH;->c:LX/Jkm;

    if-eqz v0, :cond_0

    .line 2733886
    invoke-static {}, LX/FFq;->values()[LX/FFq;

    move-result-object v0

    .line 2733887
    iget v1, p1, Lcom/facebook/messaging/dialog/MenuDialogItem;->a:I

    move v1, v1

    .line 2733888
    aget-object v0, v0, v1

    .line 2733889
    iget-object v1, p0, LX/JnF;->c:LX/JnH;

    iget-object v1, v1, LX/JnH;->h:LX/3Ec;

    iget-object v2, p0, LX/JnF;->a:Lcom/facebook/user/model/User;

    .line 2733890
    iget-object v3, v2, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v3

    .line 2733891
    invoke-virtual {v1, v2}, LX/3Ec;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2733892
    iget-object v2, p0, LX/JnF;->c:LX/JnH;

    iget-object v2, v2, LX/JnH;->i:LX/3Rl;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Click on lightweight action in dialog: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/6FV;->LIGHTWEIGHT_ACTIONS:LX/6FV;

    invoke-virtual {v2, v3, v4}, LX/3Rl;->a(Ljava/lang/String;LX/6FV;)V

    .line 2733893
    iget-object v2, p0, LX/JnF;->c:LX/JnH;

    iget-object v3, p0, LX/JnF;->b:LX/9VZ;

    invoke-static {v2, v1, v0, v3}, LX/JnH;->a$redex0(LX/JnH;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FFq;LX/9VZ;)V

    .line 2733894
    iget-object v2, p0, LX/JnF;->c:LX/JnH;

    iget-object v3, p0, LX/JnF;->a:Lcom/facebook/user/model/User;

    .line 2733895
    iget-object v4, v2, LX/JnH;->d:Landroid/content/Context;

    .line 2733896
    sget-object v5, LX/FFq;->OTHERS:LX/FFq;

    if-ne v0, v5, :cond_1

    .line 2733897
    const-string v5, ""

    .line 2733898
    :goto_0
    move-object v4, v5

    .line 2733899
    iget-object v5, v2, LX/JnH;->f:Landroid/view/View;

    const/4 p0, 0x0

    invoke-static {v5, v4, p0}, LX/4nm;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/4nm;

    move-result-object v4

    const v5, 0x7f08099b

    new-instance p0, LX/JnG;

    invoke-direct {p0, v2, v1}, LX/JnG;-><init>(LX/JnH;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v4, v5, p0}, LX/4nm;->a(ILandroid/view/View$OnClickListener;)LX/4nm;

    move-result-object v4

    .line 2733900
    iget-object v5, v2, LX/JnH;->d:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f0a00d5

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, LX/4nm;->a(I)LX/4nm;

    .line 2733901
    invoke-virtual {v4}, LX/4nm;->a()V

    .line 2733902
    const/4 v0, 0x1

    .line 2733903
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget v5, v0, LX/FFq;->deliveredResId:I

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p0, p1

    invoke-virtual {v4, v5, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method
