.class public final LX/JkT;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/JoinRequestNotification;

.field public final synthetic b:Landroid/app/PendingIntent;

.field public final synthetic c:I

.field public final synthetic d:LX/JkU;


# direct methods
.method public constructor <init>(LX/JkU;Lcom/facebook/messaging/notify/JoinRequestNotification;Landroid/app/PendingIntent;I)V
    .locals 0

    .prologue
    .line 2729898
    iput-object p1, p0, LX/JkT;->d:LX/JkU;

    iput-object p2, p0, LX/JkT;->a:Lcom/facebook/messaging/notify/JoinRequestNotification;

    iput-object p3, p0, LX/JkT;->b:Landroid/app/PendingIntent;

    iput p4, p0, LX/JkT;->c:I

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 12
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2729921
    iget-object v0, p0, LX/JkT;->d:LX/JkU;

    iget-object v2, p0, LX/JkT;->a:Lcom/facebook/messaging/notify/JoinRequestNotification;

    iget-object v3, p0, LX/JkT;->b:Landroid/app/PendingIntent;

    iget v4, p0, LX/JkT;->c:I

    move-object v1, p1

    .line 2729922
    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 2729923
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2729924
    new-instance v7, LX/2HB;

    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    invoke-direct {v7, v8}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v7

    .line 2729925
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const/high16 v9, 0x4000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3GK;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3RH;->J:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_key_for_settings"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_settings_type_for_settings"

    sget-object v11, LX/Jod;->GROUP:LX/Jod;

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "start_fragment"

    const/16 v11, 0x3e9

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    .line 2729926
    invoke-static {v0, v8}, LX/JkU;->a(LX/JkU;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v8

    move-object v8, v8

    .line 2729927
    iput-object v8, v7, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2729928
    move-object v7, v7

    .line 2729929
    invoke-virtual {v7, v3}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v7

    invoke-virtual {v7, p1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v7

    .line 2729930
    iget-object v8, v0, LX/JkU;->h:LX/2Og;

    invoke-virtual {v8, v6}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2729931
    goto :goto_0

    .line 2729932
    :goto_0
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v6, v8, p1}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2729933
    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v8, v9, p0}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v8

    .line 2729934
    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b16

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, p0, v9, v6}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v6

    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b17

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, p0, v9, v8}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2729935
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->b:Ljava/lang/String;

    invoke-virtual {v7, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 2729936
    if-eqz v1, :cond_0

    .line 2729937
    iput-object v1, v7, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2729938
    :cond_0
    iget-object v6, v0, LX/JkU;->e:LX/3RQ;

    invoke-virtual {v6, v7}, LX/3RQ;->a(LX/2HB;)Z

    .line 2729939
    iget-object v6, v0, LX/JkU;->i:LX/3RK;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x272f

    invoke-virtual {v7}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v6, v8, v9, v7}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2729940
    iput-boolean p1, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->e:Z

    .line 2729941
    invoke-virtual {v2}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2729942
    return-void
    .line 2729943
.end method

.method public final f(LX/1ca;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2729899
    iget-object v0, p0, LX/JkT;->d:LX/JkU;

    iget-object v2, p0, LX/JkT;->a:Lcom/facebook/messaging/notify/JoinRequestNotification;

    iget-object v3, p0, LX/JkT;->b:Landroid/app/PendingIntent;

    iget v4, p0, LX/JkT;->c:I

    .line 2729900
    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 2729901
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2729902
    new-instance v7, LX/2HB;

    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    invoke-direct {v7, v8}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v7

    .line 2729903
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const/high16 v9, 0x4000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3GK;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3RH;->J:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_key_for_settings"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_settings_type_for_settings"

    sget-object v11, LX/Jod;->GROUP:LX/Jod;

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "start_fragment"

    const/16 v11, 0x3e9

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    .line 2729904
    invoke-static {v0, v8}, LX/JkU;->a(LX/JkU;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v8

    move-object v8, v8

    .line 2729905
    iput-object v8, v7, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2729906
    move-object v7, v7

    .line 2729907
    invoke-virtual {v7, v3}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v7

    invoke-virtual {v7, p1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v7

    .line 2729908
    iget-object v8, v0, LX/JkU;->h:LX/2Og;

    invoke-virtual {v8, v6}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2729909
    goto :goto_0

    .line 2729910
    :goto_0
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v6, v8, p1}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2729911
    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v8, v9, p0}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v8

    .line 2729912
    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b16

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, p0, v9, v6}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v6

    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b17

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, p0, v9, v8}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2729913
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->b:Ljava/lang/String;

    invoke-virtual {v7, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 2729914
    goto :goto_1

    .line 2729915
    :goto_1
    iget-object v6, v0, LX/JkU;->e:LX/3RQ;

    invoke-virtual {v6, v7}, LX/3RQ;->a(LX/2HB;)Z

    .line 2729916
    iget-object v6, v0, LX/JkU;->i:LX/3RK;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x272f

    invoke-virtual {v7}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v6, v8, v9, v7}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2729917
    iput-boolean p1, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->e:Z

    .line 2729918
    invoke-virtual {v2}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2729919
    return-void
    .line 2729920
.end method
