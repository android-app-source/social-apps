.class public final LX/K9g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/K9l;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/livemap/LiveMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/livemap/LiveMapFragment;)V
    .locals 0

    .prologue
    .line 2777697
    iput-object p1, p0, LX/K9g;->a:Lcom/facebook/video/livemap/LiveMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2777698
    check-cast p1, LX/K9l;

    check-cast p2, LX/K9l;

    .line 2777699
    iget v0, p1, LX/K9l;->e:F

    iget v1, p2, LX/K9l;->e:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    .line 2777700
    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p1, LX/K9l;->f:F

    iget v1, p2, LX/K9l;->f:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method
