.class public final LX/Jkr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/user/model/UserKey;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;)V
    .locals 0

    .prologue
    .line 2730398
    iput-object p1, p0, LX/Jkr;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/user/model/UserKey;Lcom/facebook/user/model/UserKey;)I
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    .line 2730390
    iget-object v2, p0, LX/Jkr;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;

    iget-object v2, v2, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->c:LX/2CH;

    invoke-virtual {v2, p1}, LX/2CH;->e(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/LastActive;

    move-result-object v2

    .line 2730391
    iget-object v3, p0, LX/Jkr;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;

    iget-object v3, v3, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->c:LX/2CH;

    invoke-virtual {v3, p2}, LX/2CH;->e(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/LastActive;

    move-result-object v4

    .line 2730392
    if-eqz v2, :cond_1

    .line 2730393
    iget-wide v5, v2, Lcom/facebook/user/model/LastActive;->a:J

    move-wide v2, v5

    .line 2730394
    :goto_0
    if-eqz v4, :cond_0

    .line 2730395
    iget-wide v5, v4, Lcom/facebook/user/model/LastActive;->a:J

    move-wide v0, v5

    .line 2730396
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0

    :cond_1
    move-wide v2, v0

    .line 2730397
    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2730389
    check-cast p1, Lcom/facebook/user/model/UserKey;

    check-cast p2, Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, p1, p2}, LX/Jkr;->a(Lcom/facebook/user/model/UserKey;Lcom/facebook/user/model/UserKey;)I

    move-result v0

    return v0
.end method
