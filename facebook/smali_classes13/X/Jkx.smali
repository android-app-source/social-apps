.class public final LX/Jkx;
.super LX/3Mg;
.source ""


# instance fields
.field public final synthetic a:LX/Jkz;


# direct methods
.method public constructor <init>(LX/Jkz;)V
    .locals 0

    .prologue
    .line 2730644
    iput-object p1, p0, LX/Jkx;->a:LX/Jkz;

    invoke-direct {p0}, LX/3Mg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 2730645
    iget-object v0, p0, LX/Jkx;->a:LX/Jkz;

    const/4 v5, 0x0

    .line 2730646
    iget-object v1, v0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2CH;

    .line 2730647
    iget-object v2, v1, LX/2CH;->J:LX/2AL;

    move-object v1, v2

    .line 2730648
    sget-object v2, LX/2AL;->MQTT_DISCONNECTED:LX/2AL;

    if-ne v1, v2, :cond_0

    .line 2730649
    iget-object v1, v0, LX/Jkz;->j:LX/3Lb;

    invoke-virtual {v1}, LX/3Lb;->b()V

    .line 2730650
    iput-boolean v5, v0, LX/Jkz;->s:Z

    .line 2730651
    :goto_0
    return-void

    .line 2730652
    :cond_0
    iget-boolean v1, v0, LX/Jkz;->w:Z

    if-eqz v1, :cond_1

    iget-boolean v1, v0, LX/Jkz;->v:Z

    if-nez v1, :cond_1

    iget-object v1, v0, LX/Jkz;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x12d

    invoke-virtual {v1, v2, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, v0, LX/Jkz;->t:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x2bf20

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    .line 2730653
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Jkz;->u:Z

    goto :goto_0

    .line 2730654
    :cond_1
    iput-boolean v5, v0, LX/Jkz;->u:Z

    .line 2730655
    const/4 v11, 0x0

    .line 2730656
    iget-object v6, v0, LX/Jkz;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v7, 0x1ad

    invoke-virtual {v6, v7, v11}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v0, LX/Jkz;->x:LX/0qK;

    invoke-virtual {v6}, LX/0qK;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2730657
    :cond_2
    invoke-static {v0}, LX/Jkz;->e(LX/Jkz;)V

    .line 2730658
    iget-object v6, v0, LX/Jkz;->y:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v6, :cond_3

    .line 2730659
    iget-object v6, v0, LX/Jkz;->y:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v6, v11}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2730660
    const/4 v6, 0x0

    iput-object v6, v0, LX/Jkz;->y:Ljava/util/concurrent/ScheduledFuture;

    .line 2730661
    :cond_3
    :goto_1
    goto :goto_0

    .line 2730662
    :cond_4
    iget-object v6, v0, LX/Jkz;->y:Ljava/util/concurrent/ScheduledFuture;

    if-nez v6, :cond_3

    .line 2730663
    iget-object v12, v0, LX/Jkz;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;

    iget-wide v8, v0, LX/Jkz;->t:J

    const/4 v10, 0x1

    move-object v7, v0

    invoke-direct/range {v6 .. v10}, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;-><init>(LX/Jkz;JZ)V

    const-wide/16 v8, 0x1e

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v12, v6, v8, v9, v7}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, v0, LX/Jkz;->y:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1
.end method
