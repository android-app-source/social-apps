.class public LX/JsS;
.super Lcom/facebook/messaging/xma/ui/XMALinearLayout;
.source ""


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/xma/SubattachmentStyleRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2745525
    invoke-direct {p0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;-><init>(Landroid/content/Context;)V

    .line 2745526
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JsS;->setOrientation(I)V

    .line 2745527
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JsS;->b:Ljava/util/List;

    .line 2745528
    return-void
.end method


# virtual methods
.method public final a(LX/6ll;)V
    .locals 3
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745529
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/JsS;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2745530
    invoke-virtual {p0, v1}, LX/JsS;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2745531
    instance-of v2, v0, LX/6lq;

    if-eqz v2, :cond_0

    .line 2745532
    check-cast v0, LX/6lq;

    invoke-interface {v0, p1}, LX/6lq;->setXMACallback(LX/6ll;)V

    .line 2745533
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2745534
    :cond_1
    return-void
.end method
