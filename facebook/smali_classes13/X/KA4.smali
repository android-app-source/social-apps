.class public final LX/KA4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/wearlistener/WearNodeListener;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/KAv;

.field public final synthetic b:Lcom/facebook/wearlistener/DataLayerListenerService;


# direct methods
.method public constructor <init>(Lcom/facebook/wearlistener/DataLayerListenerService;LX/KAv;)V
    .locals 0

    .prologue
    .line 2778910
    iput-object p1, p0, LX/KA4;->b:Lcom/facebook/wearlistener/DataLayerListenerService;

    iput-object p2, p0, LX/KA4;->a:LX/KAv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/3Rh;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/wearlistener/WearNodeListener;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2778912
    iget-object v0, p0, LX/KA4;->a:LX/KAv;

    .line 2778913
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 2778914
    :try_start_0
    iget-object v1, p1, LX/3Rh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2778915
    iget-object v1, p1, LX/3Rh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V

    .line 2778916
    :cond_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 2778917
    iget-object v3, p1, LX/3Rh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0db;->aB:LX/0Tn;

    const-string p0, ""

    invoke-interface {v3, v4, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2778918
    invoke-static {v1, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 2778919
    invoke-interface {v0}, LX/KAv;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2778920
    iget-object v3, p1, LX/3Rh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/0db;->aB:LX/0Tn;

    const-string p0, ":"

    invoke-static {p0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v4, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2778921
    const/4 v1, 0x0

    const v3, -0x245a9c50

    invoke-static {v2, v1, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2778922
    :goto_0
    move-object v0, v2

    .line 2778923
    return-object v0

    .line 2778924
    :catch_0
    move-exception v1

    .line 2778925
    invoke-virtual {v2, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2778911
    check-cast p1, LX/3Rh;

    invoke-direct {p0, p1}, LX/KA4;->a(LX/3Rh;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
