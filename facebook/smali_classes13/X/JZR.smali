.class public LX/JZR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JZM;


# direct methods
.method public constructor <init>(LX/JZM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2708624
    iput-object p1, p0, LX/JZR;->a:LX/JZM;

    .line 2708625
    return-void
.end method

.method public static a(LX/0QB;)LX/JZR;
    .locals 7

    .prologue
    .line 2708626
    const-class v1, LX/JZR;

    monitor-enter v1

    .line 2708627
    :try_start_0
    sget-object v0, LX/JZR;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708628
    sput-object v2, LX/JZR;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708629
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708630
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708631
    new-instance v4, LX/JZR;

    .line 2708632
    new-instance p0, LX/JZM;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/1AY;->b(LX/0QB;)LX/1AY;

    move-result-object v5

    check-cast v5, LX/1AY;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v6

    check-cast v6, LX/1Bv;

    invoke-direct {p0, v3, v5, v6}, LX/JZM;-><init>(LX/0Uh;LX/1AY;LX/1Bv;)V

    .line 2708633
    move-object v3, p0

    .line 2708634
    check-cast v3, LX/JZM;

    invoke-direct {v4, v3}, LX/JZR;-><init>(LX/JZM;)V

    .line 2708635
    move-object v0, v4

    .line 2708636
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708637
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708638
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708639
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
