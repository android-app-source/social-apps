.class public LX/Jtm;
.super LX/CGt;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CGt",
        "<",
        "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2747557
    invoke-direct {p0, p1, p2}, LX/CGt;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2747558
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2747529
    new-instance v0, LX/Jtv;

    invoke-direct {v0}, LX/Jtv;-><init>()V

    move-object v0, v0

    .line 2747530
    const-string v1, "noteID"

    .line 2747531
    iget-object v2, p0, LX/CGt;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2747532
    move-object v2, v2

    .line 2747533
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "maxAuthors"

    .line 2747534
    iget v2, p0, LX/CGt;->d:I

    move v2, v2

    .line 2747535
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "maxElements"

    .line 2747536
    iget v2, p0, LX/CGt;->e:I

    move v2, v2

    .line 2747537
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "maxListElements"

    .line 2747538
    iget v2, p0, LX/CGt;->f:I

    move v2, v2

    .line 2747539
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "maxSlideshowMedia"

    .line 2747540
    iget v2, p0, LX/CGt;->g:I

    move v2, v2

    .line 2747541
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "media_type"

    .line 2747542
    iget-object v2, p0, LX/CGt;->h:LX/0wF;

    move-object v2, v2

    .line 2747543
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "scale"

    .line 2747544
    iget-object v2, p0, LX/CGt;->i:LX/0wC;

    move-object v2, v2

    .line 2747545
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "final_image_width"

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "final_image_height"

    .line 2747546
    iget v2, p0, LX/CGt;->o:I

    move v2, v2

    .line 2747547
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Jtv;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2747548
    iget-wide v4, p0, LX/CGt;->j:J

    move-wide v2, v4

    .line 2747549
    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2747550
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2747551
    move-object v0, v0

    .line 2747552
    iget-object v1, p0, LX/CGt;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 2747553
    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2747554
    iget-object v1, p0, LX/CGt;->k:LX/0zS;

    move-object v1, v1

    .line 2747555
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2747556
    return-object v0
.end method
