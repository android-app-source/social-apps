.class public LX/Jxt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2753712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753713
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/Jxt;->b:LX/0UE;

    .line 2753714
    return-void
.end method

.method public static a(LX/0QB;)LX/Jxt;
    .locals 4

    .prologue
    .line 2753715
    const-class v1, LX/Jxt;

    monitor-enter v1

    .line 2753716
    :try_start_0
    sget-object v0, LX/Jxt;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2753717
    sput-object v2, LX/Jxt;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2753718
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2753719
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2753720
    new-instance p0, LX/Jxt;

    invoke-direct {p0}, LX/Jxt;-><init>()V

    .line 2753721
    invoke-static {v0}, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->a(LX/0QB;)Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    move-result-object v3

    check-cast v3, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    .line 2753722
    iput-object v3, p0, LX/Jxt;->a:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    .line 2753723
    move-object v0, p0

    .line 2753724
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2753725
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Jxt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2753726
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2753727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
