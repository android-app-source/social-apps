.class public LX/K1n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/315;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/K1n;


# instance fields
.field public final a:LX/0em;

.field public final b:LX/GaN;

.field private final c:LX/0en;


# direct methods
.method public constructor <init>(LX/0em;LX/GaN;LX/0en;)V
    .locals 0
    .param p2    # LX/GaN;
        .annotation runtime Lcom/facebook/common/patch/annotation/Bsdiff;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2762641
    iput-object p1, p0, LX/K1n;->a:LX/0em;

    .line 2762642
    iput-object p2, p0, LX/K1n;->b:LX/GaN;

    .line 2762643
    iput-object p3, p0, LX/K1n;->c:LX/0en;

    .line 2762644
    return-void
.end method

.method public static a(LX/0QB;)LX/K1n;
    .locals 6

    .prologue
    .line 2762625
    sget-object v0, LX/K1n;->d:LX/K1n;

    if-nez v0, :cond_1

    .line 2762626
    const-class v1, LX/K1n;

    monitor-enter v1

    .line 2762627
    :try_start_0
    sget-object v0, LX/K1n;->d:LX/K1n;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2762628
    if-eqz v2, :cond_0

    .line 2762629
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2762630
    new-instance p0, LX/K1n;

    invoke-static {v0}, LX/0em;->a(LX/0QB;)LX/0em;

    move-result-object v3

    check-cast v3, LX/0em;

    .line 2762631
    invoke-static {}, LX/JHa;->a()LX/GaN;

    move-result-object v4

    move-object v4, v4

    .line 2762632
    check-cast v4, LX/GaN;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v5

    check-cast v5, LX/0en;

    invoke-direct {p0, v3, v4, v5}, LX/K1n;-><init>(LX/0em;LX/GaN;LX/0en;)V

    .line 2762633
    move-object v0, p0

    .line 2762634
    sput-object v0, LX/K1n;->d:LX/K1n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2762635
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2762636
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2762637
    :cond_1
    sget-object v0, LX/K1n;->d:LX/K1n;

    return-object v0

    .line 2762638
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2762639
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;LX/0ff;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2762592
    invoke-static {p2}, LX/K1n;->a(LX/0ff;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2762593
    iget-object v0, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v0, p1, p2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    move v0, v0

    .line 2762594
    if-nez v0, :cond_1

    .line 2762595
    :cond_0
    :goto_0
    return-void

    .line 2762596
    :cond_1
    iget-boolean v0, p2, LX/0ff;->d:Z

    move v0, v0

    .line 2762597
    if-eqz v0, :cond_2

    .line 2762598
    invoke-virtual {p2}, LX/0ff;->o()LX/0ff;

    move-result-object v0

    .line 2762599
    if-nez v0, :cond_5

    .line 2762600
    const/4 v0, 0x0

    .line 2762601
    :goto_1
    move v0, v0

    .line 2762602
    if-eqz v0, :cond_3

    .line 2762603
    :cond_2
    invoke-static {p0, p1, p2}, LX/K1n;->k(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    goto :goto_0

    .line 2762604
    :cond_3
    sget-object v0, LX/K1m;->a:[I

    invoke-virtual {p2}, LX/0ff;->i()LX/0gR;

    move-result-object v1

    invoke-virtual {v1}, LX/0gR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2762605
    invoke-static {p0, p1, p2}, LX/K1n;->k(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    goto :goto_0

    .line 2762606
    :pswitch_0
    invoke-static {p0, p1, p2}, LX/K1n;->i(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2762607
    invoke-static {p0, p1, p2}, LX/K1n;->k(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    .line 2762608
    :goto_2
    goto :goto_0

    .line 2762609
    :pswitch_1
    invoke-static {p0, p1, p2}, LX/K1n;->c(LX/K1n;Landroid/content/Context;LX/0ff;)V

    goto :goto_0

    .line 2762610
    :pswitch_2
    invoke-static {p0, p1, p2}, LX/K1n;->i(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2762611
    invoke-static {p0, p1, p2}, LX/K1n;->k(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    .line 2762612
    :cond_4
    goto :goto_0

    .line 2762613
    :cond_5
    iget-object v1, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v1, p1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    .line 2762614
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_1

    .line 2762615
    :cond_6
    :try_start_0
    invoke-virtual {p2}, LX/0ff;->m()LX/0ff;

    move-result-object v0

    .line 2762616
    invoke-virtual {p2}, LX/0ff;->q()LX/0ff;

    move-result-object v1

    .line 2762617
    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 2762618
    iget-object v2, p0, LX/K1n;->b:LX/GaN;

    iget-object v3, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v3, p1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    iget-object v3, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v3, p1, p2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v4, p1, v1}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, LX/GaN;->a(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    .line 2762619
    invoke-static {p0, p1, p2}, LX/K1n;->k(LX/K1n;Landroid/content/Context;LX/0ff;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2762620
    const/4 v0, 0x1

    .line 2762621
    :goto_3
    move v0, v0

    .line 2762622
    if-nez v0, :cond_7

    .line 2762623
    invoke-static {p0, p1, p2}, LX/K1n;->h(LX/K1n;Landroid/content/Context;LX/0ff;)V

    goto :goto_2

    .line 2762624
    :cond_7
    invoke-virtual {p2}, LX/0ff;->q()LX/0ff;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/K1n;->c(LX/K1n;Landroid/content/Context;LX/0ff;)V

    goto :goto_2

    :catch_0
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Ljava/io/InputStream;LX/0ff;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2762587
    iget-object v0, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v0, p1, p3}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    .line 2762588
    new-instance v1, Ljava/util/zip/ZipInputStream;

    invoke-direct {v1, p2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2762589
    invoke-virtual {v1}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    .line 2762590
    iget-object v2, p0, LX/K1n;->c:LX/0en;

    invoke-virtual {v2, v1, v0}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V

    .line 2762591
    return-void
.end method

.method public static a(LX/0ff;)Z
    .locals 1

    .prologue
    .line 2762535
    invoke-virtual {p0}, LX/0ff;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0ff;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 2762586
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public static c(LX/K1n;Landroid/content/Context;LX/0ff;)V
    .locals 3

    .prologue
    .line 2762575
    :try_start_0
    iget-object v0, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v0, p1, p2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v0

    .line 2762576
    invoke-static {v0}, LX/03l;->b([B)Ljava/lang/String;

    move-result-object v0

    .line 2762577
    invoke-virtual {p2}, LX/0ff;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2762578
    :goto_0
    move v0, v0

    .line 2762579
    if-nez v0, :cond_0

    .line 2762580
    invoke-static {p0, p1, p2}, LX/K1n;->h(LX/K1n;Landroid/content/Context;LX/0ff;)V

    .line 2762581
    :goto_1
    return-void

    .line 2762582
    :cond_0
    invoke-virtual {p2}, LX/0ff;->o()LX/0ff;

    move-result-object v0

    .line 2762583
    if-eqz v0, :cond_1

    .line 2762584
    iget-object v1, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v1, p1, p2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v2, p1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    invoke-static {v1, v0}, LX/K1n;->a(Ljava/io/File;Ljava/io/File;)Z

    .line 2762585
    :cond_1
    goto :goto_1

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/K1n;Landroid/content/Context;LX/0ff;)V
    .locals 2

    .prologue
    .line 2762569
    sget-object v0, LX/0gR;->FAILED:LX/0gR;

    invoke-static {p2, v0}, LX/0ff;->a(LX/0ff;LX/0gR;)LX/0ff;

    move-result-object v0

    move-object v0, v0

    .line 2762570
    iget-object v1, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v1, p1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    .line 2762571
    iget-object v1, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v1, p1, p2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1, v0}, LX/K1n;->a(Ljava/io/File;Ljava/io/File;)Z

    .line 2762572
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2762573
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    .line 2762574
    :catch_1
    goto :goto_0
.end method

.method public static i(LX/K1n;Landroid/content/Context;LX/0ff;)Z
    .locals 2

    .prologue
    .line 2762563
    invoke-virtual {p2}, LX/0ff;->m()LX/0ff;

    move-result-object v0

    .line 2762564
    if-nez v0, :cond_0

    .line 2762565
    const/4 v0, 0x0

    .line 2762566
    :goto_0
    return v0

    .line 2762567
    :cond_0
    iget-object v1, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v1, p1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    .line 2762568
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_0
.end method

.method public static k(LX/K1n;Landroid/content/Context;LX/0ff;)Z
    .locals 1

    .prologue
    .line 2762562
    iget-object v0, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v0, p1, p2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 2762548
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2762549
    iget-object v0, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v0, p1}, LX/0em;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2762550
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ff;

    .line 2762551
    invoke-static {v0}, LX/K1n;->a(LX/0ff;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, LX/0ff;->d()I

    move-result v5

    if-ne v5, p2, :cond_0

    invoke-virtual {v0}, LX/0ff;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2762552
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2762553
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2762554
    :cond_1
    move-object v2, v2

    .line 2762555
    iget-object v0, p0, LX/K1n;->a:LX/0em;

    invoke-virtual {v0, p1, v2}, LX/0em;->a(Landroid/content/Context;Ljava/util/ArrayList;)LX/0ff;

    move-result-object v3

    .line 2762556
    if-nez v3, :cond_2

    .line 2762557
    :goto_1
    return-void

    .line 2762558
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ff;

    .line 2762559
    invoke-static {p0, p1, v0}, LX/K1n;->k(LX/K1n;Landroid/content/Context;LX/0ff;)Z

    .line 2762560
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2762561
    :cond_3
    invoke-direct {p0, p1, v3}, LX/K1n;->a(Landroid/content/Context;LX/0ff;)V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Ljava/io/InputStream;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 2762544
    new-instance v0, LX/0ff;

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ff;-><init>(Ljava/lang/String;)V

    .line 2762545
    invoke-direct {p0, p1, p2, v0}, LX/K1n;->a(Landroid/content/Context;Ljava/io/InputStream;LX/0ff;)V

    .line 2762546
    invoke-direct {p0, p1, v0}, LX/K1n;->a(Landroid/content/Context;LX/0ff;)V

    .line 2762547
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/resources/impl/loading/LanguagePackInfo;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2762536
    iget-object v1, p2, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2762537
    if-nez v1, :cond_1

    .line 2762538
    :cond_0
    :goto_1
    return v0

    .line 2762539
    :cond_1
    iget v1, p2, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    int-to-long v4, v1

    .line 2762540
    iget-object v3, p2, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    .line 2762541
    iget-object v1, p2, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    invoke-static {p3, v1}, LX/0ff;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2762542
    iget-object v1, p0, LX/K1n;->a:LX/0em;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0em;->b(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2762543
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
