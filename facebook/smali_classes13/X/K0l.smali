.class public final LX/K0l;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/K0j;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/K0n;

.field private final c:LX/K0o;

.field public d:I

.field private final e:Landroid/view/View$OnLayoutChangeListener;


# direct methods
.method public constructor <init>(LX/K0o;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2759506
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2759507
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K0l;->a:Ljava/util/List;

    .line 2759508
    iput v1, p0, LX/K0l;->d:I

    .line 2759509
    new-instance v0, LX/K0k;

    invoke-direct {v0, p0}, LX/K0k;-><init>(LX/K0l;)V

    iput-object v0, p0, LX/K0l;->e:Landroid/view/View$OnLayoutChangeListener;

    .line 2759510
    iput-object p1, p0, LX/K0l;->c:LX/K0o;

    .line 2759511
    new-instance v0, LX/K0n;

    invoke-direct {v0, p0}, LX/K0n;-><init>(LX/K0l;)V

    iput-object v0, p0, LX/K0l;->b:LX/K0n;

    .line 2759512
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2759513
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;)LX/K0j;
    .locals 3

    .prologue
    .line 2759545
    new-instance v0, LX/K0j;

    new-instance v1, LX/K0m;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/K0m;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/K0j;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method private a(LX/K0j;)V
    .locals 1

    .prologue
    .line 2759542
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2759543
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, LX/K0m;

    invoke-virtual {v0}, LX/K0m;->removeAllViews()V

    .line 2759544
    return-void
.end method

.method private a(LX/K0j;I)V
    .locals 3

    .prologue
    .line 2759537
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, LX/K0m;

    .line 2759538
    iget-object v1, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2759539
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2759540
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/K0m;->addView(Landroid/view/View;I)V

    .line 2759541
    :cond_0
    return-void
.end method

.method public static h(LX/K0l;I)V
    .locals 2

    .prologue
    .line 2759533
    if-eqz p1, :cond_0

    .line 2759534
    iget v0, p0, LX/K0l;->d:I

    add-int/2addr v0, p1

    iput v0, p0, LX/K0l;->d:I

    .line 2759535
    iget-object v0, p0, LX/K0l;->c:LX/K0o;

    iget v1, p0, LX/K0l;->d:I

    invoke-static {v0, v1}, LX/K0o;->h(LX/K0o;I)V

    .line 2759536
    :cond_0
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2759532
    iget-object v0, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 2759531
    invoke-static {p1}, LX/K0l;->a(Landroid/view/ViewGroup;)LX/K0j;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1a1;)V
    .locals 0

    .prologue
    .line 2759530
    check-cast p1, LX/K0j;

    invoke-direct {p0, p1}, LX/K0l;->a(LX/K0j;)V

    return-void
.end method

.method public final bridge synthetic a(LX/1a1;I)V
    .locals 0

    .prologue
    .line 2759529
    check-cast p1, LX/K0j;

    invoke-direct {p0, p1, p2}, LX/K0l;->a(LX/K0j;I)V

    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2759524
    iget-object v0, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2759525
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {p0, v0}, LX/K0l;->h(LX/K0l;I)V

    .line 2759526
    iget-object v0, p0, LX/K0l;->e:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2759527
    invoke-virtual {p0, p2}, LX/1OM;->j_(I)V

    .line 2759528
    return-void
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 2759517
    iget-object v0, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2759518
    if-eqz v0, :cond_0

    .line 2759519
    iget-object v1, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2759520
    iget-object v1, p0, LX/K0l;->e:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2759521
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    invoke-static {p0, v0}, LX/K0l;->h(LX/K0l;I)V

    .line 2759522
    invoke-virtual {p0, p1}, LX/1OM;->d(I)V

    .line 2759523
    :cond_0
    return-void
.end method

.method public final f(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2759516
    iget-object v0, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final g(I)I
    .locals 1

    .prologue
    .line 2759515
    iget-object v0, p0, LX/K0l;->b:LX/K0n;

    invoke-virtual {v0, p1}, LX/K0n;->a(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2759514
    iget-object v0, p0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
