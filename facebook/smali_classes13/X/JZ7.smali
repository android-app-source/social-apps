.class public final enum LX/JZ7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JZ7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JZ7;

.field public static final enum COMPLETE:LX/JZ7;

.field public static final enum NONE:LX/JZ7;

.field public static final enum SAVING:LX/JZ7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2707893
    new-instance v0, LX/JZ7;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/JZ7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JZ7;->NONE:LX/JZ7;

    .line 2707894
    new-instance v0, LX/JZ7;

    const-string v1, "SAVING"

    invoke-direct {v0, v1, v3}, LX/JZ7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JZ7;->SAVING:LX/JZ7;

    .line 2707895
    new-instance v0, LX/JZ7;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v4}, LX/JZ7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JZ7;->COMPLETE:LX/JZ7;

    .line 2707896
    const/4 v0, 0x3

    new-array v0, v0, [LX/JZ7;

    sget-object v1, LX/JZ7;->NONE:LX/JZ7;

    aput-object v1, v0, v2

    sget-object v1, LX/JZ7;->SAVING:LX/JZ7;

    aput-object v1, v0, v3

    sget-object v1, LX/JZ7;->COMPLETE:LX/JZ7;

    aput-object v1, v0, v4

    sput-object v0, LX/JZ7;->$VALUES:[LX/JZ7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2707897
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JZ7;
    .locals 1

    .prologue
    .line 2707898
    const-class v0, LX/JZ7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JZ7;

    return-object v0
.end method

.method public static values()[LX/JZ7;
    .locals 1

    .prologue
    .line 2707899
    sget-object v0, LX/JZ7;->$VALUES:[LX/JZ7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JZ7;

    return-object v0
.end method
