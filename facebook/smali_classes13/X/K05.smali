.class public LX/K05;
.super LX/5pb;
.source ""

# interfaces
.implements LX/9n2;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "PermissionsAndroid"
.end annotation


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/facebook/react/bridge/Callback;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2757287
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2757288
    const/4 v0, 0x0

    iput v0, p0, LX/K05;->b:I

    .line 2757289
    const-string v0, "granted"

    iput-object v0, p0, LX/K05;->c:Ljava/lang/String;

    .line 2757290
    const-string v0, "denied"

    iput-object v0, p0, LX/K05;->d:Ljava/lang/String;

    .line 2757291
    const-string v0, "never_ask_again"

    iput-object v0, p0, LX/K05;->e:Ljava/lang/String;

    .line 2757292
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/K05;->a:Landroid/util/SparseArray;

    .line 2757293
    return-void
.end method

.method private h()LX/2lD;
    .locals 2

    .prologue
    .line 2757281
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2757282
    if-nez v0, :cond_0

    .line 2757283
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to use permissions API while not attached to an Activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757284
    :cond_0
    instance-of v1, v0, LX/2lD;

    if-nez v1, :cond_1

    .line 2757285
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to use permissions API but the host Activity doesn\'t implement PermissionAwareActivity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757286
    :cond_1
    check-cast v0, LX/2lD;

    return-object v0
.end method


# virtual methods
.method public final a(I[I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2757278
    iget-object v0, p0, LX/K05;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v2

    invoke-direct {p0}, LX/K05;->h()LX/2lD;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-interface {v0, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2757279
    iget-object v0, p0, LX/K05;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 2757280
    iget-object v0, p0, LX/K05;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public checkPermission(Ljava/lang/String;LX/5pW;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2757294
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2757295
    invoke-virtual {v2}, LX/5pY;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 2757296
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v3, v4, :cond_1

    .line 2757297
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2757298
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2757299
    goto :goto_0

    .line 2757300
    :cond_1
    invoke-virtual {v2, p1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757277
    const-string v0, "PermissionsAndroid"

    return-object v0
.end method

.method public requestMultiplePermissions(LX/5pC;LX/5pW;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2757253
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2757254
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2757255
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2757256
    invoke-virtual {v1}, LX/5pY;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    move v1, v0

    .line 2757257
    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 2757258
    invoke-interface {p1, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2757259
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x17

    if-ge v2, v7, :cond_1

    .line 2757260
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v7

    invoke-virtual {v5, v6, v2, v7}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "granted"

    :goto_1
    invoke-interface {v3, v6, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757261
    add-int/lit8 v1, v1, 0x1

    .line 2757262
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2757263
    :cond_0
    const-string v2, "denied"

    goto :goto_1

    .line 2757264
    :cond_1
    invoke-virtual {v5, v6}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 2757265
    const-string v2, "granted"

    invoke-interface {v3, v6, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757266
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2757267
    :cond_2
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2757268
    :cond_3
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 2757269
    invoke-interface {p2, v3}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2757270
    :goto_3
    return-void

    .line 2757271
    :cond_4
    :try_start_0
    invoke-direct {p0}, LX/K05;->h()LX/2lD;

    move-result-object v1

    .line 2757272
    iget-object v0, p0, LX/K05;->a:Landroid/util/SparseArray;

    iget v2, p0, LX/K05;->b:I

    new-instance v5, LX/K04;

    invoke-direct {v5, p0, v4, v3, p2}, LX/K04;-><init>(LX/K05;Ljava/util/ArrayList;LX/5pH;LX/5pW;)V

    invoke-virtual {v0, v2, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2757273
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iget v2, p0, LX/K05;->b:I

    invoke-interface {v1, v0, v2, p0}, LX/2lD;->a([Ljava/lang/String;ILX/9n2;)V

    .line 2757274
    iget v0, p0, LX/K05;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/K05;->b:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 2757275
    :catch_0
    move-exception v0

    .line 2757276
    const-string v1, "E_INVALID_ACTIVITY"

    invoke-interface {p2, v1, v0}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method public requestPermission(Ljava/lang/String;LX/5pW;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2757239
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2757240
    invoke-virtual {v2}, LX/5pY;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 2757241
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v3, v4, :cond_1

    .line 2757242
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2757243
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2757244
    goto :goto_0

    .line 2757245
    :cond_1
    invoke-virtual {v2, p1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 2757246
    const-string v0, "granted"

    invoke-interface {p2, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 2757247
    :cond_2
    :try_start_0
    invoke-direct {p0}, LX/K05;->h()LX/2lD;

    move-result-object v0

    .line 2757248
    iget-object v1, p0, LX/K05;->a:Landroid/util/SparseArray;

    iget v2, p0, LX/K05;->b:I

    new-instance v3, LX/K03;

    invoke-direct {v3, p0, p2, p1}, LX/K03;-><init>(LX/K05;LX/5pW;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2757249
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget v2, p0, LX/K05;->b:I

    invoke-interface {v0, v1, v2, p0}, LX/2lD;->a([Ljava/lang/String;ILX/9n2;)V

    .line 2757250
    iget v0, p0, LX/K05;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/K05;->b:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2757251
    :catch_0
    move-exception v0

    .line 2757252
    const-string v1, "E_INVALID_ACTIVITY"

    invoke-interface {p2, v1, v0}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public shouldShowRequestPermissionRationale(Ljava/lang/String;LX/5pW;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757233
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 2757234
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2757235
    :goto_0
    return-void

    .line 2757236
    :cond_0
    :try_start_0
    invoke-direct {p0}, LX/K05;->h()LX/2lD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/2lD;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, LX/5pW;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2757237
    :catch_0
    move-exception v0

    .line 2757238
    const-string v1, "E_INVALID_ACTIVITY"

    invoke-interface {p2, v1, v0}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
