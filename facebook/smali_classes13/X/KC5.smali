.class public LX/KC5;
.super LX/KB5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/KB5;"
    }
.end annotation


# instance fields
.field private a:LX/4uw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uw",
            "<",
            "LX/Jt9;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/4uw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uw",
            "<",
            "LX/JtA;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/4uw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uw",
            "<",
            "LX/JtB;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/4uw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uw",
            "<",
            "LX/Jt8;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/4uw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uw",
            "<",
            "LX/Jt7;",
            ">;"
        }
    .end annotation
.end field

.field public final f:[Landroid/content/IntentFilter;

.field public final g:Ljava/lang/String;


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    iget-object v0, p0, LX/KC5;->a:LX/4uw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KC5;->a:LX/4uw;

    new-instance v1, LX/KBz;

    invoke-direct {v1, p1}, LX/KBz;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    move-object v1, v1

    invoke-virtual {v0, v1}, LX/4uw;->a(LX/4uv;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->close()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/AmsEntityUpdateParcelable;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/AncsNotificationParcelable;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/CapabilityInfoParcelable;)V
    .locals 2

    iget-object v0, p0, LX/KC5;->e:LX/4uw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KC5;->e:LX/4uw;

    new-instance v1, LX/KC4;

    invoke-direct {v1, p1}, LX/KC4;-><init>(Lcom/google/android/gms/wearable/internal/CapabilityInfoParcelable;)V

    move-object v1, v1

    invoke-virtual {v0, v1}, LX/4uw;->a(LX/4uv;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ChannelEventParcelable;)V
    .locals 2

    iget-object v0, p0, LX/KC5;->d:LX/4uw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KC5;->d:LX/4uw;

    new-instance v1, LX/KC3;

    invoke-direct {v1, p1}, LX/KC3;-><init>(Lcom/google/android/gms/wearable/internal/ChannelEventParcelable;)V

    move-object v1, v1

    invoke-virtual {v0, v1}, LX/4uw;->a(LX/4uv;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/MessageEventParcelable;)V
    .locals 2

    iget-object v0, p0, LX/KC5;->b:LX/4uw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KC5;->b:LX/4uw;

    new-instance v1, LX/KC0;

    invoke-direct {v1, p1}, LX/KC0;-><init>(Lcom/google/android/gms/wearable/internal/MessageEventParcelable;)V

    move-object v1, v1

    invoke-virtual {v0, v1}, LX/4uw;->a(LX/4uv;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V
    .locals 2

    iget-object v0, p0, LX/KC5;->c:LX/4uw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KC5;->c:LX/4uw;

    new-instance v1, LX/KC1;

    invoke-direct {v1, p1}, LX/KC1;-><init>(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V

    move-object v1, v1

    invoke-virtual {v0, v1}, LX/4uw;->a(LX/4uv;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/wearable/internal/NodeParcelable;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final b(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V
    .locals 2

    iget-object v0, p0, LX/KC5;->c:LX/4uw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KC5;->c:LX/4uw;

    new-instance v1, LX/KC2;

    invoke-direct {v1, p1}, LX/KC2;-><init>(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V

    move-object v1, v1

    invoke-virtual {v0, v1}, LX/4uw;->a(LX/4uv;)V

    :cond_0
    return-void
.end method
