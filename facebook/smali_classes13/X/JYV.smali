.class public LX/JYV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYV",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706455
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2706456
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYV;->b:LX/0Zi;

    .line 2706457
    iput-object p1, p0, LX/JYV;->a:LX/0Ot;

    .line 2706458
    return-void
.end method

.method public static a(LX/0QB;)LX/JYV;
    .locals 4

    .prologue
    .line 2706444
    const-class v1, LX/JYV;

    monitor-enter v1

    .line 2706445
    :try_start_0
    sget-object v0, LX/JYV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706446
    sput-object v2, LX/JYV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706447
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706448
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706449
    new-instance v3, LX/JYV;

    const/16 p0, 0x2178

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYV;-><init>(LX/0Ot;)V

    .line 2706450
    move-object v0, v3

    .line 2706451
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706452
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706453
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2706459
    const v0, -0x670b9ed6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2706409
    check-cast p2, LX/JYU;

    .line 2706410
    iget-object v0, p0, LX/JYV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;

    iget-object v1, p2, LX/JYU;->a:LX/JY4;

    iget-object v2, p2, LX/JYU;->b:LX/1Pq;

    const/16 v12, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2706411
    iget-object v3, v1, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    invoke-static {v3}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    .line 2706412
    if-eqz v7, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, LX/0Tp;->b(Z)V

    .line 2706413
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->x()I

    move-result v8

    .line 2706414
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->a()I

    move-result v3

    move v6, v3

    :goto_1
    move-object v3, v2

    .line 2706415
    check-cast v3, LX/1Pr;

    new-instance v9, LX/JY5;

    iget-object v10, v1, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    invoke-direct {v9, v10}, LX/JY5;-><init>(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;)V

    iget-object v10, v1, LX/JY4;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-interface {v3, v9, v10}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JY6;

    .line 2706416
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    const v10, 0x7f020a3c

    invoke-interface {v9, v10}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    invoke-static {v0, p1, v7, v1}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a(Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLProfile;LX/JY4;)LX/1Dh;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    const v11, 0x7f0b0933

    invoke-interface {v10, v11}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v10

    const v11, 0x7f0b0936

    invoke-interface {v10, v11}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v10

    const/4 v11, 0x7

    invoke-interface {v10, v11, v12}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v5, v12}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v10

    const/4 v11, 0x2

    invoke-interface {v10, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    const/4 p0, 0x3

    const/4 v12, 0x1

    .line 2706417
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    const v11, 0x7f0b0050

    invoke-virtual {v7, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v11, 0x7f0a0428

    invoke-virtual {v7, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v12}, LX/1ne;->t(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v12}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v7

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v11}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v11, 0x7f020a8d

    invoke-interface {v7, v11}, LX/1Di;->x(I)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7, v12, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v7

    move-object v5, v7

    .line 2706418
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1, v8}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a(LX/1De;I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2706419
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f00cf

    new-array v11, p2, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, p0

    invoke-virtual {v7, v8, v6, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    const v7, 0x7f0b004b

    invoke-virtual {v5, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v7, 0x7f0a015d

    invoke-virtual {v5, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    move-object v5, v5

    .line 2706420
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/JYV;->d(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v10, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 2706421
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0046

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 2706422
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->c:LX/JYA;

    const/4 v6, 0x0

    .line 2706423
    new-instance v7, LX/JY8;

    invoke-direct {v7, v5}, LX/JY8;-><init>(LX/JYA;)V

    .line 2706424
    iget-object v8, v5, LX/JYA;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/JY9;

    .line 2706425
    if-nez v8, :cond_0

    .line 2706426
    new-instance v8, LX/JY9;

    invoke-direct {v8, v5}, LX/JY9;-><init>(LX/JYA;)V

    .line 2706427
    :cond_0
    invoke-static {v8, p1, v6, v6, v7}, LX/JY9;->a$redex0(LX/JY9;LX/1De;IILX/JY8;)V

    .line 2706428
    move-object v7, v8

    .line 2706429
    move-object v6, v7

    .line 2706430
    move-object v5, v6

    .line 2706431
    iget-object v6, v5, LX/JY9;->a:LX/JY8;

    iput-object v2, v6, LX/JY8;->c:LX/1Pq;

    .line 2706432
    iget-object v6, v5, LX/JY9;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2706433
    move-object v5, v5

    .line 2706434
    iget-object v6, v5, LX/JY9;->a:LX/JY8;

    iput-object v3, v6, LX/JY8;->b:LX/JY6;

    .line 2706435
    iget-object v6, v5, LX/JY9;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2706436
    move-object v3, v5

    .line 2706437
    iget-object v5, v3, LX/JY9;->a:LX/JY8;

    iput-object v1, v5, LX/JY8;->a:LX/JY4;

    .line 2706438
    iget-object v5, v3, LX/JY9;->e:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2706439
    move-object v3, v3

    .line 2706440
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v9, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2706441
    return-object v0

    :cond_1
    move v3, v5

    .line 2706442
    goto/16 :goto_0

    :cond_2
    move v6, v5

    .line 2706443
    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2706392
    invoke-static {}, LX/1dS;->b()V

    .line 2706393
    iget v0, p1, LX/1dQ;->b:I

    .line 2706394
    sparse-switch v0, :sswitch_data_0

    .line 2706395
    :goto_0
    return-object v2

    .line 2706396
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2706397
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2706398
    check-cast v1, LX/JYU;

    .line 2706399
    iget-object v3, p0, LX/JYV;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;

    iget-object p1, v1, LX/JYU;->a:LX/JY4;

    .line 2706400
    iget-object p2, v3, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->d:LX/JY7;

    .line 2706401
    iget-object p0, p1, LX/JY4;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iget-object v1, p1, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    const-string v3, "sgny_profile"

    invoke-static {p2, p0, v1, v3}, LX/JY7;->a(LX/JY7;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;Ljava/lang/String;)V

    .line 2706402
    iget-object p0, p1, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    invoke-static {p0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    .line 2706403
    iget-object v1, p2, LX/JY7;->g:LX/1nA;

    invoke-static {p0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object p0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, p0, v3}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2706404
    goto :goto_0

    .line 2706405
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2706406
    check-cast v0, LX/JYU;

    .line 2706407
    iget-object v1, p0, LX/JYV;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;

    iget-object v3, v0, LX/JYU;->a:LX/JY4;

    invoke-virtual {v1, v3}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a(LX/JY4;)V

    .line 2706408
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x670b9ed6 -> :sswitch_0
        -0x1c78f93b -> :sswitch_1
    .end sparse-switch
.end method
