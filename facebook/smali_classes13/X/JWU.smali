.class public LX/JWU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field private final b:LX/17Q;

.field private final c:LX/0Zb;

.field private final d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field private final e:LX/JWD;

.field private final f:LX/2dj;

.field private final g:LX/2do;

.field private final h:LX/2dp;

.field public final i:LX/1vg;

.field public final j:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/23P;LX/17Q;LX/0Zb;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/JWD;LX/2dj;LX/2do;LX/2dp;LX/1vg;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703006
    iput-object p1, p0, LX/JWU;->a:LX/23P;

    .line 2703007
    iput-object p2, p0, LX/JWU;->b:LX/17Q;

    .line 2703008
    iput-object p3, p0, LX/JWU;->c:LX/0Zb;

    .line 2703009
    iput-object p4, p0, LX/JWU;->d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2703010
    iput-object p5, p0, LX/JWU;->e:LX/JWD;

    .line 2703011
    iput-object p6, p0, LX/JWU;->f:LX/2dj;

    .line 2703012
    iput-object p7, p0, LX/JWU;->g:LX/2do;

    .line 2703013
    iput-object p8, p0, LX/JWU;->h:LX/2dp;

    .line 2703014
    iput-object p9, p0, LX/JWU;->i:LX/1vg;

    .line 2703015
    iput-object p10, p0, LX/JWU;->j:Landroid/content/res/Resources;

    .line 2703016
    return-void
.end method

.method public static a(LX/0QB;)LX/JWU;
    .locals 14

    .prologue
    .line 2703017
    const-class v1, LX/JWU;

    monitor-enter v1

    .line 2703018
    :try_start_0
    sget-object v0, LX/JWU;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703019
    sput-object v2, LX/JWU;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703020
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703021
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703022
    new-instance v3, LX/JWU;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v7

    check-cast v7, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0}, LX/JWD;->a(LX/0QB;)LX/JWD;

    move-result-object v8

    check-cast v8, LX/JWD;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v9

    check-cast v9, LX/2dj;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v10

    check-cast v10, LX/2do;

    invoke-static {v0}, LX/2dp;->b(LX/0QB;)LX/2dp;

    move-result-object v11

    check-cast v11, LX/2dp;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v12

    check-cast v12, LX/1vg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v13}, LX/JWU;-><init>(LX/23P;LX/17Q;LX/0Zb;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/JWD;LX/2dj;LX/2do;LX/2dp;LX/1vg;Landroid/content/res/Resources;)V

    .line 2703023
    move-object v0, v3

    .line 2703024
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703025
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703026
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/JWU;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;LX/JWT;)V
    .locals 2

    .prologue
    .line 2703028
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    .line 2703029
    sget-object v1, LX/JWT;->ADD_FRIEND:LX/JWT;

    if-ne p3, v1, :cond_0

    .line 2703030
    invoke-static {v0}, LX/17Q;->b(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2703031
    :goto_0
    iget-object v1, p0, LX/JWU;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2703032
    return-void

    .line 2703033
    :cond_0
    sget-object v1, LX/JWT;->REMOVE:LX/JWT;

    if-ne p3, v1, :cond_1

    .line 2703034
    invoke-static {v0}, LX/17Q;->f(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0

    .line 2703035
    :cond_1
    invoke-static {v0}, LX/17Q;->c(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;LX/JWT;LX/1Pc;Ljava/lang/String;LX/3mj;)V
    .locals 15
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/JWT;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/3mj;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;",
            "LX/JWT;",
            "TE;",
            "Ljava/lang/String;",
            "LX/3mj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2703036
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {p0, v2, v0, v1}, LX/JWU;->a(LX/JWU;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;LX/JWT;)V

    .line 2703037
    sget-object v2, LX/JWT;->REMOVE:LX/JWT;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_1

    .line 2703038
    iget-object v2, p0, LX/JWU;->f:LX/2dj;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/2dj;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2703039
    iget-object v2, p0, LX/JWU;->g:LX/2do;

    new-instance v3, LX/2iF;

    invoke-static/range {p5 .. p5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, LX/2iF;-><init>(J)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2703040
    iget-object v3, p0, LX/JWU;->d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    move-result-object v2

    .line 2703041
    if-eqz v2, :cond_0

    .line 2703042
    iget-object v3, p0, LX/JWU;->e:LX/JWD;

    invoke-virtual {v3, v2}, LX/JWD;->c(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    .line 2703043
    :cond_0
    :goto_0
    return-void

    .line 2703044
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->g()Ljava/lang/String;

    move-result-object v2

    .line 2703045
    new-instance v5, LX/JWN;

    const/4 v3, 0x0

    invoke-direct {v5, v2, v3}, LX/JWN;-><init>(Ljava/lang/String;Z)V

    .line 2703046
    sget-object v2, LX/JWT;->UNDO:LX/JWT;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_2

    const/4 v6, 0x1

    .line 2703047
    :goto_1
    iget-object v14, p0, LX/JWU;->h:LX/2dp;

    sget-object v8, LX/2iQ;->FEED:LX/2iQ;

    if-nez v6, :cond_3

    const/4 v9, 0x1

    :goto_2
    invoke-static/range {p5 .. p5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->a()Ljava/lang/String;

    move-result-object v13

    new-instance v2, LX/JWS;

    move-object v3, p0

    move-object/from16 v4, p4

    move-object/from16 v7, p1

    invoke-direct/range {v2 .. v7}, LX/JWS;-><init>(LX/JWU;LX/1Pc;LX/JWN;ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v7, v14

    move-object v14, v2

    invoke-virtual/range {v7 .. v14}, LX/2dp;->a(LX/2iQ;ZJLjava/lang/String;Ljava/lang/String;LX/84Y;)V

    move-object/from16 v2, p4

    .line 2703048
    check-cast v2, LX/1Pr;

    if-nez v6, :cond_4

    const/4 v3, 0x1

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v5, v3}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2703049
    if-nez v6, :cond_5

    if-eqz p6, :cond_5

    .line 2703050
    const/4 v2, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, LX/3mj;->a(Z)V

    goto :goto_0

    .line 2703051
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 2703052
    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 2703053
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 2703054
    :cond_5
    check-cast p4, LX/1Pq;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method
