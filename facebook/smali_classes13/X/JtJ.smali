.class public final LX/JtJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/stickers/model/Sticker;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2746552
    iput-object p1, p0, LX/JtJ;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iput-object p2, p0, LX/JtJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/JtJ;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746553
    sget-object v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v1, "Sticker request failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746554
    iget-object v0, p0, LX/JtJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746555
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746556
    check-cast p1, Lcom/facebook/stickers/model/Sticker;

    .line 2746557
    invoke-static {p1}, LX/4m9;->a(Lcom/facebook/stickers/model/Sticker;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2746558
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    .line 2746559
    :goto_0
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    move-object v0, v0

    .line 2746560
    if-nez v0, :cond_0

    .line 2746561
    iget-object v0, p0, LX/JtJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746562
    :goto_1
    return-void

    .line 2746563
    :cond_0
    iget-object v1, p0, LX/JtJ;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    invoke-static {v1, v0}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a$redex0(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2746564
    new-instance v1, LX/JtI;

    invoke-direct {v1, p0}, LX/JtI;-><init>(LX/JtJ;)V

    iget-object v2, p0, LX/JtJ;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v2, v2, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2746565
    :cond_1
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    goto :goto_0

    .line 2746566
    :cond_2
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    goto :goto_0
.end method
