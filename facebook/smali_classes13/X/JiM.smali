.class public final LX/JiM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JiN;


# direct methods
.method public constructor <init>(LX/JiN;)V
    .locals 0

    .prologue
    .line 2725897
    iput-object p1, p0, LX/JiM;->a:LX/JiN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x725d249a

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2725898
    iget-object v1, p0, LX/JiM;->a:LX/JiN;

    iget-object v1, v1, LX/JiN;->e:LX/0Zb;

    const-string v2, "invite_friends_upsell_not_now"

    invoke-interface {v1, v2, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2725899
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2725900
    const-string v2, "messenger_people_tab_invite_friends_upsell"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2725901
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2725902
    :cond_0
    iget-object v1, p0, LX/JiM;->a:LX/JiN;

    iget-object v1, v1, LX/JiN;->d:LX/Ifj;

    .line 2725903
    iget-object v5, v1, LX/Ifj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/Ifk;->b:LX/0Tn;

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v5

    .line 2725904
    iget-object v6, v1, LX/Ifj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, LX/Ifk;->a:LX/0Tn;

    iget-object v8, v1, LX/Ifj;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    invoke-interface {v6, v7, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    sget-object v7, LX/Ifk;->b:LX/0Tn;

    add-int/lit8 v5, v5, 0x1

    invoke-interface {v6, v7, v5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2725905
    iget-object v1, p0, LX/JiM;->a:LX/JiN;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/JiN;->setVisibility(I)V

    .line 2725906
    const v1, -0x29eac4f7

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
