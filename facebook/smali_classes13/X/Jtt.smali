.class public LX/Jtt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5Y;


# instance fields
.field private final a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

.field private final b:Ljava/lang/String;

.field public final c:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

.field public final d:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

.field private final f:LX/15i;

.field private final g:I

.field private final h:LX/Jts;

.field private final i:LX/Jtr;


# direct methods
.method public constructor <init>(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;Ljava/lang/String;Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2747616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2747617
    iput-object p1, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747618
    iput-object p2, p0, LX/Jtt;->b:Ljava/lang/String;

    .line 2747619
    iput-object p3, p0, LX/Jtt;->c:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    .line 2747620
    iput-object p4, p0, LX/Jtt;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2747621
    iput-object p5, p0, LX/Jtt;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    .line 2747622
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p6, p0, LX/Jtt;->f:LX/15i;

    iput p7, p0, LX/Jtt;->g:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2747623
    new-instance v0, LX/Jts;

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->e()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Jts;-><init>(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;)V

    iput-object v0, p0, LX/Jtt;->h:LX/Jts;

    .line 2747624
    new-instance v0, LX/Jtr;

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->d()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Jtr;-><init>(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;)V

    iput-object v0, p0, LX/Jtt;->i:LX/Jtr;

    .line 2747625
    return-void

    .line 2747626
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747627
    iget-object v0, p0, LX/Jtt;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2747615
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747614
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineProfile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2747613
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()LX/1vs;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacyScope"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747612
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/Jtt;->f:LX/15i;

    iget v2, p0, LX/Jtt;->g:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v2}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final iP_()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747611
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iQ_()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747610
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->b()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747609
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->c()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/B5W;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747628
    iget-object v0, p0, LX/Jtt;->i:LX/Jtr;

    return-object v0
.end method

.method public final l()LX/B5X;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747608
    iget-object v0, p0, LX/Jtt;->h:LX/Jts;

    return-object v0
.end method

.method public final m()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747607
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->no_()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/8Yp;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747606
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->nn_()LX/8Yp;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747605
    const/4 v0, 0x0

    return-object v0
.end method

.method public final p()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747604
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->j()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final q()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747603
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->k()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747602
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()J
    .locals 2

    .prologue
    .line 2747600
    iget-object v0, p0, LX/Jtt;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747601
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->LEFT_TO_RIGHT:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    return-object v0
.end method
