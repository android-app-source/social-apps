.class public LX/Jot;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Jot;


# instance fields
.field private final a:LX/Jop;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Jop;LX/0Ot;LX/0pq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jop;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0pq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2736337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736338
    iput-object p1, p0, LX/Jot;->a:LX/Jop;

    .line 2736339
    iput-object p2, p0, LX/Jot;->b:LX/0Ot;

    .line 2736340
    invoke-virtual {p3, p0}, LX/0pq;->a(LX/0po;)V

    .line 2736341
    return-void
.end method

.method public static a(LX/0QB;)LX/Jot;
    .locals 6

    .prologue
    .line 2736324
    sget-object v0, LX/Jot;->c:LX/Jot;

    if-nez v0, :cond_1

    .line 2736325
    const-class v1, LX/Jot;

    monitor-enter v1

    .line 2736326
    :try_start_0
    sget-object v0, LX/Jot;->c:LX/Jot;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2736327
    if-eqz v2, :cond_0

    .line 2736328
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2736329
    new-instance v5, LX/Jot;

    invoke-static {v0}, LX/Jop;->a(LX/0QB;)LX/Jop;

    move-result-object v3

    check-cast v3, LX/Jop;

    const/16 v4, 0x2ba

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v4

    check-cast v4, LX/0pq;

    invoke-direct {v5, v3, p0, v4}, LX/Jot;-><init>(LX/Jop;LX/0Ot;LX/0pq;)V

    .line 2736330
    move-object v0, v5

    .line 2736331
    sput-object v0, LX/Jot;->c:LX/Jot;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2736332
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2736333
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2736334
    :cond_1
    sget-object v0, LX/Jot;->c:LX/Jot;

    return-object v0

    .line 2736335
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2736336
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized U_()V
    .locals 1

    .prologue
    .line 2736321
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/Jot;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2736322
    monitor-exit p0

    return-void

    .line 2736323
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)LX/0Px;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "I)",
            "LX/0Px",
            "<",
            "LX/DiN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2736236
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Jot;->a:LX/Jop;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2736237
    const v2, -0x5a720293

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2736238
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2736239
    const/4 v10, 0x0

    .line 2736240
    :try_start_0
    sget-object v2, LX/Joq;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 2736241
    const-string v2, "actions"

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v6, LX/Joq;->b:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x1

    sget-object v6, LX/Joq;->c:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x2

    sget-object v6, LX/Joq;->d:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x3

    sget-object v6, LX/Joq;->e:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x4

    sget-object v6, LX/Joq;->b:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x5

    sget-object v6, LX/Joq;->f:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x6

    sget-object v6, LX/Joq;->g:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x7

    sget-object v6, LX/Joq;->h:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/16 v4, 0x8

    sget-object v6, LX/Joq;->i:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/Joq;->a:LX/0U1;

    invoke-virtual {v9}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " DESC"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 2736242
    :try_start_1
    sget-object v2, LX/Joq;->b:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 2736243
    sget-object v2, LX/Joq;->c:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 2736244
    sget-object v2, LX/Joq;->d:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v14

    .line 2736245
    sget-object v2, LX/Joq;->e:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v15

    .line 2736246
    sget-object v2, LX/Joq;->f:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v16

    .line 2736247
    sget-object v2, LX/Joq;->g:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v17

    .line 2736248
    sget-object v2, LX/Joq;->h:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v18

    .line 2736249
    sget-object v2, LX/Joq;->i:LX/0U1;

    invoke-virtual {v2, v10}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v19

    .line 2736250
    :cond_0
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2736251
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2736252
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2736253
    invoke-interface {v10, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2736254
    invoke-interface {v10, v15}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    .line 2736255
    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 2736256
    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2736257
    move/from16 v0, v18

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 2736258
    move/from16 v0, v19

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 2736259
    const/4 v9, 0x0

    .line 2736260
    invoke-static/range {v21 .. v21}, LX/0YN;->a(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-nez v6, :cond_1

    .line 2736261
    :try_start_2
    move-object/from16 v0, p0

    iget-object v6, v0, LX/Jot;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0lC;

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    .line 2736262
    :cond_1
    :try_start_3
    invoke-static/range {v20 .. v20}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v6

    invoke-static/range {v2 .. v9}, LX/DiN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;ILX/0lF;)LX/DiN;

    move-result-object v2

    .line 2736263
    if-eqz v2, :cond_0

    .line 2736264
    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2736265
    :catchall_0
    move-exception v2

    move-object v3, v10

    :goto_1
    if-eqz v3, :cond_2

    .line 2736266
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2736267
    :cond_2
    const v3, -0x4be1e391

    invoke-static {v1, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v2

    .line 2736268
    :catch_0
    move-exception v2

    .line 2736269
    :try_start_4
    const-string v3, "OmniMDbStorage"

    const-string v4, "Could not parse action data: %s. Exception: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v21, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2736270
    :cond_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2736271
    if-eqz v10, :cond_4

    .line 2736272
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 2736273
    :cond_4
    const v2, -0x392d7c55

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2736274
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    return-object v1

    .line 2736275
    :catchall_1
    move-exception v2

    move-object v3, v10

    goto :goto_1
.end method

.method public final declared-synchronized a(LX/DiN;)V
    .locals 5

    .prologue
    .line 2736279
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jot;->a:LX/Jop;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2736280
    const v0, 0x6c0f54e7

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2736281
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2736282
    sget-object v2, LX/Joq;->b:LX/0U1;

    .line 2736283
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736284
    iget-object v3, p1, LX/DiN;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2736285
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736286
    sget-object v2, LX/Joq;->c:LX/0U1;

    .line 2736287
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736288
    iget-object v3, p1, LX/DiN;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2736289
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736290
    sget-object v2, LX/Joq;->d:LX/0U1;

    .line 2736291
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736292
    iget-object v3, p1, LX/DiN;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2736293
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736294
    sget-object v2, LX/Joq;->e:LX/0U1;

    .line 2736295
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736296
    iget v3, p1, LX/DiN;->d:F

    move v3, v3

    .line 2736297
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2736298
    sget-object v2, LX/Joq;->f:LX/0U1;

    .line 2736299
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736300
    iget-object v3, p1, LX/DiN;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v3

    .line 2736301
    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736302
    sget-object v2, LX/Joq;->g:LX/0U1;

    .line 2736303
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736304
    iget-object v3, p1, LX/DiN;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2736305
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736306
    sget-object v2, LX/Joq;->h:LX/0U1;

    .line 2736307
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2736308
    iget-object v3, p1, LX/DiN;->g:LX/DiO;

    move-object v3, v3

    .line 2736309
    invoke-virtual {v3}, LX/DiO;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2736310
    iget-object v2, p1, LX/DiN;->h:LX/DiR;

    move-object v2, v2

    .line 2736311
    if-eqz v2, :cond_0

    .line 2736312
    sget-object v3, LX/Joq;->i:LX/0U1;

    .line 2736313
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2736314
    invoke-interface {v2}, LX/DiR;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736315
    :cond_0
    const-string v2, "actions"

    const/4 v3, 0x0

    const v4, -0x5d5f90e6

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x346069c9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2736316
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2736317
    const v0, 0x3344861

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2736318
    monitor-exit p0

    return-void

    .line 2736319
    :catchall_0
    move-exception v0

    const v2, 0x5989a27a

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2736320
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 2736276
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jot;->a:LX/Jop;

    invoke-virtual {v0}, LX/0Tr;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2736277
    monitor-exit p0

    return-void

    .line 2736278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
