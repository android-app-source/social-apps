.class public LX/Jz5;
.super LX/Jyx;
.source ""


# instance fields
.field private final g:[D

.field private final h:[D

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private k:LX/Jyx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pG;)V
    .locals 1

    .prologue
    .line 2755731
    invoke-direct {p0}, LX/Jyx;-><init>()V

    .line 2755732
    const-string v0, "inputRange"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-static {v0}, LX/Jz5;->a(LX/5pC;)[D

    move-result-object v0

    iput-object v0, p0, LX/Jz5;->g:[D

    .line 2755733
    const-string v0, "outputRange"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-static {v0}, LX/Jz5;->a(LX/5pC;)[D

    move-result-object v0

    iput-object v0, p0, LX/Jz5;->h:[D

    .line 2755734
    const-string v0, "extrapolateLeft"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Jz5;->i:Ljava/lang/String;

    .line 2755735
    const-string v0, "extrapolateRight"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Jz5;->j:Ljava/lang/String;

    .line 2755736
    return-void
.end method

.method private static a(DDDDDLjava/lang/String;Ljava/lang/String;)D
    .locals 4

    .prologue
    .line 2755737
    cmpg-double v0, p0, p2

    if-gez v0, :cond_3

    .line 2755738
    const/4 v0, -0x1

    invoke-virtual {p10}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2755739
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid extrapolation type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "for left extrapolation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755740
    :sswitch_0
    const-string v1, "identity"

    invoke-virtual {p10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "clamp"

    invoke-virtual {p10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "extend"

    invoke-virtual {p10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :pswitch_0
    move-wide v0, p2

    .line 2755741
    :goto_1
    cmpl-double v2, v0, p4

    if-lez v2, :cond_2

    .line 2755742
    const/4 v2, -0x1

    invoke-virtual {p11}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_1

    :cond_1
    :goto_2
    packed-switch v2, :pswitch_data_1

    .line 2755743
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid extrapolation type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "for right extrapolation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move-wide v0, p0

    .line 2755744
    goto :goto_1

    .line 2755745
    :sswitch_3
    const-string v3, "identity"

    invoke-virtual {p11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_4
    const-string v3, "clamp"

    invoke-virtual {p11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_5
    const-string v3, "extend"

    invoke-virtual {p11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x2

    goto :goto_2

    :pswitch_2
    move-wide p0, v0

    .line 2755746
    :goto_3
    :pswitch_3
    return-wide p0

    :pswitch_4
    move-wide v0, p4

    :cond_2
    :pswitch_5
    sub-double v2, p8, p6

    sub-double/2addr v0, p2

    mul-double/2addr v0, v2

    sub-double v2, p4, p2

    div-double/2addr v0, v2

    add-double p0, p6, v0

    goto :goto_3

    :cond_3
    move-wide v0, p0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x4cd540e6 -> :sswitch_2
        -0x8178f42 -> :sswitch_0
        0x5a5a8bb -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x4cd540e6 -> :sswitch_5
        -0x8178f42 -> :sswitch_3
        0x5a5a8bb -> :sswitch_4
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(D[D[DLjava/lang/String;Ljava/lang/String;)D
    .locals 12

    .prologue
    .line 2755747
    invoke-static {p0, p1, p2}, LX/Jz5;->a(D[D)I

    move-result v0

    .line 2755748
    aget-wide v2, p2, v0

    add-int/lit8 v1, v0, 0x1

    aget-wide v4, p2, v1

    aget-wide v6, p3, v0

    add-int/lit8 v0, v0, 0x1

    aget-wide v8, p3, v0

    move-wide v0, p0

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-static/range {v0 .. v11}, LX/Jz5;->a(DDDDDLjava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method private static a(D[D)I
    .locals 4

    .prologue
    .line 2755749
    const/4 v0, 0x1

    :goto_0
    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 2755750
    aget-wide v2, p2, v0

    cmpl-double v1, v2, p0

    if-gez v1, :cond_0

    .line 2755751
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2755752
    :cond_0
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private static a(LX/5pC;)[D
    .locals 4

    .prologue
    .line 2755753
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v0

    new-array v1, v0, [D

    .line 2755754
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2755755
    invoke-interface {p0, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 2755756
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2755757
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2755758
    iget-object v0, p0, LX/Jz5;->k:LX/Jyx;

    if-nez v0, :cond_0

    .line 2755759
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to update interpolation node that has not been attached to the parent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755760
    :cond_0
    iget-object v0, p0, LX/Jz5;->k:LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->b()D

    move-result-wide v0

    iget-object v2, p0, LX/Jz5;->g:[D

    iget-object v3, p0, LX/Jz5;->h:[D

    iget-object v4, p0, LX/Jz5;->i:Ljava/lang/String;

    iget-object v5, p0, LX/Jz5;->j:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, LX/Jz5;->a(D[D[DLjava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz5;->e:D

    .line 2755761
    return-void
.end method

.method public final c(LX/Jyw;)V
    .locals 2

    .prologue
    .line 2755762
    iget-object v0, p0, LX/Jz5;->k:LX/Jyx;

    if-eqz v0, :cond_0

    .line 2755763
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Parent already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755764
    :cond_0
    instance-of v0, p1, LX/Jyx;

    if-nez v0, :cond_1

    .line 2755765
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parent is of an invalid type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755766
    :cond_1
    check-cast p1, LX/Jyx;

    iput-object p1, p0, LX/Jz5;->k:LX/Jyx;

    .line 2755767
    return-void
.end method

.method public final d(LX/Jyw;)V
    .locals 2

    .prologue
    .line 2755768
    iget-object v0, p0, LX/Jz5;->k:LX/Jyx;

    if-eq p1, v0, :cond_0

    .line 2755769
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid parent node provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755770
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jz5;->k:LX/Jyx;

    .line 2755771
    return-void
.end method
