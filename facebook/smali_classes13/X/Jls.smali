.class public final LX/Jls;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JlW;

.field public final synthetic b:LX/Jlx;


# direct methods
.method public constructor <init>(LX/Jlx;LX/JlW;)V
    .locals 0

    .prologue
    .line 2731927
    iput-object p1, p0, LX/Jls;->b:LX/Jlx;

    iput-object p2, p0, LX/Jls;->a:LX/JlW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2731928
    iget-object v0, p0, LX/Jls;->a:LX/JlW;

    iget-object v0, v0, LX/JlW;->b:LX/JlY;

    sget-object v1, LX/JlY;->ALL:LX/JlY;

    if-ne v0, v1, :cond_0

    .line 2731929
    iget-object v0, p0, LX/Jls;->b:LX/Jlx;

    iget-object v0, v0, LX/Jlx;->g:LX/0Uh;

    const/16 v1, 0x166

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2731930
    iget-object v0, p0, LX/Jls;->b:LX/Jlx;

    iget-object v0, v0, LX/Jlx;->c:LX/0kd;

    invoke-virtual {v0}, LX/0kd;->b()V

    .line 2731931
    :cond_0
    iget-object v0, p0, LX/Jls;->b:LX/Jlx;

    iget-object v1, p0, LX/Jls;->a:LX/JlW;

    invoke-static {v0, v1}, LX/Jlx;->b(LX/Jlx;LX/JlW;)LX/Jlv;

    move-result-object v0

    .line 2731932
    return-object v0
.end method
