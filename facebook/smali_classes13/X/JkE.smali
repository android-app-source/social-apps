.class public final LX/JkE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2729511
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2729512
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2729513
    :goto_0
    return v1

    .line 2729514
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2729515
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2729516
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2729517
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2729518
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2729519
    const-string v5, "action_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2729520
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 2729521
    :cond_2
    const-string v5, "contact"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2729522
    invoke-static {p0, p1}, LX/JkC;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2729523
    :cond_3
    const-string v5, "item_description"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2729524
    invoke-static {p0, p1}, LX/JkD;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2729525
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2729526
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2729527
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2729528
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2729529
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2729496
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2729497
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2729498
    if-eqz v0, :cond_0

    .line 2729499
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2729500
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2729501
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2729502
    if-eqz v0, :cond_1

    .line 2729503
    const-string v1, "contact"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2729504
    invoke-static {p0, v0, p2, p3}, LX/JkC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2729505
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2729506
    if-eqz v0, :cond_2

    .line 2729507
    const-string v1, "item_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2729508
    invoke-static {p0, v0, p2}, LX/JkD;->a(LX/15i;ILX/0nX;)V

    .line 2729509
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2729510
    return-void
.end method
