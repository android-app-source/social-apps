.class public LX/JWY;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mW",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final e:LX/JWn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;LX/1Pc;LX/25M;LX/JWD;LX/JWn;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;",
            ">;TE;",
            "LX/25M;",
            "LX/JWD;",
            "LX/JWn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703132
    new-instance v3, LX/JWf;

    .line 2703133
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2703134
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-direct {v3, p6, v0}, LX/JWf;-><init>(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    move-object v4, p4

    check-cast v4, LX/1Pq;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 2703135
    iput-object p4, p0, LX/JWY;->d:LX/1Pc;

    .line 2703136
    iput-object p2, p0, LX/JWY;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703137
    iput-object p7, p0, LX/JWY;->e:LX/JWn;

    .line 2703138
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2703139
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2703140
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 2703141
    iget-object v0, p0, LX/JWY;->e:LX/JWn;

    const/4 v1, 0x0

    .line 2703142
    new-instance v2, LX/JWm;

    invoke-direct {v2, v0}, LX/JWm;-><init>(LX/JWn;)V

    .line 2703143
    iget-object v3, v0, LX/JWn;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JWl;

    .line 2703144
    if-nez v3, :cond_0

    .line 2703145
    new-instance v3, LX/JWl;

    invoke-direct {v3, v0}, LX/JWl;-><init>(LX/JWn;)V

    .line 2703146
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JWl;->a$redex0(LX/JWl;LX/1De;IILX/JWm;)V

    .line 2703147
    move-object v2, v3

    .line 2703148
    move-object v1, v2

    .line 2703149
    move-object v0, v1

    .line 2703150
    iget-object v1, p0, LX/JWY;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703151
    iget-object v2, v0, LX/JWl;->a:LX/JWm;

    iput-object v1, v2, LX/JWm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703152
    iget-object v2, v0, LX/JWl;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2703153
    move-object v0, v0

    .line 2703154
    iget-object v1, v0, LX/JWl;->a:LX/JWm;

    iput-object p2, v1, LX/JWm;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 2703155
    iget-object v1, v0, LX/JWl;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2703156
    move-object v0, v0

    .line 2703157
    iget-object v1, p0, LX/JWY;->d:LX/1Pc;

    .line 2703158
    iget-object v2, v0, LX/JWl;->a:LX/JWm;

    iput-object v1, v2, LX/JWm;->c:LX/1Pc;

    .line 2703159
    iget-object v2, v0, LX/JWl;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2703160
    move-object v0, v0

    .line 2703161
    iget-object v1, p0, LX/3mX;->l:LX/3mj;

    move-object v1, v1

    .line 2703162
    iget-object v2, v0, LX/JWl;->a:LX/JWm;

    iput-object v1, v2, LX/JWm;->d:LX/3mj;

    .line 2703163
    iget-object v2, v0, LX/JWl;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2703164
    move-object v0, v0

    .line 2703165
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2703166
    const/4 v0, 0x0

    return v0
.end method
