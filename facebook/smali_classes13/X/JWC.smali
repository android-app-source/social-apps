.class public final LX/JWC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JWD;


# direct methods
.method public constructor <init>(LX/JWD;)V
    .locals 0

    .prologue
    .line 2702536
    iput-object p1, p0, LX/JWC;->a:LX/JWD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2702537
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2702538
    if-eqz p1, :cond_0

    .line 2702539
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2702540
    if-nez v0, :cond_1

    .line 2702541
    :cond_0
    const/4 v0, 0x0

    .line 2702542
    :goto_0
    return-object v0

    .line 2702543
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2702544
    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;

    invoke-static {v0}, LX/86R;->a(Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->E()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    goto :goto_0
.end method
