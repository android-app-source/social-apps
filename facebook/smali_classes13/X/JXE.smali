.class public final LX/JXE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2704171
    iput-object p1, p0, LX/JXE;->c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    iput-object p2, p0, LX/JXE;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iput-object p3, p0, LX/JXE;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const v0, -0x51b51fd

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2704172
    iget-object v0, p0, LX/JXE;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m()Ljava/lang/String;

    move-result-object v0

    .line 2704173
    iget-object v2, p0, LX/JXE;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v2

    .line 2704174
    iget-object v3, p0, LX/JXE;->c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    const/4 v8, 0x0

    const/4 v13, 0x1

    .line 2704175
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 2704176
    new-instance v6, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    move-object v7, v2

    move-object v10, v0

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;)V

    .line 2704177
    const-string v7, "submitResearchPollResponseParamsKey"

    invoke-virtual {v12, v7, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2704178
    iget-object v6, v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a:LX/0aG;

    const-string v7, "feed_submit_research_poll_response"

    sget-object v9, LX/1ME;->BY_EXCEPTION:LX/1ME;

    iget-object v10, v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v11, 0x5f189f13

    move-object v8, v12

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6, v13}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    .line 2704179
    iget-object v0, p0, LX/JXE;->c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    const-string v3, "open_poll"

    invoke-virtual {v0, v2, v3}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2704180
    iget-object v0, p0, LX/JXE;->b:LX/1Pq;

    check-cast v0, LX/1Pr;

    new-instance v2, LX/JXD;

    iget-object v3, p0, LX/JXE;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-direct {v2, v3}, LX/JXD;-><init>(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    iget-object v3, p0, LX/JXE;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-interface {v0, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JXB;

    .line 2704181
    iput-boolean v4, v0, LX/JXB;->g:Z

    .line 2704182
    iget-object v0, p0, LX/JXE;->b:LX/1Pq;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/JXE;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2704183
    const v0, -0x547ae562

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
