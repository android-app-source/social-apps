.class public LX/JqU;
.super LX/7GD;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/JqU;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Uh;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JrY;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/7GW;

.field public final g:LX/7GS;

.field public final h:LX/6hN;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739158
    const-class v0, LX/JqU;

    sput-object v0, LX/JqU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2Hu;LX/01T;LX/7GA;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Or;LX/7GW;LX/7GS;LX/6hN;LX/0Or;LX/0Or;)V
    .locals 9
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/sync/annotations/MessagesSyncApiVersion;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/JrY;",
            ">;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/01T;",
            "LX/7GA;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;",
            "LX/7GW;",
            "LX/7GS;",
            "LX/6hN;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739159
    move-object v1, p0

    move-object v2, p6

    move-object/from16 v3, p7

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p13

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, LX/7GD;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2Hu;LX/01T;LX/0Or;LX/7GA;LX/0Ot;)V

    .line 2739160
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/JqU;->k:Z

    .line 2739161
    const/high16 v1, -0x80000000

    iput v1, p0, LX/JqU;->l:I

    .line 2739162
    iput-object p6, p0, LX/JqU;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2739163
    move-object/from16 v0, p7

    iput-object v0, p0, LX/JqU;->c:LX/0Uh;

    .line 2739164
    move-object/from16 v0, p8

    iput-object v0, p0, LX/JqU;->d:LX/0Or;

    .line 2739165
    iput-object p1, p0, LX/JqU;->e:LX/0Or;

    .line 2739166
    move-object/from16 v0, p9

    iput-object v0, p0, LX/JqU;->f:LX/7GW;

    .line 2739167
    move-object/from16 v0, p10

    iput-object v0, p0, LX/JqU;->g:LX/7GS;

    .line 2739168
    move-object/from16 v0, p11

    iput-object v0, p0, LX/JqU;->h:LX/6hN;

    .line 2739169
    move-object/from16 v0, p12

    iput-object v0, p0, LX/JqU;->i:LX/0Or;

    .line 2739170
    move-object/from16 v0, p13

    iput-object v0, p0, LX/JqU;->j:LX/0Or;

    .line 2739171
    return-void
.end method

.method private static a(LX/0m9;Ljava/lang/String;)LX/0m9;
    .locals 2

    .prologue
    .line 2739172
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    .line 2739173
    if-eqz v0, :cond_0

    .line 2739174
    :goto_0
    return-object v0

    .line 2739175
    :cond_0
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2739176
    invoke-virtual {p0, p1, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/JqU;
    .locals 3

    .prologue
    .line 2739177
    sget-object v0, LX/JqU;->m:LX/JqU;

    if-nez v0, :cond_1

    .line 2739178
    const-class v1, LX/JqU;

    monitor-enter v1

    .line 2739179
    :try_start_0
    sget-object v0, LX/JqU;->m:LX/JqU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2739180
    if-eqz v2, :cond_0

    .line 2739181
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/JqU;->b(LX/0QB;)LX/JqU;

    move-result-object v0

    sput-object v0, LX/JqU;->m:LX/JqU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2739182
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2739183
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2739184
    :cond_1
    sget-object v0, LX/JqU;->m:LX/JqU;

    return-object v0

    .line 2739185
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2739186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/JqU;
    .locals 14

    .prologue
    .line 2739187
    new-instance v0, LX/JqU;

    const/16 v1, 0x29fb

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v2

    check-cast v2, LX/2Hu;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {p0}, LX/7GA;->a(LX/0QB;)LX/7GA;

    move-result-object v4

    check-cast v4, LX/7GA;

    const/16 v5, 0x259

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const/16 v8, 0x2a01

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/7GW;->a(LX/0QB;)LX/7GW;

    move-result-object v9

    check-cast v9, LX/7GW;

    invoke-static {p0}, LX/7GS;->a(LX/0QB;)LX/7GS;

    move-result-object v10

    check-cast v10, LX/7GS;

    invoke-static {p0}, LX/6hN;->a(LX/0QB;)LX/6hN;

    move-result-object v11

    check-cast v11, LX/6hN;

    const/16 v12, 0x15d4

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x15e8

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/JqU;-><init>(LX/0Or;LX/2Hu;LX/01T;LX/7GA;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Or;LX/7GW;LX/7GS;LX/6hN;LX/0Or;LX/0Or;)V

    .line 2739188
    return-object v0
.end method

.method private d(LX/0m9;)V
    .locals 2

    .prologue
    .line 2739189
    invoke-direct {p0}, LX/JqU;->e()LX/0m9;

    move-result-object v0

    .line 2739190
    invoke-virtual {v0}, LX/0m9;->hashCode()I

    move-result v1

    iput v1, p0, LX/JqU;->l:I

    .line 2739191
    const-string v1, "queue_params"

    invoke-virtual {p1, v1, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2739192
    return-void
.end method

.method private e()LX/0m9;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2739193
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2739194
    iget-object v0, p0, LX/JqU;->c:LX/0Uh;

    const/16 v2, 0x1c5

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2739195
    const-string v2, "client_delta_sync_bitmask"

    iget-object v0, p0, LX/JqU;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrY;

    invoke-virtual {v0}, LX/JrY;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2739196
    :cond_0
    invoke-direct {p0, v1}, LX/JqU;->e(LX/0m9;)V

    .line 2739197
    iget-object v0, p0, LX/JqU;->c:LX/0Uh;

    const/16 v2, 0x1ea

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2739198
    :goto_0
    return-object v1

    .line 2739199
    :cond_1
    invoke-static {v1}, LX/JqU;->h(LX/0m9;)LX/0m9;

    move-result-object v2

    .line 2739200
    invoke-static {v1}, LX/JqU;->i(LX/0m9;)LX/0m9;

    move-result-object v3

    .line 2739201
    iget-object v0, p0, LX/JqU;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2739202
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2739203
    const/16 v5, 0x14

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0Px;ILjava/lang/Boolean;)LX/5ZA;

    move-result-object v0

    .line 2739204
    iget-object v4, v0, LX/0gW;->h:Ljava/lang/String;

    move-object v4, v4

    .line 2739205
    const-string v5, "threads_query_id"

    invoke-virtual {v2, v5, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2739206
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v2, v2

    .line 2739207
    invoke-virtual {v2}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v2

    .line 2739208
    const-string v5, "thread_ids"

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2739209
    const-string v0, "thread_ids"

    .line 2739210
    :goto_1
    const-string v5, "<IDs>"

    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739211
    new-instance v0, LX/0lC;

    invoke-direct {v0}, LX/0lC;-><init>()V

    invoke-virtual {v0, v2}, LX/0lC;->a(Ljava/lang/Object;)LX/0lF;

    move-result-object v0

    .line 2739212
    invoke-virtual {v3, v4, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 2739213
    :cond_2
    const-string v5, "thread_ids"

    invoke-virtual {v0, v5}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private e(LX/0m9;)V
    .locals 5

    .prologue
    .line 2739134
    iget-object v0, p0, LX/JqU;->c:LX/0Uh;

    const/16 v1, 0x1f9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2739135
    :goto_0
    return-void

    .line 2739136
    :cond_0
    invoke-static {p1}, LX/JqU;->h(LX/0m9;)LX/0m9;

    move-result-object v0

    .line 2739137
    invoke-static {p1}, LX/JqU;->i(LX/0m9;)LX/0m9;

    move-result-object v1

    .line 2739138
    const-string v2, "xma_query_id"

    invoke-static {}, LX/5ZC;->h()LX/5ZB;

    move-result-object v3

    .line 2739139
    iget-object p1, v3, LX/0gW;->h:Ljava/lang/String;

    move-object v3, p1

    .line 2739140
    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2739141
    new-instance v0, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2739142
    const-string v2, "xma_id"

    const-string v3, "<ID>"

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2739143
    iget-object v2, p0, LX/JqU;->h:LX/6hN;

    invoke-virtual {v2}, LX/6hN;->h()I

    move-result v2

    .line 2739144
    iget-object v3, p0, LX/JqU;->h:LX/6hN;

    invoke-virtual {v3}, LX/6hN;->f()I

    move-result v3

    .line 2739145
    const-string v4, "small_preview_width"

    mul-int/lit8 p1, v2, 0x2

    invoke-virtual {v0, v4, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2739146
    const-string v4, "small_preview_height"

    invoke-virtual {v0, v4, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2739147
    const-string v2, "large_preview_width"

    mul-int/lit8 v4, v3, 0x2

    invoke-virtual {v0, v2, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2739148
    const-string v2, "large_preview_height"

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2739149
    invoke-static {}, LX/5ZC;->h()LX/5ZB;

    move-result-object v2

    .line 2739150
    iget-object v3, v2, LX/0gW;->h:Ljava/lang/String;

    move-object v2, v3

    .line 2739151
    invoke-virtual {v1, v2, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0
.end method

.method private g()LX/0Tn;
    .locals 2

    .prologue
    .line 2739152
    sget-object v0, LX/0db;->o:LX/0Tn;

    .line 2739153
    iget-object v1, p0, LX/JqU;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2739154
    if-nez v1, :cond_0

    .line 2739155
    const-string v1, "null"

    .line 2739156
    :cond_0
    move-object v1, v1

    .line 2739157
    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static h(LX/0m9;)LX/0m9;
    .locals 1

    .prologue
    .line 2739133
    const-string v0, "graphql_query_hashes"

    invoke-static {p0, v0}, LX/JqU;->a(LX/0m9;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/0m9;)LX/0m9;
    .locals 1

    .prologue
    .line 2739132
    const-string v0, "graphql_query_params"

    invoke-static {p0, v0}, LX/JqU;->a(LX/0m9;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/7GT;
    .locals 1

    .prologue
    .line 2739131
    sget-object v0, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    return-object v0
.end method

.method public final a(LX/0m9;)V
    .locals 0

    .prologue
    .line 2739130
    return-void
.end method

.method public final b(LX/0m9;)V
    .locals 0

    .prologue
    .line 2739128
    invoke-direct {p0, p1}, LX/JqU;->d(LX/0m9;)V

    .line 2739129
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2739126
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JqU;->k:Z

    .line 2739127
    return-void
.end method

.method public final c(LX/0m9;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2739119
    iget-boolean v0, p0, LX/JqU;->k:Z

    if-eqz v0, :cond_1

    .line 2739120
    :cond_0
    :goto_0
    return-void

    .line 2739121
    :cond_1
    iget-object v0, p0, LX/JqU;->c:LX/0Uh;

    const/16 v1, 0x1a1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2739122
    iget-object v0, p0, LX/JqU;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0}, LX/JqU;->g()LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2739123
    if-eq v0, v3, :cond_2

    .line 2739124
    invoke-direct {p0}, LX/JqU;->e()LX/0m9;

    move-result-object v1

    invoke-virtual {v1}, LX/0m9;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2739125
    :cond_2
    invoke-direct {p0, p1}, LX/JqU;->d(LX/0m9;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2739115
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JqU;->k:Z

    .line 2739116
    iget v0, p0, LX/JqU;->l:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2739117
    iget-object v0, p0, LX/JqU;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-direct {p0}, LX/JqU;->g()LX/0Tn;

    move-result-object v1

    iget v2, p0, LX/JqU;->l:I

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2739118
    :cond_0
    return-void
.end method
