.class public LX/Jy9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2754212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/Jy6;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;IIZZLandroid/content/res/Resources;)V
    .locals 5

    .prologue
    .line 2754213
    iput-object p1, p0, LX/Jy6;->p:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2754214
    iget-object v0, p0, LX/Jy6;->n:LX/JyG;

    iget-object v1, p0, LX/Jy6;->m:LX/1De;

    const/4 v2, 0x0

    .line 2754215
    new-instance v3, LX/JyF;

    invoke-direct {v3, v0}, LX/JyF;-><init>(LX/JyG;)V

    .line 2754216
    sget-object v4, LX/JyG;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JyE;

    .line 2754217
    if-nez v4, :cond_0

    .line 2754218
    new-instance v4, LX/JyE;

    invoke-direct {v4}, LX/JyE;-><init>()V

    .line 2754219
    :cond_0
    invoke-static {v4, v1, v2, v2, v3}, LX/JyE;->a$redex0(LX/JyE;LX/1De;IILX/JyF;)V

    .line 2754220
    move-object v3, v4

    .line 2754221
    move-object v2, v3

    .line 2754222
    move-object v0, v2

    .line 2754223
    iget-object v1, v0, LX/JyE;->a:LX/JyF;

    iput-object p1, v1, LX/JyF;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2754224
    iget-object v1, v0, LX/JyE;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2754225
    move-object v0, v0

    .line 2754226
    iget-object v1, v0, LX/JyE;->a:LX/JyF;

    iput-object p0, v1, LX/JyF;->b:Landroid/view/View$OnClickListener;

    .line 2754227
    iget-object v1, v0, LX/JyE;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2754228
    move-object v0, v0

    .line 2754229
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2754230
    iget-object v1, p0, LX/Jy6;->l:Lcom/facebook/components/ComponentView;

    iget-object v2, p0, LX/Jy6;->m:LX/1De;

    invoke-static {v1, v2, v0}, LX/Jy7;->b(Lcom/facebook/components/ComponentView;LX/1De;LX/1X1;)V

    .line 2754231
    iput p2, p0, LX/Jy6;->q:I

    .line 2754232
    iput p3, p0, LX/Jy6;->r:I

    .line 2754233
    iput-boolean p4, p0, LX/Jy6;->s:Z

    .line 2754234
    iget-object v0, p0, LX/Jy6;->l:Lcom/facebook/components/ComponentView;

    move-object v1, v0

    .line 2754235
    if-eqz p5, :cond_1

    const v0, 0x7f0b26d1

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    invoke-static {v1, v0}, LX/Jy9;->a(Landroid/view/View;I)V

    .line 2754236
    return-void

    .line 2754237
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2754238
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 2754239
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2754240
    :cond_0
    return-void
.end method
