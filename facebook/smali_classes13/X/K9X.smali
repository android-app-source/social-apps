.class public LX/K9X;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/K9e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public c:LX/K9d;

.field public d:Lcom/facebook/widget/CustomViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Calendar;)V
    .locals 5

    .prologue
    .line 2777603
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2777604
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2777605
    const-class v0, LX/K9X;

    invoke-static {v0, p0}, LX/K9X;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2777606
    const v0, 0x7f0303d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2777607
    invoke-virtual {p0, v1}, LX/K9X;->setOrientation(I)V

    .line 2777608
    const v0, 0x7f0d0bf1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, LX/K9X;->d:Lcom/facebook/widget/CustomViewPager;

    .line 2777609
    iget-object v0, p0, LX/K9X;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/widget/CustomViewPager;->b(IZ)V

    .line 2777610
    const v0, 0x7f0d0bf0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, LX/K9X;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2777611
    iget-object v1, p0, LX/K9X;->a:LX/K9e;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 2777612
    :goto_0
    new-instance v4, LX/K9d;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 p1, 0x1617

    invoke-static {v1, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    const/16 p2, 0x161a

    invoke-static {v1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-direct {v4, v0, v3, p1, p2}, LX/K9d;-><init>(Ljava/util/Calendar;Landroid/content/Context;LX/0Or;LX/0Or;)V

    .line 2777613
    move-object v0, v4

    .line 2777614
    iput-object v0, p0, LX/K9X;->c:LX/K9d;

    .line 2777615
    iget-object v1, p0, LX/K9X;->c:LX/K9d;

    iget-object v0, p0, LX/K9X;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    .line 2777616
    iput-object v0, v1, LX/K9d;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    .line 2777617
    iget-object v0, p0, LX/K9X;->d:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/K9X;->c:LX/K9d;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2777618
    iget-object v0, p0, LX/K9X;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, LX/K9X;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2777619
    return-void

    .line 2777620
    :cond_0
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/K9X;

    const-class p0, LX/K9e;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/K9e;

    iput-object v1, p1, LX/K9X;->a:LX/K9e;

    return-void
.end method
