.class public LX/JWa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JWi;


# direct methods
.method public constructor <init>(LX/JWi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703221
    iput-object p1, p0, LX/JWa;->a:LX/JWi;

    .line 2703222
    return-void
.end method

.method public static a(LX/0QB;)LX/JWa;
    .locals 4

    .prologue
    .line 2703209
    const-class v1, LX/JWa;

    monitor-enter v1

    .line 2703210
    :try_start_0
    sget-object v0, LX/JWa;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703211
    sput-object v2, LX/JWa;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703212
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703213
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703214
    new-instance p0, LX/JWa;

    invoke-static {v0}, LX/JWi;->a(LX/0QB;)LX/JWi;

    move-result-object v3

    check-cast v3, LX/JWi;

    invoke-direct {p0, v3}, LX/JWa;-><init>(LX/JWi;)V

    .line 2703215
    move-object v0, p0

    .line 2703216
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703217
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703218
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
