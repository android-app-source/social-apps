.class public final enum LX/K8u;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K8u;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K8u;

.field public static final enum DIGEST_COVER:LX/K8u;

.field public static final enum END_CARD:LX/K8u;

.field public static final enum STORY_CARD:LX/K8u;


# instance fields
.field public final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2776350
    new-instance v0, LX/K8u;

    const-string v1, "DIGEST_COVER"

    const-string v2, "digest_cover"

    invoke-direct {v0, v1, v3, v2}, LX/K8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/K8u;->DIGEST_COVER:LX/K8u;

    .line 2776351
    new-instance v0, LX/K8u;

    const-string v1, "STORY_CARD"

    const-string v2, "story_cover"

    invoke-direct {v0, v1, v4, v2}, LX/K8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/K8u;->STORY_CARD:LX/K8u;

    .line 2776352
    new-instance v0, LX/K8u;

    const-string v1, "END_CARD"

    const-string v2, "tarot_end"

    invoke-direct {v0, v1, v5, v2}, LX/K8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/K8u;->END_CARD:LX/K8u;

    .line 2776353
    const/4 v0, 0x3

    new-array v0, v0, [LX/K8u;

    sget-object v1, LX/K8u;->DIGEST_COVER:LX/K8u;

    aput-object v1, v0, v3

    sget-object v1, LX/K8u;->STORY_CARD:LX/K8u;

    aput-object v1, v0, v4

    sget-object v1, LX/K8u;->END_CARD:LX/K8u;

    aput-object v1, v0, v5

    sput-object v0, LX/K8u;->$VALUES:[LX/K8u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2776354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2776355
    iput-object p3, p0, LX/K8u;->mName:Ljava/lang/String;

    .line 2776356
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K8u;
    .locals 1

    .prologue
    .line 2776357
    const-class v0, LX/K8u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K8u;

    return-object v0
.end method

.method public static values()[LX/K8u;
    .locals 1

    .prologue
    .line 2776358
    sget-object v0, LX/K8u;->$VALUES:[LX/K8u;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K8u;

    return-object v0
.end method
