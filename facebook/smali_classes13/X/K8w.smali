.class public LX/K8w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/K8w;


# instance fields
.field public final a:LX/1CD;

.field public b:Z


# direct methods
.method public constructor <init>(LX/1CD;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2776361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776362
    iput-object p1, p0, LX/K8w;->a:LX/1CD;

    .line 2776363
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K8w;->b:Z

    .line 2776364
    return-void
.end method

.method public static a(LX/0QB;)LX/K8w;
    .locals 4

    .prologue
    .line 2776365
    sget-object v0, LX/K8w;->c:LX/K8w;

    if-nez v0, :cond_1

    .line 2776366
    const-class v1, LX/K8w;

    monitor-enter v1

    .line 2776367
    :try_start_0
    sget-object v0, LX/K8w;->c:LX/K8w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2776368
    if-eqz v2, :cond_0

    .line 2776369
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2776370
    new-instance p0, LX/K8w;

    invoke-static {v0}, LX/1CD;->a(LX/0QB;)LX/1CD;

    move-result-object v3

    check-cast v3, LX/1CD;

    invoke-direct {p0, v3}, LX/K8w;-><init>(LX/1CD;)V

    .line 2776371
    move-object v0, p0

    .line 2776372
    sput-object v0, LX/K8w;->c:LX/K8w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2776373
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2776374
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2776375
    :cond_1
    sget-object v0, LX/K8w;->c:LX/K8w;

    return-object v0

    .line 2776376
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2776377
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
