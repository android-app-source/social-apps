.class public final LX/K1s;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/Clf;

.field public b:LX/Clf;

.field public c:LX/CmQ;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2762781
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2762782
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2762777
    invoke-static {}, LX/K1r;->values()[LX/K1r;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2762778
    sget-object v1, LX/K1q;->a:[I

    invoke-virtual {v0}, LX/K1r;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2762779
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2762780
    :pswitch_0
    new-instance v0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;-><init>(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2762735
    invoke-static {}, LX/K1r;->values()[LX/K1r;

    move-result-object v0

    aget-object v0, v0, p4

    .line 2762736
    sget-object v1, LX/K1q;->a:[I

    invoke-virtual {v0}, LX/K1r;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2762737
    :goto_0
    return-void

    .line 2762738
    :pswitch_0
    check-cast p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;

    .line 2762739
    iget-object v0, p0, LX/K1s;->a:LX/Clf;

    const/4 p2, 0x0

    .line 2762740
    if-eqz v0, :cond_0

    .line 2762741
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->b:LX/CIg;

    iget-object p1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1, v0}, LX/CIg;->a(Landroid/widget/TextView;LX/Clf;)V

    .line 2762742
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->a:LX/Cju;

    const p1, 0x7f0d012c

    invoke-interface {v1, p1}, LX/Cju;->c(I)I

    move-result v1

    .line 2762743
    iget-object p1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    int-to-float v1, v1

    invoke-virtual {p1, p2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 2762744
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2762745
    :goto_1
    iget-object v0, p0, LX/K1s;->c:LX/CmQ;

    .line 2762746
    if-eqz v0, :cond_1

    .line 2762747
    iget-object v1, v0, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object v1, v1

    .line 2762748
    if-eqz v1, :cond_1

    .line 2762749
    iget-object v1, v0, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object v1, v1

    .line 2762750
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2762751
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2762752
    iget-object p1, v0, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object p1, p1

    .line 2762753
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->e()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p2, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2762754
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2762755
    invoke-virtual {p3}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 2762756
    iget-object p2, v0, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object p2, p2

    .line 2762757
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->en_()I

    move-result p2

    int-to-float p2, p2

    invoke-static {p1, p2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result p1

    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2762758
    invoke-virtual {p3}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 2762759
    iget-object p2, v0, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object p2, p2

    .line 2762760
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->d()I

    move-result p2

    int-to-float p2, p2

    invoke-static {p1, p2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result p1

    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2762761
    iget-object p1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2762762
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2762763
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->d:Landroid/widget/FrameLayout;

    .line 2762764
    iget-object p1, v0, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object p1, p1

    .line 2762765
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/CIg;->a(Ljava/lang/String;)I

    move-result p1

    invoke-static {v1, p1}, LX/8ba;->a(Landroid/view/View;I)V

    .line 2762766
    :goto_2
    iget-object v0, p0, LX/K1s;->b:LX/Clf;

    .line 2762767
    if-eqz v0, :cond_2

    .line 2762768
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->b:LX/CIg;

    iget-object p1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1, v0}, LX/CIg;->a(Landroid/widget/TextView;LX/Clf;)V

    .line 2762769
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2762770
    :goto_3
    iget-boolean v0, p0, LX/K1s;->d:Z

    .line 2762771
    iput-boolean v0, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->h:Z

    .line 2762772
    invoke-static {p3}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->a(Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;)V

    .line 2762773
    goto/16 :goto_0

    .line 2762774
    :cond_0
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2762775
    :cond_1
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_2

    .line 2762776
    :cond_2
    iget-object v1, p3, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2762734
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2762730
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2762733
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2762732
    sget-object v0, LX/K1r;->ARTICLE_INFORMATION:LX/K1r;

    invoke-virtual {v0}, LX/K1r;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2762731
    invoke-static {}, LX/K1r;->values()[LX/K1r;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
