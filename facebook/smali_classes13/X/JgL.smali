.class public final LX/JgL;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:LX/JgM;

.field public final m:LX/JgE;

.field public n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/CKT;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/Jfh;


# direct methods
.method public constructor <init>(LX/JgM;LX/JgE;LX/0Or;LX/Jfh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JgE;",
            "LX/0Or",
            "<",
            "LX/CKT;",
            ">;",
            "Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManageAdapterViewFactory$Callback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2722614
    iput-object p1, p0, LX/JgL;->l:LX/JgM;

    .line 2722615
    iget-object v0, p2, LX/JgE;->a:Landroid/view/ViewGroup;

    move-object v0, v0

    .line 2722616
    invoke-direct {p0, v0}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2722617
    iput-object p2, p0, LX/JgL;->m:LX/JgE;

    .line 2722618
    iput-object p3, p0, LX/JgL;->n:LX/0Or;

    .line 2722619
    iput-object p4, p0, LX/JgL;->o:LX/Jfh;

    .line 2722620
    return-void
.end method

.method public static a$redex0(LX/JgL;LX/Jg7;ZI)V
    .locals 8

    .prologue
    .line 2722621
    iget-object v0, p1, LX/Jg7;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    move-object v0, v0

    .line 2722622
    new-instance v1, LX/CL5;

    invoke-direct {v1}, LX/CL5;-><init>()V

    .line 2722623
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/CL5;->a:Ljava/lang/String;

    .line 2722624
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/CL5;->b:Ljava/lang/String;

    .line 2722625
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->l()Z

    move-result v2

    iput-boolean v2, v1, LX/CL5;->c:Z

    .line 2722626
    move-object v0, v1

    .line 2722627
    iput-boolean p2, v0, LX/CL5;->c:Z

    .line 2722628
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2722629
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 2722630
    iget-object v2, v0, LX/CL5;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2722631
    iget-object v4, v0, LX/CL5;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2722632
    const/4 v6, 0x3

    invoke-virtual {v1, v6}, LX/186;->c(I)V

    .line 2722633
    invoke-virtual {v1, v7, v2}, LX/186;->b(II)V

    .line 2722634
    invoke-virtual {v1, v5, v4}, LX/186;->b(II)V

    .line 2722635
    const/4 v2, 0x2

    iget-boolean v4, v0, LX/CL5;->c:Z

    invoke-virtual {v1, v2, v4}, LX/186;->a(IZ)V

    .line 2722636
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 2722637
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 2722638
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 2722639
    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2722640
    new-instance v1, LX/15i;

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2722641
    new-instance v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    invoke-direct {v2, v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;-><init>(LX/15i;)V

    .line 2722642
    move-object v0, v2

    .line 2722643
    iput-object v0, p1, LX/Jg7;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    .line 2722644
    iget-object v0, p0, LX/JgL;->o:LX/Jfh;

    .line 2722645
    iget-object v1, v0, LX/Jfh;->a:LX/Jfk;

    invoke-virtual {v1, p3}, LX/1OM;->i_(I)V

    .line 2722646
    return-void
.end method
