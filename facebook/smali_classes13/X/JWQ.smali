.class public final LX/JWQ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JWR;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/JWT;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

.field public d:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:LX/3mj;

.field public final synthetic g:LX/JWR;


# direct methods
.method public constructor <init>(LX/JWR;)V
    .locals 1

    .prologue
    .line 2702937
    iput-object p1, p0, LX/JWQ;->g:LX/JWR;

    .line 2702938
    move-object v0, p1

    .line 2702939
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2702940
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2702941
    const-string v0, "FutureFriendingButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2702911
    if-ne p0, p1, :cond_1

    .line 2702912
    :cond_0
    :goto_0
    return v0

    .line 2702913
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2702914
    goto :goto_0

    .line 2702915
    :cond_3
    check-cast p1, LX/JWQ;

    .line 2702916
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2702917
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2702918
    if-eq v2, v3, :cond_0

    .line 2702919
    iget-object v2, p0, LX/JWQ;->a:LX/JWT;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JWQ;->a:LX/JWT;

    iget-object v3, p1, LX/JWQ;->a:LX/JWT;

    invoke-virtual {v2, v3}, LX/JWT;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2702920
    goto :goto_0

    .line 2702921
    :cond_5
    iget-object v2, p1, LX/JWQ;->a:LX/JWT;

    if-nez v2, :cond_4

    .line 2702922
    :cond_6
    iget-object v2, p0, LX/JWQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JWQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JWQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2702923
    goto :goto_0

    .line 2702924
    :cond_8
    iget-object v2, p1, LX/JWQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2702925
    :cond_9
    iget-object v2, p0, LX/JWQ;->c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JWQ;->c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v3, p1, LX/JWQ;->c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2702926
    goto :goto_0

    .line 2702927
    :cond_b
    iget-object v2, p1, LX/JWQ;->c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    if-nez v2, :cond_a

    .line 2702928
    :cond_c
    iget-object v2, p0, LX/JWQ;->d:LX/1Pc;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/JWQ;->d:LX/1Pc;

    iget-object v3, p1, LX/JWQ;->d:LX/1Pc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2702929
    goto :goto_0

    .line 2702930
    :cond_e
    iget-object v2, p1, LX/JWQ;->d:LX/1Pc;

    if-nez v2, :cond_d

    .line 2702931
    :cond_f
    iget-object v2, p0, LX/JWQ;->e:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/JWQ;->e:Ljava/lang/String;

    iget-object v3, p1, LX/JWQ;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2702932
    goto :goto_0

    .line 2702933
    :cond_11
    iget-object v2, p1, LX/JWQ;->e:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 2702934
    :cond_12
    iget-object v2, p0, LX/JWQ;->f:LX/3mj;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/JWQ;->f:LX/3mj;

    iget-object v3, p1, LX/JWQ;->f:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2702935
    goto/16 :goto_0

    .line 2702936
    :cond_13
    iget-object v2, p1, LX/JWQ;->f:LX/3mj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
