.class public LX/JpT;
.super LX/Dnx;
.source ""


# static fields
.field private static final a:LX/Do1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Do1",
            "<",
            "LX/JpS;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/1qM;

.field public final c:LX/1qM;

.field private final d:LX/1qM;

.field private final e:LX/2Oq;

.field private final f:LX/26j;

.field private final g:LX/FMl;

.field private final h:LX/6jC;

.field private final i:LX/2Or;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2737532
    new-instance v0, LX/Do2;

    invoke-direct {v0}, LX/Do2;-><init>()V

    sput-object v0, LX/JpT;->a:LX/Do1;

    return-void
.end method

.method public constructor <init>(LX/1qM;LX/1qM;LX/1qM;LX/2Oq;LX/26j;LX/FMl;LX/6jC;LX/2Or;)V
    .locals 1
    .param p1    # LX/1qM;
        .annotation runtime Lcom/facebook/messaging/service/multicache/FacebookCachingServiceChain;
        .end annotation
    .end param
    .param p2    # LX/1qM;
        .annotation runtime Lcom/facebook/messaging/service/multicache/SmsCachingServiceChain;
        .end annotation
    .end param
    .param p3    # LX/1qM;
        .annotation runtime Lcom/facebook/messaging/tincan/messenger/annotations/TincanCachingServiceChain;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2737533
    const-string v0, "MultiCacheServiceHandler"

    invoke-direct {p0, v0}, LX/Dnx;-><init>(Ljava/lang/String;)V

    .line 2737534
    iput-object p1, p0, LX/JpT;->b:LX/1qM;

    .line 2737535
    iput-object p2, p0, LX/JpT;->c:LX/1qM;

    .line 2737536
    iput-object p3, p0, LX/JpT;->d:LX/1qM;

    .line 2737537
    iput-object p4, p0, LX/JpT;->e:LX/2Oq;

    .line 2737538
    iput-object p5, p0, LX/JpT;->f:LX/26j;

    .line 2737539
    iput-object p6, p0, LX/JpT;->g:LX/FMl;

    .line 2737540
    iput-object p7, p0, LX/JpT;->h:LX/6jC;

    .line 2737541
    iput-object p8, p0, LX/JpT;->i:LX/2Or;

    .line 2737542
    return-void
.end method

.method private a(LX/JpS;)LX/1qM;
    .locals 3

    .prologue
    .line 2737543
    sget-object v0, LX/JpR;->a:[I

    invoke-virtual {p1}, LX/JpS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2737544
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected PermalinkCacheType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2737545
    :pswitch_0
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    .line 2737546
    :goto_0
    return-object v0

    .line 2737547
    :pswitch_1
    iget-object v0, p0, LX/JpT;->c:LX/1qM;

    goto :goto_0

    .line 2737548
    :pswitch_2
    iget-object v0, p0, LX/JpT;->d:LX/1qM;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;
    .locals 1

    .prologue
    .line 2737549
    invoke-static {p1}, LX/JpT;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/JpS;

    move-result-object v0

    invoke-direct {p0, v0}, LX/JpT;->a(LX/JpS;)LX/1qM;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/6ek;LX/6em;LX/1qK;)LX/Do4;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "LX/6em;",
            "LX/1qK;",
            ")",
            "LX/Do4",
            "<",
            "LX/JpS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2737550
    new-instance v0, LX/Do4;

    invoke-direct {v0}, LX/Do4;-><init>()V

    .line 2737551
    sget-object v1, LX/6em;->SMS:LX/6em;

    if-eq p2, v1, :cond_0

    .line 2737552
    sget-object v1, LX/JpS;->FACEBOOK:LX/JpS;

    iget-object v2, p0, LX/JpT;->b:LX/1qM;

    invoke-virtual {v0, v1, v2, p3}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    .line 2737553
    :cond_0
    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    if-ne p1, v1, :cond_3

    .line 2737554
    sget-object v1, LX/6em;->NON_SMS:LX/6em;

    if-eq p2, v1, :cond_1

    invoke-direct {p0}, LX/JpT;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2737555
    sget-object v1, LX/JpS;->SMS:LX/JpS;

    iget-object v2, p0, LX/JpT;->c:LX/1qM;

    invoke-virtual {v0, v1, v2, p3}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    .line 2737556
    :cond_1
    sget-object v1, LX/6em;->SMS:LX/6em;

    if-eq p2, v1, :cond_2

    iget-object v1, p0, LX/JpT;->f:LX/26j;

    invoke-virtual {v1}, LX/26j;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2737557
    sget-object v1, LX/JpS;->TINCAN:LX/JpS;

    iget-object v2, p0, LX/JpT;->d:LX/1qM;

    invoke-virtual {v0, v1, v2, p3}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    .line 2737558
    :cond_2
    :goto_0
    return-object v0

    .line 2737559
    :cond_3
    sget-object v1, LX/6ek;->SMS_SPAM:LX/6ek;

    if-ne p1, v1, :cond_4

    sget-object v1, LX/6em;->SMS:LX/6em;

    if-ne p2, v1, :cond_4

    .line 2737560
    sget-object v1, LX/JpS;->SMS:LX/JpS;

    iget-object v2, p0, LX/JpT;->c:LX/1qM;

    invoke-virtual {v0, v1, v2, p3}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    goto :goto_0

    .line 2737561
    :cond_4
    sget-object v1, LX/6ek;->SMS_BUSINESS:LX/6ek;

    if-ne p1, v1, :cond_2

    sget-object v1, LX/6em;->SMS:LX/6em;

    if-ne p2, v1, :cond_2

    .line 2737562
    sget-object v1, LX/JpS;->SMS:LX/JpS;

    iget-object v2, p0, LX/JpT;->c:LX/1qM;

    invoke-virtual {v0, v1, v2, p3}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "LX/JpS;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2737563
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2737564
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2737565
    invoke-static {v0}, LX/JpT;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/JpS;

    move-result-object v4

    .line 2737566
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 2737567
    if-nez v1, :cond_0

    .line 2737568
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2737569
    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2737570
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2737571
    :cond_1
    return-object v2
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 2737572
    iget-object v0, p0, LX/JpT;->e:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JpT;->g:LX/FMl;

    invoke-virtual {v0}, LX/FMl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/JpS;
    .locals 3

    .prologue
    .line 2737573
    sget-object v0, LX/JpR;->b:[I

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v1}, LX/5e9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2737574
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected thread key type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2737575
    :pswitch_0
    sget-object v0, LX/JpS;->FACEBOOK:LX/JpS;

    .line 2737576
    :goto_0
    return-object v0

    .line 2737577
    :pswitch_1
    sget-object v0, LX/JpS;->SMS:LX/JpS;

    goto :goto_0

    .line 2737578
    :pswitch_2
    sget-object v0, LX/JpS;->TINCAN:LX/JpS;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/JpT;
    .locals 9

    .prologue
    .line 2737579
    new-instance v0, LX/JpT;

    invoke-static {p0}, LX/JpG;->a(LX/0QB;)LX/1qM;

    move-result-object v1

    check-cast v1, LX/1qM;

    invoke-static {p0}, LX/JpL;->a(LX/0QB;)LX/1qM;

    move-result-object v2

    check-cast v2, LX/1qM;

    invoke-static {p0}, LX/JpM;->a(LX/0QB;)LX/1qM;

    move-result-object v3

    check-cast v3, LX/1qM;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v4

    check-cast v4, LX/2Oq;

    invoke-static {p0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v5

    check-cast v5, LX/26j;

    invoke-static {p0}, LX/FMl;->a(LX/0QB;)LX/FMl;

    move-result-object v6

    check-cast v6, LX/FMl;

    invoke-static {p0}, LX/6jC;->a(LX/0QB;)LX/6jC;

    move-result-object v7

    check-cast v7, LX/6jC;

    invoke-static {p0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v8

    check-cast v8, LX/2Or;

    invoke-direct/range {v0 .. v8}, LX/JpT;-><init>(LX/1qM;LX/1qM;LX/1qM;LX/2Oq;LX/26j;LX/FMl;LX/6jC;LX/2Or;)V

    .line 2737580
    return-object v0
.end method

.method public static b(Ljava/util/HashMap;LX/0Px;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2737521
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2737522
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2737523
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2737524
    if-eqz v1, :cond_0

    .line 2737525
    iget-wide v8, v0, Lcom/facebook/user/model/User;->M:J

    move-wide v4, v8

    .line 2737526
    iget-wide v8, v1, Lcom/facebook/user/model/User;->M:J

    move-wide v6, v8

    .line 2737527
    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 2737528
    :cond_0
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2737529
    invoke-virtual {p0, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2737530
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2737531
    :cond_2
    return-void
.end method


# virtual methods
.method public final A(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737584
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737585
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737586
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final D(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737599
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final E(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737608
    iget-object v0, p0, LX/JpT;->c:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final F(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737607
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final G(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737606
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final H(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737605
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final I(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737604
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final J(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737603
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final K(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737602
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final L(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737601
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final M(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737600
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final N(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737587
    iget-object v0, p0, LX/JpT;->d:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2737589
    invoke-direct {p0}, LX/JpT;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2737590
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2737591
    :goto_0
    return-object v0

    .line 2737592
    :cond_0
    const-string v0, "fetchPinnedThreadsParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2737593
    new-instance v1, LX/Do4;

    invoke-direct {v1}, LX/Do4;-><init>()V

    .line 2737594
    sget-object v2, LX/JpS;->SMS:LX/JpS;

    iget-object p2, p0, LX/JpT;->c:LX/1qM;

    invoke-virtual {v1, v2, p2, p1}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    .line 2737595
    sget-object v2, LX/JpS;->FACEBOOK:LX/JpS;

    iget-object p2, p0, LX/JpT;->b:LX/1qM;

    invoke-virtual {v1, v2, p2, p1}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    .line 2737596
    move-object v1, v1

    .line 2737597
    new-instance v2, LX/JpQ;

    invoke-direct {v2, p0, v0}, LX/JpQ;-><init>(LX/JpT;Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;)V

    .line 2737598
    invoke-static {v1, v2}, LX/Do5;->a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final P(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737588
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final Q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737581
    const-string v0, "updateAgentSuggestionsParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;

    .line 2737582
    iget-object v0, v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0, v0}, LX/JpT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;

    move-result-object v0

    .line 2737583
    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2737419
    const-string v0, "fetchThreadListParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2737420
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2737421
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2737422
    iget-object p2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    move-object v0, p2

    .line 2737423
    invoke-direct {p0, v2, v0, p1}, LX/JpT;->a(LX/6ek;LX/6em;LX/1qK;)LX/Do4;

    move-result-object v0

    .line 2737424
    new-instance v2, LX/JpP;

    invoke-direct {v2, p0, v1}, LX/JpP;-><init>(LX/JpT;LX/6ek;)V

    .line 2737425
    invoke-static {v0, v2}, LX/Do5;->a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2737434
    const-string v0, "fetchMoreThreadsParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    .line 2737435
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v1, v1

    .line 2737436
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v2, v2

    .line 2737437
    iget-object p2, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->b:LX/6em;

    move-object v0, p2

    .line 2737438
    invoke-direct {p0, v2, v0, p1}, LX/JpT;->a(LX/6ek;LX/6em;LX/1qK;)LX/Do4;

    move-result-object v0

    .line 2737439
    new-instance v2, LX/JpO;

    invoke-direct {v2, p0, v1}, LX/JpO;-><init>(LX/JpT;LX/6ek;)V

    .line 2737440
    invoke-static {v0, v2}, LX/Do5;->a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2737441
    const-string v0, "fetchThreadParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2737442
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v1, v1

    .line 2737443
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2737444
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2737445
    :goto_0
    return-object v0

    .line 2737446
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v0, v1

    .line 2737447
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0, v0}, LX/JpT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2737426
    const-string v0, "fetch_thread_with_participants_key"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;

    .line 2737427
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->a:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2737428
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    move-object v0, v2

    .line 2737429
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2737430
    invoke-virtual {v0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2737431
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2737432
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2737433
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/JpT;->c:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737448
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737449
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2737450
    const-string v0, "createThreadParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    .line 2737451
    iget-object p2, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    move-object v0, p2

    .line 2737452
    const/4 v2, 0x0

    .line 2737453
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move v1, v2

    .line 2737454
    :goto_0
    move v0, v1

    .line 2737455
    if-eqz v0, :cond_1

    .line 2737456
    iget-object v0, p0, LX/JpT;->c:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2737457
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 2737458
    :cond_2
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    move v3, v2

    :goto_2
    if-ge v3, p2, :cond_4

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserIdentifier;

    .line 2737459
    instance-of v1, v1, Lcom/facebook/user/model/UserSmsIdentifier;

    if-nez v1, :cond_3

    move v1, v2

    .line 2737460
    goto :goto_0

    .line 2737461
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 2737462
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737463
    const-string v0, "fetchMoreMessagesParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    .line 2737464
    iget-object p2, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, p2

    .line 2737465
    invoke-direct {p0, v0}, LX/JpT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737466
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2737467
    const-string v0, "markThreadsParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2737468
    iget-object v1, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-static {v1}, LX/JpT;->a(Ljava/lang/Iterable;)Ljava/util/HashMap;

    move-result-object v1

    .line 2737469
    new-instance v5, LX/Do4;

    invoke-direct {v5}, LX/Do4;-><init>()V

    .line 2737470
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2737471
    new-instance v2, LX/6ie;

    invoke-direct {v2}, LX/6ie;-><init>()V

    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 2737472
    iput-object v3, v2, LX/6ie;->a:LX/6iW;

    .line 2737473
    move-object v2, v2

    .line 2737474
    iget-boolean v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->b:Z

    .line 2737475
    iput-boolean v3, v2, LX/6ie;->b:Z

    .line 2737476
    move-object v7, v2

    .line 2737477
    iget-object v8, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v9, :cond_1

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2737478
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    iget-object v10, v2, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2737479
    invoke-virtual {v7, v2}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    .line 2737480
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2737481
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2737482
    const-string v3, "markThreadsParams"

    invoke-virtual {v7}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2737483
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JpS;

    .line 2737484
    invoke-direct {p0, v1}, LX/JpT;->a(LX/JpS;)LX/1qM;

    move-result-object v3

    new-instance v4, LX/1qK;

    const-string v7, "mark_threads"

    invoke-direct {v4, v7, v2}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v5, v1, v3, v4}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    goto :goto_0

    .line 2737485
    :cond_2
    sget-object v0, LX/JpT;->a:LX/Do1;

    invoke-static {v5, v0}, LX/Do5;->a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final k(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2737486
    iget-object v0, p0, LX/JpT;->f:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2737487
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2737488
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Do4;

    invoke-direct {v0}, LX/Do4;-><init>()V

    sget-object v1, LX/JpS;->FACEBOOK:LX/JpS;

    iget-object v2, p0, LX/JpT;->b:LX/1qM;

    invoke-virtual {v0, v1, v2, p1}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    move-result-object v0

    sget-object v1, LX/JpS;->TINCAN:LX/JpS;

    iget-object v2, p0, LX/JpT;->d:LX/1qM;

    invoke-virtual {v0, v1, v2, p1}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    move-result-object v0

    sget-object v1, LX/JpT;->a:LX/Do1;

    invoke-static {v0, v1}, LX/Do5;->a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2737489
    const-string v0, "deleteThreadsParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    .line 2737490
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v0, v1

    .line 2737491
    invoke-static {v0}, LX/JpT;->a(Ljava/lang/Iterable;)Ljava/util/HashMap;

    move-result-object v0

    .line 2737492
    new-instance v2, LX/Do4;

    invoke-direct {v2}, LX/Do4;-><init>()V

    .line 2737493
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2737494
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JpS;

    .line 2737495
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2737496
    const-string v5, "deleteThreadsParams"

    new-instance v6, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v6, v0}, Lcom/facebook/messaging/service/model/DeleteThreadsParams;-><init>(Ljava/util/List;)V

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2737497
    invoke-direct {p0, v1}, LX/JpT;->a(LX/JpS;)LX/1qM;

    move-result-object v0

    new-instance v5, LX/1qK;

    const-string v6, "delete_threads"

    invoke-direct {v5, v6, v4}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v2, v1, v0, v5}, LX/Do4;->a(Ljava/lang/Object;LX/1qM;LX/1qK;)LX/Do4;

    goto :goto_0

    .line 2737498
    :cond_0
    sget-object v0, LX/JpT;->a:LX/Do1;

    invoke-static {v2, v0}, LX/Do5;->a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final m(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737499
    iget-object v0, p0, LX/JpT;->d:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737500
    const-string v0, "deleteMessagesParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    .line 2737501
    iget-object v0, v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2737502
    if-nez v0, :cond_0

    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    .line 2737503
    :goto_0
    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2737504
    :cond_0
    invoke-direct {p0, v0}, LX/JpT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;

    move-result-object v0

    goto :goto_0
.end method

.method public final o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737505
    const-string v0, "modifyThreadParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2737506
    iget-object p2, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, p2

    .line 2737507
    invoke-direct {p0, v0}, LX/JpT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;

    move-result-object v0

    .line 2737508
    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737509
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737510
    const-string v0, "saveDraftParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SaveDraftParams;

    .line 2737511
    iget-object v0, v0, Lcom/facebook/messaging/service/model/SaveDraftParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0, v0}, LX/JpT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/1qM;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737512
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final s(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737513
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final t(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737514
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737515
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737516
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737517
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737518
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final y(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737519
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2737520
    iget-object v0, p0, LX/JpT;->b:LX/1qM;

    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
