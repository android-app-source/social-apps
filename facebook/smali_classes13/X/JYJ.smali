.class public LX/JYJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JYK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYJ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JYK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706105
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2706106
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYJ;->b:LX/0Zi;

    .line 2706107
    iput-object p1, p0, LX/JYJ;->a:LX/0Ot;

    .line 2706108
    return-void
.end method

.method public static a(LX/0QB;)LX/JYJ;
    .locals 4

    .prologue
    .line 2706109
    const-class v1, LX/JYJ;

    monitor-enter v1

    .line 2706110
    :try_start_0
    sget-object v0, LX/JYJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706111
    sput-object v2, LX/JYJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706112
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706113
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706114
    new-instance v3, LX/JYJ;

    const/16 p0, 0x2174

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYJ;-><init>(LX/0Ot;)V

    .line 2706115
    move-object v0, v3

    .line 2706116
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706117
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706118
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706119
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2706120
    iget-object v0, p0, LX/JYJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 2706121
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020a3f

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f0214c6

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0933

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4, v3}, LX/1Di;->d(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 p2, 0x0

    const/4 p0, 0x1

    const/4 v4, 0x3

    .line 2706122
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0f0b

    invoke-interface {v1, p2, v2}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0f0c

    invoke-interface {v1, p0, v2}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0f0e

    invoke-interface {v1, v2}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f081c26

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00ab

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f020a8d

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v4, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p0, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f081c27

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00a4

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f020a8d

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v4, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p0, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    move-object v1, v1

    .line 2706123
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v3, 0x2

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2706124
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a009f

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b0f0f

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0033

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f081c28

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00a4

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0f0d

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    move-object v1, v1

    .line 2706125
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2706126
    const v1, 0x7ea4eec7

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2706127
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0f0a

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2706128
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2706129
    invoke-static {}, LX/1dS;->b()V

    .line 2706130
    iget v0, p1, LX/1dQ;->b:I

    .line 2706131
    packed-switch v0, :pswitch_data_0

    .line 2706132
    :goto_0
    return-object v2

    .line 2706133
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2706134
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 2706135
    iget-object p1, p0, LX/JYJ;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JYK;

    .line 2706136
    iget-object p2, p1, LX/JYK;->a:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->X:Ljava/lang/String;

    invoke-virtual {p2, p0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2706137
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7ea4eec7
        :pswitch_0
    .end packed-switch
.end method
