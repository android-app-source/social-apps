.class public LX/JhR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3Lx;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/3Lx;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2724479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2724480
    iput-object p1, p0, LX/JhR;->a:LX/3Lx;

    .line 2724481
    iput-object p2, p0, LX/JhR;->b:Landroid/content/res/Resources;

    .line 2724482
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2724464
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->ax()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2724465
    const-string v0, ""

    .line 2724466
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v1

    .line 2724467
    if-eqz v1, :cond_0

    .line 2724468
    iget-object v0, p0, LX/JhR;->a:LX/3Lx;

    .line 2724469
    iget-object v2, v1, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2724470
    invoke-virtual {v0, v2}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2724471
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2724472
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2724473
    iget v2, v1, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    move v1, v2

    .line 2724474
    packed-switch v1, :pswitch_data_0

    .line 2724475
    :cond_0
    :goto_0
    return-object v0

    .line 2724476
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/JhR;->b:Landroid/content/res/Resources;

    const v2, 0x7f080537

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2724477
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/JhR;->b:Landroid/content/res/Resources;

    const v2, 0x7f080538

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2724478
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/JhR;->b:Landroid/content/res/Resources;

    const v2, 0x7f080539

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
