.class public final LX/KBs;
.super LX/KBr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/KBr",
        "<",
        "LX/KAk;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/2wh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wh",
            "<",
            "LX/KAk;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, LX/KBr;-><init>(LX/2wh;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/internal/DeleteDataItemsResponse;)V
    .locals 3

    new-instance v0, LX/KCY;

    iget v1, p1, Lcom/google/android/gms/wearable/internal/DeleteDataItemsResponse;->b:I

    invoke-static {v1}, LX/KBn;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget v2, p1, Lcom/google/android/gms/wearable/internal/DeleteDataItemsResponse;->c:I

    invoke-direct {v0, v1, v2}, LX/KCY;-><init>(Lcom/google/android/gms/common/api/Status;I)V

    invoke-virtual {p0, v0}, LX/KBr;->a(Ljava/lang/Object;)V

    return-void
.end method
