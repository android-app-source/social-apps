.class public LX/JX5;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JX6;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JX5",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JX6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703863
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703864
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JX5;->b:LX/0Zi;

    .line 2703865
    iput-object p1, p0, LX/JX5;->a:LX/0Ot;

    .line 2703866
    return-void
.end method

.method public static a(LX/0QB;)LX/JX5;
    .locals 4

    .prologue
    .line 2703867
    const-class v1, LX/JX5;

    monitor-enter v1

    .line 2703868
    :try_start_0
    sget-object v0, LX/JX5;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703869
    sput-object v2, LX/JX5;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703870
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703871
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703872
    new-instance v3, LX/JX5;

    const/16 p0, 0x2136

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JX5;-><init>(LX/0Ot;)V

    .line 2703873
    move-object v0, v3

    .line 2703874
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703875
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JX5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703876
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703877
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c(LX/1X1;)V
    .locals 12

    .prologue
    .line 2703878
    check-cast p1, LX/JX4;

    .line 2703879
    iget-object v0, p0, LX/JX5;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JX6;

    iget-object v1, p1, LX/JX4;->a:LX/JWr;

    .line 2703880
    iget-object v2, v0, LX/JX6;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;

    .line 2703881
    iget-object v3, v1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    iget-object v4, v1, LX/JWr;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    invoke-static {v3, v4}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    .line 2703882
    invoke-static {v3}, LX/17Q;->F(LX/0lF;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2703883
    const/4 v4, 0x0

    .line 2703884
    :goto_0
    move-object v3, v4

    .line 2703885
    iget-object v4, v2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->g:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2703886
    iget-object v3, v2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->e:LX/2dj;

    iget-object v4, v1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 2703887
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2703888
    const-string v6, "blacklistPeopleYouShouldFollowParamsKey"

    invoke-virtual {v8, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2703889
    iget-object v6, v3, LX/2dj;->q:LX/0aG;

    const-string v7, "friending_blacklist_people_you_should_follow"

    sget-object v9, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v11, 0x75a1cde4

    move-object v10, v5

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    .line 2703890
    invoke-static {v6}, LX/2dm;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2703891
    const/4 v7, 0x0

    invoke-static {v7}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v7

    iget-object v8, v3, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2703892
    iget-object v3, v2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->h:LX/0ti;

    new-instance v4, LX/JWq;

    iget-object v5, v1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/JWq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2703893
    return-void

    .line 2703894
    :cond_0
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "pysf_xout"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tracking"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "native_newsfeed"

    .line 2703895
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2703896
    move-object v4, v4

    .line 2703897
    goto :goto_0
.end method

.method public static f(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2703898
    const v0, 0x72f29bbb

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2703899
    check-cast p2, LX/JX4;

    .line 2703900
    iget-object v0, p0, LX/JX5;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JX6;

    iget-object v1, p2, LX/JX4;->a:LX/JWr;

    iget-object v2, p2, LX/JX4;->b:LX/1Pb;

    const/4 v7, 0x1

    .line 2703901
    iget-object v3, v1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 2703902
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2703903
    iget-object v4, v0, LX/JX6;->d:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    const/high16 v5, 0x42000000    # 32.0f

    invoke-static {p1, v5}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 2703904
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3c

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JX6;->a:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    sget-object v5, LX/1Up;->h:LX/1Up;

    invoke-virtual {v3, v5}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 v5, 0x8

    const/4 v6, 0x3

    invoke-interface {v3, v5, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b092e

    invoke-interface {v3, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v3, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v3

    invoke-static {p1}, LX/JX5;->f(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020afc

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x5

    invoke-interface {v4, v7, v5}, LX/1Di;->m(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    const/16 v6, 0x9

    invoke-interface {v4, v5, v6}, LX/1Di;->m(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0a058e

    invoke-interface {v4, v5}, LX/1Di;->y(I)LX/1Di;

    move-result-object v4

    .line 2703905
    const v5, -0x3f72e3e7

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2703906
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    check-cast v2, LX/1Pr;

    const/16 p2, 0x8

    const/4 p0, 0x6

    const/4 v10, 0x2

    .line 2703907
    iget-object v5, v1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 2703908
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    .line 2703909
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x7

    invoke-interface {v6, v7, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b092f

    invoke-interface {v6, v7}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b0917

    invoke-interface {v7, p0, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-static {v5}, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b004e

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v10}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b004b

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a043b

    invoke-virtual {v8, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/JX5;->f(LX/1De;)LX/1dQ;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 2703910
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v8

    const v9, 0x7f0a0046

    invoke-virtual {v8, v9}, LX/25Q;->i(I)LX/25Q;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, LX/1Di;->g(I)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    move-object v7, v7

    .line 2703911
    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, LX/JX6;->c:LX/359;

    invoke-virtual {v7, p1}, LX/359;->c(LX/1De;)LX/35B;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v4, v8, :cond_0

    const v4, 0x7f020a52

    .line 2703912
    :goto_0
    iget-object v8, v7, LX/35B;->a:LX/35A;

    invoke-virtual {v7, v4}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v9

    iput-object v9, v8, LX/35A;->e:LX/1dc;

    .line 2703913
    iget-object v8, v7, LX/35B;->e:Ljava/util/BitSet;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 2703914
    move-object v4, v7

    .line 2703915
    invoke-virtual {v4, v2}, LX/35B;->a(LX/1Pr;)LX/35B;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/35B;->b(Ljava/lang/String;)LX/35B;

    move-result-object v4

    iget-object v5, v1, LX/JWr;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    invoke-virtual {v4, v5}, LX/35B;->a(LX/0jW;)LX/35B;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    .line 2703916
    const v5, 0x7be7cc46

    const/4 v7, 0x0

    invoke-static {p1, v5, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2703917
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object v4, v4

    .line 2703918
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2703919
    return-object v0

    :cond_0
    const v4, 0x7f020ad5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2703920
    invoke-static {}, LX/1dS;->b()V

    .line 2703921
    iget v0, p1, LX/1dQ;->b:I

    .line 2703922
    sparse-switch v0, :sswitch_data_0

    .line 2703923
    :goto_0
    return-object v2

    .line 2703924
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2703925
    check-cast v0, LX/JX4;

    .line 2703926
    iget-object v1, p0, LX/JX5;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JX6;

    iget-object p1, v0, LX/JX4;->a:LX/JWr;

    iget-object p2, v0, LX/JX4;->b:LX/1Pb;

    .line 2703927
    iget-object p0, v1, LX/JX6;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2703928
    iget-object p0, p1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object p0

    .line 2703929
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    .line 2703930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object p0

    .line 2703931
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_0

    .line 2703932
    const-string v0, "PYSF_NETEGO"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p0, v0}, LX/1Pb;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2703933
    :goto_1
    goto :goto_0

    .line 2703934
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/JX5;->c(LX/1X1;)V

    goto :goto_0

    .line 2703935
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 2703936
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2703937
    check-cast v1, LX/JX4;

    .line 2703938
    iget-object v3, p0, LX/JX5;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JX6;

    iget-object p1, v1, LX/JX4;->a:LX/JWr;

    .line 2703939
    iget-object p2, v3, LX/JX6;->b:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;

    iget-object p0, p1, LX/JWr;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 2703940
    iget-object v1, p2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nA;

    invoke-static {p0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-static {v3}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v3

    const/4 p1, 0x0

    invoke-virtual {v1, v0, v3, p1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2703941
    goto :goto_0

    .line 2703942
    :cond_0
    const-string v0, "PYSF_NETEGO"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p0, v0}, LX/1Pb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x3f72e3e7 -> :sswitch_1
        0x72f29bbb -> :sswitch_2
        0x7be7cc46 -> :sswitch_0
    .end sparse-switch
.end method
