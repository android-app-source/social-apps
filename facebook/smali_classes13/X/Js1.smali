.class public LX/Js1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2P0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9lb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/model/threadkey/ThreadKey;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2744992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/Js1;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/4Jq;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2744993
    iget-object v0, p0, LX/Js1;->a:LX/2P0;

    iget-object v1, p0, LX/Js1;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)LX/0Px;

    move-result-object v4

    .line 2744994
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2744995
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;

    .line 2744996
    new-instance v7, LX/4Jq;

    invoke-direct {v7}, LX/4Jq;-><init>()V

    .line 2744997
    iget-wide v8, v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->a:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2744998
    const-string v8, "sender_id"

    invoke-virtual {v7, v8, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2744999
    iget-wide v8, v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->b:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v1, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2745000
    const-string v8, "send_time"

    invoke-virtual {v7, v8, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2745001
    iget-object v1, v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->d:[B

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->d:[B

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 2745002
    :goto_1
    const-string v8, "hmac"

    invoke-virtual {v7, v8, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2745003
    iget-object v1, v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->c:[B

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->c:[B

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 2745004
    :goto_2
    const-string v1, "salamander_thrift"

    invoke-virtual {v7, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2745005
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2745006
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2745007
    :cond_0
    const-string v1, ""

    goto :goto_1

    .line 2745008
    :cond_1
    const-string v0, ""

    goto :goto_2

    .line 2745009
    :cond_2
    return-object v5
.end method

.method public static b(LX/0QB;)LX/Js1;
    .locals 3

    .prologue
    .line 2745010
    new-instance v2, LX/Js1;

    invoke-direct {v2}, LX/Js1;-><init>()V

    .line 2745011
    invoke-static {p0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v0

    check-cast v0, LX/2P0;

    invoke-static {p0}, LX/9lb;->b(LX/0QB;)LX/9lb;

    move-result-object v1

    check-cast v1, LX/9lb;

    .line 2745012
    iput-object v0, v2, LX/Js1;->a:LX/2P0;

    iput-object v1, v2, LX/Js1;->b:LX/9lb;

    .line 2745013
    return-object v2
.end method
