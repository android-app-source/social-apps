.class public final LX/K5d;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public a:Z

.field public final synthetic b:LX/K5e;


# direct methods
.method public constructor <init>(LX/K5e;)V
    .locals 1

    .prologue
    .line 2770524
    iput-object p1, p0, LX/K5d;->b:LX/K5e;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 2770525
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K5d;->a:Z

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2770526
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K5d;->a:Z

    .line 2770527
    const/4 v0, 0x1

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2770528
    iget-boolean v1, p0, LX/K5d;->a:Z

    if-nez v1, :cond_0

    .line 2770529
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    .line 2770530
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    .line 2770531
    const/4 v3, 0x0

    cmpg-float v3, v1, v3

    if-gez v3, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v3, p0, LX/K5d;->b:LX/K5e;

    iget v3, v3, LX/K5e;->f:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, LX/K5d;->b:LX/K5e;

    iget v2, v2, LX/K5e;->e:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 2770532
    iput-boolean v0, p0, LX/K5d;->a:Z

    .line 2770533
    iget-object v1, p0, LX/K5d;->b:LX/K5e;

    .line 2770534
    iget-object v2, v1, LX/K5e;->a:LX/K7m;

    new-instance v3, LX/K7p;

    invoke-direct {v3}, LX/K7p;-><init>()V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2770535
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2770536
    iget-object v0, p0, LX/K5d;->b:LX/K5e;

    .line 2770537
    iget-object p0, v0, LX/K5e;->a:LX/K7m;

    new-instance p1, LX/K7o;

    invoke-direct {p1}, LX/K7o;-><init>()V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2770538
    const/4 v0, 0x1

    return v0
.end method
