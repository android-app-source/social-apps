.class public LX/JrY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public a:LX/JrX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743722
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrY;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2743723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2743724
    return-void
.end method

.method public static a(LX/0QB;)LX/JrY;
    .locals 7

    .prologue
    .line 2743725
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2743726
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2743727
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2743728
    if-nez v1, :cond_0

    .line 2743729
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743730
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2743731
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2743732
    sget-object v1, LX/JrY;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2743733
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2743734
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2743735
    :cond_1
    if-nez v1, :cond_4

    .line 2743736
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2743737
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2743738
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2743739
    new-instance p0, LX/JrY;

    invoke-direct {p0}, LX/JrY;-><init>()V

    .line 2743740
    invoke-static {v0}, LX/JrX;->a(LX/0QB;)LX/JrX;

    move-result-object v1

    check-cast v1, LX/JrX;

    .line 2743741
    iput-object v1, p0, LX/JrY;->a:LX/JrX;

    .line 2743742
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2743743
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2743744
    if-nez v1, :cond_2

    .line 2743745
    sget-object v0, LX/JrY;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrY;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2743746
    :goto_1
    if-eqz v0, :cond_3

    .line 2743747
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743748
    :goto_3
    check-cast v0, LX/JrY;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2743749
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2743750
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2743751
    :catchall_1
    move-exception v0

    .line 2743752
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743753
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2743754
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2743755
    :cond_2
    :try_start_8
    sget-object v0, LX/JrY;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrY;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/6kU;)LX/Jqi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kU;",
            ")",
            "Lcom/facebook/messaging/sync/delta/handlerbase/MessagesDeltaHandler",
            "<",
            "LX/6kU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2743756
    iget-object v0, p0, LX/JrY;->a:LX/JrX;

    .line 2743757
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 2743758
    invoke-virtual {v0, v1}, LX/JrX;->a(I)LX/Jqi;

    move-result-object v0

    .line 2743759
    if-nez v0, :cond_0

    .line 2743760
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported client only delta type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2743761
    iget v2, p1, LX/6kT;->setField_:I

    move v2, v2

    .line 2743762
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743763
    :cond_0
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 15

    .prologue
    .line 2743764
    iget-object v2, p0, LX/JrY;->a:LX/JrX;

    const/4 v12, 0x0

    .line 2743765
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 2743766
    const/4 v8, 0x7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743767
    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743768
    const/16 v8, 0xb

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743769
    const/16 v8, 0xf

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743770
    const/16 v8, 0xe

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743771
    iget-object v8, v2, LX/JrX;->a:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Jqh;

    .line 2743772
    invoke-virtual {v8}, LX/Jqh;->a()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2743773
    iget-object v8, v8, LX/Jqh;->a:Ljava/lang/Integer;

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2743774
    :cond_1
    iget-object v8, v2, LX/JrX;->k:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0Uh;

    const/16 v9, 0x175

    invoke-virtual {v8, v9, v12}, LX/0Uh;->a(IZ)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2743775
    const/16 v8, 0x8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743776
    :cond_2
    iget-object v8, v2, LX/JrX;->k:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0Uh;

    const/16 v9, 0x1ea

    invoke-virtual {v8, v9, v12}, LX/0Uh;->a(IZ)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2743777
    const/16 v8, 0xc

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743778
    :cond_3
    iget-object v8, v2, LX/JrX;->l:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0W3;

    sget-wide v12, LX/0X5;->kl:J

    invoke-interface {v8, v12, v13}, LX/0W4;->a(J)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2743779
    const/16 v8, 0x10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2743780
    :cond_4
    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_5
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2743781
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 2743782
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v2, v9}, LX/JrX;->a(I)LX/Jqi;

    move-result-object v9

    if-nez v9, :cond_5

    .line 2743783
    iget-object v9, v2, LX/JrX;->j:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/03V;

    const-string v12, "ClientOnlyDeltas"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "No handler for client-only delta type: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v12, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743784
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 2743785
    :cond_6
    move-object v4, v10

    .line 2743786
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2743787
    const/4 v2, 0x0

    new-array v2, v2, [B

    .line 2743788
    :goto_2
    move-object v0, v2

    .line 2743789
    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2743790
    :cond_7
    invoke-static {v4}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    div-int/lit8 v2, v2, 0x8

    add-int/lit8 v2, v2, 0x1

    new-array v3, v2, [B

    .line 2743791
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2743792
    array-length v5, v3

    div-int/lit8 v6, v2, 0x8

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    aget-byte v6, v3, v5

    const/4 v7, 0x1

    rem-int/lit8 v2, v2, 0x8

    shl-int v2, v7, v2

    or-int/2addr v2, v6

    int-to-byte v2, v2

    aput-byte v2, v3, v5

    goto :goto_3

    :cond_8
    move-object v2, v3

    .line 2743793
    goto :goto_2
.end method
