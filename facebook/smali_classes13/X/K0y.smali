.class public final LX/K0y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/csslayout/YogaMeasureFunction;


# instance fields
.field public final synthetic a:Lcom/facebook/react/views/text/ReactTextShadowNode;


# direct methods
.method public constructor <init>(Lcom/facebook/react/views/text/ReactTextShadowNode;)V
    .locals 0

    .prologue
    .line 2760225
    iput-object p1, p0, LX/K0y;->a:Lcom/facebook/react/views/text/ReactTextShadowNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 9

    .prologue
    .line 2760226
    sget-object v2, Lcom/facebook/react/views/text/ReactTextShadowNode;->e:Landroid/text/TextPaint;

    .line 2760227
    iget-object v0, p0, LX/K0y;->a:Lcom/facebook/react/views/text/ReactTextShadowNode;

    iget-object v0, v0, Lcom/facebook/react/views/text/ReactTextShadowNode;->v:Landroid/text/Spannable;

    const-string v1, "Spannable element has not been prepared in onBeforeLayout"

    invoke-static {v0, v1}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Spanned;

    .line 2760228
    invoke-static {v1, v2}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;

    move-result-object v7

    .line 2760229
    if-nez v7, :cond_2

    invoke-static {v1, v2}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    move v3, v0

    .line 2760230
    :goto_0
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-eq p3, v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 2760231
    :goto_1
    if-nez v7, :cond_4

    if-nez v0, :cond_1

    invoke-static {v3}, LX/1mo;->a(F)Z

    move-result v4

    if-nez v4, :cond_4

    cmpg-float v4, v3, p2

    if-gtz v4, :cond_4

    .line 2760232
    :cond_1
    new-instance v0, Landroid/text/StaticLayout;

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2760233
    :goto_2
    iget-object v1, p0, LX/K0y;->a:Lcom/facebook/react/views/text/ReactTextShadowNode;

    iget v1, v1, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    iget-object v1, p0, LX/K0y;->a:Lcom/facebook/react/views/text/ReactTextShadowNode;

    iget v1, v1, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 2760234
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/K0y;->a:Lcom/facebook/react/views/text/ReactTextShadowNode;

    iget v2, v2, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v0

    invoke-static {v1, v0}, LX/1Dr;->a(II)J

    move-result-wide v0

    .line 2760235
    :goto_3
    return-wide v0

    .line 2760236
    :cond_2
    const/high16 v0, 0x7fc00000    # NaNf

    move v3, v0

    goto :goto_0

    .line 2760237
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2760238
    :cond_4
    if-eqz v7, :cond_6

    if-nez v0, :cond_5

    iget v0, v7, Landroid/text/BoringLayout$Metrics;->width:I

    int-to-float v0, v0

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_6

    .line 2760239
    :cond_5
    iget v3, v7, Landroid/text/BoringLayout$Metrics;->width:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v8, 0x1

    invoke-static/range {v1 .. v8}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    move-result-object v0

    goto :goto_2

    .line 2760240
    :cond_6
    new-instance v0, Landroid/text/StaticLayout;

    float-to-int v3, p2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    goto :goto_2

    .line 2760241
    :cond_7
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v0

    invoke-static {v1, v0}, LX/1Dr;->a(II)J

    move-result-wide v0

    goto :goto_3
.end method
