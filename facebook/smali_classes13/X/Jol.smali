.class public LX/Jol;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Joj;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/user/model/User;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/Joj;LX/0Ot;Lcom/facebook/user/model/User;LX/0W3;)V
    .locals 0
    .param p3    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Joj;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/user/model/User;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2736103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736104
    iput-object p1, p0, LX/Jol;->a:LX/Joj;

    .line 2736105
    iput-object p2, p0, LX/Jol;->b:LX/0Ot;

    .line 2736106
    iput-object p3, p0, LX/Jol;->c:Lcom/facebook/user/model/User;

    .line 2736107
    iput-object p4, p0, LX/Jol;->d:LX/0W3;

    .line 2736108
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DiP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2736073
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DiP;

    .line 2736074
    sget-object v1, LX/Jok;->a:[I

    .line 2736075
    iget-object v3, v0, LX/DiP;->a:LX/DiQ;

    move-object v3, v3

    .line 2736076
    invoke-virtual {v3}, LX/DiQ;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 2736077
    iget-object v1, p0, LX/Jol;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v3, "OmniMDirectiveHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error parsing directive type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2736078
    iget-object v5, v0, LX/DiP;->a:LX/DiQ;

    move-object v0, v5

    .line 2736079
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2736080
    :pswitch_0
    iget-object v7, v0, LX/DiP;->b:LX/0lF;

    move-object v7, v7

    .line 2736081
    const-string v8, "actions"

    invoke-virtual {v7, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2736082
    if-nez v7, :cond_2

    .line 2736083
    iget-object v7, p0, LX/Jol;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/03V;

    const-string v8, "OmniMDirectiveHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Add actions directive data has no actions: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2736084
    iget-object v10, v0, LX/DiP;->b:LX/0lF;

    move-object v10, v10

    .line 2736085
    invoke-virtual {v10}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736086
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 2736087
    :goto_1
    move-object v6, v7

    .line 2736088
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    const/4 v7, 0x0

    move v8, v7

    :goto_2
    if-ge v8, v9, :cond_0

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/DiN;

    .line 2736089
    iget-object v10, p0, LX/Jol;->a:LX/Joj;

    invoke-virtual {v10, v7}, LX/Joj;->a(LX/DiN;)V

    .line 2736090
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 2736091
    :cond_0
    goto/16 :goto_0

    .line 2736092
    :pswitch_1
    iget-object v0, p0, LX/Jol;->a:LX/Joj;

    invoke-virtual {v0}, LX/Joj;->clearUserData()V

    goto/16 :goto_0

    .line 2736093
    :cond_1
    return-void

    .line 2736094
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2736095
    invoke-virtual {v7}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 2736096
    :cond_3
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2736097
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0lF;

    iget-object v10, p0, LX/Jol;->c:Lcom/facebook/user/model/User;

    .line 2736098
    iget-object v11, v10, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v10, v11

    .line 2736099
    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v7, v11, v12}, LX/DiN;->a(LX/0lF;J)LX/DiN;

    move-result-object v7

    .line 2736100
    if-eqz v7, :cond_3

    .line 2736101
    invoke-virtual {v8, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2736102
    :cond_4
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/Jol;
    .locals 5

    .prologue
    .line 2736050
    new-instance v3, LX/Jol;

    invoke-static {p0}, LX/Joj;->a(LX/0QB;)LX/Joj;

    move-result-object v0

    check-cast v0, LX/Joj;

    const/16 v1, 0x259

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v2

    check-cast v2, LX/0W3;

    invoke-direct {v3, v0, v4, v1, v2}, LX/Jol;-><init>(LX/Joj;LX/0Ot;Lcom/facebook/user/model/User;LX/0W3;)V

    .line 2736051
    return-object v3
.end method


# virtual methods
.method public final a(LX/0lF;)V
    .locals 8

    .prologue
    .line 2736052
    iget-object v0, p0, LX/Jol;->d:LX/0W3;

    sget-wide v2, LX/0X5;->jW:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2736053
    :goto_0
    return-void

    .line 2736054
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2736055
    invoke-virtual {p1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2736056
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2736057
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2736058
    const/4 v3, 0x0

    .line 2736059
    const-string v4, "type"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 2736060
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2736061
    const-string v4, "OmniMDirective"

    const-string v5, "Directive missing type"

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2736062
    :goto_2
    move-object v3, v3

    .line 2736063
    if-nez v3, :cond_1

    .line 2736064
    const-string v3, "OmniMDirectiveHandler"

    const-string v4, "Error parsing directive: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2736065
    :cond_1
    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2736066
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2736067
    invoke-direct {p0, v0}, LX/Jol;->a(Ljava/util/List;)V

    goto :goto_0

    .line 2736068
    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, LX/DiQ;->fromId(I)LX/DiQ;

    move-result-object v5

    .line 2736069
    if-nez v5, :cond_4

    .line 2736070
    const-string v5, "OmniMDirective"

    const-string v6, "Unknown directive type id: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v4, v7, p1

    invoke-static {v5, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2736071
    :cond_4
    const-string v3, "data"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 2736072
    new-instance v3, LX/DiP;

    invoke-direct {v3, v5, v4}, LX/DiP;-><init>(LX/DiQ;LX/0lF;)V

    goto :goto_2
.end method
