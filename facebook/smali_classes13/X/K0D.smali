.class public LX/K0D;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2758023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5pC;[F)I
    .locals 4

    .prologue
    .line 2758024
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v0

    array-length v1, p1

    if-le v0, v1, :cond_0

    array-length v0, p1

    .line 2758025
    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 2758026
    invoke-interface {p0, v1}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, p1, v1

    .line 2758027
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2758028
    :cond_0
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v0

    goto :goto_0

    .line 2758029
    :cond_1
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v0

    return v0
.end method

.method public static a(LX/5pC;)[F
    .locals 1
    .param p0    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2758030
    if-eqz p0, :cond_0

    .line 2758031
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v0

    new-array v0, v0, [F

    .line 2758032
    invoke-static {p0, v0}, LX/K0D;->a(LX/5pC;[F)I

    .line 2758033
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
