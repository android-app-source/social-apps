.class public LX/Jbt;
.super LX/796;
.source ""


# instance fields
.field private final b:LX/0xC;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0xC;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/795;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1MT;",
            ">;>;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportActivityFileProvider;",
            ">;>;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Ze;",
            ">;>;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/4lA;",
            ">;>;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/792;",
            ">;",
            "LX/0Or",
            "<",
            "LX/793;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0l0;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0W9;",
            ">;",
            "LX/0xC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717234
    invoke-direct/range {p0 .. p13}, LX/796;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2717235
    iput-object p14, p0, LX/Jbt;->b:LX/0xC;

    .line 2717236
    return-void
.end method

.method public static b(LX/0QB;)LX/Jbt;
    .locals 15

    .prologue
    .line 2717242
    new-instance v0, LX/Jbt;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x2fd

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x31a6

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 2717243
    new-instance v5, LX/4lD;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-direct {v5, v6}, LX/4lD;-><init>(LX/0QB;)V

    move-object v5, v5

    .line 2717244
    new-instance v6, LX/4lB;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    invoke-direct {v6, v7}, LX/4lB;-><init>(LX/0QB;)V

    move-object v6, v6

    .line 2717245
    new-instance v7, LX/4lC;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    invoke-direct {v7, v8}, LX/4lC;-><init>(LX/0QB;)V

    move-object v7, v7

    .line 2717246
    new-instance v8, LX/4lE;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v9

    invoke-direct {v8, v9}, LX/4lE;-><init>(LX/0QB;)V

    move-object v8, v8

    .line 2717247
    const/16 v9, 0x455

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x31a3

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x31a4

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0xa1

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x2be

    invoke-static {p0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {p0}, LX/0xC;->a(LX/0QB;)LX/0xC;

    move-result-object v14

    check-cast v14, LX/0xC;

    invoke-direct/range {v0 .. v14}, LX/Jbt;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0xC;)V

    .line 2717248
    return-object v0
.end method


# virtual methods
.method public final b()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2717241
    new-instance v0, LX/Jbs;

    invoke-direct {v0, p0}, LX/Jbs;-><init>(LX/Jbt;)V

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2717249
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const/4 v1, 0x0

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081900

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x13311d38701dfL

    const v6, 0x7f02072b

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081904

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x7e03179efa26L

    const v6, 0x7f02092c

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08190c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0xcf12d3dadf6fL

    const v6, 0x7f02085b

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081910

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x832f4646743dL

    const v6, 0x7f0208be

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081912

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1ecffa3cfefedL

    const v6, 0x7f020911

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081916

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x110c733fce97aL

    const v6, 0x7f020943

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081917

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x7cbeb7d438eeL

    const v6, 0x7f02098b

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081918

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x73fa5c4a8838L

    const v6, 0x7f020743

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08191b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1f4f089d1d620L

    const v6, 0x7f02095e

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08191d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x10cce0c2ea175L

    const v6, 0x7f020914

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08191e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1963f36bb3c2eL

    const v6, 0x7f02098f

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08191c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x15dc32146a603L    # 1.90001656953293E-309

    const v6, 0x7f020a20

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081921

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1af5b08493673L

    const v6, 0x7f02091d

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081923

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0xa795854cf461L

    const v6, 0x7f020886

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081933

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x822e48768e6aL

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081928

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0xbf148a6a18adL

    const v6, 0x7f020722

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2717250
    iget-object v1, p0, LX/Jbt;->b:LX/0xC;

    invoke-virtual {v1}, LX/0xC;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2717251
    new-instance v1, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v2, p0, LX/78l;->a:Landroid/content/Context;

    const v3, 0x7f081934

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide v4, 0x23307124cfd26L

    const v3, 0x7f020737

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2717252
    :cond_0
    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2717239
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const/4 v1, 0x0

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Bookmarks / Navigation"

    const-wide v4, 0x10ee5de20282dL

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Camera"

    const-wide v4, 0x201a94e08dea3L

    const v6, 0x7f0209c5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Check Ins / Places"

    const-wide v4, 0x16c25e946e792L

    const v6, 0x7f020966

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Comments / Likes / Shares"

    const-wide v4, 0x5ddbfd8e4f68L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Composer"

    const-wide v4, 0x104e02b717630L

    const v6, 0x7f02080f

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Friend Sharing"

    const-wide v4, 0x6144b4c518be9L

    const v6, 0x7f0209c5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Platform / Games / Apps"

    const-wide v4, 0x165b63cee4dc5L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Login"

    const-wide v4, 0x19e8177fe1864L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Place Search / Local"

    const-wide v4, 0x1645502bcff15L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Place Tips"

    const-wide v4, 0x260f75bc7e143L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const-string v3, "Public Content / Hashtags / Topics"

    const-wide v4, 0xe2fa82a993a1L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2717240
    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2717237
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    const/4 v1, 0x0

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081908

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x16c25e946e792L

    const v6, 0x7f020912

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08190b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x104e02b717630L

    const v6, 0x7f020846

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f08190f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x165b63cee4dc5L

    const v6, 0x7f02074d

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    iget-object v3, p0, LX/78l;->a:Landroid/content/Context;

    const v4, 0x7f081913

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1645502bcff15L

    const v6, 0x7f020737

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;-><init>(Ljava/lang/String;JI)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2717238
    return-object v0
.end method
