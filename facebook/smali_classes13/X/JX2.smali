.class public LX/JX2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/JX0;

.field public final b:LX/3mL;


# direct methods
.method public constructor <init>(LX/JX0;LX/3mL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703793
    iput-object p1, p0, LX/JX2;->a:LX/JX0;

    .line 2703794
    iput-object p2, p0, LX/JX2;->b:LX/3mL;

    .line 2703795
    return-void
.end method

.method public static a(LX/JX2;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;)LX/25M;
    .locals 2
    .param p0    # LX/JX2;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;",
            ")",
            "LX/25M;"
        }
    .end annotation

    .prologue
    .line 2703796
    new-instance v0, LX/JX1;

    invoke-direct {v0, p0, p2}, LX/JX1;-><init>(LX/JX2;Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;)V

    .line 2703797
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v1

    .line 2703798
    iput-object v0, v1, LX/3mP;->g:LX/25K;

    .line 2703799
    move-object v0, v1

    .line 2703800
    iput-object p2, v0, LX/3mP;->e:LX/0jW;

    .line 2703801
    move-object v0, v0

    .line 2703802
    iput-object p1, v0, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703803
    move-object v0, v0

    .line 2703804
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v1

    .line 2703805
    iput-object v1, v0, LX/3mP;->d:LX/25L;

    .line 2703806
    move-object v0, v0

    .line 2703807
    const/16 v1, 0x8

    .line 2703808
    iput v1, v0, LX/3mP;->b:I

    .line 2703809
    move-object v0, v0

    .line 2703810
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JX2;
    .locals 5

    .prologue
    .line 2703811
    const-class v1, LX/JX2;

    monitor-enter v1

    .line 2703812
    :try_start_0
    sget-object v0, LX/JX2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703813
    sput-object v2, LX/JX2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703814
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703815
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703816
    new-instance p0, LX/JX2;

    const-class v3, LX/JX0;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/JX0;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-direct {p0, v3, v4}, LX/JX2;-><init>(LX/JX0;LX/3mL;)V

    .line 2703817
    move-object v0, p0

    .line 2703818
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703819
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JX2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703820
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703821
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
