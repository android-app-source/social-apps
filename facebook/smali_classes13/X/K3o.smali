.class public final LX/K3o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766117
    iput-object p1, p0, LX/K3o;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1afff065

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2766118
    iget-object v1, p0, LX/K3o;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766119
    iget v2, v1, LX/K4o;->y:I

    iget-object p1, v1, LX/K4o;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2766120
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/K3o;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766121
    iget-object v2, v1, LX/K4o;->p:LX/K4l;

    move-object v1, v2

    .line 2766122
    sget-object v2, LX/K4l;->READY_TO_PLAY:LX/K4l;

    if-ne v1, v2, :cond_0

    .line 2766123
    iget-object v1, p0, LX/K3o;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v1}, LX/K4o;->a()V

    .line 2766124
    const v1, -0x580e1c47

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2766125
    :goto_1
    return-void

    .line 2766126
    :cond_0
    iget-object v1, p0, LX/K3o;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766127
    sget-object v2, LX/K4i;->a:[I

    iget-object v3, v1, LX/K4o;->p:LX/K4l;

    invoke-virtual {v3}, LX/K4l;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2766128
    :goto_2
    const v1, 0x6583f9a8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 2766129
    :pswitch_0
    invoke-virtual {v1}, LX/K4o;->b()V

    goto :goto_2

    .line 2766130
    :pswitch_1
    invoke-virtual {v1}, LX/K4o;->d()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
