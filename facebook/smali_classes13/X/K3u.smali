.class public final LX/K3u;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/ipc/media/MediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766204
    iput-object p1, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2766193
    iget-object v0, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2766194
    :goto_0
    return-void

    .line 2766195
    :cond_0
    iget-object v0, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766196
    iput-object p1, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    .line 2766197
    iget-object v0, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->m(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2766198
    iget-object v0, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766199
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object p0, LX/0ig;->V:LX/0ih;

    const-string p1, "no_recent_photos_to_prefill"

    invoke-virtual {v1, p0, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2766200
    goto :goto_0

    .line 2766201
    :cond_1
    iget-object v0, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766202
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string p1, "recent_photos_prefilled"

    invoke-virtual {v1, v2, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2766203
    iget-object v0, p0, LX/K3u;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->v(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2766191
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2766192
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/K3u;->a(LX/0Px;)V

    return-void
.end method
