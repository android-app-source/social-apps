.class public final LX/JkS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/JoinRequestNotification;

.field public final synthetic b:Landroid/app/PendingIntent;

.field public final synthetic c:I

.field public final synthetic d:LX/JkU;


# direct methods
.method public constructor <init>(LX/JkU;Lcom/facebook/messaging/notify/JoinRequestNotification;Landroid/app/PendingIntent;I)V
    .locals 0

    .prologue
    .line 2729839
    iput-object p1, p0, LX/JkS;->d:LX/JkU;

    iput-object p2, p0, LX/JkS;->a:Lcom/facebook/messaging/notify/JoinRequestNotification;

    iput-object p3, p0, LX/JkS;->b:Landroid/app/PendingIntent;

    iput p4, p0, LX/JkS;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 2729872
    iget-object v0, p0, LX/JkS;->d:LX/JkU;

    const/4 v1, 0x0

    iget-object v2, p0, LX/JkS;->a:Lcom/facebook/messaging/notify/JoinRequestNotification;

    iget-object v3, p0, LX/JkS;->b:Landroid/app/PendingIntent;

    iget v4, p0, LX/JkS;->c:I

    const/4 v5, 0x0

    .line 2729873
    const/4 v10, 0x2

    const/4 p0, 0x1

    const/4 v12, 0x0

    .line 2729874
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2729875
    new-instance v7, LX/2HB;

    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    invoke-direct {v7, v8}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v7

    .line 2729876
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const/high16 v9, 0x4000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3GK;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3RH;->J:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_key_for_settings"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_settings_type_for_settings"

    sget-object v11, LX/Jod;->GROUP:LX/Jod;

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "start_fragment"

    const/16 v11, 0x3e9

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    .line 2729877
    invoke-static {v0, v8}, LX/JkU;->a(LX/JkU;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v8

    move-object v8, v8

    .line 2729878
    iput-object v8, v7, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2729879
    move-object v7, v7

    .line 2729880
    invoke-virtual {v7, v3}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v7

    invoke-virtual {v7, p0}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v7

    .line 2729881
    iget-object v8, v0, LX/JkU;->h:LX/2Og;

    invoke-virtual {v8, v6}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 2729882
    if-nez v5, :cond_0

    if-eqz v6, :cond_0

    iget-object v8, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v8, v8, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v8, v10, :cond_2

    .line 2729883
    :cond_0
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v6, v8, p0}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2729884
    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v8, v9, v12}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v8

    .line 2729885
    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b16

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v12, v9, v6}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v6

    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b17

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v12, v9, v8}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2729886
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->b:Ljava/lang/String;

    invoke-virtual {v7, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 2729887
    :goto_0
    if-eqz v1, :cond_1

    .line 2729888
    iput-object v1, v7, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2729889
    :cond_1
    iget-object v6, v0, LX/JkU;->e:LX/3RQ;

    invoke-virtual {v6, v7}, LX/3RQ;->a(LX/2HB;)Z

    .line 2729890
    iget-object v6, v0, LX/JkU;->i:LX/3RK;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x272f

    invoke-virtual {v7}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v6, v8, v9, v7}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2729891
    iput-boolean p0, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->e:Z

    .line 2729892
    invoke-virtual {v2}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2729893
    return-void

    .line 2729894
    :cond_2
    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2729895
    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    const v9, 0x7f083b25

    new-array v10, v10, [Ljava/lang/Object;

    iget-object v11, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v11, v11, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    aput-object v6, v10, p0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2729896
    :goto_1
    invoke-virtual {v7, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    goto :goto_0

    .line 2729897
    :cond_3
    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    const v9, 0x7f083b24

    new-array v10, p0, [Ljava/lang/Object;

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public final a(LX/1FJ;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2729840
    const/4 v1, 0x0

    .line 2729841
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 2729842
    instance-of v2, v0, LX/1lm;

    if-eqz v2, :cond_0

    .line 2729843
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2729844
    :cond_0
    iget-object v0, p0, LX/JkS;->d:LX/JkU;

    iget-object v2, p0, LX/JkS;->a:Lcom/facebook/messaging/notify/JoinRequestNotification;

    iget-object v3, p0, LX/JkS;->b:Landroid/app/PendingIntent;

    iget v4, p0, LX/JkS;->c:I

    const/4 v5, 0x0

    .line 2729845
    const/4 v10, 0x2

    const/4 p0, 0x1

    const/4 v12, 0x0

    .line 2729846
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2729847
    new-instance v7, LX/2HB;

    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    invoke-direct {v7, v8}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v7

    .line 2729848
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const/high16 v9, 0x4000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3GK;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    sget-object v9, LX/3RH;->J:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_key_for_settings"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "thread_settings_type_for_settings"

    sget-object v11, LX/Jod;->GROUP:LX/Jod;

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "start_fragment"

    const/16 v11, 0x3e9

    invoke-virtual {v8, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    .line 2729849
    invoke-static {v0, v8}, LX/JkU;->a(LX/JkU;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v8

    move-object v8, v8

    .line 2729850
    iput-object v8, v7, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2729851
    move-object v7, v7

    .line 2729852
    invoke-virtual {v7, v3}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v7

    invoke-virtual {v7, p0}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v7

    .line 2729853
    iget-object v8, v0, LX/JkU;->h:LX/2Og;

    invoke-virtual {v8, v6}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 2729854
    if-nez v5, :cond_1

    if-eqz v6, :cond_1

    iget-object v8, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v8, v8, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v8, v10, :cond_3

    .line 2729855
    :cond_1
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v6, v8, p0}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2729856
    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v8, v9, v12}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;

    move-result-object v8

    .line 2729857
    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b16

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v12, v9, v6}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v6

    iget-object v9, v0, LX/JkU;->c:Landroid/content/Context;

    const v10, 0x7f083b17

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v12, v9, v8}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2729858
    iget-object v6, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->b:Ljava/lang/String;

    invoke-virtual {v7, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 2729859
    :goto_0
    if-eqz v1, :cond_2

    .line 2729860
    iput-object v1, v7, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2729861
    :cond_2
    iget-object v6, v0, LX/JkU;->e:LX/3RQ;

    invoke-virtual {v6, v7}, LX/3RQ;->a(LX/2HB;)Z

    .line 2729862
    iget-object v6, v0, LX/JkU;->i:LX/3RK;

    iget-object v8, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x272f

    invoke-virtual {v7}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v6, v8, v9, v7}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2729863
    iput-boolean p0, v2, Lcom/facebook/messaging/notify/JoinRequestNotification;->e:Z

    .line 2729864
    invoke-virtual {v2}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2729865
    invoke-static {p1}, LX/1FJ;->c(LX/1FJ;)V

    .line 2729866
    return-void

    .line 2729867
    :catchall_0
    move-exception v0

    invoke-static {p1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 2729868
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2729869
    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    const v9, 0x7f083b25

    new-array v10, v10, [Ljava/lang/Object;

    iget-object v11, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v11, v11, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    aput-object v6, v10, p0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2729870
    :goto_1
    invoke-virtual {v7, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    goto :goto_0

    .line 2729871
    :cond_4
    iget-object v8, v0, LX/JkU;->c:Landroid/content/Context;

    const v9, 0x7f083b24

    new-array v10, p0, [Ljava/lang/Object;

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method
