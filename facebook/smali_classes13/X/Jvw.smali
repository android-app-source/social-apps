.class public LX/Jvw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationProcessor",
        "<",
        "Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryInterfaces$GravitySuggestifierQuery$Suggestions$Edges;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/0tX;

.field public final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final d:LX/9jj;

.field public final e:LX/0yD;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/9jj;LX/0yD;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2751303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2751304
    iput-object p1, p0, LX/Jvw;->a:Ljava/util/concurrent/Executor;

    .line 2751305
    iput-object p2, p0, LX/Jvw;->b:LX/0tX;

    .line 2751306
    iput-object p3, p0, LX/Jvw;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2751307
    iput-object p4, p0, LX/Jvw;->d:LX/9jj;

    .line 2751308
    iput-object p5, p0, LX/Jvw;->e:LX/0yD;

    .line 2751309
    return-void
.end method

.method private static a(Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2751302
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2751310
    check-cast p1, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;

    invoke-static {p1}, LX/Jvw;->a(Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/location/ImmutableLocation;Ljava/lang/Object;LX/2ct;Ljava/lang/String;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)V
    .locals 5
    .param p1    # Lcom/facebook/location/ImmutableLocation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2751261
    check-cast p2, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;

    const/4 v1, 0x0

    .line 2751262
    invoke-virtual {p2}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;->b()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2751263
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v2

    .line 2751264
    invoke-static {p1}, LX/Jw1;->a(Lcom/facebook/location/ImmutableLocation;)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/2ct;->a(Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/CeG;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 2751265
    iput-object v4, v3, LX/CeG;->c:Ljava/lang/String;

    .line 2751266
    move-object v3, v3

    .line 2751267
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 2751268
    iput-object v4, v3, LX/CeG;->d:Ljava/lang/String;

    .line 2751269
    move-object v3, v3

    .line 2751270
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->b()LX/0Px;

    move-result-object v0

    .line 2751271
    iput-object v0, v3, LX/CeG;->e:LX/0Px;

    .line 2751272
    move-object v0, v3

    .line 2751273
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->c()LX/175;

    move-result-object v3

    .line 2751274
    iput-object v3, v0, LX/CeG;->g:LX/175;

    .line 2751275
    move-object v0, v0

    .line 2751276
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->b()LX/175;

    move-result-object v3

    .line 2751277
    iput-object v3, v0, LX/CeG;->h:LX/175;

    .line 2751278
    move-object v3, v0

    .line 2751279
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2751280
    :goto_0
    iput-boolean v0, v3, LX/CeG;->i:Z

    .line 2751281
    move-object v3, v3

    .line 2751282
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;->b()LX/175;

    move-result-object v0

    .line 2751283
    :goto_1
    iput-object v0, v3, LX/CeG;->j:LX/175;

    .line 2751284
    move-object v3, v3

    .line 2751285
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;->a()LX/175;

    move-result-object v0

    .line 2751286
    :goto_2
    iput-object v0, v3, LX/CeG;->k:LX/175;

    .line 2751287
    move-object v0, v3

    .line 2751288
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel$FooterModel;->c()LX/175;

    move-result-object v1

    .line 2751289
    :cond_0
    iput-object v1, v0, LX/CeG;->l:LX/175;

    .line 2751290
    move-object v0, v0

    .line 2751291
    invoke-static {p2}, LX/Jvw;->a(Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;)Ljava/lang/String;

    move-result-object v1

    .line 2751292
    iput-object v1, v0, LX/CeG;->n:Ljava/lang/String;

    .line 2751293
    move-object v0, v0

    .line 2751294
    invoke-virtual {p2}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;->a()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    move-result-object v1

    invoke-static {v1}, LX/Cdj;->from(Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;)LX/Cdj;

    move-result-object v1

    .line 2751295
    iput-object v1, v0, LX/CeG;->p:LX/Cdj;

    .line 2751296
    move-object v0, v0

    .line 2751297
    iput-object p5, v0, LX/CeG;->q:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;

    .line 2751298
    move-object v0, v0

    .line 2751299
    invoke-virtual {v0}, LX/CeG;->a()LX/0bZ;

    .line 2751300
    return-void

    .line 2751301
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
