.class public LX/JZE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2708157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTarotDigest;
    .locals 2

    .prologue
    .line 2708158
    invoke-static {p0}, LX/JZE;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 2708159
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2708160
    :cond_0
    const/4 v0, 0x0

    .line 2708161
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTarotCard;Z)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2708162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotCard;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    .line 2708163
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2708164
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 2708165
    :cond_0
    :goto_0
    return-object v0

    .line 2708166
    :cond_1
    if-eqz p1, :cond_0

    .line 2708167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotCard;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 2708168
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2708169
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2708170
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2708171
    invoke-static {p0}, LX/JZE;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v3

    .line 2708172
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2708173
    :goto_0
    return-object v0

    .line 2708174
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2708175
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2708176
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 2708177
    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotDigest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2708178
    invoke-static {p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 2708179
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2708180
    :cond_0
    const/4 v0, 0x0

    .line 2708181
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
