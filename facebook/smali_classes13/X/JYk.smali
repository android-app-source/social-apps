.class public final LX/JYk;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JYl;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Landroid/net/Uri;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/String;

.field public final synthetic f:LX/JYl;


# direct methods
.method public constructor <init>(LX/JYl;)V
    .locals 1

    .prologue
    .line 2707122
    iput-object p1, p0, LX/JYk;->f:LX/JYl;

    .line 2707123
    move-object v0, p1

    .line 2707124
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2707125
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2707126
    const-string v0, "FundraiserAttachmentHeaderContentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2707100
    if-ne p0, p1, :cond_1

    .line 2707101
    :cond_0
    :goto_0
    return v0

    .line 2707102
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2707103
    goto :goto_0

    .line 2707104
    :cond_3
    check-cast p1, LX/JYk;

    .line 2707105
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2707106
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2707107
    if-eq v2, v3, :cond_0

    .line 2707108
    iget-boolean v2, p0, LX/JYk;->a:Z

    iget-boolean v3, p1, LX/JYk;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2707109
    goto :goto_0

    .line 2707110
    :cond_4
    iget-object v2, p0, LX/JYk;->b:Landroid/net/Uri;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/JYk;->b:Landroid/net/Uri;

    iget-object v3, p1, LX/JYk;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2707111
    goto :goto_0

    .line 2707112
    :cond_6
    iget-object v2, p1, LX/JYk;->b:Landroid/net/Uri;

    if-nez v2, :cond_5

    .line 2707113
    :cond_7
    iget-object v2, p0, LX/JYk;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/JYk;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JYk;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2707114
    goto :goto_0

    .line 2707115
    :cond_9
    iget-object v2, p1, LX/JYk;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 2707116
    :cond_a
    iget-object v2, p0, LX/JYk;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/JYk;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/JYk;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2707117
    goto :goto_0

    .line 2707118
    :cond_c
    iget-object v2, p1, LX/JYk;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_b

    .line 2707119
    :cond_d
    iget-object v2, p0, LX/JYk;->e:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/JYk;->e:Ljava/lang/String;

    iget-object v3, p1, LX/JYk;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2707120
    goto :goto_0

    .line 2707121
    :cond_e
    iget-object v2, p1, LX/JYk;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
