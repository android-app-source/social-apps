.class public final LX/K3v;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766205
    iput-object p1, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2766206
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2766207
    if-eqz v0, :cond_2

    .line 2766208
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2766209
    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel$MovieFactoryConfigModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2766210
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2766211
    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel$MovieFactoryConfigModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel$MovieFactoryConfigModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2766212
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2766213
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2766214
    iget-object v1, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766215
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2766216
    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel$MovieFactoryConfigModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$FBStorylineMoodsVisualMoodsAndCategoriesConfigQueryModel$MovieFactoryConfigModel;->a()LX/0Px;

    move-result-object v0

    .line 2766217
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2766218
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v12

    const/4 v2, 0x0

    move v10, v2

    :goto_0
    if-ge v10, v12, :cond_0

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2766219
    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    move-result-object v2

    .line 2766220
    new-instance v6, Lcom/facebook/storyline/model/Mood$Icon;

    invoke-virtual {v2}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->k()I

    move-result v4

    invoke-virtual {v2}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->a()I

    move-result v2

    invoke-direct {v6, v3, v4, v2}, Lcom/facebook/storyline/model/Mood$Icon;-><init>(Ljava/lang/String;II)V

    .line 2766221
    new-instance v2, Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->n()Z

    move-result v5

    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k()LX/0Px;

    move-result-object v7

    invoke-static {v7}, LX/K4I;->b(LX/0Px;)LX/0Px;

    move-result-object v7

    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->o()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lcom/facebook/storyline/model/Mood;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/storyline/model/Mood$Icon;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2766222
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_0

    .line 2766223
    :cond_0
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2766224
    iput-object v0, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    .line 2766225
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2766226
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766227
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string v3, "fetch_moods_succeeded"

    invoke-virtual {v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2766228
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766229
    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->o$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766230
    :goto_1
    return-void

    .line 2766231
    :cond_1
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766232
    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->p$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766233
    goto :goto_1

    .line 2766234
    :cond_2
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766235
    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->p$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766236
    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2766237
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766238
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string p1, "fetch_moods_failed"

    invoke-virtual {v1, v2, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2766239
    iget-object v0, p0, LX/K3v;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766240
    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->p$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766241
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2766242
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/K3v;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
