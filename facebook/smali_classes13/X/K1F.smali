.class public final LX/K1F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/5rJ;

.field public final synthetic b:LX/K19;

.field public final synthetic c:Lcom/facebook/react/views/textinput/ReactTextInputManager;


# direct methods
.method public constructor <init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/5rJ;LX/K19;)V
    .locals 0

    .prologue
    .line 2761653
    iput-object p1, p0, LX/K1F;->c:Lcom/facebook/react/views/textinput/ReactTextInputManager;

    iput-object p2, p0, LX/K1F;->a:LX/5rJ;

    iput-object p3, p0, LX/K1F;->b:LX/K19;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 2761654
    iget-object v0, p0, LX/K1F;->a:LX/5rJ;

    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2761655
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2761656
    if-eqz p2, :cond_0

    .line 2761657
    new-instance v1, LX/K1E;

    iget-object v2, p0, LX/K1F;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getId()I

    move-result v2

    invoke-direct {v1, v2}, LX/K1E;-><init>(I)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2761658
    :goto_0
    return-void

    .line 2761659
    :cond_0
    new-instance v1, LX/K1B;

    iget-object v2, p0, LX/K1F;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getId()I

    move-result v2

    invoke-direct {v1, v2}, LX/K1B;-><init>(I)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2761660
    new-instance v1, LX/K1C;

    iget-object v2, p0, LX/K1F;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getId()I

    move-result v2

    iget-object v3, p0, LX/K1F;->b:LX/K19;

    invoke-virtual {v3}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/K1C;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0
.end method
