.class public LX/Jaj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2715759
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2715760
    invoke-static {}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->m()LX/F5b;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "/groups_discovery"

    .line 2715761
    iput-object v2, v1, LX/98r;->a:Ljava/lang/String;

    .line 2715762
    move-object v1, v1

    .line 2715763
    const-string v2, "FBGroupsHubSearchRoute"

    .line 2715764
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2715765
    move-object v1, v1

    .line 2715766
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5b;->a(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    const/4 v1, 0x1

    .line 2715767
    iget-object v2, v0, LX/F5b;->a:Landroid/os/Bundle;

    const-string p0, "show_search_bar"

    invoke-virtual {v2, p0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2715768
    move-object v0, v0

    .line 2715769
    invoke-virtual {v0}, LX/F5b;->a()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    return-object v0
.end method
