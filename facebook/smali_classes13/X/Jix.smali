.class public LX/Jix;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/2CH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3Kx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Js8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/FJv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Lcom/facebook/widget/tiles/ThreadTileView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/view/View;

.field private m:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field public o:Lcom/facebook/user/model/UserKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final p:LX/9l7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2727150
    const v0, 0x7f0104ec

    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727151
    new-instance v0, LX/Jiw;

    invoke-direct {v0, p0}, LX/Jiw;-><init>(LX/Jix;)V

    iput-object v0, p0, LX/Jix;->p:LX/9l7;

    .line 2727152
    const v0, 0x7f0104ec

    invoke-direct {p0, p1, v1, v0}, LX/Jix;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727153
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727162
    const v0, 0x7f0104ec

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727163
    new-instance v0, LX/Jiw;

    invoke-direct {v0, p0}, LX/Jiw;-><init>(LX/Jix;)V

    iput-object v0, p0, LX/Jix;->p:LX/9l7;

    .line 2727164
    const v0, 0x7f0104ec

    invoke-direct {p0, p1, p2, v0}, LX/Jix;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727158
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727159
    new-instance v0, LX/Jiw;

    invoke-direct {v0, p0}, LX/Jiw;-><init>(LX/Jix;)V

    iput-object v0, p0, LX/Jix;->p:LX/9l7;

    .line 2727160
    invoke-direct {p0, p1, p2, p3}, LX/Jix;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727161
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2727155
    iget-object v0, p0, LX/Jix;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2727156
    iget-object v0, p0, LX/Jix;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2727157
    return-void
.end method

.method private static a(LX/Jix;LX/2CH;LX/3Kx;LX/1Ad;LX/0Or;LX/Js8;LX/FJv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jix;",
            "Lcom/facebook/presence/PresenceManager;",
            "LX/3Kx;",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/Js8;",
            "LX/FJv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2727154
    iput-object p1, p0, LX/Jix;->a:LX/2CH;

    iput-object p2, p0, LX/Jix;->b:LX/3Kx;

    iput-object p3, p0, LX/Jix;->c:LX/1Ad;

    iput-object p4, p0, LX/Jix;->d:LX/0Or;

    iput-object p5, p0, LX/Jix;->e:LX/Js8;

    iput-object p6, p0, LX/Jix;->f:LX/FJv;

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2727115
    const-class v0, LX/Jix;

    invoke-static {v0, p0}, LX/Jix;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2727116
    const v0, 0x7f030396

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2727117
    const v0, 0x7f0d0b7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/Jix;->g:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2727118
    const v0, 0x7f0d0b7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jix;->h:Landroid/widget/TextView;

    .line 2727119
    const v0, 0x7f0d0b80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jix;->i:Landroid/widget/TextView;

    .line 2727120
    const v0, 0x7f0d0b7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jix;->j:Landroid/widget/TextView;

    .line 2727121
    const v0, 0x7f0d0b7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Jix;->k:Landroid/widget/ImageView;

    .line 2727122
    const v0, 0x7f0d0b7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jix;->l:Landroid/view/View;

    .line 2727123
    iget-object v0, p0, LX/Jix;->e:LX/Js8;

    const/4 v4, 0x0

    .line 2727124
    sget-object v2, LX/03r;->ThreadItemView:[I

    invoke-virtual {p1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 2727125
    const/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, v0, LX/Js8;->a:I

    .line 2727126
    const/16 v3, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    iput v3, v0, LX/Js8;->b:I

    .line 2727127
    const/16 v3, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    iput v3, v0, LX/Js8;->c:I

    .line 2727128
    const/16 v3, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    iput v3, v0, LX/Js8;->d:I

    .line 2727129
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 2727130
    iget v2, v0, LX/Js8;->b:I

    if-lez v2, :cond_1

    .line 2727131
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, v0, LX/Js8;->e:Landroid/graphics/Paint;

    .line 2727132
    iget-object v2, v0, LX/Js8;->e:Landroid/graphics/Paint;

    iget v3, v0, LX/Js8;->a:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2727133
    :goto_0
    iget-object v0, p0, LX/Jix;->e:LX/Js8;

    .line 2727134
    iget-object v2, v0, LX/Js8;->e:Landroid/graphics/Paint;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 2727135
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0, v0}, LX/Jix;->setWillNotDraw(Z)V

    .line 2727136
    invoke-virtual {p0}, LX/Jix;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2727137
    const v2, 0x7f0b265a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2727138
    iget-object v3, p0, LX/Jix;->i:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineHeight()I

    move-result v3

    sub-int v2, v3, v2

    iput v2, p0, LX/Jix;->n:I

    .line 2727139
    const v2, 0x7f0203b7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2727140
    iget v3, p0, LX/Jix;->n:I

    iget v4, p0, LX/Jix;->n:I

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2727141
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2727142
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2727143
    move-object v0, v1

    .line 2727144
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2727145
    invoke-static {v0, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, LX/Jix;->m:LX/1aX;

    .line 2727146
    iget-object v0, p0, LX/Jix;->m:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2727147
    return-void

    :cond_0
    move v0, v1

    .line 2727148
    goto :goto_2

    .line 2727149
    :cond_1
    const/4 v2, 0x0

    iput-object v2, v0, LX/Js8;->e:Landroid/graphics/Paint;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Jix;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, LX/Jix;

    invoke-static {v6}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v1

    check-cast v1, LX/2CH;

    invoke-static {v6}, LX/3Kx;->a(LX/0QB;)LX/3Kx;

    move-result-object v2

    check-cast v2, LX/3Kx;

    invoke-static {v6}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    const/16 v4, 0x1453

    invoke-static {v6, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    new-instance v5, LX/Js8;

    invoke-direct {v5}, LX/Js8;-><init>()V

    move-object v5, v5

    move-object v5, v5

    check-cast v5, LX/Js8;

    invoke-static {v6}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v6

    check-cast v6, LX/FJv;

    invoke-static/range {v0 .. v6}, LX/Jix;->a(LX/Jix;LX/2CH;LX/3Kx;LX/1Ad;LX/0Or;LX/Js8;LX/FJv;)V

    return-void
.end method

.method private a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2727166
    iget-object v0, p0, LX/Jix;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2727167
    iget-object v0, p0, LX/Jix;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/Jix;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2727168
    iget-object v0, p0, LX/Jix;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2727169
    iget-object v0, p0, LX/Jix;->k:Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/Jix;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2727170
    iget-object v0, p0, LX/Jix;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2727171
    return-void
.end method

.method private setAndSubscribeToPresenceFromUserId(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2727101
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2727102
    invoke-direct {p0}, LX/Jix;->a()V

    .line 2727103
    :cond_0
    :goto_0
    return-void

    .line 2727104
    :cond_1
    invoke-static {p1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 2727105
    iget-object v1, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2727106
    iget-object v1, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Jix;->p:LX/9l7;

    if-eqz v1, :cond_2

    .line 2727107
    iget-object v1, p0, LX/Jix;->a:LX/2CH;

    iget-object v2, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    iget-object v3, p0, LX/Jix;->p:LX/9l7;

    invoke-virtual {v1, v2, v3}, LX/2CH;->b(Lcom/facebook/user/model/UserKey;LX/9l7;)V

    .line 2727108
    :cond_2
    iput-object v0, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    .line 2727109
    iget-object v0, p0, LX/Jix;->a:LX/2CH;

    iget-object v1, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    .line 2727110
    iget-object v2, v0, LX/2CH;->A:LX/0aq;

    invoke-virtual {v2, v1, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    .line 2727111
    if-nez v2, :cond_3

    iget-object v2, v0, LX/2CH;->B:LX/0aq;

    invoke-virtual {v2, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    iget-object v2, v0, LX/2CH;->C:LX/0UE;

    invoke-virtual {v2, v1}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v2, v1}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2727112
    invoke-virtual {v0}, LX/2CH;->b()V

    .line 2727113
    :cond_3
    iget-object v0, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    invoke-static {p0, v0}, LX/Jix;->setPresenceFromUserKey(LX/Jix;Lcom/facebook/user/model/UserKey;)V

    .line 2727114
    iget-object v0, p0, LX/Jix;->a:LX/2CH;

    iget-object v1, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    iget-object v2, p0, LX/Jix;->p:LX/9l7;

    invoke-virtual {v0, v1, v2}, LX/2CH;->a(Lcom/facebook/user/model/UserKey;LX/9l7;)V

    goto :goto_0
.end method

.method public static setPresenceFromUserKey(LX/Jix;Lcom/facebook/user/model/UserKey;)V
    .locals 6

    .prologue
    .line 2727083
    iget-object v0, p0, LX/Jix;->a:LX/2CH;

    invoke-virtual {v0, p1}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v0

    .line 2727084
    iget-object v1, v0, LX/3Ox;->b:LX/3Oz;

    sget-object v2, LX/3Oz;->AVAILABLE:LX/3Oz;

    if-ne v1, v2, :cond_4

    iget v1, v0, LX/3Ox;->f:I

    and-int/lit8 v1, v1, 0xc

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2727085
    if-eqz v1, :cond_0

    .line 2727086
    invoke-virtual {p0}, LX/Jix;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080470

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a01f7

    const v2, 0x7f0214d1

    invoke-direct {p0, v0, v1, v2}, LX/Jix;->a(Ljava/lang/String;II)V

    .line 2727087
    :goto_1
    return-void

    .line 2727088
    :cond_0
    iget-object v1, v0, LX/3Ox;->b:LX/3Oz;

    sget-object v2, LX/3Oz;->AVAILABLE:LX/3Oz;

    if-ne v1, v2, :cond_5

    iget v1, v0, LX/3Ox;->f:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2727089
    if-eqz v1, :cond_1

    .line 2727090
    invoke-virtual {p0}, LX/Jix;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080471

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a01f7

    const v2, 0x7f0214d1

    invoke-direct {p0, v0, v1, v2}, LX/Jix;->a(Ljava/lang/String;II)V

    goto :goto_1

    .line 2727091
    :cond_1
    iget-object v1, p0, LX/Jix;->a:LX/2CH;

    invoke-virtual {v1, p1}, LX/2CH;->e(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/LastActive;

    move-result-object v1

    .line 2727092
    if-nez v1, :cond_2

    .line 2727093
    invoke-direct {p0}, LX/Jix;->a()V

    goto :goto_1

    .line 2727094
    :cond_2
    iget-wide v4, v1, Lcom/facebook/user/model/LastActive;->a:J

    move-wide v2, v4

    .line 2727095
    iget-object v1, v0, LX/3Ox;->b:LX/3Oz;

    move-object v0, v1

    .line 2727096
    invoke-static {v2, v3, v0}, LX/3Kx;->a(JLX/3Oz;)J

    move-result-wide v0

    .line 2727097
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_3

    .line 2727098
    invoke-direct {p0}, LX/Jix;->a()V

    goto :goto_1

    .line 2727099
    :cond_3
    iget-object v2, p0, LX/Jix;->b:LX/3Kx;

    sget-object v3, LX/3P1;->UPPER_CASE:LX/3P1;

    invoke-virtual {v2, v0, v1, v3}, LX/3Kx;->a(JLX/3P1;)Ljava/lang/String;

    move-result-object v0

    .line 2727100
    const v1, 0x7f0a01f8

    const v2, 0x7f0214d2

    invoke-direct {p0, v0, v1, v2}, LX/Jix;->a(Ljava/lang/String;II)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x47acab02

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2727080
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 2727081
    iget-object v1, p0, LX/Jix;->m:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2727082
    const/16 v1, 0x2d

    const v2, -0x500b4f58

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x6a8b5e2c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2727063
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 2727064
    iget-object v1, p0, LX/Jix;->m:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2727065
    iget-object v1, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Jix;->p:LX/9l7;

    if-eqz v1, :cond_0

    .line 2727066
    iget-object v1, p0, LX/Jix;->a:LX/2CH;

    iget-object v2, p0, LX/Jix;->o:Lcom/facebook/user/model/UserKey;

    iget-object v3, p0, LX/Jix;->p:LX/9l7;

    invoke-virtual {v1, v2, v3}, LX/2CH;->b(Lcom/facebook/user/model/UserKey;LX/9l7;)V

    .line 2727067
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x4aa47db

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 2727075
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2727076
    iget-object v0, p0, LX/Jix;->e:LX/Js8;

    invoke-virtual {p0}, LX/Jix;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/Jix;->getHeight()I

    move-result v2

    .line 2727077
    iget-object v3, v0, LX/Js8;->e:Landroid/graphics/Paint;

    if-eqz v3, :cond_0

    .line 2727078
    iget v3, v0, LX/Js8;->c:I

    int-to-float v4, v3

    iget v3, v0, LX/Js8;->b:I

    sub-int v3, v2, v3

    int-to-float v5, v3

    iget v3, v0, LX/Js8;->d:I

    sub-int v3, v1, v3

    int-to-float v6, v3

    int-to-float v7, v2

    iget-object v8, v0, LX/Js8;->e:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2727079
    :cond_0
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2727072
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onFinishTemporaryDetach()V

    .line 2727073
    iget-object v0, p0, LX/Jix;->m:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2727074
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2727069
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onStartTemporaryDetach()V

    .line 2727070
    iget-object v0, p0, LX/Jix;->m:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2727071
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2727068
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Jix;->m:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
