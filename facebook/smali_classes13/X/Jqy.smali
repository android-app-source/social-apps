.class public LX/Jqy;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/Jrc;

.field private final b:LX/Jqb;

.field private final c:LX/2N4;

.field private final d:LX/FDs;

.field private final e:LX/0SG;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741154
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqy;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/Jrc;LX/Jqb;LX/2N4;LX/FDs;LX/0SG;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jrc;",
            "LX/Jqb;",
            "LX/2N4;",
            "LX/FDs;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2741145
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2741146
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741147
    iput-object v0, p0, LX/Jqy;->f:LX/0Ot;

    .line 2741148
    iput-object p2, p0, LX/Jqy;->a:LX/Jrc;

    .line 2741149
    iput-object p3, p0, LX/Jqy;->b:LX/Jqb;

    .line 2741150
    iput-object p4, p0, LX/Jqy;->c:LX/2N4;

    .line 2741151
    iput-object p5, p0, LX/Jqy;->d:LX/FDs;

    .line 2741152
    iput-object p6, p0, LX/Jqy;->e:LX/0SG;

    .line 2741153
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqy;
    .locals 14

    .prologue
    .line 2741114
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741115
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741116
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741117
    if-nez v1, :cond_0

    .line 2741118
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741119
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741120
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741121
    sget-object v1, LX/Jqy;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741122
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741123
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741124
    :cond_1
    if-nez v1, :cond_4

    .line 2741125
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741126
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741127
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741128
    new-instance v7, LX/Jqy;

    const/16 v8, 0x35bd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v9

    check-cast v9, LX/Jrc;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v10

    check-cast v10, LX/Jqb;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v11

    check-cast v11, LX/2N4;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v12

    check-cast v12, LX/FDs;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v7 .. v13}, LX/Jqy;-><init>(LX/0Ot;LX/Jrc;LX/Jqb;LX/2N4;LX/FDs;LX/0SG;)V

    .line 2741129
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2741130
    iput-object v8, v7, LX/Jqy;->f:LX/0Ot;

    .line 2741131
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741132
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741133
    if-nez v1, :cond_2

    .line 2741134
    sget-object v0, LX/Jqy;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqy;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741135
    :goto_1
    if-eqz v0, :cond_3

    .line 2741136
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741137
    :goto_3
    check-cast v0, LX/Jqy;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741138
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741139
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741140
    :catchall_1
    move-exception v0

    .line 2741141
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741142
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741143
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741144
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqy;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqy;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741155
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741156
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2741089
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2741090
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->F()LX/6js;

    move-result-object v0

    .line 2741091
    iget-object v3, v0, LX/6js;->mode:Ljava/lang/Integer;

    if-nez v3, :cond_0

    move-object v0, v6

    .line 2741092
    :goto_0
    return-object v0

    .line 2741093
    :cond_0
    iget-object v3, v0, LX/6js;->mode:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 2741094
    :goto_1
    iget-object v3, p0, LX/Jqy;->c:LX/2N4;

    iget-object v4, p0, LX/Jqy;->a:LX/Jrc;

    iget-object v5, v0, LX/6js;->messageMetadata:LX/6kn;

    iget-object v5, v5, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v4, v5}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    .line 2741095
    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741096
    if-nez v1, :cond_2

    move-object v0, v6

    .line 2741097
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2741098
    goto :goto_1

    .line 2741099
    :cond_2
    iget-object v3, v0, LX/6js;->link:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v0, v0, LX/6js;->link:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2741100
    :goto_2
    iget-object v0, p0, LX/Jqy;->d:LX/FDs;

    iget-object v4, p0, LX/Jqy;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 2741101
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object v7

    iget-object p0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-virtual {v7, p0}, LX/6fm;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6fm;

    move-result-object v7

    .line 2741102
    iput-boolean v2, v7, LX/6fm;->b:Z

    .line 2741103
    move-object v7, v7

    .line 2741104
    iput-object v3, v7, LX/6fm;->a:Landroid/net/Uri;

    .line 2741105
    move-object v7, v7

    .line 2741106
    invoke-virtual {v7}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v7

    .line 2741107
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    move-result-object v7

    .line 2741108
    invoke-virtual {v7}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v7

    invoke-static {v0, v7, v4, v5}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741109
    iget-object v7, v0, LX/FDs;->d:LX/2N4;

    iget-object p0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 p1, 0x0

    invoke-virtual {v7, p0, p1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v7

    iget-object v7, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v7

    .line 2741110
    if-eqz v0, :cond_3

    .line 2741111
    const-string v1, "joinable_mode_thread_summary"

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    move-object v0, v6

    .line 2741112
    goto :goto_0

    .line 2741113
    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741083
    const-string v0, "joinable_mode_thread_summary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741084
    if-eqz v0, :cond_0

    .line 2741085
    iget-object v1, p0, LX/Jqy;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/Jqy;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741086
    iget-object v1, p0, LX/Jqy;->b:LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741087
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741088
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2741079
    check-cast p1, LX/6kW;

    .line 2741080
    invoke-virtual {p1}, LX/6kW;->F()LX/6js;

    move-result-object v0

    .line 2741081
    iget-object v1, p0, LX/Jqy;->a:LX/Jrc;

    iget-object v0, v0, LX/6js;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2741082
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
