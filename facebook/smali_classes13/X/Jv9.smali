.class public LX/Jv9;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Jv9;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/oxygen/preloads/integration/dogfooding/BroadcastLogicForDogfoodingBroadcastRegistration;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2749811
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 2749812
    return-void
.end method

.method public static a(LX/0QB;)LX/Jv9;
    .locals 4

    .prologue
    .line 2749813
    sget-object v0, LX/Jv9;->a:LX/Jv9;

    if-nez v0, :cond_1

    .line 2749814
    const-class v1, LX/Jv9;

    monitor-enter v1

    .line 2749815
    :try_start_0
    sget-object v0, LX/Jv9;->a:LX/Jv9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2749816
    if-eqz v2, :cond_0

    .line 2749817
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2749818
    new-instance v3, LX/Jv9;

    const/16 p0, 0x153b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Jv9;-><init>(LX/0Ot;)V

    .line 2749819
    move-object v0, v3

    .line 2749820
    sput-object v0, LX/Jv9;->a:LX/Jv9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2749821
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2749822
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2749823
    :cond_1
    sget-object v0, LX/Jv9;->a:LX/Jv9;

    return-object v0

    .line 2749824
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2749825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x35223c66    # -7266765.0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2749826
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2749827
    const-string v2, "is_running"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2749828
    const/4 v2, -0x1

    invoke-interface {p3, v2}, LX/0Yf;->setResultCode(I)V

    .line 2749829
    invoke-interface {p3, v1}, LX/0Yf;->setResultExtras(Landroid/os/Bundle;)V

    .line 2749830
    const/16 v1, 0x27

    const v2, -0x65014823

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2749831
    return-void
.end method
