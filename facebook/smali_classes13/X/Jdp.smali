.class public final enum LX/Jdp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jdp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jdp;

.field public static final enum BLOCK_ALL:LX/Jdp;

.field public static final enum CHOOSE_TOPICS_TITLE:LX/Jdp;

.field public static final enum DIVIDER:LX/Jdp;

.field public static final enum EXTRA_SPACE_DIVIDER:LX/Jdp;

.field public static final enum MANAGE_NOTIFICATIONS:LX/Jdp;

.field public static final enum MANAGE_TOPIC_STATION:LX/Jdp;

.field public static final enum MANAGE_TOPIC_SUBSTATION:LX/Jdp;

.field public static final enum MESSAGE_TYPES_TITLE:LX/Jdp;

.field public static final enum UNBLOCK_ALL:LX/Jdp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2720103
    new-instance v0, LX/Jdp;

    const-string v1, "CHOOSE_TOPICS_TITLE"

    invoke-direct {v0, v1, v3}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->CHOOSE_TOPICS_TITLE:LX/Jdp;

    .line 2720104
    new-instance v0, LX/Jdp;

    const-string v1, "MANAGE_TOPIC_STATION"

    invoke-direct {v0, v1, v4}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->MANAGE_TOPIC_STATION:LX/Jdp;

    .line 2720105
    new-instance v0, LX/Jdp;

    const-string v1, "MANAGE_NOTIFICATIONS"

    invoke-direct {v0, v1, v5}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->MANAGE_NOTIFICATIONS:LX/Jdp;

    .line 2720106
    new-instance v0, LX/Jdp;

    const-string v1, "MANAGE_TOPIC_SUBSTATION"

    invoke-direct {v0, v1, v6}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->MANAGE_TOPIC_SUBSTATION:LX/Jdp;

    .line 2720107
    new-instance v0, LX/Jdp;

    const-string v1, "MESSAGE_TYPES_TITLE"

    invoke-direct {v0, v1, v7}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->MESSAGE_TYPES_TITLE:LX/Jdp;

    .line 2720108
    new-instance v0, LX/Jdp;

    const-string v1, "BLOCK_ALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->BLOCK_ALL:LX/Jdp;

    .line 2720109
    new-instance v0, LX/Jdp;

    const-string v1, "UNBLOCK_ALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->UNBLOCK_ALL:LX/Jdp;

    .line 2720110
    new-instance v0, LX/Jdp;

    const-string v1, "DIVIDER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->DIVIDER:LX/Jdp;

    .line 2720111
    new-instance v0, LX/Jdp;

    const-string v1, "EXTRA_SPACE_DIVIDER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Jdp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdp;->EXTRA_SPACE_DIVIDER:LX/Jdp;

    .line 2720112
    const/16 v0, 0x9

    new-array v0, v0, [LX/Jdp;

    sget-object v1, LX/Jdp;->CHOOSE_TOPICS_TITLE:LX/Jdp;

    aput-object v1, v0, v3

    sget-object v1, LX/Jdp;->MANAGE_TOPIC_STATION:LX/Jdp;

    aput-object v1, v0, v4

    sget-object v1, LX/Jdp;->MANAGE_NOTIFICATIONS:LX/Jdp;

    aput-object v1, v0, v5

    sget-object v1, LX/Jdp;->MANAGE_TOPIC_SUBSTATION:LX/Jdp;

    aput-object v1, v0, v6

    sget-object v1, LX/Jdp;->MESSAGE_TYPES_TITLE:LX/Jdp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Jdp;->BLOCK_ALL:LX/Jdp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Jdp;->UNBLOCK_ALL:LX/Jdp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Jdp;->DIVIDER:LX/Jdp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Jdp;->EXTRA_SPACE_DIVIDER:LX/Jdp;

    aput-object v2, v0, v1

    sput-object v0, LX/Jdp;->$VALUES:[LX/Jdp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2720113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jdp;
    .locals 1

    .prologue
    .line 2720114
    const-class v0, LX/Jdp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jdp;

    return-object v0
.end method

.method public static values()[LX/Jdp;
    .locals 1

    .prologue
    .line 2720115
    sget-object v0, LX/Jdp;->$VALUES:[LX/Jdp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jdp;

    return-object v0
.end method
