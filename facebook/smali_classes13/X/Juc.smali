.class public final enum LX/Juc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Juc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Juc;

.field public static final enum ANSWER:LX/Juc;

.field public static final enum DECLINE:LX/Juc;

.field public static final enum SHOW_INCALL:LX/Juc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2749020
    new-instance v0, LX/Juc;

    const-string v1, "ANSWER"

    invoke-direct {v0, v1, v2}, LX/Juc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Juc;->ANSWER:LX/Juc;

    .line 2749021
    new-instance v0, LX/Juc;

    const-string v1, "SHOW_INCALL"

    invoke-direct {v0, v1, v3}, LX/Juc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Juc;->SHOW_INCALL:LX/Juc;

    .line 2749022
    new-instance v0, LX/Juc;

    const-string v1, "DECLINE"

    invoke-direct {v0, v1, v4}, LX/Juc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Juc;->DECLINE:LX/Juc;

    .line 2749023
    const/4 v0, 0x3

    new-array v0, v0, [LX/Juc;

    sget-object v1, LX/Juc;->ANSWER:LX/Juc;

    aput-object v1, v0, v2

    sget-object v1, LX/Juc;->SHOW_INCALL:LX/Juc;

    aput-object v1, v0, v3

    sget-object v1, LX/Juc;->DECLINE:LX/Juc;

    aput-object v1, v0, v4

    sput-object v0, LX/Juc;->$VALUES:[LX/Juc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2749024
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Juc;
    .locals 1

    .prologue
    .line 2749025
    const-class v0, LX/Juc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Juc;

    return-object v0
.end method

.method public static values()[LX/Juc;
    .locals 1

    .prologue
    .line 2749026
    sget-object v0, LX/Juc;->$VALUES:[LX/Juc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Juc;

    return-object v0
.end method
