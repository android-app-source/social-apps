.class public LX/Jhr;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jnd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/CheckBox;

.field public d:Lcom/facebook/fbui/glyph/GlyphView;

.field public e:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

.field public f:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterButton;",
            ">;"
        }
    .end annotation
.end field

.field public g:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public h:LX/DAW;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725542
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725543
    const-class v0, LX/Jhr;

    invoke-static {v0, p0}, LX/Jhr;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725544
    const v0, 0x7f030ce7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725545
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/Jhr;->c:Landroid/widget/CheckBox;

    .line 2725546
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iput-object v0, p0, LX/Jhr;->e:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2725547
    const v0, 0x7f0d2037

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Jhr;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2725548
    const v0, 0x7f0d2003

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jhr;->f:LX/4ob;

    .line 2725549
    iget-object v0, p0, LX/Jhr;->e:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v0}, LX/2Wj;->getTextColor()I

    move-result v0

    iput v0, p0, LX/Jhr;->g:I

    .line 2725550
    iget-object v0, p0, LX/Jhr;->d:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Jhp;

    invoke-direct {v1, p0}, LX/Jhp;-><init>(LX/Jhr;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725551
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Jhr;

    const/16 v2, 0x282c

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x455

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, LX/Jhr;->a:LX/0Ot;

    iput-object v1, p1, LX/Jhr;->b:LX/0Ot;

    return-void
.end method

.method public static b(LX/Jhr;)V
    .locals 3

    .prologue
    .line 2725552
    iget-object v0, p0, LX/Jhr;->h:LX/DAW;

    .line 2725553
    iget-boolean v1, v0, LX/DAW;->d:Z

    move v0, v1

    .line 2725554
    if-eqz v0, :cond_0

    .line 2725555
    iget-object v0, p0, LX/Jhr;->c:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2725556
    iget-object v0, p0, LX/Jhr;->c:Landroid/widget/CheckBox;

    iget-object v1, p0, LX/Jhr;->h:LX/DAW;

    invoke-virtual {v1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2725557
    :goto_0
    iget-object v0, p0, LX/Jhr;->h:LX/DAW;

    invoke-virtual {v0}, LX/3OP;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2725558
    iget-object v0, p0, LX/Jhr;->e:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {p0}, LX/Jhr;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 2725559
    :goto_1
    iget-object v0, p0, LX/Jhr;->h:LX/DAW;

    .line 2725560
    iget-boolean v1, v0, LX/DAW;->e:Z

    move v0, v1

    .line 2725561
    if-eqz v0, :cond_3

    .line 2725562
    iget-object v0, p0, LX/Jhr;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2725563
    iget-object v0, p0, LX/Jhr;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/Jhr;->h:LX/DAW;

    .line 2725564
    iget-boolean v2, v1, LX/DAW;->b:Z

    move v1, v2

    .line 2725565
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2725566
    iget-object v0, p0, LX/Jhr;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/Jhr;->h:LX/DAW;

    .line 2725567
    iget-boolean v2, v1, LX/DAW;->b:Z

    move v1, v2

    .line 2725568
    if-eqz v1, :cond_2

    invoke-virtual {p0}, LX/Jhr;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08030c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2725569
    iget-object v0, p0, LX/Jhr;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2725570
    new-instance v1, LX/Jhq;

    invoke-direct {v1, p0, p0}, LX/Jhq;-><init>(LX/Jhr;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725571
    :goto_3
    return-void

    .line 2725572
    :cond_0
    iget-object v0, p0, LX/Jhr;->c:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 2725573
    :cond_1
    iget-object v0, p0, LX/Jhr;->e:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iget v1, p0, LX/Jhr;->g:I

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    goto :goto_1

    .line 2725574
    :cond_2
    invoke-virtual {p0}, LX/Jhr;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08043f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2725575
    :cond_3
    iget-object v0, p0, LX/Jhr;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto :goto_3
.end method


# virtual methods
.method public getContactRow()LX/DAW;
    .locals 1

    .prologue
    .line 2725541
    iget-object v0, p0, LX/Jhr;->h:LX/DAW;

    return-object v0
.end method
