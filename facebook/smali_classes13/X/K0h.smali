.class public LX/K0h;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:D

.field private e:Landroid/widget/ProgressBar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2759222
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2759223
    iput-boolean v0, p0, LX/K0h;->b:Z

    .line 2759224
    iput-boolean v0, p0, LX/K0h;->c:Z

    .line 2759225
    return-void
.end method

.method private a(Landroid/widget/ProgressBar;)V
    .locals 3

    .prologue
    .line 2759226
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2759227
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2759228
    :goto_0
    if-nez v0, :cond_1

    .line 2759229
    :goto_1
    return-void

    .line 2759230
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2759231
    :cond_1
    iget-object v1, p0, LX/K0h;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2759232
    iget-object v1, p0, LX/K0h;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_1

    .line 2759233
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2759234
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 2759235
    new-instance v0, LX/5pA;

    const-string v1, "setStyle() not called"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2759236
    :cond_0
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    iget-boolean v1, p0, LX/K0h;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2759237
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    invoke-direct {p0, v0}, LX/K0h;->a(Landroid/widget/ProgressBar;)V

    .line 2759238
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    iget-wide v2, p0, LX/K0h;->d:D

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 2759239
    iget-boolean v0, p0, LX/K0h;->c:Z

    if-eqz v0, :cond_1

    .line 2759240
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2759241
    :goto_0
    return-void

    .line 2759242
    :cond_1
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    .line 2759243
    invoke-static {p1}, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;->a(Ljava/lang/String;)I

    move-result v0

    .line 2759244
    invoke-virtual {p0}, LX/K0h;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;->a(Landroid/content/Context;I)Landroid/widget/ProgressBar;

    move-result-object v0

    iput-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    .line 2759245
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 2759246
    invoke-virtual {p0}, LX/K0h;->removeAllViews()V

    .line 2759247
    iget-object v0, p0, LX/K0h;->e:Landroid/widget/ProgressBar;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, LX/K0h;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2759248
    return-void
.end method
