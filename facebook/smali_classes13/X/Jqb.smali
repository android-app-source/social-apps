.class public LX/Jqb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public final a:LX/2Ow;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/notify/NewMessageNotification;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/Jqa;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739532
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqb;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Ow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739596
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jqb;->b:Ljava/util/Map;

    .line 2739597
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Jqb;->c:Ljava/util/Set;

    .line 2739598
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jqb;->d:Ljava/util/Map;

    .line 2739599
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jqb;->e:Ljava/util/Map;

    .line 2739600
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jqb;->f:Ljava/util/Map;

    .line 2739601
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jqb;->g:Ljava/util/Map;

    .line 2739602
    iput-object p1, p0, LX/Jqb;->a:LX/2Ow;

    .line 2739603
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqb;
    .locals 7

    .prologue
    .line 2739566
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2739567
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2739568
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2739569
    if-nez v1, :cond_0

    .line 2739570
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739571
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2739572
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2739573
    sget-object v1, LX/Jqb;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2739574
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2739575
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2739576
    :cond_1
    if-nez v1, :cond_4

    .line 2739577
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2739578
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2739579
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2739580
    new-instance p0, LX/Jqb;

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v1

    check-cast v1, LX/2Ow;

    invoke-direct {p0, v1}, LX/Jqb;-><init>(LX/2Ow;)V

    .line 2739581
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2739582
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2739583
    if-nez v1, :cond_2

    .line 2739584
    sget-object v0, LX/Jqb;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqb;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2739585
    :goto_1
    if-eqz v0, :cond_3

    .line 2739586
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739587
    :goto_3
    check-cast v0, LX/Jqb;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2739588
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2739589
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2739590
    :catchall_1
    move-exception v0

    .line 2739591
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739592
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2739593
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2739594
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqb;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqb;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/notify/NewMessageNotification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2739561
    iget-object v0, p0, LX/Jqb;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2739562
    if-nez v0, :cond_0

    .line 2739563
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2739564
    iget-object v1, p0, LX/Jqb;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739565
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 10

    .prologue
    .line 2739557
    iget-object v0, p0, LX/Jqb;->g:Ljava/util/Map;

    .line 2739558
    sget-object v2, LX/DdH;->MESSAGE_SENT_DELTA:LX/DdH;

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    move-object v3, p1

    move-wide v7, p2

    invoke-static/range {v2 .. v8}, LX/2Ow;->a(LX/DdH;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLcom/facebook/fbtrace/FbTraceNode;J)Landroid/os/Bundle;

    move-result-object v2

    move-object v1, v2

    .line 2739559
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739560
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 1

    .prologue
    .line 2739552
    invoke-static {p0, p1}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    move-result-object v0

    .line 2739553
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2739554
    iget-object v0, p0, LX/Jqb;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739555
    iget-object v0, p0, LX/Jqb;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739556
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 12

    .prologue
    .line 2739545
    iget-object v0, p0, LX/Jqb;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, LX/Jqb;->a:LX/2Ow;

    .line 2739546
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2739547
    iget-object v4, v2, LX/2Ow;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DdY;

    invoke-virtual {v4, p1}, LX/DdY;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2739548
    sget-object v4, LX/DdH;->READ_RECEIPT:LX/DdH;

    iget-object v5, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v8, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    iget-wide v9, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    move-wide v6, p2

    invoke-static/range {v4 .. v10}, LX/2Ow;->a(LX/DdH;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLcom/facebook/fbtrace/FbTraceNode;J)Landroid/os/Bundle;

    move-result-object v4

    .line 2739549
    :goto_0
    move-object v2, v4

    .line 2739550
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739551
    return-void

    :cond_0
    move-object v4, v5

    goto :goto_0
.end method

.method public final c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 2739542
    invoke-static {p0, p1}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2739543
    iget-object v0, p0, LX/Jqb;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2739544
    return-void
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 2739540
    invoke-virtual {p0}, LX/Jqb;->e()V

    .line 2739541
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2739533
    iget-object v0, p0, LX/Jqb;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2739534
    iget-object v0, p0, LX/Jqb;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2739535
    iget-object v0, p0, LX/Jqb;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2739536
    iget-object v0, p0, LX/Jqb;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2739537
    iget-object v0, p0, LX/Jqb;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2739538
    iget-object v0, p0, LX/Jqb;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2739539
    return-void
.end method
