.class public LX/Jn4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2733669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Jn4;
    .locals 1

    .prologue
    .line 2733670
    new-instance v0, LX/Jn4;

    invoke-direct {v0}, LX/Jn4;-><init>()V

    .line 2733671
    move-object v0, v0

    .line 2733672
    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2733673
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->l()I

    move-result v1

    if-nez v1, :cond_1

    .line 2733674
    :cond_0
    :goto_0
    return-object v0

    .line 2733675
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v1

    .line 2733676
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->o()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2733677
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->o()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;

    move-result-object v3

    .line 2733678
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v1

    const/4 v5, 0x0

    .line 2733679
    if-nez v1, :cond_6

    .line 2733680
    :goto_1
    move-object v4, v5

    .line 2733681
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->j()Ljava/lang/String;

    move-result-object v5

    .line 2733682
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 2733683
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2733684
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2733685
    new-instance v1, Lcom/facebook/user/model/PicSquare;

    new-instance v3, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    move-object v0, v1

    .line 2733686
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v1, v3, v5}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v1

    .line 2733687
    iput-object v4, v1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2733688
    move-object v1, v1

    .line 2733689
    iput-boolean v2, v1, LX/0XI;->z:Z

    .line 2733690
    move-object v1, v1

    .line 2733691
    iput-object v0, v1, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 2733692
    move-object v1, v1

    .line 2733693
    new-instance v0, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;-><init>(Lcom/facebook/user/model/User;)V

    goto :goto_0

    .line 2733694
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/39O;->c()I

    move-result v1

    if-lez v1, :cond_5

    .line 2733695
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2733696
    invoke-static {v6, v1}, LX/Jn4;->a(LX/15i;I)Z

    move-result v1

    :goto_2
    if-eqz v1, :cond_0

    .line 2733697
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v1, LX/0XG;->ADDRESS_BOOK:LX/0XG;

    invoke-virtual {v0, v1, v5}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    .line 2733698
    iput-object v4, v0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2733699
    move-object v0, v0

    .line 2733700
    invoke-virtual {v3}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2733701
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2733702
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :cond_3
    :goto_3
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2733703
    invoke-static {v6, v5}, LX/Jn4;->a(LX/15i;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2733704
    invoke-virtual {v6, v5, v10}, LX/15i;->g(II)I

    move-result v7

    invoke-virtual {v6, v7, v11}, LX/15i;->g(II)I

    move-result v7

    invoke-virtual {v6, v7, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 2733705
    invoke-virtual {v6, v5, v10}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v6, v5, v11}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v6, v5, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2733706
    new-instance v6, Lcom/facebook/user/model/UserPhoneNumber;

    const/4 v8, 0x0

    const/4 v9, 0x2

    invoke-direct {v6, v7, v8, v5, v9}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2733707
    :cond_4
    move-object v1, v3

    .line 2733708
    iput-object v1, v0, LX/0XI;->d:Ljava/util/List;

    .line 2733709
    move-object v0, v0

    .line 2733710
    iput-boolean v2, v0, LX/0XI;->z:Z

    .line 2733711
    move-object v1, v0

    .line 2733712
    new-instance v0, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;-><init>(Lcom/facebook/user/model/User;)V

    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 2733713
    goto :goto_2

    .line 2733714
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;->m_()Ljava/lang/String;

    move-result-object v8

    .line 2733715
    if-eqz v8, :cond_8

    .line 2733716
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v4, 0x0

    move v7, v4

    move-object v6, v5

    :goto_4
    if-ge v7, v10, :cond_9

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;

    .line 2733717
    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->n_()I

    move-result v11

    .line 2733718
    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->n_()I

    move-result v12

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->a()I

    move-result v13

    add-int/2addr v12, v13

    .line 2733719
    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v13

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v13, v14, :cond_7

    .line 2733720
    invoke-virtual {v8, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object p0, v5

    move-object v5, v4

    move-object v4, p0

    .line 2733721
    :goto_5
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move-object v6, v5

    move-object v5, v4

    goto :goto_4

    .line 2733722
    :cond_7
    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v4

    sget-object v13, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v4, v13, :cond_a

    .line 2733723
    invoke-virtual {v8, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object v5, v6

    goto :goto_5

    :cond_8
    move-object v6, v5

    .line 2733724
    :cond_9
    new-instance v4, Lcom/facebook/user/model/Name;

    invoke-direct {v4, v6, v5, v8}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    goto/16 :goto_1

    :cond_a
    move-object v4, v5

    move-object v5, v6

    goto :goto_5
.end method

.method public static a(LX/15i;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2733725
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2733726
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2733727
    invoke-virtual {p0, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2733728
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2733729
    invoke-virtual {p0, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2
.end method
