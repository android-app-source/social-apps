.class public final LX/K3s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/ipc/media/MediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/K3t;


# direct methods
.method public constructor <init>(LX/K3t;)V
    .locals 0

    .prologue
    .line 2766151
    iput-object p1, p0, LX/K3s;->a:LX/K3t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2766152
    iget-object v0, p0, LX/K3s;->a:LX/K3t;

    iget-object v0, v0, LX/K3t;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->C:LX/BOf;

    .line 2766153
    invoke-static {v0}, LX/BOf;->b(LX/BOf;)I

    move-result v1

    .line 2766154
    iget-object v2, v0, LX/BOf;->d:LX/1kZ;

    invoke-virtual {v2}, LX/1kZ;->n()I

    move-result v2

    if-lt v1, v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2766155
    if-nez v2, :cond_0

    .line 2766156
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2766157
    :goto_1
    move-object v0, v1

    .line 2766158
    return-object v0

    .line 2766159
    :cond_0
    invoke-static {v0, v1}, LX/BOf;->b(LX/BOf;I)Ljava/util/List;

    move-result-object v1

    .line 2766160
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, v0, LX/BOf;->d:LX/1kZ;

    invoke-virtual {v3}, LX/1kZ;->n()I

    move-result v3

    if-lt v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2766161
    if-nez v2, :cond_1

    .line 2766162
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2766163
    goto :goto_1

    .line 2766164
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2766165
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/media/util/model/MediaModel;

    .line 2766166
    new-instance v5, LX/74k;

    invoke-direct {v5}, LX/74k;-><init>()V

    new-instance p0, LX/4gN;

    invoke-direct {p0}, LX/4gN;-><init>()V

    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    .line 2766167
    iget-object v1, v2, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2766168
    invoke-virtual {v0, v1}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v0

    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    .line 2766169
    iget-object v1, v2, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 2766170
    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    .line 2766171
    iget v1, v2, Lcom/facebook/media/util/model/MediaModel;->e:I

    move v1, v1

    .line 2766172
    iput v1, v0, LX/4gP;->f:I

    .line 2766173
    move-object v0, v0

    .line 2766174
    iget v1, v2, Lcom/facebook/media/util/model/MediaModel;->f:I

    move v1, v1

    .line 2766175
    iput v1, v0, LX/4gP;->g:I

    .line 2766176
    move-object v0, v0

    .line 2766177
    iget v1, v2, Lcom/facebook/media/util/model/MediaModel;->g:I

    move v1, v1

    .line 2766178
    iput v1, v0, LX/4gP;->e:I

    .line 2766179
    move-object v0, v0

    .line 2766180
    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object p0

    invoke-virtual {p0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object p0

    .line 2766181
    iput-object p0, v5, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2766182
    move-object v5, v5

    .line 2766183
    invoke-virtual {v5}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v5

    move-object v2, v5

    .line 2766184
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2766185
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2766186
    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2766187
    invoke-direct {p0}, LX/K3s;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method
