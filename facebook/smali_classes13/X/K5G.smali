.class public final LX/K5G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/ui/StorylineSeekBar;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/ui/StorylineSeekBar;)V
    .locals 0

    .prologue
    .line 2770144
    iput-object p1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/storyline/ui/StorylineSeekBar;B)V
    .locals 0

    .prologue
    .line 2770127
    invoke-direct {p0, p1}, LX/K5G;-><init>(Lcom/facebook/storyline/ui/StorylineSeekBar;)V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 2770138
    int-to-float v0, p2

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 2770139
    if-eqz p3, :cond_0

    .line 2770140
    iget-object v1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    iget-object v1, v1, Lcom/facebook/storyline/ui/StorylineSeekBar;->d:LX/K40;

    invoke-interface {v1, v0}, LX/K40;->a(F)V

    .line 2770141
    :cond_0
    iget-object v1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    .line 2770142
    invoke-static {v1, v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a$redex0(Lcom/facebook/storyline/ui/StorylineSeekBar;F)V

    .line 2770143
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 2770133
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 2770134
    iget-object v1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    iget-object v1, v1, Lcom/facebook/storyline/ui/StorylineSeekBar;->d:LX/K40;

    invoke-interface {v1, v0}, LX/K40;->b(F)V

    .line 2770135
    iget-object v1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    .line 2770136
    invoke-static {v1, v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a$redex0(Lcom/facebook/storyline/ui/StorylineSeekBar;F)V

    .line 2770137
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 2770128
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 2770129
    iget-object v1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    iget-object v1, v1, Lcom/facebook/storyline/ui/StorylineSeekBar;->d:LX/K40;

    invoke-interface {v1, v0}, LX/K40;->c(F)V

    .line 2770130
    iget-object v1, p0, LX/K5G;->a:Lcom/facebook/storyline/ui/StorylineSeekBar;

    .line 2770131
    invoke-static {v1, v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a$redex0(Lcom/facebook/storyline/ui/StorylineSeekBar;F)V

    .line 2770132
    return-void
.end method
