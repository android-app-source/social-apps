.class public LX/K29;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2763125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2763126
    iput-object p1, p0, LX/K29;->a:LX/0Ot;

    .line 2763127
    iput-object p2, p0, LX/K29;->b:LX/0Ot;

    .line 2763128
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/Clw;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2763129
    invoke-interface {p2}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    .line 2763130
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2763131
    invoke-interface {v0}, LX/8Yr;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    .line 2763132
    new-instance p2, LX/K2V;

    invoke-direct {p2, v1}, LX/K2V;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;)V

    invoke-interface {v4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2763133
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2763134
    :cond_0
    move-object v0, v4

    .line 2763135
    iget-object v1, p0, LX/K29;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dh;

    invoke-virtual {v1}, LX/7Dh;->e()Z

    move-result v1

    invoke-static {p1}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, LX/7E3;->a(Ljava/util/List;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2763136
    iget-object v0, p0, LX/K29;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0xa2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
