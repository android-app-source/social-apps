.class public LX/Jyl;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

.field private b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2755386
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2755387
    iput-object p1, p0, LX/Jyl;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    .line 2755388
    iput-object p2, p0, LX/Jyl;->b:Landroid/view/View$OnClickListener;

    .line 2755389
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2755390
    if-nez p2, :cond_0

    .line 2755391
    new-instance v1, LX/JEM;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/JEM;-><init>(Landroid/content/Context;)V

    .line 2755392
    new-instance v0, LX/Jyk;

    invoke-direct {v0, v1}, LX/Jyk;-><init>(LX/JEM;)V

    .line 2755393
    :goto_0
    return-object v0

    .line 2755394
    :cond_0
    new-instance v1, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    .line 2755395
    const v0, 0x7f083c24

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2755396
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2755397
    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2755398
    const/16 v0, 0x102

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2755399
    new-instance v0, LX/Jyj;

    invoke-direct {v0, v1}, LX/Jyj;-><init>(Lcom/facebook/fig/button/FigButton;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2755400
    instance-of v0, p1, LX/Jyk;

    if-eqz v0, :cond_1

    .line 2755401
    iget-object v0, p0, LX/Jyl;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;

    .line 2755402
    check-cast p1, LX/Jyk;

    .line 2755403
    iget-object v1, p1, LX/Jyk;->l:LX/JEM;

    move-object v1, v1

    .line 2755404
    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    move-result-object v4

    iget-object v0, p0, LX/Jyl;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, LX/JEM;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;Z)V

    .line 2755405
    :goto_1
    return-void

    .line 2755406
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2755407
    :cond_1
    check-cast p1, LX/Jyj;

    .line 2755408
    iget-object v0, p1, LX/Jyj;->l:Lcom/facebook/fig/button/FigButton;

    move-object v0, v0

    .line 2755409
    iget-object v1, p0, LX/Jyl;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2755410
    iget-object v0, p0, LX/Jyl;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2755411
    const/4 v0, 0x0

    .line 2755412
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2755413
    iget-object v0, p0, LX/Jyl;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
