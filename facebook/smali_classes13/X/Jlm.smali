.class public final enum LX/Jlm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jlm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jlm;

.field public static final enum START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

.field public static final enum START_NEW_INDEPENDENT_FETCH:LX/Jlm;

.field public static final enum USE_CURRENT_FETCH:LX/Jlm;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2731721
    new-instance v0, LX/Jlm;

    const-string v1, "USE_CURRENT_FETCH"

    invoke-direct {v0, v1, v2}, LX/Jlm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlm;->USE_CURRENT_FETCH:LX/Jlm;

    .line 2731722
    new-instance v0, LX/Jlm;

    const-string v1, "START_NEW_FETCH_AND_TRACK_RESULT"

    invoke-direct {v0, v1, v3}, LX/Jlm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlm;->START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

    .line 2731723
    new-instance v0, LX/Jlm;

    const-string v1, "START_NEW_INDEPENDENT_FETCH"

    invoke-direct {v0, v1, v4}, LX/Jlm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlm;->START_NEW_INDEPENDENT_FETCH:LX/Jlm;

    .line 2731724
    const/4 v0, 0x3

    new-array v0, v0, [LX/Jlm;

    sget-object v1, LX/Jlm;->USE_CURRENT_FETCH:LX/Jlm;

    aput-object v1, v0, v2

    sget-object v1, LX/Jlm;->START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

    aput-object v1, v0, v3

    sget-object v1, LX/Jlm;->START_NEW_INDEPENDENT_FETCH:LX/Jlm;

    aput-object v1, v0, v4

    sput-object v0, LX/Jlm;->$VALUES:[LX/Jlm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2731720
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jlm;
    .locals 1

    .prologue
    .line 2731725
    const-class v0, LX/Jlm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jlm;

    return-object v0
.end method

.method public static values()[LX/Jlm;
    .locals 1

    .prologue
    .line 2731719
    sget-object v0, LX/Jlm;->$VALUES:[LX/Jlm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jlm;

    return-object v0
.end method
