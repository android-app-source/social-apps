.class public final LX/Jcx;
.super LX/4Ae;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V
    .locals 0

    .prologue
    .line 2718674
    iput-object p1, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-direct {p0}, LX/4Ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailed(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2718675
    iget-object v0, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2718676
    :goto_0
    return-void

    .line 2718677
    :cond_0
    iget-object v0, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2718678
    iget-object v0, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->s()V

    goto :goto_0
.end method

.method public final onSucceeded(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2718679
    iget-object v0, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    if-eqz v0, :cond_0

    .line 2718680
    iget-object v0, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v1, "_op_success"

    iget-object v2, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718681
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;

    .line 2718682
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.messaging.accountswitch.SWITH_OPERATION_COMPLETE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718683
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2718684
    const-string v3, "account_switch_result"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2718685
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2718686
    iget-object v0, p0, LX/Jcx;->a:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Landroid/content/Intent;)V

    .line 2718687
    :cond_0
    return-void
.end method
