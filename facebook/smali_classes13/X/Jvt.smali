.class public final LX/Jvt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        "Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryInterfaces$GravitySuggestifierQuery$Suggestions$Edges;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jvw;


# direct methods
.method public constructor <init>(LX/Jvw;)V
    .locals 0

    .prologue
    .line 2751196
    iput-object p1, p0, LX/Jvt;->a:LX/Jvw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11

    .prologue
    .line 2751197
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2751198
    iget-object v0, p0, LX/Jvt;->a:LX/Jvw;

    .line 2751199
    iget-object v1, v0, LX/Jvw;->e:LX/0yD;

    .line 2751200
    if-nez p1, :cond_2

    .line 2751201
    const/4 v5, 0x0

    .line 2751202
    :goto_0
    move-object v1, v5

    .line 2751203
    new-instance v2, LX/4DI;

    invoke-direct {v2}, LX/4DI;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, LX/4DI;->a(Ljava/lang/String;)LX/4DI;

    move-result-object v2

    .line 2751204
    if-eqz v1, :cond_0

    .line 2751205
    invoke-virtual {v2, v1}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 2751206
    :cond_0
    if-eqz p1, :cond_1

    .line 2751207
    iget-object v1, v0, LX/Jvw;->d:LX/9jj;

    invoke-virtual {v1, v2}, LX/9jj;->a(LX/4DI;)Z

    .line 2751208
    :cond_1
    new-instance v1, LX/Jw4;

    invoke-direct {v1}, LX/Jw4;-><init>()V

    move-object v1, v1

    .line 2751209
    const-string v3, "gravity_query_data"

    invoke-virtual {v1, v3, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    const-string v3, "gravity_checkin_context"

    const-string v4, "places_feed_gps"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2751210
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2751211
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->hashCode()I

    move-result v2

    .line 2751212
    iget-object v3, v0, LX/Jvw;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x1e0005

    invoke-interface {v3, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2751213
    iget-object v3, v0, LX/Jvw;->b:LX/0tX;

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v3, LX/Jvu;

    invoke-direct {v3, v0, v2}, LX/Jvu;-><init>(LX/Jvw;I)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v1, v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2751214
    new-instance v3, LX/Jvv;

    invoke-direct {v3, v0, v2}, LX/Jvv;-><init>(LX/Jvw;I)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v1, v3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2751215
    move-object v0, v1

    .line 2751216
    return-object v0

    .line 2751217
    :cond_2
    new-instance v5, LX/3Aj;

    invoke-direct {v5}, LX/3Aj;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    .line 2751218
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2751219
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 2751220
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v5

    .line 2751221
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2751222
    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 2751223
    :cond_4
    invoke-virtual {v1, p1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v7

    .line 2751224
    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-ltz v5, :cond_5

    .line 2751225
    invoke-static {v7, v8}, LX/1lQ;->n(J)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    :cond_5
    move-object v5, v6

    .line 2751226
    goto/16 :goto_0
.end method
