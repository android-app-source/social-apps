.class public LX/Jxw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EoA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public a:LX/Eo0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Jxv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/JI4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Jxr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Jxo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/Jxt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0hB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:LX/Jxq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/Jxn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/Jxs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2753870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753871
    return-void
.end method

.method public static a(LX/0QB;)LX/Jxw;
    .locals 11

    .prologue
    .line 2753839
    const-class v1, LX/Jxw;

    monitor-enter v1

    .line 2753840
    :try_start_0
    sget-object v0, LX/Jxw;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2753841
    sput-object v2, LX/Jxw;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2753842
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2753843
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2753844
    new-instance v3, LX/Jxw;

    invoke-direct {v3}, LX/Jxw;-><init>()V

    .line 2753845
    const-class v4, LX/Eo0;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Eo0;

    const-class v5, LX/Jxv;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Jxv;

    invoke-static {v0}, LX/JI4;->a(LX/0QB;)LX/JI4;

    move-result-object v6

    check-cast v6, LX/JI4;

    const-class v7, LX/Jxr;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Jxr;

    const-class v8, LX/Jxo;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Jxo;

    invoke-static {v0}, LX/Jxt;->a(LX/0QB;)LX/Jxt;

    move-result-object v9

    check-cast v9, LX/Jxt;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    .line 2753846
    iput-object v4, v3, LX/Jxw;->a:LX/Eo0;

    iput-object v5, v3, LX/Jxw;->b:LX/Jxv;

    iput-object v6, v3, LX/Jxw;->c:LX/JI4;

    iput-object v7, v3, LX/Jxw;->d:LX/Jxr;

    iput-object v8, v3, LX/Jxw;->e:LX/Jxo;

    iput-object v9, v3, LX/Jxw;->f:LX/Jxt;

    iput-object v10, v3, LX/Jxw;->g:LX/0hB;

    iput-object p0, v3, LX/Jxw;->h:Landroid/content/res/Resources;

    .line 2753847
    move-object v0, v3

    .line 2753848
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2753849
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Jxw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2753850
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2753851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)LX/Emj;
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2753865
    iget-object v0, p0, LX/Jxw;->k:LX/Jxs;

    if-nez v0, :cond_0

    .line 2753866
    if-eqz p1, :cond_1

    const-string v0, "bucketid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2753867
    :goto_0
    new-instance v1, LX/Jxs;

    invoke-virtual {p0, p1}, LX/Jxw;->c(Landroid/os/Bundle;)LX/Jxq;

    move-result-object v2

    iget-object v3, p0, LX/Jxw;->f:LX/Jxt;

    invoke-direct {v1, v2, v3, v0}, LX/Jxs;-><init>(LX/Jxq;LX/Jxt;Ljava/lang/String;)V

    iput-object v1, p0, LX/Jxw;->k:LX/Jxs;

    .line 2753868
    :cond_0
    iget-object v0, p0, LX/Jxw;->k:LX/Jxs;

    return-object v0

    .line 2753869
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1My;LX/0Px;Ljava/lang/String;LX/Emq;LX/Emy;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)LX/Enl;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/Emq;",
            "LX/Emy;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/facebook/entitycards/model/EntityCardsDataSource;"
        }
    .end annotation

    .prologue
    .line 2753862
    iget-object v1, p0, LX/Jxw;->a:LX/Eo0;

    iget-object v2, p0, LX/Jxw;->b:LX/Jxv;

    move-object/from16 v0, p7

    invoke-virtual {p0, v0}, LX/Jxw;->c(Landroid/os/Bundle;)LX/Jxq;

    move-result-object v3

    const-string v4, "bucketid"

    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "bucket_has_title"

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/Jxv;->a(LX/Jxq;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/Jxw;->a()Ljava/lang/String;

    move-result-object v7

    sget-object v8, LX/En3;->a:LX/En3;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move-object v2, p1

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v12}, LX/Eo0;->a(LX/1My;LX/Enh;LX/Eoc;LX/Emq;LX/Emy;Ljava/lang/String;LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)LX/Enz;

    move-result-object v1

    .line 2753863
    iget-object v2, p0, LX/Jxw;->e:LX/Jxo;

    invoke-virtual {v1}, LX/Enl;->j()LX/Eny;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Jxo;->a(LX/Eny;)LX/Jxn;

    move-result-object v2

    iput-object v2, p0, LX/Jxw;->j:LX/Jxn;

    .line 2753864
    return-object v1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2753861
    const-string v0, "people_discovery"

    return-object v0
.end method

.method public final b()LX/Enm;
    .locals 1

    .prologue
    .line 2753860
    iget-object v0, p0, LX/Jxw;->c:LX/JI4;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)LX/Jxq;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2753859
    invoke-virtual {p0, p1}, LX/Jxw;->c(Landroid/os/Bundle;)LX/Jxq;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 2753858
    iget-object v0, p0, LX/Jxw;->g:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->g()I

    move-result v0

    iget-object v1, p0, LX/Jxw;->h:Landroid/content/res/Resources;

    const v2, 0x7f0b003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/Jxw;->h:Landroid/content/res/Resources;

    const v2, 0x7f0b2539

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final c(Landroid/os/Bundle;)LX/Jxq;
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2753853
    iget-object v0, p0, LX/Jxw;->i:LX/Jxq;

    if-nez v0, :cond_0

    .line 2753854
    if-eqz p1, :cond_1

    const-string v0, "discovery_curation_logging_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753855
    :goto_0
    iget-object v1, p0, LX/Jxw;->d:LX/Jxr;

    invoke-virtual {v1, v0}, LX/Jxr;->a(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)LX/Jxq;

    move-result-object v0

    iput-object v0, p0, LX/Jxw;->i:LX/Jxq;

    .line 2753856
    :cond_0
    iget-object v0, p0, LX/Jxw;->i:LX/Jxq;

    return-object v0

    .line 2753857
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2753852
    const/4 v0, 0x1

    return v0
.end method
