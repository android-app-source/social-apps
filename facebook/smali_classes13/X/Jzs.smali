.class public LX/Jzs;
.super LX/5pb;
.source ""

# interfaces
.implements LX/9mx;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FrescoModule"
.end annotation


# static fields
.field public static b:Z


# instance fields
.field private a:LX/1H6;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2756859
    const/4 v0, 0x0

    sput-boolean v0, LX/Jzs;->b:Z

    return-void
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2756860
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Jzs;-><init>(LX/5pY;LX/1H6;)V

    .line 2756861
    return-void
.end method

.method private constructor <init>(LX/5pY;LX/1H6;)V
    .locals 0
    .param p2    # LX/1H6;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2756862
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2756863
    iput-object p2, p0, LX/Jzs;->a:LX/1H6;

    .line 2756864
    return-void
.end method

.method private static a(Landroid/content/Context;)LX/1H6;
    .locals 1

    .prologue
    .line 2756865
    invoke-static {p0}, LX/Jzs;->b(Landroid/content/Context;)LX/1H8;

    move-result-object v0

    invoke-virtual {v0}, LX/1H8;->c()LX/1H6;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;)LX/1H8;
    .locals 4

    .prologue
    .line 2756866
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2756867
    new-instance v1, LX/Jzt;

    invoke-direct {v1}, LX/Jzt;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2756868
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, LX/9nD;->a()LX/64w;

    move-result-object v2

    .line 2756869
    invoke-static {v1}, LX/1H6;->a(Landroid/content/Context;)LX/1H8;

    move-result-object v3

    new-instance p0, LX/JbM;

    invoke-direct {p0, v2}, LX/JbM;-><init>(LX/64w;)V

    .line 2756870
    iput-object p0, v3, LX/1H8;->n:LX/1Gj;

    .line 2756871
    move-object v3, v3

    .line 2756872
    move-object v1, v3

    .line 2756873
    const/4 v2, 0x0

    .line 2756874
    iput-boolean v2, v1, LX/1H8;->f:Z

    .line 2756875
    move-object v1, v1

    .line 2756876
    iput-object v0, v1, LX/1H8;->r:Ljava/util/Set;

    .line 2756877
    move-object v0, v1

    .line 2756878
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2756879
    invoke-static {}, LX/4AN;->b()LX/1HI;

    move-result-object v0

    invoke-virtual {v0}, LX/1HI;->b()V

    .line 2756880
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2756881
    invoke-super {p0}, LX/5pb;->d()V

    .line 2756882
    sget-boolean v0, LX/Jzs;->b:Z

    move v0, v0

    .line 2756883
    if-nez v0, :cond_2

    .line 2756884
    new-instance v0, LX/Jzr;

    invoke-direct {v0}, LX/Jzr;-><init>()V

    invoke-static {v0}, LX/02C;->a(LX/02B;)V

    .line 2756885
    iget-object v0, p0, LX/Jzs;->a:LX/1H6;

    if-nez v0, :cond_0

    .line 2756886
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756887
    invoke-static {v0}, LX/Jzs;->a(Landroid/content/Context;)LX/1H6;

    move-result-object v0

    iput-object v0, p0, LX/Jzs;->a:LX/1H6;

    .line 2756888
    :cond_0
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756889
    invoke-virtual {v0}, LX/5pY;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2756890
    iget-object v1, p0, LX/Jzs;->a:LX/1H6;

    .line 2756891
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/4AN;->a(Landroid/content/Context;LX/1H6;LX/4AM;)V

    .line 2756892
    const/4 v0, 0x1

    sput-boolean v0, LX/Jzs;->b:Z

    .line 2756893
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jzs;->a:LX/1H6;

    .line 2756894
    return-void

    .line 2756895
    :cond_2
    iget-object v0, p0, LX/Jzs;->a:LX/1H6;

    if-eqz v0, :cond_1

    .line 2756896
    const-string v0, "React"

    const-string v1, "Fresco has already been initialized with a different config. The new Fresco configuration will be ignored!"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2756897
    const-string v0, "FrescoModule"

    return-object v0
.end method
