.class public LX/K6a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/K5t;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/K7m;

.field private final c:LX/K8t;

.field private final d:LX/K8M;

.field private final e:LX/K8s;

.field private f:Ljava/lang/String;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/K79;

.field private i:Z

.field private final j:LX/K6X;

.field public k:LX/K6Z;


# direct methods
.method public constructor <init>(LX/K7m;LX/K8t;LX/K8M;LX/K8s;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2771954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2771955
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    .line 2771956
    new-instance v0, LX/K6Y;

    invoke-direct {v0, p0}, LX/K6Y;-><init>(LX/K6a;)V

    iput-object v0, p0, LX/K6a;->j:LX/K6X;

    .line 2771957
    new-instance v0, LX/K6Z;

    invoke-direct {v0, p0}, LX/K6Z;-><init>(LX/K6a;)V

    iput-object v0, p0, LX/K6a;->k:LX/K6Z;

    .line 2771958
    iput-object p1, p0, LX/K6a;->b:LX/K7m;

    .line 2771959
    iput-object p2, p0, LX/K6a;->c:LX/K8t;

    .line 2771960
    iput-object p3, p0, LX/K6a;->d:LX/K8M;

    .line 2771961
    iput-object p4, p0, LX/K6a;->e:LX/K8s;

    .line 2771962
    invoke-virtual {p0}, LX/K6a;->a()V

    .line 2771963
    return-void
.end method

.method public static a(LX/0QB;)LX/K6a;
    .locals 7

    .prologue
    .line 2772005
    const-class v1, LX/K6a;

    monitor-enter v1

    .line 2772006
    :try_start_0
    sget-object v0, LX/K6a;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2772007
    sput-object v2, LX/K6a;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2772008
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2772009
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2772010
    new-instance p0, LX/K6a;

    invoke-static {v0}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v3

    check-cast v3, LX/K7m;

    invoke-static {v0}, LX/K8t;->a(LX/0QB;)LX/K8t;

    move-result-object v4

    check-cast v4, LX/K8t;

    invoke-static {v0}, LX/K8M;->a(LX/0QB;)LX/K8M;

    move-result-object v5

    check-cast v5, LX/K8M;

    invoke-static {v0}, LX/K8s;->a(LX/0QB;)LX/K8s;

    move-result-object v6

    check-cast v6, LX/K8s;

    invoke-direct {p0, v3, v4, v5, v6}, LX/K6a;-><init>(LX/K7m;LX/K8t;LX/K8M;LX/K8s;)V

    .line 2772011
    move-object v0, p0

    .line 2772012
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2772013
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K6a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2772014
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2772015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/K6a;LX/K6Z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2772000
    iget-boolean v0, p1, LX/K6Z;->c:Z

    if-eqz v0, :cond_0

    .line 2772001
    invoke-static {p0}, LX/K6a;->f(LX/K6a;)Ljava/lang/String;

    move-result-object v0

    .line 2772002
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    iget v1, p1, LX/K6Z;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2772003
    iget-object v1, v0, LX/K5t;->b:LX/K5s;

    move-object v0, v1

    .line 2772004
    iget-object v0, v0, LX/K5s;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a$redex0(LX/K6a;LX/K7r;)V
    .locals 14

    .prologue
    .line 2771964
    iget v0, p1, LX/K7r;->a:I

    move v0, v0

    .line 2771965
    invoke-static {p0, v0}, LX/K6a;->b(LX/K6a;I)LX/K6Z;

    move-result-object v9

    .line 2771966
    if-eqz v9, :cond_3

    iget-object v0, p0, LX/K6a;->k:LX/K6Z;

    if-eqz v0, :cond_3

    .line 2771967
    iget-object v0, p0, LX/K6a;->c:LX/K8t;

    .line 2771968
    iget-object v1, p0, LX/K6a;->k:LX/K6Z;

    invoke-static {p0, v1}, LX/K6a;->a(LX/K6a;LX/K6Z;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2771969
    invoke-static {p0, v9}, LX/K6a;->a(LX/K6a;LX/K6Z;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/K6a;->k:LX/K6Z;

    invoke-static {p0, v3}, LX/K6a;->b(LX/K6a;LX/K6Z;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/K6a;->k:LX/K6Z;

    iget v4, v4, LX/K6Z;->b:I

    invoke-static {p0, v9}, LX/K6a;->b(LX/K6a;LX/K6Z;)Ljava/lang/String;

    move-result-object v5

    iget v6, v9, LX/K6Z;->b:I

    iget-object v7, p0, LX/K6a;->k:LX/K6Z;

    const/4 v8, 0x0

    .line 2771970
    iget-boolean v10, v7, LX/K6Z;->c:Z

    if-eqz v10, :cond_5

    .line 2771971
    const/4 v8, 0x0

    .line 2771972
    iget-object v10, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v10, v8

    move v11, v8

    :goto_0
    if-ge v10, v7, :cond_0

    iget-object v8, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/K5t;

    .line 2771973
    invoke-virtual {v8}, LX/K5t;->b()I

    move-result v8

    add-int/2addr v11, v8

    .line 2771974
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_0

    .line 2771975
    :cond_0
    move v8, v11

    .line 2771976
    :goto_1
    move v7, v8

    .line 2771977
    iget v8, p1, LX/K7r;->a:I

    move v8, v8

    .line 2771978
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v10

    const-string v11, "nav_from_digest_id"

    invoke-virtual {v10, v11, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_to_digest_id"

    invoke-virtual {v10, v11, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_from_card_deck_index"

    invoke-virtual {v10, v11, v4}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_from_card_deck_id"

    invoke-virtual {v10, v11, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_to_card_deck_index"

    invoke-virtual {v10, v11, v6}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_to_card_deck_id"

    invoke-virtual {v10, v11, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_from_pager_index"

    invoke-virtual {v10, v11, v7}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v10

    const-string v11, "nav_to_pager_index"

    invoke-virtual {v10, v11, v8}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v10

    .line 2771979
    iget-object v11, v0, LX/K8t;->i:LX/0if;

    sget-object v12, LX/0ig;->aO:LX/0ih;

    const-string v13, "tarot_navigation"

    const-string p1, "tarot_card_deck"

    invoke-virtual {v11, v12, v13, p1, v10}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2771980
    iget-boolean v0, v9, LX/K6Z;->c:Z

    .line 2771981
    iget-object v1, p0, LX/K6a;->k:LX/K6Z;

    iget-boolean v1, v1, LX/K6Z;->c:Z

    .line 2771982
    if-eqz v0, :cond_1

    .line 2771983
    iget-object v2, p0, LX/K6a;->b:LX/K7m;

    new-instance v3, LX/K7z;

    invoke-static {p0}, LX/K6a;->f(LX/K6a;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/K7z;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2771984
    :cond_1
    if-eqz v1, :cond_2

    .line 2771985
    iget-object v2, p0, LX/K6a;->b:LX/K7m;

    new-instance v3, LX/K7y;

    invoke-static {p0}, LX/K6a;->f(LX/K6a;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/K7y;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2771986
    :cond_2
    if-nez v0, :cond_3

    if-nez v1, :cond_3

    iget v0, v9, LX/K6Z;->a:I

    iget-object v1, p0, LX/K6a;->k:LX/K6Z;

    iget v1, v1, LX/K6Z;->a:I

    if-eq v0, v1, :cond_3

    .line 2771987
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    iget v1, v9, LX/K6Z;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/K5t;

    .line 2771988
    iget-object v7, p0, LX/K6a;->b:LX/K7m;

    new-instance v0, LX/K80;

    iget-object v1, p0, LX/K6a;->k:LX/K6Z;

    iget v1, v1, LX/K6Z;->a:I

    iget v2, v9, LX/K6Z;->a:I

    iget-object v3, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2771989
    iget-object v4, v6, LX/K5t;->b:LX/K5s;

    move-object v4, v4

    .line 2771990
    iget-object v4, v4, LX/K5s;->b:Ljava/lang/String;

    iget v5, v9, LX/K6Z;->b:I

    invoke-virtual {v6}, LX/K5t;->b()I

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/K80;-><init>(IIILjava/lang/String;II)V

    invoke-virtual {v7, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2771991
    :cond_3
    iput-object v9, p0, LX/K6a;->k:LX/K6Z;

    .line 2771992
    iget-boolean v0, p0, LX/K6a;->i:Z

    if-eqz v0, :cond_4

    .line 2771993
    iget-object v0, p0, LX/K6a;->e:LX/K8s;

    iget-object v1, p0, LX/K6a;->h:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    invoke-virtual {p0}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/K8s;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2771994
    :cond_4
    return-void

    :cond_5
    move v10, v8

    move v11, v8

    .line 2771995
    :goto_2
    iget v8, v7, LX/K6Z;->a:I

    if-ge v10, v8, :cond_6

    .line 2771996
    iget-object v8, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/K5t;

    invoke-virtual {v8}, LX/K5t;->b()I

    move-result v8

    add-int/2addr v11, v8

    .line 2771997
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_2

    .line 2771998
    :cond_6
    iget v8, v7, LX/K6Z;->b:I

    add-int/2addr v8, v11

    .line 2771999
    goto/16 :goto_1
.end method

.method public static b(LX/K6a;I)LX/K6Z;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2771933
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    move-object v0, v1

    .line 2771934
    :goto_0
    return-object v0

    :cond_1
    move v2, v3

    .line 2771935
    :goto_1
    if-ltz p1, :cond_4

    .line 2771936
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_2

    .line 2771937
    new-instance v0, LX/K6Z;

    invoke-direct {v0, p0}, LX/K6Z;-><init>(LX/K6a;)V

    .line 2771938
    invoke-virtual {v0}, LX/K6Z;->a()V

    .line 2771939
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/K6Z;->c:Z

    .line 2771940
    move-object v0, v0

    .line 2771941
    goto :goto_0

    .line 2771942
    :cond_2
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2771943
    invoke-virtual {v0}, LX/K5t;->b()I

    move-result v4

    if-ge p1, v4, :cond_3

    .line 2771944
    new-instance v0, LX/K6Z;

    invoke-direct {v0, p0}, LX/K6Z;-><init>(LX/K6a;)V

    .line 2771945
    iput v2, v0, LX/K6Z;->a:I

    .line 2771946
    iput p1, v0, LX/K6Z;->b:I

    .line 2771947
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/K6Z;->c:Z

    .line 2771948
    move-object v0, v0

    .line 2771949
    goto :goto_0

    .line 2771950
    :cond_3
    invoke-virtual {v0}, LX/K5t;->b()I

    move-result v0

    sub-int/2addr p1, v0

    .line 2771951
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 2771952
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 2771953
    goto :goto_0
.end method

.method private static b(LX/K6a;LX/K6Z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2772016
    iget-boolean v0, p1, LX/K6Z;->c:Z

    if-eqz v0, :cond_0

    .line 2772017
    invoke-static {p0}, LX/K6a;->f(LX/K6a;)Ljava/lang/String;

    move-result-object v0

    .line 2772018
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    iget v1, p1, LX/K6Z;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    iget v1, p1, LX/K6Z;->b:I

    invoke-virtual {v0, v1}, LX/K5t;->a(I)LX/K7D;

    move-result-object v0

    .line 2772019
    iget-object v1, v0, LX/K7D;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2772020
    goto :goto_0
.end method

.method public static f(LX/K6a;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2771923
    iget-object v0, p0, LX/K6a;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2771924
    iget-object v0, p0, LX/K6a;->f:Ljava/lang/String;

    .line 2771925
    :goto_0
    return-object v0

    .line 2771926
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "endcard"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2771927
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_1

    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2771928
    const-string v5, "_%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    .line 2771929
    iget-object v7, v0, LX/K5t;->c:Ljava/lang/String;

    move-object v0, v7

    .line 2771930
    aput-object v0, v6, v2

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2771931
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2771932
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2771846
    iget-object v0, p0, LX/K6a;->k:LX/K6Z;

    invoke-virtual {v0}, LX/K6Z;->a()V

    .line 2771847
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2771848
    iget-object v0, p0, LX/K6a;->b:LX/K7m;

    iget-object v1, p0, LX/K6a;->j:LX/K6X;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2771849
    const/4 v0, 0x0

    iput-object v0, p0, LX/K6a;->f:Ljava/lang/String;

    .line 2771850
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K6a;->i:Z

    .line 2771851
    return-void
.end method

.method public final a(LX/K5t;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2771898
    iget-object v0, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2771899
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2771900
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/K5t;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2771901
    invoke-virtual {p1, v0}, LX/K5t;->a(I)LX/K7D;

    move-result-object v2

    .line 2771902
    iget-object v4, v2, LX/K7D;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2771903
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2771904
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2771905
    :cond_0
    iget-object v0, p0, LX/K6a;->d:LX/K8M;

    .line 2771906
    iget-object v4, p1, LX/K5t;->e:LX/8GE;

    if-nez v4, :cond_2

    iget-object v4, p1, LX/K5t;->f:LX/8GE;

    if-nez v4, :cond_2

    .line 2771907
    :cond_1
    :goto_1
    iput-object v3, p0, LX/K6a;->f:Ljava/lang/String;

    .line 2771908
    iput-object v3, p0, LX/K6a;->g:Ljava/util/List;

    .line 2771909
    return-void

    .line 2771910
    :cond_2
    iget-object v4, v0, LX/K8M;->d:Ljava/util/Map;

    iget-object v5, p1, LX/K5t;->c:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2771911
    iget-object v10, v0, LX/K8M;->e:Ljava/util/Map;

    iget-object v11, p1, LX/K5t;->c:Ljava/lang/String;

    new-instance v4, LX/K8L;

    iget-object v6, p1, LX/K5t;->e:LX/8GE;

    iget-object v7, p1, LX/K5t;->f:LX/8GE;

    iget-object v8, p1, LX/K5t;->i:Ljava/lang/String;

    iget-object v9, p1, LX/K5t;->j:Ljava/lang/String;

    move-object v5, v0

    invoke-direct/range {v4 .. v9}, LX/K8L;-><init>(LX/K8M;LX/8GE;LX/8GE;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771912
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2771913
    iget-object v5, p1, LX/K5t;->e:LX/8GE;

    invoke-static {v4, v5}, LX/K8M;->a(LX/0Pz;LX/8GE;)V

    .line 2771914
    iget-object v5, p1, LX/K5t;->f:LX/8GE;

    invoke-static {v4, v5}, LX/K8M;->a(LX/0Pz;LX/8GE;)V

    .line 2771915
    iget-object v5, v0, LX/K8M;->a:LX/8Yg;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/8Yg;->a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2771916
    const/4 v6, 0x0

    .line 2771917
    if-eqz v4, :cond_3

    invoke-interface {v4}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    move v5, v6

    .line 2771918
    :goto_2
    move v5, v5

    .line 2771919
    if-eqz v5, :cond_1

    .line 2771920
    iget-object v5, v0, LX/K8M;->d:Ljava/util/Map;

    iget-object v6, p1, LX/K5t;->c:Ljava/lang/String;

    new-instance v7, LX/K8J;

    invoke-direct {v7, v0, p1}, LX/K8J;-><init>(LX/K8M;LX/K5t;)V

    iget-object v8, v0, LX/K8M;->c:LX/0TD;

    invoke-static {v4, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2771921
    :cond_4
    const v5, 0x1def839e

    :try_start_0
    invoke-static {v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    goto :goto_2

    :cond_5
    move v5, v6

    goto :goto_2

    .line 2771922
    :catch_0
    move v5, v6

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2771896
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K6a;->i:Z

    .line 2771897
    return-void
.end method

.method public final c()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2771852
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2771853
    iget-object v1, p0, LX/K6a;->h:LX/K79;

    if-eqz v1, :cond_0

    .line 2771854
    iget-object v1, p0, LX/K6a;->h:LX/K79;

    invoke-virtual {v1, v0}, LX/K79;->a(Ljava/util/Map;)V

    .line 2771855
    :cond_0
    iget-object v1, p0, LX/K6a;->k:LX/K6Z;

    if-nez v1, :cond_1

    .line 2771856
    :goto_0
    return-object v0

    .line 2771857
    :cond_1
    const-string v1, "chained_digest_ids"

    .line 2771858
    iget-object v2, p0, LX/K6a;->g:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 2771859
    iget-object v2, p0, LX/K6a;->g:Ljava/util/List;

    .line 2771860
    :goto_1
    move-object v2, v2

    .line 2771861
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771862
    const-string v1, "tarot_session_length"

    const/4 v2, 0x0

    .line 2771863
    iget-object v3, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    move v4, v2

    :goto_2
    if-ge v3, v5, :cond_2

    iget-object v2, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K5t;

    .line 2771864
    invoke-virtual {v2}, LX/K5t;->b()I

    move-result v2

    add-int/2addr v4, v2

    .line 2771865
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2771866
    :cond_2
    add-int/lit8 v2, v4, 0x1

    .line 2771867
    move v2, v2

    .line 2771868
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771869
    const-string v1, "digest_id"

    iget-object v2, p0, LX/K6a;->k:LX/K6Z;

    .line 2771870
    iget-boolean v3, v2, LX/K6Z;->c:Z

    if-eqz v3, :cond_5

    .line 2771871
    const/4 v3, 0x0

    .line 2771872
    :goto_3
    move-object v2, v3

    .line 2771873
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771874
    const-string v1, "page_id"

    .line 2771875
    iget-object v2, p0, LX/K6a;->k:LX/K6Z;

    iget-boolean v2, v2, LX/K6Z;->c:Z

    if-eqz v2, :cond_6

    .line 2771876
    invoke-static {p0}, LX/K6a;->f(LX/K6a;)Ljava/lang/String;

    move-result-object v2

    .line 2771877
    :goto_4
    move-object v2, v2

    .line 2771878
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771879
    const-string v1, "card_type"

    iget-object v2, p0, LX/K6a;->k:LX/K6Z;

    .line 2771880
    iget-boolean v3, v2, LX/K6Z;->c:Z

    if-eqz v3, :cond_7

    sget-object v3, LX/K8u;->END_CARD:LX/K8u;

    iget-object v3, v3, LX/K8u;->mName:Ljava/lang/String;

    :goto_5
    move-object v2, v3

    .line 2771881
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771882
    const-string v1, "card_id"

    iget-object v2, p0, LX/K6a;->k:LX/K6Z;

    invoke-static {p0, v2}, LX/K6a;->b(LX/K6a;LX/K6Z;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771883
    const-string v1, "tarot_session_index"

    iget-object v2, p0, LX/K6a;->k:LX/K6Z;

    iget v2, v2, LX/K6Z;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771884
    const-string v1, "digest_card_index"

    iget-object v2, p0, LX/K6a;->k:LX/K6Z;

    iget v2, v2, LX/K6Z;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2771885
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/K6a;->g:Ljava/util/List;

    .line 2771886
    iget-object v2, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v4, :cond_4

    iget-object v2, p0, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K5t;

    .line 2771887
    iget-object v5, p0, LX/K6a;->g:Ljava/util/List;

    .line 2771888
    iget-object v6, v2, LX/K5t;->c:Ljava/lang/String;

    move-object v2, v6

    .line 2771889
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2771890
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 2771891
    :cond_4
    iget-object v2, p0, LX/K6a;->g:Ljava/util/List;

    goto/16 :goto_1

    :cond_5
    iget-object v3, v2, LX/K6Z;->d:LX/K6a;

    iget-object v3, v3, LX/K6a;->a:Ljava/util/ArrayList;

    iget v4, v2, LX/K6Z;->a:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/K5t;

    .line 2771892
    iget-object v4, v3, LX/K5t;->c:Ljava/lang/String;

    move-object v3, v4

    .line 2771893
    goto/16 :goto_3

    :cond_6
    iget-object v2, p0, LX/K6a;->a:Ljava/util/ArrayList;

    iget-object v3, p0, LX/K6a;->k:LX/K6Z;

    iget v3, v3, LX/K6Z;->a:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K5t;

    .line 2771894
    iget-object v3, v2, LX/K5t;->b:LX/K5s;

    move-object v2, v3

    .line 2771895
    iget-object v2, v2, LX/K5s;->a:Ljava/lang/String;

    goto/16 :goto_4

    :cond_7
    iget v3, v2, LX/K6Z;->b:I

    if-nez v3, :cond_8

    sget-object v3, LX/K8u;->DIGEST_COVER:LX/K8u;

    iget-object v3, v3, LX/K8u;->mName:Ljava/lang/String;

    goto :goto_5

    :cond_8
    sget-object v3, LX/K8u;->STORY_CARD:LX/K8u;

    iget-object v3, v3, LX/K8u;->mName:Ljava/lang/String;

    goto/16 :goto_5
.end method
