.class public LX/K8f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2775658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2775659
    iput-object p1, p0, LX/K8f;->a:LX/0tX;

    .line 2775660
    iput-object p2, p0, LX/K8f;->b:Ljava/util/concurrent/ExecutorService;

    .line 2775661
    return-void
.end method

.method public static b(LX/0QB;)LX/K8f;
    .locals 3

    .prologue
    .line 2775684
    new-instance v2, LX/K8f;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v2, v0, v1}, LX/K8f;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 2775685
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2775673
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2775674
    :goto_0
    return-void

    .line 2775675
    :cond_0
    new-instance v0, LX/4Jf;

    invoke-direct {v0}, LX/4Jf;-><init>()V

    .line 2775676
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2775677
    move-object v0, v0

    .line 2775678
    new-instance v1, LX/K8g;

    invoke-direct {v1}, LX/K8g;-><init>()V

    move-object v1, v1

    .line 2775679
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2775680
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2775681
    new-instance v1, LX/K8d;

    invoke-direct {v1, p0}, LX/K8d;-><init>(LX/K8f;)V

    .line 2775682
    iget-object v2, p0, LX/K8f;->a:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2775683
    iget-object v2, p0, LX/K8f;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2775662
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2775663
    :goto_0
    return-void

    .line 2775664
    :cond_0
    new-instance v0, LX/4Jg;

    invoke-direct {v0}, LX/4Jg;-><init>()V

    .line 2775665
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2775666
    move-object v0, v0

    .line 2775667
    new-instance v1, LX/K8h;

    invoke-direct {v1}, LX/K8h;-><init>()V

    move-object v1, v1

    .line 2775668
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2775669
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2775670
    new-instance v1, LX/K8e;

    invoke-direct {v1, p0}, LX/K8e;-><init>(LX/K8f;)V

    .line 2775671
    iget-object v2, p0, LX/K8f;->a:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2775672
    iget-object v2, p0, LX/K8f;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
