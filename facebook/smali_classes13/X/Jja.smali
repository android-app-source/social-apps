.class public final LX/Jja;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V
    .locals 0

    .prologue
    .line 2727957
    iput-object p1, p0, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x56ae029b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2727958
    iget-object v1, p0, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->s:Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-static {v1}, Lcom/facebook/messaging/events/banner/EventReminderParams;->a(Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/JjW;

    move-result-object v1

    iget-object v2, p0, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727959
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v2, v3

    .line 2727960
    iput-object v2, v1, LX/JjW;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727961
    move-object v1, v1

    .line 2727962
    iget-object v2, p0, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727963
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2727964
    iput-object v2, v1, LX/JjW;->h:Ljava/lang/String;

    .line 2727965
    move-object v1, v1

    .line 2727966
    iget-object v2, p0, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    .line 2727967
    iput-object v2, v1, LX/JjW;->e:Ljava/lang/String;

    .line 2727968
    move-object v1, v1

    .line 2727969
    invoke-virtual {v1}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v1

    .line 2727970
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727971
    iget-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727972
    iget-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727973
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2727974
    const-string v3, "reminder_params"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2727975
    new-instance v3, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    invoke-direct {v3}, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;-><init>()V

    .line 2727976
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2727977
    move-object v1, v3

    .line 2727978
    new-instance v2, LX/JjZ;

    invoke-direct {v2, p0}, LX/JjZ;-><init>(LX/Jja;)V

    .line 2727979
    iput-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->u:LX/JjZ;

    .line 2727980
    iget-object v2, p0, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "edit_event_reminder_title"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2727981
    const v1, 0x42753b92

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
