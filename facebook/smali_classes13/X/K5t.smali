.class public LX/K5t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/K7D;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/K5s;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final e:LX/8GE;

.field public final f:LX/8GE;

.field public final g:F

.field public final h:F

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2770723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770724
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/K5t;->a:Ljava/util/ArrayList;

    .line 2770725
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/K5t;->c:Ljava/lang/String;

    .line 2770726
    new-instance v1, LX/K5s;

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/K5s;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, LX/K5t;->b:LX/K5s;

    .line 2770727
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, p0, LX/K5t;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2770728
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v1

    iput-object v1, p0, LX/K5t;->e:LX/8GE;

    .line 2770729
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v1

    iput-object v1, p0, LX/K5t;->f:LX/8GE;

    .line 2770730
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->t()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/K5t;->i:Ljava/lang/String;

    .line 2770731
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/K5t;->j:Ljava/lang/String;

    .line 2770732
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->u()D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, LX/K5t;->g:F

    .line 2770733
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->m()D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, LX/K5t;->h:F

    .line 2770734
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2770735
    const-string v1, "cover_%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/K5t;->c:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2770736
    new-instance v3, LX/K7D;

    invoke-direct {v3, v1}, LX/K7D;-><init>(Ljava/lang/String;)V

    .line 2770737
    new-instance v4, Lcom/facebook/tarot/data/SlideshowData;

    invoke-direct {v4}, Lcom/facebook/tarot/data/SlideshowData;-><init>()V

    .line 2770738
    new-instance v5, Lcom/facebook/tarot/data/TarotCardCoverData;

    invoke-direct {v5, v1, p1, v4}, Lcom/facebook/tarot/data/TarotCardCoverData;-><init>(Ljava/lang/String;Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;Lcom/facebook/tarot/data/SlideshowData;)V

    .line 2770739
    invoke-virtual {v3, v5}, LX/K7D;->a(Lcom/facebook/tarot/data/BaseTarotCardData;)V

    .line 2770740
    iget-object v1, p0, LX/K5t;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 2770741
    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2770742
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;

    .line 2770743
    new-instance v3, LX/K7D;

    iget-object v4, p0, LX/K5t;->c:Ljava/lang/String;

    iget v6, p0, LX/K5t;->g:F

    iget v7, p0, LX/K5t;->h:F

    invoke-direct {v3, v0, v4, v6, v7}, LX/K7D;-><init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;Ljava/lang/String;FF)V

    .line 2770744
    iget-object v0, p0, LX/K5t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2770745
    iget-object v0, v3, LX/K7D;->e:Ljava/util/ArrayList;

    move-object v0, v0

    .line 2770746
    invoke-virtual {v5, v0}, Lcom/facebook/tarot/data/TarotCardCoverData;->a(Ljava/util/List;)V

    .line 2770747
    iget-object v0, v3, LX/K7D;->f:Ljava/util/ArrayList;

    move-object v0, v0

    .line 2770748
    invoke-virtual {v5, v0}, Lcom/facebook/tarot/data/TarotCardCoverData;->b(Ljava/util/List;)V

    .line 2770749
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2770750
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/K7D;
    .locals 1

    .prologue
    .line 2770751
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/K5t;->b()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2770752
    :cond_0
    const/4 v0, 0x0

    .line 2770753
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/K5t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7D;

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2770754
    iget-object v0, p0, LX/K5t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
