.class public final LX/K4B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2767551
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2767552
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2767553
    if-eqz v0, :cond_0

    .line 2767554
    const-string v1, "audio_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767555
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2767556
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2767557
    if-eqz v0, :cond_1

    .line 2767558
    const-string v1, "audio_streaming_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767559
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2767560
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2767561
    if-eqz v0, :cond_2

    .line 2767562
    const-string v1, "duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767563
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2767564
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2767565
    if-eqz v0, :cond_3

    .line 2767566
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767567
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2767568
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2767569
    if-eqz v0, :cond_4

    .line 2767570
    const-string v1, "max_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767571
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2767572
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2767573
    if-eqz v0, :cond_5

    .line 2767574
    const-string v1, "min_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767575
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2767576
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2767577
    if-eqz v0, :cond_6

    .line 2767578
    const-string v1, "timing_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767579
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2767580
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2767581
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 2767582
    const/4 v9, 0x0

    .line 2767583
    const/4 v8, 0x0

    .line 2767584
    const/4 v7, 0x0

    .line 2767585
    const/4 v6, 0x0

    .line 2767586
    const/4 v5, 0x0

    .line 2767587
    const/4 v4, 0x0

    .line 2767588
    const/4 v3, 0x0

    .line 2767589
    const/4 v2, 0x0

    .line 2767590
    const/4 v1, 0x0

    .line 2767591
    const/4 v0, 0x0

    .line 2767592
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 2767593
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2767594
    const/4 v0, 0x0

    .line 2767595
    :goto_0
    return v0

    .line 2767596
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2767597
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 2767598
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2767599
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2767600
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2767601
    const-string v11, "audio_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2767602
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2767603
    :cond_2
    const-string v11, "audio_streaming_url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2767604
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2767605
    :cond_3
    const-string v11, "duration"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2767606
    const/4 v2, 0x1

    .line 2767607
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 2767608
    :cond_4
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2767609
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2767610
    :cond_5
    const-string v11, "max_photos"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2767611
    const/4 v1, 0x1

    .line 2767612
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 2767613
    :cond_6
    const-string v11, "min_photos"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 2767614
    const/4 v0, 0x1

    .line 2767615
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    goto :goto_1

    .line 2767616
    :cond_7
    const-string v11, "timing_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2767617
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 2767618
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2767619
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2767620
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 2767621
    if-eqz v2, :cond_9

    .line 2767622
    const/4 v2, 0x2

    const/4 v8, 0x0

    invoke-virtual {p1, v2, v7, v8}, LX/186;->a(III)V

    .line 2767623
    :cond_9
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2767624
    if-eqz v1, :cond_a

    .line 2767625
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 2767626
    :cond_a
    if-eqz v0, :cond_b

    .line 2767627
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 2767628
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2767629
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
