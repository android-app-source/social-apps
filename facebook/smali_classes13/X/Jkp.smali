.class public final enum LX/Jkp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jkp;

.field public static final enum ENTERING:LX/Jkp;

.field public static final enum EXITING:LX/Jkp;

.field public static final enum GONE:LX/Jkp;

.field public static final enum VISIBLE:LX/Jkp;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2730343
    new-instance v0, LX/Jkp;

    const-string v1, "ENTERING"

    invoke-direct {v0, v1, v2}, LX/Jkp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jkp;->ENTERING:LX/Jkp;

    .line 2730344
    new-instance v0, LX/Jkp;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v3}, LX/Jkp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jkp;->VISIBLE:LX/Jkp;

    .line 2730345
    new-instance v0, LX/Jkp;

    const-string v1, "EXITING"

    invoke-direct {v0, v1, v4}, LX/Jkp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jkp;->EXITING:LX/Jkp;

    .line 2730346
    new-instance v0, LX/Jkp;

    const-string v1, "GONE"

    invoke-direct {v0, v1, v5}, LX/Jkp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jkp;->GONE:LX/Jkp;

    .line 2730347
    const/4 v0, 0x4

    new-array v0, v0, [LX/Jkp;

    sget-object v1, LX/Jkp;->ENTERING:LX/Jkp;

    aput-object v1, v0, v2

    sget-object v1, LX/Jkp;->VISIBLE:LX/Jkp;

    aput-object v1, v0, v3

    sget-object v1, LX/Jkp;->EXITING:LX/Jkp;

    aput-object v1, v0, v4

    sget-object v1, LX/Jkp;->GONE:LX/Jkp;

    aput-object v1, v0, v5

    sput-object v0, LX/Jkp;->$VALUES:[LX/Jkp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2730340
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jkp;
    .locals 1

    .prologue
    .line 2730342
    const-class v0, LX/Jkp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jkp;

    return-object v0
.end method

.method public static values()[LX/Jkp;
    .locals 1

    .prologue
    .line 2730341
    sget-object v0, LX/Jkp;->$VALUES:[LX/Jkp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jkp;

    return-object v0
.end method
