.class public LX/JZQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JZO;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JZR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2708603
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JZQ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JZR;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708604
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2708605
    iput-object p1, p0, LX/JZQ;->b:LX/0Ot;

    .line 2708606
    return-void
.end method

.method public static a(LX/0QB;)LX/JZQ;
    .locals 4

    .prologue
    .line 2708607
    const-class v1, LX/JZQ;

    monitor-enter v1

    .line 2708608
    :try_start_0
    sget-object v0, LX/JZQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708609
    sput-object v2, LX/JZQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708610
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708611
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708612
    new-instance v3, LX/JZQ;

    const/16 p0, 0x21e5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JZQ;-><init>(LX/0Ot;)V

    .line 2708613
    move-object v0, v3

    .line 2708614
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708615
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708616
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708617
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2708618
    invoke-static {}, LX/1dS;->b()V

    .line 2708619
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2708620
    iget-object v0, p0, LX/JZQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2708621
    new-instance v0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    invoke-direct {v0, p1}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2708622
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 10

    .prologue
    .line 2708542
    check-cast p2, LX/JZP;

    .line 2708543
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2708544
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 2708545
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 2708546
    iget-object v0, p0, LX/JZQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JZR;

    iget-object v4, p2, LX/JZP;->a:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 2708547
    iget-object v5, v0, LX/JZR;->a:LX/JZM;

    .line 2708548
    iget-object v6, v5, LX/JZM;->a:LX/0Uh;

    const/16 v7, 0x647

    const/4 v0, 0x0

    invoke-virtual {v6, v7, v0}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v6, v6

    .line 2708549
    if-eqz v6, :cond_1

    .line 2708550
    iget-object v6, v5, LX/JZM;->c:LX/1Bv;

    invoke-virtual {v6}, LX/1Bv;->a()Z

    move-result v6

    move v6, v6

    .line 2708551
    if-eqz v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    move v6, v6

    .line 2708552
    const/4 v5, 0x5

    const/4 v7, 0x0

    .line 2708553
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2708554
    if-nez v4, :cond_2

    move-object v7, v8

    .line 2708555
    :goto_1
    move-object v7, v7

    .line 2708556
    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 2708557
    :goto_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2708558
    if-nez v4, :cond_8

    move-object v8, v9

    .line 2708559
    :goto_3
    move-object v5, v8

    .line 2708560
    iput-object v5, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2708561
    iput-object v7, v2, LX/1np;->a:Ljava/lang/Object;

    .line 2708562
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 2708563
    iput-object v5, v3, LX/1np;->a:Ljava/lang/Object;

    .line 2708564
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2708565
    check-cast v0, Ljava/util/List;

    iput-object v0, p2, LX/JZP;->b:Ljava/util/List;

    .line 2708566
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2708567
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2708568
    check-cast v0, Ljava/util/List;

    iput-object v0, p2, LX/JZP;->c:Ljava/util/List;

    .line 2708569
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 2708570
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2708571
    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p2, LX/JZP;->d:Ljava/lang/Boolean;

    .line 2708572
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 2708573
    return-void

    .line 2708574
    :cond_0
    const/4 v5, 0x0

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 2708575
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o()LX/0Px;

    move-result-object p1

    .line 2708576
    if-eqz p1, :cond_3

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    :cond_3
    move-object v7, v8

    .line 2708577
    goto :goto_1

    .line 2708578
    :cond_4
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    move p0, v7

    move v9, v7

    :goto_4
    if-ge p0, v0, :cond_6

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLTarotCard;

    .line 2708579
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTarotCard;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v7

    .line 2708580
    if-eqz v7, :cond_7

    .line 2708581
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2708582
    add-int/lit8 v7, v9, 0x1

    .line 2708583
    :goto_5
    if-ltz v5, :cond_5

    if-ge v7, v5, :cond_6

    .line 2708584
    :cond_5
    add-int/lit8 v9, p0, 0x1

    move p0, v9

    move v9, v7

    goto :goto_4

    :cond_6
    move-object v7, v8

    .line 2708585
    goto :goto_1

    :cond_7
    move v7, v9

    goto :goto_5

    .line 2708586
    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o()LX/0Px;

    move-result-object p1

    .line 2708587
    if-eqz p1, :cond_9

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_a

    :cond_9
    move-object v8, v9

    .line 2708588
    goto :goto_3

    .line 2708589
    :cond_a
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v8, 0x0

    move p0, v8

    :goto_6
    if-ge p0, v0, :cond_c

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLTarotCard;

    .line 2708590
    invoke-static {v8, v5}, LX/JZE;->a(Lcom/facebook/graphql/model/GraphQLTarotCard;Z)Ljava/lang/String;

    move-result-object v8

    .line 2708591
    if-eqz v8, :cond_b

    .line 2708592
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2708593
    :cond_b
    add-int/lit8 v8, p0, 0x1

    move p0, v8

    goto :goto_6

    :cond_c
    move-object v8, v9

    .line 2708594
    goto/16 :goto_3
.end method

.method public final c(LX/1De;)LX/JZO;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2708595
    new-instance v1, LX/JZP;

    invoke-direct {v1, p0}, LX/JZP;-><init>(LX/JZQ;)V

    .line 2708596
    sget-object v2, LX/JZQ;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JZO;

    .line 2708597
    if-nez v2, :cond_0

    .line 2708598
    new-instance v2, LX/JZO;

    invoke-direct {v2}, LX/JZO;-><init>()V

    .line 2708599
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JZO;->a$redex0(LX/JZO;LX/1De;IILX/JZP;)V

    .line 2708600
    move-object v1, v2

    .line 2708601
    move-object v0, v1

    .line 2708602
    return-object v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 2708526
    check-cast p3, LX/JZP;

    .line 2708527
    iget-object v0, p0, LX/JZQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    iget-object v0, p3, LX/JZP;->b:Ljava/util/List;

    iget-object v1, p3, LX/JZP;->c:Ljava/util/List;

    iget-object v2, p3, LX/JZP;->d:Ljava/lang/Boolean;

    .line 2708528
    if-eqz p2, :cond_3

    .line 2708529
    iget-object p0, p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 2708530
    iget-object p0, p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->f:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 2708531
    invoke-virtual {p2}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->removeAllViews()V

    .line 2708532
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2708533
    invoke-virtual {p2, v1}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->b(Ljava/util/List;)V

    .line 2708534
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 2708535
    if-eqz p0, :cond_1

    .line 2708536
    new-instance v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2708537
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    sget-object v2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2708538
    iget-object p0, p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->c:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2708539
    invoke-virtual {p2, v1}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2708540
    :cond_2
    const/16 p0, 0xbb8

    invoke-virtual {p2, p0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->setFlipInterval(I)V

    .line 2708541
    :cond_3
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2708525
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 2708523
    iget-object v0, p0, LX/JZQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2708524
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 2708519
    iget-object v0, p0, LX/JZQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    .line 2708520
    if-eqz p2, :cond_0

    .line 2708521
    invoke-virtual {p2}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->startFlipping()V

    .line 2708522
    :cond_0
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 2708515
    iget-object v0, p0, LX/JZQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    .line 2708516
    if-eqz p2, :cond_0

    .line 2708517
    invoke-virtual {p2}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->stopFlipping()V

    .line 2708518
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2708514
    const/4 v0, 0x3

    return v0
.end method
