.class public final LX/Jua;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/IncomingCallNotification;

.field public final synthetic b:J

.field public final synthetic c:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V
    .locals 1

    .prologue
    .line 2748979
    iput-object p1, p0, LX/Jua;->c:LX/3RG;

    iput-object p2, p0, LX/Jua;->a:Lcom/facebook/messaging/notify/IncomingCallNotification;

    iput-wide p3, p0, LX/Jua;->b:J

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748980
    iget-object v0, p0, LX/Jua;->c:LX/3RG;

    iget-object v1, p0, LX/Jua;->a:Lcom/facebook/messaging/notify/IncomingCallNotification;

    iget-wide v2, p0, LX/Jua;->b:J

    invoke-static {v0, p1, v1, v2, v3}, LX/3RG;->a$redex0(LX/3RG;Landroid/graphics/Bitmap;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V

    .line 2748981
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748982
    invoke-direct {p0, p1}, LX/Jua;->b(Landroid/graphics/Bitmap;)V

    .line 2748983
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2748984
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Jua;->b(Landroid/graphics/Bitmap;)V

    .line 2748985
    return-void
.end method
