.class public LX/Jw0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0bV;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2751328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2751329
    iput-object p1, p0, LX/Jw0;->a:LX/0ad;

    .line 2751330
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2751331
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "-------------GPS QE-------------\nEnabled: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/Jw0;->a:LX/0ad;

    sget-short v2, LX/1ST;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nFeedUnitFormatString: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Jw0;->a:LX/0ad;

    sget-char v2, LX/1ST;->b:C

    const-string v3, "%s"

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method
