.class public final LX/JgS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2723109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2723110
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2723111
    :goto_0
    return v1

    .line 2723112
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2723113
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2723114
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2723115
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2723116
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2723117
    const-string v3, "photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2723118
    const/4 v2, 0x0

    .line 2723119
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2723120
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2723121
    :goto_2
    move v0, v2

    .line 2723122
    goto :goto_1

    .line 2723123
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2723124
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2723125
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2723126
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2723127
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 2723128
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2723129
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2723130
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 2723131
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2723132
    const/4 v3, 0x0

    .line 2723133
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_b

    .line 2723134
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2723135
    :goto_4
    move v0, v3

    .line 2723136
    goto :goto_3

    .line 2723137
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2723138
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2723139
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 2723140
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2723141
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_a

    .line 2723142
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2723143
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2723144
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_9

    if-eqz v4, :cond_9

    .line 2723145
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2723146
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2723147
    :cond_a
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2723148
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2723149
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_5
.end method
