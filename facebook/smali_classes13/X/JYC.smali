.class public final LX/JYC;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JYE;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JYD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JYE",
            "<TE;>.SaleGroupsNearYouComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JYE;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JYE;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2705952
    iput-object p1, p0, LX/JYC;->b:LX/JYE;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2705953
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "feedProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JYC;->c:[Ljava/lang/String;

    .line 2705954
    iput v3, p0, LX/JYC;->d:I

    .line 2705955
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JYC;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JYC;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JYC;LX/1De;IILX/JYD;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JYE",
            "<TE;>.SaleGroupsNearYouComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2705956
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2705957
    iput-object p4, p0, LX/JYC;->a:LX/JYD;

    .line 2705958
    iget-object v0, p0, LX/JYC;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2705959
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2705960
    invoke-super {p0}, LX/1X5;->a()V

    .line 2705961
    const/4 v0, 0x0

    iput-object v0, p0, LX/JYC;->a:LX/JYD;

    .line 2705962
    iget-object v0, p0, LX/JYC;->b:LX/JYE;

    iget-object v0, v0, LX/JYE;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2705963
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JYE;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2705964
    iget-object v1, p0, LX/JYC;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JYC;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JYC;->d:I

    if-ge v1, v2, :cond_2

    .line 2705965
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2705966
    :goto_0
    iget v2, p0, LX/JYC;->d:I

    if-ge v0, v2, :cond_1

    .line 2705967
    iget-object v2, p0, LX/JYC;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2705968
    iget-object v2, p0, LX/JYC;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2705969
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2705970
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2705971
    :cond_2
    iget-object v0, p0, LX/JYC;->a:LX/JYD;

    .line 2705972
    invoke-virtual {p0}, LX/JYC;->a()V

    .line 2705973
    return-object v0
.end method
