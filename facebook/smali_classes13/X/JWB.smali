.class public final LX/JWB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

.field public final synthetic b:LX/JWD;


# direct methods
.method public constructor <init>(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V
    .locals 0

    .prologue
    .line 2702502
    iput-object p1, p0, LX/JWB;->b:LX/JWD;

    iput-object p2, p0, LX/JWB;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2702533
    iget-object v0, p0, LX/JWB;->b:LX/JWD;

    iget-object v0, v0, LX/JWD;->a:Ljava/util/Set;

    iget-object v1, p0, LX/JWB;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2702534
    iget-object v0, p0, LX/JWB;->b:LX/JWD;

    iget-object v1, p0, LX/JWB;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-static {v0, v1}, LX/JWD;->d(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    .line 2702535
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2702503
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 2702504
    iget-object v0, p0, LX/JWB;->b:LX/JWD;

    iget-object v0, v0, LX/JWD;->a:Ljava/util/Set;

    iget-object v1, p0, LX/JWB;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2702505
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2702506
    :cond_0
    iget-object v0, p0, LX/JWB;->b:LX/JWD;

    iget-object v1, p0, LX/JWB;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-static {v0, v1}, LX/JWD;->d(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    .line 2702507
    :goto_0
    return-void

    .line 2702508
    :cond_1
    iget-object v0, p0, LX/JWB;->b:LX/JWD;

    iget-object v0, v0, LX/JWD;->d:LX/189;

    iget-object v1, p0, LX/JWB;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 2702509
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2702510
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o()LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2702511
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2702512
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 2702513
    invoke-static {v2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2702514
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2702515
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2702516
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v2

    invoke-static {v2}, LX/4Xw;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;)LX/4Xw;

    move-result-object v2

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2702517
    iput-object v3, v2, LX/4Xw;->b:LX/0Px;

    .line 2702518
    move-object v2, v2

    .line 2702519
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    .line 2702520
    iput-object v3, v2, LX/4Xw;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2702521
    move-object v2, v2

    .line 2702522
    invoke-virtual {v2}, LX/4Xw;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v2

    .line 2702523
    invoke-static {v1}, LX/4Xu;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/4Xu;

    move-result-object v3

    .line 2702524
    iput-object v2, v3, LX/4Xu;->d:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 2702525
    move-object v2, v3

    .line 2702526
    iget-object v3, v0, LX/189;->i:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 2702527
    iput-wide v4, v2, LX/4Xu;->l:J

    .line 2702528
    move-object v2, v2

    .line 2702529
    invoke-virtual {v2}, LX/4Xu;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    move-result-object v2

    .line 2702530
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I_()I

    move-result v3

    invoke-static {v2, v3}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2702531
    move-object v0, v2

    .line 2702532
    iget-object v1, p0, LX/JWB;->b:LX/JWD;

    iget-object v1, v1, LX/JWD;->g:LX/2di;

    invoke-virtual {v1, v0}, LX/2di;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    goto :goto_0
.end method
