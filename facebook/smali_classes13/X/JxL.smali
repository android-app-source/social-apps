.class public final LX/JxL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# instance fields
.field public final synthetic a:LX/JxM;

.field public final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/Cdn;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/JxI;

.field public final d:Landroid/content/BroadcastReceiver;

.field public final e:Ljava/lang/Runnable;

.field public final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/JxM;LX/JxI;)V
    .locals 4

    .prologue
    .line 2752943
    iput-object p1, p0, LX/JxL;->a:LX/JxM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752944
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2752945
    iput-object p2, p0, LX/JxL;->c:LX/JxI;

    .line 2752946
    new-instance v0, LX/0Yd;

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    new-instance v2, LX/JxK;

    invoke-direct {v2, p0}, LX/JxK;-><init>(LX/JxL;)V

    invoke-direct {v0, v1, v2}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v0, p0, LX/JxL;->d:Landroid/content/BroadcastReceiver;

    .line 2752947
    new-instance v0, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$1;-><init>(LX/JxL;LX/JxM;)V

    iput-object v0, p0, LX/JxL;->e:Ljava/lang/Runnable;

    .line 2752948
    new-instance v0, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$2;

    invoke-direct {v0, p0, p1}, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$2;-><init>(LX/JxL;LX/JxM;)V

    iput-object v0, p0, LX/JxL;->f:Ljava/lang/Runnable;

    .line 2752949
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2752950
    iget-object v0, p0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2752951
    :goto_0
    return-void

    .line 2752952
    :cond_0
    iget-object v0, p0, LX/JxL;->a:LX/JxM;

    iget-object v0, v0, LX/JxM;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/JxL;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2752953
    iget-object v0, p0, LX/JxL;->a:LX/JxM;

    iget-object v0, v0, LX/JxM;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/JxL;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2752954
    iget-object v0, p0, LX/JxL;->a:LX/JxM;

    iget-object v0, v0, LX/JxM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p0}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 2752955
    iget-object v0, p0, LX/JxL;->a:LX/JxM;

    iget-object v0, v0, LX/JxM;->a:Landroid/content/Context;

    iget-object v1, p0, LX/JxL;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2752956
    iget-object v0, p0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 12

    .prologue
    .line 2752957
    iget-object v0, p0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2752958
    :goto_0
    return-void

    .line 2752959
    :cond_0
    iget-object v0, p0, LX/JxL;->a:LX/JxM;

    iget-object v0, v0, LX/JxM;->a:Landroid/content/Context;

    iget-object v1, p0, LX/JxL;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2752960
    iget-object v0, p0, LX/JxL;->c:LX/JxI;

    .line 2752961
    iget-wide v5, v0, LX/JxI;->e:J

    .line 2752962
    iget-object v3, v0, LX/JxI;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v7

    .line 2752963
    iget-object v3, v0, LX/JxI;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iget-wide v9, v0, LX/JxI;->f:J

    sub-long v9, v3, v9

    .line 2752964
    iget v11, v0, LX/JxI;->d:I

    .line 2752965
    iget-object v3, v0, LX/JxI;->c:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2752966
    new-instance v3, LX/JxH;

    invoke-direct {v3, v0}, LX/JxH;-><init>(LX/JxI;)V

    invoke-static {v4, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2752967
    new-instance v3, LX/Cdn;

    invoke-direct/range {v3 .. v11}, LX/Cdn;-><init>(Ljava/util/List;JJJI)V

    move-object v0, v3

    .line 2752968
    iget-object v1, p0, LX/JxL;->a:LX/JxM;

    iget-object v1, v1, LX/JxM;->f:LX/0bT;

    new-instance v2, LX/Cdp;

    invoke-direct {v2, v0}, LX/Cdp;-><init>(LX/Cdn;)V

    invoke-virtual {v1, v2}, LX/0bT;->a(LX/0bY;)V

    .line 2752969
    iget-object v1, p0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x1d14961a

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method public final onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 3

    .prologue
    .line 2752970
    new-instance v0, LX/Cdl;

    invoke-direct {v0, p1, p3}, LX/Cdl;-><init>(Landroid/bluetooth/BluetoothDevice;[B)V

    .line 2752971
    iget-object v1, p0, LX/JxL;->c:LX/JxI;

    .line 2752972
    iget-object v2, v1, LX/JxI;->c:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2752973
    iget-object v2, v1, LX/JxI;->c:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cdm;

    invoke-virtual {v2, p2}, LX/Cdm;->a(I)LX/Cdm;

    .line 2752974
    :goto_0
    iget v2, v1, LX/JxI;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, LX/JxI;->d:I

    .line 2752975
    iget-object v1, p0, LX/JxL;->a:LX/JxM;

    iget-object v1, v1, LX/JxM;->f:LX/0bT;

    new-instance v2, LX/Cdo;

    invoke-direct {v2, v0, p2}, LX/Cdo;-><init>(LX/Cdl;I)V

    invoke-virtual {v1, v2}, LX/0bT;->a(LX/0bY;)V

    .line 2752976
    return-void

    .line 2752977
    :cond_0
    iget-object v2, v1, LX/JxI;->c:Ljava/util/Map;

    new-instance p1, LX/Cdm;

    invoke-direct {p1, v0}, LX/Cdm;-><init>(LX/Cdl;)V

    invoke-virtual {p1, p2}, LX/Cdm;->a(I)LX/Cdm;

    move-result-object p1

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
