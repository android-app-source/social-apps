.class public LX/K27;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K2D;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/K2D;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2763094
    iput-object p1, p0, LX/K27;->a:LX/0Ot;

    .line 2763095
    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2763096
    iget-object v0, p0, LX/K27;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K2D;

    .line 2763097
    const/4 v1, 0x0

    .line 2763098
    iget-object v2, v0, LX/K2D;->c:LX/Chi;

    .line 2763099
    iget-object p0, v2, LX/Chi;->g:LX/0lF;

    move-object v2, p0

    .line 2763100
    instance-of v2, v2, LX/162;

    if-eqz v2, :cond_0

    .line 2763101
    iget-object v1, v0, LX/K2D;->c:LX/Chi;

    .line 2763102
    iget-object v2, v1, LX/Chi;->g:LX/0lF;

    move-object v1, v2

    .line 2763103
    check-cast v1, LX/162;

    .line 2763104
    :cond_0
    new-instance v2, LX/21A;

    invoke-direct {v2}, LX/21A;-><init>()V

    const-string p0, "native_article_story"

    .line 2763105
    iput-object p0, v2, LX/21A;->c:Ljava/lang/String;

    .line 2763106
    move-object v2, v2

    .line 2763107
    iput-object v1, v2, LX/21A;->a:LX/162;

    .line 2763108
    move-object v1, v2

    .line 2763109
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 2763110
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object v2

    .line 2763111
    iput-boolean p1, v2, LX/5H8;->b:Z

    .line 2763112
    move-object v2, v2

    .line 2763113
    iput-object p2, v2, LX/5H8;->a:Ljava/lang/String;

    .line 2763114
    move-object v2, v2

    .line 2763115
    iget-object p0, v0, LX/K2D;->b:LX/20i;

    invoke-virtual {p0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    .line 2763116
    iput-object p0, v2, LX/5H8;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2763117
    move-object v2, v2

    .line 2763118
    iput-object v1, v2, LX/5H8;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2763119
    move-object v1, v2

    .line 2763120
    invoke-virtual {v1}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object v1

    .line 2763121
    iget-object v2, v0, LX/K2D;->a:LX/25G;

    invoke-virtual {v2, v1}, LX/25G;->a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2763122
    return-object v0
.end method
