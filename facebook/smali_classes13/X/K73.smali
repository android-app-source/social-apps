.class public final enum LX/K73;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K73;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K73;

.field public static final enum FOLLOWING_AND_SUBSCRIBED:LX/K73;

.field public static final enum FOLLOWING_NOT_SUBSCRIBED:LX/K73;

.field public static final enum NONE:LX/K73;

.field public static final enum NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2772594
    new-instance v0, LX/K73;

    const-string v1, "FOLLOWING_AND_SUBSCRIBED"

    invoke-direct {v0, v1, v2}, LX/K73;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K73;->FOLLOWING_AND_SUBSCRIBED:LX/K73;

    .line 2772595
    new-instance v0, LX/K73;

    const-string v1, "FOLLOWING_NOT_SUBSCRIBED"

    invoke-direct {v0, v1, v3}, LX/K73;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K73;->FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    .line 2772596
    new-instance v0, LX/K73;

    const-string v1, "NOT_FOLLOWING_NOT_SUBSCRIBED"

    invoke-direct {v0, v1, v4}, LX/K73;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K73;->NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    .line 2772597
    new-instance v0, LX/K73;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, LX/K73;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K73;->NONE:LX/K73;

    .line 2772598
    const/4 v0, 0x4

    new-array v0, v0, [LX/K73;

    sget-object v1, LX/K73;->FOLLOWING_AND_SUBSCRIBED:LX/K73;

    aput-object v1, v0, v2

    sget-object v1, LX/K73;->FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    aput-object v1, v0, v3

    sget-object v1, LX/K73;->NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    aput-object v1, v0, v4

    sget-object v1, LX/K73;->NONE:LX/K73;

    aput-object v1, v0, v5

    sput-object v0, LX/K73;->$VALUES:[LX/K73;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2772599
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K73;
    .locals 1

    .prologue
    .line 2772600
    const-class v0, LX/K73;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K73;

    return-object v0
.end method

.method public static values()[LX/K73;
    .locals 1

    .prologue
    .line 2772601
    sget-object v0, LX/K73;->$VALUES:[LX/K73;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K73;

    return-object v0
.end method
