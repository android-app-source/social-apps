.class public final LX/JwJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/2cy;",
        "LX/Jwa;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JwS;


# direct methods
.method public constructor <init>(LX/JwS;)V
    .locals 0

    .prologue
    .line 2751776
    iput-object p1, p0, LX/JwJ;->a:LX/JwS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 2751777
    check-cast p1, LX/2cy;

    .line 2751778
    invoke-virtual {p1}, LX/2cy;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2751779
    new-instance v0, Ljava/lang/IllegalAccessException;

    const-string v1, "Pulsar detection disabled in settings"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751780
    :cond_0
    iget-object v0, p0, LX/JwJ;->a:LX/JwS;

    iget-object v0, v0, LX/JwS;->k:LX/1SR;

    invoke-virtual {v0}, LX/1SR;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2751781
    new-instance v0, Ljava/lang/IllegalAccessException;

    const-string v1, "Location is not supported on device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751782
    :cond_1
    iget-object v0, p0, LX/JwJ;->a:LX/JwS;

    .line 2751783
    iget-object v1, v0, LX/JwS;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2751784
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2751785
    if-nez v0, :cond_2

    .line 2751786
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No network connectivity detected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751787
    :cond_2
    iget-object v0, p0, LX/JwJ;->a:LX/JwS;

    iget-object v0, v0, LX/JwS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
