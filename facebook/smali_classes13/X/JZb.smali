.class public LX/JZb;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z

.field public b:Landroid/widget/ImageView;

.field public c:Landroid/graphics/Paint;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2708821
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2708822
    const/4 p1, 0x0

    .line 2708823
    const v0, 0x7f03162b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2708824
    invoke-virtual {p0, p1}, LX/JZb;->setOrientation(I)V

    .line 2708825
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2708826
    invoke-virtual {p0, v0}, LX/JZb;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2708827
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/JZb;->setGravity(I)V

    .line 2708828
    const v0, 0x7f0d31d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/JZb;->b:Landroid/widget/ImageView;

    .line 2708829
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/JZb;->c:Landroid/graphics/Paint;

    .line 2708830
    iget-object v0, p0, LX/JZb;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/JZb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2708831
    iget-object v0, p0, LX/JZb;->c:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2708832
    iput-boolean p1, p0, LX/JZb;->d:Z

    .line 2708833
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2708815
    iget-boolean v0, p0, LX/JZb;->a:Z

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2708816
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2708817
    iget-boolean v0, p0, LX/JZb;->d:Z

    if-nez v0, :cond_0

    .line 2708818
    invoke-virtual {p0}, LX/JZb;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    .line 2708819
    invoke-virtual {p0}, LX/JZb;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p0}, LX/JZb;->getWidth()I

    move-result v3

    invoke-virtual {p0}, LX/JZb;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/JZb;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2708820
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x770110de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2708807
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2708808
    const/4 v1, 0x1

    .line 2708809
    iput-boolean v1, p0, LX/JZb;->a:Z

    .line 2708810
    const/16 v1, 0x2d

    const v2, -0x94bb32b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x41410ec7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2708811
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2708812
    const/4 v1, 0x0

    .line 2708813
    iput-boolean v1, p0, LX/JZb;->a:Z

    .line 2708814
    const/16 v1, 0x2d

    const v2, 0x6b84b119

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
