.class public final enum LX/JpA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JpA;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JpA;

.field public static final enum FRIENDS_ARE_ADMINS:LX/JpA;

.field public static final enum FRIEND_JOINED:LX/JpA;

.field public static final enum NONE:LX/JpA;

.field public static final enum SHARED_FROM_FRIENDS:LX/JpA;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2737045
    new-instance v0, LX/JpA;

    const-string v1, "SHARED_FROM_FRIENDS"

    const-string v2, "shared_from_friends"

    invoke-direct {v0, v1, v3, v2}, LX/JpA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JpA;->SHARED_FROM_FRIENDS:LX/JpA;

    .line 2737046
    new-instance v0, LX/JpA;

    const-string v1, "FRIEND_JOINED"

    const-string v2, "friends_joined"

    invoke-direct {v0, v1, v4, v2}, LX/JpA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JpA;->FRIEND_JOINED:LX/JpA;

    .line 2737047
    new-instance v0, LX/JpA;

    const-string v1, "FRIENDS_ARE_ADMINS"

    const-string v2, "friends_are_admins"

    invoke-direct {v0, v1, v5, v2}, LX/JpA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JpA;->FRIENDS_ARE_ADMINS:LX/JpA;

    .line 2737048
    new-instance v0, LX/JpA;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v6, v2}, LX/JpA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JpA;->NONE:LX/JpA;

    .line 2737049
    const/4 v0, 0x4

    new-array v0, v0, [LX/JpA;

    sget-object v1, LX/JpA;->SHARED_FROM_FRIENDS:LX/JpA;

    aput-object v1, v0, v3

    sget-object v1, LX/JpA;->FRIEND_JOINED:LX/JpA;

    aput-object v1, v0, v4

    sget-object v1, LX/JpA;->FRIENDS_ARE_ADMINS:LX/JpA;

    aput-object v1, v0, v5

    sget-object v1, LX/JpA;->NONE:LX/JpA;

    aput-object v1, v0, v6

    sput-object v0, LX/JpA;->$VALUES:[LX/JpA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2737050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2737051
    iput-object p3, p0, LX/JpA;->mValue:Ljava/lang/String;

    .line 2737052
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/JpA;
    .locals 5

    .prologue
    .line 2737053
    invoke-static {}, LX/JpA;->values()[LX/JpA;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2737054
    iget-object v4, v0, LX/JpA;->mValue:Ljava/lang/String;

    invoke-static {p0, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2737055
    :goto_1
    return-object v0

    .line 2737056
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2737057
    :cond_1
    sget-object v0, LX/JpA;->NONE:LX/JpA;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/JpA;
    .locals 1

    .prologue
    .line 2737058
    const-class v0, LX/JpA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JpA;

    return-object v0
.end method

.method public static values()[LX/JpA;
    .locals 1

    .prologue
    .line 2737059
    sget-object v0, LX/JpA;->$VALUES:[LX/JpA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JpA;

    return-object v0
.end method
