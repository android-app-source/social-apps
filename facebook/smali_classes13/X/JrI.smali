.class public LX/JrI;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field public a:LX/FDs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Jrc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/IiS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742729
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrI;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2742730
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2742731
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742732
    iput-object v0, p0, LX/JrI;->d:LX/0Ot;

    .line 2742733
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742734
    iput-object v0, p0, LX/JrI;->e:LX/0Ot;

    .line 2742735
    return-void
.end method

.method public static a(LX/0QB;)LX/JrI;
    .locals 11

    .prologue
    .line 2742736
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742737
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742738
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742739
    if-nez v1, :cond_0

    .line 2742740
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742741
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742742
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742743
    sget-object v1, LX/JrI;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742744
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742745
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742746
    :cond_1
    if-nez v1, :cond_4

    .line 2742747
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742748
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742749
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742750
    new-instance v1, LX/JrI;

    invoke-direct {v1}, LX/JrI;-><init>()V

    .line 2742751
    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v7

    check-cast v7, LX/FDs;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v8

    check-cast v8, LX/Jrc;

    invoke-static {v0}, LX/IiS;->a(LX/0QB;)LX/IiS;

    move-result-object v9

    check-cast v9, LX/IiS;

    const/16 v10, 0xce5

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0x29c4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2742752
    iput-object v7, v1, LX/JrI;->a:LX/FDs;

    iput-object v8, v1, LX/JrI;->b:LX/Jrc;

    iput-object v9, v1, LX/JrI;->c:LX/IiS;

    iput-object v10, v1, LX/JrI;->d:LX/0Ot;

    iput-object p0, v1, LX/JrI;->e:LX/0Ot;

    .line 2742753
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742754
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742755
    if-nez v1, :cond_2

    .line 2742756
    sget-object v0, LX/JrI;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrI;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742757
    :goto_1
    if-eqz v0, :cond_3

    .line 2742758
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742759
    :goto_3
    check-cast v0, LX/JrI;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742760
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742761
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742762
    :catchall_1
    move-exception v0

    .line 2742763
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742764
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742765
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742766
    :cond_2
    :try_start_8
    sget-object v0, LX/JrI;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrI;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742767
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742768
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742769
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->h()LX/6kN;

    move-result-object v0

    .line 2742770
    iget-object v1, p0, LX/JrI;->b:LX/Jrc;

    iget-object v0, v0, LX/6kN;->threadKeys:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 2742771
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742772
    iget-object v4, p0, LX/JrI;->c:LX/IiS;

    .line 2742773
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2742774
    :goto_1
    iget-object v4, p0, LX/JrI;->a:LX/FDs;

    invoke-virtual {v4, v0}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742775
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2742776
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0

    .line 2742777
    :cond_1
    iget-object v5, v4, LX/IiS;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FDs;

    .line 2742778
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v6

    if-nez v6, :cond_3

    .line 2742779
    :cond_2
    :goto_2
    goto :goto_1

    .line 2742780
    :cond_3
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v7

    .line 2742781
    const-string v6, "montage_thread_key"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2742782
    const-string v6, "folder"

    sget-object v8, LX/6ek;->INBOX:LX/6ek;

    iget-object v8, v8, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v6, v8}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2742783
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2742784
    const-string v6, "montage_thread_key"

    invoke-virtual {v8, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2742785
    iget-object v6, v5, LX/FDs;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6dQ;

    invoke-virtual {v6}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 2742786
    const-string v9, "threads"

    invoke-virtual {v7}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v9, v8, v10, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2742787
    iget-object v6, v5, LX/FDs;->f:LX/3N0;

    iget-wide v8, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    const-wide/16 v10, 0x0

    invoke-virtual {v6, v8, v9, v10, v11}, LX/3N0;->a(JJ)V

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742788
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->h()LX/6kN;

    move-result-object v0

    .line 2742789
    iget-object v1, p0, LX/JrI;->b:LX/Jrc;

    iget-object v0, v0, LX/6kN;->threadKeys:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742790
    iget-object v1, p0, LX/JrI;->c:LX/IiS;

    .line 2742791
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2742792
    const/4 v5, 0x0

    .line 2742793
    :goto_1
    move-object v5, v5

    .line 2742794
    if-eqz v5, :cond_0

    .line 2742795
    iget-object v1, p0, LX/JrI;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jqb;

    .line 2742796
    invoke-static {v1, v5}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2742797
    :cond_0
    iget-object v1, p0, LX/JrI;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    sget-object v5, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v1, v5, v0}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742798
    iget-object v1, p0, LX/JrI;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    sget-object v5, LX/6ek;->MONTAGE:LX/6ek;

    invoke-virtual {v1, v5, v0}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742799
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2742800
    :cond_1
    return-void

    :cond_2
    iget-object v5, v1, LX/IiS;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Oe;

    invoke-virtual {v5, v0}, LX/2Oe;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2742801
    check-cast p1, LX/6kW;

    .line 2742802
    invoke-virtual {p1}, LX/6kW;->h()LX/6kN;

    move-result-object v0

    .line 2742803
    iget-object v1, p0, LX/JrI;->b:LX/Jrc;

    iget-object v0, v0, LX/6kN;->threadKeys:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    .line 2742804
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
