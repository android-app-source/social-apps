.class public LX/JcF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/app/KeyguardManager;

.field private final c:LX/JcH;

.field private final d:LX/JcI;

.field private final e:LX/JcG;

.field private final f:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2717728
    const-class v0, LX/JcF;

    sput-object v0, LX/JcF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/app/KeyguardManager;LX/JcH;LX/JcI;LX/JcG;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717730
    iput-object p1, p0, LX/JcF;->b:Landroid/app/KeyguardManager;

    .line 2717731
    iput-object p2, p0, LX/JcF;->c:LX/JcH;

    .line 2717732
    iput-object p3, p0, LX/JcF;->d:LX/JcI;

    .line 2717733
    iput-object p4, p0, LX/JcF;->e:LX/JcG;

    .line 2717734
    iput-object p5, p0, LX/JcF;->f:LX/03V;

    .line 2717735
    return-void
.end method
