.class public LX/JWy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JX2;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWy",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JX2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703760
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703761
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWy;->b:LX/0Zi;

    .line 2703762
    iput-object p1, p0, LX/JWy;->a:LX/0Ot;

    .line 2703763
    return-void
.end method

.method public static a(LX/0QB;)LX/JWy;
    .locals 4

    .prologue
    .line 2703729
    const-class v1, LX/JWy;

    monitor-enter v1

    .line 2703730
    :try_start_0
    sget-object v0, LX/JWy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703731
    sput-object v2, LX/JWy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703732
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703733
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703734
    new-instance v3, LX/JWy;

    const/16 p0, 0x2134

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWy;-><init>(LX/0Ot;)V

    .line 2703735
    move-object v0, v3

    .line 2703736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2703742
    check-cast p2, LX/JWx;

    .line 2703743
    iget-object v0, p0, LX/JWy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JX2;

    iget-object v1, p2, LX/JWx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JWx;->b:LX/1Pb;

    .line 2703744
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2703745
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    .line 2703746
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2703747
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->n()LX/0Px;

    move-result-object v7

    .line 2703748
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_1

    .line 2703749
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 2703750
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->z()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2703751
    new-instance v8, LX/JWr;

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    invoke-direct {v8, v3, v4}, LX/JWr;-><init>(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;)V

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2703752
    :cond_0
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2703753
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 2703754
    invoke-static {v0, v1, v3}, LX/JX2;->a(LX/JX2;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;)LX/25M;

    move-result-object v3

    .line 2703755
    iget-object v5, v0, LX/JX2;->a:LX/JX0;

    .line 2703756
    new-instance v6, LX/JWz;

    move-object v9, v2

    check-cast v9, LX/1Pb;

    invoke-static {v5}, LX/JX5;->a(LX/0QB;)LX/JX5;

    move-result-object v11

    check-cast v11, LX/JX5;

    move-object v7, p1

    move-object v8, v4

    move-object v10, v3

    invoke-direct/range {v6 .. v11}, LX/JWz;-><init>(Landroid/content/Context;LX/0Px;LX/1Pb;LX/25M;LX/JX5;)V

    .line 2703757
    move-object v3, v6

    .line 2703758
    iget-object v4, v0, LX/JX2;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 v4, 0x8

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2703759
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703740
    invoke-static {}, LX/1dS;->b()V

    .line 2703741
    const/4 v0, 0x0

    return-object v0
.end method
