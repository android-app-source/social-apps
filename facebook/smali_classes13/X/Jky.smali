.class public final LX/Jky;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "Ljava/lang/Void;",
        "LX/3MZ;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jkz;


# direct methods
.method public constructor <init>(LX/Jkz;)V
    .locals 0

    .prologue
    .line 2730664
    iput-object p1, p0, LX/Jky;->a:LX/Jkz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 2730665
    iget-object v0, p0, LX/Jky;->a:LX/Jkz;

    const/4 v1, 0x1

    .line 2730666
    iput-boolean v1, v0, LX/Jkz;->s:Z

    .line 2730667
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2730668
    check-cast p2, LX/3MZ;

    .line 2730669
    iget-object v0, p0, LX/Jky;->a:LX/Jkz;

    const/16 v5, 0x14

    .line 2730670
    iget-object v2, p2, LX/3MZ;->f:LX/0Px;

    move-object v4, v2

    .line 2730671
    iget-object v2, p2, LX/3MZ;->d:LX/0Px;

    move-object v3, v2

    .line 2730672
    if-eqz v4, :cond_0

    if-nez v3, :cond_1

    .line 2730673
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Jky;->a:LX/Jkz;

    const/4 v1, 0x1

    .line 2730674
    iput-boolean v1, v0, LX/Jkz;->w:Z

    .line 2730675
    return-void

    .line 2730676
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    if-gt v2, v5, :cond_3

    move-object v2, v3

    .line 2730677
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    iget v6, v0, LX/Jkz;->r:I

    if-ne v5, v6, :cond_2

    iget-object v5, v0, LX/Jkz;->p:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v0, LX/Jkz;->q:LX/0Px;

    invoke-virtual {v2, v5}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2730678
    :cond_2
    invoke-virtual {v4}, LX/0Px;->size()I

    .line 2730679
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    iput v3, v0, LX/Jkz;->r:I

    .line 2730680
    iput-object v4, v0, LX/Jkz;->p:LX/0Px;

    .line 2730681
    iput-object v2, v0, LX/Jkz;->q:LX/0Px;

    .line 2730682
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, LX/Jkz;->t:J

    goto :goto_0

    .line 2730683
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v3, v2, v5}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2730684
    iget-object v0, p0, LX/Jky;->a:LX/Jkz;

    const/4 v1, 0x0

    .line 2730685
    iput-boolean v1, v0, LX/Jkz;->s:Z

    .line 2730686
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2730687
    iget-object v0, p0, LX/Jky;->a:LX/Jkz;

    const/4 v1, 0x0

    .line 2730688
    iput-boolean v1, v0, LX/Jkz;->s:Z

    .line 2730689
    return-void
.end method
