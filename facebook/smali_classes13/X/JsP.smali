.class public LX/JsP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0tX;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2745476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745477
    iput-object p1, p0, LX/JsP;->a:LX/0tX;

    .line 2745478
    iput-object p2, p0, LX/JsP;->b:LX/0Or;

    .line 2745479
    return-void
.end method

.method public static a(LX/JsP;Ljava/lang/String;Z)LX/399;
    .locals 3

    .prologue
    .line 2745480
    new-instance v0, LX/JsJ;

    invoke-direct {v0}, LX/JsJ;-><init>()V

    move-object v1, v0

    .line 2745481
    new-instance v2, LX/4K3;

    invoke-direct {v2}, LX/4K3;-><init>()V

    iget-object v0, p0, LX/JsP;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2745482
    const-string p0, "actor_id"

    invoke-virtual {v2, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2745483
    move-object v0, v2

    .line 2745484
    const-string v2, "username"

    invoke-virtual {v0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2745485
    move-object v0, v0

    .line 2745486
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2745487
    const-string p0, "save_username"

    invoke-virtual {v0, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2745488
    move-object v0, v0

    .line 2745489
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2745490
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2745491
    return-object v0
.end method

.method public static b(LX/0QB;)LX/JsP;
    .locals 3

    .prologue
    .line 2745492
    new-instance v1, LX/JsP;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/JsP;-><init>(LX/0tX;LX/0Or;)V

    .line 2745493
    return-object v1
.end method


# virtual methods
.method public final b(Ljava/lang/String;)Lcom/facebook/messaging/service/model/EditUsernameResult;
    .locals 3

    .prologue
    .line 2745494
    iget-object v0, p0, LX/JsP;->a:LX/0tX;

    const/4 v1, 0x1

    invoke-static {p0, p1, v1}, LX/JsP;->a(LX/JsP;Ljava/lang/String;Z)LX/399;

    move-result-object v1

    sget-object v2, LX/3Fz;->a:LX/3Fz;

    invoke-virtual {v0, v1, v2}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2745495
    const v1, -0x4449dc8

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2745496
    if-nez v0, :cond_0

    .line 2745497
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2745498
    :cond_0
    new-instance v0, Lcom/facebook/messaging/service/model/EditUsernameResult;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/service/model/EditUsernameResult;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
