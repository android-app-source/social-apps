.class public final enum LX/JcD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JcD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JcD;

.field public static final enum ALPHABETIC:LX/JcD;

.field public static final enum ALPHANUMERIC:LX/JcD;

.field public static final enum AOSP_NONE:LX/JcD;

.field public static final enum BIOMETRIC_WITH_PATTERN_BACKUP:LX/JcD;

.field public static final enum BIOMETRIC_WITH_UNKNOWN_BACKUP:LX/JcD;

.field public static final enum COMPLEX:LX/JcD;

.field public static final enum DETECTION_FAILED:LX/JcD;

.field public static final enum NUMERIC:LX/JcD;

.field public static final enum PATTERN:LX/JcD;

.field public static final enum UNKNOWN_INSECURE:LX/JcD;

.field public static final enum UNKNOWN_SECURE:LX/JcD;

.field public static final enum UNSPECIFIED_PROBABLY_OEM_INSECURE:LX/JcD;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2717704
    new-instance v0, LX/JcD;

    const-string v1, "ALPHABETIC"

    invoke-direct {v0, v1, v3}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->ALPHABETIC:LX/JcD;

    .line 2717705
    new-instance v0, LX/JcD;

    const-string v1, "ALPHANUMERIC"

    invoke-direct {v0, v1, v4}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->ALPHANUMERIC:LX/JcD;

    .line 2717706
    new-instance v0, LX/JcD;

    const-string v1, "AOSP_NONE"

    invoke-direct {v0, v1, v5}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->AOSP_NONE:LX/JcD;

    .line 2717707
    new-instance v0, LX/JcD;

    const-string v1, "BIOMETRIC_WITH_UNKNOWN_BACKUP"

    invoke-direct {v0, v1, v6}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->BIOMETRIC_WITH_UNKNOWN_BACKUP:LX/JcD;

    .line 2717708
    new-instance v0, LX/JcD;

    const-string v1, "BIOMETRIC_WITH_PATTERN_BACKUP"

    invoke-direct {v0, v1, v7}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->BIOMETRIC_WITH_PATTERN_BACKUP:LX/JcD;

    .line 2717709
    new-instance v0, LX/JcD;

    const-string v1, "COMPLEX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->COMPLEX:LX/JcD;

    .line 2717710
    new-instance v0, LX/JcD;

    const-string v1, "DETECTION_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->DETECTION_FAILED:LX/JcD;

    .line 2717711
    new-instance v0, LX/JcD;

    const-string v1, "NUMERIC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->NUMERIC:LX/JcD;

    .line 2717712
    new-instance v0, LX/JcD;

    const-string v1, "PATTERN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->PATTERN:LX/JcD;

    .line 2717713
    new-instance v0, LX/JcD;

    const-string v1, "UNSPECIFIED_PROBABLY_OEM_INSECURE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->UNSPECIFIED_PROBABLY_OEM_INSECURE:LX/JcD;

    .line 2717714
    new-instance v0, LX/JcD;

    const-string v1, "UNKNOWN_SECURE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->UNKNOWN_SECURE:LX/JcD;

    .line 2717715
    new-instance v0, LX/JcD;

    const-string v1, "UNKNOWN_INSECURE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/JcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcD;->UNKNOWN_INSECURE:LX/JcD;

    .line 2717716
    const/16 v0, 0xc

    new-array v0, v0, [LX/JcD;

    sget-object v1, LX/JcD;->ALPHABETIC:LX/JcD;

    aput-object v1, v0, v3

    sget-object v1, LX/JcD;->ALPHANUMERIC:LX/JcD;

    aput-object v1, v0, v4

    sget-object v1, LX/JcD;->AOSP_NONE:LX/JcD;

    aput-object v1, v0, v5

    sget-object v1, LX/JcD;->BIOMETRIC_WITH_UNKNOWN_BACKUP:LX/JcD;

    aput-object v1, v0, v6

    sget-object v1, LX/JcD;->BIOMETRIC_WITH_PATTERN_BACKUP:LX/JcD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/JcD;->COMPLEX:LX/JcD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/JcD;->DETECTION_FAILED:LX/JcD;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/JcD;->NUMERIC:LX/JcD;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/JcD;->PATTERN:LX/JcD;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/JcD;->UNSPECIFIED_PROBABLY_OEM_INSECURE:LX/JcD;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/JcD;->UNKNOWN_SECURE:LX/JcD;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/JcD;->UNKNOWN_INSECURE:LX/JcD;

    aput-object v2, v0, v1

    sput-object v0, LX/JcD;->$VALUES:[LX/JcD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2717717
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2717718
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JcD;
    .locals 1

    .prologue
    .line 2717719
    const-class v0, LX/JcD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JcD;

    return-object v0
.end method

.method public static values()[LX/JcD;
    .locals 1

    .prologue
    .line 2717720
    sget-object v0, LX/JcD;->$VALUES:[LX/JcD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JcD;

    return-object v0
.end method


# virtual methods
.method public final isInsecure()Z
    .locals 2

    .prologue
    .line 2717721
    sget-object v0, LX/JcC;->a:[I

    invoke-virtual {p0}, LX/JcD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2717722
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2717723
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final isSecure()Z
    .locals 1

    .prologue
    .line 2717724
    invoke-virtual {p0}, LX/JcD;->isInsecure()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
