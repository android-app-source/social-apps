.class public final LX/JpX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/Jpb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V
    .locals 0

    .prologue
    .line 2737618
    iput-object p1, p0, LX/JpX;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2737619
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2737620
    if-nez p1, :cond_0

    .line 2737621
    sget-object v0, LX/Jpb;->a:LX/Jpb;

    .line 2737622
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/JpX;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a:LX/JqC;

    const/4 v3, 0x0

    .line 2737623
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableList()Ljava/util/ArrayList;

    move-result-object v4

    .line 2737624
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2737625
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2737626
    iget-object v1, v0, LX/JqC;->c:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v7

    .line 2737627
    const/4 v2, 0x0

    .line 2737628
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 2737629
    iget-object v1, v0, LX/JqC;->b:LX/0ad;

    sget-short v9, LX/JqA;->a:S

    invoke-interface {v1, v9, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2737630
    iget-object v1, v0, LX/JqC;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Kk;

    invoke-virtual {v1}, LX/3Kk;->c()LX/0Px;

    move-result-object v9

    .line 2737631
    if-eqz v9, :cond_1

    .line 2737632
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    :goto_1
    if-ge v2, v10, :cond_1

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2737633
    iget-object v0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2737634
    invoke-virtual {v8, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2737635
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2737636
    :cond_1
    move-object v8, v8

    .line 2737637
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v2, v3

    :goto_2
    if-ge v2, v9, :cond_5

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 2737638
    const-string v10, "id"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2737639
    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 2737640
    invoke-static {v10}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    const-string p0, "0"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2737641
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d()LX/Jpx;

    move-result-object v10

    const-string p0, "name"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2737642
    iput-object p0, v10, LX/Jpx;->b:Ljava/lang/String;

    .line 2737643
    move-object v10, v10

    .line 2737644
    const-string p0, "phone_number"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2737645
    iput-object p0, v10, LX/Jpx;->a:Ljava/lang/String;

    .line 2737646
    move-object v10, v10

    .line 2737647
    invoke-virtual {v10}, LX/Jpx;->b()Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    move-result-object v10

    move-object v1, v10

    .line 2737648
    iput-boolean v3, v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->f:Z

    .line 2737649
    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2737650
    :cond_3
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2737651
    :cond_4
    new-instance v10, LX/Jpz;

    invoke-direct {v10}, LX/Jpz;-><init>()V

    move-object v10, v10

    .line 2737652
    const-string p0, "id"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2737653
    iput-object p0, v10, LX/Jpz;->a:Ljava/lang/String;

    .line 2737654
    move-object v10, v10

    .line 2737655
    const-string p0, "name"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2737656
    iput-object p0, v10, LX/Jpz;->b:Ljava/lang/String;

    .line 2737657
    move-object v10, v10

    .line 2737658
    const-string p0, "profile_pic"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 2737659
    iput-object p0, v10, LX/Jpz;->c:Landroid/net/Uri;

    .line 2737660
    move-object v10, v10

    .line 2737661
    const-string p0, "phone_number"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2737662
    iput-object p0, v10, LX/Jpz;->d:Ljava/lang/String;

    .line 2737663
    move-object v10, v10

    .line 2737664
    new-instance p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    invoke-direct {p0, v10}, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;-><init>(LX/Jpz;)V

    move-object v10, p0

    .line 2737665
    move-object v1, v10

    .line 2737666
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, LX/3OP;->a(Z)V

    .line 2737667
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2737668
    :cond_5
    new-instance v1, LX/Jpb;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Jpb;-><init>(LX/0Px;LX/0Px;)V

    move-object v0, v1

    .line 2737669
    goto/16 :goto_0
.end method
