.class public LX/JuP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JuO;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2748710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2748711
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2748712
    iput-object v0, p0, LX/JuP;->a:LX/0Ot;

    .line 2748713
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2748714
    iput-object v0, p0, LX/JuP;->b:LX/0Ot;

    .line 2748715
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/upload/ContactsUploadState;)V
    .locals 9

    .prologue
    .line 2748716
    iget v0, p1, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    move v0, v0

    .line 2748717
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JuP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x121

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2748718
    :cond_0
    :goto_0
    return-void

    .line 2748719
    :cond_1
    iget-object v0, p0, LX/JuP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JuO;

    .line 2748720
    iget v1, p1, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    move v1, v1

    .line 2748721
    const/4 v8, 0x1

    const/4 p1, 0x0

    .line 2748722
    iget-object v2, v0, LX/JuO;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3RG;

    new-instance v4, Lcom/facebook/orca/notify/ContactsUploadNotification;

    iget-object v3, v0, LX/JuO;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0f0188

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, p1

    invoke-virtual {v3, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, v0, LX/JuO;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v6, 0x7f0f0189

    invoke-virtual {v3, v6, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    iget-object v3, v0, LX/JuO;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v7, 0x7f0f0188

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v8, p1

    invoke-virtual {v3, v7, v1, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v6, v3}, Lcom/facebook/orca/notify/ContactsUploadNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 2748723
    iget-object v3, v2, LX/3RG;->e:LX/2zj;

    invoke-interface {v3}, LX/2zj;->g()I

    move-result v3

    .line 2748724
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    sget-object v7, LX/3RH;->i:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2748725
    const-string v6, "from_notification"

    invoke-virtual {v5, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2748726
    iget-object v6, v2, LX/3RG;->b:Landroid/content/Context;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v5, v8}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 2748727
    new-instance v6, LX/2HB;

    iget-object v7, v2, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v6, v7}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v7, v4, Lcom/facebook/orca/notify/ContactsUploadNotification;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    iget-object v7, v4, Lcom/facebook/orca/notify/ContactsUploadNotification;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    iget-object v7, v4, Lcom/facebook/orca/notify/ContactsUploadNotification;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    new-instance v7, LX/3pe;

    invoke-direct {v7}, LX/3pe;-><init>()V

    iget-object v8, v4, Lcom/facebook/orca/notify/ContactsUploadNotification;->b:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v3

    .line 2748728
    iput-object v5, v3, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748729
    move-object v3, v3

    .line 2748730
    invoke-virtual {v3, p1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v3

    .line 2748731
    iget-object v5, v2, LX/3RG;->f:LX/3RQ;

    new-instance v6, LX/Dhq;

    invoke-direct {v6}, LX/Dhq;-><init>()V

    invoke-virtual {v5, v3, v6, p0, p0}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2748732
    iget-object v5, v2, LX/3RG;->m:LX/2bC;

    const-string v6, "CONTACTS_UPLOAD"

    invoke-virtual {v5, p0, v6, p0, p0}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2748733
    iget-object v5, v2, LX/3RG;->d:LX/3RK;

    const/16 v6, 0x2714

    invoke-virtual {v3}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v5, v6, v3}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 2748734
    iput-boolean p1, v4, Lcom/facebook/orca/notify/ContactsUploadNotification;->d:Z

    .line 2748735
    invoke-virtual {v4}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2748736
    goto/16 :goto_0
.end method
