.class public LX/JdH;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2719335
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2719336
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0W3;LX/0Or;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 6
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAMessengerOnlyUser;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsAccountSwitchingAvailable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2719310
    sget-object v0, LX/2Vv;->b:LX/0Tn;

    invoke-interface {p0, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2719311
    if-eqz v0, :cond_0

    .line 2719312
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2719313
    :goto_0
    return-object v0

    .line 2719314
    :cond_0
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2719315
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2719316
    :cond_1
    const/16 v0, 0xfa

    invoke-virtual {p1, v0, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2719317
    if-eqz v0, :cond_2

    .line 2719318
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2719319
    :cond_2
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2719320
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2719321
    :cond_3
    const/16 v0, 0xf9

    invoke-virtual {p1, v0, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2719322
    const/16 v1, 0x110

    invoke-virtual {p2, v1}, LX/0W3;->a(I)LX/0W4;

    move-result-object v1

    .line 2719323
    sget-wide v2, LX/0X5;->kC:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->c(JZ)V

    .line 2719324
    if-eqz v0, :cond_4

    .line 2719325
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2719326
    :cond_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0W3;LX/0Uh;)Ljava/lang/Boolean;
    .locals 5
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsUnseenCountFetchingForAccountSwitchingEnabled;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2719328
    sget-object v0, LX/2Vv;->c:LX/0Tn;

    invoke-interface {p0, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2719329
    if-eqz v0, :cond_0

    .line 2719330
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2719331
    :goto_0
    return-object v0

    .line 2719332
    :cond_0
    const/16 v0, 0xfd

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2719333
    const/16 v1, 0x110

    invoke-virtual {p1, v1}, LX/0W3;->a(I)LX/0W4;

    move-result-object v1

    .line 2719334
    sget-wide v2, LX/0X5;->kF:J

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2719327
    return-void
.end method
