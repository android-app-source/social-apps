.class public LX/K7D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tarot/data/TarotCardData;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/tarot/data/VideoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;Ljava/lang/String;FF)V
    .locals 13

    .prologue
    .line 2772840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772841
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/K7D;->b:Ljava/util/List;

    .line 2772842
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/K7D;->e:Ljava/util/ArrayList;

    .line 2772843
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/K7D;->f:Ljava/util/ArrayList;

    .line 2772844
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/K7D;->a:Ljava/lang/String;

    .line 2772845
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->j()Ljava/lang/String;

    move-result-object v1

    :goto_0
    iput-object v1, p0, LX/K7D;->c:Ljava/lang/String;

    .line 2772846
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;->a()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, LX/K7D;->d:Ljava/lang/String;

    .line 2772847
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->k()Ljava/lang/String;

    move-result-object v9

    .line 2772848
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->j()Ljava/lang/String;

    move-result-object v10

    .line 2772849
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    move-result-object v11

    .line 2772850
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;

    move-result-object v12

    .line 2772851
    if-eqz v11, :cond_3

    const/4 v1, 0x1

    move v8, v1

    .line 2772852
    :goto_2
    if-eqz v12, :cond_4

    const/4 v1, 0x1

    move v7, v1

    .line 2772853
    :goto_3
    new-instance v1, Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    const v4, -0x11b13572    # -1.5999696E28f

    const/16 v5, 0x7d5

    const-string v6, "chromeless:content:fragment:tag"

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/facebook/tarot/data/FeedbackData;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;)V

    .line 2772854
    if-eqz v8, :cond_5

    .line 2772855
    new-instance v2, LX/K7H;

    iget-object v3, p0, LX/K7D;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/K7H;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/K7H;->a(Ljava/lang/String;)LX/K7H;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/K7H;->b(Ljava/lang/String;)LX/K7H;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/K7H;->c(Ljava/lang/String;)LX/K7H;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/K7H;->a(Lcom/facebook/tarot/data/FeedbackData;)LX/K7H;

    move-result-object v1

    iget-object v2, p0, LX/K7D;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/K7H;->d(Ljava/lang/String;)LX/K7H;

    move-result-object v1

    iget-object v2, p0, LX/K7D;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/K7H;->e(Ljava/lang/String;)LX/K7H;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v1, v0}, LX/K7H;->a(F)LX/K7H;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, LX/K7H;->b(F)LX/K7H;

    move-result-object v1

    invoke-virtual {v1}, LX/K7H;->a()Lcom/facebook/tarot/data/TarotCardImageData;

    move-result-object v1

    .line 2772856
    iget-object v2, p0, LX/K7D;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2772857
    iget-object v1, p0, LX/K7D;->e:Ljava/util/ArrayList;

    invoke-virtual {v11}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2772858
    :cond_0
    :goto_4
    return-void

    .line 2772859
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2772860
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 2772861
    :cond_3
    const/4 v1, 0x0

    move v8, v1

    goto :goto_2

    .line 2772862
    :cond_4
    const/4 v1, 0x0

    move v7, v1

    goto :goto_3

    .line 2772863
    :cond_5
    if-eqz v7, :cond_0

    .line 2772864
    new-instance v2, LX/K7J;

    iget-object v3, p0, LX/K7D;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/K7J;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, LX/K7J;->a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;)LX/K7J;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/K7J;->a(Ljava/lang/String;)LX/K7J;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/K7J;->b(Ljava/lang/String;)LX/K7J;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/K7J;->a(Lcom/facebook/tarot/data/FeedbackData;)LX/K7J;

    move-result-object v1

    iget-object v2, p0, LX/K7D;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/K7J;->c(Ljava/lang/String;)LX/K7J;

    move-result-object v1

    iget-object v2, p0, LX/K7D;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/K7J;->d(Ljava/lang/String;)LX/K7J;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v1, v0}, LX/K7J;->a(F)LX/K7J;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, LX/K7J;->b(F)LX/K7J;

    move-result-object v1

    invoke-virtual {v1}, LX/K7J;->a()Lcom/facebook/tarot/data/TarotCardVideoData;

    move-result-object v1

    .line 2772865
    iget-object v2, p0, LX/K7D;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2772866
    iget-object v1, p0, LX/K7D;->f:Ljava/util/ArrayList;

    new-instance v2, Lcom/facebook/tarot/data/VideoData;

    invoke-direct {v2, v12}, Lcom/facebook/tarot/data/VideoData;-><init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2772867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772868
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K7D;->b:Ljava/util/List;

    .line 2772869
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K7D;->e:Ljava/util/ArrayList;

    .line 2772870
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K7D;->f:Ljava/util/ArrayList;

    .line 2772871
    iput-object p1, p0, LX/K7D;->a:Ljava/lang/String;

    .line 2772872
    iput-object v1, p0, LX/K7D;->c:Ljava/lang/String;

    .line 2772873
    iput-object v1, p0, LX/K7D;->d:Ljava/lang/String;

    .line 2772874
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/tarot/data/BaseTarotCardData;)V
    .locals 1

    .prologue
    .line 2772875
    iget-object v0, p0, LX/K7D;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2772876
    return-void
.end method
