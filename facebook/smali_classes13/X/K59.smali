.class public LX/K59;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:F

.field public final d:F

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Landroid/graphics/Paint$Align;

.field public final i:LX/K58;

.field public final j:Z

.field private final k:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FFIIILandroid/graphics/Paint$Align;LX/K58;Z)V
    .locals 3

    .prologue
    const/16 v1, 0x7fe

    .line 2769926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769927
    iput-object p1, p0, LX/K59;->a:Ljava/lang/String;

    .line 2769928
    iput-object p2, p0, LX/K59;->b:Ljava/lang/String;

    .line 2769929
    iput p3, p0, LX/K59;->c:F

    .line 2769930
    iput p4, p0, LX/K59;->d:F

    .line 2769931
    iput p5, p0, LX/K59;->e:I

    .line 2769932
    if-nez p6, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, LX/K59;->f:I

    .line 2769933
    if-nez p7, :cond_1

    :goto_1
    iput v1, p0, LX/K59;->g:I

    .line 2769934
    iput-object p8, p0, LX/K59;->h:Landroid/graphics/Paint$Align;

    .line 2769935
    iput-object p9, p0, LX/K59;->i:LX/K58;

    .line 2769936
    iput-boolean p10, p0, LX/K59;->j:Z

    .line 2769937
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p8, v0, v1

    const/16 v1, 0x8

    aput-object p9, v0, v1

    const/16 v1, 0x9

    invoke-static {p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, LX/K59;->k:I

    .line 2769938
    return-void

    :cond_0
    move v0, p6

    .line 2769939
    goto :goto_0

    :cond_1
    move v1, p7

    .line 2769940
    goto :goto_1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2769941
    if-ne p0, p1, :cond_1

    .line 2769942
    :cond_0
    :goto_0
    return v0

    .line 2769943
    :cond_1
    instance-of v2, p1, LX/K59;

    if-nez v2, :cond_2

    move v0, v1

    .line 2769944
    goto :goto_0

    .line 2769945
    :cond_2
    check-cast p1, LX/K59;

    .line 2769946
    iget v2, p0, LX/K59;->k:I

    iget v3, p1, LX/K59;->k:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/K59;->a:Ljava/lang/String;

    iget-object v3, p1, LX/K59;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/K59;->b:Ljava/lang/String;

    iget-object v3, p1, LX/K59;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, LX/K59;->c:F

    iget v3, p1, LX/K59;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, LX/K59;->d:F

    iget v3, p1, LX/K59;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, LX/K59;->e:I

    iget v3, p1, LX/K59;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, LX/K59;->f:I

    iget v3, p1, LX/K59;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, LX/K59;->g:I

    iget v3, p1, LX/K59;->g:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/K59;->h:Landroid/graphics/Paint$Align;

    iget-object v3, p1, LX/K59;->h:Landroid/graphics/Paint$Align;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/K59;->i:LX/K58;

    iget-object v3, p1, LX/K59;->i:LX/K58;

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/K59;->j:Z

    iget-boolean v3, p1, LX/K59;->j:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2769947
    iget v0, p0, LX/K59;->k:I

    return v0
.end method
