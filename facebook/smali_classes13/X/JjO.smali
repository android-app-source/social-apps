.class public LX/JjO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/DdT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2727624
    new-instance v0, LX/JjM;

    invoke-direct {v0}, LX/JjM;-><init>()V

    sput-object v0, LX/JjO;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/DdT;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/DdT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2727625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727626
    iput-object p1, p0, LX/JjO;->b:LX/0Or;

    .line 2727627
    iput-object p2, p0, LX/JjO;->c:LX/DdT;

    .line 2727628
    return-void
.end method

.method public static b(LX/0QB;)LX/JjO;
    .locals 3

    .prologue
    .line 2727629
    new-instance v1, LX/JjO;

    const/16 v0, 0x12cd

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/DdT;->b(LX/0QB;)LX/DdT;

    move-result-object v0

    check-cast v0, LX/DdT;

    invoke-direct {v1, v2, v0}, LX/JjO;-><init>(LX/0Or;LX/DdT;)V

    .line 2727630
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0P1;)Lcom/facebook/messaging/events/banner/EventReminderMembers;
    .locals 9
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;)",
            "Lcom/facebook/messaging/events/banner/EventReminderMembers;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2727631
    if-nez p2, :cond_0

    .line 2727632
    const/4 v0, 0x0

    .line 2727633
    :goto_0
    return-object v0

    .line 2727634
    :cond_0
    iget-object v0, p0, LX/JjO;->c:LX/DdT;

    invoke-virtual {v0, p1}, LX/DdT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v3

    .line 2727635
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2727636
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2727637
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2727638
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v7, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2727639
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2727640
    invoke-virtual {p2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 2727641
    :goto_2
    sget-object v8, LX/JjN;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->ordinal()I

    move-result v1

    aget v1, v8, v1

    packed-switch v1, :pswitch_data_0

    .line 2727642
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2727643
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2727644
    :cond_1
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2727645
    invoke-virtual {p2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    goto :goto_2

    .line 2727646
    :pswitch_0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2727647
    :pswitch_1
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2727648
    :cond_2
    sget-object v0, LX/JjO;->a:Ljava/util/Comparator;

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2727649
    sget-object v0, LX/JjO;->a:Ljava/util/Comparator;

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2727650
    sget-object v0, LX/JjO;->a:Ljava/util/Comparator;

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2727651
    iget-object v0, p0, LX/JjO;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 2727652
    if-nez v0, :cond_3

    .line 2727653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 2727654
    :cond_3
    new-instance v1, Lcom/facebook/messaging/events/banner/EventReminderMembers;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/facebook/messaging/events/banner/EventReminderMembers;-><init>(Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;LX/0Px;LX/0Px;LX/0Px;)V

    move-object v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
