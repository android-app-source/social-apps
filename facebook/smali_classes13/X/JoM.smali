.class public final enum LX/JoM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JoM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JoM;

.field public static final enum FAILED:LX/JoM;

.field public static final enum NONE:LX/JoM;

.field public static final enum PENDING:LX/JoM;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2735361
    new-instance v0, LX/JoM;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/JoM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JoM;->NONE:LX/JoM;

    .line 2735362
    new-instance v0, LX/JoM;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, LX/JoM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JoM;->PENDING:LX/JoM;

    .line 2735363
    new-instance v0, LX/JoM;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/JoM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JoM;->FAILED:LX/JoM;

    .line 2735364
    const/4 v0, 0x3

    new-array v0, v0, [LX/JoM;

    sget-object v1, LX/JoM;->NONE:LX/JoM;

    aput-object v1, v0, v2

    sget-object v1, LX/JoM;->PENDING:LX/JoM;

    aput-object v1, v0, v3

    sget-object v1, LX/JoM;->FAILED:LX/JoM;

    aput-object v1, v0, v4

    sput-object v0, LX/JoM;->$VALUES:[LX/JoM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2735367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JoM;
    .locals 1

    .prologue
    .line 2735366
    const-class v0, LX/JoM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JoM;

    return-object v0
.end method

.method public static values()[LX/JoM;
    .locals 1

    .prologue
    .line 2735365
    sget-object v0, LX/JoM;->$VALUES:[LX/JoM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JoM;

    return-object v0
.end method
