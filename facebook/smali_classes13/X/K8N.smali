.class public final LX/K8N;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2773823
    const-class v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    const v0, -0x7fd8f7bf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x3

    const-string v5, "FBTarotDigestQuery"

    const-string v6, "9cf45bb52f3210d02503fe91967e5d26"

    const-string v7, "nodes"

    const-string v8, "10155256064831729"

    const/4 v9, 0x0

    .line 2773824
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2773825
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2773826
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2773827
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2773828
    sparse-switch v0, :sswitch_data_0

    .line 2773829
    :goto_0
    return-object p1

    .line 2773830
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2773831
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2773832
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2773833
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2773834
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2773835
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2773836
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2773837
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2773838
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5c6c5af0 -> :sswitch_2
        -0x4aedfce3 -> :sswitch_3
        -0x33f8633e -> :sswitch_5
        -0x38aa96a -> :sswitch_7
        0x196b8 -> :sswitch_0
        0x2ef6341 -> :sswitch_6
        0x683094a -> :sswitch_1
        0x7191d8b1 -> :sswitch_4
        0x78668257 -> :sswitch_8
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 2773839
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQuery$FBTarotDigestQueryString$1;

    const-class v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQuery$FBTarotDigestQueryString$1;-><init>(LX/K8N;Ljava/lang/Class;)V

    return-object v0
.end method
