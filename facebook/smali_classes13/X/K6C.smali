.class public final LX/K6C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K5k;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardEndCard;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V
    .locals 0

    .prologue
    .line 2771192
    iput-object p1, p0, LX/K6C;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;B)V
    .locals 0

    .prologue
    .line 2771193
    invoke-direct {p0, p1}, LX/K6C;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    return-void
.end method


# virtual methods
.method public final setBackgroundImage(Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 2771194
    if-eqz p1, :cond_0

    .line 2771195
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/BJ2;

    const/16 v2, 0x14

    const/4 v3, 0x4

    const/high16 v4, -0x61000000

    invoke-direct {v1, v2, v3, v4}, LX/BJ2;-><init>(III)V

    .line 2771196
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 2771197
    move-object v0, v0

    .line 2771198
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2771199
    iget-object v1, p0, LX/K6C;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, v1, Lcom/facebook/tarot/cards/TarotCardEndCard;->d:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/tarot/cards/TarotCardEndCard;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, LX/K6C;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, v1, Lcom/facebook/tarot/cards/TarotCardEndCard;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2771200
    iget-object v1, p0, LX/K6C;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, v1, Lcom/facebook/tarot/cards/TarotCardEndCard;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2771201
    :cond_0
    return-void
.end method
