.class public LX/Jqu;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:LX/2N4;

.field private final b:LX/FDs;

.field private final c:LX/Jqb;

.field private final d:LX/Jrc;

.field private final e:LX/FO4;

.field private final f:LX/1sj;

.field private final g:LX/3QL;

.field private final h:LX/FCR;

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740783
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqu;->j:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/2N4;LX/FDs;LX/Jqb;LX/Jrc;LX/FO4;LX/1sj;LX/3QL;LX/FCR;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2740771
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2740772
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740773
    iput-object v0, p0, LX/Jqu;->i:LX/0Ot;

    .line 2740774
    iput-object p1, p0, LX/Jqu;->a:LX/2N4;

    .line 2740775
    iput-object p2, p0, LX/Jqu;->b:LX/FDs;

    .line 2740776
    iput-object p3, p0, LX/Jqu;->c:LX/Jqb;

    .line 2740777
    iput-object p4, p0, LX/Jqu;->d:LX/Jrc;

    .line 2740778
    iput-object p5, p0, LX/Jqu;->e:LX/FO4;

    .line 2740779
    iput-object p6, p0, LX/Jqu;->f:LX/1sj;

    .line 2740780
    iput-object p7, p0, LX/Jqu;->g:LX/3QL;

    .line 2740781
    iput-object p8, p0, LX/Jqu;->h:LX/FCR;

    .line 2740782
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqu;
    .locals 7

    .prologue
    .line 2740744
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740745
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740746
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740747
    if-nez v1, :cond_0

    .line 2740748
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740749
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740750
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740751
    sget-object v1, LX/Jqu;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740752
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740753
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740754
    :cond_1
    if-nez v1, :cond_4

    .line 2740755
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740756
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740757
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqu;->b(LX/0QB;)LX/Jqu;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2740758
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740759
    if-nez v1, :cond_2

    .line 2740760
    sget-object v0, LX/Jqu;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqu;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740761
    :goto_1
    if-eqz v0, :cond_3

    .line 2740762
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740763
    :goto_3
    check-cast v0, LX/Jqu;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740764
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740765
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740766
    :catchall_1
    move-exception v0

    .line 2740767
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740768
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740769
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740770
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqu;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqu;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Jqu;
    .locals 9

    .prologue
    .line 2740740
    new-instance v0, LX/Jqu;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v1

    check-cast v1, LX/2N4;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v2

    check-cast v2, LX/FDs;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v3

    check-cast v3, LX/Jqb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v4

    check-cast v4, LX/Jrc;

    invoke-static {p0}, LX/FO4;->a(LX/0QB;)LX/FO4;

    move-result-object v5

    check-cast v5, LX/FO4;

    invoke-static {p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v6

    check-cast v6, LX/1sj;

    invoke-static {p0}, LX/3QL;->b(LX/0QB;)LX/3QL;

    move-result-object v7

    check-cast v7, LX/3QL;

    invoke-static {p0}, LX/FCR;->a(LX/0QB;)LX/FCR;

    move-result-object v8

    check-cast v8, LX/FCR;

    invoke-direct/range {v0 .. v8}, LX/Jqu;-><init>(LX/2N4;LX/FDs;LX/Jqb;LX/Jrc;LX/FO4;LX/1sj;LX/3QL;LX/FCR;)V

    .line 2740741
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2740742
    iput-object v1, v0, LX/Jqu;->i:LX/0Ot;

    .line 2740743
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740738
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740739
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740703
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->v()LX/6jm;

    move-result-object v4

    .line 2740704
    iget-object v0, p0, LX/Jqu;->d:LX/Jrc;

    iget-object v1, v4, LX/6jm;->threadKey:LX/6l9;

    invoke-virtual {v0, v1}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2740705
    iget-object v0, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v2, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v2, :cond_1

    .line 2740706
    iget-wide v2, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    .line 2740707
    :goto_0
    iget-object v0, p2, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2740708
    invoke-static {v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v8

    .line 2740709
    invoke-static {v8}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 2740710
    const-string v3, "op"

    const-string v5, "handle_delivery_receipt"

    invoke-interface {v0, v3, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740711
    const-string v3, "other_fbid"

    invoke-interface {v0, v3, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740712
    const-string v3, "thread_key"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v3, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740713
    const-string v3, "user_id"

    iget-wide v6, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v3, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740714
    iget-object v3, p0, LX/Jqu;->f:LX/1sj;

    sget-object v5, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v3, v8, v5, v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2740715
    new-instance v0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;

    const/4 v3, 0x0

    iget-object v4, v4, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-wide v6, p2, LX/7GJ;->b:J

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Ljava/lang/String;JJ)V

    .line 2740716
    iget-object v2, p0, LX/Jqu;->b:LX/FDs;

    invoke-virtual {v2, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/DeliveredReceiptParams;)V

    .line 2740717
    iget-object v0, p0, LX/Jqu;->a:LX/2N4;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2740718
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740719
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2740720
    if-eqz v1, :cond_0

    .line 2740721
    const-string v3, "threadSummary"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2740722
    const-string v1, "fetchTimeMs"

    iget-wide v4, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2740723
    const-string v0, "fbTraceNode"

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2740724
    :cond_0
    return-object v2

    .line 2740725
    :cond_1
    iget-object v0, v4, LX/6jm;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740728
    const-string v0, "threadSummary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740729
    if-nez v0, :cond_0

    .line 2740730
    :goto_0
    return-void

    .line 2740731
    :cond_0
    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v1}, LX/6kW;->v()LX/6jm;

    move-result-object v1

    .line 2740732
    iget-object v1, v1, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2740733
    iget-object v1, p0, LX/Jqu;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const-string v4, "fetchTimeMs"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/2Oe;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740734
    const-string v1, "fbTraceNode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbtrace/FbTraceNode;

    .line 2740735
    iget-object v4, p0, LX/Jqu;->c:LX/Jqb;

    .line 2740736
    iget-object v5, v4, LX/Jqb;->e:Ljava/util/Map;

    iget-object p0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v2, v3, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;JLcom/facebook/fbtrace/FbTraceNode;)Landroid/os/Bundle;

    move-result-object p1

    invoke-interface {v5, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740737
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740726
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740727
    return-object v0
.end method
