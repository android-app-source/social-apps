.class public final LX/JdD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

.field public final synthetic b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 0

    .prologue
    .line 2718990
    iput-object p1, p0, LX/JdD;->b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iput-object p2, p0, LX/JdD;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12

    .prologue
    .line 2718991
    iget-object v0, p0, LX/JdD;->b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->j:LX/2UX;

    const-string v1, "mswitchaccounts_removal"

    invoke-virtual {v0, v1}, LX/2UX;->a(Ljava/lang/String;)V

    .line 2718992
    iget-object v0, p0, LX/JdD;->b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    iget-object v1, p0, LX/JdD;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2Os;->b(Ljava/lang/String;)V

    .line 2718993
    iget-object v0, p0, LX/JdD;->b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->k:LX/2Vu;

    iget-object v1, p0, LX/JdD;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2Vu;->a(Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2718994
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2718995
    const-string v0, "account_id"

    iget-object v1, p0, LX/JdD;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718996
    iget-object v0, p0, LX/JdD;->b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->l:LX/0aG;

    const-string v1, "expire_dbl_nonce"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->x:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x60760583

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2718997
    :cond_0
    iget-object v0, p0, LX/JdD;->b:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v1, p0, LX/JdD;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2718998
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->s:LX/2Or;

    invoke-virtual {v2}, LX/2Or;->q()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2718999
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2719000
    return-void

    .line 2719001
    :cond_2
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->q:LX/3Fd;

    iget-object v3, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2719002
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    move v6, v7

    .line 2719003
    :goto_1
    move v2, v6

    .line 2719004
    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->p:LX/2Oq;

    invoke-virtual {v2}, LX/2Oq;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    move v3, v4

    .line 2719005
    :goto_2
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v2}, LX/2Os;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 2719006
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v6, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->q:LX/3Fd;

    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2719007
    iget-object v7, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v7

    .line 2719008
    invoke-virtual {v6, v2}, LX/3Fd;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2719009
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v6, LX/3Kr;->c:LX/0Tn;

    invoke-interface {v2, v6, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    sget-object v6, LX/3Kr;->j:LX/0Tn;

    invoke-interface {v2, v6, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2719010
    :cond_3
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->q:LX/3Fd;

    .line 2719011
    iget-object v5, v2, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/3Kr;->ab:LX/0Tn;

    invoke-interface {v5, v6}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2719012
    :cond_4
    if-eqz v3, :cond_1

    .line 2719013
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->A:LX/0Yb;

    if-nez v2, :cond_5

    .line 2719014
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->u:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    sget-object v3, LX/0aY;->C:Ljava/lang/String;

    new-instance v5, LX/JdG;

    invoke-direct {v5, v0}, LX/JdG;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    invoke-interface {v2, v3, v5}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->A:LX/0Yb;

    .line 2719015
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->A:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 2719016
    :cond_5
    iput-boolean v4, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->z:Z

    .line 2719017
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->r:LX/2uq;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/FMK;->SWITCH_ACCOUNTS:LX/FMK;

    .line 2719018
    iget-object v5, v2, LX/2uq;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Or;

    .line 2719019
    iget-object v6, v5, LX/2Or;->a:LX/0Uh;

    const/16 v7, 0x638

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v6, v6

    .line 2719020
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x17

    if-lt v5, v7, :cond_10

    .line 2719021
    new-instance v5, Landroid/content/Intent;

    const-string v7, "android.provider.Telephony.ACTION_CHANGE_DEFAULT"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2719022
    const-string v7, "android.intent.category.DEFAULT"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2719023
    :goto_3
    move-object v7, v5

    .line 2719024
    if-nez v6, :cond_f

    .line 2719025
    sget-object v5, LX/2uq;->r:LX/03R;

    sget-object v8, LX/03R;->UNSET:LX/03R;

    if-ne v5, v8, :cond_6

    .line 2719026
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 2719027
    invoke-virtual {v7, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    if-nez v5, :cond_c

    sget-object v5, LX/03R;->NO:LX/03R;

    :goto_4
    sput-object v5, LX/2uq;->r:LX/03R;

    .line 2719028
    :cond_6
    sget-object v5, LX/2uq;->r:LX/03R;

    sget-object v8, LX/03R;->NO:LX/03R;

    if-ne v5, v8, :cond_f

    .line 2719029
    const/4 v5, 0x1

    .line 2719030
    :goto_5
    if-eqz v5, :cond_d

    .line 2719031
    iget-object v5, v2, LX/2uq;->o:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;

    invoke-direct {v6, v2, v3, v4}, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;-><init>(LX/2uq;Landroid/content/Context;LX/FMK;)V

    const v7, 0x19034ada

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2719032
    :goto_6
    goto/16 :goto_0

    :cond_7
    move v3, v5

    .line 2719033
    goto/16 :goto_2

    .line 2719034
    :cond_8
    iget-object v6, v2, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/3Kr;->ab:LX/0Tn;

    const-string v10, ""

    invoke-interface {v6, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v9, " "

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 2719035
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 2719036
    const-string v6, ""

    .line 2719037
    array-length p0, v10

    move v9, v7

    :goto_7
    if-ge v9, p0, :cond_a

    aget-object p2, v10, v9

    .line 2719038
    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2719039
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2719040
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2719041
    const-string v6, " "

    .line 2719042
    :cond_9
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 2719043
    :cond_a
    iget-object v6, v2, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v9, LX/3Kr;->ab:LX/0Tn;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6, v9, v11}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 2719044
    array-length v6, v10

    if-ne v6, v8, :cond_b

    aget-object v6, v10, v7

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    move v6, v8

    .line 2719045
    goto/16 :goto_1

    :cond_b
    move v6, v7

    .line 2719046
    goto/16 :goto_1

    .line 2719047
    :cond_c
    sget-object v5, LX/03R;->YES:LX/03R;

    goto :goto_4

    .line 2719048
    :cond_d
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x17

    if-lt v5, v6, :cond_e

    .line 2719049
    sget-object v5, LX/FMK;->SWITCH_ACCOUNTS:LX/FMK;

    if-eq v4, v5, :cond_e

    .line 2719050
    invoke-static {v2, v7, v3}, LX/2uq;->a$redex0(LX/2uq;Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_6

    .line 2719051
    :cond_e
    sget-object v5, LX/FMK;->SWITCH_ACCOUNTS:LX/FMK;

    if-ne v4, v5, :cond_11

    .line 2719052
    const v5, 0x7f080925

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 2719053
    const v5, 0x104000a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 2719054
    :goto_8
    new-instance v8, LX/31Y;

    invoke-direct {v8, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v8, v6}, LX/0ju;->b(I)LX/0ju;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    new-instance v8, LX/FMi;

    invoke-direct {v8, v2, v7, v3}, LX/FMi;-><init>(LX/2uq;Landroid/content/Intent;Landroid/content/Context;)V

    invoke-virtual {v6, v5, v8}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    new-instance v6, LX/FMh;

    invoke-direct {v6, v2, v4, v7, v3}, LX/FMh;-><init>(LX/2uq;LX/FMK;Landroid/content/Intent;Landroid/content/Context;)V

    invoke-virtual {v5, v6}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->a()LX/2EJ;

    move-result-object v5

    move-object v5, v5

    .line 2719055
    invoke-virtual {v5}, LX/2EJ;->show()V

    goto/16 :goto_6

    :cond_f
    move v5, v6

    goto/16 :goto_5

    .line 2719056
    :cond_10
    new-instance v5, Landroid/content/Intent;

    const-string v7, "android.settings.WIRELESS_SETTINGS"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2719057
    :cond_11
    const v5, 0x7f080924

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 2719058
    const v5, 0x7f080926

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_8
.end method
