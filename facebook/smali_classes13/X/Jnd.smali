.class public LX/Jnd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2734205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734206
    iput-object p1, p0, LX/Jnd;->a:Landroid/content/Context;

    .line 2734207
    iput-object p2, p0, LX/Jnd;->b:LX/0Ot;

    .line 2734208
    iput-object p3, p0, LX/Jnd;->c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    .line 2734209
    return-void
.end method


# virtual methods
.method public final a(ZLX/Jnc;)Z
    .locals 5
    .param p2    # LX/Jnc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734210
    iget-object v0, p0, LX/Jnd;->c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    .line 2734211
    iget-object v1, v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->f:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2734212
    iget-object v2, v1, Lcom/facebook/user/model/User;->ac:LX/0XN;

    move-object v1, v2

    .line 2734213
    if-eqz v1, :cond_0

    .line 2734214
    sget-object v2, LX/Jnf;->a:[I

    invoke-virtual {v1}, LX/0XN;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2734215
    :cond_0
    sget-object v1, LX/03R;->UNSET:LX/03R;

    :goto_0
    move-object v0, v1

    .line 2734216
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2734217
    const/4 v0, 0x0

    .line 2734218
    :goto_1
    return v0

    .line 2734219
    :cond_1
    const/4 v4, 0x0

    .line 2734220
    new-instance v0, LX/Jnb;

    invoke-direct {v0, p0, p2, p1}, LX/Jnb;-><init>(LX/Jnd;LX/Jnc;Z)V

    .line 2734221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/Jnd;->a:Landroid/content/Context;

    const v3, 0x7f082e58

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/Jnd;->a:Landroid/content/Context;

    const v3, 0x7f082e59

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2734222
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/Jnd;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    const v3, 0x7f082e57

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f082e5a

    invoke-virtual {v1, v2, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {v1, v2, v0}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2734223
    invoke-virtual {v0, v4}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 2734224
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2734225
    const/4 v0, 0x1

    goto :goto_1

    .line 2734226
    :pswitch_0
    sget-object v1, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 2734227
    :pswitch_1
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
