.class public LX/K53;
.super LX/K4z;
.source ""


# instance fields
.field public a:Z

.field public b:I

.field public c:LX/K4v;

.field public d:LX/K5C;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private g:I

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[F>;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2769786
    invoke-direct {p0}, LX/K4z;-><init>()V

    .line 2769787
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K53;->i:Ljava/util/List;

    .line 2769788
    sget-object v0, LX/K4v;->NORMAL:LX/K4v;

    .line 2769789
    iput-object v0, p0, LX/K53;->c:LX/K4v;

    .line 2769790
    sget-object v0, LX/K5C;->LESS:LX/K5C;

    .line 2769791
    iput-object v0, p0, LX/K53;->d:LX/K5C;

    .line 2769792
    iput-boolean v1, p0, LX/K53;->a:Z

    .line 2769793
    iput v1, p0, LX/K53;->j:I

    .line 2769794
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;LX/K4y;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/K54;",
            ">;",
            "LX/K4y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2769795
    new-instance v1, LX/K54;

    invoke-direct {v1}, LX/K54;-><init>()V

    .line 2769796
    iget-object v0, p0, LX/K53;->c:LX/K4v;

    sget-object v2, LX/K4v;->NORMAL:LX/K4v;

    if-eq v0, v2, :cond_0

    const/high16 v0, 0x1000000

    :goto_0
    iget v2, p0, LX/K53;->b:I

    add-int/2addr v0, v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 2769797
    iput v0, v1, LX/K54;->m:I

    .line 2769798
    iget-boolean v0, p0, LX/K53;->a:Z

    .line 2769799
    iput-boolean v0, v1, LX/K54;->j:Z

    .line 2769800
    iget-object v0, p0, LX/K53;->d:LX/K5C;

    .line 2769801
    iput-object v0, v1, LX/K54;->c:LX/K5C;

    .line 2769802
    iget-object v0, p0, LX/K53;->c:LX/K4v;

    .line 2769803
    iput-object v0, v1, LX/K54;->b:LX/K4v;

    .line 2769804
    iget v0, p0, LX/K53;->g:I

    if-nez v0, :cond_1

    .line 2769805
    iget-object v0, p0, LX/K53;->e:Ljava/lang/String;

    .line 2769806
    iput-object v0, v1, LX/K54;->h:Ljava/lang/String;

    .line 2769807
    iget-object v0, p0, LX/K53;->f:Ljava/lang/String;

    .line 2769808
    iput-object v0, v1, LX/K54;->i:Ljava/lang/String;

    .line 2769809
    :goto_1
    iget-object v0, v1, LX/K54;->a:LX/K4y;

    move-object v0, v0

    .line 2769810
    iget-object v2, p0, LX/K4z;->a:LX/K4y;

    move-object v2, v2

    .line 2769811
    invoke-virtual {v0, p2, v2}, LX/K4y;->a(LX/K4y;LX/K4y;)V

    .line 2769812
    iget-object v0, p0, LX/K53;->h:Ljava/util/Map;

    .line 2769813
    iput-object v0, v1, LX/K54;->k:Ljava/util/Map;

    .line 2769814
    iget-object v0, p0, LX/K53;->i:Ljava/util/List;

    .line 2769815
    iput-object v0, v1, LX/K54;->l:Ljava/util/List;

    .line 2769816
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2769817
    iget v0, p0, LX/K53;->j:I

    .line 2769818
    iput v0, v1, LX/K54;->n:I

    .line 2769819
    return-void

    .line 2769820
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2769821
    :cond_1
    iget v0, p0, LX/K53;->g:I

    .line 2769822
    iput v0, v1, LX/K54;->g:I

    .line 2769823
    goto :goto_1
.end method
