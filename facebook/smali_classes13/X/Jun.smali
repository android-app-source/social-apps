.class public final LX/Jun;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2HB;

.field public final synthetic b:Ljava/lang/CharSequence;

.field public final synthetic c:LX/3pw;

.field public final synthetic d:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic e:Lcom/facebook/messaging/model/threads/ThreadCustomization;

.field public final synthetic f:I

.field public final synthetic g:LX/Jul;

.field public final synthetic h:LX/3Re;


# direct methods
.method public constructor <init>(LX/3Re;LX/2HB;Ljava/lang/CharSequence;LX/3pw;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILX/Jul;)V
    .locals 0

    .prologue
    .line 2749454
    iput-object p1, p0, LX/Jun;->h:LX/3Re;

    iput-object p2, p0, LX/Jun;->a:LX/2HB;

    iput-object p3, p0, LX/Jun;->b:Ljava/lang/CharSequence;

    iput-object p4, p0, LX/Jun;->c:LX/3pw;

    iput-object p5, p0, LX/Jun;->d:Lcom/facebook/messaging/model/messages/Message;

    iput-object p6, p0, LX/Jun;->e:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iput p7, p0, LX/Jun;->f:I

    iput-object p8, p0, LX/Jun;->g:LX/Jul;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2749459
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 2749460
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2749461
    :try_start_0
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lm;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2749462
    iget-object v2, p0, LX/Jun;->a:LX/2HB;

    new-instance v3, LX/3pd;

    invoke-direct {v3}, LX/3pd;-><init>()V

    .line 2749463
    iput-object v1, v3, LX/3pd;->a:Landroid/graphics/Bitmap;

    .line 2749464
    move-object v3, v3

    .line 2749465
    iget-object v4, p0, LX/Jun;->b:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, LX/3pd;->a(Ljava/lang/CharSequence;)LX/3pd;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(LX/3pc;)LX/2HB;

    .line 2749466
    iget-object v2, p0, LX/Jun;->c:LX/3pw;

    iget-object v3, p0, LX/Jun;->h:LX/3Re;

    .line 2749467
    new-instance v4, LX/2HB;

    iget-object v5, v3, LX/3Re;->f:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/2HB;-><init>(Landroid/content/Context;)V

    new-instance v5, LX/3pd;

    invoke-direct {v5}, LX/3pd;-><init>()V

    .line 2749468
    iput-object v1, v5, LX/3pd;->a:Landroid/graphics/Bitmap;

    .line 2749469
    move-object v5, v5

    .line 2749470
    invoke-virtual {v4, v5}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v4

    new-instance v5, LX/3pw;

    invoke-direct {v5}, LX/3pw;-><init>()V

    const/4 p1, 0x1

    .line 2749471
    const/4 v3, 0x4

    invoke-static {v5, v3, p1}, LX/3pw;->a(LX/3pw;IZ)V

    .line 2749472
    move-object v5, v5

    .line 2749473
    invoke-interface {v5, v4}, LX/3pk;->a(LX/2HB;)LX/2HB;

    .line 2749474
    move-object v4, v4

    .line 2749475
    invoke-virtual {v4}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v4

    move-object v1, v4

    .line 2749476
    invoke-virtual {v2, v1}, LX/3pw;->a(Landroid/app/Notification;)LX/3pw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2749477
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 2749478
    :goto_0
    iget-object v0, p0, LX/Jun;->h:LX/3Re;

    iget-object v1, p0, LX/Jun;->d:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, p0, LX/Jun;->e:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v3, p0, LX/Jun;->f:I

    iget-object v4, p0, LX/Jun;->g:LX/Jul;

    iget-object v5, p0, LX/Jun;->c:LX/3pw;

    .line 2749479
    invoke-static/range {v0 .. v5}, LX/3Re;->a$redex0(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILX/Jul;LX/3pw;)V

    .line 2749480
    return-void

    .line 2749481
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1FJ;->close()V

    throw v1

    .line 2749482
    :cond_0
    iget-object v0, p0, LX/Jun;->a:LX/2HB;

    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    iget-object v2, p0, LX/Jun;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2749455
    iget-object v0, p0, LX/Jun;->a:LX/2HB;

    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    iget-object v2, p0, LX/Jun;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    .line 2749456
    iget-object v0, p0, LX/Jun;->h:LX/3Re;

    iget-object v1, p0, LX/Jun;->d:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, p0, LX/Jun;->e:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v3, p0, LX/Jun;->f:I

    iget-object v4, p0, LX/Jun;->g:LX/Jul;

    iget-object v5, p0, LX/Jun;->c:LX/3pw;

    .line 2749457
    invoke-static/range {v0 .. v5}, LX/3Re;->a$redex0(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILX/Jul;LX/3pw;)V

    .line 2749458
    return-void
.end method
