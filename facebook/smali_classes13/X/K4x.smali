.class public LX/K4x;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2769646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769647
    return-void
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 2769640
    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    .line 2769641
    invoke-static {v0, p0}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 2769642
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 2769643
    const/4 v1, 0x1

    new-array v1, v1, [I

    .line 2769644
    const v2, 0x8b81

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 2769645
    return v0
.end method

.method public static a(IIILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2769676
    invoke-static {p2, p3}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 2769677
    if-gez v0, :cond_0

    .line 2769678
    :goto_0
    return-void

    .line 2769679
    :cond_0
    invoke-static {p0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 2769680
    const/16 v1, 0xde1

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 2769681
    const v1, 0x84c0

    sub-int v1, p0, v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    goto :goto_0
.end method

.method public static a(LX/K57;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2769648
    iget-object v0, p0, LX/K57;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2769649
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Attempt to compile shaders with invalid vertex shader code"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2769650
    iget-object v0, p0, LX/K57;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2769651
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Attempt to compile shaders with invalid fragment shader code"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2769652
    iget-object v0, p0, LX/K57;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2769653
    const v3, 0x8b31

    invoke-static {v0, v3}, LX/K4x;->a(Ljava/lang/String;I)I

    move-result v0

    .line 2769654
    iget-object v3, p0, LX/K57;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2769655
    const v4, 0x8b30

    invoke-static {v3, v4}, LX/K4x;->a(Ljava/lang/String;I)I

    move-result v3

    .line 2769656
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v4

    .line 2769657
    iput v4, p0, LX/K57;->f:I

    .line 2769658
    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 2769659
    invoke-static {v4, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 2769660
    invoke-static {v4}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 2769661
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 2769662
    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 2769663
    new-array v0, v1, [I

    .line 2769664
    const v1, 0x8b82

    invoke-static {v4, v1, v0, v2}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 2769665
    invoke-static {v4}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 2769666
    const-string v0, "aPosition"

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    .line 2769667
    iput v0, p0, LX/K57;->c:I

    .line 2769668
    const-string v0, "aTextureCoord"

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    .line 2769669
    iput v0, p0, LX/K57;->d:I

    .line 2769670
    const-string v0, "uMVPMatrix"

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 2769671
    iput v0, p0, LX/K57;->e:I

    .line 2769672
    invoke-static {v2}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 2769673
    return-void

    :cond_0
    move v0, v2

    .line 2769674
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2769675
    goto :goto_1
.end method
