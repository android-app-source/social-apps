.class public final LX/Jch;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Jci;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field public d:Z


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718236
    const-string v0, "PrefetchMapComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2718237
    if-ne p0, p1, :cond_1

    .line 2718238
    :cond_0
    :goto_0
    return v0

    .line 2718239
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2718240
    goto :goto_0

    .line 2718241
    :cond_3
    check-cast p1, LX/Jch;

    .line 2718242
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2718243
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2718244
    if-eq v2, v3, :cond_0

    .line 2718245
    iget-object v2, p0, LX/Jch;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Jch;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Jch;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2718246
    goto :goto_0

    .line 2718247
    :cond_5
    iget-object v2, p1, LX/Jch;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2718248
    :cond_6
    iget-object v2, p0, LX/Jch;->b:LX/1Pt;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Jch;->b:LX/1Pt;

    iget-object v3, p1, LX/Jch;->b:LX/1Pt;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2718249
    goto :goto_0

    .line 2718250
    :cond_8
    iget-object v2, p1, LX/Jch;->b:LX/1Pt;

    if-nez v2, :cond_7

    .line 2718251
    :cond_9
    iget-object v2, p0, LX/Jch;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Jch;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v3, p1, LX/Jch;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2718252
    goto :goto_0

    .line 2718253
    :cond_b
    iget-object v2, p1, LX/Jch;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-nez v2, :cond_a

    .line 2718254
    :cond_c
    iget-boolean v2, p0, LX/Jch;->d:Z

    iget-boolean v3, p1, LX/Jch;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2718255
    goto :goto_0
.end method
