.class public final LX/Jbf;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/graphics/drawable/Drawable;

.field public final synthetic d:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2716908
    iput-object p1, p0, LX/Jbf;->d:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iput-object p2, p0, LX/Jbf;->a:Landroid/content/Intent;

    iput-object p3, p0, LX/Jbf;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Jbf;->c:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 2716911
    iget-object v0, p0, LX/Jbf;->d:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iget-object v0, v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->c:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    sget-object v1, LX/46b;->ROUNDED:LX/46b;

    invoke-virtual {v0, p1, v1, v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/graphics/Bitmap;LX/46b;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2716912
    iget-object v0, p0, LX/Jbf;->d:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iget-object v0, v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->c:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    iget-object v1, p0, LX/Jbf;->a:Landroid/content/Intent;

    iget-object v2, p0, LX/Jbf;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Jbf;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    .line 2716913
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2716909
    const-string v0, "QuicksilverFB4ADataProvider"

    const-string v1, "Failed to fetch icon image"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2716910
    return-void
.end method
