.class public final LX/JuW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/EventReminderNotification;

.field public final synthetic b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic c:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/notify/EventReminderNotification;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 2748825
    iput-object p1, p0, LX/JuW;->c:LX/3RG;

    iput-object p2, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iput-object p3, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 12
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 2748826
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2748827
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2748828
    iget-object v1, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/EventReminderNotification;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2748829
    const-string v1, "from_notification"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2748830
    iget-object v1, p0, LX/JuW;->c:LX/3RG;

    iget-object v1, v1, LX/3RG;->b:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v9, v0, v2}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2748831
    iget-object v1, p0, LX/JuW;->c:LX/3RG;

    const-string v2, "event_reminder"

    invoke-static {v1, v2}, LX/3RG;->c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2748832
    new-instance v2, LX/2HB;

    iget-object v4, p0, LX/JuW;->c:LX/3RG;

    iget-object v4, v4, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iget-object v4, v4, Lcom/facebook/messaging/notify/EventReminderNotification;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v4, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iget-object v4, v4, Lcom/facebook/messaging/notify/EventReminderNotification;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v4, p0, LX/JuW;->c:LX/3RG;

    iget-object v4, v4, LX/3RG;->e:LX/2zj;

    invoke-interface {v4}, LX/2zj;->g()I

    move-result v4

    invoke-virtual {v2, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v1

    .line 2748833
    iput v8, v1, LX/2HB;->j:I

    .line 2748834
    move-object v1, v1

    .line 2748835
    invoke-virtual {v1, v8}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v1

    .line 2748836
    iget-object v2, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/EventReminderNotification;->e:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->CALL:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v2, v4, :cond_4

    .line 2748837
    iget-object v2, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/EventReminderNotification;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/EventReminderNotification;->d:Ljava/lang/String;

    const-string v4, "notify"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2748838
    iget-object v0, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 2748839
    iget-object v2, p0, LX/JuW;->c:LX/3RG;

    const v4, 0x7f070077

    invoke-static {v2, v4}, LX/3RG;->a$redex0(LX/3RG;I)Landroid/net/Uri;

    move-result-object v5

    .line 2748840
    iget-object v2, p0, LX/JuW;->c:LX/3RG;

    iget-object v4, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2748841
    iget-object v6, v2, LX/3RG;->H:LX/0aU;

    const-string v7, "RTC_REMINDER_MESSAGING_ACTION"

    invoke-virtual {v6, v7}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2748842
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2748843
    iget-object v6, v2, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v6, v4}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 2748844
    if-nez v6, :cond_5

    .line 2748845
    const-string v6, "DefaultMessagingNotificationHandler"

    const-string v7, "createPendingIntentForCallReminderMessaging cannot fetch threadSummary"

    invoke-static {v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2748846
    const/4 v6, 0x0

    .line 2748847
    :goto_0
    move-object v2, v6

    .line 2748848
    invoke-static {}, LX/10A;->a()I

    move-result v4

    iget-object v6, p0, LX/JuW;->c:LX/3RG;

    iget-object v6, v6, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f08079a

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v6, v2}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2748849
    iget-object v2, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2748850
    iget-object v2, p0, LX/JuW;->c:LX/3RG;

    iget-object v4, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2748851
    iget-object v6, v2, LX/3RG;->H:LX/0aU;

    const-string v7, "RTC_CALL_WITH_CONFIRMATION_ACTION"

    invoke-virtual {v6, v7}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2748852
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2748853
    iget-object v6, v2, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v6, v4}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 2748854
    if-nez v6, :cond_6

    .line 2748855
    const-string v6, "DefaultMessagingNotificationHandler"

    const-string v7, "createPendingIntentForCallReminderCallBackWithConfirmation cannot fetch threadSummary"

    invoke-static {v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2748856
    const/4 v6, 0x0

    .line 2748857
    :goto_1
    move-object v2, v6

    .line 2748858
    iput-object v2, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748859
    const v4, 0x7f021ab9

    iget-object v6, p0, LX/JuW;->c:LX/3RG;

    iget-object v6, v6, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f080751

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v6, v2}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-object v6, v0

    .line 2748860
    :goto_2
    if-eqz p1, :cond_0

    .line 2748861
    iput-object p1, v1, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2748862
    :cond_0
    iget-object v0, p0, LX/JuW;->c:LX/3RG;

    iget-object v0, v0, LX/3RG;->f:LX/3RQ;

    new-instance v2, LX/Dhq;

    invoke-direct {v2}, LX/Dhq;-><init>()V

    iget-object v4, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual/range {v0 .. v5}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/net/Uri;)V

    .line 2748863
    iget-object v0, p0, LX/JuW;->c:LX/3RG;

    iget-object v0, v0, LX/3RG;->m:LX/2bC;

    const-string v2, "EVENT_REMINDER"

    invoke-virtual {v0, v3, v2, v3, v3}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2748864
    iget-object v0, p0, LX/JuW;->c:LX/3RG;

    iget-object v0, v0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2732

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v6, v2, v1}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2748865
    iget-object v0, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    .line 2748866
    iput-boolean v8, v0, Lcom/facebook/messaging/notify/EventReminderNotification;->g:Z

    .line 2748867
    iget-object v0, p0, LX/JuW;->a:Lcom/facebook/messaging/notify/EventReminderNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2748868
    return-void

    .line 2748869
    :cond_1
    iget-object v2, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2748870
    iget-object v2, p0, LX/JuW;->c:LX/3RG;

    iget-object v4, p0, LX/JuW;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const-string v6, "multiway_join_via_reminder_notification"

    invoke-virtual {v2, v4, v9, v3, v6}, LX/3RG;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 2748871
    iput-object v2, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748872
    const v4, 0x7f021ab9

    iget-object v6, p0, LX/JuW;->c:LX/3RG;

    iget-object v6, v6, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f080721

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v6, v2}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    :cond_2
    move-object v6, v0

    .line 2748873
    goto :goto_2

    .line 2748874
    :cond_3
    iget-object v2, p0, LX/JuW;->c:LX/3RG;

    const v4, 0x7f070076

    invoke-static {v2, v4}, LX/3RG;->a$redex0(LX/3RG;I)Landroid/net/Uri;

    move-result-object v5

    .line 2748875
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748876
    move-object v6, v3

    goto :goto_2

    .line 2748877
    :cond_4
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748878
    move-object v5, v3

    move-object v6, v3

    goto :goto_2

    .line 2748879
    :cond_5
    const-string v10, "THREAD_SUMMARY"

    invoke-virtual {v7, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2748880
    iget-object v6, v2, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v6

    .line 2748881
    iget-object v10, v2, LX/3RG;->b:Landroid/content/Context;

    const/high16 v11, 0x10000000

    invoke-static {v10, v6, v7, v11}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    goto/16 :goto_0

    .line 2748882
    :cond_6
    const-string v9, "THREAD_SUMMARY"

    invoke-virtual {v7, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2748883
    iget-object v6, v2, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v6

    .line 2748884
    iget-object v9, v2, LX/3RG;->b:Landroid/content/Context;

    const/high16 v10, 0x10000000

    invoke-static {v9, v6, v7, v10}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2748885
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuW;->a(Landroid/graphics/Bitmap;)V

    .line 2748886
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2748887
    const/4 v1, 0x0

    .line 2748888
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 2748889
    instance-of v2, v0, LX/1lm;

    if-eqz v2, :cond_0

    .line 2748890
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2748891
    :goto_0
    invoke-direct {p0, v0}, LX/JuW;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748892
    invoke-static {p1}, LX/1FJ;->c(LX/1FJ;)V

    .line 2748893
    return-void

    .line 2748894
    :catchall_0
    move-exception v0

    invoke-static {p1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
