.class public final LX/Jjh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V
    .locals 0

    .prologue
    .line 2728040
    iput-object p1, p0, LX/Jjh;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2728041
    iget-object v0, p0, LX/Jjh;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->s:Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-static {v0}, Lcom/facebook/messaging/events/banner/EventReminderParams;->a(Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/JjW;

    move-result-object v0

    iget-object v1, p0, LX/Jjh;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 2728042
    iput-wide v2, v0, LX/JjW;->c:J

    .line 2728043
    move-object v0, v0

    .line 2728044
    invoke-virtual {v0}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v0

    .line 2728045
    iget-object v1, p0, LX/Jjh;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->G:LX/JjT;

    iget-object v2, p0, LX/Jjh;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728046
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2728047
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2728048
    :goto_0
    iget-object v0, p0, LX/Jjh;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->finish()V

    .line 2728049
    const/4 v0, 0x1

    return v0

    .line 2728050
    :cond_0
    invoke-static {v1, v2, v0}, LX/JjT;->b(LX/JjT;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/4Gg;

    move-result-object v3

    .line 2728051
    new-instance v4, LX/Jjl;

    invoke-direct {v4}, LX/Jjl;-><init>()V

    move-object v4, v4

    .line 2728052
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/Jjl;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2728053
    iget-object v4, v1, LX/JjT;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2728054
    new-instance v4, LX/JjP;

    invoke-direct {v4, v1}, LX/JjP;-><init>(LX/JjT;)V

    .line 2728055
    iget-object v5, v1, LX/JjT;->d:LX/1Ck;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "tasks-deleteEvent:"

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
