.class public final LX/JW5;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JW7;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JW6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JW7",
            "<TE;>.PagesYouMayAdvertiseHeaderComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JW7;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JW7;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2702337
    iput-object p1, p0, LX/JW5;->b:LX/JW7;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2702338
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "feedProps"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JW5;->c:[Ljava/lang/String;

    .line 2702339
    iput v3, p0, LX/JW5;->d:I

    .line 2702340
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JW5;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JW5;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JW5;LX/1De;IILX/JW6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JW7",
            "<TE;>.PagesYouMayAdvertiseHeaderComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2702341
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2702342
    iput-object p4, p0, LX/JW5;->a:LX/JW6;

    .line 2702343
    iget-object v0, p0, LX/JW5;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2702344
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2702345
    invoke-super {p0}, LX/1X5;->a()V

    .line 2702346
    const/4 v0, 0x0

    iput-object v0, p0, LX/JW5;->a:LX/JW6;

    .line 2702347
    iget-object v0, p0, LX/JW5;->b:LX/JW7;

    iget-object v0, v0, LX/JW7;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2702348
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JW7;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2702349
    iget-object v1, p0, LX/JW5;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JW5;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JW5;->d:I

    if-ge v1, v2, :cond_2

    .line 2702350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2702351
    :goto_0
    iget v2, p0, LX/JW5;->d:I

    if-ge v0, v2, :cond_1

    .line 2702352
    iget-object v2, p0, LX/JW5;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2702353
    iget-object v2, p0, LX/JW5;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2702354
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2702355
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2702356
    :cond_2
    iget-object v0, p0, LX/JW5;->a:LX/JW6;

    .line 2702357
    invoke-virtual {p0}, LX/JW5;->a()V

    .line 2702358
    return-object v0
.end method
