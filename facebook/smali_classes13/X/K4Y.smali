.class public LX/K4Y;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768724
    const-class v0, LX/K4Y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K4Y;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2768722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768723
    return-void
.end method

.method public static a(Ljava/util/List;)LX/5pD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "LX/5pD;"
        }
    .end annotation

    .prologue
    .line 2768718
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v0

    .line 2768719
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2768720
    invoke-static {v2}, LX/K4Y;->a(Ljava/lang/Object;)LX/5pH;

    move-result-object v2

    invoke-interface {v0, v2}, LX/5pD;->a(LX/5pH;)V

    goto :goto_0

    .line 2768721
    :cond_0
    return-object v0
.end method

.method public static a(DDLandroid/net/Uri;)LX/5pH;
    .locals 4

    .prologue
    .line 2768713
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2768714
    const-string v1, "width"

    invoke-interface {v0, v1, p0, p1}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768715
    const-string v1, "height"

    invoke-interface {v0, v1, p2, p3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768716
    const-string v1, "uri"

    invoke-virtual {p4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768717
    return-object v0
.end method

.method public static a(Landroid/graphics/RectF;)LX/5pH;
    .locals 4
    .param p0    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2768705
    if-nez p0, :cond_0

    .line 2768706
    const/4 v0, 0x0

    .line 2768707
    :goto_0
    return-object v0

    .line 2768708
    :cond_0
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2768709
    const-string v1, "x"

    iget v2, p0, Landroid/graphics/RectF;->left:F

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768710
    const-string v1, "y"

    iget v2, p0, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768711
    const-string v1, "w"

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768712
    const-string v1, "h"

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/storyline/model/VisualData$Effect;)LX/5pH;
    .locals 5

    .prologue
    .line 2768608
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2768609
    iget-object v1, p0, Lcom/facebook/storyline/model/VisualData$Effect;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2768610
    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/storyline/model/VisualData$Effect;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768611
    :cond_0
    const-string v1, "duration"

    iget v2, p0, Lcom/facebook/storyline/model/VisualData$Effect;->duration:I

    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2768612
    const-string v1, "durationInCutDown"

    iget v2, p0, Lcom/facebook/storyline/model/VisualData$Effect;->durationInCutDown:I

    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2768613
    iget-object v1, p0, Lcom/facebook/storyline/model/VisualData$Effect;->attributes:Lcom/facebook/storyline/model/VisualData$Attributes;

    if-eqz v1, :cond_2

    .line 2768614
    const-string v1, "attributes"

    iget-object v2, p0, Lcom/facebook/storyline/model/VisualData$Effect;->attributes:Lcom/facebook/storyline/model/VisualData$Attributes;

    .line 2768615
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    .line 2768616
    iget-object v4, v2, Lcom/facebook/storyline/model/VisualData$Attributes;->overrideNext:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2768617
    const-string v4, "overrideNext"

    iget-object p0, v2, Lcom/facebook/storyline/model/VisualData$Attributes;->overrideNext:Ljava/lang/String;

    invoke-interface {v3, v4, p0}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768618
    :cond_1
    const-string v4, "alignPreviousEffectToCurrentHalf"

    iget-boolean p0, v2, Lcom/facebook/storyline/model/VisualData$Attributes;->alignPreviousEffectToCurrentHalf:Z

    invoke-interface {v3, v4, p0}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2768619
    const-string v4, "alignPreviousEffectToCurrentEnd"

    iget-boolean p0, v2, Lcom/facebook/storyline/model/VisualData$Attributes;->alignPreviousEffectToCurrentEnd:Z

    invoke-interface {v3, v4, p0}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2768620
    const-string v4, "alignNextEffectToCurrentHalf"

    iget-boolean p0, v2, Lcom/facebook/storyline/model/VisualData$Attributes;->alignNextEffectToCurrentHalf:Z

    invoke-interface {v3, v4, p0}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2768621
    move-object v2, v3

    .line 2768622
    invoke-interface {v0, v1, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2768623
    :cond_2
    return-object v0
.end method

.method private static a(Ljava/lang/Object;)LX/5pH;
    .locals 10

    .prologue
    .line 2768655
    instance-of v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;

    if-eqz v0, :cond_1

    .line 2768656
    check-cast p0, Lcom/facebook/storyline/model/StorylinePhoto;

    .line 2768657
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v4

    .line 2768658
    const-string v2, "assetType"

    const-string v3, "photo"

    invoke-interface {v4, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768659
    const-string v2, "id"

    iget-object v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    invoke-interface {v4, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768660
    const-string v2, "width"

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    invoke-interface {v4, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2768661
    const-string v2, "height"

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    invoke-interface {v4, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2768662
    const-string v2, "image"

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    int-to-double v6, v3

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    int-to-double v8, v3

    iget-object v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    invoke-static {v6, v7, v8, v9, v3}, LX/K4Y;->a(DDLandroid/net/Uri;)LX/5pH;

    move-result-object v3

    invoke-interface {v4, v2, v3}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2768663
    const-string v2, "low_res_image"

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    int-to-double v6, v3

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    int-to-double v8, v3

    iget-object v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    invoke-static {v6, v7, v8, v9, v3}, LX/K4Y;->a(DDLandroid/net/Uri;)LX/5pH;

    move-result-object v3

    invoke-interface {v4, v2, v3}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2768664
    const-string v2, "hi_res_image"

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    int-to-double v6, v3

    iget v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    int-to-double v8, v3

    iget-object v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    invoke-static {v6, v7, v8, v9, v3}, LX/K4Y;->a(DDLandroid/net/Uri;)LX/5pH;

    move-result-object v3

    invoke-interface {v4, v2, v3}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2768665
    const-string v2, "date"

    iget-wide v6, p0, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    invoke-interface {v4, v2, v6, v7}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768666
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v5

    .line 2768667
    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    if-eqz v2, :cond_0

    .line 2768668
    iget-object v6, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_0

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 2768669
    invoke-static {v2}, LX/K4Y;->a(Landroid/graphics/RectF;)LX/5pH;

    move-result-object v2

    invoke-interface {v5, v2}, LX/5pD;->a(LX/5pH;)V

    .line 2768670
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2768671
    :cond_0
    const-string v2, "faceboxes"

    invoke-interface {v4, v2, v5}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2768672
    const-string v2, "detectedFaceboxUnion"

    iget-object v3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    invoke-static {v3}, LX/K4Y;->a(Landroid/graphics/RectF;)LX/5pH;

    move-result-object v3

    invoke-interface {v4, v2, v3}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2768673
    move-object v0, v4

    .line 2768674
    :goto_1
    return-object v0

    .line 2768675
    :cond_1
    instance-of v0, p0, Lcom/facebook/storyline/model/StorylineUser;

    if-eqz v0, :cond_2

    .line 2768676
    check-cast p0, Lcom/facebook/storyline/model/StorylineUser;

    .line 2768677
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2768678
    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768679
    const-string v1, "firstName"

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768680
    const-string v1, "fullName"

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768681
    const-string v1, "profilePicURI"

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768682
    move-object v0, v0

    .line 2768683
    goto :goto_1

    .line 2768684
    :cond_2
    instance-of v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;

    if-eqz v0, :cond_8

    .line 2768685
    check-cast p0, Lcom/facebook/storyline/model/Cutdown$Section;

    .line 2768686
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v4

    .line 2768687
    iget-object v2, p0, Lcom/facebook/storyline/model/Cutdown$Section;->subdivisions:LX/0Px;

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2768688
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v5

    .line 2768689
    iget-object v6, p0, Lcom/facebook/storyline/model/Cutdown$Section;->subdivisions:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_3

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2768690
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v5, v2}, LX/5pD;->pushInt(I)V

    .line 2768691
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2768692
    :cond_3
    const-string v2, "subdivisions"

    invoke-interface {v4, v2, v5}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2768693
    :cond_4
    const-string v2, "startTime"

    iget v3, p0, Lcom/facebook/storyline/model/Cutdown$Section;->startTime:F

    float-to-double v6, v3

    invoke-interface {v4, v2, v6, v7}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2768694
    const-string v2, "preferredSubdivision"

    iget v3, p0, Lcom/facebook/storyline/model/Cutdown$Section;->preferredSubdivision:I

    invoke-interface {v4, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2768695
    iget-object v2, p0, Lcom/facebook/storyline/model/Cutdown$Section;->imageEffect:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 2768696
    const-string v2, "imageEffect"

    iget-object v3, p0, Lcom/facebook/storyline/model/Cutdown$Section;->imageEffect:Ljava/lang/String;

    invoke-interface {v4, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768697
    :cond_5
    iget-object v2, p0, Lcom/facebook/storyline/model/Cutdown$Section;->fillFrom:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 2768698
    const-string v2, "fillFrom"

    iget-object v3, p0, Lcom/facebook/storyline/model/Cutdown$Section;->fillFrom:Ljava/lang/String;

    invoke-interface {v4, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768699
    :cond_6
    iget v2, p0, Lcom/facebook/storyline/model/Cutdown$Section;->rank:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_7

    .line 2768700
    const-string v2, "rank"

    iget v3, p0, Lcom/facebook/storyline/model/Cutdown$Section;->rank:I

    invoke-interface {v4, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2768701
    const-string v2, "isOptional"

    iget-boolean v3, p0, Lcom/facebook/storyline/model/Cutdown$Section;->isOptional:Z

    invoke-interface {v4, v2, v3}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2768702
    :cond_7
    move-object v0, v4

    .line 2768703
    goto/16 :goto_1

    .line 2768704
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You screw up sucker!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/5pC;)LX/K4y;
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 2768650
    new-array v1, v4, [F

    .line 2768651
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 2768652
    invoke-interface {p0, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, v1, v0

    .line 2768653
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2768654
    :cond_0
    new-instance v0, LX/K4y;

    invoke-direct {v0, v1}, LX/K4y;-><init>([F)V

    return-object v0
.end method

.method public static a(LX/5pG;)LX/K51;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2768643
    const-string v0, "children"

    invoke-interface {p0, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    .line 2768644
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2768645
    invoke-interface {v0, v4}, LX/5pC;->getType(I)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v2

    sget-object v3, Lcom/facebook/react/bridge/ReadableType;->Map:Lcom/facebook/react/bridge/ReadableType;

    if-ne v2, v3, :cond_0

    .line 2768646
    invoke-interface {v0, v4}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    invoke-static {v2}, LX/K4Y;->c(LX/5pG;)LX/K50;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2768647
    :cond_0
    invoke-interface {v0, v5}, LX/5pC;->getType(I)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v2

    sget-object v3, Lcom/facebook/react/bridge/ReadableType;->Map:Lcom/facebook/react/bridge/ReadableType;

    if-ne v2, v3, :cond_1

    .line 2768648
    invoke-interface {v0, v5}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    invoke-static {v2}, LX/K4Y;->c(LX/5pG;)LX/K50;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2768649
    :cond_1
    new-instance v2, LX/K51;

    const/4 v3, 0x2

    invoke-static {v0, v3}, LX/K4Y;->a(LX/5pC;I)LX/K52;

    move-result-object v0

    invoke-direct {v2, v1, v0}, LX/K51;-><init>(Ljava/util/List;LX/K4z;)V

    return-object v2
.end method

.method private static a(LX/5pC;I)LX/K52;
    .locals 3

    .prologue
    .line 2768634
    new-instance v0, LX/K52;

    invoke-direct {v0}, LX/K52;-><init>()V

    .line 2768635
    :goto_0
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 2768636
    sget-object v1, LX/K4X;->a:[I

    invoke-interface {p0, p1}, LX/5pC;->getType(I)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2768637
    :cond_0
    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2768638
    :pswitch_0
    invoke-interface {p0, p1}, LX/5pC;->b(I)LX/5pC;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/K4Y;->a(LX/5pC;I)LX/K52;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K52;->a(LX/K4z;)V

    goto :goto_1

    .line 2768639
    :pswitch_1
    invoke-interface {p0, p1}, LX/5pC;->a(I)LX/5pG;

    move-result-object v1

    invoke-static {v1}, LX/K4Y;->d(LX/5pG;)LX/K4z;

    move-result-object v1

    .line 2768640
    if-eqz v1, :cond_0

    .line 2768641
    invoke-virtual {v0, v1}, LX/K52;->a(LX/K4z;)V

    goto :goto_1

    .line 2768642
    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0Px;LX/5pH;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/VisualData$Effect;",
            ">;",
            "LX/5pH;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2768627
    invoke-static {p0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2768628
    :goto_0
    return-void

    .line 2768629
    :cond_0
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v2

    .line 2768630
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/VisualData$Effect;

    .line 2768631
    invoke-static {v0}, LX/K4Y;->a(Lcom/facebook/storyline/model/VisualData$Effect;)LX/5pH;

    move-result-object v0

    invoke-interface {v2, v0}, LX/5pD;->a(LX/5pH;)V

    .line 2768632
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2768633
    :cond_1
    invoke-interface {p1, p2, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/storyline/model/VisualData$Effect;LX/5pH;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2768624
    if-nez p0, :cond_0

    .line 2768625
    :goto_0
    return-void

    .line 2768626
    :cond_0
    invoke-static {p0}, LX/K4Y;->a(Lcom/facebook/storyline/model/VisualData$Effect;)LX/5pH;

    move-result-object v0

    invoke-interface {p1, p2, v0}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    goto :goto_0
.end method

.method private static b(LX/5pC;)LX/K5B;
    .locals 6

    .prologue
    .line 2768607
    new-instance v0, LX/K5B;

    const/4 v1, 0x0

    invoke-interface {p0, v1}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-float v1, v2

    const/4 v2, 0x1

    invoke-interface {p0, v2}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-float v2, v2

    const/4 v3, 0x2

    invoke-interface {p0, v3}, LX/5pC;->getDouble(I)D

    move-result-wide v4

    double-to-float v3, v4

    const/4 v4, 0x3

    invoke-interface {p0, v4}, LX/5pC;->getDouble(I)D

    move-result-wide v4

    double-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/K5B;-><init>(FFFF)V

    return-object v0
.end method

.method private static c(LX/5pG;)LX/K50;
    .locals 3

    .prologue
    .line 2768589
    new-instance v0, LX/K50;

    invoke-direct {v0}, LX/K50;-><init>()V

    .line 2768590
    const-string v1, "colorValue"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2768591
    const-string v1, "colorValue"

    invoke-interface {p0, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    invoke-static {v1}, LX/K4Y;->b(LX/5pC;)LX/K5B;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K50;->a(LX/K5B;)V

    .line 2768592
    :cond_0
    const-string v1, "projectionMatrix"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2768593
    const-string v1, "projectionMatrix"

    invoke-interface {p0, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    invoke-static {v1}, LX/K4Y;->a(LX/5pC;)LX/K4y;

    move-result-object v1

    .line 2768594
    iget-object v2, v0, LX/K50;->a:LX/K4y;

    invoke-virtual {v2, v1}, LX/K4y;->a(LX/K4y;)V

    .line 2768595
    :cond_1
    const-string v1, "matrix"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2768596
    const-string v1, "matrix"

    invoke-interface {p0, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    invoke-static {v1}, LX/K4Y;->a(LX/5pC;)LX/K4y;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K4z;->b(LX/K4y;)V

    .line 2768597
    :cond_2
    const-string v1, "order"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2768598
    const-string v1, "order"

    invoke-interface {p0, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2768599
    iput v1, v0, LX/K50;->h:I

    .line 2768600
    :cond_3
    const-string v1, "clearColor"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2768601
    const-string v1, "clearColor"

    invoke-interface {p0, v1}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2768602
    iput-boolean v1, v0, LX/K50;->g:Z

    .line 2768603
    :cond_4
    const-string v1, "clearDepth"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2768604
    const-string v1, "clearDepth"

    invoke-interface {p0, v1}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2768605
    iput-boolean v1, v0, LX/K50;->f:Z

    .line 2768606
    :cond_5
    return-object v0
.end method

.method public static c(LX/5pC;)[F
    .locals 4

    .prologue
    .line 2768584
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v0

    new-array v1, v0, [F

    .line 2768585
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2768586
    invoke-interface {p0, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, v1, v0

    .line 2768587
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2768588
    :cond_0
    return-object v1
.end method

.method private static d(LX/5pG;)LX/K4z;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2768537
    const/4 v0, 0x0

    .line 2768538
    const-string v1, "children"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2768539
    const-string v0, "children"

    invoke-interface {p0, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/K4Y;->a(LX/5pC;I)LX/K52;

    move-result-object v0

    .line 2768540
    :cond_0
    const-string v1, "type"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "type"

    invoke-interface {p0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2768541
    new-instance v3, LX/K53;

    invoke-direct {v3}, LX/K53;-><init>()V

    .line 2768542
    const-string v4, "samplers"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2768543
    const-string v4, "samplers"

    invoke-interface {p0, v4}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v4

    .line 2768544
    invoke-interface {v4}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2768545
    invoke-interface {v5}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v6

    .line 2768546
    invoke-interface {v4, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2768547
    iget-object v8, v3, LX/K53;->i:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    const/16 v9, 0x10

    if-ge v8, v9, :cond_1

    .line 2768548
    const-string v8, "(?<!https:)//"

    const-string v9, "/"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2768549
    const-string v9, "file://"

    const-string v10, "file:///"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2768550
    iget-object v9, v3, LX/K53;->i:Ljava/util/List;

    invoke-static {v6, v8}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v8

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2768551
    :cond_1
    goto :goto_0

    .line 2768552
    :cond_2
    const-string v4, "uniforms"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2768553
    const-string v4, "uniforms"

    invoke-interface {p0, v4}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v4

    .line 2768554
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2768555
    invoke-interface {v4}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2768556
    invoke-interface {v6}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v7

    .line 2768557
    sget-object v8, LX/K4X;->a:[I

    invoke-interface {v4, v7}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 2768558
    :pswitch_1
    invoke-interface {v4, v7}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v8

    invoke-static {v8}, LX/K4Y;->c(LX/5pC;)[F

    move-result-object v8

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2768559
    :pswitch_2
    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    invoke-interface {v4, v7}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v11

    double-to-float v10, v11

    aput v10, v8, v9

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2768560
    :cond_3
    iput-object v5, v3, LX/K53;->h:Ljava/util/Map;

    .line 2768561
    :cond_4
    const-string v4, "viewMask"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2768562
    const-string v4, "viewMask"

    invoke-interface {p0, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2768563
    iput v4, v3, LX/K53;->j:I

    .line 2768564
    :cond_5
    const-string v4, "program"

    invoke-interface {p0, v4}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v4

    .line 2768565
    const-string v5, "vertexProgram"

    invoke-interface {v4, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2768566
    iput-object v5, v3, LX/K53;->e:Ljava/lang/String;

    .line 2768567
    const-string v5, "fragmentProgram"

    invoke-interface {v4, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2768568
    iput-object v4, v3, LX/K53;->f:Ljava/lang/String;

    .line 2768569
    const-string v4, "sortOrder"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2768570
    const-string v4, "sortOrder"

    invoke-interface {p0, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2768571
    iput v4, v3, LX/K53;->b:I

    .line 2768572
    :cond_6
    const-string v4, "blendMode"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2768573
    const-string v4, "blendMode"

    invoke-interface {p0, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/K4v;->convert(Ljava/lang/String;)LX/K4v;

    move-result-object v4

    .line 2768574
    iput-object v4, v3, LX/K53;->c:LX/K4v;

    .line 2768575
    :cond_7
    const-string v4, "ztestMode"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2768576
    const-string v4, "ztestMode"

    invoke-interface {p0, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/K5C;->convert(Ljava/lang/String;)LX/K5C;

    move-result-object v4

    .line 2768577
    iput-object v4, v3, LX/K53;->d:LX/K5C;

    .line 2768578
    :cond_8
    move-object v0, v3

    .line 2768579
    :cond_9
    const-string v1, "type"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "type"

    invoke-interface {p0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "text"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2768580
    invoke-static {p0}, LX/K4Y;->f(LX/5pG;)LX/K56;

    move-result-object v0

    .line 2768581
    :cond_a
    if-eqz v0, :cond_b

    .line 2768582
    const-string v1, "matrix"

    invoke-interface {p0, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    invoke-static {v1}, LX/K4Y;->a(LX/5pC;)LX/K4y;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K4z;->b(LX/K4y;)V

    .line 2768583
    :cond_b
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static f(LX/5pG;)LX/K56;
    .locals 14

    .prologue
    .line 2768489
    new-instance v11, LX/K56;

    invoke-direct {v11}, LX/K56;-><init>()V

    .line 2768490
    const-string v0, "uniforms"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2768491
    const-string v0, "uniforms"

    invoke-interface {p0, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 2768492
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2768493
    invoke-interface {v0}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2768494
    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v3

    .line 2768495
    sget-object v4, LX/K4X;->a:[I

    invoke-interface {v0, v3}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2768496
    :pswitch_1
    invoke-interface {v0, v3}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v4

    invoke-static {v4}, LX/K4Y;->c(LX/5pC;)[F

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2768497
    :pswitch_2
    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    invoke-interface {v0, v3}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v6, v6

    aput v6, v4, v5

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2768498
    :cond_0
    iput-object v1, v11, LX/K56;->l:Ljava/util/Map;

    .line 2768499
    :cond_1
    const-string v0, "viewMask"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2768500
    const-string v0, "viewMask"

    invoke-interface {p0, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2768501
    iput v0, v11, LX/K56;->m:I

    .line 2768502
    :cond_2
    const-string v0, "program"

    invoke-interface {p0, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 2768503
    const-string v1, "vertexProgram"

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2768504
    iput-object v1, v11, LX/K56;->i:Ljava/lang/String;

    .line 2768505
    const-string v1, "fragmentProgram"

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2768506
    iput-object v0, v11, LX/K56;->j:Ljava/lang/String;

    .line 2768507
    const-string v0, "sortOrder"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2768508
    const-string v0, "sortOrder"

    invoke-interface {p0, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2768509
    iput v0, v11, LX/K56;->f:I

    .line 2768510
    :cond_3
    const-string v0, "blendMode"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2768511
    const-string v0, "blendMode"

    invoke-interface {p0, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/K4v;->convert(Ljava/lang/String;)LX/K4v;

    move-result-object v0

    .line 2768512
    iput-object v0, v11, LX/K56;->g:LX/K4v;

    .line 2768513
    :cond_4
    const-string v0, "ztestMode"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2768514
    const-string v0, "ztestMode"

    invoke-interface {p0, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/K5C;->convert(Ljava/lang/String;)LX/K5C;

    move-result-object v0

    .line 2768515
    iput-object v0, v11, LX/K56;->h:LX/K5C;

    .line 2768516
    :cond_5
    const/4 v8, 0x0

    .line 2768517
    const-string v0, "align"

    invoke-interface {p0, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_6
    :goto_1
    packed-switch v0, :pswitch_data_1

    .line 2768518
    :goto_2
    const/4 v5, -0x1

    .line 2768519
    const-string v0, "textColor"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2768520
    const-string v0, "textColor"

    invoke-interface {p0, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-static {v0}, LX/K4Y;->b(LX/5pC;)LX/K5B;

    move-result-object v0

    .line 2768521
    iget v1, v0, LX/K5B;->d:F

    move v1, v1

    .line 2768522
    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 2768523
    iget v2, v0, LX/K5B;->a:F

    move v2, v2

    .line 2768524
    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 2768525
    iget v3, v0, LX/K5B;->b:F

    move v3, v3

    .line 2768526
    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 2768527
    iget v4, v0, LX/K5B;->c:F

    move v0, v4

    .line 2768528
    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    .line 2768529
    :cond_7
    new-instance v0, LX/K59;

    const-string v1, "text"

    invoke-interface {p0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fontName"

    invoke-interface {p0, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fontSize"

    invoke-interface {p0, v3}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v3, v6

    const-string v4, "lineSpacing"

    invoke-interface {p0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "lineSpacing"

    invoke-interface {p0, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v4, v6

    :goto_3
    const-string v6, "maxTextureWidth"

    invoke-interface {p0, v6}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "maxTextureWidth"

    invoke-interface {p0, v6}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-int v6, v6

    :goto_4
    const-string v7, "maxTextureHeight"

    invoke-interface {p0, v7}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    const-string v7, "maxTextureHeight"

    invoke-interface {p0, v7}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    double-to-int v7, v12

    :goto_5
    const-string v9, "vAlign"

    invoke-interface {p0, v9}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/K58;->convert(Ljava/lang/String;)LX/K58;

    move-result-object v9

    const-string v10, "lineBreak"

    invoke-interface {p0, v10}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    const-string v10, "lineBreak"

    invoke-interface {p0, v10}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v12, "tail"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    const/4 v10, 0x1

    :goto_6
    invoke-direct/range {v0 .. v10}, LX/K59;-><init>(Ljava/lang/String;Ljava/lang/String;FFIIILandroid/graphics/Paint$Align;LX/K58;Z)V

    .line 2768530
    iput-object v0, v11, LX/K56;->d:LX/K59;

    .line 2768531
    return-object v11

    .line 2768532
    :sswitch_0
    const-string v2, "left"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x0

    goto/16 :goto_1

    :sswitch_1
    const-string v2, "center"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x1

    goto/16 :goto_1

    :sswitch_2
    const-string v2, "right"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x2

    goto/16 :goto_1

    .line 2768533
    :pswitch_3
    sget-object v8, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    goto/16 :goto_2

    .line 2768534
    :pswitch_4
    sget-object v8, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    goto/16 :goto_2

    .line 2768535
    :pswitch_5
    sget-object v8, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    goto/16 :goto_2

    .line 2768536
    :cond_8
    const/4 v4, 0x0

    goto :goto_3

    :cond_9
    const/4 v6, 0x0

    goto :goto_4

    :cond_a
    const/4 v7, 0x0

    goto :goto_5

    :cond_b
    const/4 v10, 0x0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x514d33ab -> :sswitch_1
        0x32a007 -> :sswitch_0
        0x677c21c -> :sswitch_2
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
