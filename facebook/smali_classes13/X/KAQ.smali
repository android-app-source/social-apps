.class public final LX/KAQ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/KAR;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2779360
    invoke-static {}, LX/KAR;->q()LX/KAR;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2779361
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2779362
    const-string v0, "FindGroupsFindButton"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2779363
    if-ne p0, p1, :cond_1

    .line 2779364
    :cond_0
    :goto_0
    return v0

    .line 2779365
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2779366
    goto :goto_0

    .line 2779367
    :cond_3
    check-cast p1, LX/KAQ;

    .line 2779368
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2779369
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2779370
    if-eq v2, v3, :cond_0

    .line 2779371
    iget-object v2, p0, LX/KAQ;->a:LX/1dQ;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/KAQ;->a:LX/1dQ;

    iget-object v3, p1, LX/KAQ;->a:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2779372
    goto :goto_0

    .line 2779373
    :cond_4
    iget-object v2, p1, LX/KAQ;->a:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
