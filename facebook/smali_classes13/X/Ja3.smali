.class public LX/Ja3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/23P;

.field private final b:LX/3UJ;

.field public final c:LX/0xW;

.field public final d:LX/1rj;

.field public final e:LX/1vi;

.field public final f:LX/0SI;


# direct methods
.method public constructor <init>(LX/23P;LX/3UJ;LX/0xW;LX/1ri;LX/1vi;LX/0SI;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2709489
    iput-object p1, p0, LX/Ja3;->a:LX/23P;

    .line 2709490
    iput-object p2, p0, LX/Ja3;->b:LX/3UJ;

    .line 2709491
    iput-object p3, p0, LX/Ja3;->c:LX/0xW;

    .line 2709492
    iget-object v0, p4, LX/1ri;->a:LX/1rj;

    move-object v0, v0

    .line 2709493
    iput-object v0, p0, LX/Ja3;->d:LX/1rj;

    .line 2709494
    iput-object p5, p0, LX/Ja3;->e:LX/1vi;

    .line 2709495
    iput-object p6, p0, LX/Ja3;->f:LX/0SI;

    .line 2709496
    return-void
.end method

.method public static a(LX/Ja3;LX/1De;Ljava/lang/String;ILX/1dQ;)LX/1Di;
    .locals 3

    .prologue
    .line 2709497
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    iget-object v1, p0, LX/Ja3;->a:LX/23P;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b02aa

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    sget-object v1, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v0, v1}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b02ab

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Ja3;
    .locals 10

    .prologue
    .line 2709498
    const-class v1, LX/Ja3;

    monitor-enter v1

    .line 2709499
    :try_start_0
    sget-object v0, LX/Ja3;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709500
    sput-object v2, LX/Ja3;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709501
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709502
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709503
    new-instance v3, LX/Ja3;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, LX/3UJ;->b(LX/0QB;)LX/3UJ;

    move-result-object v5

    check-cast v5, LX/3UJ;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v6

    check-cast v6, LX/0xW;

    invoke-static {v0}, LX/1ri;->b(LX/0QB;)LX/1ri;

    move-result-object v7

    check-cast v7, LX/1ri;

    invoke-static {v0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v8

    check-cast v8, LX/1vi;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v9

    check-cast v9, LX/0SI;

    invoke-direct/range {v3 .. v9}, LX/Ja3;-><init>(LX/23P;LX/3UJ;LX/0xW;LX/1ri;LX/1vi;LX/0SI;)V

    .line 2709504
    move-object v0, v3

    .line 2709505
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709506
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ja3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709507
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2709509
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/friending/components/spec/FriendRequestActionListComponentSpec$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/friending/components/spec/FriendRequestActionListComponentSpec$2;-><init>(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;)V

    const-wide/16 v2, 0x1f4

    const v4, 0x694c273a

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2709510
    return-void
.end method

.method public static a(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;LX/2na;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/lang/String;",
            "LX/2na;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2709511
    iget-object v1, p0, LX/Ja3;->b:LX/3UJ;

    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    new-instance v6, LX/Ja2;

    invoke-direct {v6, p0, p1, p2, p3}, LX/Ja2;-><init>(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;)V

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, LX/3UJ;->a(JLX/2hA;LX/2na;LX/84H;)V

    .line 2709512
    return-void
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2709513
    new-instance v0, LX/JZo;

    invoke-direct {v0, p2}, LX/JZo;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, p0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JZp;

    .line 2709514
    invoke-virtual {v0, p3}, LX/JZp;->a(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    .line 2709515
    check-cast p1, LX/1Pq;

    invoke-interface {p1}, LX/1Pq;->iN_()V

    .line 2709516
    return-void
.end method
