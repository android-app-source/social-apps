.class public final LX/JuU;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:Lcom/facebook/messaging/notify/FriendInstallNotification;

.field public final synthetic c:I

.field public final synthetic d:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Landroid/content/Intent;Lcom/facebook/messaging/notify/FriendInstallNotification;I)V
    .locals 0

    .prologue
    .line 2748760
    iput-object p1, p0, LX/JuU;->d:LX/3RG;

    iput-object p2, p0, LX/JuU;->a:Landroid/content/Intent;

    iput-object p3, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    iput p4, p0, LX/JuU;->c:I

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 2748761
    iget-object v0, p0, LX/JuU;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->b:Landroid/content/Context;

    const/4 v1, 0x0

    iget-object v2, p0, LX/JuU;->a:Landroid/content/Intent;

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2748762
    new-instance v1, LX/2HB;

    iget-object v2, p0, LX/JuU;->d:LX/3RG;

    iget-object v2, v2, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2748763
    iget-object v3, v2, Lcom/facebook/messaging/notify/FriendInstallNotification;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2748764
    invoke-virtual {v1, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget-object v2, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2748765
    iget-object v3, v2, Lcom/facebook/messaging/notify/FriendInstallNotification;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2748766
    invoke-virtual {v1, v2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget-object v2, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2748767
    iget-object v3, v2, Lcom/facebook/messaging/notify/FriendInstallNotification;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2748768
    invoke-virtual {v1, v2}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget v2, p0, LX/JuU;->c:I

    invoke-virtual {v1, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    new-instance v2, LX/3pe;

    invoke-direct {v2}, LX/3pe;-><init>()V

    iget-object v3, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2748769
    iget-object v5, v3, Lcom/facebook/messaging/notify/FriendInstallNotification;->c:Ljava/lang/String;

    move-object v3, v5

    .line 2748770
    invoke-virtual {v2, v3}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v1

    .line 2748771
    iput-object p1, v1, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2748772
    move-object v1, v1

    .line 2748773
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748774
    move-object v0, v1

    .line 2748775
    invoke-virtual {v0, v4}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 2748776
    iget-object v1, p0, LX/JuU;->d:LX/3RG;

    iget-object v1, v1, LX/3RG;->d:LX/3RK;

    iget-object v2, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2748777
    iget-object v3, v2, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2748778
    const/16 v3, 0x2713

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2748779
    iget-object v0, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2748780
    iput-boolean v4, v0, Lcom/facebook/messaging/notify/FriendInstallNotification;->f:Z

    .line 2748781
    iget-object v0, p0, LX/JuU;->b:Lcom/facebook/messaging/notify/FriendInstallNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2748782
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748783
    invoke-direct {p0, p1}, LX/JuU;->b(Landroid/graphics/Bitmap;)V

    .line 2748784
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2748785
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuU;->b(Landroid/graphics/Bitmap;)V

    .line 2748786
    return-void
.end method
