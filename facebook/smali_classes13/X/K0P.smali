.class public LX/K0P;
.super Lcom/facebook/drawee/view/GenericDraweeView;
.source ""


# static fields
.field public static a:[F

.field public static final b:Landroid/graphics/Matrix;

.field public static final c:Landroid/graphics/Matrix;


# instance fields
.field private d:LX/K0L;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9nR;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/9nR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/9nR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:F

.field private l:F

.field private m:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/1Up;

.field private o:Z

.field private final p:LX/1Ae;

.field private final q:LX/K0O;

.field private r:LX/1Ai;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/1Ai;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final t:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:I

.field public v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2758630
    const/4 v0, 0x4

    new-array v0, v0, [F

    sput-object v0, LX/K0P;->a:[F

    .line 2758631
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/K0P;->b:Landroid/graphics/Matrix;

    .line 2758632
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/K0P;->c:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Ae;Ljava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2758633
    invoke-static {p1}, LX/K0P;->a(Landroid/content/Context;)LX/1af;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2758634
    sget-object v0, LX/K0L;->AUTO:LX/K0L;

    iput-object v0, p0, LX/K0P;->d:LX/K0L;

    .line 2758635
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, LX/K0P;->l:F

    .line 2758636
    const/4 v0, -0x1

    iput v0, p0, LX/K0P;->u:I

    .line 2758637
    sget-object v0, LX/1Up;->g:LX/1Up;

    move-object v0, v0

    .line 2758638
    iput-object v0, p0, LX/K0P;->n:LX/1Up;

    .line 2758639
    iput-object p2, p0, LX/K0P;->p:LX/1Ae;

    .line 2758640
    new-instance v0, LX/K0O;

    invoke-direct {v0, p0}, LX/K0O;-><init>(LX/K0P;)V

    iput-object v0, p0, LX/K0P;->q:LX/K0O;

    .line 2758641
    iput-object p3, p0, LX/K0P;->t:Ljava/lang/Object;

    .line 2758642
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/K0P;->e:Ljava/util/List;

    .line 2758643
    return-void
.end method

.method private static a(Landroid/content/Context;)LX/1af;
    .locals 2

    .prologue
    .line 2758474
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const/4 v1, 0x0

    invoke-static {v1}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v1

    .line 2758475
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2758476
    move-object v0, v0

    .line 2758477
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/9nR;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2758625
    iget-object v2, p0, LX/K0P;->d:LX/K0L;

    sget-object v3, LX/K0L;->AUTO:LX/K0L;

    if-ne v2, v3, :cond_2

    .line 2758626
    invoke-virtual {p1}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 2758627
    :cond_1
    :goto_0
    return v0

    .line 2758628
    :cond_2
    iget-object v2, p0, LX/K0P;->d:LX/K0L;

    sget-object v3, LX/K0L;->RESIZE:LX/K0L;

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 2758629
    goto :goto_0
.end method

.method public static a$redex0(LX/K0P;[F)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2758615
    iget v0, p0, LX/K0P;->l:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, LX/K0P;->l:F

    .line 2758616
    :goto_0
    iget-object v1, p0, LX/K0P;->m:[F

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v2

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v2

    :goto_1
    aput v1, p1, v2

    .line 2758617
    iget-object v1, p0, LX/K0P;->m:[F

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v3

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v3

    :goto_2
    aput v1, p1, v3

    .line 2758618
    iget-object v1, p0, LX/K0P;->m:[F

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v4

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v4

    :goto_3
    aput v1, p1, v4

    .line 2758619
    iget-object v1, p0, LX/K0P;->m:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/K0P;->m:[F

    aget v1, v1, v5

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, LX/K0P;->m:[F

    aget v0, v0, v5

    :cond_0
    aput v0, p1, v5

    .line 2758620
    return-void

    .line 2758621
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v1, v0

    .line 2758622
    goto :goto_1

    :cond_3
    move v1, v0

    .line 2758623
    goto :goto_2

    :cond_4
    move v1, v0

    .line 2758624
    goto :goto_3
.end method

.method private g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2758614
    iget-object v1, p0, LX/K0P;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2758604
    const/4 v0, 0x0

    iput-object v0, p0, LX/K0P;->f:LX/9nR;

    .line 2758605
    iget-object v0, p0, LX/K0P;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2758606
    :goto_0
    return-void

    .line 2758607
    :cond_0
    invoke-direct {p0}, LX/K0P;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2758608
    invoke-virtual {p0}, LX/K0P;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/K0P;->getHeight()I

    move-result v1

    iget-object v2, p0, LX/K0P;->e:Ljava/util/List;

    invoke-static {v0, v1, v2}, LX/K0R;->a(IILjava/util/List;)LX/K0Q;

    move-result-object v0

    .line 2758609
    iget-object v1, v0, LX/K0Q;->a:LX/9nR;

    move-object v1, v1

    .line 2758610
    iput-object v1, p0, LX/K0P;->f:LX/9nR;

    .line 2758611
    iget-object v1, v0, LX/K0Q;->b:LX/9nR;

    move-object v0, v1

    .line 2758612
    iput-object v0, p0, LX/K0P;->g:LX/9nR;

    goto :goto_0

    .line 2758613
    :cond_1
    iget-object v0, p0, LX/K0P;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9nR;

    iput-object v0, p0, LX/K0P;->f:LX/9nR;

    goto :goto_0
.end method


# virtual methods
.method public final a(FI)V
    .locals 2

    .prologue
    .line 2758597
    iget-object v0, p0, LX/K0P;->m:[F

    if-nez v0, :cond_0

    .line 2758598
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LX/K0P;->m:[F

    .line 2758599
    iget-object v0, p0, LX/K0P;->m:[F

    const/high16 v1, 0x7fc00000    # NaNf

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 2758600
    :cond_0
    iget-object v0, p0, LX/K0P;->m:[F

    aget v0, v0, p2

    invoke-static {v0, p1}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2758601
    iget-object v0, p0, LX/K0P;->m:[F

    aput p1, v0, p2

    .line 2758602
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758603
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2758531
    iget-boolean v0, p0, LX/K0P;->o:Z

    if-nez v0, :cond_1

    .line 2758532
    :cond_0
    :goto_0
    return-void

    .line 2758533
    :cond_1
    invoke-direct {p0}, LX/K0P;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, LX/K0P;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/K0P;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 2758534
    :cond_2
    invoke-direct {p0}, LX/K0P;->h()V

    .line 2758535
    iget-object v0, p0, LX/K0P;->f:LX/9nR;

    if-eqz v0, :cond_0

    .line 2758536
    iget-object v0, p0, LX/K0P;->f:LX/9nR;

    invoke-direct {p0, v0}, LX/K0P;->a(LX/9nR;)Z

    move-result v6

    .line 2758537
    if-eqz v6, :cond_3

    invoke-virtual {p0}, LX/K0P;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/K0P;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 2758538
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2758539
    iget-object v1, p0, LX/K0P;->n:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 2758540
    iget-object v1, p0, LX/K0P;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    .line 2758541
    iget-object v1, p0, LX/K0P;->h:Landroid/graphics/drawable/Drawable;

    sget-object v4, LX/1Up;->e:LX/1Up;

    invoke-virtual {v0, v1, v4}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 2758542
    :cond_4
    iget-object v1, p0, LX/K0P;->n:LX/1Up;

    sget-object v4, LX/1Up;->g:LX/1Up;

    if-eq v1, v4, :cond_7

    iget-object v1, p0, LX/K0P;->n:LX/1Up;

    sget-object v4, LX/1Up;->h:LX/1Up;

    if-eq v1, v4, :cond_7

    move v1, v2

    .line 2758543
    :goto_1
    iget-object v4, v0, LX/1af;->c:LX/4Ab;

    move-object v4, v4

    .line 2758544
    if-eqz v1, :cond_8

    .line 2758545
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, LX/4Ab;->a(F)LX/4Ab;

    .line 2758546
    :goto_2
    iget v7, p0, LX/K0P;->i:I

    iget v8, p0, LX/K0P;->k:F

    invoke-virtual {v4, v7, v8}, LX/4Ab;->a(IF)LX/4Ab;

    .line 2758547
    iget v7, p0, LX/K0P;->j:I

    if-eqz v7, :cond_9

    .line 2758548
    iget v7, p0, LX/K0P;->j:I

    invoke-virtual {v4, v7}, LX/4Ab;->a(I)LX/4Ab;

    .line 2758549
    :goto_3
    invoke-virtual {v0, v4}, LX/1af;->a(LX/4Ab;)V

    .line 2758550
    iget v4, p0, LX/K0P;->u:I

    if-ltz v4, :cond_a

    iget v4, p0, LX/K0P;->u:I

    :goto_4
    invoke-virtual {v0, v4}, LX/1af;->a(I)V

    .line 2758551
    if-eqz v1, :cond_c

    iget-object v0, p0, LX/K0P;->q:LX/K0O;

    move-object v1, v0

    .line 2758552
    :goto_5
    if-eqz v6, :cond_d

    new-instance v0, LX/1o9;

    invoke-virtual {p0}, LX/K0P;->getWidth()I

    move-result v4

    invoke-virtual {p0}, LX/K0P;->getHeight()I

    move-result v5

    invoke-direct {v0, v4, v5}, LX/1o9;-><init>(II)V

    .line 2758553
    :goto_6
    iget-object v4, p0, LX/K0P;->f:LX/9nR;

    invoke-virtual {v4}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    .line 2758554
    iput-object v1, v4, LX/1bX;->j:LX/33B;

    .line 2758555
    move-object v4, v4

    .line 2758556
    iput-object v0, v4, LX/1bX;->c:LX/1o9;

    .line 2758557
    move-object v4, v4

    .line 2758558
    invoke-virtual {v4, v2}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v4

    iget-boolean v5, p0, LX/K0P;->v:Z

    .line 2758559
    iput-boolean v5, v4, LX/1bX;->g:Z

    .line 2758560
    move-object v4, v4

    .line 2758561
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    .line 2758562
    iget-object v5, p0, LX/K0P;->p:LX/1Ae;

    invoke-virtual {v5}, LX/1Ae;->b()LX/1Ae;

    .line 2758563
    iget-object v5, p0, LX/K0P;->p:LX/1Ae;

    invoke-virtual {v5, v2}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v5

    iget-object v6, p0, LX/K0P;->t:Ljava/lang/Object;

    invoke-virtual {v5, v6}, LX/1Ae;->b(Ljava/lang/Object;)LX/1Ae;

    move-result-object v5

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 2758564
    iget-object v4, p0, LX/K0P;->g:LX/9nR;

    if-eqz v4, :cond_5

    .line 2758565
    iget-object v4, p0, LX/K0P;->g:LX/9nR;

    invoke-virtual {v4}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    .line 2758566
    iput-object v1, v4, LX/1bX;->j:LX/33B;

    .line 2758567
    move-object v1, v4

    .line 2758568
    iput-object v0, v1, LX/1bX;->c:LX/1o9;

    .line 2758569
    move-object v0, v1

    .line 2758570
    invoke-virtual {v0, v2}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    iget-boolean v1, p0, LX/K0P;->v:Z

    .line 2758571
    iput-boolean v1, v0, LX/1bX;->g:Z

    .line 2758572
    move-object v0, v0

    .line 2758573
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2758574
    iget-object v1, p0, LX/K0P;->p:LX/1Ae;

    invoke-virtual {v1, v0}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    .line 2758575
    :cond_5
    iget-object v0, p0, LX/K0P;->r:LX/1Ai;

    if-eqz v0, :cond_e

    iget-object v0, p0, LX/K0P;->s:LX/1Ai;

    if-eqz v0, :cond_e

    .line 2758576
    new-instance v0, LX/1ff;

    invoke-direct {v0}, LX/1ff;-><init>()V

    .line 2758577
    iget-object v1, p0, LX/K0P;->r:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1ff;->a(LX/1Ai;)V

    .line 2758578
    iget-object v1, p0, LX/K0P;->s:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1ff;->a(LX/1Ai;)V

    .line 2758579
    iget-object v1, p0, LX/K0P;->p:LX/1Ae;

    invoke-virtual {v1, v0}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    .line 2758580
    :cond_6
    :goto_7
    iget-object v0, p0, LX/K0P;->p:LX/1Ae;

    invoke-virtual {v0}, LX/1Ae;->h()LX/1bp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2758581
    iput-boolean v3, p0, LX/K0P;->o:Z

    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 2758582
    goto/16 :goto_1

    .line 2758583
    :cond_8
    sget-object v7, LX/K0P;->a:[F

    invoke-static {p0, v7}, LX/K0P;->a$redex0(LX/K0P;[F)V

    .line 2758584
    sget-object v7, LX/K0P;->a:[F

    aget v7, v7, v3

    sget-object v8, LX/K0P;->a:[F

    aget v8, v8, v2

    sget-object v9, LX/K0P;->a:[F

    const/4 v10, 0x2

    aget v9, v9, v10

    sget-object v10, LX/K0P;->a:[F

    const/4 v11, 0x3

    aget v10, v10, v11

    invoke-virtual {v4, v7, v8, v9, v10}, LX/4Ab;->a(FFFF)LX/4Ab;

    goto/16 :goto_2

    .line 2758585
    :cond_9
    sget-object v7, LX/4Aa;->BITMAP_ONLY:LX/4Aa;

    .line 2758586
    iput-object v7, v4, LX/4Ab;->a:LX/4Aa;

    .line 2758587
    goto/16 :goto_3

    .line 2758588
    :cond_a
    iget-object v4, p0, LX/K0P;->f:LX/9nR;

    .line 2758589
    iget-boolean v7, v4, LX/9nR;->d:Z

    move v4, v7

    .line 2758590
    if-eqz v4, :cond_b

    move v4, v3

    goto/16 :goto_4

    :cond_b
    const/16 v4, 0x12c

    goto/16 :goto_4

    :cond_c
    move-object v1, v5

    .line 2758591
    goto/16 :goto_5

    :cond_d
    move-object v0, v5

    .line 2758592
    goto/16 :goto_6

    .line 2758593
    :cond_e
    iget-object v0, p0, LX/K0P;->s:LX/1Ai;

    if-eqz v0, :cond_f

    .line 2758594
    iget-object v0, p0, LX/K0P;->p:LX/1Ae;

    iget-object v1, p0, LX/K0P;->s:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    goto :goto_7

    .line 2758595
    :cond_f
    iget-object v0, p0, LX/K0P;->r:LX/1Ai;

    if-eqz v0, :cond_6

    .line 2758596
    iget-object v0, p0, LX/K0P;->p:LX/1Ae;

    iget-object v1, p0, LX/K0P;->r:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    goto :goto_7
.end method

.method public final hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 2758530
    const/4 v0, 0x0

    return v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x688c7b15

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2758524
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/drawee/view/GenericDraweeView;->onSizeChanged(IIII)V

    .line 2758525
    if-lez p1, :cond_1

    if-lez p2, :cond_1

    .line 2758526
    iget-boolean v0, p0, LX/K0P;->o:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/K0P;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758527
    invoke-virtual {p0}, LX/K0P;->c()V

    .line 2758528
    :cond_1
    const v0, -0x9567f25

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 2758529
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBorderColor(I)V
    .locals 1

    .prologue
    .line 2758644
    iput p1, p0, LX/K0P;->i:I

    .line 2758645
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758646
    return-void
.end method

.method public setBorderRadius(F)V
    .locals 1

    .prologue
    .line 2758520
    iget v0, p0, LX/K0P;->l:F

    invoke-static {v0, p1}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2758521
    iput p1, p0, LX/K0P;->l:F

    .line 2758522
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758523
    :cond_0
    return-void
.end method

.method public setBorderWidth(F)V
    .locals 1

    .prologue
    .line 2758517
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result v0

    iput v0, p0, LX/K0P;->k:F

    .line 2758518
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758519
    return-void
.end method

.method public setControllerListener(LX/1Ai;)V
    .locals 1

    .prologue
    .line 2758513
    iput-object p1, p0, LX/K0P;->s:LX/1Ai;

    .line 2758514
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758515
    invoke-virtual {p0}, LX/K0P;->c()V

    .line 2758516
    return-void
.end method

.method public setFadeDuration(I)V
    .locals 0

    .prologue
    .line 2758511
    iput p1, p0, LX/K0P;->u:I

    .line 2758512
    return-void
.end method

.method public setLoadingIndicatorSource(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2758506
    invoke-static {}, LX/9nS;->a()LX/9nS;

    move-result-object v0

    invoke-virtual {p0}, LX/K0P;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/9nS;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2758507
    if-eqz v1, :cond_0

    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    :goto_0
    iput-object v0, p0, LX/K0P;->h:Landroid/graphics/drawable/Drawable;

    .line 2758508
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758509
    return-void

    .line 2758510
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOverlayColor(I)V
    .locals 1

    .prologue
    .line 2758503
    iput p1, p0, LX/K0P;->j:I

    .line 2758504
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758505
    return-void
.end method

.method public setProgressiveRenderingEnabled(Z)V
    .locals 0

    .prologue
    .line 2758501
    iput-boolean p1, p0, LX/K0P;->v:Z

    .line 2758502
    return-void
.end method

.method public setResizeMethod(LX/K0L;)V
    .locals 1

    .prologue
    .line 2758498
    iput-object p1, p0, LX/K0P;->d:LX/K0L;

    .line 2758499
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758500
    return-void
.end method

.method public setScaleType(LX/1Up;)V
    .locals 1

    .prologue
    .line 2758495
    iput-object p1, p0, LX/K0P;->n:LX/1Up;

    .line 2758496
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758497
    return-void
.end method

.method public setShouldNotifyLoadEvents(Z)V
    .locals 2

    .prologue
    .line 2758488
    if-nez p1, :cond_0

    .line 2758489
    const/4 v0, 0x0

    iput-object v0, p0, LX/K0P;->r:LX/1Ai;

    .line 2758490
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0P;->o:Z

    .line 2758491
    return-void

    .line 2758492
    :cond_0
    invoke-virtual {p0}, LX/K0P;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2758493
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2758494
    new-instance v1, LX/K0N;

    invoke-direct {v1, p0, v0}, LX/K0N;-><init>(LX/K0P;LX/5s9;)V

    iput-object v1, p0, LX/K0P;->r:LX/1Ai;

    goto :goto_0
.end method

.method public setSource(LX/5pC;)V
    .locals 10
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 2758478
    iget-object v1, p0, LX/K0P;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2758479
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5pC;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2758480
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v1

    if-ne v1, v9, :cond_1

    .line 2758481
    iget-object v1, p0, LX/K0P;->e:Ljava/util/List;

    new-instance v2, LX/9nR;

    invoke-virtual {p0}, LX/K0P;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v0

    const-string v4, "uri"

    invoke-interface {v0, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2758482
    :cond_0
    iput-boolean v9, p0, LX/K0P;->o:Z

    .line 2758483
    return-void

    .line 2758484
    :cond_1
    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2758485
    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v6

    .line 2758486
    iget-object v8, p0, LX/K0P;->e:Ljava/util/List;

    new-instance v1, LX/9nR;

    invoke-virtual {p0}, LX/K0P;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "uri"

    invoke-interface {v6, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "width"

    invoke-interface {v6, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    const-string v7, "height"

    invoke-interface {v6, v7}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;DD)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2758487
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
