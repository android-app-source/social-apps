.class public final LX/Jud;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:LX/3RG;

.field private final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/messaging/notify/MessagingNotification;

.field private final e:LX/JuR;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Lcom/facebook/messaging/notify/MessagingNotification;LX/JuR;)V
    .locals 0

    .prologue
    .line 2749027
    iput-object p1, p0, LX/Jud;->a:LX/3RG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2749028
    iput-object p2, p0, LX/Jud;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2749029
    iput-object p3, p0, LX/Jud;->c:Ljava/lang/String;

    .line 2749030
    iput-object p4, p0, LX/Jud;->d:Lcom/facebook/messaging/notify/MessagingNotification;

    .line 2749031
    iput-object p5, p0, LX/Jud;->e:LX/JuR;

    .line 2749032
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749033
    iget-object v0, p0, LX/Jud;->a:LX/3RG;

    iget-object v0, v0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v1, p0, LX/Jud;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2749034
    const-string v0, ""

    .line 2749035
    iget-object v2, p0, LX/Jud;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2749036
    iget-object v0, p0, LX/Jud;->a:LX/3RG;

    iget-object v0, v0, LX/3RG;->c:Landroid/content/res/Resources;

    const v1, 0x7f0806a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2749037
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2749038
    const-string v0, "DefaultMessagingNotificationHandler"

    const-string v1, "Failed to get notification title from thread key"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749039
    iget-object v0, p0, LX/Jud;->a:LX/3RG;

    iget-object v0, v0, LX/3RG;->c:Landroid/content/res/Resources;

    invoke-static {v0}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 2749040
    :cond_1
    iget-object v1, p0, LX/Jud;->a:LX/3RG;

    iget-object v2, p0, LX/Jud;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/3RG;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2749041
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/Jud;->a:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v2, p0, LX/Jud;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-static {}, LX/10A;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 2749042
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2749043
    move-object v0, v0

    .line 2749044
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 2749045
    if-eqz p1, :cond_2

    .line 2749046
    iput-object p1, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2749047
    :cond_2
    iget-object v1, p0, LX/Jud;->a:LX/3RG;

    iget-object v1, v1, LX/3RG;->d:LX/3RK;

    iget-object v2, p0, LX/Jud;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x273a

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2749048
    iget-object v0, p0, LX/Jud;->e:LX/JuR;

    .line 2749049
    iget-object v1, v0, LX/JuR;->a:Lcom/facebook/messaging/notify/MessageReactionNotification;

    const/4 v2, 0x1

    .line 2749050
    iput-boolean v2, v1, Lcom/facebook/messaging/notify/MessageReactionNotification;->c:Z

    .line 2749051
    iget-object v0, p0, LX/Jud;->d:Lcom/facebook/messaging/notify/MessagingNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2749052
    return-void

    .line 2749053
    :cond_3
    if-eqz v1, :cond_0

    .line 2749054
    iget-object v0, p0, LX/Jud;->a:LX/3RG;

    iget-object v0, v0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2749055
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Jud;->a(Landroid/graphics/Bitmap;)V

    .line 2749056
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2749057
    const/4 v0, 0x0

    .line 2749058
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2749059
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2749060
    :cond_0
    invoke-direct {p0, v0}, LX/Jud;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2749061
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 2749062
    return-void

    .line 2749063
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
