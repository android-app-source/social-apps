.class public final LX/Jtv;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2747640
    const-class v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    const v0, 0x6ee7e77f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "NoteQuery"

    const-string v6, "07d8b1a49ba197bb5b2fdd7646577485"

    const-string v7, "node"

    const-string v8, "10155248169776729"

    const/4 v9, 0x0

    .line 2747641
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2747642
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2747643
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2747644
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2747645
    sparse-switch v0, :sswitch_data_0

    .line 2747646
    :goto_0
    return-object p1

    .line 2747647
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2747648
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2747649
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2747650
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2747651
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2747652
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2747653
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2747654
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2747655
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2747656
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2747657
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2747658
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2747659
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2747660
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2747661
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2747662
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2747663
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2747664
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2747665
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2747666
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x75243e2a -> :sswitch_9
        -0x493280e9 -> :sswitch_8
        -0x3df87b73 -> :sswitch_0
        -0x33f8633e -> :sswitch_13
        -0x2e03bc83 -> :sswitch_6
        -0x15afd767 -> :sswitch_a
        -0x1139a4c7 -> :sswitch_4
        -0x38aa96a -> :sswitch_e
        0x2ef6341 -> :sswitch_12
        0x683094a -> :sswitch_10
        0x956c6da -> :sswitch_5
        0x1f6fea44 -> :sswitch_2
        0x2e61b03a -> :sswitch_d
        0x45e5f0b4 -> :sswitch_b
        0x66c8fb9c -> :sswitch_c
        0x7191d8b1 -> :sswitch_11
        0x73a026b5 -> :sswitch_7
        0x78668257 -> :sswitch_f
        0x78a3267b -> :sswitch_3
        0x7e6ceb8b -> :sswitch_1
    .end sparse-switch
.end method
