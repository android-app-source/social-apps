.class public final enum LX/Jk2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jk2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jk2;

.field public static final enum EVENT_REMINDER:LX/Jk2;

.field public static final enum PLAN:LX/Jk2;

.field public static final enum REMINDER:LX/Jk2;

.field public static final enum REMINDER_PLAN:LX/Jk2;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2728703
    new-instance v0, LX/Jk2;

    const-string v1, "EVENT_REMINDER"

    invoke-direct {v0, v1, v2}, LX/Jk2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jk2;->EVENT_REMINDER:LX/Jk2;

    .line 2728704
    new-instance v0, LX/Jk2;

    const-string v1, "REMINDER"

    invoke-direct {v0, v1, v3}, LX/Jk2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jk2;->REMINDER:LX/Jk2;

    .line 2728705
    new-instance v0, LX/Jk2;

    const-string v1, "REMINDER_PLAN"

    invoke-direct {v0, v1, v4}, LX/Jk2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jk2;->REMINDER_PLAN:LX/Jk2;

    .line 2728706
    new-instance v0, LX/Jk2;

    const-string v1, "PLAN"

    invoke-direct {v0, v1, v5}, LX/Jk2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jk2;->PLAN:LX/Jk2;

    .line 2728707
    const/4 v0, 0x4

    new-array v0, v0, [LX/Jk2;

    sget-object v1, LX/Jk2;->EVENT_REMINDER:LX/Jk2;

    aput-object v1, v0, v2

    sget-object v1, LX/Jk2;->REMINDER:LX/Jk2;

    aput-object v1, v0, v3

    sget-object v1, LX/Jk2;->REMINDER_PLAN:LX/Jk2;

    aput-object v1, v0, v4

    sget-object v1, LX/Jk2;->PLAN:LX/Jk2;

    aput-object v1, v0, v5

    sput-object v0, LX/Jk2;->$VALUES:[LX/Jk2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2728708
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromValue(I)LX/Jk2;
    .locals 1

    .prologue
    .line 2728709
    packed-switch p0, :pswitch_data_0

    .line 2728710
    sget-object v0, LX/Jk2;->EVENT_REMINDER:LX/Jk2;

    :goto_0
    return-object v0

    .line 2728711
    :pswitch_0
    sget-object v0, LX/Jk2;->REMINDER:LX/Jk2;

    goto :goto_0

    .line 2728712
    :pswitch_1
    sget-object v0, LX/Jk2;->REMINDER_PLAN:LX/Jk2;

    goto :goto_0

    .line 2728713
    :pswitch_2
    sget-object v0, LX/Jk2;->PLAN:LX/Jk2;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jk2;
    .locals 1

    .prologue
    .line 2728714
    const-class v0, LX/Jk2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jk2;

    return-object v0
.end method

.method public static values()[LX/Jk2;
    .locals 1

    .prologue
    .line 2728715
    sget-object v0, LX/Jk2;->$VALUES:[LX/Jk2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jk2;

    return-object v0
.end method
