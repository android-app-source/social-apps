.class public final LX/Jny;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AMN;


# instance fields
.field public final synthetic a:LX/Jnz;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Jnz;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2734746
    iput-object p1, p0, LX/Jny;->a:LX/Jnz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734747
    iput-object p2, p0, LX/Jny;->b:Ljava/lang/String;

    .line 2734748
    iput-object p3, p0, LX/Jny;->c:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2734749
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/AN2;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;",
            "LX/AN2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2734742
    iget-object v1, p0, LX/Jny;->a:LX/Jnz;

    monitor-enter v1

    .line 2734743
    :try_start_0
    iget-object v0, p0, LX/Jny;->a:LX/Jnz;

    iget-object v0, v0, LX/Jnz;->a:Ljava/util/Map;

    iget-object v2, p0, LX/Jny;->b:Ljava/lang/String;

    sget-object v3, LX/Jnw;->COMPLETED:LX/Jnw;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734744
    iget-object v0, p0, LX/Jny;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v2, 0x0

    const v3, -0x135fe21d

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2734745
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2734738
    iget-object v1, p0, LX/Jny;->a:LX/Jnz;

    monitor-enter v1

    .line 2734739
    :try_start_0
    iget-object v0, p0, LX/Jny;->a:LX/Jnz;

    iget-object v0, v0, LX/Jnz;->a:Ljava/util/Map;

    iget-object v2, p0, LX/Jny;->b:Ljava/lang/String;

    sget-object v3, LX/Jnw;->NOT_STARTED:LX/Jnw;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734740
    iget-object v0, p0, LX/Jny;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2734741
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
