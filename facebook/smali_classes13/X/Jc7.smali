.class public final LX/Jc7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/8DZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/preference/Preference;

.field public final synthetic b:Landroid/preference/PreferenceCategory;

.field public final synthetic c:Lcom/facebook/katana/settings/activity/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/settings/activity/SettingsActivity;Landroid/preference/Preference;Landroid/preference/PreferenceCategory;)V
    .locals 0

    .prologue
    .line 2717408
    iput-object p1, p0, LX/Jc7;->c:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iput-object p2, p0, LX/Jc7;->a:Landroid/preference/Preference;

    iput-object p3, p0, LX/Jc7;->b:Landroid/preference/PreferenceCategory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/8DZ;)V
    .locals 2
    .param p1    # LX/8DZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2717403
    iget-boolean v0, p1, LX/8DZ;->c:Z

    move v0, v0

    .line 2717404
    if-eqz v0, :cond_0

    .line 2717405
    iget-object v0, p0, LX/Jc7;->a:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 2717406
    :goto_0
    return-void

    .line 2717407
    :cond_0
    iget-object v0, p0, LX/Jc7;->b:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, LX/Jc7;->a:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2717400
    iget-object v0, p0, LX/Jc7;->b:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, LX/Jc7;->a:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 2717401
    iget-object v0, p0, LX/Jc7;->c:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->z:LX/03V;

    const-string v1, "FIRST_PARTY_SETTINGS"

    const-string v2, "Failed to fetch first party settings."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2717402
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2717399
    check-cast p1, LX/8DZ;

    invoke-direct {p0, p1}, LX/Jc7;->a(LX/8DZ;)V

    return-void
.end method
