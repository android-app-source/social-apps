.class public LX/K0Y;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/5pQ;


# instance fields
.field private a:LX/K0X;

.field private b:Landroid/app/Dialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field public f:Landroid/content/DialogInterface$OnShowListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/K0T;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2758922
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    move-object v0, p1

    .line 2758923
    check-cast v0, LX/5pX;

    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2758924
    new-instance v0, LX/K0X;

    invoke-direct {v0, p1}, LX/K0X;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/K0Y;->a:LX/K0X;

    .line 2758925
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2758926
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 2758927
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2758928
    const/4 v0, 0x0

    iput-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    .line 2758929
    iget-object v0, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v0}, LX/K0X;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2758930
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2758931
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2758932
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    const-string v1, "mDialog must exist when we call updateProperties"

    invoke-static {v0, v1}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2758933
    iget-boolean v0, p0, LX/K0Y;->c:Z

    if-eqz v0, :cond_0

    .line 2758934
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 2758935
    :goto_0
    return-void

    .line 2758936
    :cond_0
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/Window;->setDimAmount(F)V

    .line 2758937
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0
.end method

.method private getContentView()Landroid/view/View;
    .locals 2

    .prologue
    .line 2758938
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, LX/K0Y;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2758939
    iget-object v1, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2758940
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setFitsSystemWindows(Z)V

    .line 2758941
    return-object v0
.end method


# virtual methods
.method public final addChildrenForAccessibility(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2758942
    return-void
.end method

.method public final addView(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2758952
    iget-object v0, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v0, p1, p2}, LX/K0X;->addView(Landroid/view/View;I)V

    .line 2758953
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2758943
    invoke-virtual {p0}, LX/K0Y;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    invoke-virtual {v0, p0}, LX/5pX;->b(LX/5pQ;)V

    .line 2758944
    invoke-direct {p0}, LX/K0Y;->f()V

    .line 2758945
    return-void
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2758946
    invoke-virtual {p0}, LX/K0Y;->d()V

    .line 2758947
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2758948
    invoke-direct {p0}, LX/K0Y;->f()V

    .line 2758949
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2758950
    invoke-virtual {p0}, LX/K0Y;->b()V

    .line 2758951
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2758904
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 2758905
    iget-boolean v0, p0, LX/K0Y;->e:Z

    if-eqz v0, :cond_2

    .line 2758906
    invoke-direct {p0}, LX/K0Y;->f()V

    .line 2758907
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K0Y;->e:Z

    .line 2758908
    const v0, 0x7f0e0c2c

    .line 2758909
    iget-object v1, p0, LX/K0Y;->d:Ljava/lang/String;

    const-string v2, "fade"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2758910
    const v0, 0x7f0e0c2e

    .line 2758911
    :cond_1
    :goto_0
    new-instance v1, Landroid/app/Dialog;

    invoke-virtual {p0}, LX/K0Y;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/K0Y;->b:Landroid/app/Dialog;

    .line 2758912
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-direct {p0}, LX/K0Y;->getContentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 2758913
    invoke-direct {p0}, LX/K0Y;->g()V

    .line 2758914
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    iget-object v1, p0, LX/K0Y;->f:Landroid/content/DialogInterface$OnShowListener;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 2758915
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    new-instance v1, LX/K0W;

    invoke-direct {v1, p0}, LX/K0W;-><init>(LX/K0Y;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2758916
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2758917
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2758918
    :goto_1
    return-void

    .line 2758919
    :cond_2
    invoke-direct {p0}, LX/K0Y;->g()V

    goto :goto_1

    .line 2758920
    :cond_3
    iget-object v1, p0, LX/K0Y;->d:Ljava/lang/String;

    const-string v2, "slide"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2758921
    const v0, 0x7f0e0c2d

    goto :goto_0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 2758903
    const/4 v0, 0x0

    return v0
.end method

.method public final getChildAt(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2758885
    iget-object v0, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v0, p1}, LX/K0X;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildCount()I
    .locals 1

    .prologue
    .line 2758902
    iget-object v0, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v0}, LX/K0X;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getDialog()Landroid/app/Dialog;
    .locals 1
    .annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2758901
    iget-object v0, p0, LX/K0Y;->b:Landroid/app/Dialog;

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2758900
    return-void
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2758898
    iget-object v0, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v0, p1}, LX/K0X;->removeView(Landroid/view/View;)V

    .line 2758899
    return-void
.end method

.method public final removeViewAt(I)V
    .locals 2

    .prologue
    .line 2758895
    invoke-virtual {p0, p1}, LX/K0Y;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2758896
    iget-object v1, p0, LX/K0Y;->a:LX/K0X;

    invoke-virtual {v1, v0}, LX/K0X;->removeView(Landroid/view/View;)V

    .line 2758897
    return-void
.end method

.method public setAnimationType(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2758892
    iput-object p1, p0, LX/K0Y;->d:Ljava/lang/String;

    .line 2758893
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0Y;->e:Z

    .line 2758894
    return-void
.end method

.method public setOnRequestCloseListener(LX/K0T;)V
    .locals 0

    .prologue
    .line 2758890
    iput-object p1, p0, LX/K0Y;->g:LX/K0T;

    .line 2758891
    return-void
.end method

.method public setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V
    .locals 0

    .prologue
    .line 2758888
    iput-object p1, p0, LX/K0Y;->f:Landroid/content/DialogInterface$OnShowListener;

    .line 2758889
    return-void
.end method

.method public setTransparent(Z)V
    .locals 0

    .prologue
    .line 2758886
    iput-boolean p1, p0, LX/K0Y;->c:Z

    .line 2758887
    return-void
.end method
