.class public LX/Jyy;
.super LX/Jyx;
.source ""


# instance fields
.field private final g:LX/JzS;

.field private final h:[I


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 4

    .prologue
    .line 2755625
    invoke-direct {p0}, LX/Jyx;-><init>()V

    .line 2755626
    iput-object p2, p0, LX/Jyy;->g:LX/JzS;

    .line 2755627
    const-string v0, "input"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    .line 2755628
    invoke-interface {v1}, LX/5pC;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Jyy;->h:[I

    .line 2755629
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/Jyy;->h:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2755630
    iget-object v2, p0, LX/Jyy;->h:[I

    invoke-interface {v1, v0}, LX/5pC;->getInt(I)I

    move-result v3

    aput v3, v2, v0

    .line 2755631
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2755632
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2755633
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Jyy;->e:D

    .line 2755634
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Jyy;->h:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2755635
    iget-object v0, p0, LX/Jyy;->g:LX/JzS;

    iget-object v2, p0, LX/Jyy;->h:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v0

    .line 2755636
    if-eqz v0, :cond_0

    instance-of v2, v0, LX/Jyx;

    if-eqz v2, :cond_0

    .line 2755637
    iget-wide v2, p0, LX/Jyx;->e:D

    check-cast v0, LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->b()D

    move-result-wide v4

    add-double/2addr v2, v4

    iput-wide v2, p0, LX/Jyy;->e:D

    .line 2755638
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2755639
    :cond_0
    new-instance v0, LX/5p9;

    const-string v1, "Illegal node ID set as an input for Animated.Add node"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755640
    :cond_1
    return-void
.end method
