.class public final LX/Je2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/user/model/User;

.field public final synthetic b:LX/Je3;


# direct methods
.method public constructor <init>(LX/Je3;Lcom/facebook/user/model/User;)V
    .locals 0

    .prologue
    .line 2720268
    iput-object p1, p0, LX/Je2;->b:LX/Je3;

    iput-object p2, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x2

    const v0, 0x236f25cc

    invoke-static {v4, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2720269
    iget-object v1, p0, LX/Je2;->b:LX/Je3;

    iget-object v1, v1, LX/Je3;->c:LX/JeB;

    iget-object v1, v1, LX/JeB;->c:LX/JeW;

    iget-object v2, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    .line 2720270
    iget-object p1, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2720271
    invoke-virtual {v1, v2}, LX/JeW;->a(Ljava/lang/String;)V

    .line 2720272
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    .line 2720273
    iget-object v2, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1, v2}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    .line 2720274
    iput-boolean v3, v1, LX/0XI;->Q:Z

    .line 2720275
    iget-object v2, p0, LX/Je2;->b:LX/Je3;

    iget-object v2, v2, LX/Je3;->b:LX/Jdn;

    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/Jdn;->a(Lcom/facebook/user/model/User;)V

    .line 2720276
    iget-object v1, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    .line 2720277
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2720278
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    .line 2720279
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->ab:Z

    move v1, v2

    .line 2720280
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Je2;->b:LX/Je3;

    iget-object v1, v1, LX/Je3;->c:LX/JeB;

    iget-object v1, v1, LX/JeB;->f:LX/JfY;

    const/4 v2, 0x0

    .line 2720281
    iget-object v3, v1, LX/JfY;->a:LX/0Uh;

    const/16 v5, 0x1a6

    const/4 p1, 0x0

    invoke-virtual {v3, v5, p1}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v3, v3

    .line 2720282
    if-eqz v3, :cond_0

    iget-object v3, v1, LX/JfY;->a:LX/0Uh;

    const/16 v5, 0x18f

    invoke-virtual {v3, v5, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 2720283
    if-eqz v1, :cond_1

    .line 2720284
    iget-object v1, p0, LX/Je2;->b:LX/Je3;

    iget-object v1, v1, LX/Je3;->c:LX/JeB;

    iget-object v2, p0, LX/Je2;->b:LX/Je3;

    iget-object v2, v2, LX/Je3;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Je2;->a:Lcom/facebook/user/model/User;

    const/4 p1, 0x1

    .line 2720285
    new-instance v5, LX/31Y;

    invoke-direct {v5, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v6, 0x7f083bf0

    invoke-virtual {v5, v6}, LX/0ju;->a(I)LX/0ju;

    move-result-object v5

    const v6, 0x7f083bf8

    new-array v7, p1, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v5

    const v6, 0x7f083bf0

    new-instance v7, LX/JeA;

    invoke-direct {v7, v1, v2, v3}, LX/JeA;-><init>(LX/JeB;Landroid/content/Context;Lcom/facebook/user/model/User;)V

    invoke-virtual {v5, v6, v7}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const v6, 0x7f083bf9

    new-instance v7, LX/Je9;

    invoke-direct {v7, v1}, LX/Je9;-><init>(LX/JeB;)V

    invoke-virtual {v5, v6, v7}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->b()LX/2EJ;

    .line 2720286
    :cond_1
    const v1, 0x7fc125f8

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
