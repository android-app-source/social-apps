.class public final LX/K18;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/K19;


# direct methods
.method public constructor <init>(LX/K19;)V
    .locals 0

    .prologue
    .line 2761161
    iput-object p1, p0, LX/K18;->a:LX/K19;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/K19;B)V
    .locals 0

    .prologue
    .line 2761162
    invoke-direct {p0, p1}, LX/K18;-><init>(LX/K19;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2761163
    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-boolean v0, v0, LX/K19;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-object v0, v0, LX/K19;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2761164
    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-object v0, v0, LX/K19;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 2761165
    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    goto :goto_0

    .line 2761166
    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2761167
    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-boolean v0, v0, LX/K19;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-object v0, v0, LX/K19;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2761168
    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-object v0, v0, LX/K19;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 2761169
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 2761170
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2761171
    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-boolean v0, v0, LX/K19;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-object v0, v0, LX/K19;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2761172
    iget-object v0, p0, LX/K18;->a:LX/K19;

    iget-object v0, v0, LX/K19;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 2761173
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 2761174
    :cond_0
    return-void
.end method
