.class public LX/K9H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# instance fields
.field public a:LX/K9F;

.field public final b:LX/0tX;

.field public final c:LX/K9J;

.field public final d:LX/1Ck;

.field public e:Z

.field public f:Lcom/facebook/common/callercontext/CallerContext;

.field public g:Lcom/facebook/timeline/videos/TimelineVideosTabFragment;

.field public h:J


# direct methods
.method public constructor <init>(LX/0tX;LX/K9J;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2776884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776885
    iput-object p1, p0, LX/K9H;->b:LX/0tX;

    .line 2776886
    iput-object p2, p0, LX/K9H;->c:LX/K9J;

    .line 2776887
    iput-object p3, p0, LX/K9H;->d:LX/1Ck;

    .line 2776888
    return-void
.end method

.method public static a(LX/K9H;Z)V
    .locals 11

    .prologue
    .line 2776889
    iget-boolean v0, p0, LX/K9H;->e:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, LX/K9H;->a:LX/K9F;

    .line 2776890
    iget-object v1, v0, LX/K9F;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 2776891
    if-nez v0, :cond_1

    .line 2776892
    :cond_0
    :goto_1
    return-void

    .line 2776893
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K9H;->e:Z

    .line 2776894
    iget-object v0, p0, LX/K9H;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_timeline_videos"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/K9H;->a:LX/K9F;

    invoke-virtual {v2}, LX/K9F;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    .line 2776895
    if-eqz p1, :cond_3

    .line 2776896
    new-instance v5, LX/Fsp;

    iget-wide v8, p0, LX/K9H;->h:J

    const/4 v4, 0x0

    invoke-direct {v5, v8, v9, v4, v7}, LX/Fsp;-><init>(JLjava/lang/String;Z)V

    .line 2776897
    :goto_2
    iget-object v10, p0, LX/K9H;->b:LX/0tX;

    iget-object v4, p0, LX/K9H;->c:LX/K9J;

    sget-object v6, LX/0zS;->c:LX/0zS;

    iget-object v8, p0, LX/K9H;->f:Lcom/facebook/common/callercontext/CallerContext;

    const/16 v9, 0x8

    invoke-virtual/range {v4 .. v9}, LX/K9J;->a(LX/Fsp;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zO;

    move-result-object v4

    invoke-virtual {v10, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2776898
    move-object v2, v4

    .line 2776899
    new-instance v3, LX/K9G;

    invoke-direct {v3, p0}, LX/K9G;-><init>(LX/K9H;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, LX/K9F;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v1

    goto :goto_0

    .line 2776900
    :cond_3
    new-instance v5, LX/Fsp;

    iget-wide v8, p0, LX/K9H;->h:J

    iget-object v4, p0, LX/K9H;->a:LX/K9F;

    invoke-virtual {v4}, LX/K9F;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v8, v9, v4, v7}, LX/Fsp;-><init>(JLjava/lang/String;Z)V

    goto :goto_2
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2776901
    const/4 v0, 0x0

    iput-object v0, p0, LX/K9H;->a:LX/K9F;

    .line 2776902
    return-void
.end method
