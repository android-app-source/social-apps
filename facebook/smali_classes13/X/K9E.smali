.class public LX/K9E;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0Ya;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/K9D;",
        ">;",
        "LX/0Ya;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K91;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/K7K;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field public final d:LX/K91;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;Ljava/util/List;LX/K91;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView;",
            "Ljava/util/List",
            "<",
            "LX/K7K;",
            ">;",
            "LX/K91;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2776762
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2776763
    const-class v0, LX/K9E;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/K9E;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2776764
    iput-object p2, p0, LX/K9E;->b:Ljava/util/List;

    .line 2776765
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    iput v0, p0, LX/K9E;->c:I

    .line 2776766
    iput-object p3, p0, LX/K9E;->d:LX/K91;

    .line 2776767
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, LX/K9E;

    const/16 p0, 0x35f8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p1, LX/K9E;->a:LX/0Ot;

    return-void
.end method

.method public static a$redex0(LX/K9E;LX/K7K;ZLcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;)V
    .locals 6

    .prologue
    .line 2776768
    iget-object v0, p0, LX/K9E;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K91;

    .line 2776769
    invoke-virtual {p1}, LX/K7K;->a()Ljava/lang/String;

    move-result-object v2

    .line 2776770
    invoke-virtual {p3}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2776771
    new-instance v4, LX/31Y;

    invoke-direct {v4, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    if-eqz p2, :cond_1

    .line 2776772
    iget-object v1, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v5, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_TITLE:LX/K8y;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K8z;

    invoke-virtual {v1}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2776773
    :goto_0
    invoke-virtual {v4, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    if-eqz p2, :cond_2

    .line 2776774
    iget-object v1, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v5, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_DESCRIPTION:LX/K8y;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K8z;

    invoke-virtual {v1, v2}, LX/K8z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2776775
    :goto_1
    invoke-virtual {v4, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    if-eqz p2, :cond_3

    .line 2776776
    iget-object v2, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v4, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_PRIMARY_BUTTON:LX/K8y;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8z;

    invoke-virtual {v2}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2776777
    :goto_2
    new-instance v2, LX/K9A;

    invoke-direct {v2, p0, p1, p2}, LX/K9A;-><init>(LX/K9E;LX/K7K;Z)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/K99;

    invoke-direct {v2, p0}, LX/K99;-><init>(LX/K9E;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/K98;

    invoke-direct {v1, p0, p3, p2, p1}, LX/K98;-><init>(LX/K9E;Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;ZLX/K7K;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2776778
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2776779
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v0

    .line 2776780
    if-eqz v0, :cond_0

    .line 2776781
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 2776782
    :cond_0
    return-void

    .line 2776783
    :cond_1
    iget-object v1, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v5, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_TITLE:LX/K8y;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K8z;

    invoke-virtual {v1}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2776784
    goto :goto_0

    .line 2776785
    :cond_2
    iget-object v1, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v5, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_DESCRIPTION:LX/K8y;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K8z;

    invoke-virtual {v1, v2}, LX/K8z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2776786
    goto :goto_1

    .line 2776787
    :cond_3
    iget-object v2, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v4, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_PRIMARY_BUTTON:LX/K8y;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8z;

    invoke-virtual {v2}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2776788
    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2776789
    const/4 v3, 0x0

    .line 2776790
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2776791
    invoke-static {}, LX/K9C;->values()[LX/K9C;

    move-result-object v1

    aget-object v1, v1, p2

    .line 2776792
    sget-object v2, LX/K9B;->a:[I

    invoke-virtual {v1}, LX/K9C;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2776793
    const v1, 0x7f031411

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    .line 2776794
    :goto_0
    new-instance v1, LX/K9D;

    invoke-direct {v1, v0}, LX/K9D;-><init>(Landroid/view/View;)V

    .line 2776795
    return-object v1

    .line 2776796
    :pswitch_0
    const v1, 0x7f031410

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2776797
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, LX/K9E;->c:I

    div-int/lit8 v3, v3, 0x4

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2776798
    if-nez v0, :cond_0

    .line 2776799
    :goto_1
    goto :goto_0

    .line 2776800
    :cond_0
    const v1, 0x7f0d2e14

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2776801
    iget-object v2, p0, LX/K9E;->d:LX/K91;

    .line 2776802
    iget-object v3, v2, LX/K91;->e:Ljava/util/Map;

    sget-object p1, LX/K8y;->MANAGE_SCREEN_TITLE:LX/K8y;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/K8z;

    invoke-virtual {v3}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2776803
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2776804
    const v1, 0x7f0d2e15

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2776805
    iget-object v2, p0, LX/K9E;->d:LX/K91;

    .line 2776806
    iget-object v3, v2, LX/K91;->e:Ljava/util/Map;

    sget-object p1, LX/K8y;->MANAGE_SCREEN_DESCRIPTION:LX/K8y;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/K8z;

    invoke-virtual {v3}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2776807
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2776808
    check-cast p1, LX/K9D;

    .line 2776809
    if-eqz p2, :cond_1

    .line 2776810
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    .line 2776811
    iget-object v1, p0, LX/K9E;->b:Ljava/util/List;

    add-int/lit8 v2, p2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K7K;

    .line 2776812
    invoke-virtual {v0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->a()V

    .line 2776813
    invoke-virtual {v1}, LX/K7K;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setName(Ljava/lang/String;)V

    .line 2776814
    invoke-virtual {v1}, LX/K7K;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setSwitchEnabled(Z)V

    .line 2776815
    iget-object v2, v1, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    .line 2776816
    invoke-virtual {v0, v2}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setLogoUri(Ljava/lang/String;)V

    .line 2776817
    new-instance v2, LX/K96;

    invoke-direct {v2, p0, v1, v0}, LX/K96;-><init>(LX/K9E;LX/K7K;Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setSwitchStateChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2776818
    :cond_1
    return-void

    :cond_2
    iget-object v2, v1, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2776819
    if-nez p1, :cond_0

    .line 2776820
    sget-object v0, LX/K9C;->Header:LX/K9C;

    invoke-virtual {v0}, LX/K9C;->ordinal()I

    move-result v0

    .line 2776821
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/K9C;->PublisherSubscriptionInfo:LX/K9C;

    invoke-virtual {v0}, LX/K9C;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2776822
    iget-object v0, p0, LX/K9E;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
