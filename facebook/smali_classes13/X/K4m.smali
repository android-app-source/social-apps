.class public final LX/K4m;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/K4o;


# direct methods
.method public constructor <init>(LX/K4o;)V
    .locals 0

    .prologue
    .line 2769283
    iput-object p1, p0, LX/K4m;->a:LX/K4o;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 2769284
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2769285
    :cond_0
    :goto_0
    return-void

    .line 2769286
    :pswitch_0
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->EXPORTING:LX/K4l;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->i:LX/K42;

    if-eqz v0, :cond_0

    .line 2769287
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    iget-object v1, v0, LX/K4o;->i:LX/K42;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, LX/K42;->b(F)V

    goto :goto_0

    .line 2769288
    :pswitch_1
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    const/4 v1, 0x0

    .line 2769289
    iput-object v1, v0, LX/K4o;->q:LX/K3c;

    .line 2769290
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->EXPORTING:LX/K4l;

    if-ne v0, v1, :cond_1

    .line 2769291
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    sget-object v1, LX/K4l;->READY_TO_PLAY:LX/K4l;

    invoke-static {v0, v1}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2769292
    :cond_1
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->i:LX/K42;

    if-eqz v0, :cond_0

    .line 2769293
    iget-object v0, p0, LX/K4m;->a:LX/K4o;

    iget-object v1, v0, LX/K4o;->i:LX/K42;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/K3g;

    invoke-virtual {v1, v0}, LX/K42;->a(LX/K3g;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
