.class public LX/K7P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/K7P;


# instance fields
.field public final a:LX/K87;

.field public final b:LX/K6k;

.field public final c:LX/K7N;

.field public d:Z


# direct methods
.method public constructor <init>(LX/K87;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2773111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773112
    new-instance v0, LX/K7M;

    invoke-direct {v0, p0}, LX/K7M;-><init>(LX/K7P;)V

    iput-object v0, p0, LX/K7P;->b:LX/K6k;

    .line 2773113
    new-instance v0, LX/K7O;

    invoke-direct {v0, p0}, LX/K7O;-><init>(LX/K7P;)V

    iput-object v0, p0, LX/K7P;->c:LX/K7N;

    .line 2773114
    iput-object p1, p0, LX/K7P;->a:LX/K87;

    .line 2773115
    return-void
.end method

.method public static a(LX/0QB;)LX/K7P;
    .locals 4

    .prologue
    .line 2773116
    sget-object v0, LX/K7P;->e:LX/K7P;

    if-nez v0, :cond_1

    .line 2773117
    const-class v1, LX/K7P;

    monitor-enter v1

    .line 2773118
    :try_start_0
    sget-object v0, LX/K7P;->e:LX/K7P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2773119
    if-eqz v2, :cond_0

    .line 2773120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2773121
    new-instance p0, LX/K7P;

    invoke-static {v0}, LX/K87;->a(LX/0QB;)LX/K87;

    move-result-object v3

    check-cast v3, LX/K87;

    invoke-direct {p0, v3}, LX/K7P;-><init>(LX/K87;)V

    .line 2773122
    move-object v0, p0

    .line 2773123
    sput-object v0, LX/K7P;->e:LX/K7P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2773124
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2773125
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2773126
    :cond_1
    sget-object v0, LX/K7P;->e:LX/K7P;

    return-object v0

    .line 2773127
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2773128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
