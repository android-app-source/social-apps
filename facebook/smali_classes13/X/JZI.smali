.class public LX/JZI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JZG;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2708291
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JZI;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708292
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2708293
    iput-object p1, p0, LX/JZI;->b:LX/0Ot;

    .line 2708294
    return-void
.end method

.method public static a(LX/0QB;)LX/JZI;
    .locals 4

    .prologue
    .line 2708236
    const-class v1, LX/JZI;

    monitor-enter v1

    .line 2708237
    :try_start_0
    sget-object v0, LX/JZI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708238
    sput-object v2, LX/JZI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708239
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708240
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708241
    new-instance v3, LX/JZI;

    const/16 p0, 0x21e0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JZI;-><init>(LX/0Ot;)V

    .line 2708242
    move-object v0, v3

    .line 2708243
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708244
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708245
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708246
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2708289
    invoke-static {}, LX/1dS;->b()V

    .line 2708290
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2708295
    iget-object v0, p0, LX/JZI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2708296
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    move-object v0, v0

    .line 2708297
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 12

    .prologue
    .line 2708255
    check-cast p2, LX/JZH;

    .line 2708256
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2708257
    iget-object v0, p0, LX/JZI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;

    iget-object v2, p2, LX/JZH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/JZH;->c:Ljava/lang/String;

    .line 2708258
    iget-object v4, v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->d:LX/0Uh;

    const/16 v5, 0x75

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    .line 2708259
    if-nez v4, :cond_1

    .line 2708260
    :cond_0
    :goto_0
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2708261
    check-cast v0, LX/K8D;

    iput-object v0, p2, LX/JZH;->d:LX/K8D;

    .line 2708262
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2708263
    return-void

    .line 2708264
    :cond_1
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2708265
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2708266
    invoke-static {v4}, LX/JZE;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2708267
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2708268
    iget-object v5, v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->c:LX/K8G;

    iget-object v6, v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->b:Landroid/content/Context;

    sget-object v7, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v2, 0x1

    .line 2708269
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2708270
    if-nez v4, :cond_4

    move-object v8, v9

    .line 2708271
    :goto_1
    move-object v10, v8

    .line 2708272
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2708273
    new-instance v8, LX/K8E;

    invoke-direct {v8, v5}, LX/K8E;-><init>(LX/K8G;)V

    .line 2708274
    :goto_2
    move-object v4, v8

    .line 2708275
    iput-object v4, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2708276
    goto :goto_0

    .line 2708277
    :cond_2
    invoke-static {v6, v4, v3, v2}, LX/K8G;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Z)LX/K8B;

    move-result-object v11

    .line 2708278
    new-instance v9, LX/K8F;

    invoke-direct {v9, v5}, LX/K8F;-><init>(LX/K8G;)V

    .line 2708279
    const/4 v8, 0x0

    :goto_3
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result p0

    if-ge v8, p0, :cond_3

    .line 2708280
    iget-object p0, v5, LX/K8G;->h:LX/0QI;

    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p0, p1, v0}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2708281
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 2708282
    :cond_3
    new-instance v8, LX/K8C;

    invoke-direct {v8, v5, v10, v9, v7}, LX/K8C;-><init>(LX/K8G;Ljava/util/List;LX/K8F;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v5, v11, v8}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    move-object v8, v9

    .line 2708283
    goto :goto_2

    .line 2708284
    :cond_4
    const/4 v8, 0x0

    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    if-ge v8, v10, :cond_6

    .line 2708285
    iget-object v10, v5, LX/K8G;->h:LX/0QI;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v10, v11}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-nez v10, :cond_5

    .line 2708286
    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2708287
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_6
    move-object v8, v9

    .line 2708288
    goto :goto_1
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2708254
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 2708248
    check-cast p3, LX/JZH;

    .line 2708249
    iget-object v0, p0, LX/JZI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;

    iget-object v1, p3, LX/JZH;->d:LX/K8D;

    .line 2708250
    iget-object p0, v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->d:LX/0Uh;

    const/16 p1, 0x75

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result p0

    .line 2708251
    if-eqz p0, :cond_0

    if-eqz v1, :cond_0

    .line 2708252
    invoke-interface {v1}, LX/K8D;->a()V

    .line 2708253
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2708247
    const/16 v0, 0xf

    return v0
.end method
