.class public LX/JmG;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2732571
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2732572
    invoke-direct {p0}, LX/JmG;->a()V

    .line 2732573
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732568
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2732569
    invoke-direct {p0}, LX/JmG;->a()V

    .line 2732570
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732565
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732566
    invoke-direct {p0}, LX/JmG;->a()V

    .line 2732567
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2732559
    const v0, 0x7f03077a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2732560
    const v0, 0x7f0d13fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/JmG;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2732561
    const v0, 0x7f0d13fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JmG;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2732562
    const v0, 0x7f0d13ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JmG;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2732563
    const v0, 0x7f0d1400

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JmG;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2732564
    return-void
.end method
