.class public final LX/K1K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/react/views/textinput/ReactTextInputManager;

.field private b:LX/5s9;

.field private c:LX/K19;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/5pX;LX/K19;)V
    .locals 1

    .prologue
    .line 2761705
    iput-object p1, p0, LX/K1K;->a:Lcom/facebook/react/views/textinput/ReactTextInputManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2761706
    const-class v0, LX/5rQ;

    invoke-virtual {p2, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2761707
    iget-object p1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, p1

    .line 2761708
    iput-object v0, p0, LX/K1K;->b:LX/5s9;

    .line 2761709
    iput-object p3, p0, LX/K1K;->c:LX/K19;

    .line 2761710
    const/4 v0, 0x0

    iput-object v0, p0, LX/K1K;->d:Ljava/lang/String;

    .line 2761711
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2761712
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2761713
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/K1K;->d:Ljava/lang/String;

    .line 2761714
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 9

    .prologue
    .line 2761715
    if-nez p4, :cond_1

    if-nez p3, :cond_1

    .line 2761716
    :cond_0
    :goto_0
    return-void

    .line 2761717
    :cond_1
    iget-object v0, p0, LX/K1K;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761718
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    add-int v1, p2, p4

    invoke-virtual {v0, p2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 2761719
    iget-object v0, p0, LX/K1K;->d:Ljava/lang/String;

    add-int v1, p2, p3

    invoke-virtual {v0, p2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 2761720
    if-ne p4, p3, :cond_2

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2761721
    :cond_2
    iget-object v0, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v0}, LX/K19;->getWidth()I

    move-result v1

    .line 2761722
    iget-object v0, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v0}, LX/K19;->getHeight()I

    move-result v0

    .line 2761723
    iget-object v2, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v2}, LX/K19;->getLayout()Landroid/text/Layout;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2761724
    iget-object v0, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v0}, LX/K19;->getCompoundPaddingLeft()I

    move-result v0

    iget-object v1, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v1}, LX/K19;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v1}, LX/K19;->getCompoundPaddingRight()I

    move-result v1

    add-int/2addr v1, v0

    .line 2761725
    iget-object v0, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v0}, LX/K19;->getCompoundPaddingTop()I

    move-result v0

    iget-object v2, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v2}, LX/K19;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v2}, LX/K19;->getCompoundPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    move v4, v0

    move v3, v1

    .line 2761726
    :goto_1
    iget-object v8, p0, LX/K1K;->b:LX/5s9;

    new-instance v0, LX/K1A;

    iget-object v1, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v1}, LX/K19;->getId()I

    move-result v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    int-to-float v4, v4

    invoke-static {v4}, LX/5r2;->c(F)F

    move-result v4

    iget-object v5, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v5}, LX/K19;->d()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/K1A;-><init>(ILjava/lang/String;FFI)V

    invoke-virtual {v8, v0}, LX/5s9;->a(LX/5r0;)V

    .line 2761727
    iget-object v8, p0, LX/K1K;->b:LX/5s9;

    new-instance v0, LX/K1D;

    iget-object v1, p0, LX/K1K;->c:LX/K19;

    invoke-virtual {v1}, LX/K19;->getId()I

    move-result v1

    add-int v5, p2, p3

    move-object v2, v6

    move-object v3, v7

    move v4, p2

    invoke-direct/range {v0 .. v5}, LX/K1D;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v8, v0}, LX/5s9;->a(LX/5r0;)V

    goto/16 :goto_0

    :cond_3
    move v4, v0

    move v3, v1

    goto :goto_1
.end method
