.class public LX/Jdl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field public b:Lcom/facebook/user/model/User;

.field public c:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/1CX;

.field public e:LX/0gc;

.field public f:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;

.field public j:Lcom/facebook/messaging/blocking/BlockingUtils;

.field public k:LX/JdU;

.field public l:LX/JfM;

.field public m:LX/IZK;

.field public n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0ad;

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/JfN;

.field private final r:LX/0Uh;

.field private final s:LX/26j;

.field public final t:LX/Js1;

.field private final u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2719985
    const-string v0, "https://m.facebook.com/privacy/touch/block/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/Jdl;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(LX/1CX;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/messaging/blocking/BlockingUtils;LX/JdU;LX/0Or;LX/JfM;LX/IZK;LX/0ad;LX/0Ot;LX/JfN;LX/0Uh;LX/26j;LX/Js1;Ljava/lang/Boolean;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
        .end annotation
    .end param
    .param p14    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1CX;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/messaging/blocking/BlockingUtils;",
            "LX/JdU;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/JfM;",
            "LX/IZK;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;",
            "LX/JfN;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/26j;",
            "LX/Js1;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2719986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2719987
    iput-object p1, p0, LX/Jdl;->d:LX/1CX;

    .line 2719988
    iput-object p2, p0, LX/Jdl;->i:Lcom/facebook/content/SecureContextHelper;

    .line 2719989
    iput-object p3, p0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    .line 2719990
    iput-object p4, p0, LX/Jdl;->k:LX/JdU;

    .line 2719991
    iput-object p5, p0, LX/Jdl;->n:LX/0Or;

    .line 2719992
    iput-object p6, p0, LX/Jdl;->l:LX/JfM;

    .line 2719993
    iput-object p7, p0, LX/Jdl;->m:LX/IZK;

    .line 2719994
    iput-object p8, p0, LX/Jdl;->o:LX/0ad;

    .line 2719995
    iput-object p9, p0, LX/Jdl;->p:LX/0Ot;

    .line 2719996
    iput-object p10, p0, LX/Jdl;->q:LX/JfN;

    .line 2719997
    iput-object p11, p0, LX/Jdl;->r:LX/0Uh;

    .line 2719998
    iput-object p12, p0, LX/Jdl;->s:LX/26j;

    .line 2719999
    iput-object p13, p0, LX/Jdl;->t:LX/Js1;

    .line 2720000
    invoke-virtual {p14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Jdl;->u:Z

    .line 2720001
    return-void
.end method

.method private a(LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1

    .prologue
    .line 2720002
    new-instance v0, LX/Jde;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Jde;-><init>(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V

    return-object v0
.end method

.method public static a(Landroid/view/View;LX/Jdk;)Lcom/facebook/widget/BetterSwitch;
    .locals 2

    .prologue
    .line 2720003
    sget-object v0, LX/Jdj;->a:[I

    invoke-virtual {p1}, LX/Jdk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2720004
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Block row type not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2720005
    :pswitch_0
    const v0, 0x7f0d0706

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/BetterSwitch;

    .line 2720006
    :goto_0
    return-object v0

    .line 2720007
    :pswitch_1
    const v0, 0x7f0d0717

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/BetterSwitch;

    goto :goto_0

    .line 2720008
    :pswitch_2
    const v0, 0x7f0d071c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/BetterSwitch;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;LX/Jdk;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2720009
    sget-object v0, LX/Jdj;->a:[I

    invoke-virtual {p1}, LX/Jdk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2720010
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Block row type not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2720011
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b1d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2720012
    :goto_0
    return-object v0

    .line 2720013
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b1a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2720014
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b1b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/Jdl;LX/4ob;LX/Jdk;)V
    .locals 2

    .prologue
    .line 2720015
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2720016
    invoke-direct {p0, p2}, LX/Jdl;->a(LX/Jdk;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2720017
    invoke-virtual {p1}, LX/4ob;->d()V

    .line 2720018
    :goto_0
    return-void

    .line 2720019
    :cond_0
    invoke-virtual {p1}, LX/4ob;->e()V

    .line 2720020
    invoke-virtual {p1}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, LX/4ob;->a()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p2}, LX/Jdl;->a(Landroid/view/View;LX/Jdk;)Lcom/facebook/widget/BetterSwitch;

    move-result-object v1

    invoke-static {p0, v0, v1, p2}, LX/Jdl;->a(LX/Jdl;Landroid/view/View;Lcom/facebook/widget/BetterSwitch;LX/Jdk;)V

    goto :goto_0
.end method

.method public static a(LX/Jdl;Landroid/view/View;Lcom/facebook/widget/BetterSwitch;LX/Jdk;)V
    .locals 4

    .prologue
    .line 2720021
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2720022
    invoke-direct {p0, p3}, LX/Jdl;->a(LX/Jdk;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2720023
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2720024
    :goto_0
    return-void

    .line 2720025
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2720026
    const v0, 0x7f0d0719

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2720027
    const v1, 0x7f0d071a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2720028
    const v2, 0x7f0d0718

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 2720029
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2720030
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2720031
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p3}, LX/Jdl;->a(Landroid/content/Context;LX/Jdk;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2720032
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p3}, LX/Jdl;->b(Landroid/content/Context;LX/Jdk;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2720033
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2720034
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a019a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setLinkTextColor(I)V

    .line 2720035
    sget-object v0, LX/Jdj;->a:[I

    invoke-virtual {p3}, LX/Jdk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2720036
    const/4 v0, 0x0

    :goto_1
    move v0, v0

    .line 2720037
    invoke-virtual {p2, v0}, Lcom/facebook/widget/BetterSwitch;->setChecked(Z)V

    .line 2720038
    invoke-direct {p0, p3, p2, v2}, LX/Jdl;->a(LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 2720039
    :pswitch_0
    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2720040
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->J:Z

    move v0, v1

    .line 2720041
    goto :goto_1

    .line 2720042
    :pswitch_1
    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2720043
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->K:Z

    move v0, v1

    .line 2720044
    goto :goto_1

    .line 2720045
    :pswitch_2
    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2720046
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->S:Z

    move v0, v1

    .line 2720047
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(LX/Jdk;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2720048
    sget-object v1, LX/Jdj;->a:[I

    invoke-virtual {p1}, LX/Jdk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2720049
    :cond_0
    :goto_0
    return v0

    .line 2720050
    :pswitch_0
    iget-object v1, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2720051
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->I:Z

    move v1, v2

    .line 2720052
    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 2720053
    :pswitch_1
    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-direct {p0, v0}, LX/Jdl;->b(Lcom/facebook/user/model/User;)Z

    move-result v0

    goto :goto_0

    .line 2720054
    :pswitch_2
    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-direct {p0, v0}, LX/Jdl;->c(Lcom/facebook/user/model/User;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/Jdl;Lcom/facebook/widget/BetterSwitch;)V
    .locals 1

    .prologue
    .line 2719978
    invoke-virtual {p1}, Lcom/facebook/widget/BetterSwitch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-direct {p0, v0}, LX/Jdl;->b(Lcom/facebook/user/model/User;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2719979
    :cond_0
    iget-object v0, p0, LX/Jdl;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2719980
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/widget/BetterSwitch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-direct {p0, v0}, LX/Jdl;->c(Lcom/facebook/user/model/User;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2719981
    :cond_1
    iget-object v0, p0, LX/Jdl;->g:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2719982
    :goto_1
    return-void

    .line 2719983
    :cond_2
    iget-object v0, p0, LX/Jdl;->f:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    goto :goto_0

    .line 2719984
    :cond_3
    iget-object v0, p0, LX/Jdl;->g:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/Jdl;
    .locals 15

    .prologue
    .line 2719929
    new-instance v0, LX/Jdl;

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v1

    check-cast v1, LX/1CX;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, Lcom/facebook/messaging/blocking/BlockingUtils;->b(LX/0QB;)Lcom/facebook/messaging/blocking/BlockingUtils;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/blocking/BlockingUtils;

    invoke-static {p0}, LX/JdU;->b(LX/0QB;)LX/JdU;

    move-result-object v4

    check-cast v4, LX/JdU;

    const/16 v5, 0x300

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 2719930
    new-instance v7, LX/JfM;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {v7, v6}, LX/JfM;-><init>(LX/0Zb;)V

    .line 2719931
    move-object v6, v7

    .line 2719932
    check-cast v6, LX/JfM;

    invoke-static {p0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v7

    check-cast v7, LX/IZK;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    const/16 v9, 0x27c5

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 2719933
    new-instance v11, LX/JfN;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct {v11, v10}, LX/JfN;-><init>(LX/0Uh;)V

    .line 2719934
    move-object v10, v11

    .line 2719935
    check-cast v10, LX/JfN;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v12

    check-cast v12, LX/26j;

    invoke-static {p0}, LX/Js1;->b(LX/0QB;)LX/Js1;

    move-result-object v13

    check-cast v13, LX/Js1;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v14}, LX/Jdl;-><init>(LX/1CX;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/messaging/blocking/BlockingUtils;LX/JdU;LX/0Or;LX/JfM;LX/IZK;LX/0ad;LX/0Ot;LX/JfN;LX/0Uh;LX/26j;LX/Js1;Ljava/lang/Boolean;)V

    .line 2719936
    return-object v0
.end method

.method private b(Landroid/content/Context;LX/Jdk;)Landroid/text/SpannableString;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2719963
    sget-object v0, LX/Jdj;->a:[I

    invoke-virtual {p2}, LX/Jdk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2719964
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Block row type not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2719965
    :pswitch_0
    iget-object v0, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2719966
    new-instance v2, LX/47x;

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2719967
    const v3, 0x7f082b22

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2719968
    const-string v3, "[[link_learn_more]]"

    const v4, 0x7f082b24    # 1.80999E38f

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2719969
    new-instance v5, LX/63A;

    invoke-direct {v5}, LX/63A;-><init>()V

    .line 2719970
    new-instance p2, LX/Jdd;

    invoke-direct {p2, p0, p1}, LX/Jdd;-><init>(LX/Jdl;Landroid/content/Context;)V

    .line 2719971
    iput-object p2, v5, LX/63A;->a:LX/639;

    .line 2719972
    move-object v5, v5

    .line 2719973
    const/16 p2, 0x21

    invoke-virtual {v2, v3, v4, v5, p2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2719974
    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    move-object v0, v2

    .line 2719975
    :goto_0
    return-object v0

    .line 2719976
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b21

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-static {v3}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0

    .line 2719977
    :pswitch_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b23

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    invoke-static {v3}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Lcom/facebook/user/model/User;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2719957
    iget-boolean v1, p0, LX/Jdl;->u:Z

    if-eqz v1, :cond_1

    .line 2719958
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/Jdl;->q:LX/JfN;

    .line 2719959
    iget-object v2, v1, LX/JfN;->a:LX/0Uh;

    const/16 v3, 0x111

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2719960
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->P()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2719961
    iget-object v1, p1, Lcom/facebook/user/model/User;->u:LX/4nY;

    move-object v1, v1

    .line 2719962
    sget-object v2, LX/4nY;->COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Lcom/facebook/user/model/User;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2719952
    iget-object v1, p0, LX/Jdl;->r:LX/0Uh;

    const/16 v2, 0x1e0

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->P()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2719953
    iget-object v1, p1, Lcom/facebook/user/model/User;->u:LX/4nY;

    move-object v1, v1

    .line 2719954
    sget-object v2, LX/4nY;->COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

    if-eq v1, v2, :cond_0

    .line 2719955
    iget-boolean v1, p1, Lcom/facebook/user/model/User;->S:Z

    move v1, v1

    .line 2719956
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static d(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/Jdf;
    .locals 1

    .prologue
    .line 2719951
    new-instance v0, LX/Jdf;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Jdf;-><init>(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V

    return-object v0
.end method

.method public static e(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/2h0;
    .locals 1

    .prologue
    .line 2719950
    new-instance v0, LX/Jdg;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Jdg;-><init>(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V

    return-object v0
.end method

.method public static f(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 2719945
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2719946
    invoke-virtual {p2}, Lcom/facebook/widget/BetterSwitch;->toggle()V

    .line 2719947
    invoke-direct {p0, p1, p2, p3}, LX/Jdl;->a(LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2719948
    invoke-static {p0, p1, p2, p3}, LX/Jdl;->h(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V

    .line 2719949
    return-void
.end method

.method public static g(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 2719941
    invoke-direct {p0, p1}, LX/Jdl;->a(LX/Jdk;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 2719942
    :cond_0
    :goto_0
    return-void

    .line 2719943
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/facebook/widget/BetterSwitch;->setVisibility(I)V

    .line 2719944
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public static h(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 2719937
    invoke-direct {p0, p1}, LX/Jdl;->a(LX/Jdk;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 2719938
    :cond_0
    :goto_0
    return-void

    .line 2719939
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/BetterSwitch;->setVisibility(I)V

    .line 2719940
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
