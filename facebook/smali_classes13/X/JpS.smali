.class public final enum LX/JpS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JpS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JpS;

.field public static final enum FACEBOOK:LX/JpS;

.field public static final enum SMS:LX/JpS;

.field public static final enum TINCAN:LX/JpS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2737413
    new-instance v0, LX/JpS;

    const-string v1, "FACEBOOK"

    invoke-direct {v0, v1, v2}, LX/JpS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JpS;->FACEBOOK:LX/JpS;

    .line 2737414
    new-instance v0, LX/JpS;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v3}, LX/JpS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JpS;->SMS:LX/JpS;

    .line 2737415
    new-instance v0, LX/JpS;

    const-string v1, "TINCAN"

    invoke-direct {v0, v1, v4}, LX/JpS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JpS;->TINCAN:LX/JpS;

    .line 2737416
    const/4 v0, 0x3

    new-array v0, v0, [LX/JpS;

    sget-object v1, LX/JpS;->FACEBOOK:LX/JpS;

    aput-object v1, v0, v2

    sget-object v1, LX/JpS;->SMS:LX/JpS;

    aput-object v1, v0, v3

    sget-object v1, LX/JpS;->TINCAN:LX/JpS;

    aput-object v1, v0, v4

    sput-object v0, LX/JpS;->$VALUES:[LX/JpS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2737412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JpS;
    .locals 1

    .prologue
    .line 2737418
    const-class v0, LX/JpS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JpS;

    return-object v0
.end method

.method public static values()[LX/JpS;
    .locals 1

    .prologue
    .line 2737417
    sget-object v0, LX/JpS;->$VALUES:[LX/JpS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JpS;

    return-object v0
.end method
