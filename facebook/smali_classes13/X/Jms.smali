.class public LX/Jms;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2733423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2733385
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->u()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    move-result-object v0

    const/4 v2, 0x0

    .line 2733386
    if-nez v0, :cond_0

    .line 2733387
    :goto_0
    move-object v0, v2

    .line 2733388
    return-object v0

    .line 2733389
    :cond_0
    new-instance v3, LX/5Ss;

    invoke-direct {v3}, LX/5Ss;-><init>()V

    .line 2733390
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2733391
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2733392
    :goto_1
    iput-object v1, v3, LX/5Ss;->b:Ljava/lang/String;

    .line 2733393
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2733394
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2733395
    :goto_2
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_a

    const/4 p0, 0x0

    :goto_3
    iput-object p0, v3, LX/5Ss;->h:Landroid/net/Uri;

    .line 2733396
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2733397
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2733398
    :goto_4
    iput-object v1, v3, LX/5Ss;->d:Ljava/lang/String;

    .line 2733399
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->cv_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v1

    if-nez v1, :cond_8

    move-object v1, v2

    .line 2733400
    :goto_5
    iput-object v1, v3, LX/5Ss;->e:Ljava/lang/String;

    .line 2733401
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 2733402
    iput-object v1, v3, LX/5Ss;->a:Ljava/lang/String;

    .line 2733403
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->m()Ljava/lang/String;

    move-result-object v1

    .line 2733404
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_b

    const/4 p0, 0x0

    :goto_6
    iput-object p0, v3, LX/5Ss;->f:Landroid/net/Uri;

    .line 2733405
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->cu_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->cu_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;->a()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel$ImageModel;

    move-result-object v1

    if-nez v1, :cond_9

    .line 2733406
    :cond_1
    :goto_7
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x0

    :goto_8
    iput-object v1, v3, LX/5Ss;->g:Landroid/net/Uri;

    .line 2733407
    new-instance v1, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    invoke-direct {v1, v3}, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;-><init>(LX/5Ss;)V

    move-object v2, v1

    .line 2733408
    goto/16 :goto_0

    .line 2733409
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2733410
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 2733411
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->b()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 2733412
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->d()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->d()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2733413
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->d()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_5
    move-object v1, v2

    .line 2733414
    goto/16 :goto_2

    .line 2733415
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->k()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->k()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2733416
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->k()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    :cond_7
    move-object v1, v2

    .line 2733417
    goto/16 :goto_4

    .line 2733418
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->cv_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;->b()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 2733419
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->cu_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;->a()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 2733420
    :cond_a
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    goto/16 :goto_3

    .line 2733421
    :cond_b
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    goto/16 :goto_6

    .line 2733422
    :cond_c
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_8
.end method
