.class public LX/Jqe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GO;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final o:Ljava/lang/Object;


# instance fields
.field private final a:LX/JqK;

.field private final b:LX/Jqc;

.field private final c:LX/7GP;

.field public final d:LX/Jqb;

.field public final e:LX/3QJ;

.field public final f:LX/2Ow;

.field public final g:LX/Di5;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/CLl;

.field public final j:LX/2Mk;

.field private final k:LX/Jqd;

.field public final l:LX/01T;

.field public m:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739982
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqe;->o:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/JqK;LX/Jqc;LX/7GP;LX/Jqb;LX/3QJ;LX/2Ow;LX/Di5;LX/0Or;LX/CLl;LX/2Mk;LX/Jqd;LX/01T;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JqK;",
            "LX/Jqc;",
            "LX/7GP;",
            "LX/Jqb;",
            "LX/3QJ;",
            "LX/2Ow;",
            "LX/Di5;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "Lcom/facebook/messaging/deliveryreceipt/SendDeliveryReceiptManager;",
            "LX/2Mk;",
            "LX/Jqd;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2739964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739965
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739966
    iput-object v0, p0, LX/Jqe;->m:LX/0Ot;

    .line 2739967
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739968
    iput-object v0, p0, LX/Jqe;->n:LX/0Ot;

    .line 2739969
    iput-object p1, p0, LX/Jqe;->a:LX/JqK;

    .line 2739970
    iput-object p2, p0, LX/Jqe;->b:LX/Jqc;

    .line 2739971
    iput-object p3, p0, LX/Jqe;->c:LX/7GP;

    .line 2739972
    iput-object p4, p0, LX/Jqe;->d:LX/Jqb;

    .line 2739973
    iput-object p5, p0, LX/Jqe;->e:LX/3QJ;

    .line 2739974
    iput-object p6, p0, LX/Jqe;->f:LX/2Ow;

    .line 2739975
    iput-object p7, p0, LX/Jqe;->g:LX/Di5;

    .line 2739976
    iput-object p8, p0, LX/Jqe;->h:LX/0Or;

    .line 2739977
    iput-object p9, p0, LX/Jqe;->i:LX/CLl;

    .line 2739978
    iput-object p10, p0, LX/Jqe;->j:LX/2Mk;

    .line 2739979
    iput-object p11, p0, LX/Jqe;->k:LX/Jqd;

    .line 2739980
    iput-object p12, p0, LX/Jqe;->l:LX/01T;

    .line 2739981
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqe;
    .locals 7

    .prologue
    .line 2739843
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2739844
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2739845
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2739846
    if-nez v1, :cond_0

    .line 2739847
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739848
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2739849
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2739850
    sget-object v1, LX/Jqe;->o:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2739851
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2739852
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2739853
    :cond_1
    if-nez v1, :cond_4

    .line 2739854
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2739855
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2739856
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqe;->b(LX/0QB;)LX/Jqe;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2739857
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2739858
    if-nez v1, :cond_2

    .line 2739859
    sget-object v0, LX/Jqe;->o:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqe;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2739860
    :goto_1
    if-eqz v0, :cond_3

    .line 2739861
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739862
    :goto_3
    check-cast v0, LX/Jqe;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2739863
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2739864
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2739865
    :catchall_1
    move-exception v0

    .line 2739866
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739867
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2739868
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2739869
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqe;->o:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqe;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Jqe;
    .locals 13

    .prologue
    .line 2739960
    new-instance v0, LX/Jqe;

    invoke-static {p0}, LX/JqK;->a(LX/0QB;)LX/JqK;

    move-result-object v1

    check-cast v1, LX/JqK;

    invoke-static {p0}, LX/Jqc;->a(LX/0QB;)LX/Jqc;

    move-result-object v2

    check-cast v2, LX/Jqc;

    invoke-static {p0}, LX/7GP;->a(LX/0QB;)LX/7GP;

    move-result-object v3

    check-cast v3, LX/7GP;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v4

    check-cast v4, LX/Jqb;

    invoke-static {p0}, LX/3QI;->a(LX/0QB;)LX/3QI;

    move-result-object v5

    check-cast v5, LX/3QJ;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v6

    check-cast v6, LX/2Ow;

    invoke-static {p0}, LX/Di5;->a(LX/0QB;)LX/Di5;

    move-result-object v7

    check-cast v7, LX/Di5;

    const/16 v8, 0xce8

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/CLl;->b(LX/0QB;)LX/CLl;

    move-result-object v9

    check-cast v9, LX/CLl;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v10

    check-cast v10, LX/2Mk;

    invoke-static {p0}, LX/Jqd;->a(LX/0QB;)LX/Jqd;

    move-result-object v11

    check-cast v11, LX/Jqd;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v12

    check-cast v12, LX/01T;

    invoke-direct/range {v0 .. v12}, LX/Jqe;-><init>(LX/JqK;LX/Jqc;LX/7GP;LX/Jqb;LX/3QJ;LX/2Ow;LX/Di5;LX/0Or;LX/CLl;LX/2Mk;LX/Jqd;LX/01T;)V

    .line 2739961
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x29c5

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2739962
    iput-object v1, v0, LX/Jqe;->m:LX/0Ot;

    iput-object v2, v0, LX/Jqe;->n:LX/0Ot;

    .line 2739963
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2739957
    iget-object v0, p0, LX/Jqe;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0}, LX/2Oe;->a()V

    .line 2739958
    iget-object v0, p0, LX/Jqe;->f:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->a()V

    .line 2739959
    return-void
.end method

.method public final a(LX/6l3;Lcom/facebook/fbtrace/FbTraceNode;)V
    .locals 12

    .prologue
    .line 2739915
    iget-object v0, p0, LX/Jqe;->j:LX/2Mk;

    invoke-virtual {v0}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 2739916
    if-nez v0, :cond_0

    .line 2739917
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "No user is logged in!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739918
    :cond_0
    iget-object v1, p0, LX/Jqe;->c:LX/7GP;

    sget-object v2, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    iget-object v3, p1, LX/6l3;->deltas:Ljava/util/List;

    iget-object v4, p1, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p0, LX/Jqe;->k:LX/Jqd;

    iget-object v7, p0, LX/Jqe;->n:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/7GH;

    iget-object v8, p0, LX/Jqe;->a:LX/JqK;

    iget-object v9, p0, LX/Jqe;->b:LX/Jqc;

    move-object v10, p2

    move-object v11, p0

    invoke-virtual/range {v1 .. v11}, LX/7GP;->a(LX/7GT;Ljava/util/List;JLX/7GK;LX/7GH;LX/7Fx;LX/7GI;Lcom/facebook/fbtrace/FbTraceNode;LX/7GO;)LX/7GN;

    move-result-object v1

    .line 2739919
    iget-object v2, p0, LX/Jqe;->j:LX/2Mk;

    invoke-virtual {v2}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    .line 2739920
    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2739921
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Logged in user has changed during delta processing!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739922
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2739923
    iget-object v0, p0, LX/Jqe;->l:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v0, v3, :cond_3

    .line 2739924
    :cond_2
    :goto_0
    return-void

    .line 2739925
    :cond_3
    iget-object v0, v1, LX/7GN;->a:LX/0P1;

    .line 2739926
    sget-object v3, LX/146;->a:Ljava/util/Comparator;

    check-cast v3, LX/1sm;

    .line 2739927
    invoke-static {v0, v3}, LX/146;->b(Ljava/util/Map;Ljava/util/Comparator;)LX/146;

    move-result-object v3

    move-object v0, v3

    .line 2739928
    iget-object v3, p0, LX/Jqe;->j:LX/2Mk;

    invoke-virtual {v3}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v3

    .line 2739929
    if-eqz v3, :cond_2

    .line 2739930
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    .line 2739931
    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2739932
    const-string v5, "newMessageResult"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2739933
    if-eqz v0, :cond_4

    .line 2739934
    iget-object v5, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v5

    .line 2739935
    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v5, :cond_4

    .line 2739936
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v5, :cond_4

    .line 2739937
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v5}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    .line 2739938
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2739939
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2739940
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2739941
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2739942
    iget-object v2, p0, LX/Jqe;->i:LX/CLl;

    .line 2739943
    iget-object v3, v2, LX/CLl;->g:LX/01T;

    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    if-eq v3, v4, :cond_7

    .line 2739944
    :cond_6
    :goto_2
    goto :goto_0

    .line 2739945
    :cond_7
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_6

    iget-object v3, v2, LX/CLl;->n:LX/0Uh;

    const/16 v4, 0x206

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2739946
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 2739947
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    const/4 v3, 0x0

    move v5, v3

    :goto_3
    if-ge v5, p0, :cond_9

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messages/Message;

    .line 2739948
    iget-object v1, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739949
    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 2739950
    if-nez v4, :cond_8

    .line 2739951
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2739952
    invoke-interface {v6, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739953
    :cond_8
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2739954
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 2739955
    :cond_9
    move-object v3, v6

    .line 2739956
    invoke-static {v2, v3}, LX/CLl;->a(LX/CLl;Ljava/util/Map;)V

    goto :goto_2
.end method

.method public final b()V
    .locals 15

    .prologue
    .line 2739870
    iget-object v0, p0, LX/Jqe;->l:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_b

    .line 2739871
    :cond_0
    iget-object v1, p0, LX/Jqe;->d:LX/Jqb;

    .line 2739872
    iget-object v2, v1, LX/Jqb;->c:Ljava/util/Set;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v3, v2

    .line 2739873
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739874
    iget-object v2, p0, LX/Jqe;->f:LX/2Ow;

    invoke-virtual {v2, v1}, LX/2Ow;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2739875
    iget-object v2, p0, LX/Jqe;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Og;

    invoke-virtual {v2, v1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2739876
    if-eqz v1, :cond_1

    .line 2739877
    iget-object v2, p0, LX/Jqe;->g:LX/Di5;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v1}, LX/Di5;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0

    .line 2739878
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2739879
    iget-object v1, p0, LX/Jqe;->f:LX/2Ow;

    invoke-virtual {v1}, LX/2Ow;->c()V

    .line 2739880
    :cond_3
    iget-object v1, p0, LX/Jqe;->d:LX/Jqb;

    .line 2739881
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v8

    .line 2739882
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2739883
    iget-object v5, v1, LX/Jqb;->b:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739884
    invoke-interface {v8, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2739885
    :cond_4
    iget-object v5, v1, LX/Jqb;->f:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739886
    invoke-interface {v8, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2739887
    :cond_5
    iget-object v5, v1, LX/Jqb;->g:Ljava/util/Map;

    invoke-interface {v8, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2739888
    iget-object v5, v1, LX/Jqb;->e:Ljava/util/Map;

    invoke-interface {v8, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2739889
    iget-object v5, v1, LX/Jqb;->d:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 2739890
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739891
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 2739892
    iget-object v7, v1, LX/Jqb;->e:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    .line 2739893
    if-eqz v7, :cond_7

    const-string v10, "sequence_id"

    invoke-virtual {v7, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    const-string v7, "sequence_id"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    cmp-long v7, v11, v13

    if-gtz v7, :cond_6

    .line 2739894
    :cond_7
    invoke-interface {v8, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 2739895
    :cond_8
    invoke-static {v8}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v5

    move-object v1, v5

    .line 2739896
    invoke-virtual {v1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2739897
    iget-object v4, p0, LX/Jqe;->f:LX/2Ow;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v4, v2, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/os/Bundle;)V

    goto :goto_4

    .line 2739898
    :cond_9
    iget-object v1, p0, LX/Jqe;->d:LX/Jqb;

    .line 2739899
    iget-object v2, v1, LX/Jqb;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v3, v2

    .line 2739900
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_a

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jqa;

    .line 2739901
    iget-object v5, p0, LX/Jqe;->f:LX/2Ow;

    iget-object v6, v1, LX/Jqa;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739902
    iget-object v7, v1, LX/Jqa;->b:Ljava/util/List;

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    move-object v7, v7

    .line 2739903
    iget-object v8, v1, LX/Jqa;->c:Ljava/util/List;

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v8

    move-object v1, v8

    .line 2739904
    invoke-virtual {v5, v6, v7, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 2739905
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 2739906
    :cond_a
    iget-object v0, p0, LX/Jqe;->d:LX/Jqb;

    invoke-virtual {v0}, LX/Jqb;->e()V

    .line 2739907
    return-void

    .line 2739908
    :cond_b
    iget-object v0, p0, LX/Jqe;->d:LX/Jqb;

    .line 2739909
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2739910
    iget-object v1, v0, LX/Jqb;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2739911
    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_6

    .line 2739912
    :cond_c
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2739913
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/notify/NewMessageNotification;

    .line 2739914
    iget-object v2, p0, LX/Jqe;->e:LX/3QJ;

    invoke-interface {v2, v0}, LX/3QJ;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_7
.end method
