.class public LX/K4q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylinePhoto;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylineUser;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/storyline/model/Mood;

.field public final e:Lcom/facebook/storyline/model/Cutdown;

.field public final f:F

.field public final g:LX/K4p;

.field private final h:I


# direct methods
.method public constructor <init>(LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/storyline/model/Mood;Lcom/facebook/storyline/model/Cutdown;FLX/K4p;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylinePhoto;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylineUser;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/storyline/model/Mood;",
            "Lcom/facebook/storyline/model/Cutdown;",
            "F",
            "LX/K4p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2769521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769522
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2769523
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2769524
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2769525
    iput-object p1, p0, LX/K4q;->a:LX/0Px;

    .line 2769526
    iput-object p2, p0, LX/K4q;->b:LX/0Px;

    .line 2769527
    iput-object p3, p0, LX/K4q;->c:Ljava/lang/String;

    .line 2769528
    iput-object p4, p0, LX/K4q;->d:Lcom/facebook/storyline/model/Mood;

    .line 2769529
    iput-object p5, p0, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    .line 2769530
    iput p6, p0, LX/K4q;->f:F

    .line 2769531
    iput-object p7, p0, LX/K4q;->g:LX/K4p;

    .line 2769532
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, LX/K4q;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/K4q;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/K4q;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/K4q;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/K4q;->g:LX/K4p;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, LX/K4q;->h:I

    .line 2769533
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2769534
    if-ne p0, p1, :cond_1

    .line 2769535
    :cond_0
    :goto_0
    return v0

    .line 2769536
    :cond_1
    instance-of v2, p1, LX/K4q;

    if-nez v2, :cond_2

    move v0, v1

    .line 2769537
    goto :goto_0

    .line 2769538
    :cond_2
    check-cast p1, LX/K4q;

    .line 2769539
    invoke-virtual {p0}, LX/K4q;->hashCode()I

    move-result v2

    invoke-virtual {p1}, LX/K4q;->hashCode()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, LX/K4q;->f:F

    iget v3, p1, LX/K4q;->f:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, LX/K4q;->c:Ljava/lang/String;

    iget-object v3, p1, LX/K4q;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    iget-object v3, p1, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/K4q;->a:LX/0Px;

    iget-object v3, p1, LX/K4q;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/K4q;->b:LX/0Px;

    iget-object v3, p1, LX/K4q;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/K4q;->g:LX/K4p;

    iget-object v3, p1, LX/K4q;->g:LX/K4p;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2769540
    iget v0, p0, LX/K4q;->h:I

    return v0
.end method
