.class public LX/JZK;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JZJ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JZL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2708383
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JZK;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JZL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708389
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2708390
    iput-object p1, p0, LX/JZK;->b:LX/0Ot;

    .line 2708391
    return-void
.end method

.method public static a(LX/0QB;)LX/JZK;
    .locals 4

    .prologue
    .line 2708392
    const-class v1, LX/JZK;

    monitor-enter v1

    .line 2708393
    :try_start_0
    sget-object v0, LX/JZK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708394
    sput-object v2, LX/JZK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708395
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708396
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708397
    new-instance v3, LX/JZK;

    const/16 p0, 0x21e2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JZK;-><init>(LX/0Ot;)V

    .line 2708398
    move-object v0, v3

    .line 2708399
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708400
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708401
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708402
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 16

    .prologue
    .line 2708386
    check-cast p2, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    .line 2708387
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JZK;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JZL;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->b:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->c:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->d:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->e:J

    move-object/from16 v0, p2

    iget v10, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->f:I

    move-object/from16 v0, p2

    iget v11, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->g:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->h:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->i:Landroid/graphics/Typeface;

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->k:Ljava/lang/Boolean;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v15}, LX/JZL;->a(LX/1De;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIILandroid/view/View$OnClickListener;Landroid/graphics/Typeface;Lcom/facebook/graphql/model/GraphQLTarotDigest;Ljava/lang/Boolean;)LX/1Dg;

    move-result-object v2

    .line 2708388
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2708384
    invoke-static {}, LX/1dS;->b()V

    .line 2708385
    const/4 v0, 0x0

    return-object v0
.end method
