.class public LX/Jme;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Jmn;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;


# direct methods
.method public constructor <init>(LX/0Px;Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;)V
    .locals 0
    .param p3    # Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Jmn;",
            ">;",
            "Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;",
            "Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2733181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2733182
    iput-object p1, p0, LX/Jme;->a:LX/0Px;

    .line 2733183
    iput-object p3, p0, LX/Jme;->b:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;

    .line 2733184
    iput-object p2, p0, LX/Jme;->c:Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;

    .line 2733185
    return-void
.end method
