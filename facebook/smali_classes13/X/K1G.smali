.class public final LX/K1G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:LX/5rJ;

.field public final synthetic b:LX/K19;

.field public final synthetic c:Lcom/facebook/react/views/textinput/ReactTextInputManager;


# direct methods
.method public constructor <init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/5rJ;LX/K19;)V
    .locals 0

    .prologue
    .line 2761661
    iput-object p1, p0, LX/K1G;->c:Lcom/facebook/react/views/textinput/ReactTextInputManager;

    iput-object p2, p0, LX/K1G;->a:LX/5rJ;

    iput-object p3, p0, LX/K1G;->b:LX/K19;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2761662
    and-int/lit16 v0, p2, 0xff

    if-gtz v0, :cond_0

    if-nez p2, :cond_1

    .line 2761663
    :cond_0
    iget-object v0, p0, LX/K1G;->a:LX/5rJ;

    const-class v2, LX/5rQ;

    invoke-virtual {v0, v2}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2761664
    iget-object v2, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v2

    .line 2761665
    new-instance v2, LX/K1M;

    iget-object v3, p0, LX/K1G;->b:LX/K19;

    invoke-virtual {v3}, LX/K19;->getId()I

    move-result v3

    iget-object v4, p0, LX/K1G;->b:LX/K19;

    invoke-virtual {v4}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/K1M;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v2}, LX/5s9;->a(LX/5r0;)V

    .line 2761666
    :cond_1
    const/4 v0, 0x5

    if-eq p2, v0, :cond_2

    const/4 v0, 0x7

    if-ne p2, v0, :cond_4

    .line 2761667
    :cond_2
    iget-object v0, p0, LX/K1G;->b:LX/K19;

    .line 2761668
    iget-boolean v2, v0, LX/K19;->l:Z

    move v0, v2

    .line 2761669
    if-eqz v0, :cond_3

    .line 2761670
    iget-object v0, p0, LX/K1G;->b:LX/K19;

    invoke-virtual {v0}, LX/K19;->clearFocus()V

    :cond_3
    move v0, v1

    .line 2761671
    :goto_0
    return v0

    :cond_4
    iget-object v0, p0, LX/K1G;->b:LX/K19;

    .line 2761672
    iget-boolean v2, v0, LX/K19;->l:Z

    move v0, v2

    .line 2761673
    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
