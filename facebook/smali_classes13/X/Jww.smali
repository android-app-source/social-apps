.class public LX/Jww;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2752609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752610
    return-void
.end method

.method public static final a(Ljava/util/List;)LX/Jwv;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Jwt;",
            ">;)",
            "LX/Jwv;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752611
    const/4 v0, 0x0

    .line 2752612
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jwt;

    .line 2752613
    if-nez v1, :cond_2

    .line 2752614
    const/4 p0, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2752615
    iget v4, v0, LX/Jwt;->a:I

    move v4, v4

    .line 2752616
    if-ne v4, p0, :cond_0

    .line 2752617
    iget-byte v4, v0, LX/Jwt;->b:B

    move v4, v4

    .line 2752618
    if-ne v4, p0, :cond_0

    .line 2752619
    iget-object v4, v0, LX/Jwt;->c:[B

    move-object v4, v4

    .line 2752620
    array-length v4, v4

    const/4 p0, 0x2

    if-eq v4, p0, :cond_5

    :cond_0
    move v1, v3

    .line 2752621
    :cond_1
    :goto_1
    move v0, v1

    .line 2752622
    move v1, v0

    goto :goto_0

    .line 2752623
    :cond_2
    const/4 v3, 0x1

    .line 2752624
    iget v1, v0, LX/Jwt;->a:I

    move v1, v1

    .line 2752625
    const/16 v2, 0x17

    if-ne v1, v2, :cond_3

    .line 2752626
    iget-byte v1, v0, LX/Jwt;->b:B

    move v1, v1

    .line 2752627
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 2752628
    iget-object v1, v0, LX/Jwt;->c:[B

    move-object v1, v1

    .line 2752629
    const/4 v2, 0x0

    aget-byte v1, v1, v2

    const/16 v2, -0x55

    if-ne v1, v2, :cond_3

    .line 2752630
    iget-object v1, v0, LX/Jwt;->c:[B

    move-object v1, v1

    .line 2752631
    aget-byte v1, v1, v3

    if-eq v1, v3, :cond_7

    .line 2752632
    :cond_3
    const/4 v1, 0x0

    .line 2752633
    :goto_2
    move-object v0, v1

    .line 2752634
    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 2752635
    :cond_5
    iget-object v4, v0, LX/Jwt;->c:[B

    move-object v4, v4

    .line 2752636
    aget-byte v4, v4, v3

    const/16 p0, -0x48

    if-ne v4, p0, :cond_6

    .line 2752637
    iget-object v4, v0, LX/Jwt;->c:[B

    move-object v4, v4

    .line 2752638
    aget-byte v4, v4, v1

    const/4 p0, -0x2

    if-eq v4, p0, :cond_1

    :cond_6
    move v1, v3

    goto :goto_1

    .line 2752639
    :cond_7
    new-instance v1, Ljava/math/BigInteger;

    .line 2752640
    iget-object v2, v0, LX/Jwt;->c:[B

    move-object v2, v2

    .line 2752641
    invoke-direct {v1, v3, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 2752642
    :goto_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 2752643
    iget-object v3, v0, LX/Jwt;->c:[B

    move-object v3, v3

    .line 2752644
    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_8

    .line 2752645
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 2752646
    :cond_8
    new-instance v2, LX/Jwv;

    invoke-direct {v2, v1}, LX/Jwv;-><init>(Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_2
.end method
