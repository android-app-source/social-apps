.class public LX/Jr1;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDq;

.field private final b:LX/FDs;

.field private final c:LX/FDI;

.field private final d:LX/Jrc;

.field public final e:LX/Jqb;

.field private final f:LX/Iul;

.field public final g:LX/2My;

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741402
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr1;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDq;LX/FDs;LX/FDI;LX/Jrc;LX/Jqb;LX/Iul;LX/2My;)V
    .locals 1
    .param p3    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2741391
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2741392
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741393
    iput-object v0, p0, LX/Jr1;->h:LX/0Ot;

    .line 2741394
    iput-object p1, p0, LX/Jr1;->a:LX/FDq;

    .line 2741395
    iput-object p2, p0, LX/Jr1;->b:LX/FDs;

    .line 2741396
    iput-object p3, p0, LX/Jr1;->c:LX/FDI;

    .line 2741397
    iput-object p4, p0, LX/Jr1;->d:LX/Jrc;

    .line 2741398
    iput-object p5, p0, LX/Jr1;->e:LX/Jqb;

    .line 2741399
    iput-object p6, p0, LX/Jr1;->f:LX/Iul;

    .line 2741400
    iput-object p7, p0, LX/Jr1;->g:LX/2My;

    .line 2741401
    return-void
.end method

.method private static a(LX/6jv;)J
    .locals 2

    .prologue
    .line 2741389
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2741390
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/Jr1;
    .locals 15

    .prologue
    .line 2741289
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741290
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741291
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741292
    if-nez v1, :cond_0

    .line 2741293
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741294
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741295
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741296
    sget-object v1, LX/Jr1;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741297
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741298
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741299
    :cond_1
    if-nez v1, :cond_4

    .line 2741300
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741301
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741302
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741303
    new-instance v7, LX/Jr1;

    invoke-static {v0}, LX/FDq;->a(LX/0QB;)LX/FDq;

    move-result-object v8

    check-cast v8, LX/FDq;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v9

    check-cast v9, LX/FDs;

    invoke-static {v0}, LX/FDK;->a(LX/0QB;)LX/FDI;

    move-result-object v10

    check-cast v10, LX/FDI;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v11

    check-cast v11, LX/Jrc;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v12

    check-cast v12, LX/Jqb;

    invoke-static {v0}, LX/Iul;->a(LX/0QB;)LX/Iul;

    move-result-object v13

    check-cast v13, LX/Iul;

    invoke-static {v0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v14

    check-cast v14, LX/2My;

    invoke-direct/range {v7 .. v14}, LX/Jr1;-><init>(LX/FDq;LX/FDs;LX/FDI;LX/Jrc;LX/Jqb;LX/Iul;LX/2My;)V

    .line 2741304
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2741305
    iput-object v8, v7, LX/Jr1;->h:LX/0Ot;

    .line 2741306
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741307
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741308
    if-nez v1, :cond_2

    .line 2741309
    sget-object v0, LX/Jr1;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr1;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741310
    :goto_1
    if-eqz v0, :cond_3

    .line 2741311
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741312
    :goto_3
    check-cast v0, LX/Jr1;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741313
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741314
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741315
    :catchall_1
    move-exception v0

    .line 2741316
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741317
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741318
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741319
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr1;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr1;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V
    .locals 10

    .prologue
    .line 2741358
    iget-object v0, p0, LX/Jr1;->b:LX/FDs;

    new-instance v1, LX/6ia;

    invoke-direct {v1}, LX/6ia;-><init>()V

    .line 2741359
    iput-object p1, v1, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741360
    move-object v1, v1

    .line 2741361
    const/4 v2, 0x1

    .line 2741362
    iput-boolean v2, v1, LX/6ia;->b:Z

    .line 2741363
    move-object v1, v1

    .line 2741364
    iput-wide p2, v1, LX/6ia;->e:J

    .line 2741365
    move-object v1, v1

    .line 2741366
    iput-wide p4, v1, LX/6ia;->d:J

    .line 2741367
    move-object v1, v1

    .line 2741368
    invoke-virtual {v1}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v1

    .line 2741369
    iget-object v4, v0, LX/FDs;->d:LX/2N4;

    iget-object v5, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741370
    if-nez v4, :cond_0

    .line 2741371
    sget-object v4, LX/FDr;->THREAD_NOT_FOUND:LX/FDr;

    .line 2741372
    :goto_0
    move-object v0, v4

    .line 2741373
    sget-object v1, LX/Jr0;->a:[I

    invoke-virtual {v0}, LX/FDr;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2741374
    :goto_1
    return-void

    .line 2741375
    :pswitch_0
    iget-object v0, p0, LX/Jr1;->f:LX/Iul;

    invoke-virtual {v0, p2, p3}, LX/Iul;->a(J)V

    .line 2741376
    iget-object v0, p0, LX/Jr1;->e:LX/Jqb;

    .line 2741377
    iget-object v1, v0, LX/Jqb;->c:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2741378
    iget-object v1, v0, LX/Jqb;->b:Ljava/util/Map;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2741379
    goto :goto_1

    .line 2741380
    :pswitch_1
    iget-object v0, p0, LX/Jr1;->f:LX/Iul;

    invoke-virtual {v0, p2, p3}, LX/Iul;->a(J)V

    goto :goto_1

    .line 2741381
    :cond_0
    iget-wide v6, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    iget-wide v8, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v5, v6, v8

    if-gez v5, :cond_1

    .line 2741382
    sget-object v4, LX/FDr;->THREAD_NOT_MARKED_READ:LX/FDr;

    goto :goto_0

    .line 2741383
    :cond_1
    iget-boolean v5, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, LX/FDr;->THREAD_MARKED_READ:LX/FDr;

    .line 2741384
    :goto_2
    new-instance v5, LX/6ie;

    invoke-direct {v5}, LX/6ie;-><init>()V

    sget-object v6, LX/6iW;->READ:LX/6iW;

    .line 2741385
    iput-object v6, v5, LX/6ie;->a:LX/6iW;

    .line 2741386
    move-object v5, v5

    .line 2741387
    invoke-virtual {v5, v1}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    move-result-object v5

    invoke-virtual {v5}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    goto :goto_0

    .line 2741388
    :cond_2
    sget-object v4, LX/FDr;->THREAD_NOT_MARKED_READ:LX/FDr;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741356
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741357
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741344
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->e()LX/6jv;

    move-result-object v7

    .line 2741345
    invoke-static {v7}, LX/Jr1;->a(LX/6jv;)J

    move-result-wide v2

    .line 2741346
    iget-object v0, p0, LX/Jr1;->d:LX/Jrc;

    iget-object v1, v7, LX/6jv;->threadKeys:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741347
    iget-wide v4, p2, LX/7GJ;->b:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Jr1;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V

    .line 2741348
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2741349
    :cond_0
    iget-object v0, v7, LX/6jv;->folders:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2741350
    iget-object v0, v7, LX/6jv;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2741351
    invoke-static {v0}, LX/Jrc;->a(I)LX/6ek;

    move-result-object v0

    .line 2741352
    iget-object v1, p0, LX/Jr1;->a:LX/FDq;

    const-wide/16 v4, -0x1

    const/4 v7, -0x1

    invoke-virtual {v1, v0, v4, v5, v7}, LX/FDq;->a(LX/6ek;JI)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 2741353
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741354
    iget-wide v4, p2, LX/7GJ;->b:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Jr1;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V

    goto :goto_1

    .line 2741355
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2741324
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->e()LX/6jv;

    move-result-object v8

    .line 2741325
    invoke-static {v8}, LX/Jr1;->a(LX/6jv;)J

    move-result-wide v2

    .line 2741326
    iget-object v0, p0, LX/Jr1;->d:LX/Jrc;

    iget-object v1, v8, LX/6jv;->threadKeys:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v7

    :goto_0
    if-ge v6, v10, :cond_1

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741327
    iget-object v0, p0, LX/Jr1;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual/range {v0 .. v5}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V

    .line 2741328
    iget-object v0, p0, LX/Jr1;->g:LX/2My;

    invoke-virtual {v0}, LX/2My;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2741329
    iget-object v0, p0, LX/Jr1;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1, v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2741330
    if-eqz v0, :cond_0

    .line 2741331
    iget-object v4, p0, LX/Jr1;->e:LX/Jqb;

    .line 2741332
    invoke-static {v4, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741333
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2741334
    :cond_1
    iget-object v0, v8, LX/6jv;->folders:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 2741335
    iget-object v0, v8, LX/6jv;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2741336
    invoke-static {v0}, LX/Jrc;->a(I)LX/6ek;

    move-result-object v0

    .line 2741337
    iget-object v1, p0, LX/Jr1;->c:LX/FDI;

    invoke-virtual {v1, v0}, LX/FDI;->a(LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2741338
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2741339
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v9, v1

    .line 2741340
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v7

    :goto_1
    if-ge v6, v10, :cond_2

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741341
    iget-object v0, p0, LX/Jr1;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual/range {v0 .. v5}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V

    .line 2741342
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 2741343
    :cond_3
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2741320
    check-cast p1, LX/6kW;

    .line 2741321
    invoke-virtual {p1}, LX/6kW;->e()LX/6jv;

    move-result-object v0

    .line 2741322
    iget-object v1, p0, LX/Jr1;->d:LX/Jrc;

    iget-object v0, v0, LX/6jv;->threadKeys:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    .line 2741323
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
