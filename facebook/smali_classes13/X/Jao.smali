.class public final LX/Jao;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2715948
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2715949
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2715950
    :goto_0
    return v1

    .line 2715951
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2715952
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2715953
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2715954
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2715955
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2715956
    const-string v3, "admined_groups"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2715957
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2715958
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 2715959
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2715960
    :goto_2
    move v0, v2

    .line 2715961
    goto :goto_1

    .line 2715962
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2715963
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2715964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2715965
    :cond_4
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2715966
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2715967
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2715968
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_4

    if-eqz v5, :cond_4

    .line 2715969
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2715970
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v3

    goto :goto_3

    .line 2715971
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2715972
    :cond_6
    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2715973
    if-eqz v0, :cond_7

    .line 2715974
    invoke-virtual {p1, v2, v4, v2}, LX/186;->a(III)V

    .line 2715975
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_8
    move v0, v2

    move v4, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2715976
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2715977
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2715978
    if-eqz v0, :cond_1

    .line 2715979
    const-string v1, "admined_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2715980
    const/4 v1, 0x0

    .line 2715981
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2715982
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2715983
    if-eqz v1, :cond_0

    .line 2715984
    const-string p1, "count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2715985
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2715986
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2715987
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2715988
    return-void
.end method
