.class public LX/Jdq;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Jdo;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/user/model/User;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/JeZ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/JeB;

.field public final e:LX/Jds;

.field public final f:LX/JeE;

.field public final g:Z

.field public final h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private final i:LX/Jdn;


# direct methods
.method public constructor <init>(LX/JeC;LX/Jds;LX/JeE;Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V
    .locals 1
    .param p3    # LX/JeE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720149
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2720150
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2720151
    iput-object v0, p0, LX/Jdq;->c:LX/0Px;

    .line 2720152
    new-instance v0, LX/Jdn;

    invoke-direct {v0, p0}, LX/Jdn;-><init>(LX/Jdq;)V

    iput-object v0, p0, LX/Jdq;->i:LX/Jdn;

    .line 2720153
    iput-object p3, p0, LX/Jdq;->f:LX/JeE;

    .line 2720154
    iget-object v0, p0, LX/Jdq;->i:LX/Jdn;

    invoke-virtual {p1, v0}, LX/JeC;->a(LX/Jdn;)LX/JeB;

    move-result-object v0

    iput-object v0, p0, LX/Jdq;->d:LX/JeB;

    .line 2720155
    iput-object p2, p0, LX/Jdq;->e:LX/Jds;

    .line 2720156
    iput-object p4, p0, LX/Jdq;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2720157
    iput-boolean p5, p0, LX/Jdq;->g:Z

    .line 2720158
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2720147
    iget-object v0, p0, LX/Jdq;->d:LX/JeB;

    invoke-static {}, LX/Jdp;->values()[LX/Jdp;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v0, p1, v1}, LX/JeB;->a(Landroid/view/ViewGroup;LX/Jdp;)LX/Jek;

    move-result-object v0

    .line 2720148
    new-instance v1, LX/Jdo;

    invoke-direct {v1, v0}, LX/Jdo;-><init>(LX/Jek;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2720143
    check-cast p1, LX/Jdo;

    .line 2720144
    iget-object v0, p0, LX/Jdq;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JeZ;

    .line 2720145
    iget-object p0, p1, LX/Jdo;->l:LX/Jek;

    invoke-interface {p0, v0}, LX/Jek;->a(LX/JeZ;)V

    .line 2720146
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;Lcom/facebook/user/model/User;)V
    .locals 4
    .param p1    # Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2720138
    iput-object p1, p0, LX/Jdq;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;

    .line 2720139
    iput-object p2, p0, LX/Jdq;->b:Lcom/facebook/user/model/User;

    .line 2720140
    iget-object v0, p0, LX/Jdq;->e:LX/Jds;

    iget-object v1, p0, LX/Jdq;->b:Lcom/facebook/user/model/User;

    iget-object v2, p0, LX/Jdq;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-boolean v3, p0, LX/Jdq;->g:Z

    invoke-virtual {v0, v1, v2, p1, v3}, LX/Jds;->a(Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;Z)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Jdq;->c:LX/0Px;

    .line 2720141
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2720142
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2720117
    iget-object v0, p0, LX/Jdq;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JeZ;

    .line 2720118
    instance-of v1, v0, LX/Jeb;

    if-eqz v1, :cond_0

    .line 2720119
    sget-object v0, LX/Jdp;->CHOOSE_TOPICS_TITLE:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    .line 2720120
    :goto_0
    return v0

    .line 2720121
    :cond_0
    instance-of v1, v0, LX/Jee;

    if-eqz v1, :cond_1

    .line 2720122
    sget-object v0, LX/Jdp;->MANAGE_NOTIFICATIONS:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720123
    :cond_1
    instance-of v1, v0, LX/Jef;

    if-eqz v1, :cond_2

    .line 2720124
    sget-object v0, LX/Jdp;->MANAGE_TOPIC_STATION:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720125
    :cond_2
    instance-of v1, v0, LX/Jeg;

    if-eqz v1, :cond_3

    .line 2720126
    sget-object v0, LX/Jdp;->MANAGE_TOPIC_SUBSTATION:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720127
    :cond_3
    instance-of v1, v0, LX/Jeh;

    if-eqz v1, :cond_4

    .line 2720128
    sget-object v0, LX/Jdp;->MESSAGE_TYPES_TITLE:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720129
    :cond_4
    instance-of v1, v0, LX/Jea;

    if-eqz v1, :cond_5

    .line 2720130
    sget-object v0, LX/Jdp;->BLOCK_ALL:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720131
    :cond_5
    instance-of v1, v0, LX/Jei;

    if-eqz v1, :cond_6

    .line 2720132
    sget-object v0, LX/Jdp;->UNBLOCK_ALL:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720133
    :cond_6
    instance-of v1, v0, LX/Jec;

    if-eqz v1, :cond_7

    .line 2720134
    sget-object v0, LX/Jdp;->DIVIDER:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720135
    :cond_7
    instance-of v0, v0, LX/Jed;

    if-eqz v0, :cond_8

    .line 2720136
    sget-object v0, LX/Jdp;->EXTRA_SPACE_DIVIDER:LX/Jdp;

    invoke-virtual {v0}, LX/Jdp;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2720137
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2720116
    iget-object v0, p0, LX/Jdq;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
