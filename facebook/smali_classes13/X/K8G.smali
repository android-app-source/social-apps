.class public LX/K8G;
.super LX/CH6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CH6",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Uh;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Yg;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/8bW;

.field private final d:LX/CjE;

.field private final e:LX/8bK;

.field private final f:LX/0ad;

.field private final g:LX/CjG;

.field public final h:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;LX/0Ot;LX/8bW;LX/CjE;LX/0ad;LX/8bK;LX/CjG;LX/8bL;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0t2;",
            "LX/0So;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/8Yg;",
            ">;",
            "LX/8bW;",
            "LX/CjE;",
            "LX/0ad;",
            "LX/8bK;",
            "LX/CjG;",
            "LX/8bL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2773728
    const-wide/16 v8, 0x1

    sget-object v10, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    move-object v7, p3

    invoke-direct/range {v3 .. v10}, LX/CH6;-><init>(LX/0tX;LX/0t2;LX/0Uh;LX/0So;JLjava/util/concurrent/TimeUnit;)V

    .line 2773729
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/16 v4, 0x384

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, LX/K8G;->h:LX/0QI;

    .line 2773730
    iput-object p4, p0, LX/K8G;->a:LX/0Uh;

    .line 2773731
    move-object/from16 v0, p5

    iput-object v0, p0, LX/K8G;->b:LX/0Ot;

    .line 2773732
    move-object/from16 v0, p6

    iput-object v0, p0, LX/K8G;->c:LX/8bW;

    .line 2773733
    move-object/from16 v0, p7

    iput-object v0, p0, LX/K8G;->d:LX/CjE;

    .line 2773734
    move-object/from16 v0, p9

    iput-object v0, p0, LX/K8G;->e:LX/8bK;

    .line 2773735
    move-object/from16 v0, p8

    iput-object v0, p0, LX/K8G;->f:LX/0ad;

    .line 2773736
    move-object/from16 v0, p10

    iput-object v0, p0, LX/K8G;->g:LX/CjG;

    .line 2773737
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Z)LX/K8B;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "LX/K8B;"
        }
    .end annotation

    .prologue
    .line 2773738
    new-instance v2, LX/K8B;

    invoke-direct {v2, p0, p1}, LX/K8B;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 2773739
    const-wide/16 v0, 0x384

    .line 2773740
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "feed"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2773741
    const-wide/32 v0, 0x15180

    .line 2773742
    :cond_0
    iput-wide v0, v2, LX/K8B;->e:J

    .line 2773743
    iput-boolean p3, v2, LX/K8B;->f:Z

    .line 2773744
    return-object v2
.end method

.method public static b(LX/0QB;)LX/K8G;
    .locals 12

    .prologue
    .line 2773745
    new-instance v0, LX/K8G;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v2

    check-cast v2, LX/0t2;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x31ec

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/8bW;->b(LX/0QB;)LX/8bW;

    move-result-object v6

    check-cast v6, LX/8bW;

    invoke-static {p0}, LX/CjE;->a(LX/0QB;)LX/CjE;

    move-result-object v7

    check-cast v7, LX/CjE;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v9

    check-cast v9, LX/8bK;

    invoke-static {p0}, LX/CjG;->a(LX/0QB;)LX/CjG;

    move-result-object v10

    check-cast v10, LX/CjG;

    invoke-static {p0}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v11

    check-cast v11, LX/8bL;

    invoke-direct/range {v0 .. v11}, LX/K8G;-><init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;LX/0Ot;LX/8bW;LX/CjE;LX/0ad;LX/8bK;LX/CjG;LX/8bL;)V

    .line 2773746
    return-object v0
.end method
