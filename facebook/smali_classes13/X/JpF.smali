.class public LX/JpF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/JpD;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/tiles/UserTileDrawableController;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JpE;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/tiles/UserTileDrawableController;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/Context;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/tiles/UserTileDrawableController;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/JpE;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2737116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737117
    iput-object p1, p0, LX/JpF;->b:LX/0Or;

    .line 2737118
    iput-object p2, p0, LX/JpF;->c:Landroid/content/Context;

    .line 2737119
    iput-object p3, p0, LX/JpF;->d:LX/0Ot;

    .line 2737120
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2737121
    new-instance v1, LX/JpD;

    invoke-direct {v1}, LX/JpD;-><init>()V

    iput-object v1, p0, LX/JpF;->a:LX/JpD;

    .line 2737122
    iget-object v1, p0, LX/JpF;->a:LX/JpD;

    const v2, 0x7f0b0802

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2737123
    iput v2, v1, LX/JpD;->c:I

    .line 2737124
    invoke-virtual {v1}, LX/JpD;->invalidateSelf()V

    .line 2737125
    const v1, 0x7f0b024d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2737126
    invoke-direct {p0, v3, v1, v3, v3}, LX/JpF;->a(IIII)V

    .line 2737127
    const/4 v1, 0x3

    invoke-static {v1}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/JpF;->e:Ljava/util/List;

    .line 2737128
    const v1, 0x7f0b07fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JpF;->f:I

    .line 2737129
    return-void
.end method

.method private a(IIII)V
    .locals 1

    .prologue
    .line 2737130
    iget-object v0, p0, LX/JpF;->a:LX/JpD;

    .line 2737131
    iget-object p0, v0, LX/JpD;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2737132
    return-void
.end method
