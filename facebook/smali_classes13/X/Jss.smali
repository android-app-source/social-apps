.class public final enum LX/Jss;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jss;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jss;

.field public static final enum MUTE:LX/Jss;

.field public static final enum OPEN_ON_PHONE:LX/Jss;

.field public static final enum REPLY_LIKE:LX/Jss;

.field public static final enum REPLY_STICKER:LX/Jss;

.field public static final enum REPLY_TEXT:LX/Jss;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2745967
    new-instance v0, LX/Jss;

    const-string v1, "REPLY_TEXT"

    const-string v2, "text"

    invoke-direct {v0, v1, v3, v2}, LX/Jss;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jss;->REPLY_TEXT:LX/Jss;

    .line 2745968
    new-instance v0, LX/Jss;

    const-string v1, "REPLY_STICKER"

    const-string v2, "sticker"

    invoke-direct {v0, v1, v4, v2}, LX/Jss;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jss;->REPLY_STICKER:LX/Jss;

    .line 2745969
    new-instance v0, LX/Jss;

    const-string v1, "REPLY_LIKE"

    const-string v2, "like"

    invoke-direct {v0, v1, v5, v2}, LX/Jss;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jss;->REPLY_LIKE:LX/Jss;

    .line 2745970
    new-instance v0, LX/Jss;

    const-string v1, "MUTE"

    const-string v2, "mute"

    invoke-direct {v0, v1, v6, v2}, LX/Jss;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jss;->MUTE:LX/Jss;

    .line 2745971
    new-instance v0, LX/Jss;

    const-string v1, "OPEN_ON_PHONE"

    const-string v2, "open"

    invoke-direct {v0, v1, v7, v2}, LX/Jss;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jss;->OPEN_ON_PHONE:LX/Jss;

    .line 2745972
    const/4 v0, 0x5

    new-array v0, v0, [LX/Jss;

    sget-object v1, LX/Jss;->REPLY_TEXT:LX/Jss;

    aput-object v1, v0, v3

    sget-object v1, LX/Jss;->REPLY_STICKER:LX/Jss;

    aput-object v1, v0, v4

    sget-object v1, LX/Jss;->REPLY_LIKE:LX/Jss;

    aput-object v1, v0, v5

    sget-object v1, LX/Jss;->MUTE:LX/Jss;

    aput-object v1, v0, v6

    sget-object v1, LX/Jss;->OPEN_ON_PHONE:LX/Jss;

    aput-object v1, v0, v7

    sput-object v0, LX/Jss;->$VALUES:[LX/Jss;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2745964
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2745965
    iput-object p3, p0, LX/Jss;->name:Ljava/lang/String;

    .line 2745966
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jss;
    .locals 1

    .prologue
    .line 2745961
    const-class v0, LX/Jss;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jss;

    return-object v0
.end method

.method public static values()[LX/Jss;
    .locals 1

    .prologue
    .line 2745963
    sget-object v0, LX/Jss;->$VALUES:[LX/Jss;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jss;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2745962
    iget-object v0, p0, LX/Jss;->name:Ljava/lang/String;

    return-object v0
.end method
