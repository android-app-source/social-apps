.class public final enum LX/Jf0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jf0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jf0;

.field public static final enum AD_CLICK:LX/Jf0;

.field public static final enum AD_HIDE:LX/Jf0;

.field public static final enum AD_IMPRESSION:LX/Jf0;

.field public static final enum AD_IMPRESSION_TIMESPENT:LX/Jf0;

.field public static final enum AD_USEFUL:LX/Jf0;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2721396
    new-instance v0, LX/Jf0;

    const-string v1, "AD_IMPRESSION"

    const-string v2, "inbox_ad_impression"

    invoke-direct {v0, v1, v3, v2}, LX/Jf0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf0;->AD_IMPRESSION:LX/Jf0;

    .line 2721397
    new-instance v0, LX/Jf0;

    const-string v1, "AD_IMPRESSION_TIMESPENT"

    const-string v2, "inbox_ad_impression_timespent"

    invoke-direct {v0, v1, v4, v2}, LX/Jf0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf0;->AD_IMPRESSION_TIMESPENT:LX/Jf0;

    .line 2721398
    new-instance v0, LX/Jf0;

    const-string v1, "AD_CLICK"

    const-string v2, "inbox_ad_link_click"

    invoke-direct {v0, v1, v5, v2}, LX/Jf0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf0;->AD_CLICK:LX/Jf0;

    .line 2721399
    new-instance v0, LX/Jf0;

    const-string v1, "AD_USEFUL"

    const-string v2, "inbox_ad_useful"

    invoke-direct {v0, v1, v6, v2}, LX/Jf0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf0;->AD_USEFUL:LX/Jf0;

    .line 2721400
    new-instance v0, LX/Jf0;

    const-string v1, "AD_HIDE"

    const-string v2, "inbox_ad_hide"

    invoke-direct {v0, v1, v7, v2}, LX/Jf0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf0;->AD_HIDE:LX/Jf0;

    .line 2721401
    const/4 v0, 0x5

    new-array v0, v0, [LX/Jf0;

    sget-object v1, LX/Jf0;->AD_IMPRESSION:LX/Jf0;

    aput-object v1, v0, v3

    sget-object v1, LX/Jf0;->AD_IMPRESSION_TIMESPENT:LX/Jf0;

    aput-object v1, v0, v4

    sget-object v1, LX/Jf0;->AD_CLICK:LX/Jf0;

    aput-object v1, v0, v5

    sget-object v1, LX/Jf0;->AD_USEFUL:LX/Jf0;

    aput-object v1, v0, v6

    sget-object v1, LX/Jf0;->AD_HIDE:LX/Jf0;

    aput-object v1, v0, v7

    sput-object v0, LX/Jf0;->$VALUES:[LX/Jf0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2721404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2721405
    iput-object p3, p0, LX/Jf0;->value:Ljava/lang/String;

    .line 2721406
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jf0;
    .locals 1

    .prologue
    .line 2721407
    const-class v0, LX/Jf0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jf0;

    return-object v0
.end method

.method public static values()[LX/Jf0;
    .locals 1

    .prologue
    .line 2721403
    sget-object v0, LX/Jf0;->$VALUES:[LX/Jf0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jf0;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2721402
    iget-object v0, p0, LX/Jf0;->value:Ljava/lang/String;

    return-object v0
.end method
