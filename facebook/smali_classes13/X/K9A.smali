.class public final LX/K9A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/K7K;

.field public final synthetic b:Z

.field public final synthetic c:LX/K9E;


# direct methods
.method public constructor <init>(LX/K9E;LX/K7K;Z)V
    .locals 0

    .prologue
    .line 2776744
    iput-object p1, p0, LX/K9A;->c:LX/K9E;

    iput-object p2, p0, LX/K9A;->a:LX/K7K;

    iput-boolean p3, p0, LX/K9A;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 2776745
    iget-object v0, p0, LX/K9A;->a:LX/K7K;

    iget-boolean v1, p0, LX/K9A;->b:Z

    .line 2776746
    iget-object p0, v0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    if-nez p0, :cond_1

    .line 2776747
    :cond_0
    :goto_0
    return-void

    .line 2776748
    :cond_1
    invoke-virtual {v0}, LX/K7K;->b()Z

    move-result p0

    if-eq v1, p0, :cond_0

    .line 2776749
    if-eqz v1, :cond_2

    .line 2776750
    iget-object p0, v0, LX/K7K;->a:LX/K8f;

    invoke-static {v0}, LX/K7K;->d(LX/K7K;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/K8f;->a(Ljava/lang/String;)V

    .line 2776751
    :goto_1
    goto :goto_0

    .line 2776752
    :cond_2
    iget-object p0, v0, LX/K7K;->a:LX/K8f;

    invoke-static {v0}, LX/K7K;->d(LX/K7K;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/K8f;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
