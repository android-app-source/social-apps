.class public LX/JoF;
.super LX/3hi;
.source ""


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final d:I

.field public final e:Z

.field public final synthetic f:LX/JoH;


# direct methods
.method public constructor <init>(LX/JoH;Ljava/lang/String;I)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2735307
    const/high16 v3, -0x1000000

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, LX/JoF;-><init>(LX/JoH;Ljava/lang/String;IIZ)V

    .line 2735308
    return-void
.end method

.method public constructor <init>(LX/JoH;Ljava/lang/String;IIZ)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2735309
    iput-object p1, p0, LX/JoF;->f:LX/JoH;

    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 2735310
    iput-object p2, p0, LX/JoF;->b:Ljava/lang/String;

    .line 2735311
    iput-boolean p5, p0, LX/JoF;->e:Z

    .line 2735312
    iput p3, p0, LX/JoF;->c:I

    .line 2735313
    iput p4, p0, LX/JoF;->d:I

    .line 2735314
    return-void
.end method


# virtual methods
.method public a()LX/1bh;
    .locals 3

    .prologue
    .line 2735306
    new-instance v0, LX/1ed;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v2, p0, LX/JoF;->e:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/JoF;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 10

    .prologue
    const/4 v6, 0x4

    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 2735282
    iget v0, p0, LX/JoF;->c:I

    .line 2735283
    iget-boolean v1, p0, LX/JoF;->e:Z

    if-eqz v1, :cond_1

    .line 2735284
    iget-object v1, p0, LX/JoF;->f:LX/JoH;

    iget-object v1, v1, LX/JoH;->a:LX/IiT;

    iget-object v2, p0, LX/JoF;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, LX/IiT;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)LX/HiI;

    move-result-object v2

    .line 2735285
    if-eqz v2, :cond_1

    .line 2735286
    iget-object v1, v2, LX/HiI;->b:LX/HiE;

    invoke-virtual {v1}, LX/HiE;->b()LX/HiH;

    move-result-object v1

    move-object v1, v1

    .line 2735287
    if-nez v1, :cond_0

    .line 2735288
    iget-object v1, v2, LX/HiI;->b:LX/HiE;

    invoke-virtual {v1}, LX/HiE;->c()LX/HiH;

    move-result-object v1

    move-object v1, v1

    .line 2735289
    :cond_0
    if-eqz v1, :cond_1

    .line 2735290
    iget v0, v1, LX/HiH;->d:I

    move v0, v0

    .line 2735291
    :cond_1
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2735292
    iget v1, p0, LX/JoF;->d:I

    if-ne v1, v7, :cond_2

    .line 2735293
    const/16 v1, 0x90

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2735294
    :goto_0
    return-void

    .line 2735295
    :cond_2
    iget v1, p0, LX/JoF;->d:I

    if-ne v1, v9, :cond_3

    .line 2735296
    const/16 v1, 0xcc

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0

    .line 2735297
    :cond_3
    new-array v5, v6, [I

    invoke-static {v0, v3}, LX/3qk;->b(II)I

    move-result v1

    aput v1, v5, v3

    const/16 v1, 0x40

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v1

    aput v1, v5, v7

    const/16 v1, 0xd9

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v1

    aput v1, v5, v9

    const/16 v1, 0xff

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v1

    aput v1, v5, v4

    .line 2735298
    new-array v6, v6, [F

    fill-array-data v6, :array_0

    .line 2735299
    iget v1, p0, LX/JoF;->d:I

    if-ne v1, v4, :cond_4

    .line 2735300
    new-array v5, v4, [I

    invoke-static {v0, v3}, LX/3qk;->b(II)I

    move-result v1

    aput v1, v5, v3

    const/16 v1, 0x3d

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v1

    aput v1, v5, v7

    const/16 v1, 0xbf

    invoke-static {v0, v1}, LX/3qk;->b(II)I

    move-result v0

    aput v0, v5, v9

    .line 2735301
    new-array v6, v4, [F

    fill-array-data v6, :array_1

    .line 2735302
    :cond_4
    new-instance v9, Landroid/graphics/Paint;

    const/4 v0, 0x5

    invoke-direct {v9, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 2735303
    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2735304
    invoke-virtual {v8, v9}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3e4ccccd    # 0.2f
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
    .end array-data

    .line 2735305
    :array_1
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3f000000    # 0.5f
        0x3f7d70a4    # 0.99f
    .end array-data
.end method
