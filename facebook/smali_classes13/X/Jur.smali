.class public final LX/Jur;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Q0;


# direct methods
.method public constructor <init>(LX/2Q0;)V
    .locals 0

    .prologue
    .line 2749525
    iput-object p1, p0, LX/Jur;->a:LX/2Q0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 2749526
    check-cast p1, LX/Jlv;

    .line 2749527
    iget-object v0, p0, LX/Jur;->a:LX/2Q0;

    iget-object v0, v0, LX/2Q0;->j:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    .line 2749528
    iget-object v1, p1, LX/Jlv;->b:LX/JlX;

    move-object v1, v1

    .line 2749529
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 2749530
    iget-object v4, v1, LX/JlX;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JlS;

    .line 2749531
    invoke-static {v0, v1}, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->a(Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;LX/JlS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2749532
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2749533
    :cond_0
    invoke-static {v3}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/Jl9;

    invoke-direct {v2, v0}, LX/Jl9;-><init>(Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;)V

    .line 2749534
    sget-object v3, LX/131;->INSTANCE:LX/131;

    move-object v3, v3

    .line 2749535
    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2749536
    return-object v0
.end method
