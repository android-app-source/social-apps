.class public LX/Jth;
.super LX/GmZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GmZ",
        "<",
        "LX/0zO",
        "<",
        "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final ab:Ljava/lang/String;


# instance fields
.field public X:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/Jtn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2747505
    const-class v0, LX/Jth;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Jth;->ab:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2747504
    invoke-direct {p0}, LX/GmZ;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Jth;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/Jtn;->a(LX/0QB;)LX/Jtn;

    move-result-object v2

    check-cast v2, LX/Jtn;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object p0

    check-cast p0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object v1, p1, LX/Jth;->X:LX/Chv;

    iput-object v2, p1, LX/Jth;->Y:LX/Jtn;

    iput-object v3, p1, LX/Jth;->Z:LX/03V;

    iput-object p0, p1, LX/Jth;->aa:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    return-void
.end method

.method private static d(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2747501
    if-eqz p0, :cond_0

    const-string v0, "extra_notes_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2747502
    :cond_0
    const/4 v0, 0x0

    .line 2747503
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "extra_notes_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final D()LX/CH4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/richdocument/fetcher/RichDocumentFetcher",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2747500
    iget-object v0, p0, LX/Jth;->Y:LX/Jtn;

    return-object v0
.end method

.method public final E()LX/CGs;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2747497
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2747498
    invoke-static {v0}, LX/Jth;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2747499
    new-instance v1, LX/Jtm;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/Jtm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v1
.end method

.method public final O()LX/0Pq;
    .locals 1

    .prologue
    .line 2747506
    sget-object v0, LX/Jtj;->a:LX/Jti;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2747461
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2747462
    invoke-static {v0}, LX/Jth;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2747496
    const-string v0, "native_notes"

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/Clo;
    .locals 6

    .prologue
    .line 2747477
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2747478
    if-eqz p1, :cond_0

    .line 2747479
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2747480
    if-nez v0, :cond_2

    .line 2747481
    :cond_0
    iget-object v2, p0, LX/Jth;->Z:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/Jth;->ab:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".onParseModel"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Null GraphQLResult"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v0, " "

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "for note id("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2747482
    iget-object v4, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v4, v4

    .line 2747483
    invoke-static {v4}, LX/Jth;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    move-object v0, v1

    .line 2747484
    :goto_1
    return-object v0

    .line 2747485
    :cond_1
    const-string v0, ".getResult "

    goto :goto_0

    .line 2747486
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2747487
    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2747488
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2747489
    iget-object v2, p0, LX/GmZ;->W:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/notes/NotesDelegateImpl$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/facebook/notes/NotesDelegateImpl$1;-><init>(LX/Jth;Lcom/facebook/graphql/executor/GraphQLResult;Landroid/content/Context;)V

    const-wide/16 v4, 0x3e8

    const v0, -0x1429a2ca

    invoke-static {v2, v3, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    move-object v0, v1

    .line 2747490
    goto :goto_1

    .line 2747491
    :cond_3
    new-instance v1, LX/Jtu;

    .line 2747492
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2747493
    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    invoke-direct {v1, v0}, LX/Jtu;-><init>(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;)V

    .line 2747494
    new-instance v0, LX/Ju7;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Ju7;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/Ju7;->a(LX/Jtu;)LX/Clo;

    move-result-object v0

    .line 2747495
    iget-object v1, p0, LX/Jth;->X:LX/Chv;

    new-instance v2, LX/CiP;

    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-direct {v2, v0, v3}, LX/CiP;-><init>(LX/Clo;LX/0ta;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2747475
    const-class v0, LX/Jth;

    invoke-static {v0, p0}, LX/Jth;->a(Ljava/lang/Class;LX/02k;)V

    .line 2747476
    invoke-super {p0, p1, p2, p3}, LX/GmZ;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2747470
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2747471
    const-string v1, "note_id"

    .line 2747472
    iget-object v2, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v2, v2

    .line 2747473
    invoke-static {v2}, LX/Jth;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2747474
    return-object v0
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2747463
    iget-object v0, p0, LX/Jth;->Z:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Jth;->ab:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".onFetchError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error attempting to fetch blocks. note id("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2747464
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2747465
    invoke-static {v3}, LX/Jth;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2747466
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2747467
    move-object v1, v1

    .line 2747468
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2747469
    return-void
.end method
