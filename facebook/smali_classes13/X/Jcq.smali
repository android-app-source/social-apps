.class public LX/Jcq;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Jcj;",
        ">;",
        "Lcom/facebook/messaging/accountswitch/GenericAccountRowMenuOptionsCallback;",
        "Lcom/facebook/messaging/accountswitch/model/MessengerAccountsProvider$Listener;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/LayoutInflater;

.field private final d:LX/Jd3;

.field private final e:LX/0Uh;

.field private final f:LX/4Ad;

.field private final g:LX/6lJ;

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final i:LX/0Sh;

.field private final j:LX/0ad;

.field private final k:Landroid/view/View$OnClickListener;

.field public l:LX/JdA;

.field public m:LX/2Os;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;Landroid/view/LayoutInflater;LX/Jd3;LX/0Uh;LX/4Ad;LX/6lJ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0ad;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/view/LayoutInflater;",
            "LX/Jd3;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/4Ad;",
            "LX/6lJ;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2718411
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2718412
    new-instance v0, LX/Jcp;

    invoke-direct {v0, p0}, LX/Jcp;-><init>(LX/Jcq;)V

    iput-object v0, p0, LX/Jcq;->k:Landroid/view/View$OnClickListener;

    .line 2718413
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    .line 2718414
    iput-object p1, p0, LX/Jcq;->a:Landroid/content/Context;

    .line 2718415
    iput-object p2, p0, LX/Jcq;->b:LX/0Or;

    .line 2718416
    iput-object p3, p0, LX/Jcq;->c:Landroid/view/LayoutInflater;

    .line 2718417
    iput-object p4, p0, LX/Jcq;->d:LX/Jd3;

    .line 2718418
    iput-object p5, p0, LX/Jcq;->e:LX/0Uh;

    .line 2718419
    iput-object p6, p0, LX/Jcq;->f:LX/4Ad;

    .line 2718420
    iput-object p7, p0, LX/Jcq;->g:LX/6lJ;

    .line 2718421
    iput-object p8, p0, LX/Jcq;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2718422
    iput-object p9, p0, LX/Jcq;->i:LX/0Sh;

    .line 2718423
    iput-object p10, p0, LX/Jcq;->j:LX/0ad;

    .line 2718424
    return-void
.end method

.method public static a(LX/0QB;)LX/Jcq;
    .locals 12

    .prologue
    .line 2718425
    new-instance v1, LX/Jcq;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 2718426
    new-instance v7, LX/Jd3;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {v7, v5, v6}, LX/Jd3;-><init>(Landroid/content/res/Resources;LX/0SG;)V

    .line 2718427
    move-object v5, v7

    .line 2718428
    check-cast v5, LX/Jd3;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p0}, LX/41d;->a(LX/0QB;)LX/4Ad;

    move-result-object v7

    check-cast v7, LX/4Ad;

    invoke-static {p0}, LX/6lJ;->b(LX/0QB;)LX/6lJ;

    move-result-object v8

    check-cast v8, LX/6lJ;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v1 .. v11}, LX/Jcq;-><init>(Landroid/content/Context;LX/0Or;Landroid/view/LayoutInflater;LX/Jd3;LX/0Uh;LX/4Ad;LX/6lJ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0ad;)V

    .line 2718429
    move-object v0, v1

    .line 2718430
    return-object v0
.end method

.method public static f(LX/Jcq;)V
    .locals 6

    .prologue
    .line 2718431
    iget-object v0, p0, LX/Jcq;->m:LX/2Os;

    invoke-virtual {v0}, LX/2Os;->a()Ljava/util/List;

    move-result-object v0

    .line 2718432
    iget-object v1, p0, LX/Jcq;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2718433
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 2718434
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718435
    iget-object v5, v2, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-static {v5, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2718436
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v2}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 2718437
    :cond_0
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2718438
    :cond_1
    move-object v0, v3

    .line 2718439
    iput-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    .line 2718440
    iget-object v0, p0, LX/Jcq;->i:LX/0Sh;

    new-instance v1, Lcom/facebook/messaging/accountswitch/AccountsListAdapter$2;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/accountswitch/AccountsListAdapter$2;-><init>(LX/Jcq;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2718441
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 8

    .prologue
    .line 2718442
    const/4 v3, 0x0

    .line 2718443
    packed-switch p2, :pswitch_data_0

    .line 2718444
    :pswitch_0
    iget-object v0, p0, LX/Jcq;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03001e

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2718445
    new-instance v0, LX/Jcn;

    iget-object v2, p0, LX/Jcq;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Jcq;->d:LX/Jd3;

    iget-object v4, p0, LX/Jcq;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v5, p0, LX/Jcq;->i:LX/0Sh;

    iget-object v6, p0, LX/Jcq;->g:LX/6lJ;

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, LX/Jcn;-><init>(Landroid/view/View;Landroid/content/Context;LX/Jd3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/6lJ;LX/Jcq;)V

    .line 2718446
    :goto_0
    iget-object v2, p0, LX/Jcq;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2718447
    return-object v0

    .line 2718448
    :pswitch_1
    iget-object v0, p0, LX/Jcq;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03001e

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2718449
    iget-object v0, p0, LX/Jcq;->e:LX/0Uh;

    const/16 v2, 0xfc

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 2718450
    new-instance v0, LX/Jd6;

    iget-object v2, p0, LX/Jcq;->a:Landroid/content/Context;

    iget-object v4, p0, LX/Jcq;->f:LX/4Ad;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, LX/Jd6;-><init>(Landroid/view/View;Landroid/content/Context;LX/Jcq;LX/4Ad;Z)V

    goto :goto_0

    .line 2718451
    :pswitch_2
    iget-object v0, p0, LX/Jcq;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03009a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2718452
    new-instance v0, LX/Jct;

    invoke-direct {v0, v1}, LX/Jct;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2718453
    check-cast p1, LX/Jcj;

    .line 2718454
    iget-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    iget-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718455
    :goto_0
    invoke-virtual {p1, v0}, LX/Jcj;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    .line 2718456
    return-void

    .line 2718457
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2718458
    iget-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2718459
    const/4 v0, 0x2

    .line 2718460
    :goto_0
    return v0

    .line 2718461
    :cond_0
    iget-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718462
    if-nez p1, :cond_2

    .line 2718463
    iget-object v1, p0, LX/Jcq;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2718464
    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 2718465
    :goto_1
    if-eqz v0, :cond_1

    move v0, v2

    .line 2718466
    goto :goto_0

    .line 2718467
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final ij_()I
    .locals 3

    .prologue
    .line 2718468
    iget-object v0, p0, LX/Jcq;->j:LX/0ad;

    sget-short v1, LX/Jd1;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
