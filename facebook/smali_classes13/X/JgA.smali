.class public LX/JgA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Landroid/view/ViewGroup;

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Lcom/facebook/widget/text/BetterTextView;

.field public d:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 2722522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722523
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2722524
    const v1, 0x7f030a71

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/JgA;->a:Landroid/view/ViewGroup;

    .line 2722525
    iget-object v0, p0, LX/JgA;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d02c4

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/JgA;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2722526
    iget-object v0, p0, LX/JgA;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d0550

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/JgA;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2722527
    iget-object v0, p0, LX/JgA;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d1a94

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/JgA;->d:LX/4ob;

    .line 2722528
    iget-object v0, p0, LX/JgA;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d1a95

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/JgA;->e:LX/4ob;

    .line 2722529
    return-void
.end method
