.class public final LX/JpV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/0Px",
        "<",
        "LX/DAC;",
        ">;",
        "LX/Jpb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V
    .locals 0

    .prologue
    .line 2737611
    iput-object p1, p0, LX/JpV;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 2737612
    check-cast p1, LX/0Px;

    .line 2737613
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2737614
    :cond_0
    sget-object v0, LX/Jpb;->a:LX/Jpb;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2737615
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/JpV;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-static {v0, p1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a$redex0(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
