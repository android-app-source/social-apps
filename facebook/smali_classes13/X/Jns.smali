.class public final LX/Jns;
.super LX/1OM;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

.field private final b:I

.field private final c:I

.field private final d:I

.field public e:LX/Dhc;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2734603
    iput-object p1, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2734604
    const/4 v0, 0x0

    iput v0, p0, LX/Jns;->b:I

    .line 2734605
    iput v1, p0, LX/Jns;->c:I

    .line 2734606
    const/4 v0, 0x2

    iput v0, p0, LX/Jns;->d:I

    .line 2734607
    invoke-virtual {p0, v1}, LX/1OM;->a(Z)V

    .line 2734608
    return-void
.end method

.method public static a$redex0(LX/Jns;Lcom/facebook/messaging/montage/composer/art/EffectItemView;)V
    .locals 6

    .prologue
    .line 2734609
    iget-object v0, p1, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->e:Lcom/facebook/messaging/montage/model/art/EffectItem;

    move-object v0, v0

    .line 2734610
    iget-object v1, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v1, v1, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    .line 2734611
    invoke-virtual {v1, v0}, LX/Jnx;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v2

    sget-object v3, LX/Jnw;->COMPLETED:LX/Jnw;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2734612
    if-nez v1, :cond_1

    .line 2734613
    iget-object v1, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v1, v1, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    new-instance v2, LX/Jnr;

    invoke-direct {v2, p0, p1}, LX/Jnr;-><init>(LX/Jns;Lcom/facebook/messaging/montage/composer/art/EffectItemView;)V

    .line 2734614
    invoke-virtual {v1, v0}, LX/Jnx;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v3

    sget-object v4, LX/Jnw;->NOT_STARTED:LX/Jnw;

    if-ne v3, v4, :cond_0

    .line 2734615
    new-instance v4, LX/Jnu;

    invoke-direct {v4, v1, v2, v0}, LX/Jnu;-><init>(LX/Jnx;LX/Jnr;Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734616
    sget-object v3, LX/Jnv;->a:[I

    .line 2734617
    iget-object v5, v0, Lcom/facebook/messaging/montage/model/art/EffectItem;->e:LX/Dhg;

    move-object v5, v5

    .line 2734618
    invoke-virtual {v5}, LX/Dhg;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 2734619
    :cond_0
    :goto_1
    iget-object v1, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v1, v1, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    invoke-virtual {v1, v0}, LX/Jnx;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->a(LX/Jnw;)V

    .line 2734620
    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2734621
    :pswitch_0
    iget-object v3, v1, LX/Jnx;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Jnz;

    invoke-virtual {v3, v0}, LX/Jnz;->b(Lcom/facebook/messaging/montage/model/art/EffectItem;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2734622
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 2734623
    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2734624
    :pswitch_1
    iget-object v3, v1, LX/Jnx;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    invoke-virtual {v3, v0}, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b(Lcom/facebook/messaging/montage/model/art/EffectItem;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2734625
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 2734626
    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2734627
    :pswitch_2
    iget-object v3, v1, LX/Jnx;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Jo3;

    invoke-virtual {v3, v0}, LX/Jo3;->b(Lcom/facebook/messaging/montage/model/art/EffectItem;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2734628
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 2734629
    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private e(I)Lcom/facebook/messaging/montage/model/art/BaseItem;
    .locals 1

    .prologue
    .line 2734594
    iget-object v0, p0, LX/Jns;->e:LX/Dhc;

    iget-object v0, v0, LX/Dhc;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/BaseItem;

    return-object v0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2734591
    invoke-virtual {p0, p1}, LX/Jns;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 2734592
    const-wide/16 v0, -0x1

    .line 2734593
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/Jns;->e:LX/Dhc;

    iget-object v0, v0, LX/Dhc;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/BaseItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/model/art/BaseItem;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2734595
    packed-switch p2, :pswitch_data_0

    .line 2734596
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown viewtype: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734597
    :pswitch_0
    new-instance v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;-><init>(Landroid/content/Context;)V

    .line 2734598
    new-instance v1, LX/Jnp;

    invoke-direct {v1, p0}, LX/Jnp;-><init>(LX/Jns;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2734599
    :goto_0
    new-instance v1, LX/62U;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1

    .line 2734600
    :pswitch_1
    new-instance v0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;-><init>(Landroid/content/Context;)V

    .line 2734601
    new-instance v1, LX/Jnq;

    invoke-direct {v1, p0}, LX/Jnq;-><init>(LX/Jns;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2734602
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030115

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2734567
    invoke-virtual {p0, p2}, LX/Jns;->getItemViewType(I)I

    move-result v0

    .line 2734568
    if-ne v0, v2, :cond_1

    .line 2734569
    invoke-direct {p0, p2}, LX/Jns;->e(I)Lcom/facebook/messaging/montage/model/art/BaseItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/ArtItem;

    .line 2734570
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    .line 2734571
    iget-object v2, p0, LX/Jns;->e:LX/Dhc;

    .line 2734572
    iput-object v2, v1, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->m:LX/Dhc;

    .line 2734573
    sget-object v2, LX/IqD;->PREVIEW:LX/IqD;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Lcom/facebook/messaging/montage/model/art/ArtItem;LX/IqD;)V

    .line 2734574
    iget-object v2, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v2, v2, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->e:Lcom/facebook/messaging/montage/model/art/BaseItem;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/montage/model/art/ArtItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setSelected(Z)V

    .line 2734575
    :cond_0
    :goto_0
    return-void

    .line 2734576
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2734577
    invoke-direct {p0, p2}, LX/Jns;->e(I)Lcom/facebook/messaging/montage/model/art/BaseItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/EffectItem;

    .line 2734578
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/messaging/montage/composer/art/EffectItemView;

    .line 2734579
    iget-object v2, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v2, v2, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    invoke-virtual {v2, v0}, LX/Jnx;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v2

    .line 2734580
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/montage/model/art/EffectItem;

    iput-object v3, v1, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->e:Lcom/facebook/messaging/montage/model/art/EffectItem;

    .line 2734581
    iget-object v3, v1, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->d:LX/4ob;

    invoke-virtual {v3}, LX/4ob;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2734582
    iget-object p1, v0, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    move-object p1, p1

    .line 2734583
    sget-object p2, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2734584
    invoke-virtual {v1, v2}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->a(LX/Jnw;)V

    .line 2734585
    iget-object v2, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v2, v2, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->e:Lcom/facebook/messaging/montage/model/art/BaseItem;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/montage/model/art/EffectItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->setSelected(Z)V

    goto :goto_0

    .line 2734586
    :cond_2
    if-nez v0, :cond_0

    .line 2734587
    iget-object v0, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->c:LX/Jnt;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-boolean v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->d:Z

    if-nez v0, :cond_0

    .line 2734588
    iget-object v0, p0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    .line 2734589
    iput-boolean v2, v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->d:Z

    .line 2734590
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2734557
    iget-object v0, p0, LX/Jns;->e:LX/Dhc;

    iget-object v0, v0, LX/Dhc;->a:LX/Dhb;

    .line 2734558
    iget-boolean v1, v0, LX/Dhb;->a:Z

    move v0, v1

    .line 2734559
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Jns;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2734560
    const/4 v0, 0x0

    .line 2734561
    :goto_0
    return v0

    .line 2734562
    :cond_0
    iget-object v0, p0, LX/Jns;->e:LX/Dhc;

    iget-object v0, v0, LX/Dhc;->a:LX/Dhb;

    .line 2734563
    iget-boolean v1, v0, LX/Dhb;->b:Z

    move v0, v1

    .line 2734564
    if-eqz v0, :cond_1

    .line 2734565
    const/4 v0, 0x2

    goto :goto_0

    .line 2734566
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2734554
    iget-object v0, p0, LX/Jns;->e:LX/Dhc;

    iget-object v0, v0, LX/Dhc;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    iget-object v0, p0, LX/Jns;->e:LX/Dhc;

    iget-object v0, v0, LX/Dhc;->a:LX/Dhb;

    .line 2734555
    iget-boolean p0, v0, LX/Dhb;->a:Z

    move v0, p0

    .line 2734556
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
