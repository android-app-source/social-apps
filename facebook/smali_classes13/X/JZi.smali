.class public final LX/JZi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2708922
    iput-object p1, p0, LX/JZi;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;

    iput-object p2, p0, LX/JZi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p3, p0, LX/JZi;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, -0x5f2093b0

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2708923
    iget-object v0, p0, LX/JZi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2708924
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v2

    .line 2708925
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    .line 2708926
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2708927
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2708928
    iget-object v3, p0, LX/JZi;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;

    iget-object v3, v3, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->f:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2708929
    iget-object v0, p0, LX/JZi;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v2, p0, LX/JZi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    sget-object v3, LX/Cfc;->VIEW_FRIEND_REQUEST_PROFILE_TAP:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2708930
    iget-object v0, p0, LX/JZi;->b:LX/1Pq;

    new-array v2, v7, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/JZi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2708931
    const v0, 0x2ec3e9a3

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
