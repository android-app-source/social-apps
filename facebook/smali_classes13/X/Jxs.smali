.class public LX/Jxs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Emj;


# instance fields
.field private final a:LX/Jxq;

.field private final b:LX/Jxt;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Jxq;LX/Jxt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2753706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753707
    iput-object p1, p0, LX/Jxs;->a:LX/Jxq;

    .line 2753708
    iput-object p2, p0, LX/Jxs;->b:LX/Jxt;

    .line 2753709
    iput-object p3, p0, LX/Jxs;->c:Ljava/lang/String;

    .line 2753710
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2753698
    iget-object v0, p0, LX/Jxs;->a:LX/Jxq;

    iget-object v1, p0, LX/Jxs;->c:Ljava/lang/String;

    .line 2753699
    iget-object v2, v0, LX/Jxq;->b:LX/0Zb;

    const-string v3, "profile_discovery_event"

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2753700
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2753701
    const-string v3, "profile_discovery_cog_click"

    invoke-static {v0, v2, v3}, LX/Jxq;->d(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753702
    const-string v3, "bucket_id"

    invoke-virtual {v2, v3, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753703
    const-string v3, "selected_type"

    const-string p0, "single_edit"

    invoke-virtual {v2, v3, p0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753704
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2753705
    :cond_0
    return-void
.end method

.method public final a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emo;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2753687
    instance-of v0, p5, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    if-eqz v0, :cond_1

    .line 2753688
    iget-object v0, p0, LX/Jxs;->a:LX/Jxq;

    check-cast p5, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2753689
    iget-object p0, v0, LX/Jxq;->b:LX/0Zb;

    const-string p3, "profile_discovery_event"

    const/4 p4, 0x0

    invoke-interface {p0, p3, p4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2753690
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 2753691
    const-string p3, "entity_card_click"

    invoke-static {v0, p0, p3}, LX/Jxq;->d(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753692
    invoke-static {p5, p0}, LX/Jxq;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/0oG;)V

    .line 2753693
    const-string p3, "tap_action_surface"

    invoke-virtual {p1}, LX/Emo;->name()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p3, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753694
    if-eqz p2, :cond_0

    .line 2753695
    const-string p3, "selected_fbid"

    invoke-virtual {p0, p3, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753696
    :cond_0
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 2753697
    :cond_1
    return-void
.end method

.method public final a(LX/EnB;)V
    .locals 0

    .prologue
    .line 2753711
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2753686
    return-void
.end method

.method public final a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2753664
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;D)V
    .locals 1

    .prologue
    .line 2753681
    iget-object v0, p0, LX/Jxs;->b:LX/Jxt;

    .line 2753682
    instance-of p0, p2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    if-eqz p0, :cond_0

    .line 2753683
    check-cast p2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2753684
    iget-object p0, v0, LX/Jxt;->b:LX/0UE;

    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 2753685
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2753665
    iget-object v0, p0, LX/Jxs;->b:LX/Jxt;

    .line 2753666
    iget-object v1, v0, LX/Jxt;->a:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    iget-object v2, v0, LX/Jxt;->b:LX/0UE;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2753667
    new-instance v4, LX/4Ie;

    invoke-direct {v4}, LX/4Ie;-><init>()V

    iget-object v3, v1, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2753668
    const-string p0, "actor_id"

    invoke-virtual {v4, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2753669
    move-object v3, v4

    .line 2753670
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2753671
    const-string p0, "client_mutation_id"

    invoke-virtual {v3, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2753672
    move-object v3, v3

    .line 2753673
    const-string v4, "item_tokens"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2753674
    move-object v3, v3

    .line 2753675
    new-instance v4, LX/FTy;

    invoke-direct {v4}, LX/FTy;-><init>()V

    move-object v4, v4

    .line 2753676
    const-string p0, "input"

    invoke-virtual {v4, p0, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2753677
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2753678
    iget-object v4, v1, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2753679
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    iput-object v1, v0, LX/Jxt;->b:LX/0UE;

    .line 2753680
    return-void
.end method
