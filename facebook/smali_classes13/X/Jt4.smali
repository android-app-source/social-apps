.class public final LX/Jt4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/facebook/messengerwear/shared/Message;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messengerwear/shared/Message;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2746255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Jt4;->e:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(I)LX/Jt4;
    .locals 0

    .prologue
    .line 2746240
    iput p1, p0, LX/Jt4;->b:I

    .line 2746241
    return-object p0
.end method

.method public final a(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;
    .locals 0

    .prologue
    .line 2746242
    iput-object p1, p0, LX/Jt4;->d:Lcom/facebook/messengerwear/shared/Message;

    .line 2746243
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/Jt4;
    .locals 0

    .prologue
    .line 2746244
    iput-object p1, p0, LX/Jt4;->a:Ljava/lang/String;

    .line 2746245
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/Jt4;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/Jt4;"
        }
    .end annotation

    .prologue
    .line 2746246
    iput-object p1, p0, LX/Jt4;->f:Ljava/util/List;

    .line 2746247
    return-object p0
.end method

.method public final a()Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;
    .locals 7

    .prologue
    .line 2746248
    new-instance v0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    iget-object v1, p0, LX/Jt4;->a:Ljava/lang/String;

    iget v2, p0, LX/Jt4;->b:I

    iget-object v3, p0, LX/Jt4;->c:Ljava/lang/String;

    iget-object v4, p0, LX/Jt4;->d:Lcom/facebook/messengerwear/shared/Message;

    iget-object v5, p0, LX/Jt4;->e:Ljava/util/List;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    iget-object v6, p0, LX/Jt4;->f:Ljava/util/List;

    if-nez v6, :cond_0

    .line 2746249
    sget-object v6, LX/0Q7;->a:LX/0Px;

    move-object v6, v6

    .line 2746250
    :goto_0
    invoke-direct/range {v0 .. v6}, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/messengerwear/shared/Message;LX/0Px;LX/0Px;)V

    return-object v0

    :cond_0
    iget-object v6, p0, LX/Jt4;->f:Ljava/util/List;

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;
    .locals 1

    .prologue
    .line 2746251
    iget-object v0, p0, LX/Jt4;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2746252
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/Jt4;
    .locals 0

    .prologue
    .line 2746253
    iput-object p1, p0, LX/Jt4;->c:Ljava/lang/String;

    .line 2746254
    return-object p0
.end method
