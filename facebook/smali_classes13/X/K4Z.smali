.class public LX/K4Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/K4Z;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/storyline/react/ReactNarrativeEngineCallbacks;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2768725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768726
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/K4Z;->a:Ljava/util/List;

    .line 2768727
    return-void
.end method

.method public static a(LX/0QB;)LX/K4Z;
    .locals 3

    .prologue
    .line 2768728
    sget-object v0, LX/K4Z;->b:LX/K4Z;

    if-nez v0, :cond_1

    .line 2768729
    const-class v1, LX/K4Z;

    monitor-enter v1

    .line 2768730
    :try_start_0
    sget-object v0, LX/K4Z;->b:LX/K4Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2768731
    if-eqz v2, :cond_0

    .line 2768732
    :try_start_1
    new-instance v0, LX/K4Z;

    invoke-direct {v0}, LX/K4Z;-><init>()V

    .line 2768733
    move-object v0, v0

    .line 2768734
    sput-object v0, LX/K4Z;->b:LX/K4Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2768735
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2768736
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2768737
    :cond_1
    sget-object v0, LX/K4Z;->b:LX/K4Z;

    return-object v0

    .line 2768738
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2768739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
