.class public LX/JrE;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDq;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr6;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/Jrc;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742443
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrE;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/FDq;LX/0Ot;LX/Jrc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/FDq;",
            "LX/0Ot",
            "<",
            "LX/Jr6;",
            ">;",
            "LX/Jrc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2742444
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742445
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742446
    iput-object v0, p0, LX/JrE;->d:LX/0Ot;

    .line 2742447
    iput-object p2, p0, LX/JrE;->a:LX/FDq;

    .line 2742448
    iput-object p3, p0, LX/JrE;->b:LX/0Ot;

    .line 2742449
    iput-object p4, p0, LX/JrE;->c:LX/Jrc;

    .line 2742450
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742451
    invoke-virtual {p1}, LX/6kW;->y()LX/6kI;

    move-result-object v0

    iget-object v0, v0, LX/6kI;->newMessage:LX/6k4;

    .line 2742452
    iget-object v1, p0, LX/JrE;->c:LX/Jrc;

    iget-object v0, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742453
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrE;
    .locals 10

    .prologue
    .line 2742454
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742455
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742456
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742457
    if-nez v1, :cond_0

    .line 2742458
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742459
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742460
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742461
    sget-object v1, LX/JrE;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742462
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742463
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742464
    :cond_1
    if-nez v1, :cond_4

    .line 2742465
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742466
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742467
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742468
    new-instance v8, LX/JrE;

    const/16 v1, 0x35bd

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/FDq;->a(LX/0QB;)LX/FDq;

    move-result-object v1

    check-cast v1, LX/FDq;

    const/16 v7, 0x29dd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v7

    check-cast v7, LX/Jrc;

    invoke-direct {v8, v9, v1, p0, v7}, LX/JrE;-><init>(LX/0Ot;LX/FDq;LX/0Ot;LX/Jrc;)V

    .line 2742469
    const/16 v1, 0x29da

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742470
    iput-object v1, v8, LX/JrE;->d:LX/0Ot;

    .line 2742471
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742472
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742473
    if-nez v1, :cond_2

    .line 2742474
    sget-object v0, LX/JrE;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrE;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742475
    :goto_1
    if-eqz v0, :cond_3

    .line 2742476
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742477
    :goto_3
    check-cast v0, LX/JrE;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742478
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742479
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742480
    :catchall_1
    move-exception v0

    .line 2742481
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742482
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742483
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742484
    :cond_2
    :try_start_8
    sget-object v0, LX/JrE;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrE;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742485
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742486
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2742487
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->y()LX/6kI;

    move-result-object v7

    .line 2742488
    iget-object v1, p0, LX/JrE;->a:LX/FDq;

    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-direct {p0, v0}, LX/JrE;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/FDq;->a(Ljava/util/Set;)LX/0Px;

    move-result-object v0

    .line 2742489
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2742490
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2742491
    :goto_0
    return-object v0

    .line 2742492
    :cond_0
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742493
    iget-object v0, p0, LX/JrE;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr3;

    iget-object v1, v7, LX/6kI;->replacedMessageId:Ljava/lang/String;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iget-wide v2, p2, LX/7GJ;->b:J

    iget-object v4, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual/range {v0 .. v5}, LX/Jr3;->a(Ljava/util/List;JLcom/facebook/messaging/model/threadkey/ThreadKey;Z)Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    move-result-object v1

    .line 2742494
    iget-object v0, v1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2742495
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    .line 2742496
    :cond_1
    iget-object v0, p0, LX/JrE;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr3;

    invoke-virtual {v0, v1}, LX/Jr3;->a(Lcom/facebook/messaging/service/model/DeleteMessagesResult;)V

    .line 2742497
    iget-object v0, v1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_2

    .line 2742498
    iget-object v6, v1, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742499
    :cond_2
    iget-object v0, p0, LX/JrE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr6;

    iget-object v1, v7, LX/6kI;->newMessage:LX/6k4;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v0, v6, v1, v2, v3}, LX/Jr6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6k4;J)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742500
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742501
    if-eqz v0, :cond_0

    .line 2742502
    iget-object v1, p0, LX/JrE;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jr6;

    iget-object v2, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v2, LX/6kW;

    invoke-virtual {v2}, LX/6kW;->y()LX/6kI;

    move-result-object v2

    iget-object v2, v2, LX/6kI;->newMessage:LX/6k4;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v2, v0, v4, v5}, LX/Jr6;->a(LX/6k4;Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2742503
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742504
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrE;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
