.class public LX/Jep;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Jep;


# instance fields
.field private final a:LX/03V;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/6cm;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/03V;LX/0Or;LX/6cm;LX/0W3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/2N4;",
            ">;",
            "LX/6cm;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721042
    iput-object p1, p0, LX/Jep;->a:LX/03V;

    .line 2721043
    iput-object p2, p0, LX/Jep;->b:LX/0Or;

    .line 2721044
    iput-object p3, p0, LX/Jep;->c:LX/6cm;

    .line 2721045
    iput-object p4, p0, LX/Jep;->d:LX/0W3;

    .line 2721046
    return-void
.end method

.method public static a(LX/0QB;)LX/Jep;
    .locals 7

    .prologue
    .line 2721047
    sget-object v0, LX/Jep;->e:LX/Jep;

    if-nez v0, :cond_1

    .line 2721048
    const-class v1, LX/Jep;

    monitor-enter v1

    .line 2721049
    :try_start_0
    sget-object v0, LX/Jep;->e:LX/Jep;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2721050
    if-eqz v2, :cond_0

    .line 2721051
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2721052
    new-instance v6, LX/Jep;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v4, 0xd18

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/6cm;->b(LX/0QB;)LX/6cm;

    move-result-object v4

    check-cast v4, LX/6cm;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {v6, v3, p0, v4, v5}, LX/Jep;-><init>(LX/03V;LX/0Or;LX/6cm;LX/0W3;)V

    .line 2721053
    move-object v0, v6

    .line 2721054
    sput-object v0, LX/Jep;->e:LX/Jep;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721055
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2721056
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2721057
    :cond_1
    sget-object v0, LX/Jep;->e:LX/Jep;

    return-object v0

    .line 2721058
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2721059
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 8

    .prologue
    .line 2721060
    new-instance v0, Ljava/io/File;

    const-string v1, "recent_db_messages_json.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2721061
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2721062
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x0

    .line 2721063
    :try_start_0
    iget-object v3, p0, LX/Jep;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2N4;

    .line 2721064
    invoke-static {}, LX/FDx;->newBuilder()LX/FDw;

    move-result-object v4

    const/16 v5, 0x14

    .line 2721065
    iput v5, v4, LX/FDw;->a:I

    .line 2721066
    move-object v4, v4

    .line 2721067
    invoke-virtual {v4}, LX/FDw;->a()LX/FDx;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2N4;->a(LX/FDx;)Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 2721068
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 2721069
    const/4 v3, 0x0

    .line 2721070
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messages/Message;

    .line 2721071
    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    iget-object p1, p0, LX/Jep;->c:LX/6cm;

    invoke-virtual {p1, v3}, LX/6cm;->a(Lcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v5, v7, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 2721072
    :cond_0
    move-object v3, v5

    .line 2721073
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 2721074
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2721075
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    return-object v0

    .line 2721076
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721077
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721078
    :try_start_0
    invoke-direct {p0, p1}, LX/Jep;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2721079
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2721080
    const-string v2, "recent_db_messages_json.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2721081
    :goto_0
    return-object v0

    .line 2721082
    :catch_0
    move-exception v0

    .line 2721083
    iget-object v1, p0, LX/Jep;->a:LX/03V;

    const-string v2, "RecentMessagesDbExtraFileProvider"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2721084
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721085
    :try_start_0
    invoke-direct {p0, p1}, LX/Jep;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2721086
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const/4 v2, 0x0

    new-instance v3, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v4, "recent_db_messages_json.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "text/plain"

    invoke-direct {v3, v4, v0, v5}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2721087
    :catch_0
    move-exception v0

    .line 2721088
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to write recent messages file"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 2721089
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 2721090
    iget-object v0, p0, LX/Jep;->d:LX/0W3;

    sget-wide v2, LX/0X5;->bb:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
