.class public LX/Jm5;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2732473
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jm5;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/JmB;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2732436
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "inbox_units_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 2732437
    return-void
.end method

.method public static a(LX/0QB;)LX/Jm5;
    .locals 10

    .prologue
    .line 2732440
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2732441
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2732442
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2732443
    if-nez v1, :cond_0

    .line 2732444
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2732445
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2732446
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2732447
    sget-object v1, LX/Jm5;->a:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2732448
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2732449
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2732450
    :cond_1
    if-nez v1, :cond_4

    .line 2732451
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2732452
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2732453
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2732454
    new-instance p0, LX/Jm5;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v7

    check-cast v7, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v8

    check-cast v8, LX/1qU;

    .line 2732455
    new-instance v9, LX/JmB;

    invoke-direct {v9}, LX/JmB;-><init>()V

    .line 2732456
    move-object v9, v9

    .line 2732457
    move-object v9, v9

    .line 2732458
    check-cast v9, LX/JmB;

    invoke-direct {p0, v1, v7, v8, v9}, LX/Jm5;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/JmB;)V

    .line 2732459
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2732460
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2732461
    if-nez v1, :cond_2

    .line 2732462
    sget-object v0, LX/Jm5;->a:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jm5;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2732463
    :goto_1
    if-eqz v0, :cond_3

    .line 2732464
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2732465
    :goto_3
    check-cast v0, LX/Jm5;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2732466
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2732467
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2732468
    :catchall_1
    move-exception v0

    .line 2732469
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2732470
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2732471
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2732472
    :cond_2
    :try_start_8
    sget-object v0, LX/Jm5;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jm5;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final f()V
    .locals 0

    .prologue
    .line 2732438
    invoke-super {p0}, LX/1qS;->h()V

    .line 2732439
    return-void
.end method
