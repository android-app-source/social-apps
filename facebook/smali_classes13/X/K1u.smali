.class public LX/K1u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2762894
    iput-object p1, p0, LX/K1u;->a:LX/0Ot;

    .line 2762895
    return-void
.end method

.method public static a(LX/0QB;)LX/K1u;
    .locals 4

    .prologue
    .line 2762882
    const-class v1, LX/K1u;

    monitor-enter v1

    .line 2762883
    :try_start_0
    sget-object v0, LX/K1u;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2762884
    sput-object v2, LX/K1u;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2762885
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2762886
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2762887
    new-instance v3, LX/K1u;

    const/16 p0, 0x7bc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/K1u;-><init>(LX/0Ot;)V

    .line 2762888
    move-object v0, v3

    .line 2762889
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2762890
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K1u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2762891
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2762892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2762854
    if-nez p2, :cond_0

    .line 2762855
    :goto_0
    return-void

    .line 2762856
    :cond_0
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    .line 2762857
    iput-object p2, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2762858
    move-object v0, v0

    .line 2762859
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 2762860
    iput-object v1, v0, LX/8qL;->d:Ljava/lang/String;

    .line 2762861
    move-object v0, v0

    .line 2762862
    iput-boolean p4, v0, LX/8qL;->j:Z

    .line 2762863
    move-object v0, v0

    .line 2762864
    iput-boolean p5, v0, LX/8qL;->i:Z

    .line 2762865
    move-object v0, v0

    .line 2762866
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 2762867
    iput-object v1, v0, LX/8qL;->e:Ljava/lang/String;

    .line 2762868
    move-object v0, v0

    .line 2762869
    iput-boolean v2, v0, LX/8qL;->h:Z

    .line 2762870
    move-object v1, v0

    .line 2762871
    if-eqz p3, :cond_1

    .line 2762872
    iput-object p3, v1, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2762873
    :cond_1
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    .line 2762874
    iput-boolean v2, v0, LX/8qO;->a:Z

    .line 2762875
    move-object v0, v0

    .line 2762876
    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v2

    .line 2762877
    iget-object v0, p0, LX/K1u;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nI;

    invoke-virtual {v1}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v1

    invoke-interface {v0, p1, v1, v2}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 6

    .prologue
    .line 2762880
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/K1u;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)V

    .line 2762881
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Z)V
    .locals 6

    .prologue
    .line 2762878
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/K1u;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)V

    .line 2762879
    return-void
.end method
