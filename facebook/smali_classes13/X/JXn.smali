.class public LX/JXn;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/392;
.implements LX/3FR;
.implements LX/JXQ;
.implements LX/JXU;


# instance fields
.field public a:LX/2mn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2mu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Lcom/facebook/video/player/RichVideoPlayer;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ImageView;

.field public final h:Landroid/animation/ValueAnimator;

.field private final i:Ljava/lang/Runnable;

.field public j:D

.field public k:Ljava/lang/String;

.field private l:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2705276
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JXn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2705277
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2705278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JXn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2705279
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 2705280
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2705281
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/JXn;->h:Landroid/animation/ValueAnimator;

    .line 2705282
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentView$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentView$1;-><init>(LX/JXn;)V

    iput-object v0, p0, LX/JXn;->i:Ljava/lang/Runnable;

    .line 2705283
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    iput-wide v0, p0, LX/JXn;->j:D

    .line 2705284
    const-class v0, LX/JXn;

    invoke-static {v0, p0}, LX/JXn;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2705285
    const v0, 0x7f0311ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2705286
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2705287
    const v0, 0x7f0d2a25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JXn;->e:Landroid/widget/TextView;

    .line 2705288
    const v0, 0x7f0d1d07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/JXn;->g:Landroid/widget/ImageView;

    .line 2705289
    const v0, 0x7f0d2a24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JXn;->f:Landroid/widget/TextView;

    .line 2705290
    iget-object v0, p0, LX/JXn;->b:LX/0ad;

    sget-short v1, LX/JXv;->t:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2705291
    if-nez v0, :cond_0

    .line 2705292
    iget-object v0, p0, LX/JXn;->f:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2705293
    :cond_0
    iget-object v0, p0, LX/JXn;->b:LX/0ad;

    sget-short v1, LX/JXv;->h:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2705294
    if-nez v0, :cond_1

    .line 2705295
    iget-object v0, p0, LX/JXn;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2705296
    :cond_1
    iget-object v0, p0, LX/JXn;->c:LX/2mu;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2mu;->a(Ljava/lang/Boolean;)Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    move-result-object v0

    iput-object v0, p0, LX/JXn;->l:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    .line 2705297
    iget-object v0, p0, LX/JXn;->h:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2705298
    iget-object v0, p0, LX/JXn;->h:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2705299
    iget-object v0, p0, LX/JXn;->h:Landroid/animation/ValueAnimator;

    new-instance v1, LX/JXl;

    invoke-direct {v1, p0}, LX/JXl;-><init>(LX/JXn;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2705300
    iget-object v0, p0, LX/JXn;->h:Landroid/animation/ValueAnimator;

    new-instance v1, LX/JXm;

    invoke-direct {v1, p0}, LX/JXm;-><init>(LX/JXn;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2705301
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2705302
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/JXn;

    invoke-static {v3}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v1

    check-cast v1, LX/2mn;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    const-class p0, LX/2mu;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mu;

    iput-object v1, p1, LX/JXn;->a:LX/2mn;

    iput-object v2, p1, LX/JXn;->b:LX/0ad;

    iput-object v3, p1, LX/JXn;->c:LX/2mu;

    return-void
.end method


# virtual methods
.method public final J_(I)V
    .locals 4

    .prologue
    .line 2705254
    iget-object v0, p0, LX/JXn;->i:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, LX/JXn;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2705255
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 1

    .prologue
    .line 2705303
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 2705304
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2705305
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2705306
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 2705272
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705273
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2705274
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2705275
    return-void
.end method

.method public getPluginSelector()LX/3Ge;
    .locals 1

    .prologue
    .line 2705271
    iget-object v0, p0, LX/JXn;->l:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 2705270
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 2705269
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 2705267
    iget-object v0, p0, LX/JXn;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2705268
    return-void
.end method

.method public final nk_()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2705258
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setPadding(IIII)V

    .line 2705259
    iget-object v0, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2705260
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2705261
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 2705262
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 2705263
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    .line 2705264
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    .line 2705265
    :cond_0
    iget-object v1, p0, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2705266
    return-void
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 2705257
    return-void
.end method

.method public final y()V
    .locals 0

    .prologue
    .line 2705256
    return-void
.end method
