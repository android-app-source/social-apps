.class public LX/JvL;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/JvV;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/user/model/User;

.field public b:LX/JvO;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/JvZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/JvO;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p2    # LX/JvO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherAdapter$ItemClickedDelegate;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2750292
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2750293
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, LX/JvL;->a:Lcom/facebook/user/model/User;

    .line 2750294
    iput-object p2, p0, LX/JvL;->b:LX/JvO;

    .line 2750295
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2750305
    new-instance v0, LX/JvV;

    new-instance v1, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/JvV;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2750297
    check-cast p1, LX/JvV;

    .line 2750298
    iget-object v0, p0, LX/JvL;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JvZ;

    .line 2750299
    iget-boolean v1, v0, LX/JvZ;->a:Z

    if-eqz v1, :cond_0

    .line 2750300
    iget-object v1, p0, LX/JvL;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JvL;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/JvV;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2750301
    :goto_0
    new-instance v1, LX/JvK;

    invoke-direct {v1, p0, v0}, LX/JvK;-><init>(LX/JvL;LX/JvZ;)V

    .line 2750302
    iget-object v0, p1, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2750303
    return-void

    .line 2750304
    :cond_0
    iget-object v1, v0, LX/JvZ;->d:Ljava/lang/String;

    iget-object v2, v0, LX/JvZ;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/JvV;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2750296
    iget-object v0, p0, LX/JvL;->c:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/JvL;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
