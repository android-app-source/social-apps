.class public LX/K9d;
.super LX/0gG;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:[Landroid/widget/FrameLayout;

.field public c:Ljava/text/DateFormat;

.field public d:Ljava/text/DateFormat;

.field public e:Landroid/widget/TimePicker;

.field public f:Landroid/widget/DatePicker;

.field public g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

.field private h:Landroid/content/Context;

.field public i:Ljava/util/Calendar;

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2777649
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/K9d;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Ljava/util/Calendar;Landroid/content/Context;LX/0Or;LX/0Or;)V
    .locals 3
    .param p1    # Ljava/util/Calendar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2777650
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2777651
    sget-object v0, LX/K9d;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    new-array v0, v0, [Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/K9d;->b:[Landroid/widget/FrameLayout;

    .line 2777652
    iput-object p2, p0, LX/K9d;->h:Landroid/content/Context;

    .line 2777653
    iget-object v0, p0, LX/K9d;->h:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, LX/K9d;->j:Z

    .line 2777654
    iget-boolean v0, p0, LX/K9d;->j:Z

    if-eqz v0, :cond_1

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm"

    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, LX/K9d;->c:Ljava/text/DateFormat;

    .line 2777655
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMM d"

    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/K9d;->d:Ljava/text/DateFormat;

    .line 2777656
    if-nez p1, :cond_0

    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p1

    :cond_0
    iput-object p1, p0, LX/K9d;->i:Ljava/util/Calendar;

    .line 2777657
    return-void

    .line 2777658
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "hh:mm a"

    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2777659
    sget-object v0, LX/K9d;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2777660
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2777661
    iget-object v0, p0, LX/K9d;->c:Ljava/text/DateFormat;

    iget-object v1, p0, LX/K9d;->i:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2777662
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/K9d;->d:Ljava/text/DateFormat;

    iget-object v1, p0, LX/K9d;->i:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2777663
    iget-object v0, p0, LX/K9d;->b:[Landroid/widget/FrameLayout;

    aget-object v0, v0, p2

    .line 2777664
    if-nez v0, :cond_0

    .line 2777665
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2777666
    sget-object v0, LX/K9d;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2777667
    if-ne v0, v3, :cond_1

    .line 2777668
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314c1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2777669
    const v1, 0x7f0d2f14

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TimePicker;

    iput-object v1, p0, LX/K9d;->e:Landroid/widget/TimePicker;

    .line 2777670
    iget-object v1, p0, LX/K9d;->e:Landroid/widget/TimePicker;

    iget-boolean v2, p0, LX/K9d;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 2777671
    iget-object v1, p0, LX/K9d;->e:Landroid/widget/TimePicker;

    iget-object v2, p0, LX/K9d;->i:Ljava/util/Calendar;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 2777672
    iget-object v1, p0, LX/K9d;->e:Landroid/widget/TimePicker;

    iget-object v2, p0, LX/K9d;->i:Ljava/util/Calendar;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 2777673
    iget-object v1, p0, LX/K9d;->e:Landroid/widget/TimePicker;

    new-instance v2, LX/K9b;

    invoke-direct {v2, p0, p2}, LX/K9b;-><init>(LX/K9d;I)V

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 2777674
    :goto_0
    move-object v0, v0

    .line 2777675
    iget-object v1, p0, LX/K9d;->b:[Landroid/widget/FrameLayout;

    aput-object v0, v1, p2

    .line 2777676
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2777677
    return-object v0

    .line 2777678
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0303d3

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2777679
    const v1, 0x7f0d0bef

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/DatePicker;

    iput-object v1, p0, LX/K9d;->f:Landroid/widget/DatePicker;

    .line 2777680
    iget-object v1, p0, LX/K9d;->f:Landroid/widget/DatePicker;

    iget-object v2, p0, LX/K9d;->i:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, LX/K9d;->i:Ljava/util/Calendar;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, LX/K9d;->i:Ljava/util/Calendar;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    new-instance v5, LX/K9c;

    invoke-direct {v5, p0, p2}, LX/K9c;-><init>(LX/K9d;I)V

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2777681
    iget-object v0, p0, LX/K9d;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    if-nez v0, :cond_1

    .line 2777682
    :cond_0
    :goto_0
    return-void

    .line 2777683
    :cond_1
    iget-object v0, p0, LX/K9d;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2777684
    iget-object v0, p0, LX/K9d;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2777685
    instance-of v1, v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2777686
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2777687
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string p2, ""

    :cond_2
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2777688
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2777689
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2777690
    if-ne p2, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2777691
    sget-object v0, LX/K9d;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
