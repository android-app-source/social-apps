.class public LX/KAq;
.super LX/4sj;
.source ""

# interfaces
.implements LX/2NW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4sj",
        "<",
        "LX/KAo;",
        ">;",
        "LX/2NW;"
    }
.end annotation


# instance fields
.field private final b:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    invoke-direct {p0, p1}, LX/4sj;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    iget v1, p1, Lcom/google/android/gms/common/data/DataHolder;->h:I

    move v1, v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iput-object v0, p0, LX/KAq;->b:Lcom/google/android/gms/common/api/Status;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, LX/KAq;->b:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 2

    new-instance v0, LX/KBG;

    iget-object v1, p0, LX/4sY;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v1, p1, p2}, LX/KBG;-><init>(Lcom/google/android/gms/common/data/DataHolder;II)V

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "path"

    return-object v0
.end method
