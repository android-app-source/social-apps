.class public final LX/Jw3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLInterfaces$LocationTriggerWithReactionUnits;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/ImmutableLocation;

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:LX/2ct;

.field public final synthetic d:LX/3FA;


# direct methods
.method public constructor <init>(LX/3FA;Lcom/facebook/location/ImmutableLocation;Ljava/lang/Object;LX/2ct;)V
    .locals 0

    .prologue
    .line 2751356
    iput-object p1, p0, LX/Jw3;->d:LX/3FA;

    iput-object p2, p0, LX/Jw3;->a:Lcom/facebook/location/ImmutableLocation;

    iput-object p3, p0, LX/Jw3;->b:Ljava/lang/Object;

    iput-object p4, p0, LX/Jw3;->c:LX/2ct;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2751357
    iget-object v0, p0, LX/Jw3;->d:LX/3FA;

    iget-object v0, v0, LX/3FA;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jvw;

    iget-object v1, p0, LX/Jw3;->a:Lcom/facebook/location/ImmutableLocation;

    iget-object v2, p0, LX/Jw3;->b:Ljava/lang/Object;

    iget-object v3, p0, LX/Jw3;->c:LX/2ct;

    iget-object v4, p0, LX/Jw3;->d:LX/3FA;

    iget-object v4, v4, LX/3FA;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-char v5, LX/1ST;->b:C

    const-string v6, "%s"

    invoke-interface {v4, v5, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/Jvw;->a(Lcom/facebook/location/ImmutableLocation;Ljava/lang/Object;LX/2ct;Ljava/lang/String;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)V

    .line 2751358
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2751353
    check-cast p1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;

    .line 2751354
    iget-object v0, p0, LX/Jw3;->d:LX/3FA;

    iget-object v0, v0, LX/3FA;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jvw;

    iget-object v1, p0, LX/Jw3;->a:Lcom/facebook/location/ImmutableLocation;

    iget-object v2, p0, LX/Jw3;->b:Ljava/lang/Object;

    iget-object v3, p0, LX/Jw3;->c:LX/2ct;

    iget-object v4, p0, LX/Jw3;->d:LX/3FA;

    iget-object v4, v4, LX/3FA;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-char v5, LX/1ST;->b:C

    const-string v6, "%s"

    invoke-interface {v4, v5, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/Jvw;->a(Lcom/facebook/location/ImmutableLocation;Ljava/lang/Object;LX/2ct;Ljava/lang/String;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)V

    .line 2751355
    return-void
.end method
