.class public LX/K2V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Dw;


# instance fields
.field private final a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;)V
    .locals 0

    .prologue
    .line 2764194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2764195
    iput-object p1, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    .line 2764196
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2764193
    iget-object v0, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/19o;
    .locals 1

    .prologue
    .line 2764192
    iget-object v0, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2764191
    iget-object v0, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2764168
    iget-object v0, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->ed_()I

    move-result v0

    return v0
.end method

.method public final e()LX/7Dy;
    .locals 2

    .prologue
    .line 2764189
    iget-object v0, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    move-result-object v0

    .line 2764190
    new-instance v1, LX/K2W;

    invoke-direct {v1, v0}, LX/K2W;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;)V

    return-object v1
.end method

.method public final f()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2764169
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2764170
    iget-object v0, p0, LX/K2V;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->ec_()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;

    .line 2764171
    new-instance v5, LX/7Dm;

    invoke-direct {v5}, LX/7Dm;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->c()I

    move-result v6

    .line 2764172
    iput v6, v5, LX/7Dm;->a:I

    .line 2764173
    move-object v5, v5

    .line 2764174
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->b()I

    move-result v6

    .line 2764175
    iput v6, v5, LX/7Dm;->b:I

    .line 2764176
    move-object v5, v5

    .line 2764177
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->a()I

    move-result v6

    .line 2764178
    iput v6, v5, LX/7Dm;->c:I

    .line 2764179
    move-object v5, v5

    .line 2764180
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->d()I

    move-result v6

    .line 2764181
    iput v6, v5, LX/7Dm;->d:I

    .line 2764182
    move-object v5, v5

    .line 2764183
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 2764184
    iput-object v0, v5, LX/7Dm;->e:Ljava/lang/String;

    .line 2764185
    move-object v0, v5

    .line 2764186
    invoke-virtual {v0}, LX/7Dm;->a()Lcom/facebook/spherical/photo/model/PhotoTile;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2764187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2764188
    :cond_0
    return-object v2
.end method
