.class public LX/Jet;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Jet;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FDq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jeq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6cm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Jet;
    .locals 8

    .prologue
    .line 2721287
    sget-object v0, LX/Jet;->f:LX/Jet;

    if-nez v0, :cond_1

    .line 2721288
    const-class v1, LX/Jet;

    monitor-enter v1

    .line 2721289
    :try_start_0
    sget-object v0, LX/Jet;->f:LX/Jet;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2721290
    if-eqz v2, :cond_0

    .line 2721291
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2721292
    new-instance v3, LX/Jet;

    invoke-direct {v3}, LX/Jet;-><init>()V

    .line 2721293
    const/16 v4, 0x2738

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xd18

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xce8

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2683

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/6cm;->b(LX/0QB;)LX/6cm;

    move-result-object p0

    check-cast p0, LX/6cm;

    .line 2721294
    iput-object v4, v3, LX/Jet;->a:LX/0Or;

    iput-object v5, v3, LX/Jet;->b:LX/0Or;

    iput-object v6, v3, LX/Jet;->c:LX/0Or;

    iput-object v7, v3, LX/Jet;->d:LX/0Or;

    iput-object p0, v3, LX/Jet;->e:LX/6cm;

    .line 2721295
    move-object v0, v3

    .line 2721296
    sput-object v0, LX/Jet;->f:LX/Jet;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721297
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2721298
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2721299
    :cond_1
    sget-object v0, LX/Jet;->f:LX/Jet;

    return-object v0

    .line 2721300
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2721301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 2721278
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2721279
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2721280
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x0

    .line 2721281
    :try_start_0
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 2721282
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2721283
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    return-object v0

    .line 2721284
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721285
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private a(LX/4z1;)Lorg/json/JSONObject;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2721266
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 2721267
    invoke-virtual {p1}, LX/0Xt;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2721268
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 2721269
    invoke-virtual {p1, v0}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v3

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 2721270
    add-int/lit8 v5, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v10, p0, LX/Jet;->e:LX/6cm;

    invoke-virtual {v10, v1}, LX/6cm;->a(Lcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v8, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move v4, v5

    .line 2721271
    goto :goto_1

    .line 2721272
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 2721273
    const-string v4, "thread_key"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2721274
    const-string v0, "messages"

    invoke-virtual {v1, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2721275
    add-int/lit8 v0, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move v2, v0

    .line 2721276
    goto :goto_0

    .line 2721277
    :cond_1
    return-object v6
.end method

.method public static a(LX/Jet;)Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 2721302
    iget-object v0, p0, LX/Jet;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jeq;

    .line 2721303
    iget-object v1, v0, LX/Jeq;->a:Ljava/util/List;

    if-nez v1, :cond_4

    .line 2721304
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2721305
    :goto_0
    move-object v1, v1

    .line 2721306
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 2721307
    const/4 v0, 0x0

    .line 2721308
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jrv;

    .line 2721309
    instance-of v2, v0, LX/Jrw;

    if-eqz v2, :cond_2

    .line 2721310
    check-cast v0, LX/Jrw;

    .line 2721311
    iget-object v2, v0, LX/Jrw;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2721312
    iget-object v5, v0, LX/Jrw;->b:LX/Jry;

    .line 2721313
    iget-object v0, p0, LX/Jet;->e:LX/6cm;

    invoke-virtual {v0, v2}, LX/6cm;->a(Lcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2721314
    if-nez v0, :cond_0

    .line 2721315
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2721316
    :cond_0
    if-eqz v5, :cond_1

    .line 2721317
    const-string v2, "rowReciptItem"

    invoke-virtual {v0, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    move-object v2, v0

    .line 2721318
    :goto_2
    add-int/lit8 v0, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move v1, v0

    .line 2721319
    goto :goto_1

    .line 2721320
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 2721321
    :cond_3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2721322
    const-string v1, "thread_rows"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2721323
    return-object v0

    :cond_4
    iget-object v1, v0, LX/Jeq;->a:Ljava/util/List;

    goto :goto_0
.end method

.method public static a(LX/Jet;LX/Jes;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 2721264
    iget-object v0, p1, LX/Jes;->a:LX/6ek;

    iget v1, p1, LX/Jes;->e:I

    iget v2, p1, LX/Jes;->f:I

    invoke-virtual {p0, v0, v1, v2}, LX/Jet;->a(LX/6ek;II)LX/4z1;

    move-result-object v0

    .line 2721265
    invoke-direct {p0, v0}, LX/Jet;->a(LX/4z1;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method private static a(ILX/4z1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 2721254
    if-eqz p3, :cond_2

    .line 2721255
    iget-object v1, p3, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v1, v1

    .line 2721256
    move-object v4, v1

    .line 2721257
    :goto_0
    if-nez v4, :cond_1

    .line 2721258
    invoke-virtual {p1, p2, v2}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2721259
    :cond_0
    return-void

    .line 2721260
    :cond_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v0

    move v2, v0

    :goto_1
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2721261
    add-int/lit8 v3, v2, 0x1

    if-ge v2, p0, :cond_0

    .line 2721262
    invoke-virtual {p1, p2, v0}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2721263
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_1

    :cond_2
    move-object v4, v2

    goto :goto_0
.end method

.method public static b(LX/Jet;LX/Jes;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 2721252
    iget-object v0, p1, LX/Jes;->a:LX/6ek;

    iget v1, p1, LX/Jes;->e:I

    iget v2, p1, LX/Jes;->f:I

    invoke-virtual {p0, v0, v1, v2}, LX/Jet;->b(LX/6ek;II)LX/4z1;

    move-result-object v0

    .line 2721253
    invoke-direct {p0, v0}, LX/Jet;->a(LX/4z1;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/6ek;II)LX/4z1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "II)",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721199
    iget-object v0, p0, LX/Jet;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    .line 2721200
    iget-object v1, p0, LX/Jet;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDq;

    .line 2721201
    const-wide/16 v2, -0x1

    invoke-virtual {v1, p1, v2, v3, p2}, LX/FDq;->a(LX/6ek;JI)Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 2721202
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v4

    .line 2721203
    const/4 v1, 0x0

    .line 2721204
    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2721205
    add-int/lit8 v3, v2, 0x1

    if-ge v2, p2, :cond_0

    .line 2721206
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2, p3}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    .line 2721207
    iget-object v2, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2721208
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p3, v4, v1, v2}, LX/Jet;->a(ILX/4z1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/MessagesCollection;)V

    move v2, v3

    .line 2721209
    goto :goto_0

    .line 2721210
    :cond_0
    return-object v4
.end method

.method public final b(LX/6ek;II)LX/4z1;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "II)",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2721211
    iget-object v0, p0, LX/Jet;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    .line 2721212
    sget-object v2, LX/6em;->ALL:LX/6em;

    .line 2721213
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 2721214
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2721215
    sget-object v4, LX/6em;->NON_SMS:LX/6em;

    if-eq v2, v4, :cond_2

    invoke-static {v0}, LX/2Og;->d(LX/2Og;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    .line 2721216
    :goto_0
    sget-object v7, LX/6em;->SMS:LX/6em;

    if-eq v2, v7, :cond_0

    iget-object v7, v0, LX/2Og;->i:LX/26j;

    invoke-virtual {v7}, LX/26j;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    move v6, v5

    .line 2721217
    :cond_0
    sget-object v7, LX/6em;->SMS:LX/6em;

    if-ne v2, v7, :cond_3

    .line 2721218
    sget-object v7, Lcom/facebook/messaging/model/threads/ThreadsCollection;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v7, v7

    .line 2721219
    :goto_1
    if-nez v4, :cond_4

    if-nez v6, :cond_4

    move-object v4, v7

    .line 2721220
    :goto_2
    move-object v2, v4

    .line 2721221
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v5, v3

    .line 2721222
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v6

    .line 2721223
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v7, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2721224
    add-int/lit8 v4, v3, 0x1

    if-ge v3, p2, :cond_1

    .line 2721225
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v3}, LX/2Og;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v3

    .line 2721226
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p3, v6, v1, v3}, LX/Jet;->a(ILX/4z1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/MessagesCollection;)V

    .line 2721227
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v3, v4

    goto :goto_3

    .line 2721228
    :cond_1
    return-object v6

    :cond_2
    move v4, v6

    .line 2721229
    goto :goto_0

    .line 2721230
    :cond_3
    iget-object v7, v0, LX/2Og;->c:LX/2OQ;

    invoke-virtual {v7, p1}, LX/2OQ;->d(LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v7

    goto :goto_1

    .line 2721231
    :cond_4
    new-instance v8, Ljava/util/ArrayList;

    const/4 v9, 0x3

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 2721232
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2721233
    if-eqz v4, :cond_5

    .line 2721234
    sget-object v4, LX/6el;->SMS:LX/6el;

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2721235
    iget-object v4, v0, LX/2Og;->e:LX/2Om;

    sget-object v7, LX/6el;->SMS:LX/6el;

    invoke-virtual {v4, v7}, LX/2Om;->b(LX/6el;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v7

    .line 2721236
    new-instance v4, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-direct {v4, v7, v5}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2721237
    :goto_4
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2721238
    :cond_5
    if-eqz v6, :cond_6

    .line 2721239
    iget-object v4, v0, LX/2Og;->f:LX/2OQ;

    invoke-virtual {v4, p1}, LX/2OQ;->d(LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    .line 2721240
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2721241
    :cond_6
    invoke-static {v8}, LX/DoE;->a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    goto :goto_2

    .line 2721242
    :cond_7
    iget-object v4, v0, LX/2Og;->d:LX/2OQ;

    invoke-virtual {v4, p1}, LX/2OQ;->d(LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v7

    .line 2721243
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2721244
    invoke-virtual {v3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6el;

    .line 2721245
    iget-object p0, v0, LX/2Og;->e:LX/2Om;

    invoke-virtual {p0, v4}, LX/2Om;->b(LX/6el;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    .line 2721246
    if-eqz v4, :cond_8

    .line 2721247
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2721248
    :cond_9
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    .line 2721249
    invoke-static {v9}, LX/DoE;->a(Ljava/util/List;)V

    .line 2721250
    new-instance v4, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v9

    invoke-direct {v4, v9, v5}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2721251
    invoke-static {v4, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/DoE;->a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    goto :goto_4

    :cond_a
    move-object v4, v7

    goto :goto_4
.end method
