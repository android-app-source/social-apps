.class public final LX/K6h;
.super LX/8qY;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V
    .locals 0

    .prologue
    .line 2772056
    iput-object p1, p0, LX/K6h;->b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    invoke-direct {p0, p1}, LX/8qY;-><init>(Lcom/facebook/widget/popover/PopoverFragment;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2772070
    invoke-super {p0}, LX/8qY;->a()V

    .line 2772071
    iget-object v0, p0, LX/K6h;->b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    .line 2772072
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->q:LX/Chv;

    new-instance p0, LX/CiG;

    invoke-direct {p0}, LX/CiG;-><init>()V

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2772073
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 2772067
    iget-object v0, p0, LX/K6h;->b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    instance-of v0, v0, LX/K5f;

    if-eqz v0, :cond_0

    .line 2772068
    iget-object v0, p0, LX/K6h;->b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-interface {v0, p1, p2, p3}, LX/K5f;->a(FFLX/31M;)Z

    move-result v0

    .line 2772069
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/8qY;->a(FFLX/31M;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2772061
    invoke-super {p0}, LX/8qY;->b()V

    .line 2772062
    iget-object v0, p0, LX/K6h;->b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    .line 2772063
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->q:LX/Chv;

    new-instance p0, LX/CiF;

    invoke-direct {p0}, LX/CiF;-><init>()V

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2772064
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->E:Z

    .line 2772065
    invoke-static {v0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V

    .line 2772066
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2772057
    invoke-super {p0}, LX/8qY;->d()V

    .line 2772058
    iget-object v0, p0, LX/K6h;->b:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    .line 2772059
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->q:LX/Chv;

    new-instance p0, LX/CiL;

    invoke-direct {p0}, LX/CiL;-><init>()V

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2772060
    return-void
.end method
