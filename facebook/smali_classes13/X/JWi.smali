.class public LX/JWi;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JWk;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWi",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JWk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703356
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703357
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWi;->b:LX/0Zi;

    .line 2703358
    iput-object p1, p0, LX/JWi;->a:LX/0Ot;

    .line 2703359
    return-void
.end method

.method public static a(LX/0QB;)LX/JWi;
    .locals 4

    .prologue
    .line 2703360
    const-class v1, LX/JWi;

    monitor-enter v1

    .line 2703361
    :try_start_0
    sget-object v0, LX/JWi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703362
    sput-object v2, LX/JWi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703363
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703364
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703365
    new-instance v3, LX/JWi;

    const/16 p0, 0x20d3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWi;-><init>(LX/0Ot;)V

    .line 2703366
    move-object v0, v3

    .line 2703367
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703368
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703369
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703370
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2703371
    check-cast p2, LX/JWh;

    .line 2703372
    iget-object v0, p0, LX/JWi;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWk;

    iget-object v1, p2, LX/JWh;->a:LX/1Pc;

    iget-object v2, p2, LX/JWh;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703373
    new-instance v4, LX/JWj;

    invoke-direct {v4, v0, v2}, LX/JWj;-><init>(LX/JWk;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2703374
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 2703375
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2703376
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 2703377
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 2703378
    move-object v5, v5

    .line 2703379
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2703380
    check-cast v3, LX/0jW;

    .line 2703381
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 2703382
    move-object v3, v5

    .line 2703383
    const/16 v5, 0x8

    .line 2703384
    iput v5, v3, LX/3mP;->b:I

    .line 2703385
    move-object v3, v3

    .line 2703386
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 2703387
    move-object v3, v3

    .line 2703388
    iput-object v2, v3, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703389
    move-object v3, v3

    .line 2703390
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 2703391
    iget-object v3, v0, LX/JWk;->a:LX/JWZ;

    .line 2703392
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2703393
    check-cast v4, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-static {v4}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v4

    move-object v6, v4

    .line 2703394
    move-object v4, p1

    move-object v5, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/JWZ;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;Ljava/lang/Object;LX/25M;)LX/JWY;

    move-result-object v3

    .line 2703395
    iget-object v4, v0, LX/JWk;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2703396
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703397
    invoke-static {}, LX/1dS;->b()V

    .line 2703398
    const/4 v0, 0x0

    return-object v0
.end method
