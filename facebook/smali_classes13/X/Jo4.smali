.class public LX/Jo4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2734902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JLjava/lang/String;Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;)Lcom/facebook/messaging/montage/model/art/ArtItem;
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734903
    invoke-virtual {p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 2734904
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;->j()LX/0Px;

    move-result-object v9

    invoke-virtual {p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;->l()LX/0Px;

    move-result-object v10

    invoke-virtual {p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;->m()LX/0Px;

    move-result-object v11

    move-wide v2, p0

    move-object v4, p2

    invoke-static/range {v1 .. v11}, LX/Jno;->a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;LX/15i;ILX/0Px;LX/0Px;LX/0Px;LX/0Px;)Lcom/facebook/messaging/montage/model/art/ArtItem;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2734905
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2734906
    :goto_0
    return-object v0

    .line 2734907
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2734908
    const/4 v9, 0x0

    .line 2734909
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v7

    .line 2734910
    if-nez v7, :cond_5

    .line 2734911
    :cond_2
    :goto_2
    move-object v5, v9

    .line 2734912
    invoke-static {v5}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2734913
    new-instance v1, LX/DhL;

    invoke-direct {v1, p0, v0, v5}, LX/DhL;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/0Px;)V

    .line 2734914
    new-instance v7, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    iget-object v8, v1, LX/DhL;->a:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    iget-object v9, v1, LX/DhL;->b:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    iget-object v10, v1, LX/DhL;->c:LX/0Px;

    iget-boolean v11, v1, LX/DhL;->d:Z

    invoke-direct/range {v7 .. v11}, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/0Px;Z)V

    move-object v0, v7

    .line 2734915
    goto :goto_0

    .line 2734916
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 2734917
    goto :goto_0

    .line 2734918
    :cond_5
    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->w()LX/0Px;

    move-result-object v7

    .line 2734919
    invoke-static {v7}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2734920
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2734921
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerNuxAttachmentFragmentModel$NuxMessagesModel;

    .line 2734922
    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerNuxAttachmentFragmentModel$NuxMessagesModel;->j()LX/1vs;

    move-result-object v8

    iget v8, v8, LX/1vs;->b:I

    if-nez v8, :cond_8

    move-object v8, v9

    .line 2734923
    :goto_4
    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerNuxAttachmentFragmentModel$NuxMessagesModel;->a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;

    move-result-object v12

    if-nez v12, :cond_9

    move-object v7, v9

    .line 2734924
    :goto_5
    if-nez v8, :cond_7

    if-eqz v7, :cond_6

    .line 2734925
    :cond_7
    new-instance v12, Lcom/facebook/messaging/montage/model/MontageNuxMessage;

    invoke-direct {v12, v8, v7}, Lcom/facebook/messaging/montage/model/MontageNuxMessage;-><init>(Ljava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtItem;)V

    invoke-virtual {v10, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2734926
    :cond_8
    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerNuxAttachmentFragmentModel$NuxMessagesModel;->j()LX/1vs;

    move-result-object v8

    iget-object v12, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2734927
    const/4 v13, 0x0

    invoke-virtual {v12, v8, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    goto :goto_4

    .line 2734928
    :cond_9
    const-wide/16 v13, 0x0

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerNuxAttachmentFragmentModel$NuxMessagesModel;->a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;

    move-result-object v7

    invoke-static {v13, v14, v9, v7}, LX/Jo4;->a(JLjava/lang/String;Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerCompositionModel;)Lcom/facebook/messaging/montage/model/art/ArtItem;

    move-result-object v7

    goto :goto_5

    .line 2734929
    :cond_a
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    goto :goto_2
.end method
