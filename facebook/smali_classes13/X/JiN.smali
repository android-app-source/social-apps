.class public LX/JiN;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

.field public b:Landroid/content/Context;

.field public c:Lcom/facebook/content/SecureContextHelper;

.field public d:LX/Ifj;

.field public e:LX/0Zb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725907
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/JiN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725908
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2725909
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725910
    const-class v0, LX/JiN;

    invoke-static {v0, p0}, LX/JiN;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725911
    invoke-virtual {p0}, LX/JiN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b078c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v2, v0, v2, v2}, LX/JiN;->setPadding(IIII)V

    .line 2725912
    const v0, 0x7f030cef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2725913
    const v0, 0x7f0d204a

    invoke-virtual {p0, v0}, LX/JiN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    iput-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    .line 2725914
    iget-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082df1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setNegativeButtonContentDescription(Ljava/lang/String;)V

    .line 2725915
    iget-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082def

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setTitle(Ljava/lang/String;)V

    .line 2725916
    iget-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082df0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setText(Ljava/lang/String;)V

    .line 2725917
    iget-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082df2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setPositiveButtonText(Ljava/lang/String;)V

    .line 2725918
    iget-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    new-instance v1, LX/JiL;

    invoke-direct {v1, p0}, LX/JiL;-><init>(LX/JiN;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725919
    iget-object v0, p0, LX/JiN;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    new-instance v1, LX/JiM;

    invoke-direct {v1, p0}, LX/JiM;-><init>(LX/JiN;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725920
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JiN;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/Ifj;->b(LX/0QB;)LX/Ifj;

    move-result-object v3

    check-cast v3, LX/Ifj;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    iput-object v1, p1, LX/JiN;->b:Landroid/content/Context;

    iput-object v2, p1, LX/JiN;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, p1, LX/JiN;->d:LX/Ifj;

    iput-object p0, p1, LX/JiN;->e:LX/0Zb;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2725921
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2725922
    invoke-virtual {p0}, LX/JiN;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2725923
    invoke-virtual {p0, v2, v2}, LX/JiN;->setMeasuredDimension(II)V

    .line 2725924
    :cond_0
    return-void
.end method
