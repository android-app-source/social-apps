.class public final LX/Jvm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750812
    iput-object p1, p0, LX/Jvm;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2750808
    iget-object v0, p0, LX/Jvm;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->u:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0813b9

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2750809
    iget-object v0, p0, LX/Jvm;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Y:LX/Jvj;

    if-eqz v0, :cond_0

    .line 2750810
    iget-object v0, p0, LX/Jvm;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Y:LX/Jvj;

    invoke-virtual {v0}, LX/Jvj;->a()V

    .line 2750811
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 2750708
    iget-object v0, p0, LX/Jvm;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    .line 2750709
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750710
    iget v2, v1, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->g:I

    move v1, v2

    .line 2750711
    iput v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->W:I

    .line 2750712
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v3, 0x7f0d0ba6

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 2750713
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->t(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)LX/89f;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Q:LX/89f;

    .line 2750714
    iget-object v3, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Q:LX/89f;

    iget-object v4, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Y:LX/Jvj;

    invoke-interface {v3, v2, v4}, LX/89f;->a(Landroid/view/ViewStub;LX/Jvj;)Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->n:Landroid/view/ViewGroup;

    .line 2750715
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750716
    iget-boolean v3, v2, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->a:Z

    move v2, v3

    .line 2750717
    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750718
    iget-object v3, v2, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    move-object v2, v3

    .line 2750719
    sget-object v3, LX/4gI;->PHOTO_ONLY:LX/4gI;

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    move v3, v2

    .line 2750720
    :goto_0
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Q:LX/89f;

    invoke-interface {v2}, LX/89f;->a()Landroid/view/View;

    move-result-object v2

    .line 2750721
    if-eqz v2, :cond_0

    .line 2750722
    check-cast v2, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    invoke-static {v0, v3, v2}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;ZLcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;)LX/BFq;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->C:LX/BFq;

    .line 2750723
    :cond_0
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v3, 0x7f0d0ba7

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iget-object v3, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v4, 0x7f0d0ba8

    invoke-static {v3, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    .line 2750724
    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750725
    iget v6, v5, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->j:I

    move v5, v6

    .line 2750726
    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 2750727
    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->D:LX/BGD;

    new-instance v6, LX/Jvi;

    invoke-direct {v6, v0}, LX/Jvi;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    iget-object v9, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->n:Landroid/view/ViewGroup;

    const/4 v10, 0x0

    move-object v7, v2

    move-object v8, v3

    invoke-virtual/range {v5 .. v10}, LX/BGD;->a(LX/Jvi;Landroid/widget/ProgressBar;Lcom/facebook/resources/ui/FbTextView;Landroid/view/ViewGroup;Z)LX/BGC;

    move-result-object v5

    move-object v2, v5

    .line 2750728
    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->E:LX/BGC;

    .line 2750729
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v3, 0x7f0d0681

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    .line 2750730
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    const v3, 0x7f0d0861

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 2750731
    const v3, 0x7f030c74

    invoke-virtual {v2, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2750732
    iget-object v3, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750733
    iget-boolean v4, v3, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->b:Z

    move v3, v4

    .line 2750734
    if-eqz v3, :cond_5

    sget-object v3, LX/5fM;->FRONT:LX/5fM;

    move-object v4, v3

    :goto_1
    iget-object v3, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    const v5, 0x7f0d0682

    invoke-static {v3, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativecam/ui/FocusView;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    .line 2750735
    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->k:Landroid/view/View;

    .line 2750736
    new-instance v6, LX/Jvc;

    invoke-direct {v6, v0}, LX/Jvc;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750737
    iget-object v6, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->v:LX/BG7;

    .line 2750738
    new-instance v7, LX/Jvd;

    invoke-direct {v7, v0}, LX/Jvd;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    move-object v7, v7

    .line 2750739
    move-object v8, v2

    check-cast v8, Lcom/facebook/optic/CameraPreviewView;

    iget-object v11, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v12, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    move-object v9, v3

    move-object v10, v4

    invoke-virtual/range {v6 .. v12}, LX/BG7;->a(LX/Jvd;Lcom/facebook/optic/CameraPreviewView;Lcom/facebook/photos/creativecam/ui/FocusView;LX/5fM;LX/9c7;LX/89Z;)Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    move-result-object v6

    move-object v2, v6

    .line 2750740
    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 2750741
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 2750742
    iget-object v3, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    iget-object v4, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->r:LX/5fM;

    .line 2750743
    iput-object v4, v3, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 2750744
    iget-object v3, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance v4, LX/BFw;

    invoke-direct {v4, v2}, LX/BFw;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    invoke-virtual {v3, v4}, Lcom/facebook/optic/CameraPreviewView;->setCameraInitialisedCallback(LX/5fb;)V

    .line 2750745
    iget-object v3, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance v4, LX/BFx;

    invoke-direct {v4, v2}, LX/BFx;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    invoke-virtual {v3, v4}, Lcom/facebook/optic/CameraPreviewView;->setFocusCallbackListener(LX/5fY;)V

    .line 2750746
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v3, 0x7f0d087c

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 2750747
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2750748
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v3, 0x7f0d2372    # 1.876052E38f

    invoke-static {v2, v3}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->F:LX/0am;

    .line 2750749
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v3, 0x7f0d1252

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 2750750
    iget-object v4, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v4}, LX/89S;->g()LX/89e;

    move-result-object v4

    .line 2750751
    if-nez v4, :cond_1

    .line 2750752
    iget-object v4, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->z:LX/BFk;

    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->F:LX/0am;

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->F:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    move-object v6, v5

    :goto_2
    iget-object v7, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750753
    iget-object v8, v5, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->i:Ljava/lang/String;

    move-object v8, v8

    .line 2750754
    iget-object v9, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v5}, LX/89S;->f()LX/B4P;

    move-result-object v10

    move-object v5, v2

    invoke-virtual/range {v4 .. v10}, LX/BFk;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;LX/9c7;Ljava/lang/String;LX/89Z;LX/B4P;)LX/BFj;

    move-result-object v4

    .line 2750755
    :cond_1
    iget-object v5, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->p:LX/0Px;

    invoke-interface {v4, v2, v5}, LX/89e;->a(Landroid/view/View;LX/0Px;)V

    .line 2750756
    new-instance v5, LX/Jvf;

    invoke-direct {v5, v0}, LX/Jvf;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-interface {v4, v5}, LX/89e;->a(LX/Jvf;)V

    .line 2750757
    move-object v2, v4

    .line 2750758
    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    .line 2750759
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, LX/Jvo;

    invoke-direct {v3, v0}, LX/Jvo;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2750760
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v2, 0x7f0d0753

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2750761
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    if-eqz v2, :cond_a

    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v2}, LX/89S;->e()LX/BFW;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 2750762
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v2}, LX/89S;->e()LX/BFW;

    move-result-object v2

    .line 2750763
    :goto_3
    move-object v2, v2

    .line 2750764
    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->R:LX/BFW;

    .line 2750765
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->R:LX/BFW;

    .line 2750766
    const v3, 0x7f0301cc

    invoke-virtual {v1, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2750767
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, LX/BFW;->a:Landroid/view/View;

    .line 2750768
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750769
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->R:LX/BFW;

    .line 2750770
    iget-object v2, v1, LX/BFW;->a:Landroid/view/View;

    const v3, 0x7f0d0863

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v2

    .line 2750771
    iput-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2750772
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v2, 0x7f0d0865

    invoke-static {v1, v2}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    .line 2750773
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v1, :cond_2

    .line 2750774
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750775
    iget-boolean v2, v1, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->e:Z

    move v1, v2

    .line 2750776
    if-eqz v1, :cond_7

    .line 2750777
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2750778
    :cond_2
    :goto_4
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsPhotos()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsVideos()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2750779
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2750780
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->setVisibility(I)V

    .line 2750781
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;

    new-instance v2, LX/Jvs;

    invoke-direct {v2, v0}, LX/Jvs;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750782
    iput-object v2, v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->h:LX/BFc;

    .line 2750783
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;

    iget v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->W:I

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->setSelectedPosition(I)V

    .line 2750784
    :cond_3
    iget v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->W:I

    if-nez v1, :cond_b

    .line 2750785
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750786
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->p(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750787
    :goto_5
    return-void

    .line 2750788
    :cond_4
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 2750789
    :cond_5
    sget-object v3, LX/5fM;->BACK:LX/5fM;

    move-object v4, v3

    goto/16 :goto_1

    .line 2750790
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 2750791
    :cond_7
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, LX/Jvp;

    invoke-direct {v2, v0}, LX/Jvp;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2750792
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v1}, LX/89S;->c()LX/89c;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 2750793
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v1}, LX/89S;->c()LX/89c;

    move-result-object v1

    .line 2750794
    :goto_6
    move-object v1, v1

    .line 2750795
    iput-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->T:LX/89c;

    .line 2750796
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->T:LX/89c;

    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v3, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->W:I

    invoke-interface {v1, v2, v3}, LX/89c;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;I)V

    goto :goto_4

    .line 2750797
    :cond_8
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsPhotos()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2750798
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750799
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->p(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    goto :goto_5

    .line 2750800
    :cond_9
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->s$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    goto :goto_5

    .line 2750801
    :cond_a
    iget-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BFW;

    goto/16 :goto_3

    .line 2750802
    :cond_b
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->s$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    goto :goto_5

    .line 2750803
    :cond_c
    iget-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    goto :goto_6
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2750806
    invoke-direct {p0}, LX/Jvm;->c()V

    .line 2750807
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2750804
    invoke-direct {p0}, LX/Jvm;->c()V

    .line 2750805
    return-void
.end method
