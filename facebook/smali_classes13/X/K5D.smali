.class public LX/K5D;
.super LX/3hi;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final b:Landroid/renderscript/RenderScript;

.field private final c:Landroid/renderscript/ScriptIntrinsicBlur;

.field private final d:Ljava/lang/String;

.field private final e:LX/1bh;

.field private final f:F

.field public final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2769995
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 2769996
    iput-object p2, p0, LX/K5D;->d:Ljava/lang/String;

    .line 2769997
    invoke-static {p1}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v0

    iput-object v0, p0, LX/K5D;->b:Landroid/renderscript/RenderScript;

    .line 2769998
    iget-object v0, p0, LX/K5D;->b:Landroid/renderscript/RenderScript;

    iget-object v1, p0, LX/K5D;->b:Landroid/renderscript/RenderScript;

    invoke-static {v1}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v0

    iput-object v0, p0, LX/K5D;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    .line 2769999
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2770000
    div-int/lit8 v1, v0, 0xa

    iput v1, p0, LX/K5D;->g:I

    .line 2770001
    int-to-float v0, v0

    const v1, 0x3b449ba6    # 0.003f

    mul-float/2addr v0, v1

    iput v0, p0, LX/K5D;->f:F

    .line 2770002
    new-instance v0, LX/1ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "blur:radius="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/K5D;->f:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":dimen:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/K5D;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/K5D;->e:LX/1bh;

    .line 2770003
    return-void
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2769986
    iget-object v0, p0, LX/K5D;->e:LX/1bh;

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 2769988
    iget-object v0, p0, LX/K5D;->b:Landroid/renderscript/RenderScript;

    invoke-static {v0, p2}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v0

    .line 2769989
    iget-object v1, p0, LX/K5D;->b:Landroid/renderscript/RenderScript;

    invoke-static {v1, p1}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v1

    .line 2769990
    iget-object v2, p0, LX/K5D;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    iget v3, p0, LX/K5D;->f:F

    invoke-virtual {v2, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    .line 2769991
    iget-object v2, p0, LX/K5D;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    invoke-virtual {v2, v0}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    .line 2769992
    iget-object v0, p0, LX/K5D;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    invoke-virtual {v0, v1}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    .line 2769993
    invoke-virtual {v1, p1}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V

    .line 2769994
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2769987
    iget-object v0, p0, LX/K5D;->d:Ljava/lang/String;

    return-object v0
.end method
