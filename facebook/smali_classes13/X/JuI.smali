.class public LX/JuI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JuI;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Yg;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Yg;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/app/NotificationManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1rn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2748657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2748658
    iput-object p1, p0, LX/JuI;->a:LX/0Ot;

    .line 2748659
    iput-object p2, p0, LX/JuI;->b:LX/0Ot;

    .line 2748660
    iput-object p3, p0, LX/JuI;->c:LX/0Ot;

    .line 2748661
    return-void
.end method

.method public static a(LX/0QB;)LX/JuI;
    .locals 6

    .prologue
    .line 2748662
    sget-object v0, LX/JuI;->d:LX/JuI;

    if-nez v0, :cond_1

    .line 2748663
    const-class v1, LX/JuI;

    monitor-enter v1

    .line 2748664
    :try_start_0
    sget-object v0, LX/JuI;->d:LX/JuI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2748665
    if-eqz v2, :cond_0

    .line 2748666
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2748667
    new-instance v3, LX/JuI;

    const/16 v4, 0xe48

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v5

    const/16 p0, 0x6

    invoke-static {v5, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xe8b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/JuI;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2748668
    move-object v0, v3

    .line 2748669
    sput-object v0, LX/JuI;->d:LX/JuI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2748670
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2748671
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2748672
    :cond_1
    sget-object v0, LX/JuI;->d:LX/JuI;

    return-object v0

    .line 2748673
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2748674
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
