.class public final LX/Jnl;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/montage/model/art/ArtItem;

.field public final synthetic b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/art/ArtItemView;Lcom/facebook/messaging/montage/model/art/ArtItem;)V
    .locals 0

    .prologue
    .line 2734378
    iput-object p1, p0, LX/Jnl;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    iput-object p2, p0, LX/Jnl;->a:Lcom/facebook/messaging/montage/model/art/ArtItem;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2734379
    iget-object v0, p0, LX/Jnl;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2734380
    :goto_0
    return-void

    .line 2734381
    :cond_0
    iget-object v0, p0, LX/Jnl;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->c()V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734382
    invoke-direct {p0}, LX/Jnl;->b()V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2734383
    iget-object v0, p0, LX/Jnl;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2734384
    :goto_0
    return-void

    .line 2734385
    :cond_0
    sget-object v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a:Ljava/lang/String;

    const-string v1, "Failed to load thumbnail: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Jnl;->a:Lcom/facebook/messaging/montage/model/art/ArtItem;

    .line 2734386
    iget-object p1, v4, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    move-object v4, p1

    .line 2734387
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2734388
    iget-object v0, p0, LX/Jnl;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->c()V

    goto :goto_0
.end method
