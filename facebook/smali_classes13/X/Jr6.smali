.class public LX/Jr6;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final p:Ljava/lang/Object;


# instance fields
.field private final a:LX/Ifg;

.field private final b:LX/Jrb;

.field private final c:LX/Jrc;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/FO4;

.field private final f:LX/FDI;

.field private final g:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

.field private final h:LX/2Lw;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0SG;

.field private final k:LX/3QK;

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2OS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lj;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741914
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr6;->p:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Ifg;LX/Jrb;LX/Jrc;LX/0Ot;LX/FO4;LX/FDI;Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;LX/2Lw;LX/0Or;LX/0SG;LX/3QK;)V
    .locals 1
    .param p6    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ifg;",
            "LX/Jrb;",
            "LX/Jrc;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/FO4;",
            "LX/FDI;",
            "Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;",
            "LX/2Lw;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/3QK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2741893
    invoke-direct {p0, p4}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2741894
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741895
    iput-object v0, p0, LX/Jr6;->l:LX/0Ot;

    .line 2741896
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741897
    iput-object v0, p0, LX/Jr6;->m:LX/0Ot;

    .line 2741898
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741899
    iput-object v0, p0, LX/Jr6;->n:LX/0Ot;

    .line 2741900
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741901
    iput-object v0, p0, LX/Jr6;->o:LX/0Ot;

    .line 2741902
    iput-object p1, p0, LX/Jr6;->a:LX/Ifg;

    .line 2741903
    iput-object p2, p0, LX/Jr6;->b:LX/Jrb;

    .line 2741904
    iput-object p3, p0, LX/Jr6;->c:LX/Jrc;

    .line 2741905
    iput-object p4, p0, LX/Jr6;->d:LX/0Ot;

    .line 2741906
    iput-object p5, p0, LX/Jr6;->e:LX/FO4;

    .line 2741907
    iput-object p6, p0, LX/Jr6;->f:LX/FDI;

    .line 2741908
    iput-object p7, p0, LX/Jr6;->g:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    .line 2741909
    iput-object p8, p0, LX/Jr6;->h:LX/2Lw;

    .line 2741910
    iput-object p9, p0, LX/Jr6;->i:LX/0Or;

    .line 2741911
    iput-object p10, p0, LX/Jr6;->j:LX/0SG;

    .line 2741912
    iput-object p11, p0, LX/Jr6;->k:LX/3QK;

    .line 2741913
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2741890
    invoke-virtual {p1}, LX/6kW;->d()LX/6k4;

    move-result-object v0

    .line 2741891
    iget-object v1, p0, LX/Jr6;->c:LX/Jrc;

    iget-object v0, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2741892
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jr6;
    .locals 7

    .prologue
    .line 2741863
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741864
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741865
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741866
    if-nez v1, :cond_0

    .line 2741867
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741868
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741869
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741870
    sget-object v1, LX/Jr6;->p:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741871
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741872
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741873
    :cond_1
    if-nez v1, :cond_4

    .line 2741874
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741875
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741876
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jr6;->b(LX/0QB;)LX/Jr6;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2741877
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741878
    if-nez v1, :cond_2

    .line 2741879
    sget-object v0, LX/Jr6;->p:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr6;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741880
    :goto_1
    if-eqz v0, :cond_3

    .line 2741881
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741882
    :goto_3
    check-cast v0, LX/Jr6;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741883
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741884
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741885
    :catchall_1
    move-exception v0

    .line 2741886
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741887
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741888
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741889
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr6;->p:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr6;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/6k4;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;
    .locals 4

    .prologue
    .line 2741859
    iget-object v0, p0, LX/Jr6;->e:LX/FO4;

    iget-object v1, p1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v1, v1, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FO4;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2741860
    if-nez v0, :cond_0

    .line 2741861
    iget-object v0, p0, LX/Jr6;->b:LX/Jrb;

    invoke-virtual {v0, p1, p2}, LX/Jrb;->a(LX/6k4;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2741862
    :cond_0
    return-object v0
.end method

.method private static b(LX/0QB;)LX/Jr6;
    .locals 12

    .prologue
    .line 2741855
    new-instance v0, LX/Jr6;

    invoke-static {p0}, LX/Ifg;->b(LX/0QB;)LX/Ifg;

    move-result-object v1

    check-cast v1, LX/Ifg;

    invoke-static {p0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v2

    check-cast v2, LX/Jrb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v3

    check-cast v3, LX/Jrc;

    const/16 v4, 0x35bd

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/FO4;->a(LX/0QB;)LX/FO4;

    move-result-object v5

    check-cast v5, LX/FO4;

    invoke-static {p0}, LX/FDK;->a(LX/0QB;)LX/FDI;

    move-result-object v6

    check-cast v6, LX/FDI;

    invoke-static {p0}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->b(LX/0QB;)Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    invoke-static {p0}, LX/2Lw;->a(LX/0QB;)LX/2Lw;

    move-result-object v8

    check-cast v8, LX/2Lw;

    const/16 v9, 0x15e8

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {p0}, LX/3QK;->a(LX/0QB;)LX/3QK;

    move-result-object v11

    check-cast v11, LX/3QK;

    invoke-direct/range {v0 .. v11}, LX/Jr6;-><init>(LX/Ifg;LX/Jrb;LX/Jrc;LX/0Ot;LX/FO4;LX/FDI;Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;LX/2Lw;LX/0Or;LX/0SG;LX/3QK;)V

    .line 2741856
    const/16 v1, 0xcd6

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1116

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x29c9

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1032

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 2741857
    iput-object v1, v0, LX/Jr6;->l:LX/0Ot;

    iput-object v2, v0, LX/Jr6;->m:LX/0Ot;

    iput-object v3, v0, LX/Jr6;->n:LX/0Ot;

    iput-object v4, v0, LX/Jr6;->o:LX/0Ot;

    .line 2741858
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741853
    check-cast p1, LX/6kW;

    .line 2741854
    invoke-direct {p0, p1}, LX/Jr6;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6k4;J)Landroid/os/Bundle;
    .locals 15

    .prologue
    .line 2741807
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-direct {p0, v0, v1}, LX/Jr6;->a(LX/6k4;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v13

    .line 2741808
    iget-object v2, p0, LX/Jr6;->a:LX/Ifg;

    invoke-virtual {v2, v13}, LX/Ifg;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2741809
    iget-object v4, v13, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 2741810
    iget-object v6, v2, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    if-eqz v6, :cond_2

    iget-object v2, v2, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/attachment/AudioData;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2741811
    iget-object v2, p0, LX/Jr6;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v6, LX/3Dx;->A:S

    const/4 v7, 0x0

    invoke-interface {v2, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2741812
    iget-object v2, p0, LX/Jr6;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2OS;

    invoke-virtual {v2, v13}, LX/2OS;->f(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/AudioAttachmentData;

    move-result-object v10

    .line 2741813
    iget-object v2, p0, LX/Jr6;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Lj;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v13, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-wide v6, v13, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v10}, Lcom/facebook/messaging/attachments/AudioAttachmentData;->a()J

    move-result-wide v8

    invoke-virtual {v10}, Lcom/facebook/messaging/attachments/AudioAttachmentData;->b()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v13, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual/range {v2 .. v12}, LX/3Lj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Z)V

    .line 2741814
    :cond_0
    iget-object v2, p0, LX/Jr6;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Jqf;

    const/4 v3, 0x0

    move-wide/from16 v0, p3

    invoke-virtual {v2, v13, v0, v1, v3}, LX/Jqf;->a(Lcom/facebook/messaging/model/messages/Message;JZ)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v2

    .line 2741815
    invoke-virtual {v2}, Lcom/facebook/messaging/service/model/NewMessageResult;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2741816
    move-object/from16 v0, p2

    iget-object v3, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v4, v3, LX/6kn;->messageId:Ljava/lang/String;

    .line 2741817
    move-object/from16 v0, p2

    iget-object v3, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v3}, LX/6l9;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2741818
    move-object/from16 v0, p2

    iget-object v3, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->timestamp:Ljava/lang/Long;

    if-eqz v3, :cond_3

    move-object/from16 v0, p2

    iget-object v3, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->timestamp:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2741819
    :goto_1
    move-object/from16 v0, p2

    iget-object v3, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_4

    move-object/from16 v0, p2

    iget-object v3, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2741820
    :goto_2
    iget-object v3, p0, LX/Jr6;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7G0;

    invoke-virtual/range {v3 .. v9}, LX/7G0;->a(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 2741821
    :cond_1
    iget-object v3, p0, LX/Jr6;->g:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v13}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2741822
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2741823
    const-string v4, "newMessageResult"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2741824
    return-object v3

    .line 2741825
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0

    .line 2741826
    :cond_3
    const-wide/16 v6, -0x1

    goto :goto_1

    .line 2741827
    :cond_4
    const-wide/16 v8, -0x1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741845
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->d()LX/6k4;

    move-result-object v1

    .line 2741846
    iget-object v0, p0, LX/Jr6;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7G0;

    iget-object v2, v1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v2, v2, LX/6kn;->messageId:Ljava/lang/String;

    .line 2741847
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "deltas_sync"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2741848
    sget-object v4, LX/03R;->YES:LX/03R;

    iget-object v5, v0, LX/7G0;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2741849
    const-string v4, "upload_this_event_now"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 2741850
    :cond_0
    const-string v4, "message_id"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2741851
    iget-object v4, v0, LX/7G0;->a:LX/7G1;

    sget-object v5, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v4, v3, v5}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 2741852
    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {p0, p1, v1, v2, v3}, LX/Jr6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6k4;J)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6k4;Lcom/facebook/messaging/service/model/NewMessageResult;J)V
    .locals 5

    .prologue
    .line 2741839
    iget-object v0, p0, LX/Jr6;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqf;

    invoke-virtual {v0, p2, p1, p3, p4}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;LX/6k4;J)V

    .line 2741840
    iget-object v0, p1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2741841
    iget-object v1, p0, LX/Jr6;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2741842
    iget-object v0, p0, LX/Jr6;->h:LX/2Lw;

    sget-object v1, LX/FCY;->GRAPH:LX/FCY;

    iget-object v2, p1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v2, v2, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V

    .line 2741843
    :goto_0
    return-void

    .line 2741844
    :cond_0
    iget-object v0, p0, LX/Jr6;->k:LX/3QK;

    const-string v1, "cache"

    iget-object v2, p1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v2, v2, LX/6kn;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/3QK;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/7GJ;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2741832
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->d()LX/6k4;

    move-result-object v0

    .line 2741833
    invoke-direct {p0, v0, p2}, LX/Jr6;->a(LX/6k4;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2741834
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v4, 0x0

    iget-object v5, p0, LX/Jr6;->j:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741835
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v0, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2741836
    iget-object v0, p0, LX/Jr6;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqf;

    invoke-virtual {v0, v1, v2}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;Z)V

    .line 2741837
    iget-object v0, p0, LX/Jr6;->k:LX/3QK;

    const-string v1, "recovered"

    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/3QK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741838
    return-void
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741829
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2741830
    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v1}, LX/6kW;->d()LX/6k4;

    move-result-object v1

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {p0, v1, v0, v2, v3}, LX/Jr6;->a(LX/6k4;Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2741831
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741828
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/Jr6;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
