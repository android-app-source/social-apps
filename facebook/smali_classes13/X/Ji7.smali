.class public final LX/Ji7;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/JiJ;


# direct methods
.method public constructor <init>(LX/JiJ;)V
    .locals 0

    .prologue
    .line 2725712
    iput-object p1, p0, LX/Ji7;->a:LX/JiJ;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2725713
    iget-object v0, p0, LX/Ji7;->a:LX/JiJ;

    const-string v1, "picker_contact_upload_success"

    invoke-static {v0, v1}, LX/JiJ;->a$redex0(LX/JiJ;Ljava/lang/String;)V

    .line 2725714
    iget-object v0, p0, LX/Ji7;->a:LX/JiJ;

    .line 2725715
    iget-object v1, v0, LX/JiJ;->t:LX/JiI;

    if-eqz v1, :cond_0

    .line 2725716
    iget-object v1, v0, LX/JiJ;->a:LX/94q;

    invoke-virtual {v1}, LX/94q;->b()Lcom/facebook/contacts/upload/ContactsUploadState;

    .line 2725717
    :cond_0
    invoke-static {v0}, LX/JiJ;->m(LX/JiJ;)V

    .line 2725718
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2725719
    iget-object v0, p0, LX/Ji7;->a:LX/JiJ;

    invoke-virtual {v0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2725720
    return-void
.end method
