.class public LX/JiV;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/FJw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

.field public c:Lcom/facebook/user/tiles/UserTileView;

.field public d:Lcom/facebook/messaging/presence/PresenceIndicatorView;

.field public e:LX/JiW;

.field public f:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2726002
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JiV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2726003
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2726000
    const v0, 0x7f0104c4

    invoke-direct {p0, p1, p2, v0}, LX/JiV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2726001
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2725989
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725990
    const-class v0, LX/JiV;

    invoke-static {v0, p0}, LX/JiV;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725991
    const v0, 0x7f030cfa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725992
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iput-object v0, p0, LX/JiV;->b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2725993
    const v0, 0x7f0d1e99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/JiV;->c:Lcom/facebook/user/tiles/UserTileView;

    .line 2725994
    const v0, 0x7f0d2017

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/presence/PresenceIndicatorView;

    iput-object v0, p0, LX/JiV;->d:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    .line 2725995
    const v0, 0x7f0d2068

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, LX/JiV;->f:Landroid/widget/CompoundButton;

    .line 2725996
    iget-object v0, p0, LX/JiV;->f:Landroid/widget/CompoundButton;

    new-instance v1, LX/JiU;

    invoke-direct {v1, p0}, LX/JiU;-><init>(LX/JiV;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2725997
    iget-object v0, p0, LX/JiV;->d:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    invoke-virtual {p0}, LX/JiV;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setTextColor(I)V

    .line 2725998
    iget-object v0, p0, LX/JiV;->d:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->AVAILABLE_ON_MOBILE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    .line 2725999
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JiV;

    invoke-static {p0}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object p0

    check-cast p0, LX/FJw;

    iput-object p0, p1, LX/JiV;->a:LX/FJw;

    return-void
.end method
