.class public LX/K3W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private b:Lcom/facebook/slideshow/ui/SoundListAdapter;

.field public c:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Lcom/facebook/slideshow/ui/SoundListAdapter;Landroid/content/Context;)V
    .locals 3
    .param p1    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2765837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2765838
    iput-object p1, p0, LX/K3W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2765839
    iput-object p2, p0, LX/K3W;->b:Lcom/facebook/slideshow/ui/SoundListAdapter;

    .line 2765840
    iget-object v0, p0, LX/K3W;->b:Lcom/facebook/slideshow/ui/SoundListAdapter;

    new-instance v1, LX/K3V;

    invoke-direct {v1, p0}, LX/K3V;-><init>(LX/K3W;)V

    .line 2765841
    iput-object v1, v0, Lcom/facebook/slideshow/ui/SoundListAdapter;->e:LX/K3V;

    .line 2765842
    iget-object v0, p0, LX/K3W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/K3W;->b:Lcom/facebook/slideshow/ui/SoundListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2765843
    iget-object v0, p0, LX/K3W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2765844
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V
    .locals 1
    .param p2    # Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLInterfaces$FBApplicationWithMoodsFragment$MovieFactoryConfig$Moods;",
            ">;",
            "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLInterfaces$FBApplicationWithMoodsFragment$MovieFactoryConfig$Moods;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2765845
    iget-object v0, p0, LX/K3W;->b:Lcom/facebook/slideshow/ui/SoundListAdapter;

    .line 2765846
    iput-object p1, v0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    .line 2765847
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2765848
    iget-object v0, p0, LX/K3W;->b:Lcom/facebook/slideshow/ui/SoundListAdapter;

    invoke-virtual {v0, p2}, Lcom/facebook/slideshow/ui/SoundListAdapter;->a(Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V

    .line 2765849
    return-void
.end method
