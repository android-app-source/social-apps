.class public final LX/JY8;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JYA;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/JY4;

.field public b:LX/JY6;

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/JYA;


# direct methods
.method public constructor <init>(LX/JYA;)V
    .locals 1

    .prologue
    .line 2705847
    iput-object p1, p0, LX/JY8;->d:LX/JYA;

    .line 2705848
    move-object v0, p1

    .line 2705849
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2705850
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2705851
    const-string v0, "ActionButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2705852
    if-ne p0, p1, :cond_1

    .line 2705853
    :cond_0
    :goto_0
    return v0

    .line 2705854
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2705855
    goto :goto_0

    .line 2705856
    :cond_3
    check-cast p1, LX/JY8;

    .line 2705857
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2705858
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2705859
    if-eq v2, v3, :cond_0

    .line 2705860
    iget-object v2, p0, LX/JY8;->a:LX/JY4;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JY8;->a:LX/JY4;

    iget-object v3, p1, LX/JY8;->a:LX/JY4;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2705861
    goto :goto_0

    .line 2705862
    :cond_5
    iget-object v2, p1, LX/JY8;->a:LX/JY4;

    if-nez v2, :cond_4

    .line 2705863
    :cond_6
    iget-object v2, p0, LX/JY8;->b:LX/JY6;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JY8;->b:LX/JY6;

    iget-object v3, p1, LX/JY8;->b:LX/JY6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2705864
    goto :goto_0

    .line 2705865
    :cond_8
    iget-object v2, p1, LX/JY8;->b:LX/JY6;

    if-nez v2, :cond_7

    .line 2705866
    :cond_9
    iget-object v2, p0, LX/JY8;->c:LX/1Pq;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JY8;->c:LX/1Pq;

    iget-object v3, p1, LX/JY8;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2705867
    goto :goto_0

    .line 2705868
    :cond_a
    iget-object v2, p1, LX/JY8;->c:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
