.class public final LX/JoG;
.super LX/JoF;
.source ""


# instance fields
.field public final synthetic g:LX/JoH;

.field private final h:F

.field private final i:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final j:I

.field private final k:Ljava/lang/String;

.field private final l:I

.field private final m:I

.field private final n:Z


# direct methods
.method public constructor <init>(LX/JoH;Landroid/content/Context;Ljava/lang/String;IIZ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2735315
    iput-object p1, p0, LX/JoG;->g:LX/JoH;

    .line 2735316
    const/4 v2, 0x0

    const/high16 v3, -0x1000000

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/JoF;-><init>(LX/JoH;Ljava/lang/String;IIZ)V

    .line 2735317
    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, LX/JoG;->h:F

    .line 2735318
    const v0, 0x7f0a019a

    invoke-static {p2, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/JoG;->i:I

    .line 2735319
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1f2f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JoG;->j:I

    .line 2735320
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/JoG;->k:Ljava/lang/String;

    .line 2735321
    iput p4, p0, LX/JoG;->l:I

    .line 2735322
    iput p5, p0, LX/JoG;->m:I

    .line 2735323
    iput-boolean p6, p0, LX/JoG;->n:Z

    .line 2735324
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1FZ;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2735325
    iget v1, p0, LX/JoG;->l:I

    iget v2, p0, LX/JoG;->m:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_0
    invoke-virtual {p2, v1, v2, v0}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v8

    .line 2735326
    new-instance v9, Landroid/graphics/Canvas;

    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v9, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2735327
    new-instance v2, Landroid/text/TextPaint;

    const/16 v0, 0x45

    invoke-direct {v2, v0}, Landroid/text/TextPaint;-><init>(I)V

    .line 2735328
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2735329
    iget v0, p0, LX/JoG;->i:I

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2735330
    iget v0, p0, LX/JoG;->j:I

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2735331
    invoke-virtual {v9, v2}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 2735332
    const/4 v0, -0x1

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2735333
    iget v0, p0, LX/JoG;->l:I

    int-to-float v0, v0

    const v1, 0x3d4ccccd    # 0.05f

    mul-float/2addr v0, v1

    float-to-int v10, v0

    .line 2735334
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, LX/JoG;->k:Ljava/lang/String;

    iget v3, p0, LX/JoG;->l:I

    mul-int/lit8 v4, v10, 0x2

    sub-int/2addr v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2735335
    iget v1, p0, LX/JoG;->m:I

    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2735336
    invoke-virtual {v9}, Landroid/graphics/Canvas;->save()I

    .line 2735337
    int-to-float v2, v10

    int-to-float v1, v1

    invoke-virtual {v9, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2735338
    invoke-virtual {v0, v9}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 2735339
    invoke-virtual {v9}, Landroid/graphics/Canvas;->restore()V

    .line 2735340
    iget-boolean v0, p0, LX/JoG;->n:Z

    if-eqz v0, :cond_1

    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-super {p0, v0, p2}, LX/JoF;->a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2735341
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v8

    .line 2735342
    goto :goto_1
.end method

.method public final a()LX/1bh;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2735343
    new-instance v0, LX/1ed;

    iget-object v1, p0, LX/JoG;->k:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2735344
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
