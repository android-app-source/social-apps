.class public final LX/Jd4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3x2;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

.field public final synthetic b:LX/Jd5;


# direct methods
.method public constructor <init>(LX/Jd5;Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 0

    .prologue
    .line 2718782
    iput-object p1, p0, LX/Jd4;->b:LX/Jd5;

    iput-object p2, p0, LX/Jd4;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 2718783
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d31f7

    if-ne v0, v1, :cond_1

    .line 2718784
    iget-object v0, p0, LX/Jd4;->b:LX/Jd5;

    iget-object v0, v0, LX/Jd5;->a:LX/Jd6;

    iget-object v0, v0, LX/Jd6;->m:LX/Jcq;

    iget-object v1, p0, LX/Jd4;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718785
    iget-object v2, v0, LX/Jcq;->l:LX/JdA;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 2718786
    iget-object v2, v0, LX/Jcq;->l:LX/JdA;

    .line 2718787
    iget-object v3, v2, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    const/4 p1, 0x1

    .line 2718788
    iget-object p0, v3, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->k:LX/2Vu;

    iget-object v0, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/2Vu;->a(Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;

    move-result-object p0

    if-nez p0, :cond_2

    move p0, p1

    .line 2718789
    :goto_0
    new-instance v0, LX/31Y;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    if-eqz p0, :cond_3

    const p1, 0x7f083b4d

    :goto_1
    invoke-virtual {v0, p1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    if-eqz p0, :cond_4

    const p1, 0x7f083b4e

    :goto_2
    invoke-virtual {v0, p1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    if-eqz p0, :cond_5

    const p1, 0x7f083b4f

    :goto_3
    new-instance v2, LX/JdF;

    invoke-direct {v2, v3, p0, v1}, LX/JdF;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;ZLcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    invoke-virtual {v0, p1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    const p1, 0x7f080017

    new-instance v0, LX/JdE;

    invoke-direct {v0, v3}, LX/JdE;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    invoke-virtual {p0, p1, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    .line 2718790
    :cond_0
    const/4 v0, 0x1

    .line 2718791
    :goto_4
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_4

    .line 2718792
    :cond_2
    const/4 p0, 0x0

    goto :goto_0

    .line 2718793
    :cond_3
    const p1, 0x7f083b4a

    goto :goto_1

    :cond_4
    const p1, 0x7f083b4b

    goto :goto_2

    :cond_5
    const p1, 0x7f083b4c

    goto :goto_3
.end method
