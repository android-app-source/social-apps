.class public LX/JeW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JeV;


# instance fields
.field private final a:Lcom/facebook/messaging/blocking/BlockingUtils;

.field private final b:LX/JdU;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/blocking/BlockingUtils;LX/JdU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720827
    iput-object p1, p0, LX/JeW;->a:Lcom/facebook/messaging/blocking/BlockingUtils;

    .line 2720828
    iput-object p2, p0, LX/JeW;->b:LX/JdU;

    .line 2720829
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2720833
    iget-object v0, p0, LX/JeW;->a:Lcom/facebook/messaging/blocking/BlockingUtils;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Ljava/lang/String;LX/2h0;)V

    .line 2720834
    iget-object v0, p0, LX/JeW;->b:LX/JdU;

    invoke-virtual {v0, p1}, LX/JdU;->c(Ljava/lang/String;)V

    .line 2720835
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2720830
    iget-object v0, p0, LX/JeW;->a:Lcom/facebook/messaging/blocking/BlockingUtils;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/messaging/blocking/BlockingUtils;->b(Ljava/lang/String;LX/2h0;)V

    .line 2720831
    iget-object v0, p0, LX/JeW;->b:LX/JdU;

    invoke-virtual {v0, p1}, LX/JdU;->b(Ljava/lang/String;)V

    .line 2720832
    return-void
.end method
