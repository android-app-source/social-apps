.class public LX/Jk1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0W9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2728675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2728676
    return-void
.end method

.method public static b(LX/0QB;)LX/Jk1;
    .locals 3

    .prologue
    .line 2728677
    new-instance v2, LX/Jk1;

    invoke-direct {v2}, LX/Jk1;-><init>()V

    .line 2728678
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    .line 2728679
    iput-object v0, v2, LX/Jk1;->a:Landroid/content/Context;

    iput-object v1, v2, LX/Jk1;->b:LX/0W9;

    .line 2728680
    return-object v2
.end method

.method public static b(LX/Jk1;J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 2728681
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 2728682
    iget-object v1, p0, LX/Jk1;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083bbe

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2728683
    iget-object v5, p0, LX/Jk1;->a:Landroid/content/Context;

    const v6, 0x8001a

    invoke-static {v5, p1, p2, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2728684
    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p0, p1, p2}, LX/Jk1;->d(LX/Jk1;J)Ljava/text/SimpleDateFormat;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/Jk1;J)Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 2728685
    iget-object v0, p0, LX/Jk1;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2728686
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    iget-object v2, p0, LX/Jk1;->b:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2728687
    :goto_0
    return-object v0

    .line 2728688
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2728689
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2728690
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 2728691
    if-eqz v0, :cond_1

    .line 2728692
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm aaa"

    iget-object v2, p0, LX/Jk1;->b:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 2728693
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h aaa"

    iget-object v2, p0, LX/Jk1;->b:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JLX/Jk0;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2728694
    sget-object v0, LX/Jjz;->a:[I

    invoke-virtual {p3}, LX/Jk0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2728695
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Time format style wasn\'t recognized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2728696
    :pswitch_0
    invoke-static {p0, p1, p2}, LX/Jk1;->b(LX/Jk1;J)Ljava/lang/String;

    move-result-object v0

    .line 2728697
    :goto_0
    return-object v0

    .line 2728698
    :pswitch_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 2728699
    invoke-static {p1, p2}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2728700
    iget-object v1, p0, LX/Jk1;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083bbf

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0, p1, p2}, LX/Jk1;->d(LX/Jk1;J)Ljava/text/SimpleDateFormat;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2728701
    :goto_1
    move-object v0, v0

    .line 2728702
    goto :goto_0

    :cond_0
    invoke-static {p0, p1, p2}, LX/Jk1;->b(LX/Jk1;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
