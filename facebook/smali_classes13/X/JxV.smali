.class public final LX/JxV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V
    .locals 0

    .prologue
    .line 2753057
    iput-object p1, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2753058
    iget-object v0, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    invoke-static {v0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->u(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    .line 2753059
    iget-object v0, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    invoke-static {v0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->t(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2753060
    iget-object v0, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    iget-object v0, v0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->v:LX/Jxi;

    iget-object v1, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    iget-object v1, v1, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    .line 2753061
    iget-object v2, v1, LX/Jxf;->a:Ljava/util/List;

    invoke-static {v2}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v2

    move-object v1, v2

    .line 2753062
    iget-object v2, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    iget-object v2, v2, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->F:LX/J82;

    .line 2753063
    iget-object v3, v2, LX/J82;->b:Ljava/util/List;

    move-object v2, v3

    .line 2753064
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "prof_raters_ads_only_mobile"

    invoke-direct {v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2753065
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Jxl;

    .line 2753066
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2753067
    iget-object v7, v3, LX/Jxl;->b:LX/Jxk;

    move-object v7, v7

    .line 2753068
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2753069
    iget v7, v3, LX/Jxl;->c:I

    move v7, v7

    .line 2753070
    invoke-virtual {v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2753071
    const-string v6, "comment"

    .line 2753072
    iget-object v7, v3, LX/Jxl;->a:Ljava/lang/String;

    move-object v3, v7

    .line 2753073
    invoke-virtual {v4, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2753074
    :cond_0
    const-string v5, "tracking_data"

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2753075
    iget-object v3, v0, LX/Jxi;->a:LX/0Zb;

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2753076
    iget-object v0, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    invoke-static {v0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->o(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    .line 2753077
    :goto_1
    return-void

    .line 2753078
    :cond_1
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/JxV;->a:Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2753079
    const v1, 0x7f083c19

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f083c1a

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f083c18

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2753080
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_1
.end method
