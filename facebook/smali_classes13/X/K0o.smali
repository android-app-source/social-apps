.class public LX/K0o;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# annotations
.annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2759581
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 2759582
    const/4 v0, 0x1

    .line 2759583
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2759584
    new-instance v0, LX/K0i;

    invoke-direct {v0}, LX/K0i;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 2759585
    new-instance v0, LX/1P1;

    invoke-direct {v0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2759586
    new-instance v0, LX/K0l;

    invoke-direct {v0, p0}, LX/K0l;-><init>(LX/K0o;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2759587
    return-void
.end method

.method public static h(LX/K0o;I)V
    .locals 4

    .prologue
    .line 2759588
    iget-boolean v0, p0, LX/K0o;->h:Z

    if-eqz v0, :cond_0

    .line 2759589
    invoke-virtual {p0}, LX/K0o;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2759590
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2759591
    new-instance v1, LX/5s6;

    invoke-virtual {p0}, LX/K0o;->getId()I

    move-result v2

    invoke-virtual {p0}, LX/K0o;->getWidth()I

    move-result v3

    invoke-direct {v1, v2, v3, p1}, LX/5s6;-><init>(III)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2759592
    :cond_0
    return-void
.end method

.method private l()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2759593
    invoke-virtual {p0}, LX/K0o;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 2759594
    invoke-virtual {p0, v0}, LX/K0o;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2759595
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v2

    .line 2759596
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2759597
    check-cast v0, LX/K0l;

    invoke-virtual {v0, v2}, LX/K0l;->g(I)I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2759598
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2759599
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2759600
    check-cast v0, LX/K0l;

    invoke-virtual {v0, p1, p2}, LX/K0l;->a(Landroid/view/View;I)V

    .line 2759601
    return-void
.end method

.method public final b(IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2759602
    invoke-direct {p0}, LX/K0o;->l()I

    move-result v0

    sub-int v0, p1, v0

    .line 2759603
    if-eqz p2, :cond_0

    .line 2759604
    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    .line 2759605
    :goto_0
    return-void

    .line 2759606
    :cond_0
    invoke-virtual {p0, v1, v0}, LX/K0o;->scrollBy(II)V

    goto :goto_0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 2759607
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2759608
    check-cast v0, LX/K0l;

    invoke-virtual {v0, p1}, LX/K0l;->e(I)V

    .line 2759609
    return-void
.end method

.method public final g(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2759610
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2759611
    check-cast v0, LX/K0l;

    invoke-virtual {v0, p1}, LX/K0l;->f(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildCountFromAdapter()I
    .locals 1

    .prologue
    .line 2759612
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2759613
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2759614
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2759615
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2759616
    const/4 v0, 0x1

    .line 2759617
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onScrollChanged(IIII)V
    .locals 9

    .prologue
    .line 2759618
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    .line 2759619
    invoke-virtual {p0}, LX/K0o;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2759620
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v8, v1

    .line 2759621
    invoke-virtual {p0}, LX/K0o;->getId()I

    move-result v0

    sget-object v1, LX/FUv;->SCROLL:LX/FUv;

    const/4 v2, 0x0

    invoke-direct {p0}, LX/K0o;->l()I

    move-result v3

    invoke-virtual {p0}, LX/K0o;->getWidth()I

    move-result v4

    .line 2759622
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v5, v5

    .line 2759623
    check-cast v5, LX/K0l;

    .line 2759624
    iget v6, v5, LX/K0l;->d:I

    move v5, v6

    .line 2759625
    invoke-virtual {p0}, LX/K0o;->getWidth()I

    move-result v6

    invoke-virtual {p0}, LX/K0o;->getHeight()I

    move-result v7

    invoke-static/range {v0 .. v7}, LX/FUu;->a(ILX/FUv;IIIIII)LX/FUu;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/5s9;->a(LX/5r0;)V

    .line 2759626
    return-void
.end method

.method public setSendContentSizeChangeEvents(Z)V
    .locals 0

    .prologue
    .line 2759627
    iput-boolean p1, p0, LX/K0o;->h:Z

    .line 2759628
    return-void
.end method
