.class public LX/JdN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/util/List",
        "<",
        "LX/JdL;",
        ">;",
        "Ljava/util/List",
        "<",
        "LX/JdM;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dC;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2719433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2719434
    iput-object p1, p0, LX/JdN;->a:LX/0dC;

    .line 2719435
    iput-object p2, p0, LX/JdN;->b:LX/0Or;

    .line 2719436
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 2719437
    check-cast p1, Ljava/util/List;

    .line 2719438
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2719439
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2719440
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/JdN;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2719441
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2719442
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JdL;

    .line 2719443
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 2719444
    const-string v5, "uid"

    iget-object v6, v0, LX/JdL;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2719445
    iget-wide v6, v0, LX/JdL;->c:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 2719446
    const-string v5, "last_update_time"

    iget-wide v6, v0, LX/JdL;->c:J

    invoke-virtual {v4, v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2719447
    :cond_0
    const-string v5, "session_token"

    iget-object v0, v0, LX/JdL;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2719448
    invoke-virtual {v2, v4}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 2719449
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "accounts"

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2719450
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "switchAccountsGetUnseenCounts"

    .line 2719451
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2719452
    move-object v0, v0

    .line 2719453
    const-string v2, "POST"

    .line 2719454
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2719455
    move-object v2, v0

    .line 2719456
    const-string v3, "%s/messenger_accounts"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, p0, LX/JdN;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2719457
    iget-object v6, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v6

    .line 2719458
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2719459
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2719460
    move-object v0, v2

    .line 2719461
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2719462
    move-object v0, v0

    .line 2719463
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2719464
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2719465
    move-object v0, v0

    .line 2719466
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2719467
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2719468
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2719469
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2719470
    const-string v1, "accounts"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2719471
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 2719472
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719473
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2719474
    new-instance v1, LX/JdM;

    const-string v2, "uid"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "is_token_valid"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->g(LX/0lF;)Z

    move-result v3

    const-string v4, "badge_count"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    const-string v5, "notification_text"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "last_update_time"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->c(LX/0lF;)J

    move-result-wide v6

    const-string v8, "badge_count_since_last_update_time"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v8

    invoke-direct/range {v1 .. v8}, LX/JdM;-><init>(Ljava/lang/String;ZILjava/lang/String;JI)V

    .line 2719475
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2719476
    :cond_0
    return-object v9
.end method
