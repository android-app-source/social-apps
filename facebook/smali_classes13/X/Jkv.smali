.class public LX/Jkv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:Ljava/lang/Integer;


# instance fields
.field public final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730640
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/Jkv;->a:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2730632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730633
    new-instance v0, LX/Jku;

    invoke-direct {v0, p0}, LX/Jku;-><init>(LX/Jkv;)V

    iput-object v0, p0, LX/Jkv;->c:Ljava/util/Comparator;

    .line 2730634
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2730635
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2730636
    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2730637
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2730638
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/Jkv;->b:LX/0P1;

    .line 2730639
    return-void
.end method
