.class public LX/KAE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/KAC;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/KAF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2779035
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/KAE;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/KAF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2779036
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2779037
    iput-object p1, p0, LX/KAE;->b:LX/0Ot;

    .line 2779038
    return-void
.end method

.method public static a(LX/0QB;)LX/KAE;
    .locals 4

    .prologue
    .line 2779039
    const-class v1, LX/KAE;

    monitor-enter v1

    .line 2779040
    :try_start_0
    sget-object v0, LX/KAE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2779041
    sput-object v2, LX/KAE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2779042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2779044
    new-instance v3, LX/KAE;

    const/16 p0, 0x38b7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/KAE;-><init>(LX/0Ot;)V

    .line 2779045
    move-object v0, v3

    .line 2779046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2779047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/KAE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2779048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2779049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2779050
    iget-object v0, p0, LX/KAE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/KAF;

    const/4 p2, 0x2

    const/4 p0, 0x0

    const/4 v5, 0x3

    .line 2779051
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b2707

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b2709

    invoke-interface {v1, v5, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f0b2708

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    iget-object v3, v0, LX/KAF;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f020b31

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a00a2

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b270a

    invoke-interface {v2, v5, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0e011e

    invoke-static {p1, p0, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    const v3, 0x7f083c46

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b270b

    invoke-interface {v2, v5, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0e0122

    invoke-static {p1, p0, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    const v3, 0x7f083c47

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b270c

    invoke-interface {v2, v5, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/KAR;->c(LX/1De;)LX/KAP;

    move-result-object v3

    .line 2779052
    const v4, 0x492aef32    # 700147.1f

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2779053
    invoke-virtual {v3, v4}, LX/KAP;->a(LX/1dQ;)LX/KAP;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/KAM;->c(LX/1De;)LX/KAK;

    move-result-object v3

    .line 2779054
    const v4, 0x492af4e3

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2779055
    invoke-virtual {v3, v4}, LX/KAK;->a(LX/1dQ;)LX/KAK;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2779056
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2779057
    invoke-static {}, LX/1dS;->b()V

    .line 2779058
    iget v0, p1, LX/1dQ;->b:I

    .line 2779059
    sparse-switch v0, :sswitch_data_0

    .line 2779060
    :goto_0
    return-object v2

    .line 2779061
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2779062
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2779063
    check-cast v1, LX/KAD;

    .line 2779064
    iget-object v3, p0, LX/KAE;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/KAF;

    iget-object v4, v1, LX/KAD;->a:LX/KAO;

    iget-object v5, v1, LX/KAD;->b:Ljava/lang/String;

    .line 2779065
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2779066
    const-string p1, "ref"

    const-string p2, "FEED_NULL_STATE"

    invoke-virtual {v6, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2779067
    iget-object p1, v3, LX/KAF;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->N:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {p1, p2, p0, v6, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2779068
    iget-object v6, v3, LX/KAF;->d:LX/0gh;

    const-string p1, "tap_null_state_button"

    invoke-virtual {v6, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2779069
    const-string v6, "empty_feed_find_groups_components"

    const-string p1, "create_group"

    invoke-virtual {v4, v5, v6, p1}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2779070
    goto :goto_0

    .line 2779071
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2779072
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2779073
    check-cast v1, LX/KAD;

    .line 2779074
    iget-object v3, p0, LX/KAE;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/KAF;

    iget-object v4, v1, LX/KAD;->a:LX/KAO;

    iget-object v5, v1, LX/KAD;->b:Ljava/lang/String;

    .line 2779075
    iget-boolean v6, v3, LX/KAF;->c:Z

    if-eqz v6, :cond_0

    .line 2779076
    iget-object v6, v3, LX/KAF;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->ad:Ljava/lang/String;

    invoke-interface {v6, p0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2779077
    :goto_1
    iget-object v6, v3, LX/KAF;->d:LX/0gh;

    const-string p0, "tap_null_state_button"

    invoke-virtual {v6, p0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2779078
    const-string v6, "empty_feed_find_groups_components"

    const-string p0, "join_groups"

    invoke-virtual {v4, v5, v6, p0}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2779079
    goto :goto_0

    .line 2779080
    :cond_0
    iget-object v6, v3, LX/KAF;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->E:Ljava/lang/String;

    invoke-interface {v6, p0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x492aef32 -> :sswitch_1
        0x492af4e3 -> :sswitch_0
    .end sparse-switch
.end method
