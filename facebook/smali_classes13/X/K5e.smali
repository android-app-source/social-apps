.class public LX/K5e;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/K7m;

.field private final b:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private c:Lcom/facebook/tarot/cards/BaseTarotCard;

.field private d:LX/3rW;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(LX/K7m;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2770539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770540
    new-instance v0, LX/K5d;

    invoke-direct {v0, p0}, LX/K5d;-><init>(LX/K5e;)V

    iput-object v0, p0, LX/K5e;->b:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 2770541
    iput-object p1, p0, LX/K5e;->a:LX/K7m;

    .line 2770542
    return-void
.end method

.method public static b(LX/0QB;)LX/K5e;
    .locals 2

    .prologue
    .line 2770543
    new-instance v1, LX/K5e;

    invoke-static {p0}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v0

    check-cast v0, LX/K7m;

    invoke-direct {v1, v0}, LX/K5e;-><init>(LX/K7m;)V

    .line 2770544
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/tarot/cards/BaseTarotCard;)V
    .locals 3

    .prologue
    .line 2770545
    if-nez p1, :cond_0

    .line 2770546
    :goto_0
    return-void

    .line 2770547
    :cond_0
    iput-object p1, p0, LX/K5e;->c:Lcom/facebook/tarot/cards/BaseTarotCard;

    .line 2770548
    new-instance v0, LX/3rW;

    iget-object v1, p0, LX/K5e;->c:Lcom/facebook/tarot/cards/BaseTarotCard;

    invoke-virtual {v1}, Lcom/facebook/tarot/cards/BaseTarotCard;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/K5e;->b:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, v1, v2}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/K5e;->d:LX/3rW;

    .line 2770549
    iget-object v0, p0, LX/K5e;->c:Lcom/facebook/tarot/cards/BaseTarotCard;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/BaseTarotCard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/K5e;->e:I

    .line 2770550
    iget v0, p0, LX/K5e;->e:I

    mul-int/lit8 v0, v0, 0x3

    iput v0, p0, LX/K5e;->f:I

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2770551
    iget-object v0, p0, LX/K5e;->d:LX/3rW;

    invoke-virtual {v0, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
