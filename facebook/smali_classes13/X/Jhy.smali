.class public LX/Jhy;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2725621
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2725622
    const v0, 0x7f03036a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2725623
    const v0, 0x7f0d0b29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    iput-object v0, p0, LX/Jhy;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    .line 2725624
    iget-object v0, p0, LX/Jhy;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    const v1, 0x7f020f8d

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setIcon(I)V

    .line 2725625
    iget-object v0, p0, LX/Jhy;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    invoke-virtual {p0}, LX/Jhy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080553

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/Jhy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f080011

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setText(Ljava/lang/CharSequence;)V

    .line 2725626
    return-void
.end method
