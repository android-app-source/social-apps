.class public final LX/JqP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7G5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7G5",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/JqR;


# direct methods
.method public constructor <init>(LX/JqR;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2738908
    iput-object p1, p0, LX/JqP;->b:LX/JqR;

    iput-object p2, p0, LX/JqP;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/7Gc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/7Gc",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2738909
    const-wide/16 v0, -0x1

    .line 2738910
    :try_start_0
    iget-object v2, p0, LX/JqP;->b:LX/JqR;

    iget-object v3, p0, LX/JqP;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/JqR;->a(Lcom/facebook/common/callercontext/CallerContext;)J
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    move-wide v2, v0

    .line 2738911
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    .line 2738912
    new-instance v0, LX/7Gc;

    const/4 v1, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    .line 2738913
    :goto_1
    return-object v0

    :catch_0
    move-wide v2, v0

    goto :goto_0

    :cond_0
    new-instance v0, LX/7Gc;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2738914
    iget-object v0, p0, LX/JqP;->b:LX/JqR;

    iget-object v1, v0, LX/JqR;->j:LX/03V;

    const-string v2, "messages_sync_initial_fetch_error"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Failed to do initial fetch, backing off by "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ms. viewerContextUserId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, LX/JqP;->b:LX/JqR;

    iget-object v0, v0, LX/JqR;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2738915
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2738916
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2738917
    return-void
.end method
