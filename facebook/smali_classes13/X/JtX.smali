.class public LX/JtX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final f:Ljava/lang/Class;

.field private static volatile g:LX/JtX;


# instance fields
.field public a:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/JtU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/JtW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746875
    const-class v0, LX/JtX;

    sput-object v0, LX/JtX;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2746876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746877
    return-void
.end method

.method public static a(LX/0QB;)LX/JtX;
    .locals 7

    .prologue
    .line 2746878
    sget-object v0, LX/JtX;->g:LX/JtX;

    if-nez v0, :cond_1

    .line 2746879
    const-class v1, LX/JtX;

    monitor-enter v1

    .line 2746880
    :try_start_0
    sget-object v0, LX/JtX;->g:LX/JtX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2746881
    if-eqz v2, :cond_0

    .line 2746882
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2746883
    new-instance p0, LX/JtX;

    invoke-direct {p0}, LX/JtX;-><init>()V

    .line 2746884
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/JtU;->a(LX/0QB;)LX/JtU;

    move-result-object v5

    check-cast v5, LX/JtU;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    .line 2746885
    iput-object v3, p0, LX/JtX;->a:LX/0SG;

    iput-object v4, p0, LX/JtX;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, p0, LX/JtX;->c:LX/JtU;

    iput-object v6, p0, LX/JtX;->d:Ljava/util/concurrent/ExecutorService;

    .line 2746886
    move-object v0, p0

    .line 2746887
    sput-object v0, LX/JtX;->g:LX/JtX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2746888
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2746889
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2746890
    :cond_1
    sget-object v0, LX/JtX;->g:LX/JtX;

    return-object v0

    .line 2746891
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2746892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/JtX;J)J
    .locals 3

    .prologue
    .line 2746893
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 2746894
    iget-object v0, p0, LX/JtX;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/JtV;->d:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2746895
    return-wide p1
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2746896
    iget-object v0, p0, LX/JtX;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2746897
    iget-object v0, p0, LX/JtX;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/JtV;->d:LX/0Tn;

    invoke-interface {v0, v1, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 2746898
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 2746899
    cmp-long v4, v0, v6

    if-eqz v4, :cond_0

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 2746900
    :cond_0
    const-wide/16 v0, 0x2710

    add-long/2addr v0, v2

    invoke-static {p0, v0, v1}, LX/JtX;->a$redex0(LX/JtX;J)J

    move-result-wide v0

    .line 2746901
    :cond_1
    new-instance v4, LX/JtW;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, p0, v5}, LX/JtW;-><init>(LX/JtX;Landroid/os/Looper;)V

    iput-object v4, p0, LX/JtX;->e:LX/JtW;

    .line 2746902
    iget-object v4, p0, LX/JtX;->e:LX/JtW;

    const/4 v5, 0x1

    sub-long/2addr v0, v2

    invoke-virtual {v4, v5, v0, v1}, LX/JtW;->sendEmptyMessageDelayed(IJ)Z

    .line 2746903
    return-void
.end method
