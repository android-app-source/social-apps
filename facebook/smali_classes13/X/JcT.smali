.class public LX/JcT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Mk;

.field private final e:LX/2s9;

.field public final f:LX/0yD;


# direct methods
.method public constructor <init>(LX/0dC;LX/0Or;LX/0Or;LX/1Mk;LX/2s9;LX/0yD;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/location/foreground/IsReportCellTowerLocationEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/location/foreground/IsReportWifiLocationEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1Mk;",
            "LX/2s9;",
            "LX/0yD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717877
    iput-object p1, p0, LX/JcT;->a:LX/0dC;

    .line 2717878
    iput-object p2, p0, LX/JcT;->b:LX/0Or;

    .line 2717879
    iput-object p3, p0, LX/JcT;->c:LX/0Or;

    .line 2717880
    iput-object p4, p0, LX/JcT;->d:LX/1Mk;

    .line 2717881
    iput-object p5, p0, LX/JcT;->e:LX/2s9;

    .line 2717882
    iput-object p6, p0, LX/JcT;->f:LX/0yD;

    .line 2717883
    return-void
.end method

.method private b(Lcom/facebook/location/ImmutableLocation;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/location/ImmutableLocation;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2717884
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2717885
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "coords"

    .line 2717886
    new-instance v5, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2717887
    const-string v4, "latitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v6

    invoke-virtual {v5, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2717888
    const-string v4, "longitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-virtual {v5, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2717889
    const-string v6, "accuracy"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    .line 2717890
    const-string v4, "age_ms"

    iget-object v6, p0, LX/JcT;->f:LX/0yD;

    invoke-virtual {v6, p1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v6

    invoke-virtual {v5, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2717891
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->d()LX/0am;

    move-result-object v4

    .line 2717892
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2717893
    const-string v6, "altitude_meters"

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Double;)LX/0m9;

    .line 2717894
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->e()LX/0am;

    move-result-object v4

    .line 2717895
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2717896
    const-string v6, "bearing_degrees"

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    .line 2717897
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v4

    .line 2717898
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2717899
    const-string v6, "speed_meters_per_sec"

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    .line 2717900
    :cond_2
    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2717901
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2717902
    iget-object v1, p0, LX/JcT;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_3

    .line 2717903
    iget-object v1, p0, LX/JcT;->d:LX/1Mk;

    .line 2717904
    invoke-virtual {v1}, LX/1Mk;->b()Ljava/util/Map;

    move-result-object v3

    .line 2717905
    if-nez v3, :cond_6

    .line 2717906
    const/4 v2, 0x0

    .line 2717907
    :goto_0
    move-object v1, v2

    .line 2717908
    if-eqz v1, :cond_3

    .line 2717909
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "cell_tower"

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2717910
    :cond_3
    iget-object v1, p0, LX/JcT;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_5

    .line 2717911
    iget-object v1, p0, LX/JcT;->e:LX/2s9;

    const/4 v3, 0x0

    .line 2717912
    iget-object v2, v1, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    if-nez v2, :cond_7

    move-object v2, v3

    .line 2717913
    :cond_4
    :goto_1
    move-object v1, v2

    .line 2717914
    if-eqz v1, :cond_5

    .line 2717915
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "wifi"

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2717916
    :cond_5
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "marauder_device_id"

    iget-object v3, p0, LX/JcT;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2717917
    return-object v0

    .line 2717918
    :cond_6
    new-instance v2, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2717919
    invoke-static {v1, v3, v2}, LX/1Mk;->a(LX/1Mk;Ljava/util/Map;LX/0m9;)V

    goto :goto_0

    .line 2717920
    :cond_7
    invoke-static {v1}, LX/2s9;->b(LX/2s9;)LX/0lF;

    move-result-object v2

    check-cast v2, LX/0m9;

    .line 2717921
    if-nez v2, :cond_8

    move-object v2, v3

    .line 2717922
    goto :goto_1

    .line 2717923
    :cond_8
    iget-object v3, v1, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    .line 2717924
    if-eqz v3, :cond_9

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2717925
    :cond_9
    const/4 v4, 0x0

    .line 2717926
    :goto_2
    move-object v3, v4

    .line 2717927
    if-eqz v3, :cond_4

    .line 2717928
    const-string v4, "access_points"

    invoke-virtual {v2, v4, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    .line 2717929
    :cond_a
    new-instance v5, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/162;-><init>(LX/0mC;)V

    .line 2717930
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/ScanResult;

    .line 2717931
    new-instance v7, LX/0m9;

    sget-object p1, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, p1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2717932
    const-string p1, "mac_address"

    iget-object v1, v4, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v7, p1, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2717933
    const-string p1, "ssid"

    iget-object v1, v4, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v7, p1, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2717934
    const-string p1, "signal_strength"

    iget v1, v4, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v7, p1, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2717935
    const-string p1, "frequency"

    iget v1, v4, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-virtual {v7, p1, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2717936
    const-string p1, "capabilities"

    iget-object v4, v4, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    invoke-virtual {v7, p1, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2717937
    invoke-virtual {v5, v7}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_3

    :cond_b
    move-object v4, v5

    .line 2717938
    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 2717859
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2717860
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "update-foreground-location"

    .line 2717861
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2717862
    move-object v0, v0

    .line 2717863
    const-string v1, "POST"

    .line 2717864
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2717865
    move-object v0, v0

    .line 2717866
    const-string v1, "method/places.setLastLocation"

    .line 2717867
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2717868
    move-object v0, v0

    .line 2717869
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-direct {p0, p1}, LX/JcT;->b(Lcom/facebook/location/ImmutableLocation;)Ljava/util/List;

    move-result-object v1

    .line 2717870
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2717871
    move-object v0, v0

    .line 2717872
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2717873
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2717874
    move-object v0, v0

    .line 2717875
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2717857
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2717858
    const/4 v0, 0x0

    return-object v0
.end method
