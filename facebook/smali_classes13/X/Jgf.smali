.class public final LX/Jgf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Jgj;

.field public final synthetic b:Landroid/preference/Preference;

.field public final synthetic c:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;LX/Jgj;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 2723586
    iput-object p1, p0, LX/Jgf;->c:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iput-object p2, p0, LX/Jgf;->a:LX/Jgj;

    iput-object p3, p0, LX/Jgf;->b:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2723587
    iget-object v0, p0, LX/Jgf;->c:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v0, v0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    iget-object v1, p0, LX/Jgf;->a:LX/Jgj;

    .line 2723588
    :try_start_0
    invoke-virtual {v0}, LX/Jgk;->c()Ljava/lang/String;

    move-result-object v2

    .line 2723589
    invoke-virtual {v0}, LX/Jgk;->d()LX/Jgj;

    move-result-object v3

    iget v3, v3, LX/Jgj;->modeValue:I

    .line 2723590
    iget v4, v1, LX/Jgj;->modeValue:I

    .line 2723591
    iget-object v5, v0, LX/Jgk;->a:LX/3QX;

    sget-object v6, LX/6hP;->OMNI_M_SUGGESTION_MODE_PREF:LX/6hP;

    invoke-virtual {v5, v6, v4}, LX/3QX;->a(LX/6hP;I)V

    .line 2723592
    iget-object v5, v0, LX/Jgk;->c:LX/Ddc;

    .line 2723593
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2723594
    const-string p1, "previous_mode"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2723595
    const-string p1, "next_mode"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2723596
    const-string p1, "omni_m_setting_mode_changed"

    invoke-static {v5, p1, v6, v2}, LX/Ddc;->a(LX/Ddc;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2723597
    const/4 v2, 0x1

    .line 2723598
    :goto_0
    move v0, v2

    .line 2723599
    if-eqz v0, :cond_0

    .line 2723600
    iget-object v0, p0, LX/Jgf;->c:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2723601
    iget-object v1, p0, LX/Jgf;->c:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v2, p0, LX/Jgf;->a:LX/Jgj;

    iget v2, v2, LX/Jgj;->optionStringId:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080578

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2723602
    iget-object v1, p0, LX/Jgf;->b:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2723603
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 2723604
    :catch_0
    move-exception v2

    .line 2723605
    iget-object v3, v0, LX/Jgk;->b:LX/03V;

    const-string v4, "OmniMSuggestionSettingsHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error update the suggestion mode: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2723606
    const/4 v2, 0x0

    goto :goto_0
.end method
