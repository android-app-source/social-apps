.class public LX/Jv4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/03V;

.field private final e:LX/0TD;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field public g:LX/0Tn;

.field public h:LX/0Tn;

.field public i:LX/0Tn;

.field public j:LX/4ok;

.field private k:Landroid/preference/Preference;

.field public l:Landroid/preference/Preference;

.field public m:LX/4ok;

.field public n:LX/4ok;

.field public o:LX/8DZ;

.field private p:Landroid/preference/PreferenceScreen;

.field public q:Landroid/preference/PreferenceCategory;

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2749743
    const-class v0, LX/Jv4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Jv4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/0TD;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2749744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2749745
    iput-object p1, p0, LX/Jv4;->b:Landroid/content/Context;

    .line 2749746
    iput-object p2, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2749747
    iput-object p3, p0, LX/Jv4;->d:LX/03V;

    .line 2749748
    iput-object p4, p0, LX/Jv4;->e:LX/0TD;

    .line 2749749
    iput-object p5, p0, LX/Jv4;->f:Ljava/util/concurrent/ExecutorService;

    .line 2749750
    return-void
.end method

.method public static a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V
    .locals 3
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749731
    iget-object v0, p0, LX/Jv4;->e:LX/0TD;

    new-instance v1, Lcom/facebook/oxygen/preloads/integration/appupdates/AppUpdateSettings$8;

    invoke-direct {v1, p0}, Lcom/facebook/oxygen/preloads/integration/appupdates/AppUpdateSettings$8;-><init>(LX/Jv4;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2749732
    new-instance v1, LX/Jv3;

    invoke-direct {v1, p0, p1, p2, p3}, LX/Jv3;-><init>(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    iget-object v2, p0, LX/Jv4;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2749733
    return-void
.end method

.method public static a$redex0(LX/Jv4;Z)V
    .locals 3

    .prologue
    .line 2749734
    iget-object v0, p0, LX/Jv4;->j:LX/4ok;

    .line 2749735
    iget-object v1, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/Jv4;->g:LX/0Tn;

    invoke-interface {v1, v2, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2749736
    iget-object v1, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749737
    iput-boolean p1, v1, LX/8DZ;->d:Z

    .line 2749738
    iget-object v1, p0, LX/Jv4;->g:LX/0Tn;

    invoke-static {p0, v1, p1, v0}, LX/Jv4;->a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    .line 2749739
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, LX/Jv4;->b$redex0(LX/Jv4;Z)V

    .line 2749740
    iget-object v0, p0, LX/Jv4;->j:LX/4ok;

    invoke-virtual {v0, p1}, LX/4ok;->setChecked(Z)V

    .line 2749741
    return-void

    .line 2749742
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(LX/Jv4;Z)V
    .locals 2

    .prologue
    .line 2749725
    iget-object v0, p0, LX/Jv4;->k:Landroid/preference/Preference;

    if-nez v0, :cond_0

    .line 2749726
    invoke-direct {p0}, LX/Jv4;->c()V

    .line 2749727
    :cond_0
    if-eqz p1, :cond_1

    .line 2749728
    iget-object v0, p0, LX/Jv4;->p:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, LX/Jv4;->k:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2749729
    :goto_0
    return-void

    .line 2749730
    :cond_1
    iget-object v0, p0, LX/Jv4;->p:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, LX/Jv4;->k:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public static b$redex0(LX/Jv4;ZLX/4ok;)V
    .locals 2

    .prologue
    .line 2749720
    iget-object v0, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/Jv4;->h:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2749721
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749722
    iput-boolean p1, v0, LX/8DZ;->e:Z

    .line 2749723
    iget-object v0, p0, LX/Jv4;->h:LX/0Tn;

    invoke-static {p0, v0, p1, p2}, LX/Jv4;->a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    .line 2749724
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2749711
    new-instance v2, Landroid/preference/Preference;

    iget-object v3, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/Jv4;->k:Landroid/preference/Preference;

    .line 2749712
    iget-object v2, p0, LX/Jv4;->k:Landroid/preference/Preference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 2749713
    iget-object v2, p0, LX/Jv4;->k:Landroid/preference/Preference;

    const v3, 0x7f030fdf

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2749714
    iget-object v2, p0, LX/Jv4;->k:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    .line 2749715
    iget-object v2, p0, LX/Jv4;->k:Landroid/preference/Preference;

    const v3, 0x7f083af6

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 2749716
    iget-object v2, p0, LX/Jv4;->k:Landroid/preference/Preference;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOrder(I)V

    .line 2749717
    iget-boolean v2, p0, LX/Jv4;->r:Z

    if-nez v2, :cond_0

    :goto_0
    invoke-static {p0, v0}, LX/Jv4;->b$redex0(LX/Jv4;Z)V

    .line 2749718
    return-void

    :cond_0
    move v0, v1

    .line 2749719
    goto :goto_0
.end method

.method public static c(LX/Jv4;ZLX/4ok;)V
    .locals 2

    .prologue
    .line 2749706
    iget-object v0, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/Jv4;->i:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2749707
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749708
    iput-boolean p1, v0, LX/8DZ;->f:Z

    .line 2749709
    iget-object v0, p0, LX/Jv4;->i:LX/0Tn;

    invoke-static {p0, v0, p1, p2}, LX/Jv4;->a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    .line 2749710
    return-void
.end method


# virtual methods
.method public final a(LX/8DZ;Landroid/preference/PreferenceScreen;LX/0Tn;LX/0Tn;LX/0Tn;)V
    .locals 3
    .param p1    # LX/8DZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749640
    iput-object p1, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749641
    iput-object p2, p0, LX/Jv4;->p:Landroid/preference/PreferenceScreen;

    .line 2749642
    iput-object p3, p0, LX/Jv4;->g:LX/0Tn;

    .line 2749643
    iput-object p4, p0, LX/Jv4;->h:LX/0Tn;

    .line 2749644
    iput-object p5, p0, LX/Jv4;->i:LX/0Tn;

    .line 2749645
    iget-object v0, p0, LX/Jv4;->b:Landroid/content/Context;

    iget-object v1, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageItemInfo;->labelRes:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Jv4;->u:Ljava/lang/String;

    .line 2749646
    if-eqz p1, :cond_2

    .line 2749647
    const/4 p2, 0x0

    const/4 p1, 0x1

    .line 2749648
    iget-object v0, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/Jv4;->g:LX/0Tn;

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Jv4;->r:Z

    .line 2749649
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749650
    iget-boolean v1, v0, LX/8DZ;->d:Z

    move v0, v1

    .line 2749651
    iget-boolean v1, p0, LX/Jv4;->r:Z

    if-eq v0, v1, :cond_0

    .line 2749652
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    iget-boolean v1, p0, LX/Jv4;->r:Z

    .line 2749653
    iput-boolean v1, v0, LX/8DZ;->d:Z

    .line 2749654
    iget-object v0, p0, LX/Jv4;->g:LX/0Tn;

    iget-boolean v1, p0, LX/Jv4;->r:Z

    invoke-static {p0, v0, v1, p2}, LX/Jv4;->a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    .line 2749655
    :cond_0
    iget-object v0, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/Jv4;->h:LX/0Tn;

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Jv4;->s:Z

    .line 2749656
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749657
    iget-boolean v1, v0, LX/8DZ;->e:Z

    move v0, v1

    .line 2749658
    iget-boolean v1, p0, LX/Jv4;->s:Z

    if-eq v0, v1, :cond_1

    .line 2749659
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    iget-boolean v1, p0, LX/Jv4;->s:Z

    .line 2749660
    iput-boolean v1, v0, LX/8DZ;->e:Z

    .line 2749661
    iget-object v0, p0, LX/Jv4;->h:LX/0Tn;

    iget-boolean v1, p0, LX/Jv4;->s:Z

    invoke-static {p0, v0, v1, p2}, LX/Jv4;->a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    .line 2749662
    :cond_1
    iget-object v0, p0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/Jv4;->i:LX/0Tn;

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Jv4;->t:Z

    .line 2749663
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    .line 2749664
    iget-boolean v1, v0, LX/8DZ;->f:Z

    move v0, v1

    .line 2749665
    iget-boolean v1, p0, LX/Jv4;->t:Z

    if-eq v0, v1, :cond_2

    .line 2749666
    iget-object v0, p0, LX/Jv4;->o:LX/8DZ;

    iget-boolean v1, p0, LX/Jv4;->t:Z

    .line 2749667
    iput-boolean v1, v0, LX/8DZ;->f:Z

    .line 2749668
    iget-object v0, p0, LX/Jv4;->i:LX/0Tn;

    iget-boolean v1, p0, LX/Jv4;->t:Z

    invoke-static {p0, v0, v1, p2}, LX/Jv4;->a$redex0(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    .line 2749669
    :cond_2
    iget-object v0, p0, LX/Jv4;->p:Landroid/preference/PreferenceScreen;

    const/4 p5, 0x1

    const/4 p4, 0x0

    .line 2749670
    new-instance v1, LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/4ok;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/Jv4;->j:LX/4ok;

    .line 2749671
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    const p1, 0x7f083af1

    new-array p2, p5, [Ljava/lang/Object;

    iget-object p3, p0, LX/Jv4;->u:Ljava/lang/String;

    aput-object p3, p2, p4

    invoke-virtual {v2, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2749672
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    iget-object v2, p0, LX/Jv4;->g:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 2749673
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    const p1, 0x7f083af2

    new-array p2, p5, [Ljava/lang/Object;

    iget-object p3, p0, LX/Jv4;->u:Ljava/lang/String;

    aput-object p3, p2, p4

    invoke-virtual {v2, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 2749674
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    iget-boolean v2, p0, LX/Jv4;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2749675
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/4ok;->setOrder(I)V

    .line 2749676
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    new-instance v2, LX/Juw;

    invoke-direct {v2, p0}, LX/Juw;-><init>(LX/Jv4;)V

    invoke-virtual {v1, v2}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2749677
    iget-object v1, p0, LX/Jv4;->j:LX/4ok;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2749678
    iget-object v0, p0, LX/Jv4;->p:Landroid/preference/PreferenceScreen;

    .line 2749679
    new-instance v1, Landroid/preference/Preference;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2749680
    const v2, 0x7f030439

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2749681
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 2749682
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOrder(I)V

    .line 2749683
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2749684
    invoke-direct {p0}, LX/Jv4;->c()V

    .line 2749685
    iget-object v0, p0, LX/Jv4;->p:Landroid/preference/PreferenceScreen;

    .line 2749686
    new-instance v1, Landroid/preference/PreferenceCategory;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/Jv4;->q:Landroid/preference/PreferenceCategory;

    .line 2749687
    iget-object v1, p0, LX/Jv4;->q:Landroid/preference/PreferenceCategory;

    const v2, 0x7f083af7

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2749688
    iget-object v1, p0, LX/Jv4;->q:Landroid/preference/PreferenceCategory;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    .line 2749689
    iget-object v1, p0, LX/Jv4;->q:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2749690
    new-instance v1, LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/4ok;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/Jv4;->m:LX/4ok;

    .line 2749691
    iget-object v1, p0, LX/Jv4;->m:LX/4ok;

    iget-object v2, p0, LX/Jv4;->h:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 2749692
    iget-object v1, p0, LX/Jv4;->m:LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    const p1, 0x7f083af8

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    iget-object v0, p0, LX/Jv4;->u:Ljava/lang/String;

    aput-object v0, p2, p3

    invoke-virtual {v2, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2749693
    iget-object v1, p0, LX/Jv4;->m:LX/4ok;

    const v2, 0x7f083af9

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(I)V

    .line 2749694
    iget-object v1, p0, LX/Jv4;->m:LX/4ok;

    iget-boolean v2, p0, LX/Jv4;->s:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2749695
    iget-object v1, p0, LX/Jv4;->m:LX/4ok;

    new-instance v2, LX/Juz;

    invoke-direct {v2, p0}, LX/Juz;-><init>(LX/Jv4;)V

    invoke-virtual {v1, v2}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2749696
    iget-object v1, p0, LX/Jv4;->q:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, LX/Jv4;->m:LX/4ok;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2749697
    const/4 v0, 0x1

    const/4 p4, 0x0

    .line 2749698
    new-instance v1, LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/4ok;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/Jv4;->n:LX/4ok;

    .line 2749699
    iget-object v1, p0, LX/Jv4;->n:LX/4ok;

    iget-object v2, p0, LX/Jv4;->i:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 2749700
    iget-object v1, p0, LX/Jv4;->n:LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    const p1, 0x7f083afa

    new-array p2, v0, [Ljava/lang/Object;

    iget-object p3, p0, LX/Jv4;->u:Ljava/lang/String;

    aput-object p3, p2, p4

    invoke-virtual {v2, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2749701
    iget-object v1, p0, LX/Jv4;->n:LX/4ok;

    iget-object v2, p0, LX/Jv4;->b:Landroid/content/Context;

    const p1, 0x7f083afb

    new-array p2, v0, [Ljava/lang/Object;

    iget-object p3, p0, LX/Jv4;->u:Ljava/lang/String;

    aput-object p3, p2, p4

    invoke-virtual {v2, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 2749702
    iget-object v1, p0, LX/Jv4;->n:LX/4ok;

    iget-boolean v2, p0, LX/Jv4;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2749703
    iget-object v1, p0, LX/Jv4;->n:LX/4ok;

    new-instance v2, LX/Jv2;

    invoke-direct {v2, p0}, LX/Jv2;-><init>(LX/Jv4;)V

    invoke-virtual {v1, v2}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2749704
    iget-object v1, p0, LX/Jv4;->q:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, LX/Jv4;->n:LX/4ok;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2749705
    return-void
.end method
