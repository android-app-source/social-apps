.class public LX/Jzq;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "DialogManagerAndroid"
.end annotation


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 2756831
    const-string v0, "buttonClicked"

    const-string v1, "buttonClicked"

    const-string v2, "dismissed"

    const-string v3, "dismissed"

    const-string v4, "buttonPositive"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "buttonNegative"

    const/4 v7, -0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "buttonNeutral"

    const/4 v9, -0x3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LX/Jzq;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2756829
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2756830
    return-void
.end method

.method public static synthetic a(LX/Jzq;)LX/5pY;
    .locals 1

    .prologue
    .line 2756827
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756828
    return-object v0
.end method

.method public static synthetic b(LX/Jzq;)LX/5pY;
    .locals 1

    .prologue
    .line 2756825
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756826
    return-object v0
.end method

.method private h()LX/Jzp;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2756815
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2756816
    if-nez v0, :cond_0

    .line 2756817
    const/4 v0, 0x0

    .line 2756818
    :goto_0
    return-object v0

    .line 2756819
    :cond_0
    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_1

    .line 2756820
    new-instance v1, LX/Jzp;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/Jzp;-><init>(LX/Jzq;LX/0gc;)V

    move-object v0, v1

    goto :goto_0

    .line 2756821
    :cond_1
    new-instance v1, LX/Jzp;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/Jzp;-><init>(LX/Jzq;Landroid/app/FragmentManager;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2756824
    sget-object v0, LX/Jzq;->a:Ljava/util/Map;

    return-object v0
.end method

.method public final bM_()V
    .locals 2

    .prologue
    .line 2756832
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Jzq;->b:Z

    .line 2756833
    invoke-direct {p0}, LX/Jzq;->h()LX/Jzp;

    move-result-object v0

    .line 2756834
    if-eqz v0, :cond_0

    .line 2756835
    invoke-virtual {v0}, LX/Jzp;->a()V

    .line 2756836
    :goto_0
    return-void

    .line 2756837
    :cond_0
    const-class v0, LX/Jzq;

    const-string v1, "onHostResume called but no FragmentManager found"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 2756822
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Jzq;->b:Z

    .line 2756823
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2756814
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2756811
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756812
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2756813
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2756810
    const-string v0, "DialogManagerAndroid"

    return-object v0
.end method

.method public showAlert(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2756785
    invoke-direct {p0}, LX/Jzq;->h()LX/Jzp;

    move-result-object v1

    .line 2756786
    if-nez v1, :cond_0

    .line 2756787
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Tried to show an alert while not attached to an Activity"

    aput-object v2, v1, v0

    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2756788
    :goto_0
    return-void

    .line 2756789
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2756790
    const-string v3, "title"

    invoke-interface {p1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2756791
    const-string v3, "title"

    const-string v4, "title"

    invoke-interface {p1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756792
    :cond_1
    const-string v3, "message"

    invoke-interface {p1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2756793
    const-string v3, "message"

    const-string v4, "message"

    invoke-interface {p1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756794
    :cond_2
    const-string v3, "buttonPositive"

    invoke-interface {p1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2756795
    const-string v3, "button_positive"

    const-string v4, "buttonPositive"

    invoke-interface {p1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756796
    :cond_3
    const-string v3, "buttonNegative"

    invoke-interface {p1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2756797
    const-string v3, "button_negative"

    const-string v4, "buttonNegative"

    invoke-interface {p1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756798
    :cond_4
    const-string v3, "buttonNeutral"

    invoke-interface {p1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2756799
    const-string v3, "button_neutral"

    const-string v4, "buttonNeutral"

    invoke-interface {p1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756800
    :cond_5
    const-string v3, "items"

    invoke-interface {p1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2756801
    const-string v3, "items"

    invoke-interface {p1, v3}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v3

    .line 2756802
    invoke-interface {v3}, LX/5pC;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/CharSequence;

    .line 2756803
    :goto_1
    invoke-interface {v3}, LX/5pC;->size()I

    move-result v5

    if-ge v0, v5, :cond_6

    .line 2756804
    invoke-interface {v3, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 2756805
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2756806
    :cond_6
    const-string v0, "items"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    .line 2756807
    :cond_7
    const-string v0, "cancelable"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2756808
    const-string v0, "cancelable"

    const-string v3, "cancelable"

    invoke-interface {p1, v3}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2756809
    :cond_8
    iget-boolean v0, p0, LX/Jzq;->b:Z

    invoke-virtual {v1, v0, v2, p3}, LX/Jzp;->a(ZLandroid/os/Bundle;Lcom/facebook/react/bridge/Callback;)V

    goto/16 :goto_0
.end method
