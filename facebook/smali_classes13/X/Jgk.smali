.class public LX/Jgk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3QX;

.field public final b:LX/03V;

.field public final c:LX/Ddc;

.field public final d:LX/0W3;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/3QX;LX/03V;LX/Ddc;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2723750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2723751
    iput-object p1, p0, LX/Jgk;->a:LX/3QX;

    .line 2723752
    iput-object p2, p0, LX/Jgk;->b:LX/03V;

    .line 2723753
    iput-object p3, p0, LX/Jgk;->c:LX/Ddc;

    .line 2723754
    iput-object p4, p0, LX/Jgk;->d:LX/0W3;

    .line 2723755
    iput-object p5, p0, LX/Jgk;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2723756
    return-void
.end method

.method public static b(LX/0QB;)LX/Jgk;
    .locals 6

    .prologue
    .line 2723757
    new-instance v0, LX/Jgk;

    invoke-static {p0}, LX/3QX;->a(LX/0QB;)LX/3QX;

    move-result-object v1

    check-cast v1, LX/3QX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/Ddc;->b(LX/0QB;)LX/Ddc;

    move-result-object v3

    check-cast v3, LX/Ddc;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v0 .. v5}, LX/Jgk;-><init>(LX/3QX;LX/03V;LX/Ddc;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2723758
    return-object v0
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2723759
    invoke-virtual {p0}, LX/Jgk;->d()LX/Jgj;

    move-result-object v0

    invoke-virtual {v0}, LX/Jgj;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Jgj;
    .locals 4

    .prologue
    .line 2723760
    sget-object v0, LX/Jgj;->CONSERVATIVE:LX/Jgj;

    iget v0, v0, LX/Jgj;->modeValue:I

    move v0, v0

    .line 2723761
    iget-object v1, p0, LX/Jgk;->d:LX/0W3;

    sget-wide v2, LX/0X5;->jX:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2723762
    iget-object v1, p0, LX/Jgk;->a:LX/3QX;

    sget-object v2, LX/6hP;->OMNI_M_SUGGESTION_MODE_PREF:LX/6hP;

    invoke-virtual {v1, v2}, LX/3QX;->b(LX/6hP;)LX/0am;

    move-result-object v1

    .line 2723763
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2723764
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2723765
    :cond_0
    invoke-static {v0}, LX/Jgj;->getSuggestionModeByModeValue(I)LX/Jgj;

    move-result-object v0

    return-object v0
.end method
