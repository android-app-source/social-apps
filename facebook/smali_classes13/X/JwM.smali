.class public final LX/JwM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/0bZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JwS;


# direct methods
.method public constructor <init>(LX/JwS;)V
    .locals 0

    .prologue
    .line 2751800
    iput-object p1, p0, LX/JwM;->a:LX/JwS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8

    .prologue
    .line 2751801
    check-cast p1, Ljava/util/List;

    .line 2751802
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bZ;

    .line 2751803
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jwa;

    .line 2751804
    iget-object v2, p0, LX/JwM;->a:LX/JwS;

    .line 2751805
    iget-object v3, v2, LX/JwS;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2751806
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2751807
    :goto_0
    move-object v0, v3

    .line 2751808
    return-object v0

    .line 2751809
    :cond_0
    iget-object v3, v2, LX/JwS;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Tf;

    new-instance v4, LX/JwG;

    invoke-direct {v4, v2, v0}, LX/JwG;-><init>(LX/JwS;LX/0bZ;)V

    iget v5, v1, LX/Jwa;->h:I

    int-to-long v5, v5

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6, v7}, LX/0Tf;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v3

    .line 2751810
    new-instance v4, LX/JwH;

    invoke-direct {v4, v2}, LX/JwH;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0
.end method
