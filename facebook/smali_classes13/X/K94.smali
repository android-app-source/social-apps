.class public LX/K94;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/animation/ValueAnimator;

.field public final b:Landroid/view/View;

.field public c:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2776606
    const-wide/16 v0, 0x1f4

    invoke-direct {p0, p1, v0, v1}, LX/K94;-><init>(Landroid/view/View;J)V

    .line 2776607
    return-void
.end method

.method private constructor <init>(Landroid/view/View;J)V
    .locals 2

    .prologue
    .line 2776608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776609
    iput-object p1, p0, LX/K94;->b:Landroid/view/View;

    .line 2776610
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/K94;->a:Landroid/animation/ValueAnimator;

    .line 2776611
    iget-object v0, p0, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2776612
    const/4 v0, 0x0

    iput v0, p0, LX/K94;->c:I

    .line 2776613
    return-void

    .line 2776614
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
