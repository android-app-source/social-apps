.class public final LX/JeG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JeH;


# direct methods
.method public constructor <init>(LX/JeH;)V
    .locals 0

    .prologue
    .line 2720492
    iput-object p1, p0, LX/JeG;->a:LX/JeH;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2720493
    iget-object v0, p0, LX/JeG;->a:LX/JeH;

    invoke-static {v0}, LX/JeH;->e(LX/JeH;)V

    .line 2720494
    iget-object v0, p0, LX/JeG;->a:LX/JeH;

    iget-object v0, v0, LX/JeH;->c:LX/03V;

    const-string v1, "ManageMessagesFragmentPresenter"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2720495
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2720496
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;

    .line 2720497
    iget-object v0, p0, LX/JeG;->a:LX/JeH;

    invoke-static {v0}, LX/JeH;->e(LX/JeH;)V

    .line 2720498
    iget-object v0, p0, LX/JeG;->a:LX/JeH;

    iget-object v0, v0, LX/JeH;->b:LX/Jdq;

    iget-object v1, p0, LX/JeG;->a:LX/JeH;

    iget-object v1, v1, LX/JeH;->d:Lcom/facebook/user/model/User;

    invoke-virtual {v0, p1, v1}, LX/Jdq;->a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;Lcom/facebook/user/model/User;)V

    .line 2720499
    return-void
.end method
