.class public LX/JW8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/5JV;

.field public final b:LX/1vb;


# direct methods
.method public constructor <init>(LX/5JV;LX/1vb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2702416
    iput-object p1, p0, LX/JW8;->a:LX/5JV;

    .line 2702417
    iput-object p2, p0, LX/JW8;->b:LX/1vb;

    .line 2702418
    return-void
.end method

.method public static a(LX/0QB;)LX/JW8;
    .locals 5

    .prologue
    .line 2702404
    const-class v1, LX/JW8;

    monitor-enter v1

    .line 2702405
    :try_start_0
    sget-object v0, LX/JW8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702406
    sput-object v2, LX/JW8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702407
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702408
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702409
    new-instance p0, LX/JW8;

    invoke-static {v0}, LX/5JV;->a(LX/0QB;)LX/5JV;

    move-result-object v3

    check-cast v3, LX/5JV;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v4

    check-cast v4, LX/1vb;

    invoke-direct {p0, v3, v4}, LX/JW8;-><init>(LX/5JV;LX/1vb;)V

    .line 2702410
    move-object v0, p0

    .line 2702411
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702412
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JW8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702413
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
