.class public final enum LX/K4K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K4K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K4K;

.field public static final enum EDIT_PHOTO:LX/K4K;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2767831
    new-instance v0, LX/K4K;

    const-string v1, "EDIT_PHOTO"

    invoke-direct {v0, v1, v2}, LX/K4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4K;->EDIT_PHOTO:LX/K4K;

    .line 2767832
    const/4 v0, 0x1

    new-array v0, v0, [LX/K4K;

    sget-object v1, LX/K4K;->EDIT_PHOTO:LX/K4K;

    aput-object v1, v0, v2

    sput-object v0, LX/K4K;->$VALUES:[LX/K4K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2767833
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K4K;
    .locals 1

    .prologue
    .line 2767834
    const-class v0, LX/K4K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K4K;

    return-object v0
.end method

.method public static values()[LX/K4K;
    .locals 1

    .prologue
    .line 2767835
    sget-object v0, LX/K4K;->$VALUES:[LX/K4K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K4K;

    return-object v0
.end method
