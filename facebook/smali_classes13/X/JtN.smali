.class public final LX/JtN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/JtT;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JtO;


# direct methods
.method public constructor <init>(LX/JtO;)V
    .locals 0

    .prologue
    .line 2746681
    iput-object p1, p0, LX/JtN;->a:LX/JtO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746682
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "Unable to retrieve assets"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746683
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746684
    check-cast p1, Ljava/util/List;

    .line 2746685
    if-nez p1, :cond_0

    .line 2746686
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "Unable to retrieve assets"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2746687
    :goto_0
    return-void

    .line 2746688
    :cond_0
    iget-object v0, p0, LX/JtN;->a:LX/JtO;

    iget-object v0, v0, LX/JtO;->a:LX/JtU;

    invoke-static {v0, p1}, LX/JtU;->b(LX/JtU;Ljava/util/List;)V

    goto :goto_0
.end method
