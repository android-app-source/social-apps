.class public final LX/K1R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:LX/K1S;


# direct methods
.method public constructor <init>(LX/K1S;)V
    .locals 0

    .prologue
    .line 2762000
    iput-object p1, p0, LX/K1R;->a:LX/K1S;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/K1S;B)V
    .locals 0

    .prologue
    .line 2761999
    invoke-direct {p0, p1}, LX/K1R;-><init>(LX/K1S;)V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 3

    .prologue
    .line 2761996
    iget-object v0, p0, LX/K1R;->a:LX/K1S;

    iget-boolean v0, v0, LX/K1S;->b:Z

    if-nez v0, :cond_0

    .line 2761997
    iget-object v0, p0, LX/K1R;->a:LX/K1S;

    iget-object v0, v0, LX/K1S;->a:LX/5s9;

    new-instance v1, LX/K1P;

    iget-object v2, p0, LX/K1R;->a:LX/K1S;

    invoke-virtual {v2}, LX/K1S;->getId()I

    move-result v2

    invoke-direct {v1, v2, p1}, LX/K1P;-><init>(II)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2761998
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 3

    .prologue
    .line 2761987
    iget-object v0, p0, LX/K1R;->a:LX/K1S;

    iget-object v0, v0, LX/K1S;->a:LX/5s9;

    new-instance v1, LX/K1N;

    iget-object v2, p0, LX/K1R;->a:LX/K1S;

    invoke-virtual {v2}, LX/K1S;->getId()I

    move-result v2

    invoke-direct {v1, v2, p1, p2}, LX/K1N;-><init>(IIF)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2761988
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 2761989
    packed-switch p1, :pswitch_data_0

    .line 2761990
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported pageScrollState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2761991
    :pswitch_0
    const-string v0, "idle"

    .line 2761992
    :goto_0
    iget-object v1, p0, LX/K1R;->a:LX/K1S;

    iget-object v1, v1, LX/K1S;->a:LX/5s9;

    new-instance v2, LX/K1O;

    iget-object v3, p0, LX/K1R;->a:LX/K1S;

    invoke-virtual {v3}, LX/K1S;->getId()I

    move-result v3

    invoke-direct {v2, v3, v0}, LX/K1O;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v2}, LX/5s9;->a(LX/5r0;)V

    .line 2761993
    return-void

    .line 2761994
    :pswitch_1
    const-string v0, "dragging"

    goto :goto_0

    .line 2761995
    :pswitch_2
    const-string v0, "settling"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
