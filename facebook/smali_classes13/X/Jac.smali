.class public final enum LX/Jac;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jac;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jac;

.field public static final enum CREATE:LX/Jac;

.field public static final enum DISCOVER:LX/Jac;

.field public static final enum GROUPS:LX/Jac;

.field public static final enum INBOX:LX/Jac;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2715620
    new-instance v0, LX/Jac;

    const-string v1, "INBOX"

    invoke-direct {v0, v1, v2}, LX/Jac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jac;->INBOX:LX/Jac;

    .line 2715621
    new-instance v0, LX/Jac;

    const-string v1, "GROUPS"

    invoke-direct {v0, v1, v3}, LX/Jac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jac;->GROUPS:LX/Jac;

    .line 2715622
    new-instance v0, LX/Jac;

    const-string v1, "DISCOVER"

    invoke-direct {v0, v1, v4}, LX/Jac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jac;->DISCOVER:LX/Jac;

    .line 2715623
    new-instance v0, LX/Jac;

    const-string v1, "CREATE"

    invoke-direct {v0, v1, v5}, LX/Jac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jac;->CREATE:LX/Jac;

    .line 2715624
    const/4 v0, 0x4

    new-array v0, v0, [LX/Jac;

    sget-object v1, LX/Jac;->INBOX:LX/Jac;

    aput-object v1, v0, v2

    sget-object v1, LX/Jac;->GROUPS:LX/Jac;

    aput-object v1, v0, v3

    sget-object v1, LX/Jac;->DISCOVER:LX/Jac;

    aput-object v1, v0, v4

    sget-object v1, LX/Jac;->CREATE:LX/Jac;

    aput-object v1, v0, v5

    sput-object v0, LX/Jac;->$VALUES:[LX/Jac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2715625
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jac;
    .locals 1

    .prologue
    .line 2715626
    const-class v0, LX/Jac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jac;

    return-object v0
.end method

.method public static values()[LX/Jac;
    .locals 1

    .prologue
    .line 2715627
    sget-object v0, LX/Jac;->$VALUES:[LX/Jac;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jac;

    return-object v0
.end method
