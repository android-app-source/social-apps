.class public final LX/JbK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/64X;


# instance fields
.field public final synthetic a:LX/JbL;

.field public final synthetic b:LX/1ut;

.field public final synthetic c:LX/JbM;


# direct methods
.method public constructor <init>(LX/JbM;LX/JbL;LX/1ut;)V
    .locals 0

    .prologue
    .line 2716678
    iput-object p1, p0, LX/JbK;->c:LX/JbM;

    iput-object p2, p0, LX/JbK;->a:LX/JbL;

    iput-object p3, p0, LX/JbK;->b:LX/1ut;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/64y;LX/655;)V
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 2716679
    iget-object v2, p0, LX/JbK;->a:LX/JbL;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v2, LX/JbL;->b:J

    .line 2716680
    iget-object v2, p2, LX/655;->g:LX/656;

    move-object v4, v2

    .line 2716681
    :try_start_0
    iget v2, p2, LX/655;->c:I

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_2

    iget v2, p2, LX/655;->c:I

    const/16 v3, 0x12c

    if-ge v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2716682
    if-nez v2, :cond_0

    .line 2716683
    iget-object v0, p0, LX/JbK;->c:LX/JbM;

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected HTTP code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/JbK;->b:LX/1ut;

    invoke-static {v0, p1, v1, v2}, LX/JbM;->a(LX/JbM;LX/64y;Ljava/lang/Exception;LX/1ut;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2716684
    :try_start_1
    invoke-virtual {v4}, LX/656;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2716685
    :goto_1
    return-void

    .line 2716686
    :catch_0
    move-exception v0

    .line 2716687
    const-string v1, "OkHttpNetworkFetchProducer"

    const-string v2, "Exception when closing response body"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2716688
    :cond_0
    :try_start_2
    invoke-virtual {v4}, LX/656;->b()J

    move-result-wide v2

    .line 2716689
    cmp-long v5, v2, v0

    if-gez v5, :cond_1

    .line 2716690
    :goto_2
    iget-object v2, p0, LX/JbK;->b:LX/1ut;

    .line 2716691
    invoke-virtual {v4}, LX/656;->d()LX/671;

    move-result-object v3

    invoke-interface {v3}, LX/671;->f()Ljava/io/InputStream;

    move-result-object v3

    move-object v3, v3

    .line 2716692
    long-to-int v0, v0

    invoke-virtual {v2, v3, v0}, LX/1ut;->a(Ljava/io/InputStream;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2716693
    :try_start_3
    invoke-virtual {v4}, LX/656;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 2716694
    :catch_1
    move-exception v0

    .line 2716695
    const-string v1, "OkHttpNetworkFetchProducer"

    const-string v2, "Exception when closing response body"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2716696
    :catch_2
    move-exception v0

    .line 2716697
    :try_start_4
    iget-object v2, p0, LX/JbK;->b:LX/1ut;

    .line 2716698
    iget-object v1, p1, LX/64y;->c:LX/66R;

    .line 2716699
    iget-boolean p1, v1, LX/66R;->d:Z

    move v1, p1

    .line 2716700
    move v1, v1

    .line 2716701
    if-eqz v1, :cond_3

    .line 2716702
    invoke-virtual {v2}, LX/1ut;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2716703
    :goto_3
    :try_start_5
    invoke-virtual {v4}, LX/656;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 2716704
    :catch_3
    move-exception v0

    .line 2716705
    const-string v1, "OkHttpNetworkFetchProducer"

    const-string v2, "Exception when closing response body"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2716706
    :catchall_0
    move-exception v0

    .line 2716707
    :try_start_6
    invoke-virtual {v4}, LX/656;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 2716708
    :goto_4
    throw v0

    .line 2716709
    :catch_4
    move-exception v1

    .line 2716710
    const-string v2, "OkHttpNetworkFetchProducer"

    const-string v3, "Exception when closing response body"

    invoke-static {v2, v3, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_1
    move-wide v0, v2

    goto :goto_2

    :cond_2
    :try_start_7
    const/4 v2, 0x0

    goto :goto_0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2716711
    :cond_3
    invoke-virtual {v2, v0}, LX/1ut;->a(Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method public final a(LX/64y;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 2716712
    iget-object v1, p0, LX/JbK;->b:LX/1ut;

    .line 2716713
    iget-object v0, p1, LX/64y;->c:LX/66R;

    .line 2716714
    iget-boolean p1, v0, LX/66R;->d:Z

    move v0, p1

    .line 2716715
    move v0, v0

    .line 2716716
    if-eqz v0, :cond_0

    .line 2716717
    invoke-virtual {v1}, LX/1ut;->a()V

    .line 2716718
    :goto_0
    return-void

    .line 2716719
    :cond_0
    invoke-virtual {v1, p2}, LX/1ut;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
