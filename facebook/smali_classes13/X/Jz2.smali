.class public LX/Jz2;
.super LX/Jyx;
.source ""


# instance fields
.field private final g:LX/JzS;

.field private final h:I

.field private final i:D

.field private final j:D

.field private k:D


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 2

    .prologue
    .line 2755659
    invoke-direct {p0}, LX/Jyx;-><init>()V

    .line 2755660
    iput-object p2, p0, LX/Jz2;->g:LX/JzS;

    .line 2755661
    const-string v0, "input"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Jz2;->h:I

    .line 2755662
    const-string v0, "min"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz2;->i:D

    .line 2755663
    const-string v0, "max"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz2;->j:D

    .line 2755664
    invoke-direct {p0}, LX/Jz2;->f()D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz2;->k:D

    iput-wide v0, p0, LX/Jz2;->e:D

    .line 2755665
    return-void
.end method

.method private f()D
    .locals 2

    .prologue
    .line 2755666
    iget-object v0, p0, LX/Jz2;->g:LX/JzS;

    iget v1, p0, LX/Jz2;->h:I

    invoke-virtual {v0, v1}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v0

    .line 2755667
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2755668
    :cond_0
    new-instance v0, LX/5p9;

    const-string v1, "Illegal node ID set as an input for Animated.DiffClamp node"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755669
    :cond_1
    check-cast v0, LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->b()D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2755670
    invoke-direct {p0}, LX/Jz2;->f()D

    move-result-wide v0

    .line 2755671
    iget-wide v2, p0, LX/Jz2;->k:D

    sub-double v2, v0, v2

    .line 2755672
    iput-wide v0, p0, LX/Jz2;->k:D

    .line 2755673
    iget-wide v0, p0, LX/Jyx;->e:D

    add-double/2addr v0, v2

    iget-wide v2, p0, LX/Jz2;->i:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v2, p0, LX/Jz2;->j:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz2;->e:D

    .line 2755674
    return-void
.end method
