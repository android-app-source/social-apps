.class public LX/KAx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/google/android/gms/wearable/PutDataRequest;

.field public final b:LX/KAr;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wearable/PutDataRequest;LX/KAr;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/KAx;->a:Lcom/google/android/gms/wearable/PutDataRequest;

    new-instance v0, LX/KAr;

    invoke-direct {v0}, LX/KAr;-><init>()V

    iput-object v0, p0, LX/KAx;->b:LX/KAr;

    if-eqz p2, :cond_0

    iget-object v0, p0, LX/KAx;->b:LX/KAr;

    invoke-virtual {p2}, LX/KAr;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object p0, v0, LX/KAr;->a:Ljava/util/HashMap;

    invoke-virtual {p2, v1}, LX/KAr;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/KAx;
    .locals 3

    new-instance v0, LX/KAx;

    invoke-static {p0}, Lcom/google/android/gms/wearable/PutDataRequest;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wearable/PutDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v1

    move-object v1, v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/KAx;-><init>(Lcom/google/android/gms/wearable/PutDataRequest;LX/KAr;)V

    return-object v0
.end method


# virtual methods
.method public final c()Lcom/google/android/gms/wearable/PutDataRequest;
    .locals 9

    iget-object v0, p0, LX/KAx;->b:LX/KAr;

    new-instance v1, LX/KAe;

    invoke-direct {v1}, LX/KAe;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/TreeSet;

    invoke-virtual {v0}, LX/KAr;->b()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v4}, Ljava/util/TreeSet;->size()I

    move-result v3

    new-array v5, v3, [LX/KAd;

    const/4 v3, 0x0

    invoke-virtual {v4}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/KAr;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    new-instance v8, LX/KAd;

    invoke-direct {v8}, LX/KAd;-><init>()V

    aput-object v8, v5, v4

    aget-object v8, v5, v4

    iput-object v3, v8, LX/KAd;->c:Ljava/lang/String;

    aget-object v3, v5, v4

    invoke-static {v2, v7}, LX/KAa;->a(Ljava/util/List;Ljava/lang/Object;)LX/KAc;

    move-result-object v7

    iput-object v7, v3, LX/KAd;->d:LX/KAc;

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    move-object v3, v5

    iput-object v3, v1, LX/KAe;->c:[LX/KAd;

    new-instance v3, LX/KAZ;

    invoke-direct {v3, v1, v2}, LX/KAZ;-><init>(LX/KAe;Ljava/util/List;)V

    move-object v2, v3

    iget-object v0, p0, LX/KAx;->a:Lcom/google/android/gms/wearable/PutDataRequest;

    iget-object v1, v2, LX/KAZ;->a:LX/KAe;

    invoke-virtual {v1}, LX/4u5;->d()I

    move-result v3

    new-array v3, v3, [B

    const/4 v4, 0x0

    array-length v5, v3

    invoke-static {v1, v3, v4, v5}, LX/4u5;->a(LX/4u5;[BII)V

    move-object v1, v3

    iput-object v1, v0, Lcom/google/android/gms/wearable/PutDataRequest;->f:[B

    iget-object v0, v2, LX/KAZ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v2, LX/KAZ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/Asset;

    if-nez v4, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "asset key cannot be null: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-nez v0, :cond_3

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "asset cannot be null: key="

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string v5, "DataMap"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x21

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "asPutDataRequest: adding asset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v5, p0, LX/KAx;->a:Lcom/google/android/gms/wearable/PutDataRequest;

    invoke-virtual {v5, v4, v0}, Lcom/google/android/gms/wearable/PutDataRequest;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)Lcom/google/android/gms/wearable/PutDataRequest;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, LX/KAx;->a:Lcom/google/android/gms/wearable/PutDataRequest;

    return-object v0
.end method
