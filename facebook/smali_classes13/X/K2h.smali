.class public abstract LX/K2h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/8pw;",
        ">",
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/richdocument/optional/impl/UFIViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/20j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;LX/20j;)V
    .locals 1

    .prologue
    .line 2764275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2764276
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/K2h;->a:Ljava/lang/ref/WeakReference;

    .line 2764277
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/K2h;->b:Ljava/lang/ref/WeakReference;

    .line 2764278
    return-void
.end method

.method private a(LX/8pw;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 2764279
    iget-object v0, p0, LX/K2h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    .line 2764280
    iget-object v1, p0, LX/K2h;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20j;

    .line 2764281
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 2764282
    :cond_0
    :goto_0
    return-void

    .line 2764283
    :cond_1
    iget-object v2, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v2, v2

    .line 2764284
    iget-object v3, v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {p0, v3, v2, v1}, LX/K2h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/20j;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 2764285
    iput-object v1, v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2764286
    const/4 v1, 0x1

    .line 2764287
    iput-boolean v1, v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->z:Z

    .line 2764288
    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a()V

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/20j;)Lcom/facebook/graphql/model/GraphQLFeedback;
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2764289
    check-cast p1, LX/8pw;

    invoke-direct {p0, p1}, LX/K2h;->a(LX/8pw;)V

    return-void
.end method
