.class public LX/JWp;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2703537
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2703538
    const v0, 0x7f030765

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2703539
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2703540
    iget-boolean v0, p0, LX/JWp;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3c00eda4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2703541
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2703542
    const/4 v1, 0x1

    .line 2703543
    iput-boolean v1, p0, LX/JWp;->a:Z

    .line 2703544
    const/16 v1, 0x2d

    const v2, 0x4b964ee8    # 1.97012E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x54b6a3a9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2703545
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2703546
    const/4 v1, 0x0

    .line 2703547
    iput-boolean v1, p0, LX/JWp;->a:Z

    .line 2703548
    const/16 v1, 0x2d

    const v2, 0x40f08d11

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
