.class public final LX/JZ2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V
    .locals 3

    .prologue
    .line 2707786
    iput-object p1, p0, LX/JZ2;->a:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2707787
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    .line 2707788
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "survey:title:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JZ2;->b:Ljava/lang/String;

    .line 2707789
    iput-object p2, p0, LX/JZ2;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 2707790
    return-void

    .line 2707791
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2707792
    const/4 v7, 0x0

    .line 2707793
    iget-object v0, p0, LX/JZ2;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    invoke-static {v0}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v0

    .line 2707794
    iget-object v1, p0, LX/JZ2;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2707795
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2707796
    :cond_0
    new-instance v0, LX/1yT;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v7}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    .line 2707797
    :goto_0
    return-object v0

    .line 2707798
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    .line 2707799
    iget-object v3, p0, LX/JZ2;->a:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->b:LX/1Uf;

    invoke-static {v1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v4

    sget-object v5, LX/1eK;->SUBTITLE:LX/1eK;

    iget-object v6, p0, LX/JZ2;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    invoke-static {v6}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v2, v6}, LX/1Uf;->a(LX/1eE;LX/1eK;Landroid/text/Spannable;LX/0lF;)V

    .line 2707800
    iget-object v3, p0, LX/JZ2;->a:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->b:LX/1Uf;

    invoke-static {v1}, LX/1eD;->d(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1y8;

    move-result-object v1

    invoke-virtual {v3, v1, v2, v0}, LX/1Uf;->a(LX/1y8;Landroid/text/Spannable;LX/0lF;)V

    .line 2707801
    new-instance v0, LX/1yT;

    invoke-direct {v0, v2, v7}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2707802
    iget-object v0, p0, LX/JZ2;->b:Ljava/lang/String;

    return-object v0
.end method
