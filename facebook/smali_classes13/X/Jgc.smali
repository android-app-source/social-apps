.class public final LX/Jgc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;)V
    .locals 0

    .prologue
    .line 2723560
    iput-object p1, p0, LX/Jgc;->a:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2723561
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2723562
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2723563
    iget-object v1, p0, LX/Jgc;->a:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v1, v1, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->d:LX/Ddc;

    iget-object v2, p0, LX/Jgc;->a:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v2, v2, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    invoke-virtual {v2}, LX/Jgk;->c()Ljava/lang/String;

    move-result-object v2

    .line 2723564
    if-eqz v0, :cond_1

    const-string p1, "omni_m_setting_turned_on"

    .line 2723565
    :goto_0
    const/4 p2, 0x0

    invoke-static {v1, p1, p2, v2}, LX/Ddc;->a(LX/Ddc;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    .line 2723566
    iget-object v1, p0, LX/Jgc;->a:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v1, v1, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    .line 2723567
    :try_start_0
    iget-object v2, v1, LX/Jgk;->a:LX/3QX;

    sget-object v3, LX/6hP;->OMNI_M_SUGGESTION_ENABLED_PREF:LX/6hP;

    .line 2723568
    if-eqz v0, :cond_2

    const/4 p0, 0x1

    :goto_1
    invoke-virtual {v2, v3, p0}, LX/3QX;->a(LX/6hP;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2723569
    const/4 v2, 0x1

    .line 2723570
    :goto_2
    move v0, v2

    .line 2723571
    :goto_3
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_3

    .line 2723572
    :cond_1
    const-string p1, "omni_m_setting_turned_off"

    goto :goto_0

    .line 2723573
    :catch_0
    move-exception v2

    .line 2723574
    iget-object v3, v1, LX/Jgk;->b:LX/03V;

    const-string p0, "OmniMSuggestionSettingsHelper"

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "Error update the OmniM enabled pref: "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2723575
    const/4 v2, 0x0

    goto :goto_2

    .line 2723576
    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method
