.class public final enum LX/JgD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JgD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JgD;

.field public static final enum CHECKBOX:LX/JgD;

.field public static final enum SWITCH:LX/JgD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2722538
    new-instance v0, LX/JgD;

    const-string v1, "CHECKBOX"

    invoke-direct {v0, v1, v2}, LX/JgD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JgD;->CHECKBOX:LX/JgD;

    .line 2722539
    new-instance v0, LX/JgD;

    const-string v1, "SWITCH"

    invoke-direct {v0, v1, v3}, LX/JgD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JgD;->SWITCH:LX/JgD;

    .line 2722540
    const/4 v0, 0x2

    new-array v0, v0, [LX/JgD;

    sget-object v1, LX/JgD;->CHECKBOX:LX/JgD;

    aput-object v1, v0, v2

    sget-object v1, LX/JgD;->SWITCH:LX/JgD;

    aput-object v1, v0, v3

    sput-object v0, LX/JgD;->$VALUES:[LX/JgD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2722541
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JgD;
    .locals 1

    .prologue
    .line 2722542
    const-class v0, LX/JgD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JgD;

    return-object v0
.end method

.method public static values()[LX/JgD;
    .locals 1

    .prologue
    .line 2722543
    sget-object v0, LX/JgD;->$VALUES:[LX/JgD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JgD;

    return-object v0
.end method
