.class public final LX/Joq;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2736222
    new-instance v0, LX/0U1;

    const-string v1, "row_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->a:LX/0U1;

    .line 2736223
    new-instance v0, LX/0U1;

    const-string v1, "id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->b:LX/0U1;

    .line 2736224
    new-instance v0, LX/0U1;

    const-string v1, "label"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->c:LX/0U1;

    .line 2736225
    new-instance v0, LX/0U1;

    const-string v1, "icon"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->d:LX/0U1;

    .line 2736226
    new-instance v0, LX/0U1;

    const-string v1, "confidence"

    const-string v2, "FLOAT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->e:LX/0U1;

    .line 2736227
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->f:LX/0U1;

    .line 2736228
    new-instance v0, LX/0U1;

    const-string v1, "message_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->g:LX/0U1;

    .line 2736229
    new-instance v0, LX/0U1;

    const-string v1, "type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->h:LX/0U1;

    .line 2736230
    new-instance v0, LX/0U1;

    const-string v1, "data"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Joq;->i:LX/0U1;

    return-void
.end method
