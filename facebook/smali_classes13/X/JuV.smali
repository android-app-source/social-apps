.class public final LX/JuV;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/PaymentNotification;

.field public final synthetic b:I

.field public final synthetic c:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/notify/PaymentNotification;I)V
    .locals 0

    .prologue
    .line 2748787
    iput-object p1, p0, LX/JuV;->c:LX/3RG;

    iput-object p2, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    iput p3, p0, LX/JuV;->b:I

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 2748788
    iget-object v0, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    .line 2748789
    iget-object v1, v0, Lcom/facebook/messaging/notify/PaymentNotification;->b:LX/DiF;

    sget-object v2, LX/DiF;->REQUEST:LX/DiF;

    if-ne v1, v2, :cond_4

    .line 2748790
    iget v1, v0, Lcom/facebook/messaging/notify/PaymentNotification;->i:I

    const/4 v2, 0x0

    .line 2748791
    sget-object v5, LX/2JT;->a:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v7, v5, v3

    .line 2748792
    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->ordinal()I

    move-result v7

    if-ne v1, v7, :cond_6

    .line 2748793
    const/4 v2, 0x1

    .line 2748794
    :cond_0
    move v1, v2

    .line 2748795
    if-eqz v1, :cond_2

    .line 2748796
    const-string v1, "requests"

    iget-object v2, v0, Lcom/facebook/messaging/notify/PaymentNotification;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/messaging/notify/PaymentNotification;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2748797
    :goto_1
    move-object v0, v1

    .line 2748798
    if-nez v0, :cond_1

    .line 2748799
    :goto_2
    return-void

    .line 2748800
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2748801
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2748802
    const-string v0, "from_notification"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2748803
    iget-object v0, p0, LX/JuV;->c:LX/3RG;

    iget-object v0, v0, LX/3RG;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2748804
    new-instance v1, LX/2HB;

    iget-object v2, p0, LX/JuV;->c:LX/3RG;

    iget-object v2, v2, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/PaymentNotification;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget-object v2, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/PaymentNotification;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget-object v2, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/PaymentNotification;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget v2, p0, LX/JuV;->b:I

    invoke-virtual {v1, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    new-instance v2, LX/3pe;

    invoke-direct {v2}, LX/3pe;-><init>()V

    iget-object v3, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    iget-object v3, v3, Lcom/facebook/messaging/notify/PaymentNotification;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v1

    .line 2748805
    iput-object p1, v1, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2748806
    move-object v1, v1

    .line 2748807
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748808
    move-object v0, v1

    .line 2748809
    invoke-virtual {v0, v4}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 2748810
    iget-object v1, p0, LX/JuV;->c:LX/3RG;

    iget-object v1, v1, LX/3RG;->d:LX/3RK;

    iget-object v2, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/PaymentNotification;->a:Ljava/lang/String;

    const/16 v3, 0x2713

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2748811
    iget-object v0, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    .line 2748812
    iput-boolean v4, v0, Lcom/facebook/messaging/notify/PaymentNotification;->k:Z

    .line 2748813
    iget-object v0, p0, LX/JuV;->a:Lcom/facebook/messaging/notify/PaymentNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    goto :goto_2

    .line 2748814
    :cond_2
    iget-object v1, v0, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2748815
    sget-object v1, LX/3RH;->G:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_1

    .line 2748816
    :cond_3
    sget-object v1, LX/3RH;->B:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/messaging/notify/PaymentNotification;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_1

    .line 2748817
    :cond_4
    iget-object v1, v0, Lcom/facebook/messaging/notify/PaymentNotification;->b:LX/DiF;

    sget-object v2, LX/DiF;->TRANSFER:LX/DiF;

    if-ne v1, v2, :cond_5

    .line 2748818
    const-string v1, "transfers"

    iget-object v2, v0, Lcom/facebook/messaging/notify/PaymentNotification;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/messaging/notify/PaymentNotification;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_1

    .line 2748819
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 2748820
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748821
    invoke-direct {p0, p1}, LX/JuV;->b(Landroid/graphics/Bitmap;)V

    .line 2748822
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2748823
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuV;->b(Landroid/graphics/Bitmap;)V

    .line 2748824
    return-void
.end method
