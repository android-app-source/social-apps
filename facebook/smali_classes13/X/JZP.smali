.class public final LX/JZP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JZQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLTarotDigest;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/Boolean;

.field public final synthetic e:LX/JZQ;


# direct methods
.method public constructor <init>(LX/JZQ;)V
    .locals 1

    .prologue
    .line 2708510
    iput-object p1, p0, LX/JZP;->e:LX/JZQ;

    .line 2708511
    move-object v0, p1

    .line 2708512
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2708513
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2708487
    const-string v0, "TarotDigestSlideshowComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/JZQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2708505
    check-cast p1, LX/JZP;

    .line 2708506
    iget-object v0, p1, LX/JZP;->b:Ljava/util/List;

    iput-object v0, p0, LX/JZP;->b:Ljava/util/List;

    .line 2708507
    iget-object v0, p1, LX/JZP;->c:Ljava/util/List;

    iput-object v0, p0, LX/JZP;->c:Ljava/util/List;

    .line 2708508
    iget-object v0, p1, LX/JZP;->d:Ljava/lang/Boolean;

    iput-object v0, p0, LX/JZP;->d:Ljava/lang/Boolean;

    .line 2708509
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2708494
    if-ne p0, p1, :cond_1

    .line 2708495
    :cond_0
    :goto_0
    return v0

    .line 2708496
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2708497
    goto :goto_0

    .line 2708498
    :cond_3
    check-cast p1, LX/JZP;

    .line 2708499
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2708500
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2708501
    if-eq v2, v3, :cond_0

    .line 2708502
    iget-object v2, p0, LX/JZP;->a:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JZP;->a:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    iget-object v3, p1, LX/JZP;->a:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2708503
    goto :goto_0

    .line 2708504
    :cond_4
    iget-object v2, p1, LX/JZP;->a:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2708488
    const/4 v1, 0x0

    .line 2708489
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/JZP;

    .line 2708490
    iput-object v1, v0, LX/JZP;->b:Ljava/util/List;

    .line 2708491
    iput-object v1, v0, LX/JZP;->c:Ljava/util/List;

    .line 2708492
    iput-object v1, v0, LX/JZP;->d:Ljava/lang/Boolean;

    .line 2708493
    return-object v0
.end method
