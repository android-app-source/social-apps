.class public LX/Jmy;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2733559
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\Q"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/3RH;->aa:Ljava/lang/String;

    const-string v2, "%s"

    const-string v3, "\\E[0-9]+"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Jmy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2733560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733561
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/Jmy;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2733562
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->o:Landroid/net/Uri;

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 2733563
    :goto_0
    invoke-static {v0}, LX/Jmy;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2733564
    :cond_1
    :goto_1
    return-object v0

    .line 2733565
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->o:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2733566
    :cond_3
    if-eqz p0, :cond_5

    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2733567
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    const/4 v3, 0x0

    .line 2733568
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_4
    move-object v2, v3

    .line 2733569
    :goto_2
    move-object v0, v2

    .line 2733570
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_5
    move-object v0, v1

    .line 2733571
    goto :goto_1

    .line 2733572
    :cond_6
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v4, v2

    :goto_3
    if-ge v4, v5, :cond_8

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2733573
    if-eqz v2, :cond_7

    iget-object p0, v2, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    if-eqz p0, :cond_7

    iget-object p0, v2, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/Jmy;->b(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2733574
    iget-object v2, v2, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2733575
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    :cond_8
    move-object v2, v3

    .line 2733576
    goto :goto_2
.end method
