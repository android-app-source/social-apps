.class public final LX/Je7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jdt;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic b:Landroid/app/Dialog;

.field public final synthetic c:LX/Je8;


# direct methods
.method public constructor <init>(LX/Je8;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 2720323
    iput-object p1, p0, LX/Je7;->c:LX/Je8;

    iput-object p2, p0, LX/Je7;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object p3, p0, LX/Je7;->b:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2720324
    iget-object v0, p0, LX/Je7;->c:LX/Je8;

    iget-object v0, v0, LX/Je8;->b:LX/JeB;

    iget-object v0, v0, LX/JeB;->i:LX/297;

    iget-object v1, p0, LX/Je7;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2720325
    if-nez v1, :cond_0

    .line 2720326
    :goto_0
    return-void

    .line 2720327
    :cond_0
    invoke-static {v1}, LX/0db;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v2

    .line 2720328
    iget-object v3, v0, LX/297;->d:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "set"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "notification_settings"

    .line 2720329
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 2720330
    move-object v4, v4

    .line 2720331
    const-string v5, "thread_key"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "value"

    const-string v6, "unmute"

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "ConversationsSettingsView"

    .line 2720332
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2720333
    move-object v4, v4

    .line 2720334
    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2720335
    iget-object v3, v0, LX/297;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-interface {v3, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2720336
    iget-object v0, p0, LX/Je7;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2720337
    return-void
.end method
