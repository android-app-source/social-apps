.class public LX/Jre;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Jre;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2744327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Jre;
    .locals 5

    .prologue
    .line 2744312
    sget-object v0, LX/Jre;->c:LX/Jre;

    if-nez v0, :cond_1

    .line 2744313
    const-class v1, LX/Jre;

    monitor-enter v1

    .line 2744314
    :try_start_0
    sget-object v0, LX/Jre;->c:LX/Jre;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2744315
    if-eqz v2, :cond_0

    .line 2744316
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2744317
    new-instance p0, LX/Jre;

    invoke-direct {p0}, LX/Jre;-><init>()V

    .line 2744318
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    .line 2744319
    iput-object v3, p0, LX/Jre;->a:LX/0ad;

    iput-object v4, p0, LX/Jre;->b:LX/0Uh;

    .line 2744320
    move-object v0, p0

    .line 2744321
    sput-object v0, LX/Jre;->c:LX/Jre;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2744322
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2744323
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2744324
    :cond_1
    sget-object v0, LX/Jre;->c:LX/Jre;

    return-object v0

    .line 2744325
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2744326
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
