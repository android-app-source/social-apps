.class public LX/JbM;
.super LX/1Gj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Gj",
        "<",
        "LX/JbL;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/64w;

.field public b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/64w;)V
    .locals 1

    .prologue
    .line 2716758
    iget-object v0, p1, LX/64w;->a:LX/64i;

    move-object v0, v0

    .line 2716759
    invoke-virtual {v0}, LX/64i;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/JbM;-><init>(LX/64w;Ljava/util/concurrent/Executor;)V

    .line 2716760
    return-void
.end method

.method private constructor <init>(LX/64w;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 2716761
    invoke-direct {p0}, LX/1Gj;-><init>()V

    .line 2716762
    iput-object p1, p0, LX/JbM;->a:LX/64w;

    .line 2716763
    iput-object p2, p0, LX/JbM;->b:Ljava/util/concurrent/Executor;

    .line 2716764
    return-void
.end method

.method public static synthetic a(LX/JbM;LX/64y;Ljava/lang/Exception;LX/1ut;)V
    .locals 0

    .prologue
    .line 2716750
    iget-object p0, p1, LX/64y;->c:LX/66R;

    .line 2716751
    iget-boolean p1, p0, LX/66R;->d:Z

    move p0, p1

    .line 2716752
    move p0, p0

    .line 2716753
    if-eqz p0, :cond_0

    .line 2716754
    invoke-virtual {p3}, LX/1ut;->a()V

    .line 2716755
    :goto_0
    return-void

    .line 2716756
    :cond_0
    invoke-virtual {p3, p2}, LX/1ut;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)LX/1us;
    .locals 1

    .prologue
    .line 2716757
    new-instance v0, LX/JbL;

    invoke-direct {v0, p1, p2}, LX/JbL;-><init>(LX/1cd;LX/1cW;)V

    return-object v0
.end method

.method public final a(LX/1us;I)V
    .locals 2

    .prologue
    .line 2716747
    check-cast p1, LX/JbL;

    .line 2716748
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p1, LX/JbL;->c:J

    .line 2716749
    return-void
.end method

.method public final a(LX/1us;LX/1ut;)V
    .locals 5

    .prologue
    .line 2716729
    check-cast p1, LX/JbL;

    .line 2716730
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p1, LX/JbL;->a:J

    .line 2716731
    invoke-virtual {p1}, LX/1us;->e()Landroid/net/Uri;

    move-result-object v0

    .line 2716732
    new-instance v1, LX/64z;

    invoke-direct {v1}, LX/64z;-><init>()V

    new-instance v2, LX/64V;

    invoke-direct {v2}, LX/64V;-><init>()V

    .line 2716733
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/64V;->b:Z

    .line 2716734
    move-object v2, v2

    .line 2716735
    invoke-virtual {v2}, LX/64V;->d()LX/64W;

    move-result-object v2

    .line 2716736
    invoke-virtual {v2}, LX/64W;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2716737
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v3, "Cache-Control"

    invoke-virtual {v1, v3}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    move-result-object v3

    .line 2716738
    :goto_0
    move-object v1, v3

    .line 2716739
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/64z;->a(Ljava/lang/String;)LX/64z;

    move-result-object v0

    .line 2716740
    const-string v1, "GET"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/64z;->a(Ljava/lang/String;LX/651;)LX/64z;

    move-result-object v1

    move-object v0, v1

    .line 2716741
    invoke-virtual {v0}, LX/64z;->b()LX/650;

    move-result-object v0

    .line 2716742
    iget-object v1, p0, LX/JbM;->a:LX/64w;

    invoke-virtual {v1, v0}, LX/64w;->a(LX/650;)LX/64y;

    move-result-object v1

    .line 2716743
    iget-object v2, p1, LX/1us;->b:LX/1cW;

    move-object v2, v2

    .line 2716744
    new-instance v3, LX/JbJ;

    invoke-direct {v3, p0, v1}, LX/JbJ;-><init>(LX/JbM;LX/64y;)V

    invoke-virtual {v2, v3}, LX/1cW;->a(LX/1cg;)V

    .line 2716745
    new-instance v2, LX/JbK;

    invoke-direct {v2, p0, p1, p2}, LX/JbK;-><init>(LX/JbM;LX/JbL;LX/1ut;)V

    invoke-virtual {v1, v2}, LX/64y;->a(LX/64X;)V

    .line 2716746
    return-void

    :cond_0
    const-string v4, "Cache-Control"

    invoke-virtual {v1, v4, v3}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v3

    goto :goto_0
.end method

.method public final b(LX/1us;I)Ljava/util/Map;
    .locals 6

    .prologue
    .line 2716722
    check-cast p1, LX/JbL;

    .line 2716723
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 2716724
    const-string v1, "queue_time"

    iget-wide v2, p1, LX/JbL;->b:J

    iget-wide v4, p1, LX/JbL;->a:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2716725
    const-string v1, "fetch_time"

    iget-wide v2, p1, LX/JbL;->c:J

    iget-wide v4, p1, LX/JbL;->b:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2716726
    const-string v1, "total_time"

    iget-wide v2, p1, LX/JbL;->c:J

    iget-wide v4, p1, LX/JbL;->a:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2716727
    const-string v1, "image_size"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2716728
    return-object v0
.end method
