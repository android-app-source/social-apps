.class public final LX/JXC;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

.field public final synthetic d:Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;LX/0Px;ILcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V
    .locals 0

    .prologue
    .line 2704119
    iput-object p1, p0, LX/JXC;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    iput-object p2, p0, LX/JXC;->a:LX/0Px;

    iput p3, p0, LX/JXC;->b:I

    iput-object p4, p0, LX/JXC;->c:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2704120
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/JXC;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2704121
    iget-object v0, p0, LX/JXC;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    iget-object v2, v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    new-instance v3, LX/JXM;

    iget-object v0, p0, LX/JXC;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    iget v4, p0, LX/JXC;->b:I

    invoke-direct {v3, v0, v4}, LX/JXM;-><init>(Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;I)V

    invoke-virtual {p1, v2, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2704122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2704123
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2704124
    iget-object v0, p0, LX/JXC;->c:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iget-object v1, p0, LX/JXC;->a:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 2704125
    return-void
.end method
