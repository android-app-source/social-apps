.class public LX/JuD;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/notes/view/block/FromBlockView;",
        "Lcom/facebook/notes/model/data/FromAuthorBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;)V
    .locals 0

    .prologue
    .line 2748598
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2748599
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 4

    .prologue
    .line 2748600
    check-cast p1, LX/JuA;

    const/4 v1, 0x0

    .line 2748601
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2748602
    check-cast v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 2748603
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2748604
    check-cast v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;

    .line 2748605
    iget-object v2, p1, LX/JuA;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2748606
    iget-object v3, p1, LX/JuA;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2748607
    if-eqz v3, :cond_0

    .line 2748608
    iget-object v1, p1, LX/JuA;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2748609
    :cond_0
    iget-object v3, v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2748610
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2748611
    iget-object v3, v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2748612
    :goto_0
    return-void

    .line 2748613
    :cond_1
    iget-object v3, v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    sget-object p1, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
