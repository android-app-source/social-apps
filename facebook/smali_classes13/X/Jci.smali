.class public LX/Jci;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/maps/components/PrefetchMapComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Jci",
            "<TE;>.java/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/maps/components/PrefetchMapComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2718262
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2718263
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Jci;->b:LX/0Zi;

    .line 2718264
    iput-object p1, p0, LX/Jci;->a:LX/0Ot;

    .line 2718265
    return-void
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2718256
    invoke-static {}, LX/1dS;->b()V

    .line 2718257
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2718259
    check-cast p4, LX/Jch;

    .line 2718260
    iget-object v0, p0, LX/Jci;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/components/PrefetchMapComponentSpec;

    iget-object v4, p4, LX/Jch;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p4, LX/Jch;->b:LX/1Pt;

    iget-object v6, p4, LX/Jch;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-boolean v7, p4, LX/Jch;->d:Z

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->a(LX/1De;IILcom/facebook/feed/rows/core/props/FeedProps;LX/1Pt;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;Z)LX/1Dg;

    move-result-object v0

    .line 2718261
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2718258
    const/4 v0, 0x1

    return v0
.end method
