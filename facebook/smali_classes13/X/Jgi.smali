.class public final LX/Jgi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/preference/Preference;

.field public final synthetic d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;Ljava/lang/String;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 2723631
    iput-object p1, p0, LX/Jgi;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iput-object p2, p0, LX/Jgi;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;

    iput-object p3, p0, LX/Jgi;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Jgi;->c:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x42f1974b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2723632
    iget-object v1, p0, LX/Jgi;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v1, v1, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    iget-object v2, p0, LX/Jgi;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2723633
    iget-object v3, v1, LX/Jgk;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/Iid;->c:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2723634
    iget-object v1, p0, LX/Jgi;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v2, p0, LX/Jgi;->b:Ljava/lang/String;

    iget-object v3, p0, LX/Jgi;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08057b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2723635
    iget-object v2, p0, LX/Jgi;->c:Landroid/preference/Preference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2723636
    iget-object v1, p0, LX/Jgi;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v1, v1, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->g:LX/3Af;

    invoke-virtual {v1}, LX/3Af;->dismiss()V

    .line 2723637
    const v1, -0x20f20578

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
