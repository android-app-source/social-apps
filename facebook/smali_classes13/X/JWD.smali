.class public LX/JWD;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/JWD;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/189;

.field private final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/2di;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/189;LX/1Ck;LX/0Ot;LX/2di;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/189;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/2di;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702545
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 2702546
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/JWD;->a:Ljava/util/Set;

    .line 2702547
    iput-object p1, p0, LX/JWD;->b:LX/0tX;

    .line 2702548
    iput-object p2, p0, LX/JWD;->c:Ljava/util/concurrent/Executor;

    .line 2702549
    iput-object p3, p0, LX/JWD;->d:LX/189;

    .line 2702550
    iput-object p4, p0, LX/JWD;->e:LX/1Ck;

    .line 2702551
    iput-object p5, p0, LX/JWD;->f:LX/0Ot;

    .line 2702552
    iput-object p6, p0, LX/JWD;->g:LX/2di;

    .line 2702553
    return-void
.end method

.method public static a(LX/0QB;)LX/JWD;
    .locals 10

    .prologue
    .line 2702554
    sget-object v0, LX/JWD;->h:LX/JWD;

    if-nez v0, :cond_1

    .line 2702555
    const-class v1, LX/JWD;

    monitor-enter v1

    .line 2702556
    :try_start_0
    sget-object v0, LX/JWD;->h:LX/JWD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2702557
    if-eqz v2, :cond_0

    .line 2702558
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2702559
    new-instance v3, LX/JWD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v6

    check-cast v6, LX/189;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    const/16 v8, 0x474

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2di;->b(LX/0QB;)LX/2di;

    move-result-object v9

    check-cast v9, LX/2di;

    invoke-direct/range {v3 .. v9}, LX/JWD;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/189;LX/1Ck;LX/0Ot;LX/2di;)V

    .line 2702560
    move-object v0, v3

    .line 2702561
    sput-object v0, LX/JWD;->h:LX/JWD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702562
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2702563
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2702564
    :cond_1
    sget-object v0, LX/JWD;->h:LX/JWD;

    return-object v0

    .line 2702565
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2702566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)Z
    .locals 2

    .prologue
    .line 2702567
    invoke-static {p0}, LX/JWD;->e(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2702568
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V
    .locals 2

    .prologue
    .line 2702569
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    const/4 v1, 0x0

    .line 2702570
    iput-boolean v1, v0, LX/17L;->d:Z

    .line 2702571
    move-object v0, v0

    .line 2702572
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2702573
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v1

    invoke-static {v1}, LX/4Xw;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;)LX/4Xw;

    move-result-object v1

    .line 2702574
    iput-object v0, v1, LX/4Xw;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2702575
    move-object v0, v1

    .line 2702576
    invoke-virtual {v0}, LX/4Xw;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    .line 2702577
    invoke-static {p1}, LX/4Xu;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/4Xu;

    move-result-object v1

    .line 2702578
    iput-object v0, v1, LX/4Xu;->d:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 2702579
    move-object v0, v1

    .line 2702580
    invoke-virtual {v0}, LX/4Xu;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    move-result-object v0

    .line 2702581
    iget-object v1, p0, LX/JWD;->g:LX/2di;

    invoke-virtual {v1, v0}, LX/2di;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 2702582
    return-void
.end method

.method private static e(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 2702583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x5

    .line 2702584
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2702585
    if-ge v1, v0, :cond_0

    const/4 v0, 0x2

    .line 2702586
    :cond_0
    iget-object v2, p0, LX/JWD;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sub-int v0, v1, v0

    if-lt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 2702587
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-static {p1}, LX/JWD;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 2702588
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {p0, p1, p2}, LX/JWD;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V
    .locals 4

    .prologue
    .line 2702589
    invoke-static {p1}, LX/JWD;->e(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2702590
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2702591
    :cond_0
    :goto_0
    return-void

    .line 2702592
    :cond_1
    iget-object v1, p0, LX/JWD;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2702593
    iget-object v1, p0, LX/JWD;->e:LX/1Ck;

    const/4 v2, 0x0

    new-instance v3, LX/JWA;

    invoke-direct {v3, p0, p1, v0}, LX/JWA;-><init>(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    new-instance v0, LX/JWB;

    invoke-direct {v0, p0, p1}, LX/JWB;-><init>(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 2702594
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {p0, p1}, LX/JWD;->b(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V
    .locals 1

    .prologue
    .line 2702595
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I_()I

    move-result v0

    .line 2702596
    invoke-virtual {p0, p1, v0}, LX/JWD;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2702597
    :cond_0
    :goto_0
    return-void

    .line 2702598
    :cond_1
    invoke-static {p1}, LX/JWD;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702599
    invoke-virtual {p0, p1}, LX/JWD;->b(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    goto :goto_0
.end method
