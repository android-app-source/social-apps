.class public LX/Jin;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/Object;


# instance fields
.field public final b:LX/3LN;

.field public final c:LX/3LK;

.field public final d:LX/3LM;

.field public final e:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2726435
    sget-object v0, LX/7Hz;->a:LX/7Hz;

    move-object v0, v0

    .line 2726436
    sput-object v0, LX/Jin;->a:Ljava/util/Comparator;

    .line 2726437
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jin;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3LN;LX/3LK;LX/3LM;LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2726438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2726439
    iput-object p1, p0, LX/Jin;->b:LX/3LN;

    .line 2726440
    iput-object p2, p0, LX/Jin;->c:LX/3LK;

    .line 2726441
    iput-object p3, p0, LX/Jin;->d:LX/3LM;

    .line 2726442
    iput-object p4, p0, LX/Jin;->e:LX/0aG;

    .line 2726443
    return-void
.end method

.method public static a(LX/0QB;)LX/Jin;
    .locals 10

    .prologue
    .line 2726444
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2726445
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2726446
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2726447
    if-nez v1, :cond_0

    .line 2726448
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2726449
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2726450
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2726451
    sget-object v1, LX/Jin;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2726452
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2726453
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2726454
    :cond_1
    if-nez v1, :cond_4

    .line 2726455
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2726456
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2726457
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2726458
    new-instance p0, LX/Jin;

    invoke-static {v0}, LX/3LN;->a(LX/0QB;)LX/3LN;

    move-result-object v1

    check-cast v1, LX/3LN;

    invoke-static {v0}, LX/3LK;->a(LX/0QB;)LX/3LK;

    move-result-object v7

    check-cast v7, LX/3LK;

    invoke-static {v0}, LX/3LM;->b(LX/0QB;)LX/3LM;

    move-result-object v8

    check-cast v8, LX/3LM;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v9

    check-cast v9, LX/0aG;

    invoke-direct {p0, v1, v7, v8, v9}, LX/Jin;-><init>(LX/3LN;LX/3LK;LX/3LM;LX/0aG;)V

    .line 2726459
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2726460
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2726461
    if-nez v1, :cond_2

    .line 2726462
    sget-object v0, LX/Jin;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jin;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2726463
    :goto_1
    if-eqz v0, :cond_3

    .line 2726464
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2726465
    :goto_3
    check-cast v0, LX/Jin;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2726466
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2726467
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2726468
    :catchall_1
    move-exception v0

    .line 2726469
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2726470
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2726471
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2726472
    :cond_2
    :try_start_8
    sget-object v0, LX/Jin;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jin;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2726473
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2726474
    const-string v1, "fetch_nearby_suggestions"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2726475
    iget-object v0, p0, LX/Jin;->b:LX/3LN;

    sget-object v1, LX/DAk;->NEARBY:LX/DAk;

    invoke-virtual {v0, v1}, LX/3LN;->a(LX/DAk;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2726476
    if-eqz v0, :cond_3

    .line 2726477
    :goto_0
    move-object v0, v0

    .line 2726478
    if-nez v0, :cond_2

    .line 2726479
    const/4 v0, 0x0

    .line 2726480
    :goto_1
    move-object v0, v0

    .line 2726481
    if-nez v0, :cond_1

    .line 2726482
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2726483
    :goto_2
    move-object v0, v0

    .line 2726484
    return-object v0

    .line 2726485
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v1, Lcom/facebook/messaging/contacts/picker/service/ContactPickerNearbyResult;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/contacts/picker/service/ContactPickerNearbyResult;-><init>(LX/0Px;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2

    .line 2726486
    :cond_2
    iget-object v1, p0, LX/Jin;->d:LX/3LM;

    invoke-virtual {v1, v0}, LX/3LM;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2726487
    sget-object v1, LX/Jin;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2726488
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 2726489
    :cond_3
    iget-object v0, p0, LX/Jin;->c:LX/3LK;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    invoke-virtual {v0, v1}, LX/3LK;->a(Lcom/facebook/graphql/enums/GraphQLUserChatContextType;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2726490
    if-eqz v0, :cond_4

    .line 2726491
    iget-object v1, p0, LX/Jin;->b:LX/3LN;

    sget-object v2, LX/DAk;->NEARBY:LX/DAk;

    invoke-virtual {v1, v2, v0}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    goto :goto_0

    .line 2726492
    :cond_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2726493
    new-instance v1, Lcom/facebook/contacts/server/FetchChatContextParams;

    invoke-direct {v1}, Lcom/facebook/contacts/server/FetchChatContextParams;-><init>()V

    .line 2726494
    const-string v2, "fetchChatContextParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2726495
    iget-object v1, p0, LX/Jin;->e:LX/0aG;

    const-string v2, "sync_chat_context"

    .line 2726496
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 2726497
    const v4, -0x6f19608c

    invoke-static {v1, v2, v0, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 2726498
    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 2726499
    if-nez v0, :cond_6

    .line 2726500
    const/4 v0, 0x0

    .line 2726501
    :goto_3
    move-object v0, v0

    .line 2726502
    if-eqz v0, :cond_5

    .line 2726503
    iget-object v1, p0, LX/Jin;->b:LX/3LN;

    sget-object v2, LX/DAk;->NEARBY:LX/DAk;

    invoke-virtual {v1, v2, v0}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    goto/16 :goto_0

    .line 2726504
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2726505
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchChatContextResult;

    .line 2726506
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchChatContextResult;->a:LX/0P1;

    move-object v0, v1

    .line 2726507
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2726508
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2726509
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Lx;

    invoke-interface {v1}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    if-ne v1, v4, :cond_7

    .line 2726510
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2726511
    :cond_8
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_3
.end method
