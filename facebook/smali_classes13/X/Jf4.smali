.class public abstract LX/Jf4;
.super Landroid/support/v7/widget/CardView;
.source ""

# interfaces
.implements LX/Jey;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2721463
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/Jf4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721464
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721465
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Jf4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721466
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721467
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721468
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;)V
.end method

.method public abstract setFragmentManager(LX/0gc;)V
.end method

.method public abstract setListener(LX/Jf6;)V
.end method
