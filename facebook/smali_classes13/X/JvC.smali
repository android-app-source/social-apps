.class public final LX/JvC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;)V
    .locals 0

    .prologue
    .line 2749848
    iput-object p1, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2749849
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    sget-object v1, LX/JvD;->NON_CANCELATION_ERROR:LX/JvD;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;LX/JvD;Ljava/lang/String;)V

    .line 2749850
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2749851
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2749852
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2749853
    if-eqz p1, :cond_0

    .line 2749854
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2749855
    if-nez v0, :cond_1

    .line 2749856
    :cond_0
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    sget-object v1, LX/JvD;->INVALID_RESPONSE:LX/JvD;

    const-string v2, "result is null"

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;LX/JvD;Ljava/lang/String;)V

    .line 2749857
    :goto_0
    return-void

    .line 2749858
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2749859
    check-cast v0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;->a()Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;

    move-result-object v9

    .line 2749860
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2749861
    check-cast v0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v10

    .line 2749862
    if-eqz v9, :cond_2

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->l:Ljava/lang/String;

    const-string v1, "home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2749863
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    invoke-virtual {v0}, LX/HBf;->b()V

    .line 2749864
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2749865
    :cond_2
    if-nez v9, :cond_3

    .line 2749866
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    sget-object v1, LX/JvD;->INVALID_RESPONSE:LX/JvD;

    const-string v2, "Requested Page doesn\'t exist."

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;LX/JvD;Ljava/lang/String;)V

    goto :goto_0

    .line 2749867
    :cond_3
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2749868
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    sget-object v1, LX/JvD;->INVALID_RESPONSE:LX/JvD;

    const-string v2, "Requested Page id is null."

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;LX/JvD;Ljava/lang/String;)V

    goto :goto_0

    .line 2749869
    :cond_4
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2749870
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    sget-object v1, LX/JvD;->INVALID_RESPONSE:LX/JvD;

    const-string v2, "Requested Page name is null."

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;LX/JvD;Ljava/lang/String;)V

    goto :goto_0

    .line 2749871
    :cond_5
    if-nez v10, :cond_7

    move v0, v8

    :goto_1
    if-eqz v0, :cond_9

    move v0, v8

    :goto_2
    if-eqz v0, :cond_6

    .line 2749872
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;Ljava/lang/String;Z)V

    .line 2749873
    :cond_6
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    iget-object v1, v0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->c:LX/9XE;

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/89y;->TAB_DEEPLINK:LX/89y;

    sget-object v5, LX/89z;->DEEPLINK:LX/89z;

    const-string v6, "deeplink"

    invoke-virtual/range {v1 .. v6}, LX/9XE;->a(JLX/89y;LX/89z;Ljava/lang/String;)V

    .line 2749874
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    invoke-virtual {v1, v0, v7, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    move v1, v7

    .line 2749875
    :goto_4
    if-ge v1, v2, :cond_e

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9Y7;

    .line 2749876
    invoke-interface {v5}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    if-ne v3, v10, :cond_d

    .line 2749877
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    invoke-virtual {v0}, LX/HBf;->b()V

    .line 2749878
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_c

    .line 2749879
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2749880
    invoke-virtual {v1, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 2749881
    :goto_5
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->n()LX/0Px;

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/9Y7;)V

    goto/16 :goto_0

    .line 2749882
    :cond_7
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2749883
    if-nez v0, :cond_8

    move v0, v8

    goto/16 :goto_1

    :cond_8
    move v0, v7

    goto/16 :goto_1

    .line 2749884
    :cond_9
    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    invoke-virtual {v1, v0, v7, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2749885
    if-eqz v0, :cond_a

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    goto/16 :goto_2

    .line 2749886
    :cond_a
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2749887
    goto :goto_6

    .line 2749888
    :cond_b
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2749889
    goto :goto_3

    .line 2749890
    :cond_c
    const/4 v4, 0x0

    goto :goto_5

    .line 2749891
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2749892
    :cond_e
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    sget-object v1, LX/JvD;->TAB_NOT_SUPPORTED:LX/JvD;

    invoke-virtual {v1}, LX/JvD;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Redirect user to the corresponding Page."

    invoke-virtual {v0, v1, v2}, LX/HBf;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749893
    iget-object v0, p0, LX/JvC;->a:Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-virtual {v9}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
