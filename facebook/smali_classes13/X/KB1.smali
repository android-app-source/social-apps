.class public LX/KB1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/KAl;

.field public static final b:LX/KAg;

.field public static final c:LX/KAt;

.field public static final d:LX/KAw;

.field public static final e:LX/KAi;

.field public static final f:LX/KCD;

.field public static final g:LX/KCA;

.field public static final h:LX/KCS;

.field public static final i:LX/KBp;

.field public static final j:LX/KC6;

.field public static final k:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/KBy;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/KB0;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/KBy;",
            "LX/KB0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, LX/KCZ;

    invoke-direct {v0}, LX/KCZ;-><init>()V

    sput-object v0, LX/KB1;->a:LX/KAl;

    new-instance v0, LX/KCK;

    invoke-direct {v0}, LX/KCK;-><init>()V

    sput-object v0, LX/KB1;->b:LX/KAg;

    new-instance v0, LX/KBd;

    invoke-direct {v0}, LX/KBd;-><init>()V

    sput-object v0, LX/KB1;->c:LX/KAt;

    new-instance v0, LX/KBf;

    invoke-direct {v0}, LX/KBf;-><init>()V

    sput-object v0, LX/KB1;->d:LX/KAw;

    new-instance v0, LX/KCM;

    invoke-direct {v0}, LX/KCM;-><init>()V

    sput-object v0, LX/KB1;->e:LX/KAi;

    new-instance v0, LX/KCE;

    invoke-direct {v0}, LX/KCE;-><init>()V

    sput-object v0, LX/KB1;->f:LX/KCD;

    new-instance v0, LX/KCB;

    invoke-direct {v0}, LX/KCB;-><init>()V

    sput-object v0, LX/KB1;->g:LX/KCA;

    new-instance v0, LX/KCT;

    invoke-direct {v0}, LX/KCT;-><init>()V

    sput-object v0, LX/KB1;->h:LX/KCS;

    new-instance v0, LX/KBq;

    invoke-direct {v0}, LX/KBq;-><init>()V

    sput-object v0, LX/KB1;->i:LX/KBp;

    new-instance v0, LX/KC7;

    invoke-direct {v0}, LX/KC7;-><init>()V

    sput-object v0, LX/KB1;->j:LX/KC6;

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/KB1;->k:LX/2vn;

    new-instance v0, LX/KAy;

    invoke-direct {v0}, LX/KAy;-><init>()V

    sput-object v0, LX/KB1;->m:LX/2vq;

    new-instance v0, LX/2vs;

    const-string v1, "Wearable.API"

    sget-object v2, LX/KB1;->m:LX/2vq;

    sget-object v3, LX/KB1;->k:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/KB1;->l:LX/2vs;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
