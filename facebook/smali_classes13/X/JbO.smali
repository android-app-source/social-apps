.class public LX/JbO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47S;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JbO;


# instance fields
.field public a:LX/JbN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/17d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2716796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2716797
    return-void
.end method

.method public static a(LX/0QB;)LX/JbO;
    .locals 6

    .prologue
    .line 2716798
    sget-object v0, LX/JbO;->d:LX/JbO;

    if-nez v0, :cond_1

    .line 2716799
    const-class v1, LX/JbO;

    monitor-enter v1

    .line 2716800
    :try_start_0
    sget-object v0, LX/JbO;->d:LX/JbO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2716801
    if-eqz v2, :cond_0

    .line 2716802
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2716803
    new-instance v5, LX/JbO;

    invoke-direct {v5}, LX/JbO;-><init>()V

    .line 2716804
    invoke-static {v0}, LX/JbN;->a(LX/0QB;)LX/JbN;

    move-result-object v3

    check-cast v3, LX/JbN;

    const/16 v4, 0x97

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v4

    check-cast v4, LX/17d;

    .line 2716805
    iput-object v3, v5, LX/JbO;->a:LX/JbN;

    iput-object p0, v5, LX/JbO;->b:LX/0Ot;

    iput-object v4, v5, LX/JbO;->c:LX/17d;

    .line 2716806
    move-object v0, v5

    .line 2716807
    sput-object v0, LX/JbO;->d:LX/JbO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2716808
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2716809
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2716810
    :cond_1
    sget-object v0, LX/JbO;->d:LX/JbO;

    return-object v0

    .line 2716811
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2716812
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)V
    .locals 1
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2716813
    iget-object v0, p0, LX/JbO;->b:LX/0Ot;

    invoke-static {p1, p3, v0, p4}, LX/17d;->a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;Ljava/util/Map;)V

    .line 2716814
    iget-object v0, p0, LX/JbO;->a:LX/JbN;

    if-eqz v0, :cond_0

    .line 2716815
    iget-object v0, p0, LX/JbO;->a:LX/JbN;

    invoke-virtual {v0, p3, p4}, LX/JbN;->a(Landroid/content/Intent;Ljava/util/Map;)V

    .line 2716816
    :cond_0
    return-void
.end method
