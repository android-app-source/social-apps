.class public final LX/JcQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/foreground/GeoPixelController;


# direct methods
.method public constructor <init>(Lcom/facebook/location/foreground/GeoPixelController;)V
    .locals 0

    .prologue
    .line 2717844
    iput-object p1, p0, LX/JcQ;->a:Lcom/facebook/location/foreground/GeoPixelController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2717845
    iget-object v0, p0, LX/JcQ;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v0, v0, Lcom/facebook/location/foreground/GeoPixelController;->j:LX/03V;

    sget-object v1, Lcom/facebook/location/foreground/GeoPixelController;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2717846
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2717847
    check-cast p1, Ljava/util/Map;

    .line 2717848
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "geopixel_rtt"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "oxygen_map"

    .line 2717849
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2717850
    move-object v0, v0

    .line 2717851
    iget-object v1, p0, LX/JcQ;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v1, v1, Lcom/facebook/location/foreground/GeoPixelController;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2717852
    return-void
.end method
