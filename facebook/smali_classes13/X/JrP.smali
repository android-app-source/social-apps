.class public LX/JrP;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/2N4;

.field private final c:LX/Jqb;

.field private final d:LX/Jrc;

.field private final e:LX/0SG;

.field private final f:LX/3Le;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743348
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrP;->j:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/FDs;LX/2N4;LX/Jqb;LX/Jrc;LX/0SG;LX/3Le;LX/0Or;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/2N4;",
            "LX/Jqb;",
            "LX/Jrc;",
            "LX/0SG;",
            "LX/3Le;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2743335
    invoke-direct {p0, p8}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2743336
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743337
    iput-object v0, p0, LX/JrP;->h:LX/0Ot;

    .line 2743338
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743339
    iput-object v0, p0, LX/JrP;->i:LX/0Ot;

    .line 2743340
    iput-object p1, p0, LX/JrP;->a:LX/FDs;

    .line 2743341
    iput-object p2, p0, LX/JrP;->b:LX/2N4;

    .line 2743342
    iput-object p3, p0, LX/JrP;->c:LX/Jqb;

    .line 2743343
    iput-object p5, p0, LX/JrP;->e:LX/0SG;

    .line 2743344
    iput-object p4, p0, LX/JrP;->d:LX/Jrc;

    .line 2743345
    iput-object p6, p0, LX/JrP;->f:LX/3Le;

    .line 2743346
    iput-object p7, p0, LX/JrP;->g:LX/0Or;

    .line 2743347
    return-void
.end method

.method public static a(LX/0QB;)LX/JrP;
    .locals 7

    .prologue
    .line 2743308
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2743309
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2743310
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2743311
    if-nez v1, :cond_0

    .line 2743312
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743313
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2743314
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2743315
    sget-object v1, LX/JrP;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2743316
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2743317
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2743318
    :cond_1
    if-nez v1, :cond_4

    .line 2743319
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2743320
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2743321
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrP;->b(LX/0QB;)LX/JrP;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2743322
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2743323
    if-nez v1, :cond_2

    .line 2743324
    sget-object v0, LX/JrP;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrP;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2743325
    :goto_1
    if-eqz v0, :cond_3

    .line 2743326
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743327
    :goto_3
    check-cast v0, LX/JrP;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2743328
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2743329
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2743330
    :catchall_1
    move-exception v0

    .line 2743331
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743332
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2743333
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2743334
    :cond_2
    :try_start_8
    sget-object v0, LX/JrP;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrP;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/JrP;
    .locals 9

    .prologue
    .line 2743349
    new-instance v0, LX/JrP;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v2

    check-cast v2, LX/2N4;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v3

    check-cast v3, LX/Jqb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v4

    check-cast v4, LX/Jrc;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/3Le;->a(LX/0QB;)LX/3Le;

    move-result-object v6

    check-cast v6, LX/3Le;

    const/16 v7, 0x19e

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x35bd

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/JrP;-><init>(LX/FDs;LX/2N4;LX/Jqb;LX/Jrc;LX/0SG;LX/3Le;LX/0Or;LX/0Ot;)V

    .line 2743350
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1c5

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2743351
    iput-object v1, v0, LX/JrP;->h:LX/0Ot;

    iput-object v2, v0, LX/JrP;->i:LX/0Ot;

    .line 2743352
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2743306
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2743307
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 2743272
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2743273
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->E()LX/6kK;

    move-result-object v2

    .line 2743274
    iget-object v0, p0, LX/JrP;->b:LX/2N4;

    iget-object v1, p0, LX/JrP;->d:LX/Jrc;

    iget-object v3, v2, LX/6kK;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v9

    .line 2743275
    iget-object v10, v9, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2743276
    if-nez v10, :cond_0

    move-object v0, v8

    .line 2743277
    :goto_0
    return-object v0

    .line 2743278
    :cond_0
    iget-object v0, v2, LX/6kK;->initiator:LX/6kt;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    move-object v1, v0

    .line 2743279
    :goto_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->newBuilder()LX/6g1;

    move-result-object v0

    iget-object v3, v2, LX/6kK;->callState:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/6g1;->a(Ljava/lang/String;)LX/6g1;

    move-result-object v0

    iget-object v2, v2, LX/6kK;->serverInfoData:Ljava/lang/String;

    .line 2743280
    iput-object v2, v0, LX/6g1;->b:Ljava/lang/String;

    .line 2743281
    move-object v0, v0

    .line 2743282
    iput-object v1, v0, LX/6g1;->c:Ljava/lang/String;

    .line 2743283
    move-object v0, v0

    .line 2743284
    invoke-virtual {v0}, LX/6g1;->d()Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    move-result-object v11

    .line 2743285
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/JrP;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2743286
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2743287
    invoke-virtual {v1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v5, v6

    .line 2743288
    :cond_1
    invoke-virtual {v11}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2743289
    iget-object v0, p0, LX/JrP;->f:LX/3Le;

    iget-object v1, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JrP;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v11}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b()Z

    move-result v4

    invoke-virtual/range {v0 .. v6}, LX/3Le;->a(Ljava/lang/String;JZZZ)V

    .line 2743290
    :goto_2
    iget-wide v6, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    .line 2743291
    iget-object v0, v11, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    sget-object v1, LX/6g2;->AUDIO_GROUP_CALL:LX/6g2;

    if-eq v0, v1, :cond_2

    iget-object v0, v11, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    sget-object v1, LX/6g2;->VIDEO_GROUP_CALL:LX/6g2;

    if-ne v0, v1, :cond_3

    .line 2743292
    :cond_2
    iget-object v0, p0, LX/JrP;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 2743293
    :cond_3
    iget-object v1, p0, LX/JrP;->a:LX/FDs;

    iget-wide v4, v9, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    move-object v2, v10

    move-object v3, v11

    invoke-virtual/range {v1 .. v7}, LX/FDs;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;JJ)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2743294
    if-eqz v0, :cond_4

    .line 2743295
    const-string v1, "rtc_call_info"

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_4
    move-object v0, v8

    .line 2743296
    goto :goto_0

    .line 2743297
    :cond_5
    iget-object v0, v2, LX/6kK;->initiator:LX/6kt;

    iget-object v0, v0, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 2743298
    :cond_6
    iget-object v0, p0, LX/JrP;->f:LX/3Le;

    iget-object v1, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 2743299
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2743300
    sget-object v2, LX/3Le;->a:Ljava/lang/Class;

    const-string v3, "Invalid thread id while trying to insert call log into db!"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2743301
    :goto_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2743302
    sget-object v0, LX/2b2;->l:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2743303
    const-string v0, "thread_key_string"

    iget-object v2, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2743304
    iget-object v0, p0, LX/JrP;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_2

    .line 2743305
    :cond_7
    iget-object v2, v0, LX/3Le;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/rtc/models/RecentCallsDb$2;

    invoke-direct {v3, v0, v1, v6}, Lcom/facebook/rtc/models/RecentCallsDb$2;-><init>(LX/3Le;Ljava/lang/String;Z)V

    const v4, 0x32d4cbb8

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2743266
    const-string v0, "rtc_call_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2743267
    if-eqz v0, :cond_0

    .line 2743268
    iget-object v1, p0, LX/JrP;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/JrP;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2743269
    iget-object v1, p0, LX/JrP;->c:LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2743270
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2743271
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2743262
    check-cast p1, LX/6kW;

    .line 2743263
    invoke-virtual {p1}, LX/6kW;->E()LX/6kK;

    move-result-object v0

    .line 2743264
    iget-object v1, p0, LX/JrP;->d:LX/Jrc;

    iget-object v0, v0, LX/6kK;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2743265
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
