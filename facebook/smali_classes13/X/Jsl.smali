.class public LX/Jsl;
.super LX/6lf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/Jsk;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2745697
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2745698
    return-void
.end method


# virtual methods
.method public final a(LX/6le;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V
    .locals 3

    .prologue
    .line 2745700
    check-cast p1, LX/Jsk;

    .line 2745701
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745702
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745703
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/5Sa;->a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v1

    .line 2745704
    if-nez v1, :cond_0

    .line 2745705
    :goto_0
    return-void

    .line 2745706
    :cond_0
    iget-object v0, p1, LX/6le;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;

    .line 2745707
    iget-object v2, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v2, v1}, LX/Jso;->a(Lcom/facebook/widget/text/BetterTextView;LX/5St;)V

    .line 2745708
    iget-object v2, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v2, v1}, LX/Jso;->b(Lcom/facebook/widget/text/BetterTextView;LX/5St;)V

    .line 2745709
    iget-object v2, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->b:LX/Jso;

    iget-object p0, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->g:LX/4ob;

    invoke-virtual {v2, p0, v1}, LX/Jso;->a(LX/4ob;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2745710
    iget-object p1, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->b:LX/Jso;

    iget-object v2, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->g:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/CustomLinearLayout;

    const p0, 0x7f0d3179

    invoke-virtual {v2, p0}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    iget-object p0, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->g:LX/4ob;

    invoke-virtual {p0}, LX/4ob;->a()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/CustomLinearLayout;

    const p2, 0x7f0d3178

    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/ImageView;

    invoke-virtual {p1, v2, p0, v1}, LX/Jso;->a(Lcom/facebook/widget/text/BetterTextView;Landroid/widget/ImageView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    .line 2745711
    :cond_1
    iget-object v2, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->h:LX/4ob;

    invoke-static {v2, v1}, LX/Jso;->b(LX/4ob;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    .line 2745712
    iget-object v2, v0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p0, v1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    sget-object p1, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2745713
    new-instance v2, LX/Jsm;

    invoke-direct {v2, v0, v1}, LX/Jsm;-><init>(Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2745714
    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2745699
    new-instance v0, LX/Jsk;

    new-instance v1, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/Jsk;-><init>(Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemView;)V

    return-object v0
.end method
