.class public LX/JYS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1LV;

.field public final b:LX/3mL;

.field public final c:LX/JYQ;

.field public final d:LX/JY2;

.field public final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/3mL;LX/1LV;LX/JYQ;LX/JY2;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2706315
    iput-object p1, p0, LX/JYS;->b:LX/3mL;

    .line 2706316
    iput-object p2, p0, LX/JYS;->a:LX/1LV;

    .line 2706317
    iput-object p3, p0, LX/JYS;->c:LX/JYQ;

    .line 2706318
    iput-object p4, p0, LX/JYS;->d:LX/JY2;

    .line 2706319
    iput-object p5, p0, LX/JYS;->e:LX/0ad;

    .line 2706320
    return-void
.end method

.method public static a(LX/JYS;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;I)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
            ">;",
            "LX/1De;",
            "I)",
            "LX/0Px",
            "<",
            "LX/JY4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2706321
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2706322
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 2706323
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k()LX/0Px;

    move-result-object v1

    .line 2706324
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2706325
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 2706326
    new-instance v4, LX/JY4;

    const/4 v5, 0x0

    invoke-direct {v4, v0, v1, v5}, LX/JY4;-><init>(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;I)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2706327
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2706328
    const/4 v1, -0x2

    if-ne p3, v1, :cond_3

    .line 2706329
    iget-object v1, p0, LX/JYS;->e:LX/0ad;

    sget v4, LX/JXx;->b:I

    const/4 v5, -0x1

    invoke-interface {v1, v4, v5}, LX/0ad;->a(II)I

    move-result v1

    .line 2706330
    iget-object v4, p2, LX/1De;->g:LX/1X1;

    move-object v4, v4

    .line 2706331
    if-nez v4, :cond_4

    .line 2706332
    :goto_1
    if-ltz v1, :cond_2

    .line 2706333
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 2706334
    :cond_1
    new-instance v4, LX/JY4;

    const/4 v5, 0x0

    const/4 p1, 0x1

    invoke-direct {v4, v0, v5, p1}, LX/JY4;-><init>(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;I)V

    invoke-interface {v3, v1, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2706335
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2706336
    return-object v0

    :cond_3
    move v1, p3

    goto :goto_1

    .line 2706337
    :cond_4
    check-cast v4, LX/JYM;

    .line 2706338
    new-instance v5, LX/JYN;

    iget-object p1, v4, LX/JYM;->d:LX/JYO;

    invoke-direct {v5, p1, v1}, LX/JYN;-><init>(LX/JYO;I)V

    move-object v4, v5

    .line 2706339
    invoke-virtual {p2, v4}, LX/1De;->a(LX/48B;)V

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/JYS;
    .locals 9

    .prologue
    .line 2706340
    const-class v1, LX/JYS;

    monitor-enter v1

    .line 2706341
    :try_start_0
    sget-object v0, LX/JYS;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706342
    sput-object v2, LX/JYS;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706343
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706344
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706345
    new-instance v3, LX/JYS;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    const-class v6, LX/JYQ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/JYQ;

    invoke-static {v0}, LX/JY2;->a(LX/0QB;)LX/JY2;

    move-result-object v7

    check-cast v7, LX/JY2;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/JYS;-><init>(LX/3mL;LX/1LV;LX/JYQ;LX/JY2;LX/0ad;)V

    .line 2706346
    move-object v0, v3

    .line 2706347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
