.class public final LX/JfF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6e1;


# instance fields
.field public final synthetic a:LX/JfG;


# direct methods
.method public constructor <init>(LX/JfG;)V
    .locals 0

    .prologue
    .line 2721643
    iput-object p1, p0, LX/JfF;->a:LX/JfG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/dialog/MenuDialogItem;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2721644
    instance-of v0, p2, LX/Jew;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2721645
    check-cast p2, LX/Jew;

    .line 2721646
    iget v0, p1, Lcom/facebook/messaging/dialog/MenuDialogItem;->a:I

    move v0, v0

    .line 2721647
    packed-switch v0, :pswitch_data_0

    .line 2721648
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 2721649
    :pswitch_1
    iget-object v0, p0, LX/JfF;->a:LX/JfG;

    iget-object v0, v0, LX/JfG;->c:LX/JfC;

    invoke-interface {v0}, LX/JfC;->a()V

    .line 2721650
    iget-object v0, p0, LX/JfF;->a:LX/JfG;

    iget-object v0, v0, LX/JfG;->b:LX/Jf2;

    invoke-interface {p2}, LX/Jew;->c()Ljava/lang/String;

    move-result-object v1

    .line 2721651
    sget-object p0, LX/Jf0;->AD_HIDE:LX/Jf0;

    invoke-static {p0, v1}, LX/Jf2;->a(LX/Jf0;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-static {v0, p0}, LX/Jf2;->a(LX/Jf2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2721652
    goto :goto_0

    .line 2721653
    :pswitch_2
    iget-object v0, p0, LX/JfF;->a:LX/JfG;

    iget-object v0, v0, LX/JfG;->b:LX/Jf2;

    invoke-interface {p2}, LX/Jew;->c()Ljava/lang/String;

    move-result-object v1

    .line 2721654
    sget-object p0, LX/Jf0;->AD_USEFUL:LX/Jf0;

    invoke-static {p0, v1}, LX/Jf2;->a(LX/Jf0;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-static {v0, p0}, LX/Jf2;->a(LX/Jf2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2721655
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
