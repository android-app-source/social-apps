.class public LX/JaR;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JaR;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715480
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2715481
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2715482
    sget-object v1, LX/0ax;->P:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUPS_CREATE_TAB_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2715483
    return-void
.end method

.method public static a(LX/0QB;)LX/JaR;
    .locals 3

    .prologue
    .line 2715484
    sget-object v0, LX/JaR;->a:LX/JaR;

    if-nez v0, :cond_1

    .line 2715485
    const-class v1, LX/JaR;

    monitor-enter v1

    .line 2715486
    :try_start_0
    sget-object v0, LX/JaR;->a:LX/JaR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2715487
    if-eqz v2, :cond_0

    .line 2715488
    :try_start_1
    new-instance v0, LX/JaR;

    invoke-direct {v0}, LX/JaR;-><init>()V

    .line 2715489
    move-object v0, v0

    .line 2715490
    sput-object v0, LX/JaR;->a:LX/JaR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2715491
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2715492
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2715493
    :cond_1
    sget-object v0, LX/JaR;->a:LX/JaR;

    return-object v0

    .line 2715494
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2715495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
