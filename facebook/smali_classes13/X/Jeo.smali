.class public LX/Jeo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Jeo;


# instance fields
.field public a:LX/2Sx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721040
    return-void
.end method

.method public static a(LX/0QB;)LX/Jeo;
    .locals 7

    .prologue
    .line 2720984
    sget-object v0, LX/Jeo;->e:LX/Jeo;

    if-nez v0, :cond_1

    .line 2720985
    const-class v1, LX/Jeo;

    monitor-enter v1

    .line 2720986
    :try_start_0
    sget-object v0, LX/Jeo;->e:LX/Jeo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2720987
    if-eqz v2, :cond_0

    .line 2720988
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2720989
    new-instance v5, LX/Jeo;

    invoke-direct {v5}, LX/Jeo;-><init>()V

    .line 2720990
    invoke-static {v0}, LX/2Sx;->a(LX/0QB;)LX/2Sx;

    move-result-object v3

    check-cast v3, LX/2Sx;

    const/16 v4, 0x2744

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2720991
    iput-object v3, v5, LX/Jeo;->a:LX/2Sx;

    iput-object v6, v5, LX/Jeo;->b:LX/0Or;

    iput-object v4, v5, LX/Jeo;->c:LX/0W3;

    iput-object p0, v5, LX/Jeo;->d:LX/0Or;

    .line 2720992
    move-object v0, v5

    .line 2720993
    sput-object v0, LX/Jeo;->e:LX/Jeo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2720994
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2720995
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2720996
    :cond_1
    sget-object v0, LX/Jeo;->e:LX/Jeo;

    return-object v0

    .line 2720997
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2720998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 2721031
    new-instance v0, Ljava/io/File;

    const-string v1, "messages_sync.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2721032
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2721033
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x0

    .line 2721034
    :try_start_0
    invoke-direct {p0}, LX/Jeo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 2721035
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2721036
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    return-object v0

    .line 2721037
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721038
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 13

    .prologue
    .line 2721009
    iget-object v0, p0, LX/Jeo;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-static {v0, v1}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object v1

    .line 2721010
    new-instance v2, Ljava/util/HashMap;

    const/4 v0, 0x3

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 2721011
    const-string v3, "sequence_id"

    iget-object v0, p0, LX/Jeo;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    sget-object v4, LX/6cx;->l:LX/2bA;

    invoke-virtual {v0, v4}, LX/48u;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721012
    const-string v0, "SyncConnectionStateManager.isConnectedAndUpToDate"

    iget-object v3, p0, LX/Jeo;->a:LX/2Sx;

    invoke-virtual {v3, v1}, LX/2Sx;->a(LX/7G9;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721013
    const-string v0, "SyncConnectionStateManager.isQueueTemporarilyUnavailable"

    iget-object v3, p0, LX/Jeo;->a:LX/2Sx;

    invoke-virtual {v3, v1}, LX/2Sx;->b(LX/7G9;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721014
    sget-object v5, LX/6ek;->SYNC_SUPPORT_FOLDERS:LX/0Rf;

    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6ek;

    .line 2721015
    const-string v6, "thread_client_time_ms.%s"

    invoke-static {v6, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iget-object v6, p0, LX/Jeo;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6cy;

    invoke-static {v5}, LX/6cx;->a(LX/6ek;)LX/2bA;

    move-result-object v5

    const-wide/16 v9, -0x1

    invoke-virtual {v6, v5, v9, v10}, LX/48u;->a(LX/0To;J)J

    move-result-wide v5

    .line 2721016
    const-wide/16 v11, 0x0

    cmp-long v11, v5, v11

    if-gtz v11, :cond_2

    .line 2721017
    const-string v11, "n/a"

    .line 2721018
    :goto_1
    move-object v5, v11

    .line 2721019
    invoke-interface {v2, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2721020
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2721021
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2721022
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2721023
    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2721024
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2721025
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 2721026
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2721027
    return-object v0

    .line 2721028
    :cond_2
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11}, Landroid/text/format/Time;-><init>()V

    .line 2721029
    invoke-virtual {v11, v5, v6}, Landroid/text/format/Time;->set(J)V

    .line 2721030
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v11

    goto :goto_1
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721005
    invoke-direct {p0, p1}, LX/Jeo;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2721006
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2721007
    const-string v2, "messages_sync.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721008
    return-object v1
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721001
    invoke-direct {p0, p1}, LX/Jeo;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2721002
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2721003
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "messages_sync.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v0, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2721004
    return-object v1
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 2721000
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 2720999
    iget-object v0, p0, LX/Jeo;->c:LX/0W3;

    sget-wide v2, LX/0X5;->bl:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
