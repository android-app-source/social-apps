.class public LX/Jd9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;
.implements LX/4lA;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2Os;


# direct methods
.method public constructor <init>(LX/0Or;LX/2Os;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsAccountSwitchingAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Os;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2718964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2718965
    iput-object p1, p0, LX/Jd9;->a:LX/0Or;

    .line 2718966
    iput-object p2, p0, LX/Jd9;->b:LX/2Os;

    .line 2718967
    return-void
.end method

.method public static b(LX/0QB;)LX/Jd9;
    .locals 3

    .prologue
    .line 2718968
    new-instance v1, LX/Jd9;

    const/16 v0, 0x14db

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v0

    check-cast v0, LX/2Os;

    invoke-direct {v1, v2, v0}, LX/Jd9;-><init>(LX/0Or;LX/2Os;)V

    .line 2718969
    return-object v1
.end method


# virtual methods
.method public final c()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2718970
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2718971
    iget-object v0, p0, LX/Jd9;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2718972
    const-string v2, "isAccountSwitchingAvailable"

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2718973
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2718974
    iget-object v0, p0, LX/Jd9;->b:LX/2Os;

    invoke-virtual {v0}, LX/2Os;->a()Ljava/util/List;

    move-result-object v0

    .line 2718975
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 2718976
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2718977
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718978
    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2718979
    :cond_0
    const-string v0, "savedAccounts"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2718980
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 2718981
    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2718982
    const/4 v0, 0x0

    return-object v0
.end method
