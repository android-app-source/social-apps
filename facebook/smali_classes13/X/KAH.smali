.class public final LX/KAH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/KAI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:LX/KAI;


# direct methods
.method public constructor <init>(LX/KAI;)V
    .locals 1

    .prologue
    .line 2779164
    iput-object p1, p0, LX/KAH;->b:LX/KAI;

    .line 2779165
    move-object v0, p1

    .line 2779166
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2779167
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2779179
    const-string v0, "FeedEndFindGroupsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2779168
    if-ne p0, p1, :cond_1

    .line 2779169
    :cond_0
    :goto_0
    return v0

    .line 2779170
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2779171
    goto :goto_0

    .line 2779172
    :cond_3
    check-cast p1, LX/KAH;

    .line 2779173
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2779174
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2779175
    if-eq v2, v3, :cond_0

    .line 2779176
    iget-object v2, p0, LX/KAH;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/KAH;->a:Ljava/lang/String;

    iget-object v3, p1, LX/KAH;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2779177
    goto :goto_0

    .line 2779178
    :cond_4
    iget-object v2, p1, LX/KAH;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
