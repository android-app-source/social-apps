.class public LX/Jqk;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/Jrc;

.field private final b:LX/FDs;

.field private final c:LX/2N4;

.field private final d:LX/Jqb;

.field private final e:LX/0SG;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740192
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqk;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/Jrc;LX/FDs;LX/2N4;LX/Jqb;LX/0SG;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jrc;",
            "LX/FDs;",
            "LX/2N4;",
            "LX/Jqb;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2740122
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2740123
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740124
    iput-object v0, p0, LX/Jqk;->f:LX/0Ot;

    .line 2740125
    iput-object p2, p0, LX/Jqk;->a:LX/Jrc;

    .line 2740126
    iput-object p3, p0, LX/Jqk;->b:LX/FDs;

    .line 2740127
    iput-object p4, p0, LX/Jqk;->c:LX/2N4;

    .line 2740128
    iput-object p5, p0, LX/Jqk;->d:LX/Jqb;

    .line 2740129
    iput-object p6, p0, LX/Jqk;->e:LX/0SG;

    .line 2740130
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqk;
    .locals 14

    .prologue
    .line 2740161
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740162
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740163
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740164
    if-nez v1, :cond_0

    .line 2740165
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740166
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740167
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740168
    sget-object v1, LX/Jqk;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740169
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740170
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740171
    :cond_1
    if-nez v1, :cond_4

    .line 2740172
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740173
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740174
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2740175
    new-instance v7, LX/Jqk;

    const/16 v8, 0x35bd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v9

    check-cast v9, LX/Jrc;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v10

    check-cast v10, LX/FDs;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v11

    check-cast v11, LX/2N4;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v12

    check-cast v12, LX/Jqb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v7 .. v13}, LX/Jqk;-><init>(LX/0Ot;LX/Jrc;LX/FDs;LX/2N4;LX/Jqb;LX/0SG;)V

    .line 2740176
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2740177
    iput-object v8, v7, LX/Jqk;->f:LX/0Ot;

    .line 2740178
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2740179
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740180
    if-nez v1, :cond_2

    .line 2740181
    sget-object v0, LX/Jqk;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqk;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740182
    :goto_1
    if-eqz v0, :cond_3

    .line 2740183
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740184
    :goto_3
    check-cast v0, LX/Jqk;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740185
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740186
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740187
    :catchall_1
    move-exception v0

    .line 2740188
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740189
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740190
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740191
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqk;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqk;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740159
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740160
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740141
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2740142
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->C()LX/6je;

    move-result-object v0

    .line 2740143
    iget-object v2, p0, LX/Jqk;->c:LX/2N4;

    iget-object v3, p0, LX/Jqk;->a:LX/Jrc;

    iget-object v4, v0, LX/6je;->messageMetadata:LX/6kn;

    iget-object v4, v4, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v3, v4}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    .line 2740144
    iget-object v2, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740145
    if-nez v2, :cond_0

    move-object v0, v1

    .line 2740146
    :goto_0
    return-object v0

    .line 2740147
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2740148
    iget-object v0, v0, LX/6je;->addedAdmins:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kt;

    .line 2740149
    new-instance v5, Lcom/facebook/user/model/UserKey;

    sget-object v6, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v0, v0, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 2740150
    invoke-virtual {v2, v5}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2740151
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2740152
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DeltaAdminAddedToGroupThread came with admins who are not current participants on the thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740153
    :cond_2
    iget-object v0, p0, LX/Jqk;->b:LX/FDs;

    .line 2740154
    iget-object v4, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    const/4 v5, 0x1

    invoke-static {v4, v3, v5}, LX/FDs;->a(Ljava/util/List;Ljava/util/Set;Z)Ljava/util/List;

    move-result-object v4

    iget-object v5, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v4, v5}, LX/FDs;->a(LX/FDs;Ljava/util/List;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2740155
    iget-object v4, v0, LX/FDs;->d:LX/2N4;

    iget-object v5, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v4

    .line 2740156
    if-eqz v0, :cond_3

    .line 2740157
    const-string v2, "added_admin_thread_summary"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    move-object v0, v1

    .line 2740158
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740135
    const-string v0, "added_admin_thread_summary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740136
    if-eqz v0, :cond_0

    .line 2740137
    iget-object v1, p0, LX/Jqk;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/Jqk;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740138
    iget-object v1, p0, LX/Jqk;->d:LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2740139
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2740140
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2740131
    check-cast p1, LX/6kW;

    .line 2740132
    invoke-virtual {p1}, LX/6kW;->C()LX/6je;

    move-result-object v0

    .line 2740133
    iget-object v1, p0, LX/Jqk;->a:LX/Jrc;

    iget-object v0, v0, LX/6je;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2740134
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
