.class public final enum LX/Jpa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jpa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jpa;

.field public static final enum COMBINED_FETCH:LX/Jpa;

.field public static final enum LOCAL_CONTACT_FETCH:LX/Jpa;

.field public static final enum MATCHED_CONTACT_FETCH:LX/Jpa;

.field public static final enum NONE:LX/Jpa;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2737679
    new-instance v0, LX/Jpa;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/Jpa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jpa;->NONE:LX/Jpa;

    .line 2737680
    new-instance v0, LX/Jpa;

    const-string v1, "LOCAL_CONTACT_FETCH"

    invoke-direct {v0, v1, v3}, LX/Jpa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jpa;->LOCAL_CONTACT_FETCH:LX/Jpa;

    .line 2737681
    new-instance v0, LX/Jpa;

    const-string v1, "MATCHED_CONTACT_FETCH"

    invoke-direct {v0, v1, v4}, LX/Jpa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jpa;->MATCHED_CONTACT_FETCH:LX/Jpa;

    .line 2737682
    new-instance v0, LX/Jpa;

    const-string v1, "COMBINED_FETCH"

    invoke-direct {v0, v1, v5}, LX/Jpa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jpa;->COMBINED_FETCH:LX/Jpa;

    .line 2737683
    const/4 v0, 0x4

    new-array v0, v0, [LX/Jpa;

    sget-object v1, LX/Jpa;->NONE:LX/Jpa;

    aput-object v1, v0, v2

    sget-object v1, LX/Jpa;->LOCAL_CONTACT_FETCH:LX/Jpa;

    aput-object v1, v0, v3

    sget-object v1, LX/Jpa;->MATCHED_CONTACT_FETCH:LX/Jpa;

    aput-object v1, v0, v4

    sget-object v1, LX/Jpa;->COMBINED_FETCH:LX/Jpa;

    aput-object v1, v0, v5

    sput-object v0, LX/Jpa;->$VALUES:[LX/Jpa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2737684
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jpa;
    .locals 1

    .prologue
    .line 2737685
    const-class v0, LX/Jpa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jpa;

    return-object v0
.end method

.method public static values()[LX/Jpa;
    .locals 1

    .prologue
    .line 2737686
    sget-object v0, LX/Jpa;->$VALUES:[LX/Jpa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jpa;

    return-object v0
.end method
