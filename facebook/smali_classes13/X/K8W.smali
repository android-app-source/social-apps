.class public final LX/K8W;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2775240
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2775241
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2775242
    :goto_0
    return v1

    .line 2775243
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2775244
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2775245
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2775246
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 2775247
    const-string v6, "is_viewer_subscribed"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2775248
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2775249
    :cond_1
    const-string v6, "logo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2775250
    invoke-static {p0, p1}, LX/K8V;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2775251
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2775252
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2775253
    if-eqz v0, :cond_4

    .line 2775254
    invoke-virtual {p1, v1, v4}, LX/186;->a(IZ)V

    .line 2775255
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2775256
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2775257
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2775258
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2775259
    if-eqz v0, :cond_0

    .line 2775260
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775261
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2775262
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2775263
    if-eqz v0, :cond_1

    .line 2775264
    const-string v1, "logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775265
    invoke-static {p0, v0, p2}, LX/K8V;->a(LX/15i;ILX/0nX;)V

    .line 2775266
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2775267
    return-void
.end method
