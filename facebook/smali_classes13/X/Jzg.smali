.class public LX/Jzg;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "Clipboard"
.end annotation


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2756568
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2756569
    return-void
.end method

.method private h()Landroid/content/ClipboardManager;
    .locals 2

    .prologue
    .line 2756547
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756548
    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, LX/5pY;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2756567
    const-string v0, "Clipboard"

    return-object v0
.end method

.method public getString(LX/5pW;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2756556
    :try_start_0
    invoke-direct {p0}, LX/Jzg;->h()Landroid/content/ClipboardManager;

    move-result-object v0

    .line 2756557
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    .line 2756558
    if-nez v1, :cond_0

    .line 2756559
    const-string v0, ""

    invoke-interface {p1, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2756560
    :goto_0
    return-void

    .line 2756561
    :cond_0
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 2756562
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    .line 2756563
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/5pW;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2756564
    :catch_0
    move-exception v0

    .line 2756565
    invoke-interface {p1, v0}, LX/5pW;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2756566
    :cond_1
    :try_start_1
    const-string v0, ""

    invoke-interface {p1, v0}, LX/5pW;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setString(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2756549
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 2756550
    const/4 v0, 0x0

    invoke-static {v0, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 2756551
    invoke-direct {p0}, LX/Jzg;->h()Landroid/content/ClipboardManager;

    move-result-object v1

    .line 2756552
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 2756553
    :goto_0
    return-void

    .line 2756554
    :cond_0
    invoke-direct {p0}, LX/Jzg;->h()Landroid/content/ClipboardManager;

    move-result-object v0

    .line 2756555
    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
