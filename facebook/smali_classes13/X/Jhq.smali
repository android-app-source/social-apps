.class public final LX/Jhq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/Jhr;


# direct methods
.method public constructor <init>(LX/Jhr;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2725530
    iput-object p1, p0, LX/Jhq;->b:LX/Jhr;

    iput-object p2, p0, LX/Jhq;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x21a2c9dc

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2725531
    iget-object v0, p0, LX/Jhq;->b:LX/Jhr;

    iget-object v0, v0, LX/Jhr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jnd;

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, LX/Jnd;->a(ZLX/Jnc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2725532
    const v0, -0x34cc78f4

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2725533
    :goto_0
    return-void

    .line 2725534
    :cond_0
    iget-object v0, p0, LX/Jhq;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2725535
    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/widget/ListView;

    if-eqz v2, :cond_1

    .line 2725536
    check-cast v0, Landroid/widget/ListView;

    .line 2725537
    iget-object v2, p0, LX/Jhq;->a:Landroid/view/View;

    iget-object v3, p0, LX/Jhq;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, LX/Jhq;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 2725538
    iget-object v0, p0, LX/Jhq;->b:LX/Jhr;

    iget-object v0, v0, LX/Jhr;->h:LX/DAW;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/3OP;->b(Z)V

    .line 2725539
    iget-object v0, p0, LX/Jhq;->b:LX/Jhr;

    invoke-static {v0}, LX/Jhr;->b(LX/Jhr;)V

    .line 2725540
    :cond_1
    const v0, 0x3db41380

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
