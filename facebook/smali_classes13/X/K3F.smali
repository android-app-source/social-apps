.class public final LX/K3F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;)V
    .locals 0

    .prologue
    .line 2765407
    iput-object p1, p0, LX/K3F;->a:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x3d466776

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2765408
    new-instance v0, LX/8AA;

    sget-object v2, LX/8AB;->SLIDESHOW_EDIT:LX/8AB;

    invoke-direct {v0, v2}, LX/8AA;-><init>(LX/8AB;)V

    const/4 v2, 0x3

    const/4 v3, 0x7

    invoke-virtual {v0, v2, v3}, LX/8AA;->b(II)LX/8AA;

    move-result-object v0

    iget-object v2, p0, LX/K3F;->a:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    iget-object v2, v2, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8AA;->a(LX/0Px;)LX/8AA;

    move-result-object v0

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v0, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->m()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    .line 2765409
    iget-object v2, p0, LX/K3F;->a:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    iget-object v2, v2, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/K3F;->a:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    iget-object v3, v3, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->b:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v0, p0, LX/K3F;->a:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2765410
    const v0, 0x5b8f0b40

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
