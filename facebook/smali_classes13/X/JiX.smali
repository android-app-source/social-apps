.class public final LX/JiX;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:LX/JhU;

.field public final synthetic c:LX/Jia;


# direct methods
.method public constructor <init>(LX/Jia;Ljava/util/Set;LX/JhU;)V
    .locals 0

    .prologue
    .line 2726006
    iput-object p1, p0, LX/JiX;->c:LX/Jia;

    iput-object p2, p0, LX/JiX;->a:Ljava/util/Set;

    iput-object p3, p0, LX/JiX;->b:LX/JhU;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 11

    .prologue
    .line 2726007
    iget-object v0, p0, LX/JiX;->c:LX/Jia;

    iget-object v1, v0, LX/Jia;->a:LX/3LB;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    iget-object v2, p0, LX/JiX;->a:Ljava/util/Set;

    iget-object v3, p0, LX/JiX;->b:LX/JhU;

    .line 2726008
    iget-object p0, v3, LX/JhU;->b:LX/0Px;

    move-object v3, p0

    .line 2726009
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result p0

    .line 2726010
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result p1

    .line 2726011
    invoke-static {v3}, LX/3LB;->b(LX/0Px;)LX/FD4;

    move-result-object v10

    .line 2726012
    if-nez v10, :cond_1

    .line 2726013
    :cond_0
    return-void

    :cond_1
    move v6, p0

    .line 2726014
    :goto_0
    add-int/lit8 v4, p1, 0x1

    if-ge v6, v4, :cond_0

    .line 2726015
    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ish;

    .line 2726016
    iget-object v5, v4, LX/Ish;->a:Lcom/facebook/user/model/User;

    move-object v4, v5

    .line 2726017
    if-eqz v4, :cond_0

    .line 2726018
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2726019
    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2726020
    iget-object v4, v1, LX/3LB;->a:LX/3LC;

    sub-int v7, v6, p0

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v8

    sget-object v9, LX/FD3;->SEARCH_NULL_STATE:LX/FD3;

    .line 2726021
    if-nez v10, :cond_4

    .line 2726022
    :cond_2
    :goto_1
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2726023
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2726024
    :cond_4
    iget-object p2, v4, LX/3LC;->a:LX/0Zb;

    iget-object p3, v10, LX/FD4;->impressionLoggingEventName:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {p2, p3, v0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p2

    .line 2726025
    invoke-virtual {p2}, LX/0oG;->a()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 2726026
    const-string p3, "page_id"

    invoke-virtual {p2, p3, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2726027
    const-string p3, "abs_pos"

    invoke-virtual {p2, p3, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2726028
    const-string p3, "rel_pos"

    invoke-virtual {p2, p3, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2726029
    const-string p3, "total"

    invoke-virtual {p2, p3, v8}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2726030
    const-string p3, "product"

    invoke-virtual {v9}, LX/FD3;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2726031
    invoke-virtual {p2}, LX/0oG;->d()V

    goto :goto_1
.end method
