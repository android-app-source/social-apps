.class public final LX/JZg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JZh;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;LX/JZh;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2708892
    iput-object p1, p0, LX/JZg;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    iput-object p2, p0, LX/JZg;->a:LX/JZh;

    iput-object p3, p0, LX/JZg;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x7290b5f4

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2708893
    iget-object v0, p0, LX/JZg;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    iget-object v0, v0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->b:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->cW:Ljava/lang/String;

    sget-object v4, LX/5Oz;->FRIEND_REQUEST_RICH_NOTIFICATION:LX/5Oz;

    invoke-virtual {v4}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/JZg;->a:LX/JZh;

    iget-object v5, v5, LX/JZh;->b:LX/5P0;

    invoke-virtual {v5}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2708894
    iget-object v0, p0, LX/JZg;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v2, p0, LX/JZg;->a:LX/JZh;

    iget-object v2, v2, LX/JZh;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p0, LX/JZg;->a:LX/JZh;

    iget-object v3, v3, LX/JZh;->c:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2708895
    iget-object v0, p0, LX/JZg;->b:LX/1Pq;

    new-array v2, v7, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/JZg;->a:LX/JZh;

    iget-object v4, v4, LX/JZh;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2708896
    const v0, 0x1de3edcc

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
