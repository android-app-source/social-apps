.class public LX/K5L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/K8s;

.field public final c:LX/K5g;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/K8s;LX/K5g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2770329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770330
    iput-object p1, p0, LX/K5L;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2770331
    iput-object p2, p0, LX/K5L;->b:LX/K8s;

    .line 2770332
    iput-object p3, p0, LX/K5L;->c:LX/K5g;

    .line 2770333
    return-void
.end method

.method public static a(LX/0QB;)LX/K5L;
    .locals 6

    .prologue
    .line 2770334
    const-class v1, LX/K5L;

    monitor-enter v1

    .line 2770335
    :try_start_0
    sget-object v0, LX/K5L;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2770336
    sput-object v2, LX/K5L;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2770337
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2770338
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2770339
    new-instance p0, LX/K5L;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/K8s;->a(LX/0QB;)LX/K8s;

    move-result-object v4

    check-cast v4, LX/K8s;

    invoke-static {v0}, LX/K5g;->a(LX/0QB;)LX/K5g;

    move-result-object v5

    check-cast v5, LX/K5g;

    invoke-direct {p0, v3, v4, v5}, LX/K5L;-><init>(Lcom/facebook/content/SecureContextHelper;LX/K8s;LX/K5g;)V

    .line 2770340
    move-object v0, p0

    .line 2770341
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2770342
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K5L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2770343
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2770344
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
