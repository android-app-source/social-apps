.class public LX/JpN;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2737332
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2737333
    return-void
.end method

.method public static a(LX/01T;)LX/0TP;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/service/multicache/MultiCacheThreadsQueue;
    .end annotation

    .prologue
    .line 2737323
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-ne p0, v0, :cond_0

    .line 2737324
    sget-object v0, LX/0TP;->URGENT:LX/0TP;

    .line 2737325
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0TP;->FOREGROUND:LX/0TP;

    goto :goto_0
.end method

.method public static a()LX/1qM;
    .locals 1
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/tincan/messenger/annotations/TincanCachingServiceChain;
    .end annotation

    .prologue
    .line 2737331
    new-instance v0, LX/4BM;

    invoke-direct {v0}, LX/4BM;-><init>()V

    return-object v0
.end method

.method public static a(LX/FDP;LX/FDv;Lcom/facebook/messaging/protocol/WebServiceHandler;)LX/1qM;
    .locals 4
    .param p0    # LX/FDP;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/service/multicache/FacebookCachingServiceChain;
    .end annotation

    .prologue
    .line 2737330
    new-instance v0, LX/2m1;

    new-instance v1, LX/2m1;

    new-instance v2, LX/2m1;

    new-instance v3, LX/4BM;

    invoke-direct {v3}, LX/4BM;-><init>()V

    invoke-direct {v2, p2, v3}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v1, p1, v2}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static a(LX/FDV;LX/FDP;LX/FMD;)LX/1qM;
    .locals 4
    .param p1    # LX/FDP;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/service/multicache/SmsCachingServiceChain;
    .end annotation

    .prologue
    .line 2737329
    new-instance v0, LX/2m1;

    new-instance v1, LX/2m1;

    new-instance v2, LX/2m1;

    new-instance v3, LX/4BM;

    invoke-direct {v3}, LX/4BM;-><init>()V

    invoke-direct {v2, p2, v3}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v1, p1, v2}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static a(LX/JpT;)LX/1qM;
    .locals 2
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/service/multicache/LowPriorityThreadsQueue;
    .end annotation

    .prologue
    .line 2737328
    new-instance v0, LX/2m1;

    new-instance v1, LX/4BM;

    invoke-direct {v1}, LX/4BM;-><init>()V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static b(LX/JpT;)LX/1qM;
    .locals 2
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/service/multicache/PushQueue;
    .end annotation

    .prologue
    .line 2737327
    new-instance v0, LX/2m1;

    new-instance v1, LX/4BM;

    invoke-direct {v1}, LX/4BM;-><init>()V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static c(LX/JpT;)LX/1qM;
    .locals 2
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/service/multicache/MultiCacheThreadsQueue;
    .end annotation

    .prologue
    .line 2737326
    new-instance v0, LX/2m1;

    new-instance v1, LX/4BM;

    invoke-direct {v1}, LX/4BM;-><init>()V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2737322
    return-void
.end method
