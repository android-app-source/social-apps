.class public LX/JxN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JxJ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2753013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753014
    return-void
.end method


# virtual methods
.method public final a(J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cdn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2753012
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot start a ble scan with the EmptyBleScanner"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
