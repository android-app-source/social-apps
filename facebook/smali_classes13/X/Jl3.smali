.class public LX/Jl3;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/user/model/UserKey;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/user/tiles/UserTileView;

.field private c:Landroid/view/View;

.field private d:LX/Jl2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2730786
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2730787
    invoke-direct {p0}, LX/Jl3;->a()V

    .line 2730788
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730783
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2730784
    invoke-direct {p0}, LX/Jl3;->a()V

    .line 2730785
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730780
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2730781
    invoke-direct {p0}, LX/Jl3;->a()V

    .line 2730782
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2730772
    const-class v0, LX/Jl3;

    invoke-static {v0, p0}, LX/Jl3;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2730773
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Jl3;->setOrientation(I)V

    .line 2730774
    const v0, 0x7f0308e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2730775
    const v0, 0x7f0d1706

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Jl3;->b:Lcom/facebook/user/tiles/UserTileView;

    .line 2730776
    const v0, 0x7f0d1707

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jl3;->c:Landroid/view/View;

    .line 2730777
    iget-object v0, p0, LX/Jl3;->c:Landroid/view/View;

    new-instance v1, LX/Jl1;

    invoke-direct {v1, p0}, LX/Jl1;-><init>(LX/Jl3;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2730778
    iget-object v0, p0, LX/Jl3;->b:Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p0, LX/Jl3;->a:Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/8ue;->ACTIVE_NOW:LX/8ue;

    invoke-static {v1, v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2730779
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Jl3;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Jl3;

    invoke-static {v0}, LX/41q;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, LX/Jl3;->a:Lcom/facebook/user/model/UserKey;

    return-void
.end method


# virtual methods
.method public setListener(LX/Jl2;)V
    .locals 0
    .param p1    # LX/Jl2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730770
    iput-object p1, p0, LX/Jl3;->d:LX/Jl2;

    .line 2730771
    return-void
.end method
