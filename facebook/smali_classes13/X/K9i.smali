.class public final LX/K9i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

.field public final synthetic b:Lcom/facebook/video/livemap/LiveMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/livemap/LiveMapFragment;Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;)V
    .locals 0

    .prologue
    .line 2777718
    iput-object p1, p0, LX/K9i;->b:Lcom/facebook/video/livemap/LiveMapFragment;

    iput-object p2, p0, LX/K9i;->a:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 5

    .prologue
    .line 2777719
    iget-object v0, p0, LX/K9i;->b:Lcom/facebook/video/livemap/LiveMapFragment;

    iget-object v0, v0, Lcom/facebook/video/livemap/LiveMapFragment;->g:LX/K9m;

    iget-object v1, p0, LX/K9i;->a:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    invoke-virtual {v1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;->a()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    .line 2777720
    iget-object v2, v0, LX/K9m;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2777721
    iget-object v2, v0, LX/K9m;->p:LX/68L;

    .line 2777722
    iget-object v4, v2, LX/68L;->a:LX/68K;

    const/4 v2, 0x0

    .line 2777723
    iget-object p0, v4, LX/68K;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 2777724
    const/4 p0, 0x1

    iput-boolean p0, v4, LX/68K;->d:Z

    .line 2777725
    iput-object v2, v4, LX/68K;->e:LX/68K;

    .line 2777726
    iput-object v2, v4, LX/68K;->f:LX/68K;

    .line 2777727
    iput-object v2, v4, LX/68K;->g:LX/68K;

    .line 2777728
    iput-object v2, v4, LX/68K;->h:LX/68K;

    .line 2777729
    iput-object v3, v0, LX/K9m;->G:Ljava/util/Set;

    .line 2777730
    iput-object v3, v0, LX/K9m;->H:LX/K9l;

    .line 2777731
    const/4 v2, 0x0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    .line 2777732
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-static {v0, v2}, LX/K9m;->a(LX/K9m;Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;)LX/K9l;

    move-result-object v2

    .line 2777733
    if-eqz v2, :cond_0

    .line 2777734
    iget-object p0, v0, LX/K9m;->q:Ljava/util/ArrayList;

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2777735
    iget-object p0, v0, LX/K9m;->p:LX/68L;

    .line 2777736
    iget-object p1, p0, LX/68L;->a:LX/68K;

    invoke-static {p0, p1, v2}, LX/68L;->a(LX/68L;LX/68K;LX/67g;)Z

    .line 2777737
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2777738
    :cond_1
    iget-object v2, v0, LX/K9m;->E:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 2777739
    return-void
.end method
