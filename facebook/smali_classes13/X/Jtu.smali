.class public LX/Jtu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5Z;


# instance fields
.field private final a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

.field private final b:LX/Jtt;


# direct methods
.method public constructor <init>(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;)V
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2747635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2747636
    iput-object p1, p0, LX/Jtu;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    .line 2747637
    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 2747638
    new-instance v0, LX/Jtt;

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->e()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->np_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->nq_()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->b()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-result-object v5

    invoke-direct/range {v0 .. v7}, LX/Jtt;-><init>(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;Ljava/lang/String;Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;LX/15i;I)V

    iput-object v0, p0, LX/Jtu;->b:LX/Jtt;

    .line 2747639
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747634
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747633
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747632
    iget-object v0, p0, LX/Jtu;->a:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    invoke-virtual {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/B5Y;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747629
    iget-object v0, p0, LX/Jtu;->b:LX/Jtt;

    return-object v0
.end method

.method public final iS_()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlInterfaces$InstantArticleMaster$RelatedArticlesSections;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747631
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iT_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747630
    const/4 v0, 0x0

    return-object v0
.end method
