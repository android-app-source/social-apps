.class public LX/Jn3;
.super LX/1OM;
.source ""

# interfaces
.implements LX/Dct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Jn8;",
        ">;",
        "LX/Dct",
        "<",
        "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/JnA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/Jn1;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2733633
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2733634
    new-instance v0, LX/Jn2;

    invoke-direct {v0, p0}, LX/Jn2;-><init>(LX/Jn3;)V

    iput-object v0, p0, LX/Jn3;->e:LX/Jn1;

    .line 2733635
    iput-object p1, p0, LX/Jn3;->a:Landroid/view/LayoutInflater;

    .line 2733636
    return-void
.end method

.method public static c(LX/Jn3;Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)Ljava/lang/Integer;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2733637
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2733638
    iget-object v0, p0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;

    .line 2733639
    iget-object v2, v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    move-object v0, v2

    .line 2733640
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2733641
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2733642
    :goto_1
    return-object v0

    .line 2733643
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2733644
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2733645
    iget-object v0, p0, LX/Jn3;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0308f3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2733646
    check-cast v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;

    iget-object v2, p0, LX/Jn3;->e:LX/Jn1;

    .line 2733647
    iput-object v2, v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->b:LX/Jn1;

    .line 2733648
    new-instance v0, LX/Jn8;

    invoke-direct {v0, v1}, LX/Jn8;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2733649
    iget-object v0, p0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;

    .line 2733650
    iget-object v3, v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    move-object v3, v3

    .line 2733651
    iget-object v3, v3, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733652
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2733653
    iget-object v4, p1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733654
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2733655
    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2733656
    :goto_1
    return-object v0

    .line 2733657
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2733658
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2733659
    check-cast p1, LX/Jn8;

    .line 2733660
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;

    .line 2733661
    iget-object v1, p0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;

    .line 2733662
    iget-object v2, v1, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    move-object v1, v2

    .line 2733663
    iget-object v2, p0, LX/Jn3;->c:Ljava/util/Set;

    iget-object v3, v1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733664
    iget-object p0, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p0

    .line 2733665
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;Z)V

    .line 2733666
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->setTag(Ljava/lang/Object;)V

    .line 2733667
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2733668
    iget-object v0, p0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
