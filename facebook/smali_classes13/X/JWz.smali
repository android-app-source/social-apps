.class public LX/JWz;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<",
        "LX/JWr;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/JX5;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pb;LX/25M;LX/JX5;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/JWr;",
            ">;TE;",
            "LX/25M;",
            "LX/JX5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703764
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2703765
    iput-object p5, p0, LX/JWz;->c:LX/JX5;

    .line 2703766
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2703767
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2703768
    check-cast p2, LX/JWr;

    .line 2703769
    iget-object v0, p0, LX/JWz;->c:LX/JX5;

    const/4 v1, 0x0

    .line 2703770
    new-instance v2, LX/JX4;

    invoke-direct {v2, v0}, LX/JX4;-><init>(LX/JX5;)V

    .line 2703771
    iget-object v3, v0, LX/JX5;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JX3;

    .line 2703772
    if-nez v3, :cond_0

    .line 2703773
    new-instance v3, LX/JX3;

    invoke-direct {v3, v0}, LX/JX3;-><init>(LX/JX5;)V

    .line 2703774
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JX3;->a$redex0(LX/JX3;LX/1De;IILX/JX4;)V

    .line 2703775
    move-object v2, v3

    .line 2703776
    move-object v1, v2

    .line 2703777
    move-object v0, v1

    .line 2703778
    iget-object v1, v0, LX/JX3;->a:LX/JX4;

    iput-object p2, v1, LX/JX4;->a:LX/JWr;

    .line 2703779
    iget-object v1, v0, LX/JX3;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2703780
    move-object v1, v0

    .line 2703781
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/1Pb;

    .line 2703782
    iget-object v2, v1, LX/JX3;->a:LX/JX4;

    iput-object v0, v2, LX/JX4;->b:LX/1Pb;

    .line 2703783
    iget-object v2, v1, LX/JX3;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2703784
    move-object v0, v1

    .line 2703785
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2703786
    const/4 v0, 0x0

    return v0
.end method
