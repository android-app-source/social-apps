.class public final LX/Jzv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field public final synthetic a:LX/Jzz;


# direct methods
.method public constructor <init>(LX/Jzz;)V
    .locals 0

    .prologue
    .line 2757014
    iput-object p1, p0, LX/Jzv;->a:LX/Jzz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 3

    .prologue
    .line 2757015
    iget-object v0, p0, LX/Jzv;->a:LX/Jzz;

    invoke-static {v0}, LX/Jzz;->a(LX/Jzz;)LX/5pY;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "geolocationDidChange"

    invoke-static {p1}, LX/Jzz;->b(Landroid/location/Location;)LX/5pH;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2757016
    return-void
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757017
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757018
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2757019
    if-nez p2, :cond_1

    .line 2757020
    iget-object v0, p0, LX/Jzv;->a:LX/Jzz;

    sget v1, LX/K00;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is out of service."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2757021
    invoke-static {v0, v1, v2}, LX/Jzz;->a$redex0(LX/Jzz;ILjava/lang/String;)V

    .line 2757022
    :cond_0
    :goto_0
    return-void

    .line 2757023
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2757024
    iget-object v0, p0, LX/Jzv;->a:LX/Jzz;

    sget v1, LX/K00;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is temporarily unavailable."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2757025
    invoke-static {v0, v1, v2}, LX/Jzz;->a$redex0(LX/Jzz;ILjava/lang/String;)V

    .line 2757026
    goto :goto_0
.end method
