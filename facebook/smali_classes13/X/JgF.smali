.class public LX/JgF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jfu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jfu",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2722562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722563
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/Jg4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2722564
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2722565
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;

    .line 2722566
    new-instance v4, LX/Jg6;

    invoke-direct {v4, v0}, LX/Jg6;-><init>(Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2722567
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2722568
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
