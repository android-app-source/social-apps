.class public final LX/K5V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2770439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770440
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2770441
    const-string v0, "digest_ids"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2770442
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2770443
    const/4 v0, 0x0

    .line 2770444
    :goto_0
    return-object v0

    .line 2770445
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2770446
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2770447
    const-string v2, ","

    invoke-static {v2}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v2

    invoke-virtual {v2}, LX/2Cb;->trimResults()LX/2Cb;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2770448
    const-string v2, "tarot_story_ids"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2770449
    const-string v1, "tarot_click_source"

    const-string v2, "link"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2770450
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/facebook/tarot/MainActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2770451
    const-string v1, "force_external_activity"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method
