.class public final LX/Jl6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2730824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2730825
    new-instance v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitItem;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitItem;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2730826
    new-array v0, p1, [Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitItem;

    return-object v0
.end method
