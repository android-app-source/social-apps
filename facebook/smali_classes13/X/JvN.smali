.class public final LX/JvN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8E8;


# instance fields
.field public final synthetic a:LX/JvZ;

.field public final synthetic b:LX/4At;

.field public final synthetic c:LX/JvO;


# direct methods
.method public constructor <init>(LX/JvO;LX/JvZ;LX/4At;)V
    .locals 0

    .prologue
    .line 2750308
    iput-object p1, p0, LX/JvN;->c:LX/JvO;

    iput-object p2, p0, LX/JvN;->a:LX/JvZ;

    iput-object p3, p0, LX/JvN;->b:LX/4At;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2750309
    iget-object v0, p0, LX/JvN;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2750310
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2

    .prologue
    .line 2750311
    iget-object v0, p0, LX/JvN;->c:LX/JvO;

    iget-object v1, p0, LX/JvN;->a:LX/JvZ;

    invoke-static {v0, v1, p1}, LX/JvO;->a$redex0(LX/JvO;LX/JvZ;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2750312
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2750313
    iget-object v0, p0, LX/JvN;->c:LX/JvO;

    iget-object v0, v0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v1, LX/0ig;->aq:LX/0ih;

    iget-object v2, p0, LX/JvN;->c:LX/JvO;

    iget-object v2, v2, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-wide v2, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    const-string v4, "voice_switch_fail"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750314
    iget-object v0, p0, LX/JvN;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2750315
    iget-object v0, p0, LX/JvN;->c:LX/JvO;

    iget-object v0, v0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083bfc

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2750316
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2

    .prologue
    .line 2750317
    iget-object v0, p0, LX/JvN;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2750318
    iget-object v0, p0, LX/JvN;->c:LX/JvO;

    iget-object v1, p0, LX/JvN;->a:LX/JvZ;

    invoke-static {v0, v1, p1}, LX/JvO;->a$redex0(LX/JvO;LX/JvZ;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2750319
    return-void
.end method
