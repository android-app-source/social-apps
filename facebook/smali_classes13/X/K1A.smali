.class public LX/K1A;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K1A;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:F

.field private c:F

.field private d:I


# direct methods
.method public constructor <init>(ILjava/lang/String;FFI)V
    .locals 0

    .prologue
    .line 2761412
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2761413
    iput-object p2, p0, LX/K1A;->a:Ljava/lang/String;

    .line 2761414
    iput p3, p0, LX/K1A;->b:F

    .line 2761415
    iput p4, p0, LX/K1A;->c:F

    .line 2761416
    iput p5, p0, LX/K1A;->d:I

    .line 2761417
    return-void
.end method

.method private j()LX/5pH;
    .locals 6

    .prologue
    .line 2761418
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2761419
    const-string v1, "text"

    iget-object v2, p0, LX/K1A;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761420
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2761421
    const-string v2, "width"

    iget v3, p0, LX/K1A;->b:F

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761422
    const-string v2, "height"

    iget v3, p0, LX/K1A;->c:F

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761423
    const-string v2, "contentSize"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2761424
    const-string v1, "eventCount"

    iget v2, p0, LX/K1A;->d:I

    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761425
    const-string v1, "target"

    .line 2761426
    iget v2, p0, LX/5r0;->c:I

    move v2, v2

    .line 2761427
    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761428
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2761429
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2761430
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K1A;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2761431
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761432
    const-string v0, "topChange"

    return-object v0
.end method
