.class public LX/JX7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

.field private final b:Ljava/lang/String;

.field private final c:LX/1Uf;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Uf;)V
    .locals 2

    .prologue
    .line 2703960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703961
    iput-object p1, p0, LX/JX7;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2703962
    iput-object p2, p0, LX/JX7;->c:LX/1Uf;

    .line 2703963
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "researchpoll:header:title:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2703964
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 2703965
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2703966
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2703967
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2703968
    :cond_0
    iput-object v0, p0, LX/JX7;->b:Ljava/lang/String;

    .line 2703969
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2703971
    const/4 v7, 0x0

    .line 2703972
    iget-object v0, p0, LX/JX7;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703973
    new-instance v0, LX/1yT;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v7}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    .line 2703974
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1yT;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, LX/JX7;->c:LX/1Uf;

    iget-object v3, p0, LX/JX7;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v3

    iget-object v4, p0, LX/JX7;->a:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    sget-object v5, LX/1eK;->TITLE:LX/1eK;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v7}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703970
    iget-object v0, p0, LX/JX7;->b:Ljava/lang/String;

    return-object v0
.end method
