.class public final LX/Jjg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V
    .locals 0

    .prologue
    .line 2728032
    iput-object p1, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, -0x58d25d9b

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2728033
    iget-object v1, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->G:LX/JjT;

    iget-object v2, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728034
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2728035
    const-string v3, "DECLINED"

    iget-object v4, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v4, v4, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->s:Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-virtual {v1, v2, v3, v4}, LX/JjT;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)V

    .line 2728036
    iget-object v1, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-static {v1, v2}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->a$redex0(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;)V

    .line 2728037
    iget-object v1, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->B:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 2728038
    iget-object v1, p0, LX/Jjg;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->A:Lcom/facebook/resources/ui/FbCheckedTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 2728039
    const v1, -0x2109b798

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
