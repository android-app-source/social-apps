.class public final LX/Jvd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750684
    iput-object p1, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2750685
    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->E:LX/BGC;

    invoke-virtual {v0, p1}, LX/BGC;->a(Z)V

    .line 2750686
    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v0}, LX/4gI;->supportsPhotos()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v0}, LX/4gI;->supportsVideos()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2750687
    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2750688
    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->setVisibility(I)V

    .line 2750689
    :cond_0
    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    .line 2750690
    iget-object v0, p0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2750691
    :cond_1
    return-void
.end method
