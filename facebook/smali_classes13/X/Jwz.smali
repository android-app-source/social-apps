.class public final LX/Jwz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/4Iv;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jx9;

.field public final synthetic b:LX/Jx0;


# direct methods
.method public constructor <init>(LX/Jx0;LX/Jx9;)V
    .locals 0

    .prologue
    .line 2752691
    iput-object p1, p0, LX/Jwz;->b:LX/Jx0;

    iput-object p2, p0, LX/Jwz;->a:LX/Jx9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752681
    check-cast p1, Ljava/util/List;

    .line 2752682
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JwV;

    .line 2752683
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2ct;

    .line 2752684
    invoke-virtual {v1}, LX/2ct;->a()LX/0am;

    move-result-object v1

    .line 2752685
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2752686
    iget-object v1, v0, LX/JwV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/JwY;->a:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 2752687
    if-nez v0, :cond_1

    .line 2752688
    :cond_0
    iget-object v0, p0, LX/Jwz;->b:LX/Jx0;

    iget-object v0, v0, LX/Jx0;->a:LX/JxB;

    iget-object v0, v0, LX/JxB;->h:LX/0bW;

    const-string v1, "Creating inputParams anyway"

    invoke-interface {v0, v1}, LX/0bW;->a(Ljava/lang/String;)V

    .line 2752689
    iget-object v0, p0, LX/Jwz;->a:LX/Jx9;

    invoke-virtual {v0}, LX/Jx9;->b()LX/4Iv;

    move-result-object v0

    .line 2752690
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
