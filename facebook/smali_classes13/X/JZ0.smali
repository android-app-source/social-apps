.class public LX/JZ0;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/LinearLayout;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2707686
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JZ0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2707687
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2707688
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2707689
    const v0, 0x7f031442

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2707690
    const v0, 0x7f0d2e49

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JZ0;->a:Landroid/widget/TextView;

    .line 2707691
    const v0, 0x7f0d2e4a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/JZ0;->b:Landroid/widget/LinearLayout;

    .line 2707692
    const v0, 0x7f0d2e4b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JZ0;->c:Landroid/widget/TextView;

    .line 2707693
    return-void
.end method


# virtual methods
.method public setAnswerClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 2707694
    iget-object v0, p0, LX/JZ0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 2707695
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2707696
    iget-object v0, p0, LX/JZ0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2707697
    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2707698
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2707699
    :cond_0
    return-void
.end method

.method public setCompleteLayoutActive(Z)V
    .locals 2

    .prologue
    .line 2707700
    iget-object v1, p0, LX/JZ0;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2707701
    return-void

    .line 2707702
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setQuestionActive(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2707703
    iget-object v3, p0, LX/JZ0;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2707704
    iget-object v0, p0, LX/JZ0;->b:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2707705
    return-void

    :cond_0
    move v0, v2

    .line 2707706
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2707707
    goto :goto_1
.end method
