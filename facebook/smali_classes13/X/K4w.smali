.class public final synthetic LX/K4w;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2769638
    invoke-static {}, LX/K5C;->values()[LX/K5C;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/K4w;->b:[I

    :try_start_0
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->LESS:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_0
    :try_start_1
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->LEQUAL:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_1
    :try_start_2
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->GREATER:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_2
    :try_start_3
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->GEQUAL:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_3
    :try_start_4
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->EQUAL:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_4
    :try_start_5
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->NOTEQUAL:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_5
    :try_start_6
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->ALWAYS:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_6
    :try_start_7
    sget-object v0, LX/K4w;->b:[I

    sget-object v1, LX/K5C;->NEVER:LX/K5C;

    invoke-virtual {v1}, LX/K5C;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    .line 2769639
    :goto_7
    invoke-static {}, LX/K4v;->values()[LX/K4v;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/K4w;->a:[I

    :try_start_8
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->NORMAL:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_8
    :try_start_9
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->BLEND:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_9
    :try_start_a
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->MULTIPLY:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_a
    :try_start_b
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->SCREEN:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_b
    :try_start_c
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->ADD:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_c
    :try_start_d
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->SUBTRACT:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_d
    :try_start_e
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->REVSUBTRACT:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_e
    :try_start_f
    sget-object v0, LX/K4w;->a:[I

    sget-object v1, LX/K4v;->PREMULT_ALPHA:LX/K4v;

    invoke-virtual {v1}, LX/K4v;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_f
    return-void

    :catch_0
    goto :goto_f

    :catch_1
    goto :goto_e

    :catch_2
    goto :goto_d

    :catch_3
    goto :goto_c

    :catch_4
    goto :goto_b

    :catch_5
    goto :goto_a

    :catch_6
    goto :goto_9

    :catch_7
    goto :goto_8

    :catch_8
    goto :goto_7

    :catch_9
    goto :goto_6

    :catch_a
    goto/16 :goto_5

    :catch_b
    goto/16 :goto_4

    :catch_c
    goto/16 :goto_3

    :catch_d
    goto/16 :goto_2

    :catch_e
    goto/16 :goto_1

    :catch_f
    goto/16 :goto_0
.end method
