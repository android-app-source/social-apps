.class public LX/Jbp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drt;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Jbp;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jbq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jbr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Jbq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jbr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717132
    iput-object p1, p0, LX/Jbp;->a:LX/0Ot;

    .line 2717133
    iput-object p2, p0, LX/Jbp;->b:LX/0Ot;

    .line 2717134
    return-void
.end method

.method public static a(LX/0QB;)LX/Jbp;
    .locals 5

    .prologue
    .line 2717139
    sget-object v0, LX/Jbp;->c:LX/Jbp;

    if-nez v0, :cond_1

    .line 2717140
    const-class v1, LX/Jbp;

    monitor-enter v1

    .line 2717141
    :try_start_0
    sget-object v0, LX/Jbp;->c:LX/Jbp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2717142
    if-eqz v2, :cond_0

    .line 2717143
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2717144
    new-instance v3, LX/Jbp;

    const/16 v4, 0x25b9

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x25ba

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/Jbp;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2717145
    move-object v0, v3

    .line 2717146
    sput-object v0, LX/Jbp;->c:LX/Jbp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2717147
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2717148
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2717149
    :cond_1
    sget-object v0, LX/Jbp;->c:LX/Jbp;

    return-object v0

    .line 2717150
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2717151
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;)LX/Drs;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2717135
    sget-object v0, LX/Jbo;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2717136
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2717137
    :pswitch_0
    iget-object v0, p0, LX/Jbp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    .line 2717138
    :pswitch_1
    iget-object v0, p0, LX/Jbp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
