.class public LX/JhW;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Landroid/widget/CheckBox;

.field public b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

.field public c:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterButton;",
            ">;"
        }
    .end annotation
.end field

.field public d:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public e:LX/DAO;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2724525
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2724526
    const v0, 0x7f030cd6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2724527
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/JhW;->a:Landroid/widget/CheckBox;

    .line 2724528
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iput-object v0, p0, LX/JhW;->b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2724529
    const v0, 0x7f0d1ff9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/JhW;->c:LX/4ob;

    .line 2724530
    iget-object v0, p0, LX/JhW;->b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v0}, LX/2Wj;->getTextColor()I

    move-result v0

    iput v0, p0, LX/JhW;->d:I

    .line 2724531
    return-void
.end method

.method public static b$redex0(LX/JhW;)V
    .locals 5

    .prologue
    .line 2724532
    iget-object v0, p0, LX/JhW;->e:LX/DAO;

    .line 2724533
    iget-boolean v1, v0, LX/DAO;->d:Z

    move v0, v1

    .line 2724534
    if-eqz v0, :cond_0

    .line 2724535
    iget-object v0, p0, LX/JhW;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2724536
    iget-object v0, p0, LX/JhW;->a:Landroid/widget/CheckBox;

    iget-object v1, p0, LX/JhW;->e:LX/DAO;

    invoke-virtual {v1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2724537
    :goto_0
    iget-object v0, p0, LX/JhW;->e:LX/DAO;

    invoke-virtual {v0}, LX/3OP;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2724538
    iget-object v0, p0, LX/JhW;->b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {p0}, LX/JhW;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010592

    invoke-virtual {p0}, LX/JhW;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a019a

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v1, v2, v3}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 2724539
    :goto_1
    iget-object v0, p0, LX/JhW;->e:LX/DAO;

    .line 2724540
    iget-boolean v1, v0, LX/DAO;->e:Z

    move v0, v1

    .line 2724541
    if-eqz v0, :cond_3

    .line 2724542
    iget-object v0, p0, LX/JhW;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2724543
    iget-object v0, p0, LX/JhW;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/JhW;->e:LX/DAO;

    .line 2724544
    iget-boolean v2, v1, LX/DAO;->b:Z

    move v1, v2

    .line 2724545
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2724546
    iget-object v0, p0, LX/JhW;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/JhW;->e:LX/DAO;

    .line 2724547
    iget-boolean v2, v1, LX/DAO;->b:Z

    move v1, v2

    .line 2724548
    if-eqz v1, :cond_2

    invoke-virtual {p0}, LX/JhW;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08030d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2724549
    iget-object v0, p0, LX/JhW;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2724550
    new-instance v1, LX/JhV;

    invoke-direct {v1, p0}, LX/JhV;-><init>(LX/JhW;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724551
    :goto_3
    return-void

    .line 2724552
    :cond_0
    iget-object v0, p0, LX/JhW;->a:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 2724553
    :cond_1
    iget-object v0, p0, LX/JhW;->b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iget v1, p0, LX/JhW;->d:I

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    goto :goto_1

    .line 2724554
    :cond_2
    invoke-virtual {p0}, LX/JhW;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08030e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2724555
    :cond_3
    iget-object v0, p0, LX/JhW;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto :goto_3
.end method


# virtual methods
.method public getContactRow()LX/DAO;
    .locals 1

    .prologue
    .line 2724556
    iget-object v0, p0, LX/JhW;->e:LX/DAO;

    return-object v0
.end method
