.class public final LX/JxP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:LX/JxO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2753022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/JxP;

    invoke-static {v0}, LX/JxO;->b(LX/0QB;)LX/JxO;

    move-result-object v0

    check-cast v0, LX/JxO;

    iput-object v0, p0, LX/JxP;->a:LX/JxO;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x11318ffb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2753023
    invoke-static {p0, p1}, LX/JxP;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2753024
    iget-object v1, p0, LX/JxP;->a:LX/JxO;

    .line 2753025
    iget-object v2, v1, LX/JxO;->a:LX/0bW;

    const-string p0, "Bluetooth discovery finished, disabling receiver and starting ble scan"

    invoke-interface {v2, p0}, LX/0bW;->a(Ljava/lang/String;)V

    .line 2753026
    iget-object v2, v1, LX/JxO;->c:LX/1ZL;

    invoke-interface {v2}, LX/1ZL;->a()V

    .line 2753027
    iget-object v2, v1, LX/JxO;->b:LX/3lk;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/3lk;->a(Z)V

    .line 2753028
    const/16 v1, 0x27

    const v2, -0x6cc668cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
