.class public final LX/JZ3;
.super LX/37T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

.field private final b:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

.field private final c:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V
    .locals 2

    .prologue
    .line 2707807
    iput-object p1, p0, LX/JZ3;->a:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    invoke-direct {p0}, LX/37T;-><init>()V

    .line 2707808
    iput-object p2, p0, LX/JZ3;->b:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 2707809
    new-instance v0, LX/JZ2;

    invoke-direct {v0, p1, p2}, LX/JZ2;-><init>(Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V

    iput-object v0, p0, LX/JZ3;->c:LX/1KL;

    .line 2707810
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 2707806
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2707803
    iget-object v0, p0, LX/JZ3;->c:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2707805
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 2707804
    iget-object v0, p0, LX/JZ3;->b:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    return-object v0
.end method
