.class public LX/JbE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CFf;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/HYg;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hw;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0lC;

.field public final g:LX/0Uq;

.field public final h:Landroid/content/Context;

.field private final i:LX/2Af;


# direct methods
.method public constructor <init>(LX/0Zb;LX/HYg;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0lC;LX/0Uq;LX/2Af;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/HYg;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/0hw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0lC;",
            "LX/0Uq;",
            "LX/2Af;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2716599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2716600
    iput-object p1, p0, LX/JbE;->a:LX/0Zb;

    .line 2716601
    iput-object p2, p0, LX/JbE;->b:LX/HYg;

    .line 2716602
    iput-object p3, p0, LX/JbE;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2716603
    iput-object p4, p0, LX/JbE;->d:LX/0Ot;

    .line 2716604
    iput-object p5, p0, LX/JbE;->e:LX/0Ot;

    .line 2716605
    iput-object p6, p0, LX/JbE;->f:LX/0lC;

    .line 2716606
    iput-object p7, p0, LX/JbE;->g:LX/0Uq;

    .line 2716607
    iput-object p9, p0, LX/JbE;->h:Landroid/content/Context;

    .line 2716608
    iput-object p8, p0, LX/JbE;->i:LX/2Af;

    .line 2716609
    return-void
.end method

.method private d(LX/0P1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2716610
    const-string v0, "campaign_id"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2716611
    const-string v1, "utm_campaign"

    invoke-virtual {p1, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2716612
    iget-object v2, p0, LX/JbE;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2716613
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2716614
    :goto_0
    return-void

    .line 2716615
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/JbE;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hw;

    iget-object v1, p0, LX/JbE;->f:LX/0lC;

    invoke-virtual {v1, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 p0, 0x1

    .line 2716616
    iget-object v3, v0, LX/0hw;->d:LX/0Zb;

    const-string v4, "install_referrer_with_campaign"

    invoke-interface {v3, v4, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2716617
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2716618
    const-string v4, "growth"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2716619
    const-string v4, "campaign_id"

    invoke-virtual {v3, v4, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2716620
    const-string v4, "ad_click_time"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2716621
    const-string v4, "advertising_id"

    invoke-virtual {v0, p0}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2716622
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2716623
    :cond_1
    iget-object v3, v0, LX/0hw;->e:LX/0Uh;

    const/16 v4, 0x49

    invoke-virtual {v3, v4, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2716624
    iget-object v3, v0, LX/0hw;->c:LX/0Wd;

    new-instance v4, Lcom/facebook/growth/sem/SemTrackingLogger$2;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/growth/sem/SemTrackingLogger$2;-><init>(LX/0hw;Ljava/lang/String;Ljava/lang/String;)V

    const v5, 0x1490675

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 2716625
    :cond_2
    goto :goto_0

    .line 2716626
    :catch_0
    move-exception v0

    .line 2716627
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2716628
    iget-object v0, p0, LX/JbE;->i:LX/2Af;

    iget-object v1, p0, LX/JbE;->h:Landroid/content/Context;

    .line 2716629
    const v2, 0x1b7740

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2Af;->a(Landroid/content/Context;II)V

    .line 2716630
    const-string v0, "utm_reg"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2716631
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, LX/JbE;->d(LX/0P1;)V

    .line 2716632
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const-string v0, "utm_uid"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    const-string v0, "utm_nonce"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2716633
    :goto_1
    return-void

    .line 2716634
    :cond_1
    const-string v0, "utm_reg"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2716635
    const-string v1, "utm_source"

    invoke-virtual {p1, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2716636
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2716637
    const-string v1, "unknown"

    .line 2716638
    :cond_2
    iget-object v2, p0, LX/JbE;->g:LX/0Uq;

    invoke-virtual {v2}, LX/0Uq;->b()V

    .line 2716639
    iget-object v2, p0, LX/JbE;->b:LX/HYg;

    invoke-virtual {v2, v0}, LX/HYg;->a(Ljava/lang/String;)V

    .line 2716640
    iget-object v2, p0, LX/JbE;->a:LX/0Zb;

    const-string v3, "reg_native_app_open"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2716641
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2716642
    const-string v3, "growth"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2716643
    const-string v3, "source"

    invoke-virtual {v2, v3, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2716644
    const-string v1, "reg_instance"

    invoke-virtual {v2, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2716645
    invoke-virtual {v2}, LX/0oG;->d()V

    goto :goto_0

    .line 2716646
    :cond_3
    const-string v0, "utm_uid"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2716647
    const-string v1, "utm_nonce"

    invoke-virtual {p1, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2716648
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "google_play_referrer_login_attempt"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2716649
    const-string v3, "growth"

    .line 2716650
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2716651
    const-string v3, "contactpoint"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2716652
    const-string v3, "fblogin://login/fbauth/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 2716653
    const-string v4, "contactpoint"

    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2716654
    const-string v0, "nonce"

    invoke-virtual {v3, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2716655
    const-string v0, "landing_page"

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2716656
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2716657
    const-string v1, "landing_page"

    invoke-virtual {v3, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2716658
    const-string v1, "landing_page"

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2716659
    :cond_4
    iget-object v0, p0, LX/JbE;->a:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2716660
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2716661
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2716662
    iget-object v1, p0, LX/JbE;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/JbE;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1
.end method
