.class public LX/JWX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JWa;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWX",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JWa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703096
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703097
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWX;->b:LX/0Zi;

    .line 2703098
    iput-object p1, p0, LX/JWX;->a:LX/0Ot;

    .line 2703099
    return-void
.end method

.method public static a(LX/0QB;)LX/JWX;
    .locals 4

    .prologue
    .line 2703102
    const-class v1, LX/JWX;

    monitor-enter v1

    .line 2703103
    :try_start_0
    sget-object v0, LX/JWX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703104
    sput-object v2, LX/JWX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703105
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703106
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703107
    new-instance v3, LX/JWX;

    const/16 p0, 0x20cf

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWX;-><init>(LX/0Ot;)V

    .line 2703108
    move-object v0, v3

    .line 2703109
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703110
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703111
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2703113
    check-cast p2, LX/JWW;

    .line 2703114
    iget-object v0, p0, LX/JWX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWa;

    iget-object v1, p2, LX/JWW;->a:LX/1Pc;

    iget-object v2, p2, LX/JWW;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x0

    const/4 v5, 0x0

    .line 2703115
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b1bc0

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    const v6, 0x7f0b1bbf

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    const v6, 0x7f020b75

    invoke-virtual {v5, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0828a1

    invoke-virtual {v5, v6}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0052

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00ab

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    sget-object v6, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->MEDIUM:LX/0xr;

    invoke-static {p1, v6, p0, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0828a2

    invoke-virtual {v5, v6}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00a8

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    sget-object v6, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->REGULAR:LX/0xr;

    invoke-static {p1, v6, p0, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JWa;->a:LX/JWi;

    const/4 v5, 0x0

    .line 2703116
    new-instance v6, LX/JWh;

    invoke-direct {v6, v4}, LX/JWh;-><init>(LX/JWi;)V

    .line 2703117
    iget-object p0, v4, LX/JWi;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JWg;

    .line 2703118
    if-nez p0, :cond_0

    .line 2703119
    new-instance p0, LX/JWg;

    invoke-direct {p0, v4}, LX/JWg;-><init>(LX/JWi;)V

    .line 2703120
    :cond_0
    invoke-static {p0, p1, v5, v5, v6}, LX/JWg;->a$redex0(LX/JWg;LX/1De;IILX/JWh;)V

    .line 2703121
    move-object v6, p0

    .line 2703122
    move-object v5, v6

    .line 2703123
    move-object v4, v5

    .line 2703124
    iget-object v5, v4, LX/JWg;->a:LX/JWh;

    iput-object v1, v5, LX/JWh;->a:LX/1Pc;

    .line 2703125
    iget-object v5, v4, LX/JWg;->e:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2703126
    move-object v4, v4

    .line 2703127
    iget-object v5, v4, LX/JWg;->a:LX/JWh;

    iput-object v2, v5, LX/JWh;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703128
    iget-object v5, v4, LX/JWg;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2703129
    move-object v4, v4

    .line 2703130
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2703131
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703100
    invoke-static {}, LX/1dS;->b()V

    .line 2703101
    const/4 v0, 0x0

    return-object v0
.end method
