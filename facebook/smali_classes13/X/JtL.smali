.class public final LX/JtL;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2746581
    iput-object p1, p0, LX/JtL;->b:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iput-object p2, p0, LX/JtL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2746587
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2746588
    iget-object v0, p0, LX/JtL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x12f5a933

    invoke-static {v0, v2, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746589
    :goto_0
    return-void

    .line 2746590
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I

    .line 2746591
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 2746592
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1FK;

    .line 2746593
    invoke-virtual {v1}, LX/1FK;->a()I

    move-result v3

    new-array v3, v3, [B

    .line 2746594
    :try_start_0
    new-instance v4, LX/1lZ;

    invoke-direct {v4, v1}, LX/1lZ;-><init>(LX/1FK;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2746595
    :try_start_1
    invoke-static {v4, v3}, LX/0hW;->a(Ljava/io/InputStream;[B)V

    .line 2746596
    invoke-virtual {v4}, LX/1lZ;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2746597
    :try_start_2
    invoke-virtual {v4}, LX/1lZ;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2746598
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 2746599
    :goto_1
    iget-object v0, p0, LX/JtL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x1bbd34e4

    invoke-static {v0, v3, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0

    .line 2746600
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2746601
    :catchall_0
    move-exception v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    :goto_2
    if-eqz v2, :cond_1

    :try_start_4
    invoke-virtual {v4}, LX/1lZ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_3
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_1
    move-exception v1

    .line 2746602
    :try_start_6
    iget-object v2, p0, LX/JtL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v2, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746603
    sget-object v2, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v4, "IO Exception occurred"

    invoke-static {v2, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2746604
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_1

    .line 2746605
    :catch_2
    move-exception v4

    :try_start_7
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 2746606
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, LX/1FJ;->close()V

    throw v1

    .line 2746607
    :cond_1
    :try_start_8
    invoke-virtual {v4}, LX/1lZ;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_3

    :catchall_2
    move-exception v1

    goto :goto_2
.end method

.method public final f(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2746582
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    .line 2746583
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2746584
    iget-object v0, p0, LX/JtL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746585
    :goto_0
    return-void

    .line 2746586
    :cond_0
    iget-object v0, p0, LX/JtL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x54607ca4

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method
