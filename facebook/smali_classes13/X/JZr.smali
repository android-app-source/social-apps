.class public LX/JZr;
.super LX/2Ip;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "LX/2Ip;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<TE;>;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2709234
    invoke-direct {p0}, LX/2Ip;-><init>()V

    .line 2709235
    iput-object p1, p0, LX/JZr;->a:Ljava/lang/ref/WeakReference;

    .line 2709236
    iput-object p2, p0, LX/JZr;->b:Ljava/lang/ref/WeakReference;

    .line 2709237
    iput-object p3, p0, LX/JZr;->c:Ljava/lang/ref/WeakReference;

    .line 2709238
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2709212
    check-cast p1, LX/2f2;

    .line 2709213
    iget-object v0, p0, LX/JZr;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709214
    iget-object v1, p0, LX/JZr;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Pq;

    .line 2709215
    iget-object v2, p0, LX/JZr;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2709216
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-wide v4, p1, LX/2f2;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2709217
    :cond_0
    :goto_0
    return-void

    .line 2709218
    :cond_1
    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2709219
    sget-object v3, LX/JZq;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2709220
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    :goto_1
    move-object v5, v3

    .line 2709221
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eq v5, v2, :cond_0

    iget-object v2, p0, LX/JZr;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709222
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v3

    .line 2709223
    invoke-interface {v2}, LX/9uc;->cs()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v2

    if-eq v5, v2, :cond_0

    move-object v2, v1

    .line 2709224
    check-cast v2, LX/2kk;

    iget-object v3, p0, LX/JZr;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v4, p0, LX/JZr;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709225
    iget-object p0, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, p0

    .line 2709226
    check-cast v4, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-static {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v4

    invoke-static {v4}, LX/9vz;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/9vz;

    move-result-object v4

    .line 2709227
    iput-object v5, v4, LX/9vz;->co:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2709228
    move-object v4, v4

    .line 2709229
    invoke-virtual {v4}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 2709230
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    .line 2709231
    :pswitch_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    goto :goto_1

    .line 2709232
    :pswitch_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->PENDING:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    goto :goto_1

    .line 2709233
    :pswitch_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
