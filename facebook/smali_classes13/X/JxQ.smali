.class public final LX/JxQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0bZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/placetips/pulsarcore/service/PulsarService;


# direct methods
.method public constructor <init>(Lcom/facebook/placetips/pulsarcore/service/PulsarService;)V
    .locals 0

    .prologue
    .line 2753029
    iput-object p1, p0, LX/JxQ;->a:Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2753030
    iget-object v0, p0, LX/JxQ;->a:Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    iget-object v0, v0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->c:LX/0bW;

    const-string v1, "Finished \"successful\" scan"

    invoke-interface {v0, v1}, LX/0bW;->a(Ljava/lang/String;)V

    .line 2753031
    iget-object v0, p0, LX/JxQ;->a:Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    invoke-virtual {v0}, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->stopSelf()V

    .line 2753032
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2753033
    iget-object v0, p0, LX/JxQ;->a:Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    iget-object v0, v0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->c:LX/0bW;

    const-string v1, "Failed to execute pulsar scan"

    invoke-interface {v0, p1, v1}, LX/0bW;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2753034
    iget-object v0, p0, LX/JxQ;->a:Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    invoke-virtual {v0}, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->stopSelf()V

    .line 2753035
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2753036
    invoke-direct {p0}, LX/JxQ;->a()V

    return-void
.end method
