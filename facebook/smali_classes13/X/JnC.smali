.class public LX/JnC;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Jn3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private d:LX/1P0;

.field private e:LX/JnB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2733862
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2733863
    invoke-direct {p0}, LX/JnC;->a()V

    .line 2733864
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733829
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2733830
    invoke-direct {p0}, LX/JnC;->a()V

    .line 2733831
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733859
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733860
    invoke-direct {p0}, LX/JnC;->a()V

    .line 2733861
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2733843
    const-class v0, LX/JnC;

    invoke-static {v0, p0}, LX/JnC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733844
    const v0, 0x7f030f23

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2733845
    const v0, 0x7f0d0b3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/JnC;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2733846
    invoke-virtual {p0}, LX/JnC;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b24ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2733847
    invoke-virtual {p0}, LX/JnC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b24e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2733848
    new-instance v2, LX/1P0;

    invoke-virtual {p0}, LX/JnC;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/JnC;->d:LX/1P0;

    .line 2733849
    iget-object v2, p0, LX/JnC;->d:LX/1P0;

    invoke-virtual {v2, v4}, LX/1P1;->b(I)V

    .line 2733850
    iget-object v2, p0, LX/JnC;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/Jn9;

    invoke-direct {v3, p0, v1, v0}, LX/Jn9;-><init>(LX/JnC;II)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2733851
    iget-object v0, p0, LX/JnC;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JnC;->d:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2733852
    iget-object v0, p0, LX/JnC;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2733853
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 2733854
    iput-boolean v4, v0, LX/1Of;->g:Z

    .line 2733855
    iget-object v0, p0, LX/JnC;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JnC;->a:LX/Jn3;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2733856
    iget-object v0, p0, LX/JnC;->a:LX/Jn3;

    new-instance v1, LX/JnA;

    invoke-direct {v1, p0}, LX/JnA;-><init>(LX/JnC;)V

    .line 2733857
    iput-object v1, v0, LX/Jn3;->d:LX/JnA;

    .line 2733858
    return-void
.end method

.method private static a(LX/JnC;LX/Jn3;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0

    .prologue
    .line 2733842
    iput-object p1, p0, LX/JnC;->a:LX/Jn3;

    iput-object p2, p0, LX/JnC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/JnC;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/JnC;

    new-instance p1, LX/Jn3;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-direct {p1, v0}, LX/Jn3;-><init>(Landroid/view/LayoutInflater;)V

    move-object v0, p1

    check-cast v0, LX/Jn3;

    invoke-static {v1}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0, v0, v1}, LX/JnC;->a(LX/JnC;LX/Jn3;Ljava/util/concurrent/ScheduledExecutorService;)V

    return-void
.end method

.method public static a$redex0(LX/JnC;I)V
    .locals 5

    .prologue
    .line 2733839
    new-instance v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUnitView$3;

    invoke-direct {v0, p0, p1}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUnitView$3;-><init>(LX/JnC;I)V

    .line 2733840
    iget-object v1, p0, LX/JnC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x12c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2733841
    return-void
.end method


# virtual methods
.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2733838
    iget-object v0, p0, LX/JnC;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public getTrackableItemAdapter()LX/Dct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Dct",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2733837
    iget-object v0, p0, LX/JnC;->a:LX/Jn3;

    return-object v0
.end method

.method public setData(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2733832
    iget-object v0, p0, LX/JnC;->a:LX/Jn3;

    .line 2733833
    iput-object p1, v0, LX/Jn3;->b:LX/0Px;

    .line 2733834
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    iput-object p0, v0, LX/Jn3;->c:Ljava/util/Set;

    .line 2733835
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2733836
    return-void
.end method

.method public setListener(LX/JnB;)V
    .locals 0

    .prologue
    .line 2733827
    iput-object p1, p0, LX/JnC;->e:LX/JnB;

    .line 2733828
    return-void
.end method
