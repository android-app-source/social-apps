.class public LX/Jz3;
.super LX/Jyx;
.source ""


# instance fields
.field private final g:LX/JzS;

.field private final h:[I


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 4

    .prologue
    .line 2755675
    invoke-direct {p0}, LX/Jyx;-><init>()V

    .line 2755676
    iput-object p2, p0, LX/Jz3;->g:LX/JzS;

    .line 2755677
    const-string v0, "input"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    .line 2755678
    invoke-interface {v1}, LX/5pC;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Jz3;->h:[I

    .line 2755679
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/Jz3;->h:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2755680
    iget-object v2, p0, LX/Jz3;->h:[I

    invoke-interface {v1, v0}, LX/5pC;->getInt(I)I

    move-result v3

    aput v3, v2, v0

    .line 2755681
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2755682
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2755683
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Jz3;->h:[I

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 2755684
    iget-object v0, p0, LX/Jz3;->g:LX/JzS;

    iget-object v2, p0, LX/Jz3;->h:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v0

    .line 2755685
    if-eqz v0, :cond_2

    instance-of v2, v0, LX/Jyx;

    if-eqz v2, :cond_2

    .line 2755686
    check-cast v0, LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->b()D

    move-result-wide v2

    .line 2755687
    if-nez v1, :cond_0

    .line 2755688
    iput-wide v2, p0, LX/Jz3;->e:D

    .line 2755689
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2755690
    :cond_0
    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-nez v0, :cond_1

    .line 2755691
    new-instance v0, LX/5p9;

    const-string v1, "Detected a division by zero in Animated.divide node"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755692
    :cond_1
    iget-wide v4, p0, LX/Jyx;->e:D

    div-double v2, v4, v2

    iput-wide v2, p0, LX/Jz3;->e:D

    goto :goto_1

    .line 2755693
    :cond_2
    new-instance v0, LX/5p9;

    const-string v1, "Illegal node ID set as an input for Animated.divide node"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755694
    :cond_3
    return-void
.end method
