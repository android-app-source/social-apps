.class public final LX/JaU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jad;

.field public final synthetic b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public final synthetic c:LX/JaW;


# direct methods
.method public constructor <init>(LX/JaW;LX/Jad;Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;)V
    .locals 0

    .prologue
    .line 2715517
    iput-object p1, p0, LX/JaU;->c:LX/JaW;

    iput-object p2, p0, LX/JaU;->a:LX/Jad;

    iput-object p3, p0, LX/JaU;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2715518
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2715519
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2715520
    if-eqz p1, :cond_5

    .line 2715521
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2715522
    if-eqz v0, :cond_5

    .line 2715523
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2715524
    check-cast v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2715525
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2715526
    check-cast v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2715527
    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 2715528
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2715529
    check-cast v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2715530
    iget-object v3, p0, LX/JaU;->a:LX/Jad;

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    .line 2715531
    if-lez v0, :cond_2

    .line 2715532
    iget-object v1, v3, LX/Jad;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    iget-object v1, v3, LX/Jad;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JaX;

    .line 2715533
    instance-of p1, v1, LX/JaZ;

    if-eqz p1, :cond_0

    .line 2715534
    check-cast v1, LX/JaZ;

    .line 2715535
    if-lez v0, :cond_6

    .line 2715536
    const p1, 0x7f083aba

    iput p1, v1, LX/JaZ;->c:I

    .line 2715537
    :cond_0
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2715538
    :cond_1
    invoke-virtual {v3}, LX/0gG;->kV_()V

    .line 2715539
    :cond_2
    iget-object v0, p0, LX/JaU;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    .line 2715540
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 2715541
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    .line 2715542
    :cond_6
    const p1, 0x7f083ab9

    iput p1, v1, LX/JaZ;->c:I

    goto :goto_2
.end method
