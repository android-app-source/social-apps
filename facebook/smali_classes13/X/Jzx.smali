.class public final LX/Jzx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field public final synthetic a:LX/Jzy;


# direct methods
.method public constructor <init>(LX/Jzy;)V
    .locals 0

    .prologue
    .line 2757050
    iput-object p1, p0, LX/Jzx;->a:LX/Jzy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 5

    .prologue
    .line 2757051
    iget-object v1, p0, LX/Jzx;->a:LX/Jzy;

    monitor-enter v1

    .line 2757052
    :try_start_0
    iget-object v0, p0, LX/Jzx;->a:LX/Jzy;

    iget-boolean v0, v0, LX/Jzy;->i:Z

    if-nez v0, :cond_0

    .line 2757053
    iget-object v0, p0, LX/Jzx;->a:LX/Jzy;

    iget-object v0, v0, LX/Jzy;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, LX/Jzz;->b(Landroid/location/Location;)LX/5pH;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2757054
    iget-object v0, p0, LX/Jzx;->a:LX/Jzy;

    iget-object v0, v0, LX/Jzy;->f:Landroid/os/Handler;

    iget-object v2, p0, LX/Jzx;->a:LX/Jzy;

    iget-object v2, v2, LX/Jzy;->g:Ljava/lang/Runnable;

    invoke-static {v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2757055
    iget-object v0, p0, LX/Jzx;->a:LX/Jzy;

    const/4 v2, 0x1

    .line 2757056
    iput-boolean v2, v0, LX/Jzy;->i:Z

    .line 2757057
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757058
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757059
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2757060
    return-void
.end method
