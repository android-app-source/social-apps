.class public final LX/JxA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;

.field public final b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

.field public final c:I

.field public final d:LX/Cdj;


# direct methods
.method private constructor <init>(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;ILX/Cdj;)V
    .locals 0

    .prologue
    .line 2752872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752873
    iput-object p1, p0, LX/JxA;->a:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;

    .line 2752874
    iput-object p2, p0, LX/JxA;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 2752875
    iput p3, p0, LX/JxA;->c:I

    .line 2752876
    iput-object p4, p0, LX/JxA;->d:LX/Cdj;

    .line 2752877
    return-void
.end method

.method public static b(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;)LX/JxA;
    .locals 5

    .prologue
    .line 2752878
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    move-result-object v0

    .line 2752879
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2752880
    new-instance v1, LX/CeF;

    const-string v2, "best guess is null"

    invoke-direct {v1, v0, v2}, LX/CeF;-><init>(Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;Ljava/lang/String;)V

    throw v1

    .line 2752881
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2752882
    new-instance v1, LX/CeF;

    const-string v2, "pulsar is null"

    invoke-direct {v1, v0, v2}, LX/CeF;-><init>(Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;Ljava/lang/String;)V

    throw v1

    .line 2752883
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;

    move-result-object v1

    .line 2752884
    if-nez v1, :cond_2

    .line 2752885
    new-instance v1, LX/CeF;

    const-string v2, "page is null"

    invoke-direct {v1, v0, v2}, LX/CeF;-><init>(Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;Ljava/lang/String;)V

    throw v1

    .line 2752886
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v2

    .line 2752887
    if-nez v2, :cond_3

    .line 2752888
    new-instance v1, LX/CeF;

    const-string v2, "feed unit is null"

    invoke-direct {v1, v0, v2}, LX/CeF;-><init>(Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;Ljava/lang/String;)V

    throw v1

    .line 2752889
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    if-nez v3, :cond_4

    .line 2752890
    new-instance v1, LX/CeF;

    const-string v2, "feed unit title is null"

    invoke-direct {v1, v0, v2}, LX/CeF;-><init>(Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;Ljava/lang/String;)V

    throw v1

    .line 2752891
    :cond_4
    new-instance v0, LX/JxA;

    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->j()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    move-result-object v4

    invoke-static {v4}, LX/Cdj;->from(Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;)LX/Cdj;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/JxA;-><init>(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;ILX/Cdj;)V

    return-object v0
.end method
