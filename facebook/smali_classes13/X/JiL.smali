.class public final LX/JiL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JiN;


# direct methods
.method public constructor <init>(LX/JiN;)V
    .locals 0

    .prologue
    .line 2725888
    iput-object p1, p0, LX/JiL;->a:LX/JiN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x2

    const v0, 0x7c2b7040

    invoke-static {v4, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2725889
    iget-object v1, p0, LX/JiL;->a:LX/JiN;

    iget-object v1, v1, LX/JiN;->e:LX/0Zb;

    const-string v2, "invite_friends_upsell_start"

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2725890
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2725891
    const-string v2, "messenger_people_tab_invite_friends_upsell"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2725892
    sget-object v2, LX/Ifh;->PEOPLE_TAB_INVITE_UPSELL:LX/Ifh;

    invoke-virtual {v2}, LX/Ifh;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2725893
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2725894
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-object v2, LX/3GK;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, LX/3RH;->L:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ShareType.inviteEntryPoint"

    sget-object v3, LX/Ifh;->PEOPLE_TAB_INVITE_UPSELL:LX/Ifh;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    .line 2725895
    iget-object v2, p0, LX/JiL;->a:LX/JiN;

    iget-object v2, v2, LX/JiN;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/JiL;->a:LX/JiN;

    iget-object v3, v3, LX/JiN;->b:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2725896
    const v1, -0x61f61601

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
