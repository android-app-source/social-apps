.class public final LX/K6W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/elements/SlideshowView;

.field private b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/tarot/media/TarotVideoView;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/tarot/media/TarotVideoView;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:LX/K6V;

.field public g:J

.field public final h:Landroid/os/Handler;

.field private final i:Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/elements/SlideshowView;)V
    .locals 2

    .prologue
    .line 2771777
    iput-object p1, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2771778
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/K6W;->b:Ljava/util/Queue;

    .line 2771779
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    .line 2771780
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/K6W;->d:Ljava/util/Queue;

    .line 2771781
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/K6W;->e:Ljava/util/Queue;

    .line 2771782
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/K6W;->h:Landroid/os/Handler;

    .line 2771783
    new-instance v0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;-><init>(LX/K6W;)V

    iput-object v0, p0, LX/K6W;->i:Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;

    .line 2771784
    invoke-virtual {p0}, LX/K6W;->b()V

    .line 2771785
    invoke-direct {p0}, LX/K6W;->h()V

    .line 2771786
    invoke-direct {p0}, LX/K6W;->i()V

    .line 2771787
    return-void
.end method

.method public static a$redex0(LX/K6W;LX/K6V;)V
    .locals 2

    .prologue
    .line 2771766
    if-nez p1, :cond_0

    .line 2771767
    :goto_0
    return-void

    .line 2771768
    :cond_0
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    if-eqz v0, :cond_1

    .line 2771769
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    invoke-virtual {v0}, LX/K6V;->a()V

    .line 2771770
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    iget-boolean v0, v0, LX/K6V;->a:Z

    if-eqz v0, :cond_2

    .line 2771771
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    invoke-virtual {v0}, LX/K6V;->c()V

    .line 2771772
    iget-object v0, p0, LX/K6W;->e:Ljava/util/Queue;

    iget-object v1, p0, LX/K6W;->f:LX/K6V;

    iget-object v1, v1, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2771773
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/K6W;->f:LX/K6V;

    .line 2771774
    :cond_1
    iput-object p1, p0, LX/K6W;->f:LX/K6V;

    .line 2771775
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    invoke-virtual {v0}, LX/K6V;->b()V

    goto :goto_0

    .line 2771776
    :cond_2
    iget-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    iget-object v1, p0, LX/K6W;->f:LX/K6V;

    iget-object v1, v1, LX/K6V;->b:Ljava/lang/ref/WeakReference;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private e()I
    .locals 2

    .prologue
    .line 2771765
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v1, v1, Lcom/facebook/tarot/cards/elements/SlideshowView;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    return v0
.end method

.method private f()I
    .locals 2

    .prologue
    .line 2771764
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v1, v1, Lcom/facebook/tarot/cards/elements/SlideshowView;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    return v0
.end method

.method public static g(LX/K6W;)V
    .locals 0

    .prologue
    .line 2771745
    invoke-direct {p0}, LX/K6W;->h()V

    .line 2771746
    invoke-direct {p0}, LX/K6W;->i()V

    .line 2771747
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 2771757
    iget-object v0, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v0, v0, Lcom/facebook/tarot/cards/elements/SlideshowView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2771758
    :cond_0
    return-void

    .line 2771759
    :cond_1
    :goto_0
    iget-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2771760
    iget-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2771761
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2771762
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v2, v2, Lcom/facebook/tarot/cards/elements/SlideshowView;->i:Ljava/util/List;

    invoke-direct {p0}, LX/K6W;->e()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/tarot/cards/elements/SlideshowView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2771763
    :cond_2
    iget-object v1, p0, LX/K6W;->b:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    .line 2771788
    iget-object v0, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v0, v0, Lcom/facebook/tarot/cards/elements/SlideshowView;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2771789
    :cond_0
    return-void

    .line 2771790
    :cond_1
    :goto_0
    iget-object v0, p0, LX/K6W;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2771791
    iget-object v0, p0, LX/K6W;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2771792
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2771793
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/media/TarotVideoView;

    iget-object v2, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v2, v2, Lcom/facebook/tarot/cards/elements/SlideshowView;->j:Ljava/util/List;

    invoke-direct {p0}, LX/K6W;->f()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/data/VideoData;

    iget-object v3, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v3, v3, Lcom/facebook/tarot/cards/elements/SlideshowView;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    iget-object v4, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v4, v4, Lcom/facebook/tarot/cards/elements/SlideshowView;->b:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->d()I

    move-result v4

    sget-object v5, LX/K8u;->DIGEST_COVER:LX/K8u;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/tarot/media/TarotVideoView;->a(Lcom/facebook/tarot/data/VideoData;IILX/K8u;)V

    .line 2771794
    :cond_2
    iget-object v1, p0, LX/K6W;->d:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static j(LX/K6W;)LX/K6V;
    .locals 3

    .prologue
    .line 2771748
    iget-object v0, p0, LX/K6W;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    .line 2771749
    iget-object v1, p0, LX/K6W;->d:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    .line 2771750
    add-int v2, v0, v1

    if-nez v2, :cond_0

    .line 2771751
    const/4 v0, 0x0

    .line 2771752
    :goto_0
    return-object v0

    .line 2771753
    :cond_0
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/2addr v1, v0

    rem-int v1, v2, v1

    .line 2771754
    if-ge v1, v0, :cond_1

    .line 2771755
    iget-object v0, p0, LX/K6W;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/K6V;->a(Ljava/lang/ref/WeakReference;)LX/K6V;

    move-result-object v0

    goto :goto_0

    .line 2771756
    :cond_1
    iget-object v0, p0, LX/K6W;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/K6V;->b(Ljava/lang/ref/WeakReference;)LX/K6V;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2771742
    invoke-static {p0}, LX/K6W;->g(LX/K6W;)V

    .line 2771743
    invoke-static {p0}, LX/K6W;->j(LX/K6W;)LX/K6V;

    move-result-object v0

    invoke-static {p0, v0}, LX/K6W;->a$redex0(LX/K6W;LX/K6V;)V

    .line 2771744
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2771732
    iget-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2771733
    iget-object v0, p0, LX/K6W;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2771734
    iget-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v2, v2, Lcom/facebook/tarot/cards/elements/SlideshowView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2771735
    iget-object v0, p0, LX/K6W;->c:Ljava/util/Queue;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v2, v2, Lcom/facebook/tarot/cards/elements/SlideshowView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2771736
    iget-object v0, p0, LX/K6W;->e:Ljava/util/Queue;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v2, v2, Lcom/facebook/tarot/cards/elements/SlideshowView;->f:Lcom/facebook/tarot/media/TarotVideoView;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2771737
    iget-object v0, p0, LX/K6W;->e:Ljava/util/Queue;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v2, v2, Lcom/facebook/tarot/cards/elements/SlideshowView;->g:Lcom/facebook/tarot/media/TarotVideoView;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2771738
    const/4 v0, 0x0

    iput-object v0, p0, LX/K6W;->f:LX/K6V;

    .line 2771739
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/K6W;->g:J

    .line 2771740
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K6W;->j:Z

    .line 2771741
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2771726
    iget-boolean v0, p0, LX/K6W;->j:Z

    if-nez v0, :cond_1

    .line 2771727
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    if-eqz v0, :cond_0

    .line 2771728
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    invoke-virtual {v0}, LX/K6V;->d()V

    .line 2771729
    :cond_0
    iget-object v0, p0, LX/K6W;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/K6W;->i:Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;

    const-wide/16 v2, 0xbb8

    const v4, 0x1ed2e2c

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2771730
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K6W;->j:Z

    .line 2771731
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2771721
    iget-object v0, p0, LX/K6W;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/K6W;->i:Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2771722
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    if-eqz v0, :cond_0

    .line 2771723
    iget-object v0, p0, LX/K6W;->f:LX/K6V;

    invoke-virtual {v0}, LX/K6V;->c()V

    .line 2771724
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K6W;->j:Z

    .line 2771725
    return-void
.end method
