.class public final LX/JjA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jj7;


# instance fields
.field public final synthetic a:LX/JjC;


# direct methods
.method public constructor <init>(LX/JjC;)V
    .locals 0

    .prologue
    .line 2727309
    iput-object p1, p0, LX/JjA;->a:LX/JjC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2727310
    iget-object v0, p0, LX/JjA;->a:LX/JjC;

    iget-object v0, v0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->c(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;Z)V

    .line 2727311
    iget-object v0, p0, LX/JjA;->a:LX/JjC;

    iget-object v0, v0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    invoke-static {v0}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->a(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;)V

    .line 2727312
    iget-object v0, p0, LX/JjA;->a:LX/JjC;

    iget-object v0, v0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jjj;

    iget-object v1, p0, LX/JjA;->a:LX/JjC;

    iget-object v2, v1, LX/JjC;->a:LX/3uc;

    iget-object v1, p0, LX/JjA;->a:LX/JjC;

    iget-object v1, v1, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jjx;

    .line 2727313
    sget-object v3, LX/Jjw;->a:[I

    invoke-static {v1}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object p0

    invoke-virtual {p0}, LX/Jk2;->ordinal()I

    move-result p0

    aget v3, v3, p0

    packed-switch v3, :pswitch_data_0

    .line 2727314
    const v3, 0x7f082dd1

    :goto_0
    move v1, v3

    .line 2727315
    const v3, 0x7f082dd9

    invoke-virtual {v0, v2, v1, v3}, LX/Jjj;->a(Landroid/content/Context;II)V

    .line 2727316
    return-void

    .line 2727317
    :pswitch_0
    const v3, 0x7f082dd2

    goto :goto_0

    .line 2727318
    :pswitch_1
    const v3, 0x7f082dd3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2727319
    iget-object v0, p0, LX/JjA;->a:LX/JjC;

    iget-object v0, v0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->c(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;Z)V

    .line 2727320
    iget-object v0, p0, LX/JjA;->a:LX/JjC;

    iget-object v0, v0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    invoke-static {v0}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->a(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;)V

    .line 2727321
    iget-object v0, p0, LX/JjA;->a:LX/JjC;

    iget-object v0, v0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jjy;

    iget-object v1, p0, LX/JjA;->a:LX/JjC;

    iget-object v1, v1, LX/JjC;->a:LX/3uc;

    iget-object v2, p0, LX/JjA;->a:LX/JjC;

    iget-object v2, v2, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->y:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727322
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->EVENT:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-eq v2, v3, :cond_0

    .line 2727323
    :goto_0
    return-void

    .line 2727324
    :cond_0
    iget-object v3, v0, LX/Jjy;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gt;

    const-string p0, "272523983146849"

    .line 2727325
    iput-object p0, v3, LX/0gt;->a:Ljava/lang/String;

    .line 2727326
    move-object v3, v3

    .line 2727327
    sget-object p0, LX/1x2;->MESSENGER:LX/1x2;

    invoke-virtual {v3, p0}, LX/0gt;->a(LX/1x2;)LX/0gt;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
