.class public LX/Jb7;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I

.field private c:LX/Jb8;

.field public d:Lcom/facebook/widget/text/BetterTextView;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public final i:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Jb8;)V
    .locals 1

    .prologue
    .line 2716448
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2716449
    const/16 v0, 0x3e7

    iput v0, p0, LX/Jb7;->b:I

    .line 2716450
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    .line 2716451
    iput-object p2, p0, LX/Jb7;->c:LX/Jb8;

    .line 2716452
    const/4 p2, 0x1

    .line 2716453
    const-class v0, LX/Jb7;

    invoke-static {v0, p0}, LX/Jb7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2716454
    invoke-virtual {p0, p2}, LX/Jb7;->setClickable(Z)V

    .line 2716455
    const v0, 0x7f030abc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2716456
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Jb7;->setWillNotDraw(Z)V

    .line 2716457
    iget-object v0, p0, LX/Jb7;->a:Landroid/content/res/Resources;

    const p1, 0x7f0b11ed

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Jb7;->h:I

    .line 2716458
    iget-object v0, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2716459
    iget-object v0, p0, LX/Jb7;->a:Landroid/content/res/Resources;

    const p1, 0x7f0b11f1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/Jb7;->g:I

    .line 2716460
    iget v0, p0, LX/Jb7;->h:I

    iput v0, p0, LX/Jb7;->e:I

    .line 2716461
    iget v0, p0, LX/Jb7;->h:I

    iget p1, p0, LX/Jb7;->g:I

    add-int/2addr v0, p1

    iput v0, p0, LX/Jb7;->f:I

    .line 2716462
    iget v0, p0, LX/Jb7;->e:I

    invoke-virtual {p0, v0}, LX/Jb7;->setMinimumHeight(I)V

    .line 2716463
    iget v0, p0, LX/Jb7;->f:I

    invoke-virtual {p0, v0}, LX/Jb7;->setMinimumWidth(I)V

    .line 2716464
    const v0, 0x7f0d1b71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Jb7;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2716465
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Jb7;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object p0, p1, LX/Jb7;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 2716445
    invoke-virtual {p0}, LX/Jb7;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    invoke-virtual {p0}, LX/Jb7;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget v2, p0, LX/Jb7;->h:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2716446
    iget-object v0, p0, LX/Jb7;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, LX/Jb7;->getDrawingTime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/Jb7;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 2716447
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2716442
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2716443
    iget v0, p0, LX/Jb7;->f:I

    iget v1, p0, LX/Jb7;->e:I

    invoke-virtual {p0, v0, v1}, LX/Jb7;->setMeasuredDimension(II)V

    .line 2716444
    return-void
.end method

.method public setTypeShowCount(I)V
    .locals 7

    .prologue
    .line 2716423
    iget-object v0, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    iget-object v1, p0, LX/Jb7;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0603

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2716424
    iget-object v0, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    iget-object v1, p0, LX/Jb7;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2716425
    iget-object v0, p0, LX/Jb7;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2716426
    iget-object v0, p0, LX/Jb7;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2716427
    if-eqz v1, :cond_2

    .line 2716428
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2716429
    const/16 v4, 0x2710

    if-ge v3, v4, :cond_0

    .line 2716430
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2716431
    :goto_0
    move-object v1, v3

    .line 2716432
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2716433
    new-instance v0, LX/Jb6;

    invoke-direct {v0, p0}, LX/Jb6;-><init>(LX/Jb7;)V

    invoke-virtual {p0, v0}, LX/Jb7;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2716434
    invoke-virtual {p0}, LX/Jb7;->invalidate()V

    .line 2716435
    return-void

    .line 2716436
    :cond_0
    const v4, 0xf4240

    if-ge v3, v4, :cond_1

    .line 2716437
    int-to-double v3, v3

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-double v3, v3

    .line 2716438
    const-string v5, "%.0fk"

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2716439
    :cond_1
    int-to-double v3, v3

    const-wide v5, 0x40c3880000000000L    # 10000.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-double v3, v3

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    div-double/2addr v3, v5

    .line 2716440
    const-string v5, "%.2fm"

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2716441
    :cond_2
    if-nez v1, :cond_3

    const-string v3, ""

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
