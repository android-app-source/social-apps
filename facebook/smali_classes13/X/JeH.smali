.class public LX/JeH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Jfw;

.field public final b:LX/Jdq;

.field public final c:LX/03V;

.field public d:Lcom/facebook/user/model/User;

.field public e:LX/JeD;

.field public final f:Landroid/support/v7/widget/RecyclerView;

.field public final g:Landroid/widget/ProgressBar;

.field public final h:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Jfw;LX/Jdr;LX/03V;Landroid/support/v7/widget/RecyclerView;Landroid/widget/ProgressBar;Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLX/JeD;Z)V
    .locals 2
    .param p5    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/widget/ProgressBar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/JeD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720501
    new-instance v0, LX/JeG;

    invoke-direct {v0, p0}, LX/JeG;-><init>(LX/JeH;)V

    iput-object v0, p0, LX/JeH;->h:LX/0Vd;

    .line 2720502
    iput-object p2, p0, LX/JeH;->a:LX/Jfw;

    .line 2720503
    iput-object p4, p0, LX/JeH;->c:LX/03V;

    .line 2720504
    iput-object p10, p0, LX/JeH;->e:LX/JeD;

    .line 2720505
    new-instance v0, LX/JeE;

    invoke-direct {v0, p0}, LX/JeE;-><init>(LX/JeH;)V

    invoke-virtual {p3, v0, p8, p11}, LX/Jdr;->a(LX/JeE;Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)LX/Jdq;

    move-result-object v0

    iput-object v0, p0, LX/JeH;->b:LX/Jdq;

    .line 2720506
    iput-object p5, p0, LX/JeH;->f:Landroid/support/v7/widget/RecyclerView;

    .line 2720507
    iput-object p6, p0, LX/JeH;->g:Landroid/widget/ProgressBar;

    .line 2720508
    iput-object p7, p0, LX/JeH;->d:Lcom/facebook/user/model/User;

    .line 2720509
    iget-object v1, p0, LX/JeH;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz p9, :cond_0

    new-instance v0, LX/62V;

    invoke-direct {v0, p1}, LX/62V;-><init>(Landroid/content/Context;)V

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2720510
    iget-object v0, p0, LX/JeH;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/JeH;->b:LX/Jdq;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2720511
    iget-object v0, p0, LX/JeH;->f:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/JeF;

    invoke-direct {v1, p0}, LX/JeF;-><init>(LX/JeH;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2720512
    invoke-direct {p0}, LX/JeH;->b()V

    .line 2720513
    return-void

    .line 2720514
    :cond_0
    new-instance v0, LX/1P1;

    invoke-direct {v0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private b()V
    .locals 10

    .prologue
    .line 2720518
    iget-object v0, p0, LX/JeH;->d:Lcom/facebook/user/model/User;

    .line 2720519
    iget-object v1, v0, Lcom/facebook/user/model/User;->v:LX/0Px;

    move-object v0, v1

    .line 2720520
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JeH;->d:Lcom/facebook/user/model/User;

    .line 2720521
    iget-object v1, v0, Lcom/facebook/user/model/User;->v:LX/0Px;

    move-object v0, v1

    .line 2720522
    sget-object v1, LX/4nX;->USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2720523
    iget-object v3, p0, LX/JeH;->f:Landroid/support/v7/widget/RecyclerView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2720524
    iget-object v3, p0, LX/JeH;->g:Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2720525
    iget-object v3, p0, LX/JeH;->a:LX/Jfw;

    iget-object v4, p0, LX/JeH;->d:Lcom/facebook/user/model/User;

    .line 2720526
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2720527
    iget-object v5, p0, LX/JeH;->h:LX/0Vd;

    .line 2720528
    new-instance v6, LX/CKm;

    invoke-direct {v6}, LX/CKm;-><init>()V

    move-object v6, v6

    .line 2720529
    const-string v7, "userId"

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2720530
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    sget-object v7, LX/0zS;->a:LX/0zS;

    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0xe10

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    const/4 v7, 0x1

    .line 2720531
    iput-boolean v7, v6, LX/0zO;->p:Z

    .line 2720532
    move-object v6, v6

    .line 2720533
    iget-object v7, v3, LX/Jfw;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2720534
    iget-object v7, v3, LX/Jfw;->b:LX/1Ck;

    const-string v8, "load_topics"

    invoke-virtual {v7, v8, v6, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2720535
    :goto_0
    return-void

    .line 2720536
    :cond_0
    invoke-static {p0}, LX/JeH;->e(LX/JeH;)V

    .line 2720537
    iget-object v0, p0, LX/JeH;->b:LX/Jdq;

    const/4 v1, 0x0

    iget-object v2, p0, LX/JeH;->d:Lcom/facebook/user/model/User;

    invoke-virtual {v0, v1, v2}, LX/Jdq;->a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;Lcom/facebook/user/model/User;)V

    goto :goto_0
.end method

.method public static e(LX/JeH;)V
    .locals 2

    .prologue
    .line 2720515
    iget-object v0, p0, LX/JeH;->g:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2720516
    iget-object v0, p0, LX/JeH;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2720517
    return-void
.end method
