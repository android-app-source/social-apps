.class public LX/K9J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/K9J;


# instance fields
.field public final a:LX/0sa;

.field public final b:LX/0rq;

.field public final c:LX/0se;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0tI;

.field public final f:LX/0ad;

.field public final g:LX/0sU;

.field public final h:LX/0tG;

.field private final i:LX/A5q;

.field private final j:LX/0wo;

.field public final k:LX/0sX;

.field public final l:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0se;LX/0Or;LX/0ad;LX/0tI;LX/0sU;LX/0tG;LX/A5q;LX/0wo;LX/0sX;LX/0tQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0rq;",
            "LX/0se;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0ad;",
            "LX/0tI;",
            "LX/0sU;",
            "LX/0tG;",
            "LX/A5q;",
            "LX/0wo;",
            "LX/0sX;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2776947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776948
    iput-object p1, p0, LX/K9J;->a:LX/0sa;

    .line 2776949
    iput-object p2, p0, LX/K9J;->b:LX/0rq;

    .line 2776950
    iput-object p3, p0, LX/K9J;->c:LX/0se;

    .line 2776951
    iput-object p4, p0, LX/K9J;->d:LX/0Or;

    .line 2776952
    iput-object p6, p0, LX/K9J;->e:LX/0tI;

    .line 2776953
    iput-object p5, p0, LX/K9J;->f:LX/0ad;

    .line 2776954
    iput-object p7, p0, LX/K9J;->g:LX/0sU;

    .line 2776955
    iput-object p8, p0, LX/K9J;->h:LX/0tG;

    .line 2776956
    iput-object p9, p0, LX/K9J;->i:LX/A5q;

    .line 2776957
    iput-object p10, p0, LX/K9J;->j:LX/0wo;

    .line 2776958
    iput-object p11, p0, LX/K9J;->k:LX/0sX;

    .line 2776959
    iput-object p12, p0, LX/K9J;->l:LX/0tQ;

    .line 2776960
    return-void
.end method

.method public static a(LX/0QB;)LX/K9J;
    .locals 3

    .prologue
    .line 2776906
    sget-object v0, LX/K9J;->m:LX/K9J;

    if-nez v0, :cond_1

    .line 2776907
    const-class v1, LX/K9J;

    monitor-enter v1

    .line 2776908
    :try_start_0
    sget-object v0, LX/K9J;->m:LX/K9J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2776909
    if-eqz v2, :cond_0

    .line 2776910
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/K9J;->b(LX/0QB;)LX/K9J;

    move-result-object v0

    sput-object v0, LX/K9J;->m:LX/K9J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2776911
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2776912
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2776913
    :cond_1
    sget-object v0, LX/K9J;->m:LX/K9J;

    return-object v0

    .line 2776914
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2776915
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/K9J;
    .locals 13

    .prologue
    .line 2776945
    new-instance v0, LX/K9J;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    const/16 v4, 0x12e4

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v6

    check-cast v6, LX/0tI;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v7

    check-cast v7, LX/0sU;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v8

    check-cast v8, LX/0tG;

    invoke-static {p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v9

    check-cast v9, LX/A5q;

    invoke-static {p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v10

    check-cast v10, LX/0wo;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v11

    check-cast v11, LX/0sX;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v12

    check-cast v12, LX/0tQ;

    invoke-direct/range {v0 .. v12}, LX/K9J;-><init>(LX/0sa;LX/0rq;LX/0se;LX/0Or;LX/0ad;LX/0tI;LX/0sU;LX/0tG;LX/A5q;LX/0wo;LX/0sX;LX/0tQ;)V

    .line 2776946
    return-object v0
.end method


# virtual methods
.method public final a(LX/Fsp;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zO;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fsp;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I)",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2776916
    new-instance v2, LX/K9Q;

    invoke-direct {v2}, LX/K9Q;-><init>()V

    move-object v2, v2

    .line 2776917
    const-string v3, "nodeId"

    .line 2776918
    iget-wide v8, p1, LX/Fsp;->a:J

    move-wide v4, v8

    .line 2776919
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "timeline_videos_size"

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "angora_attachment_cover_image_size"

    iget-object v5, p0, LX/K9J;->a:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "reading_attachment_profile_image_width"

    iget-object v5, p0, LX/K9J;->a:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "reading_attachment_profile_image_height"

    iget-object v5, p0, LX/K9J;->a:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "goodwill_small_accent_image"

    iget-object v5, p0, LX/K9J;->b:LX/0rq;

    invoke-virtual {v5}, LX/0rq;->h()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "image_large_aspect_height"

    iget-object v5, p0, LX/K9J;->a:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "image_large_aspect_width"

    iget-object v5, p0, LX/K9J;->a:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "num_faceboxes_and_tags"

    iget-object v5, p0, LX/K9J;->a:LX/0sa;

    .line 2776920
    iget-object v6, v5, LX/0sa;->b:Ljava/lang/Integer;

    move-object v5, v6

    .line 2776921
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v4, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v4, "include_replies_in_total_count"

    iget-object v5, p0, LX/K9J;->f:LX/0ad;

    sget-short v6, LX/0wg;->i:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "action_location"

    sget-object v5, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v5}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "enable_download"

    iget-object v5, p0, LX/K9J;->l:LX/0tQ;

    invoke-virtual {v5}, LX/0tQ;->c()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "automatic_photo_captioning_enabled"

    iget-object v5, p0, LX/K9J;->k:LX/0sX;

    invoke-virtual {v5}, LX/0sX;->a()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2776922
    iget-object v3, p0, LX/K9J;->d:LX/0Or;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/K9J;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    .line 2776923
    const/4 v3, 0x1

    move v3, v3

    .line 2776924
    if-eqz v3, :cond_0

    .line 2776925
    const-string v3, "scrubbing"

    const-string v4, "MPEG_DASH"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2776926
    :cond_0
    iget-object v3, p1, LX/Fsp;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2776927
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2776928
    const-string v3, "timeline_videos_after"

    .line 2776929
    iget-object v4, p1, LX/Fsp;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2776930
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2776931
    :cond_1
    iget-object v3, p0, LX/K9J;->c:LX/0se;

    iget-object v4, p0, LX/K9J;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->c()LX/0wF;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2776932
    iget-object v3, p0, LX/K9J;->h:LX/0tG;

    invoke-virtual {v3, v2}, LX/0tG;->a(LX/0gW;)V

    .line 2776933
    iget-object v3, p0, LX/K9J;->g:LX/0sU;

    const-string v4, "PROFILE"

    invoke-virtual {v3, v2, v4}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 2776934
    iget-object v3, p0, LX/K9J;->e:LX/0tI;

    invoke-virtual {v3, v2}, LX/0tI;->a(LX/0gW;)V

    .line 2776935
    invoke-static {v2}, LX/0wo;->a(LX/0gW;)V

    .line 2776936
    const/4 v3, 0x1

    .line 2776937
    iput-boolean v3, v2, LX/0gW;->l:Z

    .line 2776938
    move-object v0, v2

    .line 2776939
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2776940
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2776941
    move-object v0, v0

    .line 2776942
    iput p3, v0, LX/0zO;->B:I

    .line 2776943
    move-object v0, v0

    .line 2776944
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    return-object v0
.end method
