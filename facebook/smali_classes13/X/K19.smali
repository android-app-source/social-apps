.class public LX/K19;
.super Landroid/widget/EditText;
.source ""


# static fields
.field public static final t:Landroid/text/method/KeyListener;


# instance fields
.field private final a:Landroid/view/inputmethod/InputMethodManager;

.field public b:Z

.field private c:Z

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/TextWatcher;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/K18;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field private k:Z

.field public l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/K1I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/K15;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final q:LX/K17;

.field private r:Z

.field private s:LX/9nY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2761296
    invoke-static {}, Landroid/text/method/QwertyKeyListener;->getInstanceForFullKeyboard()Landroid/text/method/QwertyKeyListener;

    move-result-object v0

    sput-object v0, LX/K19;->t:Landroid/text/method/KeyListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2761297
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2761298
    iput-boolean v2, p0, LX/K19;->r:Z

    .line 2761299
    invoke-virtual {p0, v2}, LX/K19;->setFocusableInTouchMode(Z)V

    .line 2761300
    invoke-virtual {p0}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, LX/K19;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 2761301
    invoke-virtual {p0}, LX/K19;->getGravity()I

    move-result v0

    const v1, 0x800007

    and-int/2addr v0, v1

    iput v0, p0, LX/K19;->d:I

    .line 2761302
    invoke-virtual {p0}, LX/K19;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x70

    iput v0, p0, LX/K19;->e:I

    .line 2761303
    iput v2, p0, LX/K19;->f:I

    .line 2761304
    iput v2, p0, LX/K19;->g:I

    .line 2761305
    iput-boolean v2, p0, LX/K19;->b:Z

    .line 2761306
    iput-boolean v2, p0, LX/K19;->c:Z

    .line 2761307
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K19;->l:Z

    .line 2761308
    iput-boolean v2, p0, LX/K19;->m:Z

    .line 2761309
    iput-object v3, p0, LX/K19;->h:Ljava/util/ArrayList;

    .line 2761310
    iput-object v3, p0, LX/K19;->i:LX/K18;

    .line 2761311
    invoke-virtual {p0}, LX/K19;->getInputType()I

    move-result v0

    iput v0, p0, LX/K19;->j:I

    .line 2761312
    new-instance v0, LX/K17;

    invoke-direct {v0}, LX/K17;-><init>()V

    iput-object v0, p0, LX/K19;->q:LX/K17;

    .line 2761313
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2761314
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0}, LX/K19;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    invoke-interface {v1, v0, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    .line 2761315
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 2761316
    const-class v2, Landroid/text/style/ForegroundColorSpan;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, Landroid/text/style/BackgroundColorSpan;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, Landroid/text/style/AbsoluteSizeSpan;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, LX/K0t;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, LX/K0x;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2761317
    :cond_0
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-interface {v2, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 2761318
    :cond_1
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-interface {v2, v3}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v2

    and-int/lit8 v2, v2, 0x21

    const/16 v3, 0x21

    if-ne v2, v3, :cond_2

    .line 2761319
    aget-object v2, v1, v0

    .line 2761320
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v3

    aget-object v4, v1, v0

    invoke-interface {v3, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    .line 2761321
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v4

    aget-object v5, v1, v0

    invoke-interface {v4, v5}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    .line 2761322
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v5

    aget-object v6, v1, v0

    invoke-interface {v5, v6}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v5

    .line 2761323
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v6

    aget-object v7, v1, v0

    invoke-interface {v6, v7}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 2761324
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-static {v6, p1, v3, v4}, LX/K19;->a(Landroid/text/Editable;Landroid/text/SpannableStringBuilder;II)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2761325
    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2761326
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2761327
    :cond_3
    return-void
.end method

.method private static a(Landroid/text/Editable;Landroid/text/SpannableStringBuilder;II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2761328
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-gt p2, v1, :cond_0

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-le p3, v1, :cond_2

    .line 2761329
    :cond_0
    :goto_0
    return v0

    .line 2761330
    :cond_1
    add-int/lit8 p2, p2, 0x1

    :cond_2
    if-ge p2, p3, :cond_3

    .line 2761331
    invoke-interface {p0, p2}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 2761332
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 2761333
    iget-object v0, p0, LX/K19;->a:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2761334
    iget-object v0, p0, LX/K19;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, LX/K19;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2761335
    return-void
.end method

.method private getOrCreateReactViewBackground()LX/9nY;
    .locals 5

    .prologue
    .line 2761336
    iget-object v0, p0, LX/K19;->s:LX/9nY;

    if-nez v0, :cond_0

    .line 2761337
    new-instance v0, LX/9nY;

    invoke-direct {v0}, LX/9nY;-><init>()V

    iput-object v0, p0, LX/K19;->s:LX/9nY;

    .line 2761338
    invoke-virtual {p0}, LX/K19;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2761339
    const/4 v1, 0x0

    invoke-super {p0, v1}, Landroid/widget/EditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2761340
    if-nez v0, :cond_1

    .line 2761341
    iget-object v0, p0, LX/K19;->s:LX/9nY;

    invoke-super {p0, v0}, Landroid/widget/EditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2761342
    :cond_0
    :goto_0
    iget-object v0, p0, LX/K19;->s:LX/9nY;

    return-object v0

    .line 2761343
    :cond_1
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    iget-object v4, p0, LX/K19;->s:LX/9nY;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2761344
    invoke-super {p0, v1}, Landroid/widget/EditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private getTextWatcherDelegator()LX/K18;
    .locals 2

    .prologue
    .line 2761345
    iget-object v0, p0, LX/K19;->i:LX/K18;

    if-nez v0, :cond_0

    .line 2761346
    new-instance v0, LX/K18;

    invoke-direct {v0, p0}, LX/K18;-><init>(LX/K19;)V

    iput-object v0, p0, LX/K19;->i:LX/K18;

    .line 2761347
    :cond_0
    iget-object v0, p0, LX/K19;->i:LX/K18;

    return-object v0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 2761348
    invoke-virtual {p0}, LX/K19;->getInputType()I

    move-result v0

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v4, 0x6

    .line 2761349
    iget-object v5, p0, LX/K19;->n:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 2761350
    iget-object v6, p0, LX/K19;->n:Ljava/lang/String;

    const/4 v5, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v5, :pswitch_data_0

    :cond_1
    move v0, v4

    .line 2761351
    :goto_1
    :pswitch_0
    iget-boolean v1, p0, LX/K19;->m:Z

    if-eqz v1, :cond_2

    .line 2761352
    const/high16 v1, 0x2000000

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/K19;->setImeOptions(I)V

    .line 2761353
    :goto_2
    return-void

    .line 2761354
    :sswitch_0
    const-string v7, "go"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :sswitch_1
    const-string v7, "next"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v1

    goto :goto_0

    :sswitch_2
    const-string v7, "none"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v0

    goto :goto_0

    :sswitch_3
    const-string v7, "previous"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v2

    goto :goto_0

    :sswitch_4
    const-string v7, "search"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v3

    goto :goto_0

    :sswitch_5
    const-string v7, "send"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x5

    goto :goto_0

    :sswitch_6
    const-string v7, "done"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v4

    goto :goto_0

    .line 2761355
    :pswitch_1
    const/4 v0, 0x5

    .line 2761356
    goto :goto_1

    :pswitch_2
    move v0, v1

    .line 2761357
    goto :goto_1

    .line 2761358
    :pswitch_3
    const/4 v0, 0x7

    .line 2761359
    goto :goto_1

    :pswitch_4
    move v0, v2

    .line 2761360
    goto :goto_1

    :pswitch_5
    move v0, v3

    .line 2761361
    goto :goto_1

    :pswitch_6
    move v0, v4

    .line 2761362
    goto :goto_1

    .line 2761363
    :cond_2
    invoke-virtual {p0, v0}, LX/K19;->setImeOptions(I)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x4bec4509 -> :sswitch_3
        -0x36059a58 -> :sswitch_4
        0xce8 -> :sswitch_0
        0x2f2382 -> :sswitch_6
        0x338af3 -> :sswitch_1
        0x33af38 -> :sswitch_2
        0x35cf88 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2761364
    invoke-virtual {p0}, LX/K19;->getInputType()I

    move-result v0

    iget v1, p0, LX/K19;->j:I

    if-eq v0, v1, :cond_0

    .line 2761365
    iget v0, p0, LX/K19;->j:I

    invoke-virtual {p0, v0}, LX/K19;->setInputType(I)V

    .line 2761366
    :cond_0
    return-void
.end method

.method public final a(FI)V
    .locals 1

    .prologue
    .line 2761369
    invoke-direct {p0}, LX/K19;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/9nY;->a(FI)V

    .line 2761370
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 2761367
    invoke-direct {p0}, LX/K19;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/9nY;->a(IF)V

    .line 2761368
    return-void
.end method

.method public final a(IFF)V
    .locals 1

    .prologue
    .line 2761410
    invoke-direct {p0}, LX/K19;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/9nY;->a(IFF)V

    .line 2761411
    return-void
.end method

.method public final a(LX/K10;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2761397
    iget v0, p1, LX/K10;->b:I

    move v0, v0

    .line 2761398
    iput v0, p0, LX/K19;->g:I

    .line 2761399
    iget v0, p0, LX/K19;->g:I

    iget v1, p0, LX/K19;->f:I

    if-ge v0, v1, :cond_0

    .line 2761400
    :goto_0
    return-void

    .line 2761401
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    .line 2761402
    iget-object v1, p1, LX/K10;->a:Landroid/text/Spannable;

    move-object v1, v1

    .line 2761403
    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2761404
    invoke-direct {p0, v0}, LX/K19;->a(Landroid/text/SpannableStringBuilder;)V

    .line 2761405
    iget-boolean v1, p1, LX/K10;->c:Z

    move v1, v1

    .line 2761406
    iput-boolean v1, p0, LX/K19;->k:Z

    .line 2761407
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/K19;->b:Z

    .line 2761408
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0}, LX/K19;->length()I

    move-result v2

    invoke-interface {v1, v3, v2, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2761409
    iput-boolean v3, p0, LX/K19;->b:Z

    goto :goto_0
.end method

.method public final addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2761392
    iget-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2761393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    .line 2761394
    invoke-direct {p0}, LX/K19;->getTextWatcherDelegator()LX/K18;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2761395
    :cond_0
    iget-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2761396
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2761388
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K19;->c:Z

    .line 2761389
    invoke-virtual {p0}, LX/K19;->requestFocus()Z

    .line 2761390
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K19;->c:Z

    .line 2761391
    return-void
.end method

.method public final clearFocus()V
    .locals 1

    .prologue
    .line 2761384
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/K19;->setFocusableInTouchMode(Z)V

    .line 2761385
    invoke-super {p0}, Landroid/widget/EditText;->clearFocus()V

    .line 2761386
    invoke-direct {p0}, LX/K19;->g()V

    .line 2761387
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2761215
    iget v0, p0, LX/K19;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/K19;->f:I

    return v0
.end method

.method public getBlurOnSubmit()Z
    .locals 1

    .prologue
    .line 2761383
    iget-boolean v0, p0, LX/K19;->l:Z

    return v0
.end method

.method public getDisableFullscreenUI()Z
    .locals 1

    .prologue
    .line 2761382
    iget-boolean v0, p0, LX/K19;->m:Z

    return v0
.end method

.method public getReturnKeyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761381
    iget-object v0, p0, LX/K19;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getStagedInputType()I
    .locals 1

    .prologue
    .line 2761380
    iget v0, p0, LX/K19;->j:I

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2761371
    iget-boolean v0, p0, LX/K19;->k:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 2761372
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2761373
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2761374
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 2761375
    invoke-virtual {v3}, LX/K13;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 2761376
    invoke-virtual {p0}, LX/K19;->invalidate()V

    .line 2761377
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2761378
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2761379
    return-void
.end method

.method public final isLayoutRequested()Z
    .locals 1

    .prologue
    .line 2761285
    iget-object v0, p0, LX/K19;->p:LX/K15;

    if-eqz v0, :cond_0

    .line 2761286
    invoke-direct {p0}, LX/K19;->h()Z

    move-result v0

    .line 2761287
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2c

    const v3, -0x5b9ad437

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2761288
    invoke-super {p0}, Landroid/widget/EditText;->onAttachedToWindow()V

    .line 2761289
    iget-boolean v0, p0, LX/K19;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2761290
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2761291
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, LX/K13;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2761292
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 2761293
    invoke-virtual {v4}, LX/K13;->d()V

    .line 2761294
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2761295
    :cond_0
    const v0, 0x42515da6

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2c

    const v3, -0x2b13b758

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2761178
    invoke-super {p0}, Landroid/widget/EditText;->onDetachedFromWindow()V

    .line 2761179
    iget-boolean v0, p0, LX/K19;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2761180
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2761181
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, LX/K13;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2761182
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 2761183
    invoke-virtual {v4}, LX/K13;->b()V

    .line 2761184
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2761185
    :cond_0
    const v0, -0x2e500b92

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2761231
    invoke-super {p0}, Landroid/widget/EditText;->onFinishTemporaryDetach()V

    .line 2761232
    iget-boolean v0, p0, LX/K19;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2761233
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2761234
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2761235
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 2761236
    invoke-virtual {v3}, LX/K13;->e()V

    .line 2761237
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2761238
    :cond_0
    return-void
.end method

.method public final onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, -0x6198bd5c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2761227
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 2761228
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/K19;->o:LX/K1I;

    if-eqz v1, :cond_0

    .line 2761229
    iget-object v1, p0, LX/K19;->o:LX/K1I;

    invoke-virtual {p0}, LX/K19;->getSelectionStart()I

    move-result v2

    invoke-virtual {p0}, LX/K19;->getSelectionEnd()I

    move-result v3

    invoke-interface {v1, v2, v3}, LX/K1I;->a(II)V

    .line 2761230
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7046f5de

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2761223
    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, LX/K19;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2761224
    invoke-direct {p0}, LX/K19;->g()V

    .line 2761225
    const/4 v0, 0x1

    .line 2761226
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 2761220
    iget-object v0, p0, LX/K19;->p:LX/K15;

    if-eqz v0, :cond_0

    .line 2761221
    iget-object v0, p0, LX/K19;->p:LX/K15;

    invoke-interface {v0}, LX/K15;->a()V

    .line 2761222
    :cond_0
    return-void
.end method

.method public final onSelectionChanged(II)V
    .locals 1

    .prologue
    .line 2761216
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 2761217
    iget-object v0, p0, LX/K19;->o:LX/K1I;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K19;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2761218
    iget-object v0, p0, LX/K19;->o:LX/K1I;

    invoke-interface {v0, p1, p2}, LX/K1I;->a(II)V

    .line 2761219
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2761207
    invoke-super {p0}, Landroid/widget/EditText;->onStartTemporaryDetach()V

    .line 2761208
    iget-boolean v0, p0, LX/K19;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2761209
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2761210
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2761211
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 2761212
    invoke-virtual {v3}, LX/K13;->c()V

    .line 2761213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2761214
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x4e0f6f9b    # 6.0161402E8f

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2761199
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2761200
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x1b3ae5f6

    invoke-static {v2, v0}, LX/02F;->a(II)V

    return v1

    .line 2761201
    :pswitch_1
    iput-boolean v2, p0, LX/K19;->r:Z

    .line 2761202
    invoke-virtual {p0}, LX/K19;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 2761203
    :pswitch_2
    iget-boolean v1, p0, LX/K19;->r:Z

    if-eqz v1, :cond_0

    .line 2761204
    invoke-virtual {p0, v3}, LX/K19;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v2}, LX/K19;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v3}, LX/K19;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v2}, LX/K19;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2761205
    invoke-virtual {p0}, LX/K19;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2761206
    :cond_1
    iput-boolean v4, p0, LX/K19;->r:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2761193
    iget-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2761194
    iget-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2761195
    iget-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2761196
    const/4 v0, 0x0

    iput-object v0, p0, LX/K19;->h:Ljava/util/ArrayList;

    .line 2761197
    invoke-direct {p0}, LX/K19;->getTextWatcherDelegator()LX/K18;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2761198
    :cond_0
    return-void
.end method

.method public final requestFocus(ILandroid/graphics/Rect;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2761186
    invoke-virtual {p0}, LX/K19;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2761187
    :goto_0
    return v0

    .line 2761188
    :cond_0
    iget-boolean v1, p0, LX/K19;->c:Z

    if-nez v1, :cond_1

    .line 2761189
    const/4 v0, 0x0

    goto :goto_0

    .line 2761190
    :cond_1
    invoke-virtual {p0, v0}, LX/K19;->setFocusableInTouchMode(Z)V

    .line 2761191
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 2761192
    invoke-direct {p0}, LX/K19;->f()Z

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 2761175
    if-nez p1, :cond_0

    iget-object v0, p0, LX/K19;->s:LX/9nY;

    if-eqz v0, :cond_1

    .line 2761176
    :cond_0
    invoke-direct {p0}, LX/K19;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(I)V

    .line 2761177
    :cond_1
    return-void
.end method

.method public setBlurOnSubmit(Z)V
    .locals 0

    .prologue
    .line 2761239
    iput-boolean p1, p0, LX/K19;->l:Z

    .line 2761240
    return-void
.end method

.method public setBorderRadius(F)V
    .locals 1

    .prologue
    .line 2761241
    invoke-direct {p0}, LX/K19;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(F)V

    .line 2761242
    return-void
.end method

.method public setBorderStyle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2761243
    invoke-direct {p0}, LX/K19;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(Ljava/lang/String;)V

    .line 2761244
    return-void
.end method

.method public setContentSizeWatcher(LX/K15;)V
    .locals 0

    .prologue
    .line 2761245
    iput-object p1, p0, LX/K19;->p:LX/K15;

    .line 2761246
    return-void
.end method

.method public setDisableFullscreenUI(Z)V
    .locals 0

    .prologue
    .line 2761247
    iput-boolean p1, p0, LX/K19;->m:Z

    .line 2761248
    invoke-direct {p0}, LX/K19;->i()V

    .line 2761249
    return-void
.end method

.method public setGravityHorizontal(I)V
    .locals 2

    .prologue
    .line 2761250
    if-nez p1, :cond_0

    .line 2761251
    iget p1, p0, LX/K19;->d:I

    .line 2761252
    :cond_0
    invoke-virtual {p0}, LX/K19;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, -0x8

    const v1, -0x800008

    and-int/2addr v0, v1

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, LX/K19;->setGravity(I)V

    .line 2761253
    return-void
.end method

.method public setGravityVertical(I)V
    .locals 1

    .prologue
    .line 2761254
    if-nez p1, :cond_0

    .line 2761255
    iget p1, p0, LX/K19;->e:I

    .line 2761256
    :cond_0
    invoke-virtual {p0}, LX/K19;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, -0x71

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, LX/K19;->setGravity(I)V

    .line 2761257
    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .prologue
    .line 2761258
    invoke-super {p0}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 2761259
    invoke-super {p0, p1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2761260
    iput p1, p0, LX/K19;->j:I

    .line 2761261
    invoke-super {p0, v0}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2761262
    iget-object v0, p0, LX/K19;->q:LX/K17;

    .line 2761263
    iput p1, v0, LX/K17;->a:I

    .line 2761264
    iget-object v0, p0, LX/K19;->q:LX/K17;

    invoke-virtual {p0, v0}, LX/K19;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 2761265
    return-void
.end method

.method public setReturnKeyType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2761266
    iput-object p1, p0, LX/K19;->n:Ljava/lang/String;

    .line 2761267
    invoke-direct {p0}, LX/K19;->i()V

    .line 2761268
    return-void
.end method

.method public final setSelection(II)V
    .locals 2

    .prologue
    .line 2761269
    iget v0, p0, LX/K19;->g:I

    iget v1, p0, LX/K19;->f:I

    if-ge v0, v1, :cond_0

    .line 2761270
    :goto_0
    return-void

    .line 2761271
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0
.end method

.method public setSelectionWatcher(LX/K1I;)V
    .locals 0

    .prologue
    .line 2761272
    iput-object p1, p0, LX/K19;->o:LX/K1I;

    .line 2761273
    return-void
.end method

.method public setStagedInputType(I)V
    .locals 0

    .prologue
    .line 2761274
    iput p1, p0, LX/K19;->j:I

    .line 2761275
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2761276
    iget-boolean v0, p0, LX/K19;->k:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 2761277
    invoke-virtual {p0}, LX/K19;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2761278
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2761279
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 2761280
    invoke-virtual {v3}, LX/K13;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 2761281
    const/4 v0, 0x1

    .line 2761282
    :goto_1
    return v0

    .line 2761283
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2761284
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_1
.end method
