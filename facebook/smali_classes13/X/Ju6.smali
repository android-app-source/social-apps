.class public LX/Ju6;
.super LX/I4B;
.source ""


# instance fields
.field public H:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/Clh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/Clr;

.field public M:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

.field public N:LX/Ju8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2748479
    invoke-direct {p0, p1}, LX/I4B;-><init>(Landroid/content/Context;)V

    .line 2748480
    iget-object v1, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Ju6;

    invoke-static {p1}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v2

    check-cast v2, LX/Crz;

    invoke-static {p1}, LX/Clh;->b(LX/0QB;)LX/Clh;

    move-result-object v3

    check-cast v3, LX/Clh;

    invoke-static {p1}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v4

    check-cast v4, LX/Chi;

    const/16 v0, 0x3226

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, LX/Ju6;->H:LX/Crz;

    iput-object v3, p0, LX/Ju6;->I:LX/Clh;

    iput-object v4, p0, LX/Ju6;->J:LX/Chi;

    iput-object p1, p0, LX/Ju6;->K:LX/0Ot;

    .line 2748481
    return-void
.end method


# virtual methods
.method public final synthetic a(I)LX/I4B;
    .locals 1

    .prologue
    .line 2748501
    invoke-super {p0, p1}, LX/I4B;->a(I)LX/I4B;

    .line 2748502
    move-object v0, p0

    .line 2748503
    return-object v0
.end method

.method public final a(LX/B5a;Landroid/content/Context;)LX/I4B;
    .locals 3

    .prologue
    .line 2748504
    invoke-interface {p1}, LX/B5a;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2748505
    :cond_0
    :goto_0
    return-object p0

    .line 2748506
    :cond_1
    sget-object v0, LX/Ju5;->a:[I

    invoke-interface {p1}, LX/B5a;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2748507
    invoke-super {p0, p1, p2}, LX/I4B;->a(LX/B5a;Landroid/content/Context;)LX/I4B;

    move-result-object p0

    goto :goto_0

    .line 2748508
    :pswitch_0
    invoke-interface {p1}, LX/B5a;->em_()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/B5a;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2748509
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    iget-object v1, p0, LX/I4B;->s:Ljava/lang/String;

    iget-object v2, p0, LX/Ju6;->K:LX/0Ot;

    invoke-static {p1, v1, v2}, LX/IXf;->a(LX/B5a;Ljava/lang/String;LX/0Ot;)LX/Clw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2748510
    :pswitch_1
    invoke-interface {p1}, LX/B5a;->j()LX/8Ys;

    move-result-object v0

    invoke-static {v0}, LX/I4B;->a(LX/8Ys;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2748511
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-static {p1}, LX/IXf;->a(LX/B5a;)LX/Cm4;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic a(Ljava/lang/String;)LX/I4B;
    .locals 1

    .prologue
    .line 2748544
    invoke-super {p0, p1}, LX/I4B;->a(Ljava/lang/String;)LX/I4B;

    .line 2748545
    move-object v0, p0

    .line 2748546
    return-object v0
.end method

.method public final b()LX/Clo;
    .locals 8

    .prologue
    .line 2748512
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2748513
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2748514
    iget-object v0, p0, LX/I4B;->y:LX/Clr;

    if-eqz v0, :cond_0

    .line 2748515
    iget-object v0, p0, LX/I4B;->y:LX/Clr;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2748516
    :cond_0
    iget-object v0, p0, LX/Ju6;->M:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    if-eqz v0, :cond_1

    .line 2748517
    new-instance v0, LX/JuA;

    iget-object v3, p0, LX/Ju6;->M:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    invoke-direct {v0, v3}, LX/JuA;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2748518
    :cond_1
    iget-object v0, p0, LX/I4B;->v:LX/8Z4;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/I4B;->v:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2748519
    new-instance v0, LX/CmL;

    iget-object v3, p0, LX/I4B;->v:LX/8Z4;

    invoke-direct {v0, v3}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v3, LX/Clb;->TITLE:LX/Clb;

    .line 2748520
    iput-object v3, v0, LX/CmL;->b:LX/Clb;

    .line 2748521
    move-object v3, v0

    .line 2748522
    iget-object v4, p0, LX/Ju6;->I:LX/Clh;

    iget-object v0, p0, LX/Ju6;->J:LX/Chi;

    .line 2748523
    iget-object v5, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v5

    .line 2748524
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/Ju6;->J:LX/Chi;

    .line 2748525
    iget-object v5, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v5

    .line 2748526
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    :goto_0
    iget-object v5, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v4, v0, v3, v5}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2748527
    invoke-virtual {v3}, LX/CmL;->c()LX/Cly;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2748528
    :cond_2
    iget-object v0, p0, LX/Ju6;->N:LX/Ju8;

    if-eqz v0, :cond_3

    .line 2748529
    new-instance v0, LX/JuB;

    iget-object v3, p0, LX/Ju6;->N:LX/Ju8;

    .line 2748530
    iget-object v4, v3, LX/Ju8;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2748531
    iget-object v4, p0, LX/Ju6;->N:LX/Ju8;

    .line 2748532
    iget-wide v6, v4, LX/Ju8;->a:J

    move-wide v4, v6

    .line 2748533
    invoke-direct {v0, v3, v4, v5}, LX/JuB;-><init>(Ljava/lang/String;J)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2748534
    :cond_3
    iget-object v0, p0, LX/Ju6;->L:LX/Clr;

    if-eqz v0, :cond_4

    .line 2748535
    iget-object v0, p0, LX/Ju6;->L:LX/Clr;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2748536
    :cond_4
    new-instance v0, LX/Clo;

    iget-object v3, p0, LX/I4B;->s:Ljava/lang/String;

    invoke-direct {v0, v3}, LX/Clo;-><init>(Ljava/lang/String;)V

    .line 2748537
    invoke-virtual {v0, v1}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2748538
    iget-object v1, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2748539
    invoke-virtual {v0, v2}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2748540
    iget-object v1, v0, LX/Clo;->b:Landroid/os/Bundle;

    move-object v1, v1

    .line 2748541
    iget-object v2, p0, LX/I4B;->F:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2748542
    return-object v0

    .line 2748543
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b(LX/8Z4;)LX/I4B;
    .locals 1

    .prologue
    .line 2748495
    invoke-super {p0, p1}, LX/I4B;->b(LX/8Z4;)LX/I4B;

    .line 2748496
    move-object v0, p0

    .line 2748497
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;)LX/I4B;
    .locals 1

    .prologue
    .line 2748498
    invoke-super {p0, p1}, LX/I4B;->b(Ljava/lang/String;)LX/I4B;

    .line 2748499
    move-object v0, p0

    .line 2748500
    return-object v0
.end method

.method public final b(I)LX/Ju6;
    .locals 0

    .prologue
    .line 2748493
    invoke-super {p0, p1}, LX/I4B;->a(I)LX/I4B;

    .line 2748494
    return-object p0
.end method

.method public final synthetic c(Ljava/lang/String;)LX/I4B;
    .locals 1

    .prologue
    .line 2748490
    invoke-super {p0, p1}, LX/I4B;->c(Ljava/lang/String;)LX/I4B;

    .line 2748491
    move-object v0, p0

    .line 2748492
    return-object v0
.end method

.method public final f(LX/8Z4;)LX/Ju6;
    .locals 0

    .prologue
    .line 2748488
    invoke-super {p0, p1}, LX/I4B;->b(LX/8Z4;)LX/I4B;

    .line 2748489
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/Ju6;
    .locals 0

    .prologue
    .line 2748486
    invoke-super {p0, p1}, LX/I4B;->a(Ljava/lang/String;)LX/I4B;

    .line 2748487
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/Ju6;
    .locals 0

    .prologue
    .line 2748484
    invoke-super {p0, p1}, LX/I4B;->b(Ljava/lang/String;)LX/I4B;

    .line 2748485
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/Ju6;
    .locals 0

    .prologue
    .line 2748482
    invoke-super {p0, p1}, LX/I4B;->c(Ljava/lang/String;)LX/I4B;

    .line 2748483
    return-object p0
.end method
