.class public LX/JkR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/JkR;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3QW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2729822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2729823
    return-void
.end method

.method public static a(LX/0QB;)LX/JkR;
    .locals 5

    .prologue
    .line 2729824
    sget-object v0, LX/JkR;->c:LX/JkR;

    if-nez v0, :cond_1

    .line 2729825
    const-class v1, LX/JkR;

    monitor-enter v1

    .line 2729826
    :try_start_0
    sget-object v0, LX/JkR;->c:LX/JkR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2729827
    if-eqz v2, :cond_0

    .line 2729828
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2729829
    new-instance p0, LX/JkR;

    invoke-direct {p0}, LX/JkR;-><init>()V

    .line 2729830
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/3QW;->a(LX/0QB;)LX/3QW;

    move-result-object v4

    check-cast v4, LX/3QW;

    .line 2729831
    iput-object v3, p0, LX/JkR;->a:LX/0Zb;

    iput-object v4, p0, LX/JkR;->b:LX/3QW;

    .line 2729832
    move-object v0, p0

    .line 2729833
    sput-object v0, LX/JkR;->c:LX/JkR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2729834
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2729835
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2729836
    :cond_1
    sget-object v0, LX/JkR;->c:LX/JkR;

    return-object v0

    .line 2729837
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2729838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
