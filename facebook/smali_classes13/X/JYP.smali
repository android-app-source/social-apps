.class public LX/JYP;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mW",
        "<",
        "LX/JY4;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/JYV;

.field private final d:LX/JYJ;

.field private final e:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;LX/JYV;LX/JYJ;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/3mU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/JY4;",
            ">;",
            "LX/3mU;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            "LX/25M;",
            "LX/JYV;",
            "LX/JYJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706297
    invoke-direct/range {p0 .. p6}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 2706298
    iput-object p4, p0, LX/JYP;->e:LX/1Pq;

    .line 2706299
    iput-object p7, p0, LX/JYP;->c:LX/JYV;

    .line 2706300
    iput-object p8, p0, LX/JYP;->d:LX/JYJ;

    .line 2706301
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2706296
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2706266
    check-cast p2, LX/JY4;

    .line 2706267
    iget v0, p2, LX/JY4;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2706268
    iget-object v0, p0, LX/JYP;->d:LX/JYJ;

    const/4 v1, 0x0

    .line 2706269
    new-instance p0, LX/JYI;

    invoke-direct {p0, v0}, LX/JYI;-><init>(LX/JYJ;)V

    .line 2706270
    iget-object p2, v0, LX/JYJ;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JYH;

    .line 2706271
    if-nez p2, :cond_0

    .line 2706272
    new-instance p2, LX/JYH;

    invoke-direct {p2, v0}, LX/JYH;-><init>(LX/JYJ;)V

    .line 2706273
    :cond_0
    invoke-static {p2, p1, v1, v1, p0}, LX/JYH;->a$redex0(LX/JYH;LX/1De;IILX/JYI;)V

    .line 2706274
    move-object p0, p2

    .line 2706275
    move-object v1, p0

    .line 2706276
    move-object v0, v1

    .line 2706277
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2706278
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/JYP;->c:LX/JYV;

    const/4 v1, 0x0

    .line 2706279
    new-instance v2, LX/JYU;

    invoke-direct {v2, v0}, LX/JYU;-><init>(LX/JYV;)V

    .line 2706280
    iget-object v3, v0, LX/JYV;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JYT;

    .line 2706281
    if-nez v3, :cond_2

    .line 2706282
    new-instance v3, LX/JYT;

    invoke-direct {v3, v0}, LX/JYT;-><init>(LX/JYV;)V

    .line 2706283
    :cond_2
    invoke-static {v3, p1, v1, v1, v2}, LX/JYT;->a$redex0(LX/JYT;LX/1De;IILX/JYU;)V

    .line 2706284
    move-object v2, v3

    .line 2706285
    move-object v1, v2

    .line 2706286
    move-object v0, v1

    .line 2706287
    iget-object v1, v0, LX/JYT;->a:LX/JYU;

    iput-object p2, v1, LX/JYU;->a:LX/JY4;

    .line 2706288
    iget-object v1, v0, LX/JYT;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2706289
    move-object v0, v0

    .line 2706290
    iget-object v1, p0, LX/JYP;->e:LX/1Pq;

    .line 2706291
    iget-object v2, v0, LX/JYT;->a:LX/JYU;

    iput-object v1, v2, LX/JYU;->b:LX/1Pq;

    .line 2706292
    iget-object v2, v0, LX/JYT;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2706293
    move-object v0, v0

    .line 2706294
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2706295
    const/4 v0, 0x0

    return v0
.end method
