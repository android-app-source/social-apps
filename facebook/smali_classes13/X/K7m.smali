.class public LX/K7m;
.super LX/0b4;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/K5M;",
        "LX/K7l;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2773570
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2773571
    return-void
.end method

.method public static a(LX/0QB;)LX/K7m;
    .locals 3

    .prologue
    .line 2773572
    const-class v1, LX/K7m;

    monitor-enter v1

    .line 2773573
    :try_start_0
    sget-object v0, LX/K7m;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2773574
    sput-object v2, LX/K7m;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2773575
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2773576
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2773577
    new-instance v0, LX/K7m;

    invoke-direct {v0}, LX/K7m;-><init>()V

    .line 2773578
    move-object v0, v0

    .line 2773579
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2773580
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K7m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2773581
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2773582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
