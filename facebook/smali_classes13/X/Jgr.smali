.class public final LX/Jgr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "Ljava/lang/Void;",
        "LX/3MZ;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jgt;


# direct methods
.method public constructor <init>(LX/Jgt;)V
    .locals 0

    .prologue
    .line 2723817
    iput-object p1, p0, LX/Jgr;->a:LX/Jgt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 2723818
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2723819
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2723820
    check-cast p2, LX/3MZ;

    .line 2723821
    iget-object v0, p0, LX/Jgr;->a:LX/Jgt;

    const/4 v2, 0x1

    .line 2723822
    if-nez p2, :cond_1

    .line 2723823
    :cond_0
    :goto_0
    return-void

    .line 2723824
    :cond_1
    iput-boolean v2, v0, LX/Jgt;->k:Z

    .line 2723825
    const/4 v1, 0x0

    .line 2723826
    iget-object v3, p2, LX/3MZ;->b:LX/0Px;

    move-object v3, v3

    .line 2723827
    if-eqz v3, :cond_2

    iget-boolean v3, v0, LX/Jgt;->j:Z

    if-eqz v3, :cond_2

    .line 2723828
    new-instance v1, Ljava/util/ArrayList;

    .line 2723829
    iget-object v3, p2, LX/3MZ;->b:LX/0Px;

    move-object v3, v3

    .line 2723830
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, LX/Jgt;->h:Ljava/util/List;

    move v1, v2

    .line 2723831
    :cond_2
    iget-object v3, p2, LX/3MZ;->h:LX/0Px;

    move-object v3, v3

    .line 2723832
    if-eqz v3, :cond_3

    .line 2723833
    iget-object v1, p2, LX/3MZ;->h:LX/0Px;

    move-object v1, v1

    .line 2723834
    iget-object v3, p2, LX/3MZ;->o:LX/0Px;

    move-object v3, v3

    .line 2723835
    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2723836
    :goto_1
    move-object v1, v3

    .line 2723837
    iput-object v1, v0, LX/Jgt;->i:Ljava/util/List;

    .line 2723838
    :goto_2
    if-eqz v2, :cond_0

    .line 2723839
    iget-object v1, v0, LX/Jgt;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2723840
    invoke-static {v1}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->v(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723841
    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_2

    .line 2723842
    :cond_4
    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result p0

    if-eqz p0, :cond_5

    move-object v3, v1

    .line 2723843
    goto :goto_1

    .line 2723844
    :cond_5
    new-instance p0, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result p2

    add-int/2addr p1, p2

    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2723845
    invoke-interface {p0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2723846
    invoke-interface {p0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2723847
    new-instance p1, LX/DAp;

    invoke-direct {p1, p0}, LX/DAp;-><init>(Ljava/util/Collection;)V

    invoke-static {p0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v3, p0

    .line 2723848
    goto :goto_1
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2723849
    return-void
.end method
