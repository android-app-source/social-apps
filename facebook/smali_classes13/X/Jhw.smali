.class public LX/Jhw;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/6lN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/DAV;

.field public d:Lcom/facebook/user/tiles/UserTileView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725608
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725609
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2725610
    iput-object v0, p0, LX/Jhw;->a:LX/0Ot;

    .line 2725611
    const-class v0, LX/Jhw;

    invoke-static {v0, p0}, LX/Jhw;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725612
    const v0, 0x7f030ce6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725613
    invoke-virtual {p0}, LX/Jhw;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0104d0

    const/4 p1, 0x0

    invoke-static {v0, v1, p1}, LX/0WH;->d(Landroid/content/Context;II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Jhw;->setMinimumHeight(I)V

    .line 2725614
    const v0, 0x7f0d1e99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Jhw;->d:Lcom/facebook/user/tiles/UserTileView;

    .line 2725615
    const v0, 0x7f0d2034

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jhw;->e:Landroid/widget/TextView;

    .line 2725616
    const v0, 0x7f0d2035

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jhw;->f:Landroid/widget/TextView;

    .line 2725617
    const v0, 0x7f0d2036

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jhw;->g:Landroid/widget/TextView;

    .line 2725618
    return-void
.end method

.method public static a(Landroid/text/Spannable;I)Landroid/text/Spannable;
    .locals 4

    .prologue
    const/16 v1, 0x8c

    .line 2725602
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    if-ge p1, v1, :cond_1

    .line 2725603
    :cond_0
    :goto_0
    return-object p0

    .line 2725604
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 2725605
    const/4 v1, 0x0

    invoke-interface {p0, v1, v0}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2725606
    const-string v1, " \u2026"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    add-int/lit8 v2, p1, -0x46

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v3

    invoke-interface {p0, v2, v3}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object p0, v0

    .line 2725607
    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Jhw;

    const/16 p0, 0x2be

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v1}, LX/6lN;->a(LX/0QB;)LX/6lN;

    move-result-object v1

    check-cast v1, LX/6lN;

    iput-object p0, p1, LX/Jhw;->a:LX/0Ot;

    iput-object v1, p1, LX/Jhw;->b:LX/6lN;

    return-void
.end method
