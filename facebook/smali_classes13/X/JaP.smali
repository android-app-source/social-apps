.class public LX/JaP;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JaP;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715434
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2715435
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2715436
    const-string v1, "mobile_page"

    const-string v2, "/salegroups"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2715437
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2715438
    sget-object v1, LX/0ax;->hv:Ljava/lang/String;

    invoke-virtual {p0, v1, p1, v0}, LX/398;->a(Ljava/lang/String;LX/0Or;Landroid/os/Bundle;)V

    .line 2715439
    return-void
.end method

.method public static a(LX/0QB;)LX/JaP;
    .locals 4

    .prologue
    .line 2715440
    sget-object v0, LX/JaP;->a:LX/JaP;

    if-nez v0, :cond_1

    .line 2715441
    const-class v1, LX/JaP;

    monitor-enter v1

    .line 2715442
    :try_start_0
    sget-object v0, LX/JaP;->a:LX/JaP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2715443
    if-eqz v2, :cond_0

    .line 2715444
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2715445
    new-instance v3, LX/JaP;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JaP;-><init>(LX/0Or;)V

    .line 2715446
    move-object v0, v3

    .line 2715447
    sput-object v0, LX/JaP;->a:LX/JaP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2715448
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2715449
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2715450
    :cond_1
    sget-object v0, LX/JaP;->a:LX/JaP;

    return-object v0

    .line 2715451
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2715452
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
