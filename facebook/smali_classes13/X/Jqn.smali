.class public LX/Jqn;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/Jrc;

.field private final b:LX/Jqb;

.field private final c:LX/2N4;

.field private final d:LX/FDs;

.field private final e:LX/0SG;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740352
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqn;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/Jrc;LX/Jqb;LX/2N4;LX/FDs;LX/0SG;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jrc;",
            "LX/Jqb;",
            "LX/2N4;",
            "LX/FDs;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2740353
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2740354
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740355
    iput-object v0, p0, LX/Jqn;->f:LX/0Ot;

    .line 2740356
    iput-object p2, p0, LX/Jqn;->a:LX/Jrc;

    .line 2740357
    iput-object p3, p0, LX/Jqn;->b:LX/Jqb;

    .line 2740358
    iput-object p4, p0, LX/Jqn;->c:LX/2N4;

    .line 2740359
    iput-object p5, p0, LX/Jqn;->d:LX/FDs;

    .line 2740360
    iput-object p6, p0, LX/Jqn;->e:LX/0SG;

    .line 2740361
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqn;
    .locals 14

    .prologue
    .line 2740362
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740363
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740364
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740365
    if-nez v1, :cond_0

    .line 2740366
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740367
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740368
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740369
    sget-object v1, LX/Jqn;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740370
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740371
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740372
    :cond_1
    if-nez v1, :cond_4

    .line 2740373
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740374
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740375
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2740376
    new-instance v7, LX/Jqn;

    const/16 v8, 0x35bd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v9

    check-cast v9, LX/Jrc;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v10

    check-cast v10, LX/Jqb;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v11

    check-cast v11, LX/2N4;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v12

    check-cast v12, LX/FDs;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v7 .. v13}, LX/Jqn;-><init>(LX/0Ot;LX/Jrc;LX/Jqb;LX/2N4;LX/FDs;LX/0SG;)V

    .line 2740377
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2740378
    iput-object v8, v7, LX/Jqn;->f:LX/0Ot;

    .line 2740379
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2740380
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740381
    if-nez v1, :cond_2

    .line 2740382
    sget-object v0, LX/Jqn;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqn;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740383
    :goto_1
    if-eqz v0, :cond_3

    .line 2740384
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740385
    :goto_3
    check-cast v0, LX/Jqn;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740386
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740387
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740388
    :catchall_1
    move-exception v0

    .line 2740389
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740390
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740391
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740392
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqn;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqn;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740393
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740394
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740395
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2740396
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->G()LX/6ji;

    move-result-object v0

    .line 2740397
    iget-object v2, p0, LX/Jqn;->c:LX/2N4;

    iget-object v3, p0, LX/Jqn;->a:LX/Jrc;

    iget-object v4, v0, LX/6ji;->messageMetadata:LX/6kn;

    iget-object v4, v4, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v3, v4}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    .line 2740398
    iget-object v2, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740399
    if-nez v2, :cond_0

    move-object v0, v1

    .line 2740400
    :goto_0
    return-object v0

    .line 2740401
    :cond_0
    iget-object v3, v0, LX/6ji;->mode:Ljava/lang/Integer;

    if-nez v3, :cond_1

    .line 2740402
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DeltaApprovalMode mode is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740403
    :cond_1
    iget-object v0, v0, LX/6ji;->mode:Ljava/lang/Integer;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2740404
    iget-object v3, p0, LX/Jqn;->d:LX/FDs;

    iget-object v4, p0, LX/Jqn;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 2740405
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object p0

    iget-object p1, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-virtual {p0, p1}, LX/6fm;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6fm;

    move-result-object p0

    .line 2740406
    iput-boolean v0, p0, LX/6fm;->d:Z

    .line 2740407
    move-object p0, p0

    .line 2740408
    invoke-virtual {p0}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object p0

    .line 2740409
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object p1

    invoke-virtual {p1, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object p1

    invoke-virtual {p1, p0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    move-result-object p0

    invoke-virtual {p0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object p0

    .line 2740410
    invoke-static {v3, p0, v4, v5}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740411
    iget-object p1, v3, LX/FDs;->d:LX/2N4;

    iget-object p0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 p2, 0x0

    invoke-virtual {p1, p0, p2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object p0

    iget-object p0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, p0

    .line 2740412
    if-eqz v0, :cond_2

    .line 2740413
    const-string v2, "approval_mode_thread_summary"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    move-object v0, v1

    .line 2740414
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740415
    const-string v0, "approval_mode_thread_summary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740416
    if-eqz v0, :cond_0

    .line 2740417
    iget-object v1, p0, LX/Jqn;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/Jqn;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740418
    iget-object v1, p0, LX/Jqn;->b:LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2740419
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2740420
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2740421
    check-cast p1, LX/6kW;

    .line 2740422
    invoke-virtual {p1}, LX/6kW;->G()LX/6ji;

    move-result-object v0

    .line 2740423
    iget-object v1, p0, LX/Jqn;->a:LX/Jrc;

    iget-object v0, v0, LX/6ji;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2740424
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
