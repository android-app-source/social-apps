.class public final LX/JZ5;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/JZ8;

.field public final synthetic b:LX/1Pr;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

.field public final synthetic d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;LX/JZ8;LX/1Pr;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V
    .locals 0

    .prologue
    .line 2707853
    iput-object p1, p0, LX/JZ5;->d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    iput-object p2, p0, LX/JZ5;->a:LX/JZ8;

    iput-object p3, p0, LX/JZ5;->b:LX/1Pr;

    iput-object p4, p0, LX/JZ5;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2707854
    iget-object v0, p0, LX/JZ5;->a:LX/JZ8;

    .line 2707855
    iput-boolean v2, v0, LX/JZ8;->e:Z

    .line 2707856
    iget-object v0, p0, LX/JZ5;->d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    iget-object v1, p0, LX/JZ5;->a:LX/JZ8;

    .line 2707857
    iget-object v3, v1, LX/JZ8;->a:LX/JZ7;

    move-object v3, v3

    .line 2707858
    sget-object v4, LX/JZ7;->SAVING:LX/JZ7;

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2707859
    sget-object v3, LX/JZ7;->NONE:LX/JZ7;

    .line 2707860
    iput-object v3, v1, LX/JZ8;->a:LX/JZ7;

    .line 2707861
    iget-object v3, v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->b:LX/0kL;

    new-instance v4, LX/27k;

    const p1, 0x7f081017

    invoke-direct {v4, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v3, v4}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2707862
    iget-object v0, p0, LX/JZ5;->b:LX/1Pr;

    check-cast v0, LX/1Pq;

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/JZ5;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2707863
    return-void

    .line 2707864
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2707865
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v3, 0x1

    .line 2707866
    iget-object v0, p0, LX/JZ5;->a:LX/JZ8;

    .line 2707867
    iput-boolean v3, v0, LX/JZ8;->e:Z

    .line 2707868
    iget-object v0, p0, LX/JZ5;->d:Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    iget-object v1, p0, LX/JZ5;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    iget-object v2, p0, LX/JZ5;->a:LX/JZ8;

    const/4 v5, 0x1

    .line 2707869
    iget-object v4, v2, LX/JZ8;->a:LX/JZ7;

    move-object v4, v4

    .line 2707870
    sget-object v6, LX/JZ7;->SAVING:LX/JZ7;

    if-ne v4, v6, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2707871
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2707872
    const-string v6, "markSurveyCompletedParamsKey"

    new-instance v7, Lcom/facebook/api/feed/MarkSurveyCompletedParams;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/facebook/api/feed/MarkSurveyCompletedParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2707873
    iget-object v6, v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->c:LX/0aG;

    const-string v7, "feed_mark_survey_completed"

    const v8, 0x2dff6e40

    invoke-static {v6, v7, v4, v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 2707874
    sget-object v4, LX/JZ7;->NONE:LX/JZ7;

    .line 2707875
    iput-object v4, v2, LX/JZ8;->a:LX/JZ7;

    .line 2707876
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/api/feed/SubmitSurveyResponseResult;

    .line 2707877
    iget-object v5, v4, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->a:Ljava/lang/String;

    .line 2707878
    iput-object v5, v2, LX/JZ8;->c:Ljava/lang/String;

    .line 2707879
    iget-object v5, v4, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->b:Ljava/lang/String;

    .line 2707880
    iput-object v5, v2, LX/JZ8;->d:Ljava/lang/String;

    .line 2707881
    iget-object v4, v4, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->b:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2707882
    iget v4, v2, LX/JZ8;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, LX/JZ8;->b:I

    .line 2707883
    :goto_1
    iget-object v0, p0, LX/JZ5;->b:LX/1Pr;

    check-cast v0, LX/1Pq;

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/JZ5;->c:Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2707884
    return-void

    .line 2707885
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 2707886
    :cond_1
    sget-object v4, LX/JZ7;->COMPLETE:LX/JZ7;

    .line 2707887
    iput-object v4, v2, LX/JZ8;->a:LX/JZ7;

    .line 2707888
    goto :goto_1
.end method
