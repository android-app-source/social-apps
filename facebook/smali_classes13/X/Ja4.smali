.class public final LX/Ja4;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Ja6;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/Ja5;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2709535
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "state"

    aput-object v2, v0, v1

    sput-object v0, LX/Ja4;->b:[Ljava/lang/String;

    .line 2709536
    sput v3, LX/Ja4;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2709537
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2709538
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/Ja4;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Ja4;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Ja4;LX/1De;IILX/Ja5;)V
    .locals 1

    .prologue
    .line 2709531
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2709532
    iput-object p4, p0, LX/Ja4;->a:LX/Ja5;

    .line 2709533
    iget-object v0, p0, LX/Ja4;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2709534
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2709517
    invoke-super {p0}, LX/1X5;->a()V

    .line 2709518
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ja4;->a:LX/Ja5;

    .line 2709519
    sget-object v0, LX/Ja6;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2709520
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Ja6;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2709521
    iget-object v1, p0, LX/Ja4;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Ja4;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/Ja4;->c:I

    if-ge v1, v2, :cond_2

    .line 2709522
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2709523
    :goto_0
    sget v2, LX/Ja4;->c:I

    if-ge v0, v2, :cond_1

    .line 2709524
    iget-object v2, p0, LX/Ja4;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2709525
    sget-object v2, LX/Ja4;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2709526
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2709527
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2709528
    :cond_2
    iget-object v0, p0, LX/Ja4;->a:LX/Ja5;

    .line 2709529
    invoke-virtual {p0}, LX/Ja4;->a()V

    .line 2709530
    return-object v0
.end method
