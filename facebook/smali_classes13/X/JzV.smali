.class public LX/JzV;
.super LX/Jz0;
.source ""


# instance fields
.field private e:J

.field private f:Z

.field private g:D

.field private h:D

.field private i:Z

.field private final j:LX/JzU;

.field private final k:LX/JzU;

.field private final l:LX/JzU;

.field private m:D

.field private n:D

.field private o:D

.field private p:D

.field private q:D


# direct methods
.method public constructor <init>(LX/5pG;)V
    .locals 4

    .prologue
    .line 2756216
    invoke-direct {p0}, LX/Jz0;-><init>()V

    .line 2756217
    new-instance v0, LX/JzU;

    invoke-direct {v0}, LX/JzU;-><init>()V

    iput-object v0, p0, LX/JzV;->j:LX/JzU;

    .line 2756218
    new-instance v0, LX/JzU;

    invoke-direct {v0}, LX/JzU;-><init>()V

    iput-object v0, p0, LX/JzV;->k:LX/JzU;

    .line 2756219
    new-instance v0, LX/JzU;

    invoke-direct {v0}, LX/JzU;-><init>()V

    iput-object v0, p0, LX/JzV;->l:LX/JzU;

    .line 2756220
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/JzV;->q:D

    .line 2756221
    const-string v0, "friction"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/JzV;->g:D

    .line 2756222
    const-string v0, "tension"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/JzV;->h:D

    .line 2756223
    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    const-string v1, "initialVelocity"

    invoke-interface {p1, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, v0, LX/JzU;->b:D

    .line 2756224
    const-string v0, "toValue"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/JzV;->n:D

    .line 2756225
    const-string v0, "restSpeedThreshold"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/JzV;->o:D

    .line 2756226
    const-string v0, "restDisplacementThreshold"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/JzV;->p:D

    .line 2756227
    const-string v0, "overshootClamping"

    invoke-interface {p1, v0}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/JzV;->i:Z

    .line 2756228
    return-void
.end method

.method private a(LX/JzU;)D
    .locals 4

    .prologue
    .line 2756229
    iget-wide v0, p0, LX/JzV;->n:D

    iget-wide v2, p1, LX/JzU;->a:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private a(D)V
    .locals 11

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 2756230
    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    iget-object v1, p0, LX/JzV;->j:LX/JzU;

    iget-wide v2, v1, LX/JzU;->a:D

    mul-double/2addr v2, p1

    iget-object v1, p0, LX/JzV;->k:LX/JzU;

    iget-wide v4, v1, LX/JzU;->a:D

    sub-double v6, v8, p1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, LX/JzU;->a:D

    .line 2756231
    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    iget-object v1, p0, LX/JzV;->j:LX/JzU;

    iget-wide v2, v1, LX/JzU;->b:D

    mul-double/2addr v2, p1

    iget-object v1, p0, LX/JzV;->k:LX/JzU;

    iget-wide v4, v1, LX/JzU;->b:D

    sub-double v6, v8, p1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, LX/JzU;->b:D

    .line 2756232
    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 2756233
    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    iget-wide v0, v0, LX/JzU;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, LX/JzV;->o:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    invoke-direct {p0, v0}, LX/JzV;->a(LX/JzU;)D

    move-result-wide v0

    iget-wide v2, p0, LX/JzV;->p:D

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/JzV;->h:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(D)V
    .locals 31

    .prologue
    .line 2756234
    invoke-direct/range {p0 .. p0}, LX/JzV;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2756235
    :cond_0
    :goto_0
    return-void

    .line 2756236
    :cond_1
    const-wide v2, 0x3fb0624dd2f1a9fcL    # 0.064

    cmpl-double v2, p1, v2

    if-lez v2, :cond_2

    .line 2756237
    const-wide p1, 0x3fb0624dd2f1a9fcL    # 0.064

    .line 2756238
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->q:D

    add-double v2, v2, p1

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/JzV;->q:D

    .line 2756239
    move-object/from16 v0, p0

    iget-wide v10, v0, LX/JzV;->h:D

    .line 2756240
    move-object/from16 v0, p0

    iget-wide v12, v0, LX/JzV;->g:D

    .line 2756241
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    iget-wide v8, v2, LX/JzU;->a:D

    .line 2756242
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    iget-wide v6, v2, LX/JzU;->b:D

    .line 2756243
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->l:LX/JzU;

    iget-wide v4, v2, LX/JzU;->a:D

    .line 2756244
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->l:LX/JzU;

    iget-wide v2, v2, LX/JzU;->b:D

    .line 2756245
    :goto_1
    move-object/from16 v0, p0

    iget-wide v14, v0, LX/JzV;->q:D

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v14, v14, v16

    if-ltz v14, :cond_4

    .line 2756246
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->q:D

    const-wide v14, 0x3f50624dd2f1a9fcL    # 0.001

    sub-double/2addr v2, v14

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/JzV;->q:D

    .line 2756247
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->q:D

    const-wide v14, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v2, v2, v14

    if-gez v2, :cond_3

    .line 2756248
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->k:LX/JzU;

    iput-wide v8, v2, LX/JzU;->a:D

    .line 2756249
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->k:LX/JzU;

    iput-wide v6, v2, LX/JzU;->b:D

    .line 2756250
    :cond_3
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->n:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v10

    mul-double v4, v12, v6

    sub-double v14, v2, v4

    .line 2756251
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v2, v6

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    .line 2756252
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v14

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    mul-double v4, v4, v16

    add-double v16, v6, v4

    .line 2756253
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/JzV;->n:D

    sub-double v2, v4, v2

    mul-double/2addr v2, v10

    mul-double v4, v12, v16

    sub-double v18, v2, v4

    .line 2756254
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v16

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    .line 2756255
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v4, v4, v18

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v4, v4, v20

    add-double v20, v6, v4

    .line 2756256
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/JzV;->n:D

    sub-double v2, v4, v2

    mul-double/2addr v2, v10

    mul-double v4, v12, v20

    sub-double v22, v2, v4

    .line 2756257
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v20

    add-double v4, v8, v2

    .line 2756258
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v22

    add-double/2addr v2, v6

    .line 2756259
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/JzV;->n:D

    move-wide/from16 v24, v0

    sub-double v24, v24, v4

    mul-double v24, v24, v10

    mul-double v26, v12, v2

    sub-double v24, v24, v26

    .line 2756260
    const-wide v26, 0x3fc5555555555555L    # 0.16666666666666666

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    add-double v16, v16, v20

    mul-double v16, v16, v28

    add-double v16, v16, v6

    add-double v16, v16, v2

    mul-double v16, v16, v26

    .line 2756261
    const-wide v20, 0x3fc5555555555555L    # 0.16666666666666666

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    add-double v18, v18, v22

    mul-double v18, v18, v26

    add-double v14, v14, v18

    add-double v14, v14, v24

    mul-double v14, v14, v20

    .line 2756262
    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v16, v16, v18

    add-double v8, v8, v16

    .line 2756263
    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v14, v14, v16

    add-double/2addr v6, v14

    goto/16 :goto_1

    .line 2756264
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, LX/JzV;->l:LX/JzU;

    iput-wide v4, v12, LX/JzU;->a:D

    .line 2756265
    move-object/from16 v0, p0

    iget-object v4, v0, LX/JzV;->l:LX/JzU;

    iput-wide v2, v4, LX/JzU;->b:D

    .line 2756266
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    iput-wide v8, v2, LX/JzU;->a:D

    .line 2756267
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    iput-wide v6, v2, LX/JzU;->b:D

    .line 2756268
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->q:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 2756269
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->q:D

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    div-double/2addr v2, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, LX/JzV;->a(D)V

    .line 2756270
    :cond_5
    invoke-direct/range {p0 .. p0}, LX/JzV;->a()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/JzV;->i:Z

    if-eqz v2, :cond_0

    invoke-direct/range {p0 .. p0}, LX/JzV;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2756271
    :cond_6
    const-wide/16 v2, 0x0

    cmpl-double v2, v10, v2

    if-lez v2, :cond_7

    .line 2756272
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->n:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/JzV;->m:D

    .line 2756273
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/JzV;->n:D

    iput-wide v4, v2, LX/JzU;->a:D

    .line 2756274
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    const-wide/16 v4, 0x0

    iput-wide v4, v2, LX/JzU;->b:D

    goto/16 :goto_0

    .line 2756275
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JzV;->j:LX/JzU;

    iget-wide v2, v2, LX/JzU;->a:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/JzV;->n:D

    .line 2756276
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/JzV;->n:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/JzV;->m:D

    goto :goto_2
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 2756277
    iget-wide v0, p0, LX/JzV;->h:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, LX/JzV;->m:D

    iget-wide v2, p0, LX/JzV;->n:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    iget-wide v0, v0, LX/JzU;->a:D

    iget-wide v2, p0, LX/JzV;->n:D

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    iget-wide v0, p0, LX/JzV;->m:D

    iget-wide v2, p0, LX/JzV;->n:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, LX/JzV;->j:LX/JzU;

    iget-wide v0, v0, LX/JzU;->a:D

    iget-wide v2, p0, LX/JzV;->n:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    .line 2756278
    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    .line 2756279
    iget-boolean v2, p0, LX/JzV;->f:Z

    if-nez v2, :cond_0

    .line 2756280
    iget-object v2, p0, LX/JzV;->j:LX/JzU;

    iget-object v3, p0, LX/Jz0;->b:LX/Jyx;

    iget-wide v4, v3, LX/Jyx;->e:D

    iput-wide v4, v2, LX/JzU;->a:D

    iput-wide v4, p0, LX/JzV;->m:D

    .line 2756281
    iput-wide v0, p0, LX/JzV;->e:J

    .line 2756282
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/JzV;->f:Z

    .line 2756283
    :cond_0
    iget-wide v2, p0, LX/JzV;->e:J

    sub-long v2, v0, v2

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-direct {p0, v2, v3}, LX/JzV;->b(D)V

    .line 2756284
    iput-wide v0, p0, LX/JzV;->e:J

    .line 2756285
    iget-object v0, p0, LX/Jz0;->b:LX/Jyx;

    iget-object v1, p0, LX/JzV;->j:LX/JzU;

    iget-wide v2, v1, LX/JzU;->a:D

    iput-wide v2, v0, LX/Jyx;->e:D

    .line 2756286
    invoke-direct {p0}, LX/JzV;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/JzV;->a:Z

    .line 2756287
    return-void
.end method
