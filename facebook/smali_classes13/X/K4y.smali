.class public LX/K4y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 2769682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769683
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/K4y;->a:[F

    .line 2769684
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 2769685
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x0

    aput v3, v0, v1

    .line 2769686
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 2769687
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 2769688
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 2769689
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x4

    aput v2, v0, v1

    .line 2769690
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x5

    aput v3, v0, v1

    .line 2769691
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x6

    aput v2, v0, v1

    .line 2769692
    iget-object v0, p0, LX/K4y;->a:[F

    const/4 v1, 0x7

    aput v2, v0, v1

    .line 2769693
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0x8

    aput v2, v0, v1

    .line 2769694
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0x9

    aput v2, v0, v1

    .line 2769695
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0xa

    aput v3, v0, v1

    .line 2769696
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0xb

    aput v2, v0, v1

    .line 2769697
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0xc

    aput v2, v0, v1

    .line 2769698
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0xd

    aput v2, v0, v1

    .line 2769699
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0xe

    aput v2, v0, v1

    .line 2769700
    iget-object v0, p0, LX/K4y;->a:[F

    const/16 v1, 0xf

    aput v3, v0, v1

    .line 2769701
    return-void
.end method

.method public constructor <init>(LX/K4y;)V
    .locals 1

    .prologue
    .line 2769702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769703
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/K4y;->a:[F

    .line 2769704
    invoke-virtual {p0, p1}, LX/K4y;->a(LX/K4y;)V

    .line 2769705
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0

    .prologue
    .line 2769706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769707
    iput-object p1, p0, LX/K4y;->a:[F

    .line 2769708
    return-void
.end method

.method private a(II)F
    .locals 2

    .prologue
    .line 2769709
    iget-object v0, p0, LX/K4y;->a:[F

    mul-int/lit8 v1, p1, 0x4

    add-int/2addr v1, p2

    aget v0, v0, v1

    return v0
.end method

.method private a(IIF)V
    .locals 2

    .prologue
    .line 2769710
    iget-object v0, p0, LX/K4y;->a:[F

    mul-int/lit8 v1, p1, 0x4

    add-int/2addr v1, p2

    aput p3, v0, v1

    .line 2769711
    return-void
.end method

.method public static b(LX/K4y;II)F
    .locals 12

    .prologue
    .line 2769712
    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    .line 2769713
    add-int/lit8 v1, p1, 0x2

    rem-int/lit8 v1, v1, 0x4

    .line 2769714
    add-int/lit8 v2, p1, 0x3

    rem-int/lit8 v2, v2, 0x4

    .line 2769715
    add-int/lit8 v3, p2, 0x1

    rem-int/lit8 v3, v3, 0x4

    .line 2769716
    add-int/lit8 v4, p2, 0x2

    rem-int/lit8 v4, v4, 0x4

    .line 2769717
    add-int/lit8 v5, p2, 0x3

    rem-int/lit8 v5, v5, 0x4

    .line 2769718
    iget-object v6, p0, LX/K4y;->a:[F

    mul-int/lit8 v7, v3, 0x4

    add-int/2addr v7, v0

    aget v6, v6, v7

    iget-object v7, p0, LX/K4y;->a:[F

    mul-int/lit8 v8, v4, 0x4

    add-int/2addr v8, v1

    aget v7, v7, v8

    iget-object v8, p0, LX/K4y;->a:[F

    mul-int/lit8 v9, v5, 0x4

    add-int/2addr v9, v2

    aget v8, v8, v9

    mul-float/2addr v7, v8

    iget-object v8, p0, LX/K4y;->a:[F

    mul-int/lit8 v9, v5, 0x4

    add-int/2addr v9, v1

    aget v8, v8, v9

    iget-object v9, p0, LX/K4y;->a:[F

    mul-int/lit8 v10, v4, 0x4

    add-int/2addr v10, v2

    aget v9, v9, v10

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    iget-object v7, p0, LX/K4y;->a:[F

    mul-int/lit8 v8, v4, 0x4

    add-int/2addr v8, v0

    aget v7, v7, v8

    iget-object v8, p0, LX/K4y;->a:[F

    mul-int/lit8 v9, v3, 0x4

    add-int/2addr v9, v1

    aget v8, v8, v9

    iget-object v9, p0, LX/K4y;->a:[F

    mul-int/lit8 v10, v5, 0x4

    add-int/2addr v10, v2

    aget v9, v9, v10

    mul-float/2addr v8, v9

    iget-object v9, p0, LX/K4y;->a:[F

    mul-int/lit8 v10, v5, 0x4

    add-int/2addr v10, v1

    aget v9, v9, v10

    iget-object v10, p0, LX/K4y;->a:[F

    mul-int/lit8 v11, v3, 0x4

    add-int/2addr v11, v2

    aget v10, v10, v11

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget-object v7, p0, LX/K4y;->a:[F

    mul-int/lit8 v5, v5, 0x4

    add-int/2addr v0, v5

    aget v0, v7, v0

    iget-object v5, p0, LX/K4y;->a:[F

    mul-int/lit8 v7, v3, 0x4

    add-int/2addr v7, v1

    aget v5, v5, v7

    iget-object v7, p0, LX/K4y;->a:[F

    mul-int/lit8 v8, v4, 0x4

    add-int/2addr v8, v2

    aget v7, v7, v8

    mul-float/2addr v5, v7

    iget-object v7, p0, LX/K4y;->a:[F

    mul-int/lit8 v4, v4, 0x4

    add-int/2addr v1, v4

    aget v1, v7, v1

    iget-object v4, p0, LX/K4y;->a:[F

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v2, v3

    aget v2, v4, v2

    mul-float/2addr v1, v2

    sub-float v1, v5, v1

    mul-float/2addr v0, v1

    add-float/2addr v0, v6

    .line 2769719
    add-int v1, p1, p2

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    neg-float v0, v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(LX/K4y;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2769720
    iget-object v0, p1, LX/K4y;->a:[F

    iget-object v1, p0, LX/K4y;->a:[F

    iget-object v2, p0, LX/K4y;->a:[F

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2769721
    return-void
.end method

.method public final a(LX/K4y;LX/K4y;)V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2769722
    move v7, v1

    :goto_0
    const/4 v0, 0x4

    if-ge v7, v0, :cond_1

    move v0, v1

    move v2, v3

    move v4, v3

    move v5, v3

    move v6, v3

    .line 2769723
    :goto_1
    const/4 v8, 0x4

    if-ge v0, v8, :cond_0

    .line 2769724
    invoke-direct {p2, v7, v0}, LX/K4y;->a(II)F

    move-result v8

    .line 2769725
    invoke-direct {p1, v0, v1}, LX/K4y;->a(II)F

    move-result v9

    mul-float/2addr v9, v8

    add-float/2addr v6, v9

    .line 2769726
    invoke-direct {p1, v0, v10}, LX/K4y;->a(II)F

    move-result v9

    mul-float/2addr v9, v8

    add-float/2addr v5, v9

    .line 2769727
    invoke-direct {p1, v0, v11}, LX/K4y;->a(II)F

    move-result v9

    mul-float/2addr v9, v8

    add-float/2addr v4, v9

    .line 2769728
    invoke-direct {p1, v0, v12}, LX/K4y;->a(II)F

    move-result v9

    mul-float/2addr v8, v9

    add-float/2addr v2, v8

    .line 2769729
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2769730
    :cond_0
    invoke-direct {p0, v7, v1, v6}, LX/K4y;->a(IIF)V

    .line 2769731
    invoke-direct {p0, v7, v10, v5}, LX/K4y;->a(IIF)V

    .line 2769732
    invoke-direct {p0, v7, v11, v4}, LX/K4y;->a(IIF)V

    .line 2769733
    invoke-direct {p0, v7, v12, v2}, LX/K4y;->a(IIF)V

    .line 2769734
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2769735
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2769736
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "("

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2769737
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x10

    if-ge v0, v2, :cond_1

    .line 2769738
    if-lez v0, :cond_0

    .line 2769739
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2769740
    :cond_0
    iget-object v2, p0, LX/K4y;->a:[F

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 2769741
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2769742
    :cond_1
    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2769743
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
