.class public final enum LX/Jf1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jf1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jf1;

.field public static final enum CLIENT_TOKEN:LX/Jf1;

.field public static final enum TAP_SURFACE:LX/Jf1;

.field public static final enum TIME_ON_SCREEN:LX/Jf1;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2721408
    new-instance v0, LX/Jf1;

    const-string v1, "CLIENT_TOKEN"

    const-string v2, "client_token"

    invoke-direct {v0, v1, v3, v2}, LX/Jf1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf1;->CLIENT_TOKEN:LX/Jf1;

    .line 2721409
    new-instance v0, LX/Jf1;

    const-string v1, "TAP_SURFACE"

    const-string v2, "tap_surface"

    invoke-direct {v0, v1, v4, v2}, LX/Jf1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf1;->TAP_SURFACE:LX/Jf1;

    .line 2721410
    new-instance v0, LX/Jf1;

    const-string v1, "TIME_ON_SCREEN"

    const-string v2, "time_on_screen"

    invoke-direct {v0, v1, v5, v2}, LX/Jf1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Jf1;->TIME_ON_SCREEN:LX/Jf1;

    .line 2721411
    const/4 v0, 0x3

    new-array v0, v0, [LX/Jf1;

    sget-object v1, LX/Jf1;->CLIENT_TOKEN:LX/Jf1;

    aput-object v1, v0, v3

    sget-object v1, LX/Jf1;->TAP_SURFACE:LX/Jf1;

    aput-object v1, v0, v4

    sget-object v1, LX/Jf1;->TIME_ON_SCREEN:LX/Jf1;

    aput-object v1, v0, v5

    sput-object v0, LX/Jf1;->$VALUES:[LX/Jf1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2721412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2721413
    iput-object p3, p0, LX/Jf1;->value:Ljava/lang/String;

    .line 2721414
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jf1;
    .locals 1

    .prologue
    .line 2721415
    const-class v0, LX/Jf1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jf1;

    return-object v0
.end method

.method public static values()[LX/Jf1;
    .locals 1

    .prologue
    .line 2721416
    sget-object v0, LX/Jf1;->$VALUES:[LX/Jf1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jf1;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2721417
    iget-object v0, p0, LX/Jf1;->value:Ljava/lang/String;

    return-object v0
.end method
