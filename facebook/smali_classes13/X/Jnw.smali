.class public final enum LX/Jnw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jnw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jnw;

.field public static final enum COMPLETED:LX/Jnw;

.field public static final enum IN_PROGRESS:LX/Jnw;

.field public static final enum NOT_STARTED:LX/Jnw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2734691
    new-instance v0, LX/Jnw;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LX/Jnw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jnw;->NOT_STARTED:LX/Jnw;

    .line 2734692
    new-instance v0, LX/Jnw;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/Jnw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jnw;->IN_PROGRESS:LX/Jnw;

    .line 2734693
    new-instance v0, LX/Jnw;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v4}, LX/Jnw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jnw;->COMPLETED:LX/Jnw;

    .line 2734694
    const/4 v0, 0x3

    new-array v0, v0, [LX/Jnw;

    sget-object v1, LX/Jnw;->NOT_STARTED:LX/Jnw;

    aput-object v1, v0, v2

    sget-object v1, LX/Jnw;->IN_PROGRESS:LX/Jnw;

    aput-object v1, v0, v3

    sget-object v1, LX/Jnw;->COMPLETED:LX/Jnw;

    aput-object v1, v0, v4

    sput-object v0, LX/Jnw;->$VALUES:[LX/Jnw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2734695
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jnw;
    .locals 1

    .prologue
    .line 2734696
    const-class v0, LX/Jnw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jnw;

    return-object v0
.end method

.method public static values()[LX/Jnw;
    .locals 1

    .prologue
    .line 2734697
    sget-object v0, LX/Jnw;->$VALUES:[LX/Jnw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jnw;

    return-object v0
.end method
