.class public abstract LX/JtC;
.super Landroid/app/Service;
.source ""

# interfaces
.implements LX/Jt7;
.implements LX/Jt8;
.implements LX/Jt9;
.implements LX/JtA;
.implements LX/JtB;


# instance fields
.field public a:LX/KB3;

.field private b:Landroid/os/IBinder;

.field public c:Landroid/content/Intent;

.field public final d:Ljava/lang/Object;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/JtC;->d:Ljava/lang/Object;

    return-void
.end method

.method public static a$redex0(LX/JtC;)Ljava/lang/String;
    .locals 3

    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, LX/JtC;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LX/KAh;)V
    .locals 0

    return-void
.end method

.method public a(LX/KAn;)V
    .locals 0

    return-void
.end method

.method public a(LX/KAu;)V
    .locals 0

    return-void
.end method

.method public a(LX/KAv;)V
    .locals 0

    return-void
.end method

.method public b(LX/KAv;)V
    .locals 0

    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.wearable.BIND_LISTENER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JtC;->b:Landroid/os/IBinder;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 6

    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x75043335

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v1, "WearableLS"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "onCreate: "

    invoke-static {p0}, LX/JtC;->a$redex0(LX/JtC;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "WearableListenerService"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v2, LX/KB3;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v2, p0, v1}, LX/KB3;-><init>(LX/JtC;Landroid/os/Looper;)V

    iput-object v2, p0, LX/JtC;->a:LX/KB3;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.wearable.BIND_LISTENER"

    const/4 v3, 0x0

    invoke-virtual {p0}, LX/JtC;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v1, p0, LX/JtC;->c:Landroid/content/Intent;

    new-instance v1, LX/KB6;

    invoke-direct {v1, p0}, LX/KB6;-><init>(LX/JtC;)V

    iput-object v1, p0, LX/JtC;->b:Landroid/os/IBinder;

    const v1, 0x7406a370

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 6

    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x3af86329

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    const-string v0, "WearableLS"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "onDestroy: "

    invoke-static {p0}, LX/JtC;->a$redex0(LX/JtC;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    iget-object v2, p0, LX/JtC;->d:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/JtC;->e:Z

    iget-object v0, p0, LX/JtC;->a:LX/KB3;

    if-nez v0, :cond_3

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()? component="

    invoke-static {p0}, LX/JtC;->a$redex0(LX/JtC;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v0, 0x6d413212

    invoke-static {v0, v1}, LX/02F;->d(II)V

    throw v3

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v2, -0x1c0e04a3

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0

    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, LX/JtC;->a:LX/KB3;

    invoke-virtual {v0}, LX/KB3;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const v0, 0x39ddbffc

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void
.end method
