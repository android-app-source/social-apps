.class public LX/JyX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

.field public c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

.field private d:Z

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;Lcom/facebook/profile/insight/view/ProfileInsightFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2754843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754844
    iput-boolean v0, p0, LX/JyX;->d:Z

    .line 2754845
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/JyX;->e:Ljava/lang/Integer;

    .line 2754846
    iput-object p1, p0, LX/JyX;->a:Landroid/view/View;

    .line 2754847
    iput-object p2, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 2754848
    iput-object p3, p0, LX/JyX;->b:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    .line 2754849
    iget-object v0, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    new-instance p1, LX/JyU;

    invoke-direct {p1, p0}, LX/JyU;-><init>(LX/JyX;)V

    .line 2754850
    iput-object p1, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 2754851
    iget-object v0, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    new-instance p1, LX/JyV;

    invoke-direct {p1, p0}, LX/JyV;-><init>(LX/JyX;)V

    .line 2754852
    iput-object p1, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 2754853
    iget-object v0, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    new-instance p1, LX/JyW;

    invoke-direct {p1, p0}, LX/JyW;-><init>(LX/JyX;)V

    .line 2754854
    iput-object p1, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 2754855
    return-void
.end method

.method public static a$redex0(LX/JyX;F)V
    .locals 4

    .prologue
    .line 2754856
    invoke-static {p0}, LX/JyX;->f(LX/JyX;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2754857
    :goto_0
    return-void

    .line 2754858
    :cond_0
    iget-object v0, p0, LX/JyX;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2754859
    iget-object v0, p0, LX/JyX;->a:Landroid/view/View;

    neg-float v1, p1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2754860
    :cond_1
    iget-object v0, p0, LX/JyX;->b:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    const/high16 p0, 0x3f800000    # 1.0f

    .line 2754861
    const/high16 v1, 0x40c00000    # 6.0f

    mul-float/2addr v1, p1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2754862
    iget-object v2, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->h:Lcom/facebook/profile/insight/view/ProfileInsightTitleView;

    neg-float v3, v1

    add-float/2addr v3, p0

    invoke-virtual {v2, v3}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->setAlpha(F)V

    .line 2754863
    iget-object v2, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    neg-float v1, v1

    add-float/2addr v1, p0

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setAlpha(F)V

    .line 2754864
    goto :goto_0
.end method

.method public static e(LX/JyX;)V
    .locals 2

    .prologue
    .line 2754865
    iget-boolean v0, p0, LX/JyX;->d:Z

    if-eqz v0, :cond_0

    .line 2754866
    :goto_0
    return-void

    .line 2754867
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/JyX;->a$redex0(LX/JyX;F)V

    .line 2754868
    iget-object v0, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    const/4 v1, 0x1

    .line 2754869
    iput-boolean v1, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 2754870
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/JyX;->e:Ljava/lang/Integer;

    .line 2754871
    iget-object v0, p0, LX/JyX;->b:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    const/4 p0, 0x0

    .line 2754872
    iget-object v1, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->h:Lcom/facebook/profile/insight/view/ProfileInsightTitleView;

    invoke-virtual {v1, p0}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->setVisibility(I)V

    .line 2754873
    iget-object v1, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2754874
    goto :goto_0
.end method

.method public static f(LX/JyX;)Z
    .locals 1

    .prologue
    .line 2754875
    iget-object v0, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
