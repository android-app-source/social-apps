.class public final LX/K3e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/K3f;


# direct methods
.method public constructor <init>(LX/K3f;)V
    .locals 0

    .prologue
    .line 2765940
    iput-object p1, p0, LX/K3e;->a:LX/K3f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2765941
    const/4 v4, 0x1

    const/4 v1, -0x1

    const/4 v5, 0x0

    .line 2765942
    iget-object v0, p0, LX/K3e;->a:LX/K3f;

    new-instance v2, Landroid/media/MediaMuxer;

    iget-object v3, p0, LX/K3e;->a:LX/K3f;

    iget-object v3, v3, LX/K3f;->h:LX/K3g;

    iget-object v3, v3, LX/K3g;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v5}, Landroid/media/MediaMuxer;-><init>(Ljava/lang/String;I)V

    .line 2765943
    iput-object v2, v0, LX/K3f;->g:Landroid/media/MediaMuxer;

    .line 2765944
    iget-object v0, p0, LX/K3e;->a:LX/K3f;

    iget-object v0, v0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    move v2, v1

    move v3, v4

    .line 2765945
    :goto_0
    :pswitch_0
    if-eqz v3, :cond_1

    .line 2765946
    iget-object v6, p0, LX/K3e;->a:LX/K3f;

    iget-object v6, v6, LX/K3f;->f:Landroid/media/MediaCodec;

    iget-object v7, p0, LX/K3e;->a:LX/K3f;

    iget-object v7, v7, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v8, -0x1

    invoke-virtual {v6, v7, v8, v9}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v6

    .line 2765947
    packed-switch v6, :pswitch_data_0

    .line 2765948
    iget-object v7, p0, LX/K3e;->a:LX/K3f;

    iget-object v7, v7, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    iget v7, v7, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v7, v7, 0x4

    if-eqz v7, :cond_0

    move v3, v5

    .line 2765949
    goto :goto_0

    .line 2765950
    :pswitch_1
    iget-object v1, p0, LX/K3e;->a:LX/K3f;

    iget-object v1, v1, LX/K3f;->g:Landroid/media/MediaMuxer;

    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v2

    .line 2765951
    iget-object v1, p0, LX/K3e;->a:LX/K3f;

    iget-object v1, v1, LX/K3f;->e:Landroid/media/MediaExtractor;

    invoke-virtual {v1, v5}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 2765952
    iget-object v1, p0, LX/K3e;->a:LX/K3f;

    iget-object v1, v1, LX/K3f;->g:Landroid/media/MediaMuxer;

    iget-object v6, p0, LX/K3e;->a:LX/K3f;

    iget-object v6, v6, LX/K3f;->e:Landroid/media/MediaExtractor;

    invoke-virtual {v6, v5}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v1

    .line 2765953
    iget-object v6, p0, LX/K3e;->a:LX/K3f;

    iget-object v6, v6, LX/K3f;->g:Landroid/media/MediaMuxer;

    invoke-virtual {v6}, Landroid/media/MediaMuxer;->start()V

    .line 2765954
    goto :goto_0

    .line 2765955
    :pswitch_2
    iget-object v0, p0, LX/K3e;->a:LX/K3f;

    iget-object v0, v0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0

    .line 2765956
    :cond_0
    aget-object v7, v0, v6

    .line 2765957
    iget-object v8, p0, LX/K3e;->a:LX/K3f;

    iget-object v8, v8, LX/K3f;->g:Landroid/media/MediaMuxer;

    iget-object v9, p0, LX/K3e;->a:LX/K3f;

    iget-object v9, v9, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    invoke-virtual {v8, v2, v7, v9}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 2765958
    iget-object v7, p0, LX/K3e;->a:LX/K3f;

    iget-object v7, v7, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v7, v6, v5}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    goto :goto_0

    .line 2765959
    :cond_1
    const v0, 0x3d0900

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2765960
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    iput v5, v2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    .line 2765961
    :cond_2
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    iget-object v3, p0, LX/K3e;->a:LX/K3f;

    iget-object v3, v3, LX/K3f;->e:Landroid/media/MediaExtractor;

    invoke-virtual {v3}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v6

    iput-wide v6, v2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 2765962
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    iget-object v6, p0, LX/K3e;->a:LX/K3f;

    iget-object v6, v6, LX/K3f;->h:LX/K3g;

    iget v6, v6, LX/K3g;->f:I

    int-to-long v6, v6

    cmp-long v2, v2, v6

    if-gtz v2, :cond_3

    .line 2765963
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    iget-object v3, p0, LX/K3e;->a:LX/K3f;

    iget-object v3, v3, LX/K3f;->e:Landroid/media/MediaExtractor;

    invoke-virtual {v3, v0, v5}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v3

    iput v3, v2, Landroid/media/MediaCodec$BufferInfo;->size:I

    .line 2765964
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    iget v2, v2, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-ltz v2, :cond_3

    .line 2765965
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->g:Landroid/media/MediaMuxer;

    iget-object v3, p0, LX/K3e;->a:LX/K3f;

    iget-object v3, v3, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    invoke-virtual {v2, v1, v0, v3}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 2765966
    iget-object v2, p0, LX/K3e;->a:LX/K3f;

    iget-object v2, v2, LX/K3f;->e:Landroid/media/MediaExtractor;

    invoke-virtual {v2}, Landroid/media/MediaExtractor;->advance()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2765967
    :cond_3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
