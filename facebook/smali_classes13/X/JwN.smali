.class public final LX/JwN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cdn;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JwS;


# direct methods
.method public constructor <init>(LX/JwS;)V
    .locals 0

    .prologue
    .line 2751811
    iput-object p1, p0, LX/JwN;->a:LX/JwS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2751812
    iget-object v0, p0, LX/JwN;->a:LX/JwS;

    iget-object v0, v0, LX/JwS;->e:LX/2cw;

    sget-object v1, LX/3FC;->END_BLE_SCAN_FAIL:LX/3FC;

    sget-object v2, LX/2cx;->BLE:LX/2cx;

    invoke-virtual {v0, v1, v2, p1}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/Throwable;)V

    .line 2751813
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2751814
    check-cast p1, LX/Cdn;

    .line 2751815
    iget-object v0, p0, LX/JwN;->a:LX/JwS;

    iget-object v0, v0, LX/JwS;->e:LX/2cw;

    .line 2751816
    sget-object v1, LX/3FC;->END_BLE_SCAN_SUCCESS:LX/3FC;

    .line 2751817
    invoke-virtual {v1}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2751818
    const-string v3, "ble_scan_result"

    invoke-static {p1}, LX/2cw;->b(LX/Cdn;)LX/0lF;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2751819
    sget-object v3, LX/2cx;->BLE:LX/2cx;

    invoke-static {v0, v1, v2, v3}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V

    .line 2751820
    return-void
.end method
