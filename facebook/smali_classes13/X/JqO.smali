.class public final LX/JqO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7G5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7G5",
        "<",
        "LX/76M",
        "<",
        "LX/7GC;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/JqR;


# direct methods
.method public constructor <init>(LX/JqR;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2738896
    iput-object p1, p0, LX/JqO;->c:LX/JqR;

    iput-wide p2, p0, LX/JqO;->a:J

    iput-object p4, p0, LX/JqO;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/7Gc;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/7Gc",
            "<",
            "LX/76M",
            "<",
            "LX/7GC;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2738897
    iget-object v0, p0, LX/JqO;->c:LX/JqR;

    iget-object v0, v0, LX/JqR;->b:LX/JqU;

    iget-wide v2, p0, LX/JqO;->a:J

    iget-object v1, p0, LX/JqO;->b:Ljava/lang/String;

    .line 2738898
    new-instance v6, LX/JqT;

    invoke-direct {v6, v0}, LX/JqT;-><init>(LX/JqU;)V

    .line 2738899
    iget-object v4, v0, LX/JqU;->i:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v4, v0, LX/JqU;->g:LX/7GS;

    const-string v7, "/t_ms"

    invoke-virtual {v4, v7, v6}, LX/7GS;->a(Ljava/lang/String;LX/7GQ;)LX/76J;

    move-result-object v8

    move-object v4, v0

    move-wide v6, v2

    move-object v9, v1

    invoke-virtual/range {v4 .. v9}, LX/7GD;->a(IJLX/76J;Ljava/lang/String;)LX/76M;

    move-result-object v4

    move-object v1, v4

    .line 2738900
    iget-boolean v0, v1, LX/76M;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, LX/76M;->b:Ljava/lang/Object;

    check-cast v0, LX/7GC;

    iget-boolean v0, v0, LX/7GC;->a:Z

    if-eqz v0, :cond_0

    .line 2738901
    new-instance v0, LX/7Gc;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    .line 2738902
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/7Gc;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2738903
    iget-object v0, p0, LX/JqO;->c:LX/JqR;

    iget-object v0, v0, LX/JqR;->l:LX/7G0;

    .line 2738904
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "sync_create_queue_error"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2738905
    const-string v2, "backOffAmount"

    invoke-virtual {v1, v2, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2738906
    iget-object v2, v0, LX/7G0;->a:LX/7G1;

    sget-object p0, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v2, v1, p0}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 2738907
    return-void
.end method
