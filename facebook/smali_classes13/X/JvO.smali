.class public final LX/JvO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;)V
    .locals 0

    .prologue
    .line 2750320
    iput-object p1, p0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/JvO;LX/JvZ;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 5

    .prologue
    .line 2750321
    iget-object v0, p0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v1, LX/0ig;->aq:LX/0ih;

    iget-object v2, p0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-wide v2, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    const-string v4, "voice_switch_succeed"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750322
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2750323
    const-string v1, "extra_composer_page_data"

    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    iget-object v3, p1, LX/JvZ;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    iget-object v3, p1, LX/JvZ;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2750324
    iget-object v1, p0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2750325
    iget-object v0, p0, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2750326
    return-void
.end method
