.class public LX/Jzk;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "DatePickerAndroid"
.end annotation


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2756671
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2756672
    return-void
.end method

.method public static synthetic a(LX/Jzk;)LX/5pY;
    .locals 1

    .prologue
    .line 2756669
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756670
    return-object v0
.end method

.method private static a(LX/5pG;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2756673
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2756674
    const-string v1, "date"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "date"

    invoke-interface {p0, v1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2756675
    const-string v1, "date"

    const-string v2, "date"

    invoke-interface {p0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2756676
    :cond_0
    const-string v1, "minDate"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "minDate"

    invoke-interface {p0, v1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2756677
    const-string v1, "minDate"

    const-string v2, "minDate"

    invoke-interface {p0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2756678
    :cond_1
    const-string v1, "maxDate"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "maxDate"

    invoke-interface {p0, v1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2756679
    const-string v1, "maxDate"

    const-string v2, "maxDate"

    invoke-interface {p0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2756680
    :cond_2
    const-string v1, "mode"

    invoke-interface {p0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "mode"

    invoke-interface {p0, v1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2756681
    const-string v1, "mode"

    const-string v2, "mode"

    invoke-interface {p0, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756682
    :cond_3
    return-object v0
.end method

.method public static synthetic b(LX/Jzk;)LX/5pY;
    .locals 1

    .prologue
    .line 2756667
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2756668
    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2756666
    const-string v0, "DatePickerAndroid"

    return-object v0
.end method

.method public open(LX/5pG;LX/5pW;)V
    .locals 3
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2756637
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2756638
    if-nez v0, :cond_0

    .line 2756639
    const-string v0, "E_NO_ACTIVITY"

    const-string v1, "Tried to open a DatePicker dialog while not attached to an Activity"

    invoke-interface {p2, v0, v1}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756640
    :goto_0
    return-void

    .line 2756641
    :cond_0
    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_3

    .line 2756642
    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2756643
    const-string v0, "DatePickerAndroid"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 2756644
    if-eqz v0, :cond_1

    .line 2756645
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2756646
    :cond_1
    new-instance v0, Lcom/facebook/react/modules/datepicker/SupportDatePickerDialogFragment;

    invoke-direct {v0}, Lcom/facebook/react/modules/datepicker/SupportDatePickerDialogFragment;-><init>()V

    .line 2756647
    if-eqz p1, :cond_2

    .line 2756648
    invoke-static {p1}, LX/Jzk;->a(LX/5pG;)Landroid/os/Bundle;

    move-result-object v2

    .line 2756649
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2756650
    :cond_2
    new-instance v2, LX/Jzj;

    invoke-direct {v2, p0, p2}, LX/Jzj;-><init>(LX/Jzk;LX/5pW;)V

    .line 2756651
    iput-object v2, v0, Lcom/facebook/react/modules/datepicker/SupportDatePickerDialogFragment;->k:Landroid/content/DialogInterface$OnDismissListener;

    .line 2756652
    iput-object v2, v0, Lcom/facebook/react/modules/datepicker/SupportDatePickerDialogFragment;->j:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 2756653
    const-string v2, "DatePickerAndroid"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    .line 2756654
    :cond_3
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 2756655
    const-string v0, "DatePickerAndroid"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 2756656
    if-eqz v0, :cond_4

    .line 2756657
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 2756658
    :cond_4
    new-instance v0, LX/Jzi;

    invoke-direct {v0}, LX/Jzi;-><init>()V

    .line 2756659
    if-eqz p1, :cond_5

    .line 2756660
    invoke-static {p1}, LX/Jzk;->a(LX/5pG;)Landroid/os/Bundle;

    move-result-object v2

    .line 2756661
    invoke-virtual {v0, v2}, LX/Jzi;->setArguments(Landroid/os/Bundle;)V

    .line 2756662
    :cond_5
    new-instance v2, LX/Jzj;

    invoke-direct {v2, p0, p2}, LX/Jzj;-><init>(LX/Jzk;LX/5pW;)V

    .line 2756663
    iput-object v2, v0, LX/Jzi;->b:Landroid/content/DialogInterface$OnDismissListener;

    .line 2756664
    iput-object v2, v0, LX/Jzi;->a:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 2756665
    const-string v2, "DatePickerAndroid"

    invoke-virtual {v0, v1, v2}, LX/Jzi;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
