.class public LX/JYX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JYY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2706563
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JYX;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JYY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706564
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2706565
    iput-object p1, p0, LX/JYX;->b:LX/0Ot;

    .line 2706566
    return-void
.end method

.method public static onClick(LX/1X1;Landroid/view/View$OnClickListener;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2706567
    const v0, -0x668c099

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2706568
    check-cast p2, LX/JYW;

    .line 2706569
    iget-object v0, p0, LX/JYX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JYW;->a:Landroid/view/View$OnClickListener;

    const/4 p2, 0x2

    const/4 p0, 0x1

    .line 2706570
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f02079a

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0b006f

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0069

    invoke-interface {v1, p0, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0072

    invoke-interface {v1, p2, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b006c

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    .line 2706571
    const v2, -0x668c099

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v0, v3, p2

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2706572
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f083a93

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a0124

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    .line 2706573
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2706574
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2706575
    invoke-static {}, LX/1dS;->b()V

    .line 2706576
    iget v0, p1, LX/1dQ;->b:I

    .line 2706577
    packed-switch v0, :pswitch_data_0

    .line 2706578
    :goto_0
    return-object v3

    .line 2706579
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2706580
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 2706581
    iget-object p1, p0, LX/JYX;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2706582
    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2706583
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x668c099
        :pswitch_0
    .end packed-switch
.end method
