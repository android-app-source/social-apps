.class public LX/JvZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2750536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750537
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JvZ;->a:Z

    .line 2750538
    iput-object v1, p0, LX/JvZ;->b:Ljava/lang/String;

    .line 2750539
    iput-object v1, p0, LX/JvZ;->c:Ljava/lang/String;

    .line 2750540
    iput-object v1, p0, LX/JvZ;->d:Ljava/lang/String;

    .line 2750541
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2750542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750543
    iput-boolean v2, p0, LX/JvZ;->a:Z

    .line 2750544
    iput-object p1, p0, LX/JvZ;->b:Ljava/lang/String;

    .line 2750545
    iput-object p2, p0, LX/JvZ;->c:Ljava/lang/String;

    .line 2750546
    iput-object p3, p0, LX/JvZ;->d:Ljava/lang/String;

    .line 2750547
    iget-object v0, p0, LX/JvZ;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Page Id is invalid."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2750548
    iget-object v0, p0, LX/JvZ;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Page name is invalid."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2750549
    iget-object v0, p0, LX/JvZ;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    const-string v0, "Profile picture url is invalid."

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2750550
    return-void

    :cond_0
    move v0, v2

    .line 2750551
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2750552
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2750553
    goto :goto_2
.end method

.method public static a()LX/JvZ;
    .locals 1

    .prologue
    .line 2750554
    new-instance v0, LX/JvZ;

    invoke-direct {v0}, LX/JvZ;-><init>()V

    return-object v0
.end method
