.class public LX/K91;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/K91;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Vt;

.field public final c:LX/0W9;

.field public final d:LX/03V;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/K8y;",
            "LX/K8z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Vt;LX/0W9;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2776543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776544
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/K91;->e:Ljava/util/Map;

    .line 2776545
    iput-object p1, p0, LX/K91;->a:LX/0ad;

    .line 2776546
    iput-object p2, p0, LX/K91;->b:LX/0Vt;

    .line 2776547
    iput-object p3, p0, LX/K91;->c:LX/0W9;

    .line 2776548
    iput-object p4, p0, LX/K91;->d:LX/03V;

    .line 2776549
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->PRODUCT_NAME:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->C:C

    const p3, 0x7f083a95

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776550
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->PRODUCT_NAME_LOWER_CASE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->B:C

    const p3, 0x7f083a95

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776551
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->PRODUCT_NAME_ALL_CAPS:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->A:C

    const p3, 0x7f083a95

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776552
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_UNCONNECTED_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->m:C

    const p3, 0x7f083a9a

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776553
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_UNSUBSCRIBED_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->o:C

    const p3, 0x7f083a9b

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776554
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->COVER_CARD_READ_STORY_CTA:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->a:C

    const p3, 0x7f083a96

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776555
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->t:C

    const p3, 0x7f083a98

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776556
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->s:C

    const p3, 0x7f083a99

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776557
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->FEED_UNIT_CTA:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->p:C

    const p3, 0x7f083ab1

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776558
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_UNFOLLOWED_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->n:C

    const p3, 0x7f083a9d

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776559
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_FOLLOW_CTA_OFF:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->e:C

    const p3, 0x7f083a9f

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776560
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_FOLLOW_CTA_ON:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->f:C

    const p3, 0x7f083aa0

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776561
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_SUBSCRIBE_CTA_OFF:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->j:C

    const p3, 0x7f083aa1

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776562
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_SUBSCRIBE_CTA_ON:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->k:C

    const p3, 0x7f083aa2

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776563
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_CONNECTED_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->d:C

    const p3, 0x7f083aa3

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776564
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_FOLLOWED_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->g:C

    const p3, 0x7f083aa4

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776565
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_UNSUBSCRIBED_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->o:C

    const p3, 0x7f083aa7

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776566
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_SUBSCRIBED_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->l:C

    const p3, 0x7f083aa8

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776567
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_ALREADY_CONNECTED_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->b:C

    const p3, 0x7f083aa5

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776568
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_ALREADY_SUBSCRIBED_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->c:C

    const p3, 0x7f083aa6

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776569
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->i:C

    const p3, 0x7f083aaf

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776570
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->h:C

    const p3, 0x7f083ab0

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776571
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->w:C

    const p3, 0x7f083aa9

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776572
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->u:C

    const p3, 0x7f083aaa

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776573
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_PRIMARY_BUTTON:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->v:C

    const p3, 0x7f083aab

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776574
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_TITLE:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->z:C

    const p3, 0x7f083aac

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776575
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_DESCRIPTION:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->x:C

    const p3, 0x7f083aad

    sget-object p4, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    invoke-direct {p1, p0, p2, p3, p4}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776576
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_PRIMARY_BUTTON:LX/K8y;

    new-instance p1, LX/K8z;

    sget-char p2, LX/K5X;->y:C

    const p3, 0x7f083aae

    invoke-direct {p1, p0, p2, p3}, LX/K8z;-><init>(LX/K91;CI)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776577
    return-void
.end method

.method public static a(LX/0QB;)LX/K91;
    .locals 7

    .prologue
    .line 2776583
    sget-object v0, LX/K91;->f:LX/K91;

    if-nez v0, :cond_1

    .line 2776584
    const-class v1, LX/K91;

    monitor-enter v1

    .line 2776585
    :try_start_0
    sget-object v0, LX/K91;->f:LX/K91;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2776586
    if-eqz v2, :cond_0

    .line 2776587
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2776588
    new-instance p0, LX/K91;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0Vs;->a(LX/0QB;)LX/0Vs;

    move-result-object v4

    check-cast v4, LX/0Vt;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/K91;-><init>(LX/0ad;LX/0Vt;LX/0W9;LX/03V;)V

    .line 2776589
    move-object v0, p0

    .line 2776590
    sput-object v0, LX/K91;->f:LX/K91;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2776591
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2776592
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2776593
    :cond_1
    sget-object v0, LX/K91;->f:LX/K91;

    return-object v0

    .line 2776594
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2776595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2776596
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_UNCONNECTED_TITLE:LX/K8y;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8z;

    invoke-virtual {v0, p1}, LX/K8z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2776581
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->PRODUCT_NAME_ALL_CAPS:LX/K8y;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8z;

    invoke-virtual {v0}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2776582
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_UNFOLLOWED_DESCRIPTION:LX/K8y;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8z;

    invoke-virtual {v0, p1}, LX/K8z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2776580
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_FOLLOWED_DESCRIPTION:LX/K8y;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8z;

    invoke-virtual {v0, p1}, LX/K8z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2776579
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->FEED_UNIT_CTA:LX/K8y;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8z;

    invoke-virtual {v0}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2776578
    iget-object v0, p0, LX/K91;->e:Ljava/util/Map;

    sget-object v1, LX/K8y;->END_SCREEN_CONNECTED_TITLE:LX/K8y;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8z;

    invoke-virtual {v0}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
