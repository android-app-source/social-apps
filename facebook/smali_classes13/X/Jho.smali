.class public LX/Jho;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

.field public B:Landroid/widget/ImageView;

.field public C:Landroid/widget/ImageView;

.field public D:Landroid/view/ViewStub;

.field public E:Landroid/view/View;

.field public F:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public G:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public I:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public J:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public K:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;",
            ">;"
        }
    .end annotation
.end field

.field public M:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterButton;",
            ">;"
        }
    .end annotation
.end field

.field public N:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;",
            ">;"
        }
    .end annotation
.end field

.field public O:LX/3OO;

.field public b:LX/3A0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/EFW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/FJw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/FOX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/JsR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/rtc/annotations/IsVoipVideoEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/11T;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/FCl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zC;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

.field public o:I

.field public p:Landroid/widget/TextView;

.field public q:Landroid/widget/TextView;

.field public r:Landroid/widget/TextView;

.field public s:Lcom/facebook/user/tiles/UserTileView;

.field public t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

.field public u:Landroid/widget/CheckBox;

.field public v:Landroid/widget/CheckBox;

.field public w:Landroid/widget/Button;

.field public x:Landroid/view/View;

.field public y:Landroid/widget/ImageView;

.field public z:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2725521
    const-class v0, LX/Jho;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Jho;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 14

    .prologue
    .line 2725487
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725488
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2725489
    iput-object v0, p0, LX/Jho;->l:LX/0Ot;

    .line 2725490
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2725491
    iput-object v0, p0, LX/Jho;->m:LX/0Ot;

    .line 2725492
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, LX/Jho;

    invoke-static {p1}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v3

    check-cast v3, LX/3A0;

    invoke-static {p1}, LX/EFW;->b(LX/0QB;)LX/EFW;

    move-result-object v4

    check-cast v4, LX/EFW;

    invoke-static {p1}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object v5

    check-cast v5, LX/FJw;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/FOX;->b(LX/0QB;)LX/FOX;

    move-result-object v8

    check-cast v8, LX/FOX;

    new-instance v11, LX/JsR;

    invoke-static {p1}, LX/2RJ;->b(LX/0QB;)LX/2RJ;

    move-result-object v9

    check-cast v9, LX/2RJ;

    const-class v10, Landroid/content/Context;

    invoke-interface {p1, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-direct {v11, v9, v10}, LX/JsR;-><init>(LX/2RJ;Landroid/content/Context;)V

    move-object v9, v11

    check-cast v9, LX/JsR;

    const/16 v10, 0x1561

    invoke-static {p1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p1}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v11

    check-cast v11, LX/11T;

    invoke-static {p1}, LX/FCl;->a(LX/0QB;)LX/FCl;

    move-result-object v12

    check-cast v12, LX/FCl;

    const/16 v13, 0x12ac

    invoke-static {p1, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v0, 0x29b7

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, LX/Jho;->b:LX/3A0;

    iput-object v4, v2, LX/Jho;->c:LX/EFW;

    iput-object v5, v2, LX/Jho;->d:LX/FJw;

    iput-object v6, v2, LX/Jho;->e:LX/0ad;

    iput-object v7, v2, LX/Jho;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, LX/Jho;->g:LX/FOX;

    iput-object v9, v2, LX/Jho;->h:LX/JsR;

    iput-object v10, v2, LX/Jho;->i:LX/0Or;

    iput-object v11, v2, LX/Jho;->j:LX/11T;

    iput-object v12, v2, LX/Jho;->k:LX/FCl;

    iput-object v13, v2, LX/Jho;->l:LX/0Ot;

    iput-object p1, v2, LX/Jho;->m:LX/0Ot;

    .line 2725493
    const v0, 0x7f030cde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725494
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iput-object v0, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2725495
    iget-object v0, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v0}, LX/2Wj;->getTextColor()I

    move-result v0

    iput v0, p0, LX/Jho;->o:I

    .line 2725496
    const v0, 0x7f0d2014

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jho;->p:Landroid/widget/TextView;

    .line 2725497
    const v0, 0x7f0d2015

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jho;->q:Landroid/widget/TextView;

    .line 2725498
    const v0, 0x7f0d2016

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jho;->r:Landroid/widget/TextView;

    .line 2725499
    const v0, 0x7f0d1e99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Jho;->s:Lcom/facebook/user/tiles/UserTileView;

    .line 2725500
    const v0, 0x7f0d2017

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/presence/PresenceIndicatorView;

    iput-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    .line 2725501
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/Jho;->u:Landroid/widget/CheckBox;

    .line 2725502
    const v0, 0x7f0d2019

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    .line 2725503
    const v0, 0x7f0d2018

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/Jho;->w:Landroid/widget/Button;

    .line 2725504
    const v0, 0x7f0d2004

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->L:LX/4ob;

    .line 2725505
    const v0, 0x7f0d2003

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->M:LX/4ob;

    .line 2725506
    const v0, 0x7f0d037e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->x:Landroid/view/View;

    .line 2725507
    const v0, 0x7f0d201f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Jho;->y:Landroid/widget/ImageView;

    .line 2725508
    const v0, 0x7f0d201d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->F:LX/4ob;

    .line 2725509
    const v0, 0x7f0d201a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Jho;->z:Landroid/widget/ImageView;

    .line 2725510
    const v0, 0x7f0d201c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iput-object v0, p0, LX/Jho;->A:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    .line 2725511
    const v0, 0x7f0d201b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Jho;->B:Landroid/widget/ImageView;

    .line 2725512
    const v0, 0x7f0d2020

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Jho;->C:Landroid/widget/ImageView;

    .line 2725513
    const v0, 0x7f0d2005

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Jho;->D:Landroid/view/ViewStub;

    .line 2725514
    const v0, 0x7f0d1be0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->G:LX/4ob;

    .line 2725515
    const v0, 0x7f0d2012

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->H:LX/4ob;

    .line 2725516
    const v0, 0x7f0d2013

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->I:LX/4ob;

    .line 2725517
    const v0, 0x7f0d2010

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->J:LX/4ob;

    .line 2725518
    const v0, 0x7f0d201e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->K:LX/4ob;

    .line 2725519
    const v0, 0x7f0d1be2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jho;->N:LX/4ob;

    .line 2725520
    return-void
.end method

.method private static A(LX/Jho;)Z
    .locals 2

    .prologue
    .line 2725484
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725485
    iget-object v1, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v1

    .line 2725486
    sget-object v1, LX/3ON;->MESSENGER_TAB:LX/3ON;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static C(LX/Jho;)Z
    .locals 2

    .prologue
    .line 2725481
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725482
    iget-object v1, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v1

    .line 2725483
    sget-object v1, LX/3ON;->TWO_LINE:LX/3ON;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static E(LX/Jho;)Z
    .locals 2

    .prologue
    .line 2725470
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725471
    iget-object v1, v0, LX/3OO;->q:LX/6Lx;

    move-object v0, v1

    .line 2725472
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725473
    iget-object v1, v0, LX/3OO;->q:LX/6Lx;

    move-object v0, v1

    .line 2725474
    invoke-interface {v0}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2725475
    :cond_0
    const/4 v0, 0x0

    .line 2725476
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725477
    iget-object v1, v0, LX/3OO;->q:LX/6Lx;

    move-object v0, v1

    .line 2725478
    invoke-interface {v0}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    .line 2725479
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    if-ne v1, p0, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2725480
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static G(LX/Jho;)Z
    .locals 1

    .prologue
    .line 2725467
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725468
    iget-object p0, v0, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v0, p0

    .line 2725469
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->ax()Z

    move-result v0

    return v0
.end method

.method private H()V
    .locals 15

    .prologue
    const/4 v3, 0x0

    .line 2725442
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0}, LX/3OO;->Q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2725443
    const v0, 0x7f0d1ff5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2725444
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v1}, LX/3OO;->O()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2725445
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2725446
    const v0, 0x7f0d1ff6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2725447
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725448
    iget-object v3, v1, LX/3OO;->k:Ljava/lang/String;

    move-object v1, v3

    .line 2725449
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2725450
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 2725451
    const v0, 0x7f0d1ff7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2725452
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725453
    iget-wide v6, v1, LX/3OO;->p:J

    move-wide v4, v6

    .line 2725454
    const-wide/16 v12, 0x3c

    const/4 v9, 0x1

    const/4 v11, 0x0

    .line 2725455
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    .line 2725456
    cmp-long v6, v4, v12

    if-ltz v6, :cond_0

    .line 2725457
    div-long v6, v4, v12

    long-to-int v6, v6

    .line 2725458
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f0025

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2725459
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2725460
    :goto_1
    move-object v1, v6

    .line 2725461
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2725462
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 2725463
    return-void

    .line 2725464
    :cond_0
    long-to-int v6, v4

    .line 2725465
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f0024

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2725466
    :cond_1
    const-string v6, ""

    goto :goto_1
.end method

.method public static a(LX/Jho;Landroid/widget/ImageView;)V
    .locals 6

    .prologue
    .line 2725415
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725416
    iget-boolean v1, v0, LX/3OO;->C:Z

    move v0, v1

    .line 2725417
    if-eqz v0, :cond_0

    .line 2725418
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725419
    iget-object v1, v0, LX/3OO;->F:Ljava/lang/String;

    move-object v0, v1

    .line 2725420
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2725421
    new-instance v0, LX/Jhj;

    invoke-direct {v0, p0}, LX/Jhj;-><init>(LX/Jho;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725422
    invoke-static {p0}, LX/Jho;->G(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2725423
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2725424
    const v1, 0x7f020f71

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2725425
    :goto_0
    move-object v0, v0

    .line 2725426
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2725427
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725428
    :goto_1
    return-void

    .line 2725429
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 2725430
    :cond_1
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725431
    iget-boolean v1, v0, LX/3OO;->g:Z

    move v0, v1

    .line 2725432
    if-eqz v0, :cond_3

    .line 2725433
    iget-object v0, p0, LX/Jho;->c:LX/EFW;

    .line 2725434
    iget-object v1, v0, LX/EFW;->i:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 2725435
    invoke-virtual {v0}, LX/EFW;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2725436
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    .line 2725437
    new-instance v3, LX/EFg;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    iget p0, v0, LX/EFW;->d:I

    invoke-direct {v3, v4, v5, v2, p0}, LX/EFg;-><init>(IIII)V

    move-object v2, v3

    .line 2725438
    new-instance v3, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    invoke-direct {v3, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v3, v0, LX/EFW;->i:Landroid/graphics/drawable/Drawable;

    .line 2725439
    :cond_2
    iget-object v1, v0, LX/EFW;->i:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 2725440
    goto :goto_0

    .line 2725441
    :cond_3
    iget-object v0, p0, LX/Jho;->c:LX/EFW;

    invoke-virtual {v0}, LX/EFW;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static b$redex0(LX/Jho;)V
    .locals 14

    .prologue
    const/4 v0, 0x1

    const/16 v7, 0x8

    const/4 v1, 0x0

    .line 2725183
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725184
    iget-object v3, v2, LX/3OO;->r:LX/3OI;

    move-object v2, v3

    .line 2725185
    sget-object v3, LX/3OH;->AGGREGATE_CALL_DETAILS:LX/3OH;

    if-ne v2, v3, :cond_13

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2725186
    if-nez v2, :cond_12

    .line 2725187
    iget-object v2, p0, LX/Jho;->J:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->d()V

    .line 2725188
    iget-object v2, p0, LX/Jho;->s:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v2, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2725189
    iget-object v2, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    .line 2725190
    iget-object v2, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2725191
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725192
    iget-object v3, v2, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v3, v3

    .line 2725193
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725194
    iget-boolean v4, v2, LX/3OO;->K:Z

    move v2, v4

    .line 2725195
    if-eqz v2, :cond_6

    .line 2725196
    iget-object v2, p0, LX/Jho;->s:Lcom/facebook/user/tiles/UserTileView;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2725197
    :goto_1
    const/4 v8, 0x0

    .line 2725198
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725199
    iget-boolean v4, v2, LX/3OO;->K:Z

    move v2, v4

    .line 2725200
    if-eqz v2, :cond_14

    .line 2725201
    iget-object v2, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    .line 2725202
    :goto_2
    iget-object v2, p0, LX/Jho;->A:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    .line 2725203
    iget-object v5, v4, LX/3OO;->m:LX/3OL;

    move-object v4, v5

    .line 2725204
    invoke-virtual {v2, v4}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setWaveState(LX/3OL;)V

    .line 2725205
    iget-object v2, p0, LX/Jho;->A:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    .line 2725206
    iget-object v5, v4, LX/3OO;->n:LX/DHx;

    move-object v4, v5

    .line 2725207
    iput-object v4, v2, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    .line 2725208
    invoke-direct {p0}, LX/Jho;->j()V

    .line 2725209
    invoke-direct {p0}, LX/Jho;->l()V

    .line 2725210
    iget-object v2, p0, LX/Jho;->r:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725211
    iget-object v4, p0, LX/Jho;->r:Landroid/widget/TextView;

    const-string v2, ""

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x8

    :goto_3
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2725212
    invoke-direct {p0}, LX/Jho;->k()V

    .line 2725213
    const/4 v4, 0x0

    const/16 v2, 0x8

    .line 2725214
    invoke-static {p0}, LX/Jho;->x(LX/Jho;)Z

    move-result v5

    if-nez v5, :cond_1b

    iget-object v5, p0, LX/Jho;->O:LX/3OO;

    .line 2725215
    iget-boolean v6, v5, LX/3OO;->u:Z

    move v5, v6

    .line 2725216
    if-eqz v5, :cond_1b

    .line 2725217
    iget-object v5, p0, LX/Jho;->u:Landroid/widget/CheckBox;

    invoke-static {p0}, LX/Jho;->w(LX/Jho;)Z

    move-result v6

    if-eqz v6, :cond_1a

    :goto_4
    invoke-virtual {v5, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2725218
    iget-object v2, p0, LX/Jho;->u:Landroid/widget/CheckBox;

    iget-object v5, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v5}, LX/3OP;->a()Z

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2725219
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2725220
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v4}, LX/3OP;->a()Z

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2725221
    :goto_5
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2725222
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725223
    iget-object v4, v2, LX/3OO;->y:LX/DAe;

    move-object v2, v4

    .line 2725224
    if-eqz v2, :cond_1c

    .line 2725225
    iget-object v4, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    new-instance v5, LX/Jhl;

    invoke-direct {v5, p0, v2}, LX/Jhl;-><init>(LX/Jho;LX/DAe;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725226
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 2725227
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 2725228
    :goto_6
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725229
    iget-boolean v4, v2, LX/3OO;->t:Z

    move v2, v4

    .line 2725230
    if-nez v2, :cond_1d

    .line 2725231
    iget-object v2, p0, LX/Jho;->E:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 2725232
    iget-object v2, p0, LX/Jho;->E:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2725233
    :cond_0
    :goto_7
    const/4 v2, 0x0

    .line 2725234
    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    .line 2725235
    iget-object v5, v4, LX/3OO;->r:LX/3OI;

    move-object v4, v5

    .line 2725236
    sget-object v5, LX/3OH;->SELF_PROFILE:LX/3OH;

    if-ne v4, v5, :cond_1f

    const/4 v4, 0x1

    .line 2725237
    :goto_8
    if-eqz v4, :cond_1

    .line 2725238
    iget-object v5, p0, LX/Jho;->C:Landroid/widget/ImageView;

    const v6, 0x7f020169

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2725239
    :cond_1
    iget-object v5, p0, LX/Jho;->C:Landroid/widget/ImageView;

    if-eqz v4, :cond_20

    :goto_9
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725240
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725241
    iget-boolean v4, v2, LX/3OO;->J:Z

    move v2, v4

    .line 2725242
    if-eqz v2, :cond_21

    .line 2725243
    iget-object v2, p0, LX/Jho;->H:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->e()V

    .line 2725244
    :goto_a
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725245
    iget-object v4, v2, LX/3OO;->R:Lcom/facebook/user/model/User;

    move-object v2, v4

    .line 2725246
    if-eqz v2, :cond_22

    .line 2725247
    iget-object v2, p0, LX/Jho;->N:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;

    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    .line 2725248
    iget-object v5, v4, LX/3OO;->R:Lcom/facebook/user/model/User;

    move-object v4, v5

    .line 2725249
    invoke-virtual {v2, v4}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->setUser(Lcom/facebook/user/model/User;)V

    .line 2725250
    iget-object v2, p0, LX/Jho;->N:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;

    new-instance v4, LX/Jhm;

    invoke-direct {v4, p0}, LX/Jhm;-><init>(LX/Jho;)V

    .line 2725251
    iput-object v4, v2, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->d:LX/FOQ;

    .line 2725252
    iget-object v2, p0, LX/Jho;->N:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->e()V

    .line 2725253
    :goto_b
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725254
    iget-boolean v4, v2, LX/3OO;->P:Z

    move v2, v4

    .line 2725255
    if-eqz v2, :cond_2

    invoke-static {p0}, LX/Jho;->u(LX/Jho;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2725256
    :cond_2
    iget-object v0, p0, LX/Jho;->G:LX/4ob;

    .line 2725257
    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725258
    :goto_c
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0}, LX/3OO;->P()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2725259
    iget-object v2, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0}, LX/3OO;->P()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, LX/2Wj;->setTextColor(I)V

    .line 2725260
    :cond_3
    invoke-static {p0}, LX/Jho;->C(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2725261
    iget-object v2, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0}, LX/3OO;->Q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2725262
    :cond_4
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725263
    iget-object v2, v0, LX/3OO;->B:LX/DAf;

    move-object v0, v2

    .line 2725264
    if-eqz v0, :cond_10

    .line 2725265
    iget-object v0, p0, LX/Jho;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725266
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020fae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2725267
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2725268
    iget-object v1, p0, LX/Jho;->y:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2725269
    iget-object v0, p0, LX/Jho;->x:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2725270
    iget-object v0, p0, LX/Jho;->y:Landroid/widget/ImageView;

    new-instance v1, LX/Jhd;

    invoke-direct {v1, p0, v3}, LX/Jhd;-><init>(LX/Jho;Lcom/facebook/user/model/User;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725271
    :goto_d
    iget-object v0, p0, LX/Jho;->z:Landroid/widget/ImageView;

    invoke-static {p0, v0}, LX/Jho;->a(LX/Jho;Landroid/widget/ImageView;)V

    .line 2725272
    iget-object v0, p0, LX/Jho;->B:Landroid/widget/ImageView;

    .line 2725273
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725274
    iget-boolean v2, v1, LX/3OO;->D:Z

    move v1, v2

    .line 2725275
    if-eqz v1, :cond_23

    iget-object v1, p0, LX/Jho;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 2725276
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725277
    iget-object v2, v1, LX/3OO;->G:Ljava/lang/String;

    move-object v1, v2

    .line 2725278
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2725279
    new-instance v1, LX/Jhk;

    invoke-direct {v1, p0}, LX/Jhk;-><init>(LX/Jho;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725280
    iget-object v1, p0, LX/Jho;->c:LX/EFW;

    invoke-virtual {v1}, LX/EFW;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2725281
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725282
    :goto_e
    const/4 v0, 0x0

    .line 2725283
    invoke-static {p0}, LX/Jho;->x(LX/Jho;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 2725284
    iget-object v1, p0, LX/Jho;->w:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 2725285
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v1}, LX/3OP;->a()Z

    move-result v1

    .line 2725286
    iget-object v2, p0, LX/Jho;->w:Landroid/widget/Button;

    if-nez v1, :cond_5

    const/4 v0, 0x1

    :cond_5
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2725287
    iget-object v2, p0, LX/Jho;->w:Landroid/widget/Button;

    if-eqz v1, :cond_24

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080673

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_f
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2725288
    iget-object v0, p0, LX/Jho;->w:Landroid/widget/Button;

    invoke-static {p0, v0}, LX/Jho;->setPropagateToRowClickOnClickListener(LX/Jho;Landroid/widget/Button;)V

    .line 2725289
    :goto_10
    const/4 v1, 0x0

    .line 2725290
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725291
    iget-object v2, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v2

    .line 2725292
    sget-object v2, LX/3ON;->SINGLE_TAP_SEND_WITH_UNDO:LX/3ON;

    if-ne v0, v2, :cond_29

    const/4 v0, 0x1

    :goto_11
    move v0, v0

    .line 2725293
    if-eqz v0, :cond_26

    .line 2725294
    iget-object v0, p0, LX/Jho;->L:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;

    .line 2725295
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->setVisibility(I)V

    .line 2725296
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725297
    iget-object v2, v1, LX/3OO;->z:LX/Ddk;

    move-object v1, v2

    .line 2725298
    iput-object v1, v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a:LX/Ddk;

    .line 2725299
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->setRow(LX/3OP;)V

    .line 2725300
    :goto_12
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725301
    iget-boolean v1, v0, LX/3OO;->Q:Z

    move v0, v1

    .line 2725302
    if-eqz v0, :cond_2b

    .line 2725303
    iget-object v0, p0, LX/Jho;->F:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2725304
    iget-object v0, p0, LX/Jho;->F:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725305
    iget-boolean v2, v1, LX/3OO;->w:Z

    move v1, v2

    .line 2725306
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2725307
    iget-object v0, p0, LX/Jho;->F:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, LX/Jhg;

    invoke-direct {v1, p0}, LX/Jhg;-><init>(LX/Jho;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725308
    :goto_13
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725309
    iget-object v1, v0, LX/3OO;->N:LX/EFb;

    move-object v1, v1

    .line 2725310
    if-nez v1, :cond_2c

    .line 2725311
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725312
    :goto_14
    return-void

    .line 2725313
    :cond_6
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725314
    iget-boolean v4, v2, LX/3OO;->e:Z

    move v2, v4

    .line 2725315
    if-nez v2, :cond_7

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725316
    iget-boolean v4, v2, LX/3OO;->c:Z

    move v2, v4

    .line 2725317
    if-nez v2, :cond_7

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725318
    iget-boolean v4, v2, LX/3OO;->d:Z

    move v2, v4

    .line 2725319
    if-eqz v2, :cond_8

    :cond_7
    move v2, v0

    .line 2725320
    :goto_15
    if-eqz v2, :cond_9

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725321
    iget-boolean v4, v2, LX/3OO;->Q:Z

    move v2, v4

    .line 2725322
    if-eqz v2, :cond_9

    .line 2725323
    iget-object v2, p0, LX/Jho;->s:Lcom/facebook/user/tiles/UserTileView;

    .line 2725324
    iget-object v4, v3, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2725325
    sget-object v5, LX/8ue;->ACTIVE_NOW:LX/8ue;

    invoke-static {v4, v5}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 2725326
    goto :goto_15

    .line 2725327
    :cond_9
    iget-object v2, p0, LX/Jho;->s:Lcom/facebook/user/tiles/UserTileView;

    iget-object v4, p0, LX/Jho;->d:LX/FJw;

    invoke-virtual {v4, v3}, LX/FJw;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    goto/16 :goto_1

    .line 2725328
    :cond_a
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v2}, LX/3OO;->S()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2725329
    iget-object v2, p0, LX/Jho;->h:LX/JsR;

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Jho;->G:LX/4ob;

    iget-object v6, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_b

    iget-object v6, p0, LX/Jho;->H:LX/4ob;

    invoke-virtual {v6}, LX/4ob;->c()Z

    move-result v6

    if-eqz v6, :cond_c

    :cond_b
    :goto_16
    const v6, 0x7f0e0539

    .line 2725330
    const/4 v12, 0x1

    move-object v8, v2

    move-object v9, v4

    move-object v10, v5

    move v11, v0

    move v13, v6

    invoke-static/range {v8 .. v13}, LX/JsR;->a(LX/JsR;Ljava/lang/String;LX/4ob;ZZI)V

    .line 2725331
    goto/16 :goto_c

    :cond_c
    move v0, v1

    goto :goto_16

    .line 2725332
    :cond_d
    iget-object v2, p0, LX/Jho;->h:LX/JsR;

    .line 2725333
    iget-object v4, v3, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2725334
    iget-object v5, p0, LX/Jho;->G:LX/4ob;

    iget-object v6, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_e

    iget-object v6, p0, LX/Jho;->H:LX/4ob;

    invoke-virtual {v6}, LX/4ob;->c()Z

    move-result v6

    if-eqz v6, :cond_f

    :cond_e
    :goto_17
    const v6, 0x7f0e0539

    .line 2725335
    const/4 v12, 0x0

    move-object v8, v2

    move-object v9, v4

    move-object v10, v5

    move v11, v0

    move v13, v6

    invoke-static/range {v8 .. v13}, LX/JsR;->a(LX/JsR;Ljava/lang/String;LX/4ob;ZZI)V

    .line 2725336
    goto/16 :goto_c

    :cond_f
    move v0, v1

    goto :goto_17

    .line 2725337
    :cond_10
    iget-object v0, p0, LX/Jho;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725338
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725339
    iget-object v2, v0, LX/3OO;->x:LX/DAd;

    move-object v0, v2

    .line 2725340
    if-eqz v0, :cond_11

    .line 2725341
    iget-object v2, p0, LX/Jho;->x:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2725342
    iget-object v1, p0, LX/Jho;->x:Landroid/view/View;

    new-instance v2, LX/Jhf;

    invoke-direct {v2, p0, v0}, LX/Jhf;-><init>(LX/Jho;LX/DAd;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_d

    .line 2725343
    :cond_11
    iget-object v0, p0, LX/Jho;->x:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_d

    .line 2725344
    :cond_12
    iget-object v0, p0, LX/Jho;->s:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v7}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2725345
    iget-object v0, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    .line 2725346
    iget-object v0, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2725347
    iget-object v0, p0, LX/Jho;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725348
    iget-object v0, p0, LX/Jho;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2725349
    iget-object v0, p0, LX/Jho;->F:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725350
    iget-object v0, p0, LX/Jho;->G:LX/4ob;

    .line 2725351
    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725352
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725353
    iget-object v0, p0, LX/Jho;->J:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2725354
    invoke-direct {p0}, LX/Jho;->H()V

    goto/16 :goto_14

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2725355
    :cond_14
    invoke-static {p0}, LX/Jho;->w(LX/Jho;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2725356
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v2}, LX/3OP;->a()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2725357
    iget-object v2, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a019a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, LX/2Wj;->setTextColor(I)V

    .line 2725358
    :cond_15
    :goto_18
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725359
    iget-object v4, v2, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v2, v4

    .line 2725360
    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    .line 2725361
    iget-boolean v5, v4, LX/3OO;->H:Z

    move v4, v5

    .line 2725362
    if-eqz v4, :cond_18

    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080184

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2725363
    :goto_19
    iget-object v4, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v4}, LX/3OO;->S()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 2725364
    new-instance v4, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725365
    iget-object v5, v2, LX/3OO;->L:Ljava/lang/String;

    move-object v2, v5

    .line 2725366
    invoke-direct {v4, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2725367
    iget-object v2, p0, LX/Jho;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1zC;

    iget-object v5, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v5, v4}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->a(Ljava/lang/CharSequence;)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v2, v4, v5}, LX/1zC;->a(Landroid/text/Editable;I)Z

    move-object v2, v4

    .line 2725368
    :cond_16
    iget-object v4, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2725369
    iget-object v2, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v2, v8}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2725370
    :cond_17
    iget-object v2, p0, LX/Jho;->n:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iget v4, p0, LX/Jho;->o:I

    invoke-virtual {v2, v4}, LX/2Wj;->setTextColor(I)V

    goto :goto_18

    .line 2725371
    :cond_18
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    goto :goto_19

    .line 2725372
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_1a
    move v2, v4

    .line 2725373
    goto/16 :goto_4

    .line 2725374
    :cond_1b
    iget-object v4, p0, LX/Jho;->u:Landroid/widget/CheckBox;

    invoke-virtual {v4, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2725375
    iget-object v4, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    invoke-virtual {v4, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_5

    .line 2725376
    :cond_1c
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725377
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 2725378
    iget-object v2, p0, LX/Jho;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setFocusable(Z)V

    goto/16 :goto_6

    .line 2725379
    :cond_1d
    iget-object v2, p0, LX/Jho;->E:Landroid/view/View;

    if-nez v2, :cond_1e

    .line 2725380
    iget-object v2, p0, LX/Jho;->D:Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, LX/Jho;->E:Landroid/view/View;

    .line 2725381
    :cond_1e
    iget-object v2, p0, LX/Jho;->E:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_1f
    move v4, v2

    .line 2725382
    goto/16 :goto_8

    .line 2725383
    :cond_20
    const/16 v2, 0x8

    goto/16 :goto_9

    .line 2725384
    :cond_21
    iget-object v2, p0, LX/Jho;->H:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->d()V

    goto/16 :goto_a

    .line 2725385
    :cond_22
    iget-object v2, p0, LX/Jho;->N:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->d()V

    goto/16 :goto_b

    .line 2725386
    :cond_23
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_e

    .line 2725387
    :cond_24
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080273

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_f

    .line 2725388
    :cond_25
    iget-object v0, p0, LX/Jho;->w:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_10

    .line 2725389
    :cond_26
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725390
    iget-object v2, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v2

    .line 2725391
    sget-object v2, LX/3ON;->SINGLE_TAP_SEND:LX/3ON;

    if-ne v0, v2, :cond_2a

    const/4 v0, 0x1

    :goto_1a
    move v0, v0

    .line 2725392
    if-eqz v0, :cond_28

    .line 2725393
    iget-object v0, p0, LX/Jho;->M:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    .line 2725394
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2725395
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725396
    iget-boolean v2, v1, LX/3OO;->w:Z

    move v1, v2

    .line 2725397
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2725398
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725399
    iget-boolean v2, v1, LX/3OO;->w:Z

    move v1, v2

    .line 2725400
    if-eqz v1, :cond_27

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08030c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1b
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2725401
    invoke-static {p0, v0}, LX/Jho;->setPropagateToRowClickOnClickListener(LX/Jho;Landroid/widget/Button;)V

    goto/16 :goto_12

    .line 2725402
    :cond_27
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08043f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1b

    .line 2725403
    :cond_28
    iget-object v0, p0, LX/Jho;->L:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725404
    iget-object v0, p0, LX/Jho;->M:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto/16 :goto_12

    :cond_29
    const/4 v0, 0x0

    goto/16 :goto_11

    :cond_2a
    const/4 v0, 0x0

    goto :goto_1a

    .line 2725405
    :cond_2b
    iget-object v0, p0, LX/Jho;->F:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto/16 :goto_13

    .line 2725406
    :cond_2c
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2725407
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2725408
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2725409
    iget-object v3, v1, LX/EFb;->b:LX/3A0;

    iget-object v3, v3, LX/3A0;->m:Landroid/content/Context;

    const v4, 0x7f0807c0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v3, v3

    .line 2725410
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2d

    .line 2725411
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2725412
    :cond_2d
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2725413
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 2725414
    new-instance v2, LX/Jhi;

    invoke-direct {v2, p0, v1}, LX/Jhi;-><init>(LX/Jho;LX/EFb;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_14
.end method

.method private j()V
    .locals 3

    .prologue
    .line 2725122
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725123
    iget-object v1, v0, LX/3OO;->m:LX/3OL;

    move-object v0, v1

    .line 2725124
    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2725125
    if-nez v0, :cond_2

    invoke-static {p0}, LX/Jho;->G(LX/Jho;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, LX/Jho;->A(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725126
    iget-object v1, v0, LX/3OO;->k:Ljava/lang/String;

    move-object v0, v1

    .line 2725127
    if-eqz v0, :cond_2

    :cond_0
    invoke-static {p0}, LX/Jho;->w(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725128
    iget-object v1, v0, LX/3OO;->r:LX/3OI;

    move-object v0, v1

    .line 2725129
    sget-object v1, LX/3OH;->AUTO_COMPLETE:LX/3OH;

    if-eq v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725130
    iget-boolean v1, v0, LX/3OO;->Q:Z

    move v0, v1

    .line 2725131
    if-eqz v0, :cond_3

    .line 2725132
    :cond_2
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setVisibility(I)V

    .line 2725133
    :goto_1
    return-void

    .line 2725134
    :cond_3
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725135
    iget-object v1, v0, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v0, v1

    .line 2725136
    iget-object v1, v0, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v0, v1

    .line 2725137
    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v0, v1, :cond_d

    .line 2725138
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setVisibility(I)V

    .line 2725139
    invoke-static {p0}, LX/Jho;->A(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725140
    iget-object v1, v0, LX/3OO;->k:Ljava/lang/String;

    move-object v0, v1

    .line 2725141
    if-eqz v0, :cond_4

    .line 2725142
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->NONE:LX/FKA;

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725143
    iget-object p0, v2, LX/3OO;->k:Ljava/lang/String;

    move-object v2, p0

    .line 2725144
    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a(LX/FKA;Ljava/lang/String;)V

    goto :goto_1

    .line 2725145
    :cond_4
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725146
    iget-object v1, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v1

    .line 2725147
    sget-object v1, LX/3ON;->FACEBOOK_TAB:LX/3ON;

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2725148
    if-eqz v0, :cond_8

    .line 2725149
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setTextColor(I)V

    .line 2725150
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725151
    iget-boolean v1, v0, LX/3OO;->c:Z

    move v0, v1

    .line 2725152
    if-eqz v0, :cond_5

    .line 2725153
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->AVAILABLE_ON_MOBILE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto :goto_1

    .line 2725154
    :cond_5
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725155
    iget-boolean v1, v0, LX/3OO;->d:Z

    move v0, v1

    .line 2725156
    if-nez v0, :cond_6

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725157
    iget-boolean v1, v0, LX/3OO;->e:Z

    move v0, v1

    .line 2725158
    if-eqz v0, :cond_7

    .line 2725159
    :cond_6
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->AVAILABLE_ON_WEB:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto :goto_1

    .line 2725160
    :cond_7
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->NONE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto :goto_1

    .line 2725161
    :cond_8
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725162
    iget-boolean v2, v1, LX/3OO;->j:Z

    move v1, v2

    .line 2725163
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setShowIcon(Z)V

    .line 2725164
    invoke-static {p0}, LX/Jho;->E(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2725165
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->NEARBY:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto/16 :goto_1

    .line 2725166
    :cond_9
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725167
    iget-boolean v1, v0, LX/3OO;->e:Z

    move v0, v1

    .line 2725168
    if-eqz v0, :cond_a

    .line 2725169
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->ONLINE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto/16 :goto_1

    .line 2725170
    :cond_a
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725171
    iget-boolean v1, v0, LX/3OO;->h:Z

    move v0, v1

    .line 2725172
    if-eqz v0, :cond_c

    .line 2725173
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725174
    iget-object v1, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v1

    .line 2725175
    sget-object v1, LX/3ON;->ONE_LINE:LX/3ON;

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2725176
    if-eqz v0, :cond_b

    .line 2725177
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->PUSHABLE:LX/FKA;

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    .line 2725178
    iget-object p0, v2, LX/3OO;->k:Ljava/lang/String;

    move-object v2, p0

    .line 2725179
    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a(LX/FKA;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2725180
    :cond_b
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->PUSHABLE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto/16 :goto_1

    .line 2725181
    :cond_c
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->NONE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto/16 :goto_1

    .line 2725182
    :cond_d
    iget-object v0, p0, LX/Jho;->t:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    sget-object v1, LX/FKA;->NONE:LX/FKA;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setStatus(LX/FKA;)V

    goto/16 :goto_1

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_10
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private k()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/16 v5, 0x8

    const/4 v8, 0x0

    .line 2725089
    invoke-static {p0}, LX/Jho;->u(LX/Jho;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2725090
    iget-object v0, p0, LX/Jho;->I:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2725091
    :cond_0
    :goto_0
    return-void

    .line 2725092
    :cond_1
    iget-object v0, p0, LX/Jho;->I:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2725093
    const v0, 0x7f0d203d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2725094
    const v1, 0x7f0d203c

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2725095
    const v2, 0x7f0d203b

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2725096
    const v3, 0x7f0d203a

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2725097
    const v4, 0x7f0d2039

    invoke-virtual {p0, v4}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 2725098
    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2725099
    invoke-virtual {v1, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2725100
    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2725101
    invoke-virtual {v3, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2725102
    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2725103
    iget-object v5, p0, LX/Jho;->O:LX/3OO;

    .line 2725104
    iget v6, v5, LX/3OO;->X:I

    move v5, v6

    .line 2725105
    const/4 v6, 0x5

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2725106
    packed-switch v6, :pswitch_data_0

    .line 2725107
    :goto_1
    const/4 v0, 0x5

    if-ne v6, v0, :cond_2

    .line 2725108
    invoke-virtual {v4, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 2725109
    :pswitch_0
    iget-object v5, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v5}, LX/3OO;->O()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2725110
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v5, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v5}, LX/3OO;->Q()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2725111
    invoke-virtual {v2, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2725112
    :pswitch_1
    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v2}, LX/3OO;->O()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2725113
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v2, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v2}, LX/3OO;->Q()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2725114
    invoke-virtual {v1, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2725115
    :pswitch_2
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v1}, LX/3OO;->O()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2725116
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v1}, LX/3OO;->Q()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2725117
    invoke-virtual {v0, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2725118
    :cond_2
    const/4 v0, 0x4

    if-ne v6, v0, :cond_0

    .line 2725119
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0}, LX/3OO;->O()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2725120
    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    invoke-virtual {v0}, LX/3OO;->Q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2725121
    invoke-virtual {v3, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private l()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2724929
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724930
    iget-object v5, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v5, v5

    .line 2724931
    iget-object v1, v5, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v1, v1

    .line 2724932
    sget-object v6, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v1, v6, :cond_7

    .line 2724933
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724934
    iget-object v6, v1, LX/3OO;->r:LX/3OI;

    move-object v1, v6

    .line 2724935
    sget-object v6, LX/3OH;->SELF_PROFILE:LX/3OH;

    if-ne v1, v6, :cond_0

    .line 2724936
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a70

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2724937
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2724938
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/Jho;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v0, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v5, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-interface {v1, v0, v5, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2724939
    move-object v0, v1

    .line 2724940
    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    .line 2724941
    :goto_0
    iget-object v1, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724942
    iget-object v5, p0, LX/Jho;->p:Landroid/widget/TextView;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2724943
    iget-object v1, p0, LX/Jho;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724944
    iget-object v1, p0, LX/Jho;->q:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2724945
    return-void

    .line 2724946
    :cond_0
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724947
    iget-object v6, v1, LX/3OO;->r:LX/3OI;

    move-object v1, v6

    .line 2724948
    sget-object v6, LX/3OH;->NEARBY_FRIENDS:LX/3OH;

    if-ne v1, v6, :cond_1

    .line 2724949
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2724950
    iget-object v1, v0, LX/3OO;->l:Ljava/lang/String;

    move-object v0, v1

    .line 2724951
    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    goto :goto_0

    .line 2724952
    :cond_1
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724953
    iget-object v6, v1, LX/3OO;->q:LX/6Lx;

    move-object v1, v6

    .line 2724954
    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724955
    iget-object v6, v1, LX/3OO;->q:LX/6Lx;

    move-object v1, v6

    .line 2724956
    invoke-interface {v1}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2724957
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2724958
    iget-object v1, v0, LX/3OO;->q:LX/6Lx;

    move-object v0, v1

    .line 2724959
    invoke-interface {v0}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->a()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;->a()Ljava/lang/String;

    move-result-object v0

    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    goto :goto_0

    .line 2724960
    :cond_2
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724961
    iget-boolean v6, v1, LX/3OO;->v:Z

    move v1, v6

    .line 2724962
    if-eqz v1, :cond_4

    .line 2724963
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724964
    iget-object v6, v1, LX/3OO;->r:LX/3OI;

    move-object v1, v6

    .line 2724965
    sget-object v6, LX/3OH;->NEW_CONTACTS:LX/3OH;

    if-ne v1, v6, :cond_3

    .line 2724966
    iget-wide v11, v5, Lcom/facebook/user/model/User;->x:J

    move-wide v6, v11

    .line 2724967
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_3

    .line 2724968
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x7f082df7

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v7, p0, LX/Jho;->j:LX/11T;

    invoke-virtual {v7}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v7

    .line 2724969
    iget-wide v11, v5, Lcom/facebook/user/model/User;->x:J

    move-wide v8, v11

    .line 2724970
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    invoke-virtual {v1, v6, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    goto/16 :goto_0

    .line 2724971
    :cond_3
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0801f7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    goto/16 :goto_0

    .line 2724972
    :cond_4
    invoke-static {p0}, LX/Jho;->C(LX/Jho;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2724973
    const/4 v0, 0x0

    .line 2724974
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724975
    iget-boolean v5, v1, LX/3OO;->e:Z

    move v1, v5

    .line 2724976
    if-eqz v1, :cond_f

    .line 2724977
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080449

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2724978
    :cond_5
    :goto_3
    move-object v0, v0

    .line 2724979
    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    goto/16 :goto_0

    .line 2724980
    :cond_6
    iget-boolean v0, v5, Lcom/facebook/user/model/User;->y:Z

    move v0, v0

    .line 2724981
    if-nez v0, :cond_e

    .line 2724982
    iget-object v0, v5, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2724983
    if-eqz v0, :cond_e

    .line 2724984
    iget-object v0, v5, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2724985
    move-object v10, v3

    move-object v3, v0

    move-object v0, v10

    goto/16 :goto_0

    .line 2724986
    :cond_7
    iget-object v1, v5, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v1, v1

    .line 2724987
    invoke-virtual {v1}, LX/0XG;->isPhoneContact()Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724988
    iget-boolean v6, v1, LX/3OO;->M:Z

    move v1, v6

    .line 2724989
    if-nez v1, :cond_e

    .line 2724990
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2724991
    iget-object v6, v1, LX/3OO;->r:LX/3OI;

    move-object v1, v6

    .line 2724992
    sget-object v6, LX/3OH;->SEARCH_RESULT:LX/3OH;

    if-ne v1, v6, :cond_a

    move v1, v0

    .line 2724993
    :goto_4
    iget-object v0, p0, LX/Jho;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNh;

    const/4 v6, 0x0

    .line 2724994
    invoke-virtual {v5}, Lcom/facebook/user/model/User;->ax()Z

    move-result v7

    invoke-static {v7}, LX/0PB;->checkArgument(Z)V

    .line 2724995
    invoke-virtual {v5}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 2724996
    iget-object v7, v5, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v7, v7

    .line 2724997
    if-nez v7, :cond_15

    .line 2724998
    if-nez v1, :cond_13

    .line 2724999
    :cond_8
    :goto_5
    move-object v1, v6

    .line 2725000
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725001
    iget-object v6, v0, LX/3OO;->R:Lcom/facebook/user/model/User;

    move-object v0, v6

    .line 2725002
    if-nez v0, :cond_d

    .line 2725003
    iget-object v0, p0, LX/Jho;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNh;

    const/4 v3, 0x0

    .line 2725004
    iget-object v6, v5, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v6, v6

    .line 2725005
    sget-object v7, LX/0XG;->EMAIL:LX/0XG;

    invoke-virtual {v6, v7}, LX/0XG;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_17

    .line 2725006
    iget-object v3, v0, LX/FNh;->f:Landroid/content/res/Resources;

    const v6, 0x7f080536

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2725007
    :cond_9
    :goto_6
    move-object v0, v3

    .line 2725008
    move-object v3, v1

    goto/16 :goto_0

    :cond_a
    move v1, v2

    .line 2725009
    goto :goto_4

    :cond_b
    move v1, v4

    .line 2725010
    goto/16 :goto_1

    :cond_c
    move v2, v4

    .line 2725011
    goto/16 :goto_2

    :cond_d
    move-object v0, v3

    move-object v3, v1

    goto/16 :goto_0

    :cond_e
    move-object v0, v3

    goto/16 :goto_0

    .line 2725012
    :cond_f
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725013
    iget-boolean v5, v1, LX/3OO;->h:Z

    move v1, v5

    .line 2725014
    if-nez v1, :cond_10

    invoke-static {p0}, LX/Jho;->u(LX/Jho;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2725015
    :cond_10
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725016
    iget-object v1, v0, LX/3OO;->k:Ljava/lang/String;

    move-object v0, v1

    .line 2725017
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 2725018
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725019
    iget-object v1, v0, LX/3OO;->k:Ljava/lang/String;

    move-object v0, v1

    .line 2725020
    goto/16 :goto_3

    .line 2725021
    :cond_11
    invoke-virtual {p0}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08046d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2725022
    :cond_12
    iget-object v1, p0, LX/Jho;->O:LX/3OO;

    .line 2725023
    iget-object v5, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v1, v5

    .line 2725024
    iget-object v5, v1, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v5, v5

    .line 2725025
    if-eqz v5, :cond_5

    .line 2725026
    iget-object v0, v1, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2725027
    goto/16 :goto_3

    .line 2725028
    :cond_13
    iget-object v6, v5, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v6, v6

    .line 2725029
    sget-object v7, LX/0XG;->EMAIL:LX/0XG;

    invoke-virtual {v6, v7}, LX/0XG;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 2725030
    iget-object v6, v0, LX/FNh;->f:Landroid/content/res/Resources;

    const v7, 0x7f08053a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 2725031
    :cond_14
    iget-object v6, v0, LX/FNh;->f:Landroid/content/res/Resources;

    const v7, 0x7f08053b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 2725032
    :cond_15
    iget-object v7, v5, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v7, v7

    .line 2725033
    sget-object v8, LX/0XG;->PHONE_NUMBER:LX/0XG;

    invoke-virtual {v7, v8}, LX/0XG;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 2725034
    invoke-virtual {v5}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v7

    .line 2725035
    if-eqz v7, :cond_8

    .line 2725036
    iget-object v6, v0, LX/FNh;->g:LX/3Lx;

    .line 2725037
    iget-object v8, v7, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v7, v8

    .line 2725038
    invoke-virtual {v6, v7}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 2725039
    :cond_16
    iget-object v7, v5, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v7, v7

    .line 2725040
    sget-object v8, LX/0XG;->EMAIL:LX/0XG;

    invoke-virtual {v7, v8}, LX/0XG;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2725041
    invoke-virtual {v5}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 2725042
    :cond_17
    invoke-virtual {v5}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v6

    .line 2725043
    if-eqz v6, :cond_9

    .line 2725044
    iget v7, v6, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    move v6, v7

    .line 2725045
    packed-switch v6, :pswitch_data_0

    goto/16 :goto_6

    .line 2725046
    :pswitch_0
    iget-object v3, v0, LX/FNh;->f:Landroid/content/res/Resources;

    const v6, 0x7f080538

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2725047
    :pswitch_1
    iget-object v3, v0, LX/FNh;->f:Landroid/content/res/Resources;

    const v6, 0x7f080537

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2725048
    :pswitch_2
    iget-object v3, v0, LX/FNh;->f:Landroid/content/res/Resources;

    const v6, 0x7f080539

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static setPropagateToRowClickOnClickListener(LX/Jho;Landroid/widget/Button;)V
    .locals 1

    .prologue
    .line 2725087
    new-instance v0, LX/Jhh;

    invoke-direct {v0, p0, p0}, LX/Jhh;-><init>(LX/Jho;LX/Jho;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725088
    return-void
.end method

.method public static u(LX/Jho;)Z
    .locals 2

    .prologue
    .line 2725084
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725085
    iget-object v1, v0, LX/3OO;->r:LX/3OI;

    move-object v0, v1

    .line 2725086
    sget-object v1, LX/3OH;->CALL_LOGS:LX/3OH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(LX/Jho;)Z
    .locals 2

    .prologue
    .line 2725081
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725082
    iget-object v1, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v1

    .line 2725083
    sget-object v1, LX/3ON;->NEUE_PICKER:LX/3ON;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(LX/Jho;)Z
    .locals 2

    .prologue
    .line 2725078
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    .line 2725079
    iget-object v1, v0, LX/3OO;->b:LX/3ON;

    move-object v0, v1

    .line 2725080
    sget-object v1, LX/3ON;->INVITE_BUTTON_PICKER:LX/3ON;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContactRow()LX/3OO;
    .locals 1

    .prologue
    .line 2725077
    iget-object v0, p0, LX/Jho;->O:LX/3OO;

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5a631fcc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2725065
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 2725066
    iget-object v1, p0, LX/Jho;->g:LX/FOX;

    const/4 v2, 0x0

    .line 2725067
    iput-boolean v2, v1, LX/FOX;->k:Z

    .line 2725068
    const/4 v4, 0x0

    .line 2725069
    iget-boolean v2, v1, LX/FOX;->k:Z

    if-eqz v2, :cond_1

    .line 2725070
    iget-object v2, v1, LX/FOX;->l:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v2, :cond_0

    .line 2725071
    iget-object v4, v1, LX/FOX;->l:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 2725072
    :cond_0
    invoke-static {v1, v4}, LX/FOX;->a(LX/FOX;Lcom/facebook/user/model/UserKey;)V

    .line 2725073
    :goto_0
    iget-object v1, p0, LX/Jho;->g:LX/FOX;

    const/4 v2, 0x0

    .line 2725074
    iput-object v2, v1, LX/FOX;->p:LX/FOW;

    .line 2725075
    const/16 v1, 0x2d

    const v2, 0x4797ca7c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2725076
    :cond_1
    invoke-static {v1, v4}, LX/FOX;->a(LX/FOX;Lcom/facebook/user/model/UserKey;)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 2725058
    instance-of v0, p1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;

    if-nez v0, :cond_1

    .line 2725059
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2725060
    :cond_0
    :goto_0
    return-void

    .line 2725061
    :cond_1
    check-cast p1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;

    .line 2725062
    invoke-virtual {p1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2725063
    iget-object v0, p1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->a:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2725064
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->a:LX/03R;

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 2725052
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2725053
    new-instance v1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2725054
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2725055
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, v1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->a:LX/03R;

    .line 2725056
    :goto_0
    return-object v1

    .line 2725057
    :cond_0
    iget-object v0, p0, LX/Jho;->K:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, v1, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->a:LX/03R;

    goto :goto_0
.end method

.method public setContactRow(LX/3OO;)V
    .locals 0

    .prologue
    .line 2725049
    iput-object p1, p0, LX/Jho;->O:LX/3OO;

    .line 2725050
    invoke-static {p0}, LX/Jho;->b$redex0(LX/Jho;)V

    .line 2725051
    return-void
.end method
