.class public final LX/K0O;
.super LX/3hi;
.source ""


# instance fields
.field public final synthetic b:LX/K0P;


# direct methods
.method public constructor <init>(LX/K0P;)V
    .locals 0

    .prologue
    .line 2758446
    iput-object p1, p0, LX/K0O;->b:LX/K0P;

    invoke-direct {p0}, LX/3hi;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/K0P;B)V
    .locals 0

    .prologue
    .line 2758473
    invoke-direct {p0, p1}, LX/K0O;-><init>(LX/K0P;)V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;[F[F)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 2758462
    sget-object v0, LX/K0P;->b:Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {v1, v7, v7, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v5, p0, LX/K0O;->b:LX/K0P;

    iget-object v6, v5, LX/K0P;->n:LX/1Up;

    move v5, v4

    invoke-static/range {v0 .. v6}, LX/4AZ;->a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFFLX/1Up;)Landroid/graphics/Matrix;

    .line 2758463
    sget-object v0, LX/K0P;->b:Landroid/graphics/Matrix;

    sget-object v1, LX/K0P;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 2758464
    sget-object v0, LX/K0P;->c:Landroid/graphics/Matrix;

    aget v1, p2, v7

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v0

    aput v0, p3, v7

    .line 2758465
    aget v0, p3, v7

    aput v0, p3, v9

    .line 2758466
    sget-object v0, LX/K0P;->c:Landroid/graphics/Matrix;

    aget v1, p2, v9

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v0

    aput v0, p3, v8

    .line 2758467
    aget v0, p3, v8

    aput v0, p3, v10

    .line 2758468
    const/4 v0, 0x4

    sget-object v1, LX/K0P;->c:Landroid/graphics/Matrix;

    aget v2, p2, v8

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v1

    aput v1, p3, v0

    .line 2758469
    const/4 v0, 0x5

    const/4 v1, 0x4

    aget v1, p3, v1

    aput v1, p3, v0

    .line 2758470
    const/4 v0, 0x6

    sget-object v1, LX/K0P;->c:Landroid/graphics/Matrix;

    aget v2, p2, v10

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v1

    aput v1, p3, v0

    .line 2758471
    const/4 v0, 0x7

    const/4 v1, 0x6

    aget v1, p3, v1

    aput v1, p3, v0

    .line 2758472
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 2758447
    iget-object v0, p0, LX/K0O;->b:LX/K0P;

    sget-object v1, LX/K0P;->a:[F

    .line 2758448
    invoke-static {v0, v1}, LX/K0P;->a$redex0(LX/K0P;[F)V

    .line 2758449
    invoke-virtual {p1, v2}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 2758450
    sget-object v0, LX/K0P;->a:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0, v7}, LX/5qm;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/K0P;->a:[F

    aget v0, v0, v2

    invoke-static {v0, v7}, LX/5qm;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/K0P;->a:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-static {v0, v7}, LX/5qm;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/K0P;->a:[F

    const/4 v1, 0x3

    aget v0, v0, v1

    invoke-static {v0, v7}, LX/5qm;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2758451
    invoke-super {p0, p1, p2}, LX/3hi;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2758452
    :goto_0
    return-void

    .line 2758453
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2758454
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2758455
    new-instance v1, Landroid/graphics/BitmapShader;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, p2, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2758456
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2758457
    const/16 v2, 0x8

    new-array v2, v2, [F

    .line 2758458
    sget-object v3, LX/K0P;->a:[F

    invoke-direct {p0, p2, v3, v2}, LX/K0O;->a(Landroid/graphics/Bitmap;[F[F)V

    .line 2758459
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 2758460
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v3, v4, v2, v5}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 2758461
    invoke-virtual {v1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method
