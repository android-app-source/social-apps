.class public final LX/Jxu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;",
        ">;",
        "LX/Enp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2753728
    iput-object p1, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iput-object p2, p0, LX/Jxu;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2753729
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2753730
    if-eqz p1, :cond_0

    .line 2753731
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2753732
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2753733
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null data received"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2753734
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2753735
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;

    move-result-object v4

    .line 2753736
    new-instance v5, LX/En3;

    iget-object v0, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget v0, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->m:I

    invoke-direct {v5, v0}, LX/En3;-><init>(I)V

    .line 2753737
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    move v1, v2

    .line 2753738
    :goto_0
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2753739
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;

    .line 2753740
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2753741
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 2753742
    iget-object v3, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget-object v3, v3, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->k:LX/0UE;

    invoke-virtual {v3, v7}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2753743
    const-string v0, "Duplicate item ID received: %s, bucket id: %s, current ID list: %s, cursor: %s"

    iget-object v3, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget-object v3, v3, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->i:Ljava/lang/String;

    iget-object v8, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    .line 2753744
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 2753745
    iget-object v9, v8, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->k:LX/0UE;

    invoke-virtual {v9}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 2753746
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2753747
    const-string v9, "|"

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2753748
    :cond_2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v8, v9

    .line 2753749
    iget-object v9, p0, LX/Jxu;->a:Ljava/lang/String;

    invoke-static {v0, v7, v3, v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2753750
    iget-object v0, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget-object v0, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v7, "discovery_page_loader_duplicate_id"

    invoke-virtual {v0, v7, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2753751
    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2753752
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_7

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    .line 2753753
    :goto_3
    iget-object v8, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget-object v8, v8, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->h:LX/Jxq;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v9

    iget-object v10, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget v10, v10, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->m:I

    const/4 v11, 0x0

    .line 2753754
    iget-object v12, v8, LX/Jxq;->b:LX/0Zb;

    const-string v13, "ec_download"

    invoke-interface {v12, v13, v11}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v12

    .line 2753755
    invoke-virtual {v12}, LX/0oG;->a()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2753756
    const-string v13, "entity_cards"

    invoke-virtual {v12, v13}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2753757
    const-string v13, "entity_card_impression"

    invoke-static {v8, v12, v13}, LX/Jxq;->b(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753758
    invoke-static {v8, v12}, LX/Jxq;->a(LX/Jxq;LX/0oG;)V

    .line 2753759
    invoke-static {v8, v12}, LX/Jxq;->b(LX/Jxq;LX/0oG;)V

    .line 2753760
    invoke-static {v9, v12}, LX/Jxq;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/0oG;)V

    .line 2753761
    const-string v13, "entity_card_position"

    invoke-virtual {v12, v13, v10}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2753762
    const-string v13, "is_last"

    if-eqz v3, :cond_5

    const/4 v11, 0x1

    :cond_5
    invoke-virtual {v12, v13, v11}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2753763
    invoke-virtual {v12}, LX/0oG;->d()V

    .line 2753764
    :cond_6
    iget-object v3, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget v3, v3, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->m:I

    invoke-virtual {v5, v3, v7}, LX/En2;->a(ILjava/lang/Object;)V

    .line 2753765
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2753766
    iget-object v0, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget-object v0, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->k:LX/0UE;

    invoke-virtual {v0, v7}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 2753767
    iget-object v0, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    .line 2753768
    iget v3, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->m:I

    add-int/lit8 v7, v3, 0x1

    iput v7, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->m:I

    .line 2753769
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2753770
    goto :goto_3

    .line 2753771
    :cond_8
    invoke-virtual {v5}, LX/En3;->a()V

    .line 2753772
    iget-object v0, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    iget-object v0, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    if-nez v0, :cond_9

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2753773
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2753774
    :goto_4
    iget-object v0, p0, LX/Jxu;->b:Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2753775
    iput-object v2, v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    .line 2753776
    new-instance v2, LX/Enp;

    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    .line 2753777
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2753778
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v5, v3, v1, v0}, LX/Enp;-><init>(LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 2753779
    :cond_9
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_4
.end method
