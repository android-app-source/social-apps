.class public LX/Jr5;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final k:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Mw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2My;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741806
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr5;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2741789
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2741790
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741791
    iput-object v0, p0, LX/Jr5;->b:LX/0Ot;

    .line 2741792
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741793
    iput-object v0, p0, LX/Jr5;->c:LX/0Ot;

    .line 2741794
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741795
    iput-object v0, p0, LX/Jr5;->d:LX/0Ot;

    .line 2741796
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741797
    iput-object v0, p0, LX/Jr5;->e:LX/0Ot;

    .line 2741798
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741799
    iput-object v0, p0, LX/Jr5;->f:LX/0Ot;

    .line 2741800
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741801
    iput-object v0, p0, LX/Jr5;->h:LX/0Ot;

    .line 2741802
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741803
    iput-object v0, p0, LX/Jr5;->j:LX/0Ot;

    .line 2741804
    iput-object p2, p0, LX/Jr5;->a:LX/0Or;

    .line 2741805
    return-void
.end method

.method private static a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2741787
    invoke-virtual {p0}, LX/6kW;->A()LX/6k0;

    move-result-object v0

    iget-object v0, v0, LX/6k0;->newMessage:LX/6k4;

    .line 2741788
    iget-object v0, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    iget-object v0, v0, LX/6l9;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jr5;
    .locals 15

    .prologue
    .line 2741756
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741757
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741758
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741759
    if-nez v1, :cond_0

    .line 2741760
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741761
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741762
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741763
    sget-object v1, LX/Jr5;->k:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741764
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741765
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741766
    :cond_1
    if-nez v1, :cond_4

    .line 2741767
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741768
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741769
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741770
    new-instance v1, LX/Jr5;

    const/16 v7, 0x35bd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct {v1, v7, v8}, LX/Jr5;-><init>(LX/0Ot;LX/0Or;)V

    .line 2741771
    const/16 v7, 0x29dd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x29c4

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xd6e

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xce5

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xd18

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const/16 v13, 0xd6c

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x15e8

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 p0, 0x2739

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2741772
    iput-object v7, v1, LX/Jr5;->b:LX/0Ot;

    iput-object v8, v1, LX/Jr5;->c:LX/0Ot;

    iput-object v9, v1, LX/Jr5;->d:LX/0Ot;

    iput-object v10, v1, LX/Jr5;->e:LX/0Ot;

    iput-object v11, v1, LX/Jr5;->f:LX/0Ot;

    iput-object v12, v1, LX/Jr5;->g:LX/0SG;

    iput-object v13, v1, LX/Jr5;->h:LX/0Ot;

    iput-object v14, v1, LX/Jr5;->i:LX/0Or;

    iput-object p0, v1, LX/Jr5;->j:LX/0Ot;

    .line 2741773
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741774
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741775
    if-nez v1, :cond_2

    .line 2741776
    sget-object v0, LX/Jr5;->k:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr5;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741777
    :goto_1
    if-eqz v0, :cond_3

    .line 2741778
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741779
    :goto_3
    check-cast v0, LX/Jr5;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741780
    invoke-virtual {v3}, LX/0op;->c()V

    goto/16 :goto_0

    .line 2741781
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741782
    :catchall_1
    move-exception v0

    .line 2741783
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741784
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741785
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741786
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr5;->k:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr5;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2741678
    iget-object v0, p0, LX/Jr5;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2741679
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 2741680
    :cond_0
    :goto_0
    return-object p1

    .line 2741681
    :cond_1
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 2741682
    const/4 v2, 0x1

    .line 2741683
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    if-eqz v1, :cond_3

    .line 2741684
    iget-object v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_3

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2741685
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v2, v3

    .line 2741686
    :cond_2
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2741687
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 2741688
    :cond_3
    if-eqz v2, :cond_4

    .line 2741689
    new-instance v1, LX/6fz;

    invoke-direct {v1}, LX/6fz;-><init>()V

    new-instance v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2741690
    iput-object v2, v1, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2741691
    move-object v0, v1

    .line 2741692
    invoke-virtual {v0}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2741693
    :cond_4
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 2741694
    iput-object v5, v0, LX/6g6;->h:Ljava/util/List;

    .line 2741695
    move-object v0, v0

    .line 2741696
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(LX/Jr5;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2741740
    iget-object v0, p0, LX/Jr5;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2My;

    const/4 v2, 0x0

    .line 2741741
    invoke-virtual {v0}, LX/2My;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/2My;->a:LX/0Uh;

    const/16 v4, 0x202

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 2741742
    if-nez v0, :cond_1

    iget-object v0, p0, LX/Jr5;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2741743
    :goto_0
    return-object v0

    .line 2741744
    :cond_1
    iget-object v0, p0, LX/Jr5;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {p2, p3, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2741745
    iget-object v0, p0, LX/Jr5;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDs;

    .line 2741746
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2741747
    iget-object v3, v0, LX/FDs;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6dQ;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2741748
    const-string v5, "montage_thread_key"

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741749
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v5

    .line 2741750
    const-string p2, "thread_key"

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object p2

    invoke-virtual {v5, p2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2741751
    const-string p2, "folder"

    sget-object p3, LX/6ek;->INBOX:LX/6ek;

    iget-object p3, p3, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {p2, p3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object p2

    invoke-virtual {v5, p2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2741752
    const-string p2, "threads"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p2, v4, p3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    move v0, v3

    .line 2741753
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2741754
    goto :goto_0

    .line 2741755
    :cond_2
    iget-object v0, p0, LX/Jr5;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2N4;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/service/model/FetchThreadResult;)Z
    .locals 1

    .prologue
    .line 2741739
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Long;)Z
    .locals 1
    .param p0    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2741736
    if-nez p0, :cond_0

    .line 2741737
    const/4 v0, 0x1

    .line 2741738
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Long;)Z
    .locals 2

    .prologue
    .line 2741735
    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Jr5;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static c(LX/6kW;)Ljava/lang/Long;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2741732
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/6kW;->A()LX/6k0;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/6kW;->A()LX/6k0;

    move-result-object v0

    iget-object v0, v0, LX/6k0;->newMessage:LX/6k4;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/6kW;->A()LX/6k0;

    move-result-object v0

    iget-object v0, v0, LX/6k0;->newMessage:LX/6k4;

    iget-object v0, v0, LX/6k4;->messageMetadata:LX/6kn;

    if-nez v0, :cond_1

    .line 2741733
    :cond_0
    const/4 v0, 0x0

    .line 2741734
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/6kW;->A()LX/6k0;

    move-result-object v0

    iget-object v0, v0, LX/6k0;->newMessage:LX/6k4;

    iget-object v0, v0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->actorFbId:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741728
    check-cast p1, LX/6kW;

    .line 2741729
    invoke-static {p1}, LX/Jr5;->c(LX/6kW;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/Jr5;->a(Ljava/lang/Long;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2741730
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741731
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/Jr5;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741712
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-static {v0}, LX/Jr5;->c(LX/6kW;)Ljava/lang/Long;

    move-result-object v2

    .line 2741713
    invoke-direct {p0, v2}, LX/Jr5;->b(Ljava/lang/Long;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2741714
    iget-object v0, p0, LX/Jr5;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Mw;

    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v1}, LX/6kW;->A()LX/6k0;

    move-result-object v1

    iget-object v1, v1, LX/6k0;->newMessage:LX/6k4;

    iget-object v1, v1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v1, v1, LX/6kn;->threadKey:LX/6l9;

    iget-object v1, v1, LX/6l9;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, LX/2Mw;->a(J)V

    .line 2741715
    :cond_0
    invoke-static {v2}, LX/Jr5;->a(Ljava/lang/Long;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2741716
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2741717
    :goto_0
    return-object v0

    .line 2741718
    :cond_1
    iget-object v0, p0, LX/Jr5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr6;

    invoke-direct {p0, p1}, LX/Jr5;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v1}, LX/6kW;->A()LX/6k0;

    move-result-object v1

    iget-object v1, v1, LX/6k0;->newMessage:LX/6k4;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v0, v3, v1, v4, v5}, LX/Jr6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6k4;J)Landroid/os/Bundle;

    move-result-object v1

    .line 2741719
    iget-object v0, p0, LX/Jr5;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2My;

    invoke-virtual {v0}, LX/2My;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, LX/Jr5;->b(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2741720
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2741721
    iget-object v0, p0, LX/Jr5;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, LX/2N4;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2741722
    invoke-static {v0}, LX/Jr5;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2741723
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0, v2, v3}, LX/Jr5;->a(LX/Jr5;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2741724
    invoke-static {v0}, LX/Jr5;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2741725
    :goto_1
    move-object v0, v1

    .line 2741726
    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 2741727
    :cond_3
    const-string v4, "UPDATED_INBOX_THREAD"

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741698
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2741699
    if-eqz v0, :cond_0

    .line 2741700
    iget-object v1, p0, LX/Jr5;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2741701
    iget-object v1, p0, LX/Jr5;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jqb;

    .line 2741702
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2741703
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741704
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741705
    const-string v4, "UPDATED_INBOX_THREAD"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741706
    if-nez v4, :cond_1

    .line 2741707
    :cond_0
    :goto_0
    return-void

    .line 2741708
    :cond_1
    iget-object v5, p0, LX/Jr5;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Oe;

    iget-object v6, p0, LX/Jr5;->g:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v5, v4, v6, v7}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741709
    iget-object v5, p0, LX/Jr5;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Jqb;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741710
    invoke-static {v5, v4}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741711
    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741697
    check-cast p1, LX/6kW;

    invoke-static {p1}, LX/Jr5;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
