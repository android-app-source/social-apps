.class public LX/Jih;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field public b:LX/3OS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2726370
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2726371
    const v0, 0x7f030d03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2726372
    const v0, 0x7f0d208b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/Jih;->a:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2726373
    const v0, 0x7f0d2088

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2726374
    const v1, 0x7f02128f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2726375
    const v0, 0x7f0d208a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2726376
    const v1, 0x7f080206

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2726377
    return-void
.end method

.method public static getSubtitleData(LX/Jih;)LX/FON;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2726378
    invoke-virtual {p0}, LX/Jih;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2726379
    sget-object v1, LX/Jig;->a:[I

    iget-object v2, p0, LX/Jih;->b:LX/3OS;

    iget-object v2, v2, LX/3OS;->a:LX/3OT;

    invoke-virtual {v2}, LX/3OT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2726380
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2726381
    :pswitch_0
    new-instance v0, LX/FON;

    const-string v1, ""

    invoke-direct {v0, v4, v1, v3}, LX/FON;-><init>(ZLjava/lang/String;LX/0Px;)V

    .line 2726382
    :goto_0
    return-object v0

    .line 2726383
    :pswitch_1
    const v1, 0x7f080207

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2726384
    new-instance v0, LX/FON;

    invoke-direct {v0, v4, v1, v3}, LX/FON;-><init>(ZLjava/lang/String;LX/0Px;)V

    goto :goto_0

    .line 2726385
    :pswitch_2
    iget-object v1, p0, LX/Jih;->b:LX/3OS;

    iget-object v1, v1, LX/3OS;->b:LX/0Px;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2726386
    iget-object v1, p0, LX/Jih;->b:LX/3OS;

    iget-object v1, v1, LX/3OS;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2726387
    const v1, 0x7f080207

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2726388
    new-instance v0, LX/FON;

    invoke-direct {v0, v4, v1, v3}, LX/FON;-><init>(ZLjava/lang/String;LX/0Px;)V

    goto :goto_0

    .line 2726389
    :cond_0
    iget-object v0, p0, LX/Jih;->b:LX/3OS;

    iget-object v0, v0, LX/3OS;->b:LX/0Px;

    new-instance v1, LX/Jif;

    invoke-direct {v1, p0}, LX/Jif;-><init>(LX/Jih;)V

    invoke-static {v0, v1}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2726390
    new-instance v0, LX/FON;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v3, v1}, LX/FON;-><init>(ZLjava/lang/String;LX/0Px;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
