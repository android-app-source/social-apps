.class public LX/JhJ;
.super LX/3LG;
.source ""

# interfaces
.implements LX/61x;


# static fields
.field private static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/Jh6;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/view/LayoutInflater;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Mi;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/3Mi;

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2724414
    const-class v0, LX/JhJ;

    sput-object v0, LX/JhJ;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Jh6;Landroid/content/Context;LX/0Or;Landroid/view/LayoutInflater;)V
    .locals 2
    .param p1    # LX/Jh6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/ForFavoritePickerList;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/contacts/favorites/EditFavoritesRowViewFactory;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/3Mi;",
            ">;",
            "Landroid/view/LayoutInflater;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2724401
    invoke-direct {p0}, LX/3LG;-><init>()V

    .line 2724402
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2724403
    iput-object v0, p0, LX/JhJ;->g:LX/0Px;

    .line 2724404
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2724405
    iput-object v0, p0, LX/JhJ;->d:LX/0Px;

    .line 2724406
    iput v1, p0, LX/JhJ;->i:I

    .line 2724407
    iput v1, p0, LX/JhJ;->j:I

    .line 2724408
    iput v1, p0, LX/JhJ;->k:I

    .line 2724409
    iput-object p1, p0, LX/JhJ;->a:LX/Jh6;

    .line 2724410
    iput-object p2, p0, LX/JhJ;->b:Landroid/content/Context;

    .line 2724411
    iput-object p3, p0, LX/JhJ;->f:LX/0Or;

    .line 2724412
    iput-object p4, p0, LX/JhJ;->c:Landroid/view/LayoutInflater;

    .line 2724413
    return-void
.end method

.method private static a(LX/3OQ;)Z
    .locals 1

    .prologue
    .line 2724400
    instance-of v0, p0, LX/JhD;

    if-nez v0, :cond_0

    instance-of v0, p0, LX/JhG;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2724357
    iput-object p1, p0, LX/JhJ;->d:LX/0Px;

    .line 2724358
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2724359
    const v0, 0x8ba5f2e

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2724360
    :goto_0
    return-void

    .line 2724361
    :cond_0
    const v0, 0x71cd960    # 1.1800016E-34f

    invoke-static {p0, v0}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()LX/333;
    .locals 1

    .prologue
    .line 2724399
    invoke-virtual {p0}, LX/3LG;->d()LX/3Mi;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2724384
    iput-object p1, p0, LX/JhJ;->g:LX/0Px;

    .line 2724385
    iget-object v0, p0, LX/JhJ;->g:LX/0Px;

    iput-object v0, p0, LX/JhJ;->d:LX/0Px;

    .line 2724386
    const v0, -0x51b854a4

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2724387
    iput v1, p0, LX/JhJ;->i:I

    .line 2724388
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2724389
    invoke-static {v0}, LX/JhJ;->a(LX/3OQ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2724390
    iget v0, p0, LX/JhJ;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JhJ;->i:I

    .line 2724391
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2724392
    :cond_1
    iput v1, p0, LX/JhJ;->j:I

    .line 2724393
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2724394
    invoke-static {v0}, LX/JhJ;->a(LX/3OQ;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2724395
    iget v0, p0, LX/JhJ;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JhJ;->j:I

    .line 2724396
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2724397
    :cond_2
    iget v0, p0, LX/JhJ;->j:I

    iget v1, p0, LX/JhJ;->i:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/JhJ;->k:I

    .line 2724398
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;LX/3Og;)V
    .locals 2

    .prologue
    .line 2724375
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2724376
    sget-object v0, LX/JhI;->a:[I

    .line 2724377
    iget-object v1, p2, LX/3Og;->a:LX/3Oh;

    move-object v1, v1

    .line 2724378
    invoke-virtual {v1}, LX/3Oh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2724379
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2724380
    invoke-direct {p0, v0}, LX/JhJ;->b(LX/0Px;)V

    .line 2724381
    :goto_0
    return-void

    .line 2724382
    :pswitch_0
    invoke-virtual {p2}, LX/3Og;->e()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/JhJ;->b(LX/0Px;)V

    goto :goto_0

    .line 2724383
    :pswitch_1
    invoke-virtual {p0}, LX/3LG;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2724374
    iget v0, p0, LX/JhJ;->j:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2724415
    iget v0, p0, LX/JhJ;->i:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/JhJ;->k:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final d()LX/3Mi;
    .locals 1

    .prologue
    .line 2724370
    iget-object v0, p0, LX/JhJ;->h:LX/3Mi;

    if-nez v0, :cond_0

    .line 2724371
    iget-object v0, p0, LX/JhJ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mi;

    iput-object v0, p0, LX/JhJ;->h:LX/3Mi;

    .line 2724372
    iget-object v0, p0, LX/JhJ;->h:LX/3Mi;

    invoke-interface {v0, p0}, LX/3Mi;->a(LX/3LI;)V

    .line 2724373
    :cond_0
    iget-object v0, p0, LX/JhJ;->h:LX/3Mi;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2724365
    iget-object v0, p0, LX/JhJ;->g:LX/0Px;

    iput-object v0, p0, LX/JhJ;->d:LX/0Px;

    .line 2724366
    iget-object v0, p0, LX/JhJ;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2724367
    const v0, -0x6164d645

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2724368
    :goto_0
    return-void

    .line 2724369
    :cond_0
    const v0, 0x6621bb25

    invoke-static {p0, v0}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2724364
    iget-object v0, p0, LX/JhJ;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2724363
    iget-object v0, p0, LX/JhJ;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2724362
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    .line 2724338
    iget-object v0, p0, LX/JhJ;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2724339
    instance-of v1, v0, LX/JhD;

    if-eqz v1, :cond_0

    .line 2724340
    const/4 v0, 0x0

    .line 2724341
    :goto_0
    return v0

    .line 2724342
    :cond_0
    instance-of v1, v0, LX/JhG;

    if-eqz v1, :cond_1

    .line 2724343
    const/4 v0, 0x4

    goto :goto_0

    .line 2724344
    :cond_1
    instance-of v1, v0, LX/Jgn;

    if-eqz v1, :cond_2

    .line 2724345
    const/4 v0, 0x1

    goto :goto_0

    .line 2724346
    :cond_2
    instance-of v1, v0, LX/Jgq;

    if-eqz v1, :cond_3

    .line 2724347
    const/4 v0, 0x5

    goto :goto_0

    .line 2724348
    :cond_3
    instance-of v1, v0, LX/DAa;

    if-eqz v1, :cond_4

    .line 2724349
    const/4 v0, 0x6

    goto :goto_0

    .line 2724350
    :cond_4
    sget-object v1, LX/Ji5;->d:LX/3OQ;

    if-ne v0, v1, :cond_5

    .line 2724351
    const/4 v0, 0x3

    goto :goto_0

    .line 2724352
    :cond_5
    sget-object v1, LX/Ji5;->e:LX/3OQ;

    if-ne v0, v1, :cond_6

    .line 2724353
    const/4 v0, 0x2

    goto :goto_0

    .line 2724354
    :cond_6
    sget-object v1, LX/Ji5;->g:LX/3OQ;

    if-ne v0, v1, :cond_7

    .line 2724355
    const/4 v0, 0x7

    goto :goto_0

    .line 2724356
    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2724245
    iget-object v0, p0, LX/JhJ;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2724246
    sget-object v1, LX/Ji5;->e:LX/3OQ;

    if-ne v0, v1, :cond_0

    .line 2724247
    iget-object v0, p0, LX/JhJ;->a:LX/Jh6;

    .line 2724248
    if-eqz p2, :cond_10

    .line 2724249
    :goto_0
    move-object v0, p2

    .line 2724250
    :goto_1
    return-object v0

    .line 2724251
    :cond_0
    instance-of v1, v0, LX/JhD;

    if-eqz v1, :cond_3

    .line 2724252
    iget-object v1, p0, LX/JhJ;->a:LX/Jh6;

    check-cast v0, LX/JhD;

    .line 2724253
    check-cast p2, LX/JhC;

    .line 2724254
    if-eqz p2, :cond_1

    .line 2724255
    iget-boolean v2, p2, LX/Idv;->a:Z

    move v2, v2

    .line 2724256
    if-nez v2, :cond_2

    .line 2724257
    :cond_1
    new-instance p2, LX/JhC;

    iget-object v2, v1, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/JhC;-><init>(Landroid/content/Context;)V

    .line 2724258
    :cond_2
    iput-object v0, p2, LX/JhC;->d:LX/JhD;

    .line 2724259
    iget-object v2, p2, LX/JhC;->d:LX/JhD;

    .line 2724260
    iget-object v1, v2, LX/JhD;->a:Lcom/facebook/user/model/User;

    move-object v1, v1

    .line 2724261
    iget-object v2, p2, LX/JhC;->h:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p2, LX/JhC;->b:LX/FJw;

    invoke-virtual {v0, v1}, LX/FJw;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2724262
    iget-object v2, p2, LX/JhC;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724263
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->ax()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2724264
    iget-object v0, p2, LX/JhC;->f:Landroid/widget/TextView;

    iget-object v2, p2, LX/JhC;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JhR;

    invoke-virtual {v2, v1}, LX/JhR;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724265
    iget-object v2, p2, LX/JhC;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2724266
    :goto_2
    iget-object v2, p2, LX/JhC;->g:Landroid/view/View;

    new-instance v1, LX/JhB;

    invoke-direct {v1, p2}, LX/JhB;-><init>(LX/JhC;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724267
    move-object v0, p2

    .line 2724268
    goto :goto_1

    .line 2724269
    :cond_3
    instance-of v1, v0, LX/Jgn;

    if-eqz v1, :cond_5

    .line 2724270
    iget-object v1, p0, LX/JhJ;->a:LX/Jh6;

    check-cast v0, LX/Jgn;

    .line 2724271
    check-cast p2, LX/Jgm;

    .line 2724272
    if-nez p2, :cond_4

    .line 2724273
    new-instance p2, LX/Jgm;

    iget-object v2, v1, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/Jgm;-><init>(Landroid/content/Context;)V

    .line 2724274
    :cond_4
    iput-object v0, p2, LX/Jgm;->g:LX/Jgn;

    .line 2724275
    iget-object v2, p2, LX/Jgm;->g:LX/Jgn;

    .line 2724276
    iget-object v1, v2, LX/Jgn;->a:Lcom/facebook/user/model/User;

    move-object v1, v1

    .line 2724277
    iget-object v2, p2, LX/Jgm;->f:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p2, LX/Jgm;->a:LX/FJw;

    invoke-virtual {v0, v1}, LX/FJw;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2724278
    iget-object v2, p2, LX/Jgm;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724279
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->ax()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2724280
    iget-object v0, p2, LX/Jgm;->d:Landroid/widget/TextView;

    iget-object v2, p2, LX/Jgm;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JhR;

    invoke-virtual {v2, v1}, LX/JhR;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724281
    iget-object v2, p2, LX/Jgm;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2724282
    :goto_3
    iget-object v2, p2, LX/Jgm;->e:Landroid/view/View;

    new-instance v1, LX/Jgl;

    invoke-direct {v1, p2}, LX/Jgl;-><init>(LX/Jgm;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724283
    move-object v0, p2

    .line 2724284
    goto/16 :goto_1

    .line 2724285
    :cond_5
    instance-of v1, v0, LX/DAa;

    if-eqz v1, :cond_7

    .line 2724286
    iget-object v1, p0, LX/JhJ;->a:LX/Jh6;

    check-cast v0, LX/DAa;

    .line 2724287
    check-cast p2, LX/Js7;

    .line 2724288
    if-nez p2, :cond_6

    .line 2724289
    new-instance p2, LX/Js7;

    iget-object v2, v1, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/Js7;-><init>(Landroid/content/Context;)V

    .line 2724290
    :cond_6
    iget-object v2, v0, LX/DAa;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2724291
    invoke-virtual {p2, v2}, LX/Js7;->setText(Ljava/lang/String;)V

    .line 2724292
    move-object v0, p2

    .line 2724293
    goto/16 :goto_1

    .line 2724294
    :cond_7
    instance-of v1, v0, LX/JhG;

    if-eqz v1, :cond_a

    .line 2724295
    iget-object v1, p0, LX/JhJ;->a:LX/Jh6;

    check-cast v0, LX/JhG;

    .line 2724296
    check-cast p2, LX/JhF;

    .line 2724297
    if-eqz p2, :cond_8

    .line 2724298
    iget-boolean v2, p2, LX/Idv;->a:Z

    move v2, v2

    .line 2724299
    if-nez v2, :cond_9

    .line 2724300
    :cond_8
    new-instance p2, LX/JhF;

    iget-object v2, v1, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/JhF;-><init>(Landroid/content/Context;)V

    .line 2724301
    :cond_9
    iput-object v0, p2, LX/JhF;->b:LX/JhG;

    .line 2724302
    iget-object v2, p2, LX/JhF;->b:LX/JhG;

    .line 2724303
    iget-object v1, v2, LX/JhG;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v1

    .line 2724304
    iget-object v1, p2, LX/JhF;->f:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v0, p2, LX/JhF;->h:LX/FJv;

    invoke-virtual {v0, v2}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2724305
    iget-object v1, p2, LX/JhF;->g:LX/3Ky;

    invoke-virtual {v1, v2}, LX/3Ky;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;

    move-result-object v2

    .line 2724306
    iget-object v1, p2, LX/JhF;->c:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v1, v2}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2724307
    iget-object v1, p2, LX/JhF;->d:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v1, v2}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2724308
    iget-object v2, p2, LX/JhF;->e:Landroid/view/View;

    new-instance v1, LX/JhE;

    invoke-direct {v1, p2}, LX/JhE;-><init>(LX/JhF;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724309
    move-object v0, p2

    .line 2724310
    goto/16 :goto_1

    .line 2724311
    :cond_a
    instance-of v1, v0, LX/Jgq;

    if-eqz v1, :cond_c

    .line 2724312
    iget-object v1, p0, LX/JhJ;->a:LX/Jh6;

    check-cast v0, LX/Jgq;

    .line 2724313
    check-cast p2, LX/Jgp;

    .line 2724314
    if-nez p2, :cond_b

    .line 2724315
    new-instance p2, LX/Jgp;

    iget-object v2, v1, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/Jgp;-><init>(Landroid/content/Context;)V

    .line 2724316
    :cond_b
    iput-object v0, p2, LX/Jgp;->g:LX/Jgq;

    .line 2724317
    iget-object v2, p2, LX/Jgp;->g:LX/Jgq;

    .line 2724318
    iget-object v1, v2, LX/Jgq;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v1

    .line 2724319
    iget-object v1, p2, LX/Jgp;->d:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v0, p2, LX/Jgp;->f:LX/FJv;

    invoke-virtual {v0, v2}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2724320
    iget-object v1, p2, LX/Jgp;->e:LX/3Ky;

    invoke-virtual {v1, v2}, LX/3Ky;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;

    move-result-object v2

    .line 2724321
    iget-object v1, p2, LX/Jgp;->a:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v1, v2}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2724322
    iget-object v1, p2, LX/Jgp;->b:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v1, v2}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2724323
    iget-object v2, p2, LX/Jgp;->c:Landroid/view/View;

    new-instance v1, LX/Jgo;

    invoke-direct {v1, p2}, LX/Jgo;-><init>(LX/Jgp;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724324
    move-object v0, p2

    .line 2724325
    goto/16 :goto_1

    .line 2724326
    :cond_c
    sget-object v1, LX/Ji5;->d:LX/3OQ;

    if-ne v0, v1, :cond_e

    .line 2724327
    iget-object v0, p0, LX/JhJ;->a:LX/Jh6;

    .line 2724328
    check-cast p2, LX/JhH;

    .line 2724329
    if-nez p2, :cond_d

    .line 2724330
    new-instance p2, LX/JhH;

    iget-object v1, v0, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JhH;-><init>(Landroid/content/Context;)V

    .line 2724331
    :cond_d
    move-object v0, p2

    .line 2724332
    goto/16 :goto_1

    .line 2724333
    :cond_e
    sget-object v1, LX/Ji5;->g:LX/3OQ;

    if-ne v0, v1, :cond_f

    .line 2724334
    iget-object v0, p0, LX/JhJ;->a:LX/Jh6;

    invoke-virtual {v0}, LX/Jh6;->a()Landroid/view/View;

    move-result-object v0

    goto/16 :goto_1

    .line 2724335
    :cond_f
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_10
    new-instance p2, LX/JhP;

    iget-object v1, v0, LX/Jh6;->a:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JhP;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2724336
    :cond_11
    iget-object v2, p2, LX/JhC;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2724337
    :cond_12
    iget-object v2, p2, LX/Jgm;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2724244
    const/16 v0, 0x8

    return v0
.end method
