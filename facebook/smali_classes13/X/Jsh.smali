.class public LX/Jsh;
.super LX/6lf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/Jsg;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2745647
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2745648
    return-void
.end method


# virtual methods
.method public final a(LX/6le;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V
    .locals 2

    .prologue
    .line 2745651
    check-cast p1, LX/Jsg;

    .line 2745652
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745653
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745654
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/5Sa;->a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v1

    .line 2745655
    iget-object v0, p1, LX/6le;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;

    .line 2745656
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->a(Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    .line 2745657
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2745649
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0315ff

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;

    .line 2745650
    new-instance v1, LX/Jsg;

    invoke-direct {v1, v0}, LX/Jsg;-><init>(Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;)V

    return-object v1
.end method
