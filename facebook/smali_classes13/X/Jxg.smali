.class public final LX/Jxg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jxh;


# direct methods
.method public constructor <init>(LX/Jxh;)V
    .locals 0

    .prologue
    .line 2753298
    iput-object p1, p0, LX/Jxg;->a:LX/Jxh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2753299
    iget-object v0, p0, LX/Jxg;->a:LX/Jxh;

    iget-object v0, v0, LX/Jxh;->f:LX/JxY;

    const v1, 0x7f083c1c

    invoke-virtual {v0, v1}, LX/JxY;->a(I)V

    .line 2753300
    iget-object v0, p0, LX/Jxg;->a:LX/Jxh;

    iget-object v0, v0, LX/Jxh;->b:LX/03V;

    const-string v1, "fail_to_fetch_stories_with_data"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2753301
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2753302
    check-cast p1, LX/0Px;

    const/4 v0, 0x0

    .line 2753303
    invoke-virtual {p1}, LX/0Px;->toArray()[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    .line 2753304
    if-nez v1, :cond_0

    .line 2753305
    iget-object v0, p0, LX/Jxg;->a:LX/Jxh;

    iget-object v0, v0, LX/Jxh;->f:LX/JxY;

    const v1, 0x7f083c1b

    invoke-virtual {v0, v1}, LX/JxY;->a(I)V

    .line 2753306
    :goto_0
    return-void

    .line 2753307
    :cond_0
    iget-object v2, p0, LX/Jxg;->a:LX/Jxh;

    iget-object v3, p0, LX/Jxg;->a:LX/Jxh;

    iget-object v3, v3, LX/Jxh;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/Jxh;->j:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    if-ge v3, v1, :cond_1

    iget-object v1, p0, LX/Jxg;->a:LX/Jxh;

    iget-object v1, v1, LX/Jxh;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Jxh;->j:LX/0Tn;

    invoke-interface {v1, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2753308
    :cond_1
    iput v0, v2, LX/Jxh;->g:I

    .line 2753309
    iget-object v0, p0, LX/Jxg;->a:LX/Jxh;

    .line 2753310
    iput-object p1, v0, LX/Jxh;->e:LX/0Px;

    .line 2753311
    iget-object v1, v0, LX/Jxh;->f:LX/JxY;

    invoke-virtual {v1}, LX/JxY;->a()V

    .line 2753312
    iget-object v1, v0, LX/Jxh;->f:LX/JxY;

    invoke-virtual {v1}, LX/JxY;->b()V

    .line 2753313
    goto :goto_0
.end method
