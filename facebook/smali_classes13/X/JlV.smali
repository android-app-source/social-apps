.class public LX/JlV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final v:Ljava/lang/Object;


# instance fields
.field private a:LX/3MX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Iew;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/Jn4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Og;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Ieu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2N4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/messaging/media/loader/LocalMediaLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/FJv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/DdY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/messaging/localfetch/FetchUserUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/3LP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Jre;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/0s6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2731565
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JlV;->v:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2731563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2731564
    return-void
.end method

.method public static a(LX/JlV;LX/0Rf;)LX/0P1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2731540
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2731541
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 2731542
    invoke-virtual {p1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2731543
    iget-object v4, p0, LX/JlV;->d:LX/2Og;

    invoke-virtual {v4, v0}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    .line 2731544
    if-eqz v4, :cond_3

    .line 2731545
    :goto_1
    move-object v4, v4

    .line 2731546
    if-nez v4, :cond_0

    .line 2731547
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2731548
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 2731549
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 2731550
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2731551
    :try_start_0
    iget-object v2, p0, LX/JlV;->j:Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    .line 2731552
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 2731553
    const-wide/16 v4, -0x1

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Ljava/util/Set;Ljava/util/Set;J)LX/JqV;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2731554
    iget-object v0, v0, LX/JqV;->a:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2731555
    :cond_2
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    :goto_2
    return-object v0

    .line 2731556
    :catch_0
    move-exception v0

    .line 2731557
    iget-object v1, p0, LX/JlV;->g:LX/03V;

    const-string v2, "InboxUnitFetcherHelper"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2731558
    const/4 v0, 0x0

    goto :goto_2

    .line 2731559
    :cond_3
    iget-object v4, p0, LX/JlV;->f:LX/2N4;

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v5

    const/4 p1, 0x0

    invoke-virtual {v4, v5, p1}, LX/2N4;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v4

    .line 2731560
    if-eqz v4, :cond_4

    sget-object v5, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    if-eq v4, v5, :cond_4

    .line 2731561
    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    goto :goto_1

    .line 2731562
    :cond_4
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/JlS",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2731339
    sget-object v0, LX/JlU;->a:[I

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2731340
    new-instance v0, LX/JlS;

    invoke-direct {v0, p1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)V

    .line 2731341
    :goto_0
    return-object v0

    .line 2731342
    :pswitch_0
    const/4 v1, 0x0

    .line 2731343
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 2731344
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731345
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v6

    .line 2731346
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->D()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_0

    .line 2731347
    invoke-virtual {v6}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->D()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v6, v0}, LX/JmN;->a(LX/15i;I)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2731348
    invoke-virtual {v3, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2731349
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2731350
    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_2

    .line 2731351
    :cond_3
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 2731352
    new-instance v1, LX/JlS;

    invoke-static {p0, v0}, LX/JlV;->a(LX/JlV;LX/0Rf;)LX/0P1;

    move-result-object v0

    const/4 v2, 0x0

    .line 2731353
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2731354
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    move v3, v2

    :goto_3
    if-ge v4, v7, :cond_4

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731355
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result p0

    if-eqz p0, :cond_15

    .line 2731356
    invoke-static {p1, v2, v0}, LX/JmN;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/0P1;)Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    move-result-object p0

    .line 2731357
    if-eqz p0, :cond_15

    .line 2731358
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {p0, v3}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2731359
    invoke-virtual {v5, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731360
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_3

    .line 2731361
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2731362
    invoke-direct {v1, p1, v0}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v1

    .line 2731363
    goto/16 :goto_0

    .line 2731364
    :pswitch_1
    iget-object v3, p0, LX/JlV;->e:LX/Ieu;

    const-string v4, "INBOX2"

    iget-object v5, p0, LX/JlV;->b:LX/Iew;

    .line 2731365
    new-instance v6, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    .line 2731366
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2731367
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v7, 0x0

    move v8, v7

    :goto_5
    if-ge v8, v11, :cond_6

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731368
    invoke-static {v7}, LX/Iew;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-result-object v7

    .line 2731369
    if-eqz v7, :cond_5

    .line 2731370
    invoke-virtual {v9, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731371
    :cond_5
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_5

    .line 2731372
    :cond_6
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    move-object v7, v7

    .line 2731373
    iget-object v8, v5, LX/Iew;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-direct {v6, v7, v8, v9}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;-><init>(LX/0Px;J)V

    move-object v5, v6

    .line 2731374
    invoke-virtual {v3, v4, v5}, LX/Ieu;->a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V

    .line 2731375
    new-instance v3, LX/JlS;

    const/4 v4, 0x0

    .line 2731376
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2731377
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v4

    move v5, v4

    :goto_6
    if-ge v6, v9, :cond_7

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731378
    invoke-static {v4}, LX/Iew;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-result-object v10

    .line 2731379
    if-eqz v10, :cond_16

    .line 2731380
    new-instance v10, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    invoke-static {v4}, LX/Iew;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-result-object v11

    invoke-direct {v10, p1, v4, v11}, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2731381
    add-int/lit8 v4, v5, 0x1

    invoke-virtual {v10, v5}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2731382
    invoke-virtual {v7, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731383
    :goto_7
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_6

    .line 2731384
    :cond_7
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 2731385
    invoke-direct {v3, p1, v4}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v3

    .line 2731386
    goto/16 :goto_0

    .line 2731387
    :pswitch_2
    new-instance v0, LX/JlS;

    const/4 v1, 0x0

    .line 2731388
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2731389
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v1

    move v2, v1

    :goto_8
    if-ge v3, v6, :cond_8

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731390
    invoke-static {v1}, LX/Jn4;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    move-result-object v7

    .line 2731391
    if-eqz v7, :cond_17

    .line 2731392
    new-instance v8, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;

    invoke-direct {v8, p1, v1, v7}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)V

    .line 2731393
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2731394
    invoke-virtual {v4, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731395
    :goto_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_8

    .line 2731396
    :cond_8
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2731397
    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731398
    goto/16 :goto_0

    .line 2731399
    :pswitch_3
    invoke-static {p0, p1}, LX/JlV;->n(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;

    move-result-object v0

    goto/16 :goto_0

    .line 2731400
    :pswitch_4
    const/4 v3, 0x0

    .line 2731401
    const/4 v2, 0x0

    .line 2731402
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v4

    .line 2731403
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_a
    if-ge v1, v6, :cond_a

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731404
    invoke-static {v0, v2}, LX/JlV;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Z)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2731405
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v0

    .line 2731406
    invoke-static {p0, v0}, LX/JlV;->b(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2731407
    if-eqz v0, :cond_9

    .line 2731408
    invoke-virtual {v4, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2731409
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 2731410
    :cond_a
    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 2731411
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    move-object v2, v3

    .line 2731412
    :goto_b
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2731413
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v4, v0

    :goto_c
    if-ge v4, v7, :cond_f

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731414
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/JlV;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Z)Z

    move-result v1

    if-nez v1, :cond_c

    .line 2731415
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v8

    .line 2731416
    invoke-static {p0, v8}, LX/JlV;->b(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2731417
    if-eqz v2, :cond_b

    if-nez v1, :cond_e

    :cond_b
    move-object v1, v3

    .line 2731418
    :goto_d
    new-instance v9, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;

    invoke-direct {v9, p1, v0, v1, v8}, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/Ddy;)V

    invoke-virtual {v5, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731419
    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_c

    .line 2731420
    :cond_d
    invoke-static {p0, v0}, LX/JlV;->a(LX/JlV;LX/0Rf;)LX/0P1;

    move-result-object v0

    move-object v2, v0

    goto :goto_b

    .line 2731421
    :cond_e
    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    goto :goto_d

    .line 2731422
    :cond_f
    new-instance v0, LX/JlS;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731423
    goto/16 :goto_0

    .line 2731424
    :pswitch_5
    new-instance v0, LX/JlS;

    invoke-static {p1}, LX/Jo4;->b(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    move-result-object v1

    const/4 v5, 0x0

    .line 2731425
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 2731426
    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_18

    :cond_10
    move-object v2, v3

    .line 2731427
    :goto_e
    move-object v2, v2

    .line 2731428
    invoke-static {v1, v2}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731429
    goto/16 :goto_0

    .line 2731430
    :pswitch_6
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 2731431
    iget-object v1, p0, LX/JlV;->m:LX/1Ml;

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v1, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    move v1, v1

    .line 2731432
    if-nez v1, :cond_1b

    .line 2731433
    :goto_f
    move-object v0, v0

    .line 2731434
    goto/16 :goto_0

    .line 2731435
    :pswitch_7
    new-instance v0, LX/JlS;

    const/4 v1, 0x0

    .line 2731436
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2731437
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v1

    move v3, v1

    :goto_10
    if-ge v4, v7, :cond_12

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731438
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v2

    if-eqz v2, :cond_1f

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->l()I

    move-result v2

    if-eqz v2, :cond_1f

    .line 2731439
    add-int/lit8 v2, v3, 0x1

    invoke-static {p1, v1, v3}, LX/JlG;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;I)Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    move-result-object v1

    .line 2731440
    if-eqz v1, :cond_11

    .line 2731441
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_11
    move v1, v2

    .line 2731442
    :goto_11
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    goto :goto_10

    .line 2731443
    :cond_12
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2731444
    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731445
    goto/16 :goto_0

    .line 2731446
    :pswitch_8
    new-instance v0, LX/JlS;

    invoke-static {p0, p1}, LX/JlV;->s(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731447
    goto/16 :goto_0

    .line 2731448
    :pswitch_9
    invoke-static {p1}, LX/JlV;->m(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;

    move-result-object v0

    goto/16 :goto_0

    .line 2731449
    :pswitch_a
    invoke-static {p1}, LX/JlV;->i(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;

    move-result-object v0

    goto/16 :goto_0

    .line 2731450
    :pswitch_b
    invoke-static {p1}, LX/JlV;->j(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;

    move-result-object v0

    goto/16 :goto_0

    .line 2731451
    :pswitch_c
    invoke-static {p1}, LX/JlV;->u(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;

    move-result-object v2

    .line 2731452
    new-instance v1, LX/JlS;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x0

    :goto_12
    invoke-direct {v1, p1, v0}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v1

    .line 2731453
    goto/16 :goto_0

    .line 2731454
    :cond_13
    new-instance v0, LX/Jkv;

    invoke-direct {v0, v2}, LX/Jkv;-><init>(LX/0Px;)V

    goto :goto_12

    .line 2731455
    :pswitch_d
    new-instance v0, LX/JlS;

    invoke-static {p0, p1}, LX/JlV;->t(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2731456
    :pswitch_e
    new-instance v0, LX/JlS;

    invoke-static {p1}, LX/Jmi;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/Jme;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731457
    goto/16 :goto_0

    .line 2731458
    :pswitch_f
    iget-object v0, p0, LX/JlV;->u:LX/0s6;

    .line 2731459
    invoke-static {v0}, LX/36d;->a(LX/0s6;)Z

    move-result v1

    if-nez v1, :cond_20

    .line 2731460
    const/4 v1, 0x0

    .line 2731461
    :goto_13
    move-object v0, v1

    .line 2731462
    goto/16 :goto_0

    .line 2731463
    :pswitch_10
    new-instance v0, LX/JlS;

    const/4 v1, 0x0

    .line 2731464
    if-nez p1, :cond_21

    .line 2731465
    :cond_14
    :goto_14
    move-object v1, v1

    .line 2731466
    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v0

    .line 2731467
    goto/16 :goto_0

    .line 2731468
    :pswitch_11
    const/4 v1, 0x0

    .line 2731469
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v3

    .line 2731470
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2d

    move-object v0, v1

    .line 2731471
    :goto_15
    move-object v0, v0

    .line 2731472
    goto/16 :goto_0

    :cond_15
    move v2, v3

    goto/16 :goto_4

    :cond_16
    move v4, v5

    goto/16 :goto_7

    :cond_17
    move v1, v2

    goto/16 :goto_9

    .line 2731473
    :cond_18
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_16
    if-ge v6, v8, :cond_1a

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731474
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v2

    .line 2731475
    if-eqz v2, :cond_19

    .line 2731476
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->q()LX/0Px;

    move-result-object v9

    .line 2731477
    invoke-static {v9}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 2731478
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v4, v5

    :goto_17
    if-ge v4, v10, :cond_19

    invoke-virtual {v9, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerHiddenAuthorsAttachmentModel$HiddenAuthorsModel;

    .line 2731479
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerHiddenAuthorsAttachmentModel$HiddenAuthorsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2731480
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_17

    .line 2731481
    :cond_19
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_16

    :cond_1a
    move-object v2, v3

    .line 2731482
    goto/16 :goto_e

    .line 2731483
    :cond_1b
    iget-object v1, p0, LX/JlV;->i:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-static {}, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a()LX/IgU;

    move-result-object v2

    .line 2731484
    iput-boolean v3, v2, LX/IgU;->a:Z

    .line 2731485
    move-object v2, v2

    .line 2731486
    if-lez v3, :cond_1e

    const/4 p0, 0x1

    :goto_18
    invoke-static {p0}, LX/0PB;->checkArgument(Z)V

    .line 2731487
    iput v3, v2, LX/IgU;->b:I

    .line 2731488
    move-object v2, v2

    .line 2731489
    invoke-virtual {v2}, LX/IgU;->e()Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;

    move-result-object v2

    .line 2731490
    invoke-static {v1, v2}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b(Lcom/facebook/messaging/media/loader/LocalMediaLoader;Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2731491
    const v4, 0x1c7fcd14

    :try_start_0
    invoke-static {v3, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2731492
    :goto_19
    move-object v1, v3

    .line 2731493
    if-eqz v1, :cond_1c

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2731494
    :cond_1c
    :goto_1a
    new-instance v1, LX/JlS;

    invoke-direct {v1, p1, v0}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_f

    .line 2731495
    :cond_1d
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    goto :goto_1a

    .line 2731496
    :cond_1e
    const/4 p0, 0x0

    goto :goto_18

    .line 2731497
    :catch_0
    move-exception v3

    .line 2731498
    iget-object v4, v1, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->e:LX/03V;

    sget-object p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->a:Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2731499
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2731500
    goto :goto_19

    :cond_1f
    move v1, v3

    goto/16 :goto_11

    :cond_20
    new-instance v1, LX/JlS;

    invoke-static {p1}, LX/JmE;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JmF;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    goto/16 :goto_13

    .line 2731501
    :cond_21
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;

    move-result-object v2

    if-eqz v2, :cond_25

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;

    move-result-object v2

    :goto_1b
    move-object v2, v2

    .line 2731502
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;->SINGLE_ITEM_ADS_UNIT:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;

    if-ne v2, v3, :cond_23

    .line 2731503
    new-instance v1, LX/Jeu;

    const/4 v3, 0x0

    .line 2731504
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_22

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_26

    :cond_22
    move-object v2, v3

    .line 2731505
    :goto_1c
    move-object v2, v2

    .line 2731506
    invoke-direct {v1, v2}, LX/Jeu;-><init>(Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;)V

    goto/16 :goto_14

    .line 2731507
    :cond_23
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;->HSCROLL_ADS_UNIT:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;

    if-ne v2, v3, :cond_14

    .line 2731508
    new-instance v1, LX/Jeu;

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2731509
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_24

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_29

    :cond_24
    move-object v2, v4

    .line 2731510
    :goto_1d
    move-object v2, v2

    .line 2731511
    invoke-direct {v1, v2}, LX/Jeu;-><init>(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;)V

    goto/16 :goto_14

    :cond_25
    const/4 v2, 0x0

    goto :goto_1b

    .line 2731512
    :cond_26
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731513
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v4

    .line 2731514
    if-eqz v4, :cond_27

    invoke-static {v4}, LX/Jev;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Z

    move-result v5

    if-nez v5, :cond_28

    :cond_27
    move-object v2, v3

    .line 2731515
    goto :goto_1c

    .line 2731516
    :cond_28
    new-instance v3, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;

    invoke-virtual {v4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->a()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;

    move-result-object v5

    invoke-static {v5}, LX/Jev;->b(Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;)Lcom/facebook/user/model/User;

    move-result-object v5

    invoke-direct {v3, p1, v2, v4, v5}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/user/model/User;)V

    move-object v2, v3

    goto :goto_1c

    .line 2731517
    :cond_29
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2731518
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v2

    move v3, v2

    :goto_1e
    if-ge v5, v8, :cond_2a

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731519
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v9

    if-eqz v9, :cond_2c

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->l()I

    move-result v9

    if-eqz v9, :cond_2c

    .line 2731520
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v9

    .line 2731521
    if-eqz v9, :cond_2c

    invoke-static {v9}, LX/Jev;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Z

    move-result v10

    if-eqz v10, :cond_2c

    .line 2731522
    new-instance v10, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    invoke-virtual {v9}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->a()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;

    move-result-object v11

    invoke-static {v11}, LX/Jev;->b(Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;)Lcom/facebook/user/model/User;

    move-result-object v11

    invoke-direct {v10, p1, v2, v9, v11}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/user/model/User;)V

    .line 2731523
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v10, v3}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2731524
    invoke-virtual {v6, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731525
    :goto_1f
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_1e

    .line 2731526
    :cond_2a
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2731527
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2b

    .line 2731528
    new-instance v2, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;LX/0Px;)V

    goto/16 :goto_1d

    :cond_2b
    move-object v2, v4

    .line 2731529
    goto/16 :goto_1d

    :cond_2c
    move v2, v3

    goto :goto_1f

    .line 2731530
    :cond_2d
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2731531
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_20
    if-ge v2, v5, :cond_2f

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731532
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v6

    .line 2731533
    if-eqz v6, :cond_2e

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v7

    if-eqz v7, :cond_2e

    .line 2731534
    invoke-virtual {v6}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->E()Ljava/lang/String;

    move-result-object v7

    .line 2731535
    invoke-virtual {v6}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->C()Ljava/lang/String;

    move-result-object v6

    .line 2731536
    new-instance v8, LX/JmC;

    invoke-direct {v8, v0, v7, v6}, LX/JmC;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731537
    :cond_2e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_20

    .line 2731538
    :cond_2f
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2731539
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_30

    move-object v0, v1

    goto/16 :goto_15

    :cond_30
    new-instance v0, LX/JlS;

    invoke-direct {v0, p1, v2}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    goto/16 :goto_15

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/JlV;
    .locals 7

    .prologue
    .line 2731312
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2731313
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2731314
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2731315
    if-nez v1, :cond_0

    .line 2731316
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2731317
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2731318
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2731319
    sget-object v1, LX/JlV;->v:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2731320
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2731321
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2731322
    :cond_1
    if-nez v1, :cond_4

    .line 2731323
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2731324
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2731325
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JlV;->b(LX/0QB;)LX/JlV;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2731326
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2731327
    if-nez v1, :cond_2

    .line 2731328
    sget-object v0, LX/JlV;->v:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlV;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2731329
    :goto_1
    if-eqz v0, :cond_3

    .line 2731330
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2731331
    :goto_3
    check-cast v0, LX/JlV;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2731332
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2731333
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2731334
    :catchall_1
    move-exception v0

    .line 2731335
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2731336
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2731337
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2731338
    :cond_2
    :try_start_8
    sget-object v0, LX/JlV;->v:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlV;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2731306
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->D()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2731307
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731308
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2731309
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v3

    .line 2731310
    invoke-virtual {v1, v0, v5}, LX/15i;->h(II)Z

    move-result v0

    invoke-virtual {v1, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, LX/JlV;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0

    .line 2731311
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4

    .prologue
    .line 2731303
    if-eqz p1, :cond_0

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, LX/JlV;->o:Lcom/facebook/user/model/User;

    .line 2731304
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2731305
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/JlV;LX/3MX;LX/Iew;LX/Jn4;LX/2Og;LX/Ieu;LX/2N4;LX/03V;LX/0Uh;Lcom/facebook/messaging/media/loader/LocalMediaLoader;Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;LX/FJv;LX/DdY;LX/1Ml;LX/0hB;Lcom/facebook/user/model/User;Lcom/facebook/messaging/localfetch/FetchUserUtil;LX/3LP;LX/2RQ;Landroid/content/res/Resources;LX/Jre;LX/0s6;)V
    .locals 1

    .prologue
    .line 2731302
    iput-object p1, p0, LX/JlV;->a:LX/3MX;

    iput-object p2, p0, LX/JlV;->b:LX/Iew;

    iput-object p3, p0, LX/JlV;->c:LX/Jn4;

    iput-object p4, p0, LX/JlV;->d:LX/2Og;

    iput-object p5, p0, LX/JlV;->e:LX/Ieu;

    iput-object p6, p0, LX/JlV;->f:LX/2N4;

    iput-object p7, p0, LX/JlV;->g:LX/03V;

    iput-object p8, p0, LX/JlV;->h:LX/0Uh;

    iput-object p9, p0, LX/JlV;->i:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    iput-object p10, p0, LX/JlV;->j:Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    iput-object p11, p0, LX/JlV;->k:LX/FJv;

    iput-object p12, p0, LX/JlV;->l:LX/DdY;

    iput-object p13, p0, LX/JlV;->m:LX/1Ml;

    iput-object p14, p0, LX/JlV;->n:LX/0hB;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/JlV;->o:Lcom/facebook/user/model/User;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/JlV;->p:Lcom/facebook/messaging/localfetch/FetchUserUtil;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/JlV;->q:LX/3LP;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/JlV;->r:LX/2RQ;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/JlV;->s:Landroid/content/res/Resources;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/JlV;->t:LX/Jre;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/JlV;->u:LX/0s6;

    return-void
.end method

.method private static a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/messaging/model/threads/ThreadSummary;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2731128
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->A()Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNREAD:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    if-ne v1, v2, :cond_1

    .line 2731129
    iget-object v1, p0, LX/JlV;->l:LX/DdY;

    .line 2731130
    iget-object v2, v1, LX/DdY;->a:LX/2Og;

    iget-object p0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, p0}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 2731131
    if-nez v2, :cond_2

    .line 2731132
    invoke-virtual {p2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v2

    .line 2731133
    :goto_0
    move v1, v2

    .line 2731134
    if-nez v1, :cond_1

    .line 2731135
    :cond_0
    :goto_1
    return v0

    :cond_1
    iget-object v1, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v2, LX/6ek;->ARCHIVED:LX/6ek;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v2

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2731293
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v1

    if-nez v1, :cond_1

    .line 2731294
    if-eqz p1, :cond_0

    .line 2731295
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->j()Ljava/lang/String;

    .line 2731296
    :cond_0
    :goto_0
    return v0

    .line 2731297
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->l()I

    move-result v1

    if-nez v1, :cond_2

    .line 2731298
    if-eqz p1, :cond_0

    .line 2731299
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->j()Ljava/lang/String;

    goto :goto_0

    .line 2731300
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2731301
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/JlV;
    .locals 22

    .prologue
    .line 2731290
    new-instance v0, LX/JlV;

    invoke-direct {v0}, LX/JlV;-><init>()V

    .line 2731291
    invoke-static/range {p0 .. p0}, LX/3MX;->a(LX/0QB;)LX/3MX;

    move-result-object v1

    check-cast v1, LX/3MX;

    invoke-static/range {p0 .. p0}, LX/Iew;->a(LX/0QB;)LX/Iew;

    move-result-object v2

    check-cast v2, LX/Iew;

    invoke-static/range {p0 .. p0}, LX/Jn4;->a(LX/0QB;)LX/Jn4;

    move-result-object v3

    check-cast v3, LX/Jn4;

    invoke-static/range {p0 .. p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v4

    check-cast v4, LX/2Og;

    invoke-static/range {p0 .. p0}, LX/Ieu;->a(LX/0QB;)LX/Ieu;

    move-result-object v5

    check-cast v5, LX/Ieu;

    invoke-static/range {p0 .. p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v6

    check-cast v6, LX/2N4;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->a(LX/0QB;)Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    move-result-object v9

    check-cast v9, Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(LX/0QB;)Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    move-result-object v10

    check-cast v10, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    invoke-static/range {p0 .. p0}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v11

    check-cast v11, LX/FJv;

    invoke-static/range {p0 .. p0}, LX/DdY;->a(LX/0QB;)LX/DdY;

    move-result-object v12

    check-cast v12, LX/DdY;

    invoke-static/range {p0 .. p0}, LX/1Ml;->a(LX/0QB;)LX/1Ml;

    move-result-object v13

    check-cast v13, LX/1Ml;

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v14

    check-cast v14, LX/0hB;

    invoke-static/range {p0 .. p0}, LX/0XE;->a(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v15

    check-cast v15, Lcom/facebook/user/model/User;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/localfetch/FetchUserUtil;->a(LX/0QB;)Lcom/facebook/messaging/localfetch/FetchUserUtil;

    move-result-object v16

    check-cast v16, Lcom/facebook/messaging/localfetch/FetchUserUtil;

    invoke-static/range {p0 .. p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v17

    check-cast v17, LX/3LP;

    invoke-static/range {p0 .. p0}, LX/2RQ;->a(LX/0QB;)LX/2RQ;

    move-result-object v18

    check-cast v18, LX/2RQ;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v19

    check-cast v19, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/Jre;->a(LX/0QB;)LX/Jre;

    move-result-object v20

    check-cast v20, LX/Jre;

    invoke-static/range {p0 .. p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v21

    check-cast v21, LX/0s6;

    invoke-static/range {v0 .. v21}, LX/JlV;->a(LX/JlV;LX/3MX;LX/Iew;LX/Jn4;LX/2Og;LX/Ieu;LX/2N4;LX/03V;LX/0Uh;Lcom/facebook/messaging/media/loader/LocalMediaLoader;Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;LX/FJv;LX/DdY;LX/1Ml;LX/0hB;Lcom/facebook/user/model/User;Lcom/facebook/messaging/localfetch/FetchUserUtil;LX/3LP;LX/2RQ;Landroid/content/res/Resources;LX/Jre;LX/0s6;)V

    .line 2731292
    return-object v0
.end method

.method public static b(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2731124
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->t()Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel;

    move-result-object v0

    .line 2731125
    if-nez v0, :cond_0

    .line 2731126
    const/4 v0, 0x0

    .line 2731127
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel;->a()Z

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel;->j()Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel$ThreadKeyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel$ThreadKeyModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel;->j()Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel$ThreadKeyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemThreadModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, LX/JlV;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0
.end method

.method private static i(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/JlS",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2731136
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2731137
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_3

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731138
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v0

    .line 2731139
    if-eqz v0, :cond_2

    .line 2731140
    invoke-static {v0}, LX/Jms;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    move-result-object v3

    .line 2731141
    if-eqz v3, :cond_2

    .line 2731142
    const/4 v1, 0x0

    .line 2731143
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$LikeSentenceModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$LikeSentenceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$LikeSentenceModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2731144
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$LikeSentenceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$LikeSentenceModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2731145
    :cond_0
    move-object v4, v1

    .line 2731146
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 2731147
    :cond_1
    const/4 v1, 0x0

    .line 2731148
    :goto_1
    move-object v5, v1

    .line 2731149
    new-instance v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731150
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 2731151
    :cond_3
    new-instance v0, LX/JlS;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    return-object v0

    .line 2731152
    :cond_4
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2731153
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->B()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v1, 0x0

    move v5, v1

    :goto_2
    if-ge v5, v12, :cond_6

    invoke-virtual {v11, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel;

    .line 2731154
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel$ProfilePictureModel;

    move-result-object v13

    if-eqz v13, :cond_5

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel$ProfilePictureModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 2731155
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$ExternalUrlInboxItemFragmentModel$StoryFeedbackModel$ReactorsModel$NodesModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v10, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731156
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 2731157
    :cond_6
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_1
.end method

.method private static j(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/JlS",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2731158
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2731159
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    if-ge v8, v11, :cond_1

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731160
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v2

    .line 2731161
    if-eqz v2, :cond_0

    .line 2731162
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->z()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;

    move-result-object v7

    .line 2731163
    new-instance v2, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->p()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->k()LX/0Px;

    move-result-object v7

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->USER_CONTROL_TOPIC_MANAGE_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v7, v12}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-direct/range {v2 .. v7}, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2731164
    invoke-virtual {v9, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731165
    :cond_0
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_0

    .line 2731166
    :cond_1
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2731167
    new-instance v1, LX/JlS;

    invoke-direct {v1, p0, v0}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    return-object v1
.end method

.method private static m(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/JlS",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 2731168
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v9

    .line 2731169
    invoke-virtual {v9}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2731170
    :cond_0
    :goto_0
    return-object v6

    .line 2731171
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2731172
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v11

    move v7, v8

    :goto_1
    if-ge v7, v11, :cond_7

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731173
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v1

    .line 2731174
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->o()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731175
    if-nez v2, :cond_3

    move-object v2, v6

    .line 2731176
    :goto_2
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v3

    if-eqz v3, :cond_2

    .line 2731177
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->p()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2731178
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->j()LX/1vs;

    move-result-object v5

    iget-object v12, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    sget-object v13, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v13

    :try_start_2
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2731179
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->s()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$ItemImageModel;

    move-result-object v1

    .line 2731180
    if-nez v3, :cond_4

    move-object v3, v6

    .line 2731181
    :goto_3
    if-nez v5, :cond_5

    move-object v4, v6

    .line 2731182
    :goto_4
    if-nez v1, :cond_6

    move-object v5, v6

    .line 2731183
    :goto_5
    new-instance v1, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

    invoke-direct {v1, p0, v0}, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2731184
    new-instance v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;-><init>(Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731185
    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 2731186
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2731187
    :cond_3
    invoke-virtual {v3, v2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2731188
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2731189
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 2731190
    :cond_4
    invoke-virtual {v4, v3, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2731191
    :cond_5
    invoke-virtual {v12, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 2731192
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$ItemImageModel;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 2731193
    :cond_7
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2731194
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v6, LX/JlS;

    invoke-direct {v6, p0, v0}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private static n(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/JlS",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2731195
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v4

    .line 2731196
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731197
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v7

    .line 2731198
    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2731199
    invoke-static {p0, v7}, LX/JlV;->a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2731200
    invoke-virtual {v4, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2731201
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2731202
    :cond_1
    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 2731203
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v1

    .line 2731204
    :goto_1
    return-object v0

    .line 2731205
    :cond_2
    invoke-static {p0, v0}, LX/JlV;->a(LX/JlV;LX/0Rf;)LX/0P1;

    move-result-object v7

    .line 2731206
    if-nez v7, :cond_3

    move-object v0, v1

    .line 2731207
    goto :goto_1

    .line 2731208
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2731209
    iget-object v0, p0, LX/JlV;->h:LX/0Uh;

    const/16 v1, 0x1e7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 2731210
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v2

    :goto_2
    if-ge v6, v10, :cond_7

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731211
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v0

    .line 2731212
    if-eqz v0, :cond_6

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v1

    if-eqz v1, :cond_6

    .line 2731213
    invoke-static {p0, v0}, LX/JlV;->a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2731214
    invoke-virtual {v7, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2731215
    invoke-static {p0, v0, v3}, LX/JlV;->a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2731216
    iget-object v0, p0, LX/JlV;->k:LX/FJv;

    const/4 v1, 0x1

    .line 2731217
    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadSummary;->b()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadSummary;->c()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2731218
    :cond_4
    invoke-static {v0, v3}, LX/FJv;->c(LX/FJv;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/net/Uri;

    move-result-object v4

    .line 2731219
    invoke-virtual {v4}, Landroid/net/Uri;->isAbsolute()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2731220
    :cond_5
    :goto_3
    move v4, v1

    .line 2731221
    new-instance v0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/model/threads/ThreadSummary;IZ)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731222
    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    .line 2731223
    :cond_7
    new-instance v0, LX/JlS;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JlS;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Ljava/lang/Object;)V

    goto :goto_1

    .line 2731224
    :cond_8
    invoke-static {v3}, LX/2Of;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;)I

    move-result v4

    .line 2731225
    const/4 v11, 0x2

    if-le v4, v11, :cond_5

    .line 2731226
    iget-object v4, v0, LX/FJv;->b:LX/2Of;

    invoke-virtual {v4, v3}, LX/2Of;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/util/List;

    move-result-object v4

    .line 2731227
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v11, 0x3

    invoke-static {v4, v11}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_3
.end method

.method public static s(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/rtc/RtcRecommendationInboxItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2731228
    iget-object v0, p0, LX/JlV;->r:LX/2RQ;

    sget-object v1, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/2RQ;->c(Ljava/util/Collection;I)LX/2RR;

    move-result-object v0

    .line 2731229
    iget-object v1, p0, LX/JlV;->q:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 2731230
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2731231
    if-eqz v1, :cond_1

    .line 2731232
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2731233
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2731234
    new-instance v3, Lcom/facebook/messaging/inbox2/rtc/RtcRecommendationInboxItem;

    invoke-direct {v3, p1, v0}, Lcom/facebook/messaging/inbox2/rtc/RtcRecommendationInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/user/model/User;)V

    .line 2731235
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2731236
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/3On;->close()V

    .line 2731237
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static t(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2731238
    invoke-static {p1}, LX/JlV;->u(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;

    move-result-object v0

    .line 2731239
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2731240
    const/4 v0, 0x0

    .line 2731241
    :goto_0
    return-object v0

    .line 2731242
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/JlV;->p:Lcom/facebook/messaging/localfetch/FetchUserUtil;

    .line 2731243
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731244
    invoke-static {v1, v0}, Lcom/facebook/messaging/localfetch/FetchUserUtil;->b(Lcom/facebook/messaging/localfetch/FetchUserUtil;Ljava/util/List;)LX/1MF;

    move-result-object v2

    .line 2731245
    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    new-instance v3, LX/FFx;

    invoke-direct {v3, v1}, LX/FFx;-><init>(Lcom/facebook/messaging/localfetch/FetchUserUtil;)V

    invoke-static {v2, v3}, LX/2Ck;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v1, v2

    .line 2731246
    const v2, -0x7072d007

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    .line 2731247
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 2731248
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2731249
    iget-object v6, v2, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v6, v6

    .line 2731250
    invoke-virtual {v4, v6, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2731251
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2731252
    :cond_1
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 2731253
    :goto_2
    move-object v2, v1

    .line 2731254
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2731255
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731256
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v7

    .line 2731257
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731258
    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->G()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    const/4 v6, 0x1

    :goto_4
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 2731259
    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->G()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/user/model/User;

    .line 2731260
    if-nez v9, :cond_5

    .line 2731261
    const/4 v6, 0x0

    .line 2731262
    :goto_5
    move-object v0, v6

    .line 2731263
    if-eqz v0, :cond_2

    .line 2731264
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731265
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2731266
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 2731267
    :catch_0
    move-exception v1

    .line 2731268
    :goto_6
    const-string v2, "InboxUnitFetcherHelper"

    const-string v3, "fetchUsers interrupted"

    invoke-static {v2, v3, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2731269
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 2731270
    goto :goto_2

    .line 2731271
    :catch_1
    move-exception v1

    goto :goto_6

    .line 2731272
    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    .line 2731273
    :cond_5
    new-instance v6, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->y()D

    move-result-wide v10

    invoke-virtual {v7}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->x()D

    move-result-wide v12

    move-object v7, p1

    move-object v8, v0

    invoke-direct/range {v6 .. v13}, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/user/model/User;DD)V

    goto :goto_5
.end method

.method private static u(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2731274
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2731275
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2731276
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v0

    .line 2731277
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->G()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2731278
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2731279
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2731280
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 2731281
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2731282
    const-string v1, "montage_nux_image_width"

    iget-object v2, p0, LX/JlV;->n:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731283
    const-string v1, "montage_nux_image_height"

    iget-object v2, p0, LX/JlV;->n:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731284
    const-string v1, "scaling_factor"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731285
    const-string v1, "square_profile_pic_size_big"

    iget-object v2, p0, LX/JlV;->a:LX/3MX;

    sget-object v3, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v2, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731286
    const-string v1, "friend_count"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731287
    const-string v1, "external_url_item_image_size"

    iget-object v2, p0, LX/JlV;->s:Landroid/content/res/Resources;

    const v3, 0x7f0b264c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731288
    const-string v1, "profile_pic_small_size"

    iget-object v2, p0, LX/JlV;->a:LX/3MX;

    sget-object v3, LX/3MY;->SMALL:LX/3MY;

    invoke-virtual {v2, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731289
    return-object v0
.end method
