.class public final LX/Jzp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Jzq;

.field private final b:Landroid/app/FragmentManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Jzq;LX/0gc;)V
    .locals 1

    .prologue
    .line 2756781
    iput-object p1, p0, LX/Jzp;->a:LX/Jzq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756782
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jzp;->b:Landroid/app/FragmentManager;

    .line 2756783
    iput-object p2, p0, LX/Jzp;->c:LX/0gc;

    .line 2756784
    return-void
.end method

.method public constructor <init>(LX/Jzq;Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 2756777
    iput-object p1, p0, LX/Jzp;->a:LX/Jzq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756778
    iput-object p2, p0, LX/Jzp;->b:Landroid/app/FragmentManager;

    .line 2756779
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jzp;->c:LX/0gc;

    .line 2756780
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 2756776
    iget-object v0, p0, LX/Jzp;->c:LX/0gc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2756745
    invoke-direct {p0}, LX/Jzp;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2756746
    iget-object v0, p0, LX/Jzp;->c:LX/0gc;

    const-string v1, "com.facebook.catalyst.react.dialog.DialogModule"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;

    .line 2756747
    if-eqz v0, :cond_0

    .line 2756748
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2756749
    :cond_0
    :goto_0
    return-void

    .line 2756750
    :cond_1
    iget-object v0, p0, LX/Jzp;->b:Landroid/app/FragmentManager;

    const-string v1, "com.facebook.catalyst.react.dialog.DialogModule"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, LX/Jzn;

    .line 2756751
    if-eqz v0, :cond_0

    .line 2756752
    invoke-virtual {v0}, LX/Jzn;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2756770
    iget-object v0, p0, LX/Jzp;->d:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 2756771
    :goto_0
    return-void

    .line 2756772
    :cond_0
    invoke-direct {p0}, LX/Jzp;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2756773
    iget-object v0, p0, LX/Jzp;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;

    iget-object v1, p0, LX/Jzp;->c:LX/0gc;

    const-string v2, "com.facebook.catalyst.react.dialog.DialogModule"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2756774
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jzp;->d:Ljava/lang/Object;

    goto :goto_0

    .line 2756775
    :cond_1
    iget-object v0, p0, LX/Jzp;->d:Ljava/lang/Object;

    check-cast v0, LX/Jzn;

    iget-object v1, p0, LX/Jzp;->b:Landroid/app/FragmentManager;

    const-string v2, "com.facebook.catalyst.react.dialog.DialogModule"

    invoke-virtual {v0, v1, v2}, LX/Jzn;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(ZLandroid/os/Bundle;Lcom/facebook/react/bridge/Callback;)V
    .locals 3

    .prologue
    .line 2756753
    invoke-direct {p0}, LX/Jzp;->c()V

    .line 2756754
    if-eqz p3, :cond_1

    new-instance v0, LX/Jzo;

    iget-object v1, p0, LX/Jzp;->a:LX/Jzq;

    invoke-direct {v0, v1, p3}, LX/Jzo;-><init>(LX/Jzq;Lcom/facebook/react/bridge/Callback;)V

    .line 2756755
    :goto_0
    invoke-direct {p0}, LX/Jzp;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2756756
    new-instance v1, Lcom/facebook/react/modules/dialog/SupportAlertFragment;

    invoke-direct {v1, v0, p2}, Lcom/facebook/react/modules/dialog/SupportAlertFragment;-><init>(LX/Jzo;Landroid/os/Bundle;)V

    .line 2756757
    if-eqz p1, :cond_2

    .line 2756758
    const-string v0, "cancelable"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756759
    const-string v0, "cancelable"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 2756760
    :cond_0
    iget-object v0, p0, LX/Jzp;->c:LX/0gc;

    const-string v2, "com.facebook.catalyst.react.dialog.DialogModule"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2756761
    :goto_1
    return-void

    .line 2756762
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2756763
    :cond_2
    iput-object v1, p0, LX/Jzp;->d:Ljava/lang/Object;

    goto :goto_1

    .line 2756764
    :cond_3
    new-instance v1, LX/Jzn;

    invoke-direct {v1, v0, p2}, LX/Jzn;-><init>(LX/Jzo;Landroid/os/Bundle;)V

    .line 2756765
    if-eqz p1, :cond_5

    .line 2756766
    const-string v0, "cancelable"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2756767
    const-string v0, "cancelable"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, LX/Jzn;->setCancelable(Z)V

    .line 2756768
    :cond_4
    iget-object v0, p0, LX/Jzp;->b:Landroid/app/FragmentManager;

    const-string v2, "com.facebook.catalyst.react.dialog.DialogModule"

    invoke-virtual {v1, v0, v2}, LX/Jzn;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 2756769
    :cond_5
    iput-object v1, p0, LX/Jzp;->d:Ljava/lang/Object;

    goto :goto_1
.end method
