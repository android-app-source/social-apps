.class public final LX/Jzy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/react/bridge/Callback;

.field public final b:Lcom/facebook/react/bridge/Callback;

.field public final c:Landroid/location/LocationManager;

.field private final d:Ljava/lang/String;

.field private final e:J

.field public final f:Landroid/os/Handler;

.field public final g:Ljava/lang/Runnable;

.field public final h:Landroid/location/LocationListener;

.field public i:Z


# direct methods
.method private constructor <init>(Landroid/location/LocationManager;Ljava/lang/String;JLcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 2757061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2757062
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/Jzy;->f:Landroid/os/Handler;

    .line 2757063
    new-instance v0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;

    invoke-direct {v0, p0}, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;-><init>(LX/Jzy;)V

    iput-object v0, p0, LX/Jzy;->g:Ljava/lang/Runnable;

    .line 2757064
    new-instance v0, LX/Jzx;

    invoke-direct {v0, p0}, LX/Jzx;-><init>(LX/Jzy;)V

    iput-object v0, p0, LX/Jzy;->h:Landroid/location/LocationListener;

    .line 2757065
    iput-object p1, p0, LX/Jzy;->c:Landroid/location/LocationManager;

    .line 2757066
    iput-object p2, p0, LX/Jzy;->d:Ljava/lang/String;

    .line 2757067
    iput-wide p3, p0, LX/Jzy;->e:J

    .line 2757068
    iput-object p5, p0, LX/Jzy;->a:Lcom/facebook/react/bridge/Callback;

    .line 2757069
    iput-object p6, p0, LX/Jzy;->b:Lcom/facebook/react/bridge/Callback;

    .line 2757070
    return-void
.end method

.method public synthetic constructor <init>(Landroid/location/LocationManager;Ljava/lang/String;JLcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;B)V
    .locals 1

    .prologue
    .line 2757071
    invoke-direct/range {p0 .. p6}, LX/Jzy;-><init>(Landroid/location/LocationManager;Ljava/lang/String;JLcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2757072
    iget-object v0, p0, LX/Jzy;->c:Landroid/location/LocationManager;

    iget-object v1, p0, LX/Jzy;->d:Ljava/lang/String;

    iget-object v2, p0, LX/Jzy;->h:Landroid/location/LocationListener;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 2757073
    iget-object v0, p0, LX/Jzy;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/Jzy;->g:Ljava/lang/Runnable;

    iget-wide v2, p0, LX/Jzy;->e:J

    const v4, -0x1f051e4b

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2757074
    return-void
.end method
