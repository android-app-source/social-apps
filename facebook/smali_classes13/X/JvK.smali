.class public final LX/JvK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JvZ;

.field public final synthetic b:LX/JvL;


# direct methods
.method public constructor <init>(LX/JvL;LX/JvZ;)V
    .locals 0

    .prologue
    .line 2750281
    iput-object p1, p0, LX/JvK;->b:LX/JvL;

    iput-object p2, p0, LX/JvK;->a:LX/JvZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x542bf66

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750282
    iget-object v1, p0, LX/JvK;->b:LX/JvL;

    iget-object v1, v1, LX/JvL;->b:LX/JvO;

    iget-object v2, p0, LX/JvK;->a:LX/JvZ;

    .line 2750283
    iget-boolean v4, v2, LX/JvZ;->a:Z

    if-eqz v4, :cond_0

    .line 2750284
    iget-object v4, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v5, LX/0ig;->aq:LX/0ih;

    iget-object v6, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-wide v6, v6, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    const-string v8, "user_voice_selected"

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750285
    iget-object v4, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, -0x1

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2750286
    iget-object v4, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    .line 2750287
    :goto_0
    const v1, -0x54f99d96

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2750288
    :cond_0
    iget-object v4, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v5, LX/0ig;->aq:LX/0ih;

    iget-object v6, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-wide v6, v6, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    const-string v8, "page_voice_selected"

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750289
    new-instance v4, LX/4At;

    iget-object v5, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f083bfb

    invoke-direct {v4, v5, v6}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2750290
    new-instance v5, LX/JvN;

    invoke-direct {v5, v1, v2, v4}, LX/JvN;-><init>(LX/JvO;LX/JvZ;LX/4At;)V

    .line 2750291
    iget-object v4, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v6, v4, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->c:LX/3iH;

    iget-object v7, v2, LX/JvZ;->b:Ljava/lang/String;

    iget-object v4, v1, LX/JvO;->a:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-virtual {v6, v7, v5, v4}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
