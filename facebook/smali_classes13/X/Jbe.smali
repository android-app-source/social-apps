.class public LX/Jbe;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/quicksilver/dataloader/QuicksilverComponentDataProvider;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2716892
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
    .locals 3

    .prologue
    .line 2716893
    const-class v1, LX/Jbe;

    monitor-enter v1

    .line 2716894
    :try_start_0
    sget-object v0, LX/Jbe;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2716895
    sput-object v2, LX/Jbe;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2716896
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2716897
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2716898
    invoke-static {v0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->b(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    move-result-object p0

    check-cast p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    .line 2716899
    move-object p0, p0

    .line 2716900
    move-object v0, p0

    .line 2716901
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2716902
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2716903
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2716904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2716905
    invoke-static {p0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->b(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    .line 2716906
    move-object v0, v0

    .line 2716907
    return-object v0
.end method
