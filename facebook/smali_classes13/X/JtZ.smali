.class public LX/JtZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/JtZ;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2747050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2747051
    iput-object p1, p0, LX/JtZ;->a:Landroid/content/Context;

    .line 2747052
    return-void
.end method

.method public static a(LX/0QB;)LX/JtZ;
    .locals 4

    .prologue
    .line 2747053
    sget-object v0, LX/JtZ;->b:LX/JtZ;

    if-nez v0, :cond_1

    .line 2747054
    const-class v1, LX/JtZ;

    monitor-enter v1

    .line 2747055
    :try_start_0
    sget-object v0, LX/JtZ;->b:LX/JtZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2747056
    if-eqz v2, :cond_0

    .line 2747057
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2747058
    new-instance p0, LX/JtZ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/JtZ;-><init>(Landroid/content/Context;)V

    .line 2747059
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 2747060
    iput-object v3, p0, LX/JtZ;->a:Landroid/content/Context;

    .line 2747061
    move-object v0, p0

    .line 2747062
    sput-object v0, LX/JtZ;->b:LX/JtZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2747063
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2747064
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2747065
    :cond_1
    sget-object v0, LX/JtZ;->b:LX/JtZ;

    return-object v0

    .line 2747066
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2747067
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2wX;
    .locals 2

    .prologue
    .line 2747068
    new-instance v0, LX/2vz;

    iget-object v1, p0, LX/JtZ;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2vz;-><init>(Landroid/content/Context;)V

    sget-object v1, LX/KB1;->l:LX/2vs;

    invoke-virtual {v0, v1}, LX/2vz;->a(LX/2vs;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v0

    return-object v0
.end method
