.class public LX/JYi;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYg;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2706935
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JYi;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706936
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2706937
    iput-object p1, p0, LX/JYi;->b:LX/0Ot;

    .line 2706938
    return-void
.end method

.method public static a(LX/0QB;)LX/JYi;
    .locals 4

    .prologue
    .line 2706939
    const-class v1, LX/JYi;

    monitor-enter v1

    .line 2706940
    :try_start_0
    sget-object v0, LX/JYi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706941
    sput-object v2, LX/JYi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706942
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706943
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706944
    new-instance v3, LX/JYi;

    const/16 p0, 0x2184

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYi;-><init>(LX/0Ot;)V

    .line 2706945
    move-object v0, v3

    .line 2706946
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706947
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706948
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2706950
    check-cast p2, LX/JYh;

    .line 2706951
    iget-object v0, p0, LX/JYi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;

    iget-object v1, p2, LX/JYh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 2706952
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2706953
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2706954
    invoke-static {v2}, LX/JYy;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2706955
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2706956
    :goto_0
    invoke-static {v2}, LX/JYy;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2706957
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2706958
    :goto_1
    invoke-static {v2}, LX/JYy;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    .line 2706959
    invoke-static {p1, v2}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 2706960
    invoke-static {v2}, LX/JYy;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v8

    .line 2706961
    if-eqz v3, :cond_3

    const/4 v2, 0x1

    .line 2706962
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    .line 2706963
    const p2, 0x2d6b9cf7

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2706964
    invoke-interface {p0, p2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p0

    if-nez v2, :cond_4

    :goto_3
    invoke-interface {p0, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->e:LX/JYl;

    const/4 p0, 0x0

    .line 2706965
    new-instance p2, LX/JYk;

    invoke-direct {p2, v4}, LX/JYk;-><init>(LX/JYl;)V

    .line 2706966
    sget-object v0, LX/JYl;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JYj;

    .line 2706967
    if-nez v0, :cond_0

    .line 2706968
    new-instance v0, LX/JYj;

    invoke-direct {v0}, LX/JYj;-><init>()V

    .line 2706969
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/JYj;->a$redex0(LX/JYj;LX/1De;IILX/JYk;)V

    .line 2706970
    move-object p2, v0

    .line 2706971
    move-object p0, p2

    .line 2706972
    move-object v4, p0

    .line 2706973
    iget-object p0, v4, LX/JYj;->a:LX/JYk;

    iput-boolean v2, p0, LX/JYk;->a:Z

    .line 2706974
    iget-object p0, v4, LX/JYj;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2706975
    move-object v2, v4

    .line 2706976
    iget-object v4, v2, LX/JYj;->a:LX/JYk;

    iput-object v5, v4, LX/JYk;->b:Landroid/net/Uri;

    .line 2706977
    iget-object v4, v2, LX/JYj;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2706978
    move-object v2, v2

    .line 2706979
    iget-object v4, v2, LX/JYj;->a:LX/JYk;

    iput-object v8, v4, LX/JYk;->e:Ljava/lang/String;

    .line 2706980
    iget-object v4, v2, LX/JYj;->d:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2706981
    move-object v2, v2

    .line 2706982
    iget-object v4, v2, LX/JYj;->a:LX/JYk;

    iput-object v6, v4, LX/JYk;->c:Ljava/lang/String;

    .line 2706983
    iget-object v4, v2, LX/JYj;->d:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2706984
    move-object v2, v2

    .line 2706985
    iget-object v4, v2, LX/JYj;->a:LX/JYk;

    iput-object v7, v4, LX/JYk;->d:Ljava/lang/CharSequence;

    .line 2706986
    iget-object v4, v2, LX/JYj;->d:Ljava/util/BitSet;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2706987
    move-object v2, v2

    .line 2706988
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2706989
    return-object v0

    :cond_1
    move-object v3, v4

    .line 2706990
    goto/16 :goto_0

    :cond_2
    move-object v5, v4

    .line 2706991
    goto/16 :goto_1

    .line 2706992
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 2706993
    :cond_4
    iget-object v4, v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->c:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    const v4, 0x3fe3d70a    # 1.78f

    invoke-virtual {v3, v4}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    const v4, 0x7f020bc4

    invoke-virtual {v3, v4}, LX/1nw;->l(I)LX/1nw;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const p2, 0x7f0b25fd

    invoke-interface {v3, v4, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    goto/16 :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2706994
    invoke-static {}, LX/1dS;->b()V

    .line 2706995
    iget v0, p1, LX/1dQ;->b:I

    .line 2706996
    packed-switch v0, :pswitch_data_0

    .line 2706997
    :goto_0
    return-object v2

    .line 2706998
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2706999
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2707000
    check-cast v1, LX/JYh;

    .line 2707001
    iget-object v3, p0, LX/JYi;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;

    iget-object v4, v1, LX/JYh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707002
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 2707003
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2707004
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object p2

    .line 2707005
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2707006
    :goto_1
    goto :goto_0

    .line 2707007
    :cond_0
    invoke-static {p1}, LX/JYy;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object p1

    .line 2707008
    iget-object p0, v3, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->b:LX/0Zb;

    invoke-static {p1}, LX/BOe;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2707009
    iget-object p1, v3, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->d:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {}, LX/BOe;->b()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, p0, p2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2d6b9cf7
        :pswitch_0
    .end packed-switch
.end method
