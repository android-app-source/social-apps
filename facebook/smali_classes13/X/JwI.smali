.class public final LX/JwI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/Jwa;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JwS;


# direct methods
.method public constructor <init>(LX/JwS;)V
    .locals 0

    .prologue
    .line 2751769
    iput-object p1, p0, LX/JwI;->a:LX/JwS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 2751770
    check-cast p1, Ljava/util/List;

    .line 2751771
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2751772
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jwa;

    .line 2751773
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, v1, LX/Jwa;->a:Z

    if-nez v0, :cond_1

    .line 2751774
    :cond_0
    new-instance v0, Ljava/lang/IllegalAccessException;

    const-string v1, "Pulsar detection not enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751775
    :cond_1
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
