.class public final LX/K09;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/K0A;


# direct methods
.method public constructor <init>(LX/K0A;I)V
    .locals 0

    .prologue
    .line 2757366
    iput-object p1, p0, LX/K09;->b:LX/K0A;

    iput p2, p0, LX/K09;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2757367
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2757368
    const-string v1, "id"

    iget v2, p0, LX/K09;->a:I

    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2757369
    const-string v1, "code"

    invoke-interface {v0, v1, p1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2757370
    const-string v1, "reason"

    invoke-interface {v0, v1, p2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757371
    iget-object v1, p0, LX/K09;->b:LX/K0A;

    const-string v2, "websocketClosed"

    .line 2757372
    invoke-static {v1, v2, v0}, LX/K0A;->a$redex0(LX/K0A;Ljava/lang/String;LX/5pH;)V

    .line 2757373
    return-void
.end method

.method public final a(LX/656;)V
    .locals 5

    .prologue
    .line 2757374
    :try_start_0
    invoke-virtual {p1}, LX/656;->a()LX/64s;

    move-result-object v0

    sget-object v1, LX/66l;->b:LX/64s;

    if-ne v0, v1, :cond_0

    .line 2757375
    invoke-virtual {p1}, LX/656;->d()LX/671;

    move-result-object v0

    invoke-interface {v0}, LX/671;->r()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 2757376
    :goto_0
    :try_start_1
    invoke-virtual {p1}, LX/656;->d()LX/671;

    move-result-object v0

    invoke-interface {v0}, LX/65D;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2757377
    :goto_1
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v2

    .line 2757378
    const-string v0, "id"

    iget v3, p0, LX/K09;->a:I

    invoke-interface {v2, v0, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2757379
    const-string v0, "data"

    invoke-interface {v2, v0, v1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757380
    const-string v1, "type"

    invoke-virtual {p1}, LX/656;->a()LX/64s;

    move-result-object v0

    sget-object v3, LX/66l;->b:LX/64s;

    if-ne v0, v3, :cond_1

    const-string v0, "binary"

    :goto_2
    invoke-interface {v2, v1, v0}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757381
    iget-object v0, p0, LX/K09;->b:LX/K0A;

    const-string v1, "websocketMessage"

    .line 2757382
    invoke-static {v0, v1, v2}, LX/K0A;->a$redex0(LX/K0A;Ljava/lang/String;LX/5pH;)V

    .line 2757383
    :goto_3
    return-void

    .line 2757384
    :cond_0
    :try_start_2
    invoke-virtual {p1}, LX/656;->d()LX/671;

    move-result-object v0

    invoke-interface {v0}, LX/671;->p()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 2757385
    goto :goto_0

    .line 2757386
    :catch_0
    move-exception v0

    .line 2757387
    iget-object v1, p0, LX/K09;->b:LX/K0A;

    iget v2, p0, LX/K09;->a:I

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 2757388
    invoke-static {v1, v2, v0}, LX/K0A;->a$redex0(LX/K0A;ILjava/lang/String;)V

    .line 2757389
    goto :goto_3

    .line 2757390
    :catch_1
    move-exception v0

    .line 2757391
    const-string v2, "React"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not close BufferedSource for WebSocket id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, LX/K09;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2757392
    :cond_1
    const-string v0, "text"

    goto :goto_2
.end method

.method public final a(LX/66l;)V
    .locals 3

    .prologue
    .line 2757393
    iget-object v0, p0, LX/K09;->b:LX/K0A;

    iget-object v0, v0, LX/K0A;->a:Ljava/util/Map;

    iget v1, p0, LX/K09;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2757394
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2757395
    const-string v1, "id"

    iget v2, p0, LX/K09;->a:I

    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2757396
    iget-object v1, p0, LX/K09;->b:LX/K0A;

    const-string v2, "websocketOpen"

    .line 2757397
    invoke-static {v1, v2, v0}, LX/K0A;->a$redex0(LX/K0A;Ljava/lang/String;LX/5pH;)V

    .line 2757398
    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 2757399
    iget-object v0, p0, LX/K09;->b:LX/K0A;

    iget v1, p0, LX/K09;->a:I

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2757400
    invoke-static {v0, v1, v2}, LX/K0A;->a$redex0(LX/K0A;ILjava/lang/String;)V

    .line 2757401
    return-void
.end method
