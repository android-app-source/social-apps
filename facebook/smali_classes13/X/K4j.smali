.class public final LX/K4j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# instance fields
.field public final synthetic a:LX/K4o;


# direct methods
.method public constructor <init>(LX/K4o;)V
    .locals 0

    .prologue
    .line 2769256
    iput-object p1, p0, LX/K4j;->a:LX/K4o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 0

    .prologue
    .line 2769257
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 2769258
    return-void
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 2769259
    const/4 v0, 0x0

    return v0
.end method

.method public final onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 2769260
    const/4 v0, 0x0

    return v0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 2769261
    iget-object v0, p0, LX/K4j;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->UNINITIALIZED:LX/K4l;

    if-eq v0, v1, :cond_0

    .line 2769262
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    .line 2769263
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, LX/K4j;->a:LX/K4o;

    iget v0, v0, LX/K4o;->D:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 2769264
    iget-object v0, p0, LX/K4j;->a:LX/K4o;

    .line 2769265
    iget v1, v0, LX/K4o;->D:I

    add-int/lit8 p1, v1, 0x1

    iput p1, v0, LX/K4o;->D:I

    .line 2769266
    iget-object v0, p0, LX/K4j;->a:LX/K4o;

    iget-object v1, p0, LX/K4j;->a:LX/K4o;

    iget-object v1, v1, LX/K4o;->o:Ljava/lang/String;

    invoke-static {v0, v1}, LX/K4o;->b(LX/K4o;Ljava/lang/String;)V

    .line 2769267
    :cond_0
    :goto_0
    return-void

    .line 2769268
    :cond_1
    iget-object v0, p0, LX/K4j;->a:LX/K4o;

    const/4 v1, 0x1

    .line 2769269
    iput-boolean v1, v0, LX/K4o;->t:Z

    .line 2769270
    iget-object v0, p0, LX/K4j;->a:LX/K4o;

    invoke-static {v0}, LX/K4o;->t(LX/K4o;)V

    goto :goto_0
.end method

.method public final onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 2769271
    return-void
.end method
