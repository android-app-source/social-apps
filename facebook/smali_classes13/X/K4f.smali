.class public final synthetic LX/K4f;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2769079
    invoke-static {}, LX/K4d;->values()[LX/K4d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/K4f;->a:[I

    :try_start_0
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->CREATE_EGL_CONTEXT:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->UPDATE_TEXTURES:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->INIT_DATA:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->DRAW_FRAME:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->DESTROY_EGL_CONTEXT:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->EXPORT_VIDEO:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, LX/K4f;->a:[I

    sget-object v1, LX/K4d;->QUIT_THREAD:LX/K4d;

    invoke-virtual {v1}, LX/K4d;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    goto :goto_6

    :catch_1
    goto :goto_5

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_3

    :catch_4
    goto :goto_2

    :catch_5
    goto :goto_1

    :catch_6
    goto :goto_0
.end method
