.class public final LX/Jvq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750840
    iput-object p1, p0, LX/Jvq;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3c2e0a68

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750841
    iget-object v1, p0, LX/Jvq;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->b()Z

    .line 2750842
    iget-object v1, p0, LX/Jvq;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->H:Landroid/net/Uri;

    invoke-static {v1}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(Landroid/net/Uri;)V

    .line 2750843
    iget-object v1, p0, LX/Jvq;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    invoke-static {v1}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(Landroid/net/Uri;)V

    .line 2750844
    iget-object v1, p0, LX/Jvq;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v2, p0, LX/Jvq;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    .line 2750845
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/9c4;->CREATIVECAM_RETAKE_PHOTO:LX/9c4;

    invoke-virtual {p1}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object p1, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {p1}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "composer"

    .line 2750846
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2750847
    move-object p0, p0

    .line 2750848
    invoke-static {v1, p0}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2750849
    const v1, -0x282bb872

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
