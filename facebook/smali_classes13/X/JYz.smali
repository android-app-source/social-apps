.class public final LX/JYz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1PT;

.field public final synthetic b:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;LX/1PT;)V
    .locals 0

    .prologue
    .line 2707673
    iput-object p1, p0, LX/JYz;->b:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    iput-object p2, p0, LX/JYz;->a:LX/1PT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2d103750

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2707674
    iget-object v1, p0, LX/JYz;->b:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    iget-object v2, p0, LX/JYz;->a:LX/1PT;

    .line 2707675
    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v3}, LX/1Qt;->name()Ljava/lang/String;

    move-result-object v3

    .line 2707676
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "ssfy_see_more_click"

    invoke-direct {v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "native_newsfeed"

    .line 2707677
    iput-object v2, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2707678
    move-object v5, v5

    .line 2707679
    if-eqz v3, :cond_0

    .line 2707680
    const-string v2, "feed_name"

    invoke-virtual {v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2707681
    :cond_0
    move-object v3, v5

    .line 2707682
    iget-object v5, v1, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->f:LX/0Zb;

    invoke-interface {v5, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2707683
    sget-object v1, LX/0ax;->gh:Ljava/lang/String;

    const-string v2, "netegossfy"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2707684
    iget-object v2, p0, LX/JYz;->b:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->h:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2707685
    const v1, -0x4f7f3aff

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
