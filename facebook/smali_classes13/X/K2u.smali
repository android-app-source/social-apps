.class public final LX/K2u;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/slideshow/SlideshowEditActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/SlideshowEditActivity;)V
    .locals 0

    .prologue
    .line 2764694
    iput-object p1, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 2764695
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2764696
    const-string v1, "extra_media_items"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v3, v3, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v3}, LX/K2z;->a()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2764697
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;

    move-result-object v1

    .line 2764698
    iget-object v2, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v2, v2, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v2}, LX/K2z;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2764699
    iget-object v2, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v2, v2, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v2}, LX/K2z;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->setMoodId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;

    .line 2764700
    :cond_0
    const-string v2, "extra_slideshow_data"

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2764701
    iget-object v1, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v1, v1, Lcom/facebook/slideshow/SlideshowEditActivity;->s:LX/3l1;

    iget-object v2, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v2, v2, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v2}, LX/K2z;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, LX/3l1;->a(I)V

    .line 2764702
    iget-object v1, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/slideshow/SlideshowEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 2764703
    iget-object v0, p0, LX/K2u;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    invoke-virtual {v0}, Lcom/facebook/slideshow/SlideshowEditActivity;->finish()V

    .line 2764704
    return-void
.end method
