.class public final LX/JpO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Do1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Do1",
        "<",
        "LX/JpS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6ek;

.field public final synthetic b:LX/JpT;


# direct methods
.method public constructor <init>(LX/JpT;LX/6ek;)V
    .locals 0

    .prologue
    .line 2737334
    iput-object p1, p0, LX/JpO;->b:LX/JpT;

    iput-object p2, p0, LX/JpO;->a:LX/6ek;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;LX/0Rf;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/JpS;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 2737335
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2737336
    new-instance v0, LX/Dnz;

    invoke-direct {v0, p2}, LX/Dnz;-><init>(Ljava/util/Collection;)V

    throw v0

    .line 2737337
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2737338
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2737339
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2737340
    const-wide/high16 v6, -0x8000000000000000L

    .line 2737341
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 2737342
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    .line 2737343
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737344
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737345
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    invoke-static {v5, v3}, LX/JpT;->b(Ljava/util/HashMap;LX/0Px;)V

    .line 2737346
    iget-wide v8, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_0

    .line 2737347
    :cond_1
    sget-object v0, LX/JpS;->FACEBOOK:LX/JpS;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2737348
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2737349
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2737350
    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737351
    :cond_2
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    invoke-static {v2}, Lcom/facebook/fbservice/results/DataFetchDisposition;->buildCombinedDisposition(Ljava/util/List;)Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v2

    iget-object v3, p0, LX/JpO;->a:LX/6ek;

    invoke-static {v4}, LX/DoE;->a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
