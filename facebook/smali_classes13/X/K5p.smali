.class public final LX/K5p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardCover;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardCover;)V
    .locals 0

    .prologue
    .line 2770668
    iput-object p1, p0, LX/K5p;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardCover;B)V
    .locals 0

    .prologue
    .line 2770675
    invoke-direct {p0, p1}, LX/K5p;-><init>(Lcom/facebook/tarot/cards/TarotCardCover;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2770673
    iget-object v0, p0, LX/K5p;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->f:Lcom/facebook/tarot/cards/elements/CoverTextView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->a()V

    .line 2770674
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2770676
    iget-object v0, p0, LX/K5p;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->f:Lcom/facebook/tarot/cards/elements/CoverTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setDescriptionText(Ljava/lang/String;)V

    .line 2770677
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 2770671
    iget-object v0, p0, LX/K5p;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->f:Lcom/facebook/tarot/cards/elements/CoverTextView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/tarot/cards/elements/CoverTextView;->a(Ljava/lang/String;II)V

    .line 2770672
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2770669
    iget-object v0, p0, LX/K5p;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->f:Lcom/facebook/tarot/cards/elements/CoverTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setCreationTimeText(Ljava/lang/String;)V

    .line 2770670
    return-void
.end method
