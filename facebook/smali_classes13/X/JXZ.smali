.class public final LX/JXZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V
    .locals 0

    .prologue
    .line 2704767
    iput-object p1, p0, LX/JXZ;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 2704768
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 2704769
    iget-object v1, p0, LX/JXZ;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v1, v1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2704770
    iget-object v1, p0, LX/JXZ;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v1, v1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->h:Landroid/widget/TextView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v2, v0

    iget-object v2, p0, LX/JXZ;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v2, v2, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 2704771
    return-void
.end method
