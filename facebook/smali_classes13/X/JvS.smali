.class public LX/JvS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2750407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750408
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2750409
    const-string v0, "target_type_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Target type is not provided for voice switcher!"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2750410
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2750411
    new-instance v1, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    invoke-direct {v1}, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;-><init>()V

    .line 2750412
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2750413
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2750414
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2750415
    move-object v0, v1

    .line 2750416
    return-object v0
.end method
