.class public final LX/Jvj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750697
    iput-object p1, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2750698
    iget-object v0, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v0}, LX/89S;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2750699
    :cond_0
    :goto_0
    return-void

    .line 2750700
    :cond_1
    iget-object v0, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2750701
    iget-object v0, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v2}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->z(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2750702
    iget-object v0, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2750703
    iget-object v0, p0, LX/Jvj;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 2750704
    iget-object v1, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance p0, LX/BFy;

    invoke-direct {p0, v0}, LX/BFy;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    invoke-virtual {v1, p0}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;)V

    .line 2750705
    return-void
.end method
