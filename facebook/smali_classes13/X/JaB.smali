.class public LX/JaB;
.super LX/76U;
.source ""


# instance fields
.field public final b:LX/2Ip;

.field private final c:LX/17W;

.field public final d:LX/2do;

.field private final e:LX/3UJ;

.field public final f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/JaG;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/Ja9;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/17W;LX/2do;LX/3UJ;LX/JaG;LX/Ja9;LX/78A;Ljava/lang/Runnable;)V
    .locals 1
    .param p4    # LX/JaG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Ja9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/78A;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709581
    invoke-direct {p0, p7, p6}, LX/76U;-><init>(Ljava/lang/Runnable;LX/78A;)V

    .line 2709582
    new-instance v0, LX/JaA;

    invoke-direct {v0, p0}, LX/JaA;-><init>(LX/JaB;)V

    iput-object v0, p0, LX/JaB;->b:LX/2Ip;

    .line 2709583
    iput-object p1, p0, LX/JaB;->c:LX/17W;

    .line 2709584
    iput-object p2, p0, LX/JaB;->d:LX/2do;

    .line 2709585
    iput-object p3, p0, LX/JaB;->e:LX/3UJ;

    .line 2709586
    iput-object p5, p0, LX/JaB;->g:LX/Ja9;

    .line 2709587
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    .line 2709588
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 8

    .prologue
    .line 2709589
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->c()V

    .line 2709590
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709591
    invoke-virtual {p0}, LX/76U;->f()V

    .line 2709592
    :cond_0
    iget-object v0, p0, LX/JaB;->g:LX/Ja9;

    .line 2709593
    iget-object v1, v0, LX/Ja9;->a:Ljava/lang/String;

    move-object v7, v1

    .line 2709594
    iget-object v0, p0, LX/JaB;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2709595
    iget-object v1, p0, LX/JaB;->c:LX/17W;

    iget-object v0, p0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaG;

    invoke-virtual {v0}, LX/JaG;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2709596
    :goto_0
    return-void

    .line 2709597
    :cond_1
    iget-object v1, p0, LX/JaB;->e:LX/3UJ;

    iget-object v0, p0, LX/JaB;->h:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2hA;->INCOMING_FR_QP:LX/2hA;

    sget-object v5, LX/2na;->CONFIRM:LX/2na;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, LX/3UJ;->a(JLX/2hA;LX/2na;LX/84H;)V

    .line 2709598
    iget-object v0, p0, LX/JaB;->g:LX/Ja9;

    .line 2709599
    iget-object v1, v0, LX/Ja9;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2709600
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/JaB;->g:LX/Ja9;

    .line 2709601
    iget-object v1, v0, LX/Ja9;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2709602
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2709603
    :cond_2
    iget-object v1, p0, LX/JaB;->c:LX/17W;

    iget-object v0, p0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaG;

    invoke-virtual {v0}, LX/JaG;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 2709604
    :cond_3
    iget-object v1, p0, LX/JaB;->c:LX/17W;

    iget-object v0, p0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaG;

    invoke-virtual {v0}, LX/JaG;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, LX/0ax;->cX:Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, LX/5Oz;->INCOMING_FR_QP:LX/5Oz;

    invoke-virtual {v5}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/JaB;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, LX/JaB;->g:LX/Ja9;

    .line 2709605
    iget-object v6, v5, LX/Ja9;->c:Ljava/lang/String;

    move-object v5, v6

    .line 2709606
    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, LX/JaB;->g:LX/Ja9;

    .line 2709607
    iget-object v6, v5, LX/Ja9;->b:Ljava/lang/String;

    move-object v5, v6

    .line 2709608
    aput-object v5, v3, v4

    const/4 v4, 0x4

    sget-object v5, LX/5Oy;->REQUEST_ACCEPTED_NOTICE:LX/5Oy;

    invoke-virtual {v5}, LX/5Oy;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2709609
    iget-object v0, p0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2709610
    iget-object v0, p0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaG;

    invoke-virtual {v0}, LX/JaG;->a()V

    .line 2709611
    :cond_0
    iget-object v0, p0, LX/JaB;->d:LX/2do;

    iget-object v1, p0, LX/JaB;->b:LX/2Ip;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2709612
    return-void
.end method
