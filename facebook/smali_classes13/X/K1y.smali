.class public final LX/K1y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/K20;


# direct methods
.method public constructor <init>(LX/K20;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2763015
    iput-object p1, p0, LX/K1y;->d:LX/K20;

    iput-object p2, p0, LX/K1y;->a:Landroid/content/Context;

    iput-object p3, p0, LX/K1y;->b:Ljava/lang/String;

    iput-object p4, p0, LX/K1y;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    .line 2763016
    iget-object v0, p0, LX/K1y;->d:LX/K20;

    iget-object v2, p0, LX/K1y;->b:Ljava/lang/String;

    iget-object v3, p0, LX/K1y;->c:Ljava/lang/String;

    .line 2763017
    new-instance v4, LX/K1z;

    invoke-direct {v4, v0}, LX/K1z;-><init>(LX/K20;)V

    .line 2763018
    iget-object v7, v0, LX/K20;->f:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2sP;

    invoke-virtual {v7}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    .line 2763019
    invoke-static {v0}, LX/K20;->a(LX/K20;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-nez v8, :cond_1

    .line 2763020
    :cond_0
    :goto_0
    iget-object v4, v0, LX/K20;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0kL;

    new-instance v5, LX/27k;

    const v6, 0x7f081412

    invoke-direct {v5, v6}, LX/27k;-><init>(I)V

    invoke-virtual {v4, v5}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2763021
    const/4 v0, 0x1

    return v0

    .line 2763022
    :cond_1
    iget-object v7, v0, LX/K20;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0WJ;

    invoke-virtual {v7}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v7

    .line 2763023
    iget-object v9, v7, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v7, v9

    .line 2763024
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    .line 2763025
    new-instance v7, LX/5M9;

    invoke-direct {v7}, LX/5M9;-><init>()V

    .line 2763026
    iput-wide v9, v7, LX/5M9;->b:J

    .line 2763027
    move-object v7, v7

    .line 2763028
    iput-wide v9, v7, LX/5M9;->k:J

    .line 2763029
    move-object v7, v7

    .line 2763030
    iput-object v3, v7, LX/5M9;->j:Ljava/lang/String;

    .line 2763031
    move-object v7, v7

    .line 2763032
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v8

    .line 2763033
    iput-object v8, v7, LX/5M9;->h:Ljava/lang/String;

    .line 2763034
    move-object v7, v7

    .line 2763035
    iput-object v2, v7, LX/5M9;->G:Ljava/lang/String;

    .line 2763036
    move-object v7, v7

    .line 2763037
    sget-object v8, LX/2rt;->SHARE:LX/2rt;

    .line 2763038
    iput-object v8, v7, LX/5M9;->q:LX/2rt;

    .line 2763039
    move-object v8, v7

    .line 2763040
    iget-object v7, v0, LX/K20;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Chi;

    invoke-virtual {v7}, LX/Chi;->c()Ljava/lang/String;

    move-result-object v7

    .line 2763041
    iput-object v7, v8, LX/5M9;->m:Ljava/lang/String;

    .line 2763042
    move-object v7, v8

    .line 2763043
    invoke-virtual {v7}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v7

    .line 2763044
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 2763045
    const-string v9, "publishPostParams"

    invoke-virtual {v8, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2763046
    iget-object v7, v0, LX/K20;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    new-instance v9, LX/K1x;

    invoke-direct {v9, v0, v4}, LX/K1x;-><init>(LX/K20;LX/K1z;)V

    invoke-virtual {v7, v8, v9}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
