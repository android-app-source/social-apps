.class public LX/K1t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1nL;


# direct methods
.method public constructor <init>(LX/1nL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2762811
    iput-object p1, p0, LX/K1t;->a:LX/1nL;

    .line 2762812
    return-void
.end method

.method public static a(LX/0QB;)LX/K1t;
    .locals 4

    .prologue
    .line 2762813
    const-class v1, LX/K1t;

    monitor-enter v1

    .line 2762814
    :try_start_0
    sget-object v0, LX/K1t;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2762815
    sput-object v2, LX/K1t;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2762816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2762817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2762818
    new-instance p0, LX/K1t;

    invoke-static {v0}, LX/1nL;->b(LX/0QB;)LX/1nL;

    move-result-object v3

    check-cast v3, LX/1nL;

    invoke-direct {p0, v3}, LX/K1t;-><init>(LX/1nL;)V

    .line 2762819
    move-object v0, p0

    .line 2762820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2762821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K1t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2762822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2762823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;LX/Cly;LX/CmD;LX/CmQ;Z)Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;
    .locals 3

    .prologue
    .line 2762824
    new-instance v0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;

    invoke-direct {v0}, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;-><init>()V

    .line 2762825
    invoke-virtual {p2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->v()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2762826
    move-object v2, v0

    .line 2762827
    const-class v0, LX/0ew;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 2762828
    const-class v1, Landroid/app/Activity;

    invoke-static {p1, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 2762829
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2762830
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2762831
    if-nez p4, :cond_3

    .line 2762832
    const/4 v0, 0x0

    .line 2762833
    :goto_0
    move-object v0, v0

    .line 2762834
    iput-object v0, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->B:LX/Clf;

    .line 2762835
    iget-object v1, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    if-eqz v1, :cond_0

    .line 2762836
    iget-object v1, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    .line 2762837
    iput-object v0, v1, LX/K1s;->a:LX/Clf;

    .line 2762838
    :cond_0
    if-nez p5, :cond_4

    .line 2762839
    const/4 v0, 0x0

    .line 2762840
    :goto_1
    move-object v0, v0

    .line 2762841
    iput-object v0, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->C:LX/Clf;

    .line 2762842
    iget-object v1, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    if-eqz v1, :cond_1

    .line 2762843
    iget-object v1, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    .line 2762844
    iput-object v0, v1, LX/K1s;->b:LX/Clf;

    .line 2762845
    :cond_1
    iput-object p6, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->E:LX/CmQ;

    .line 2762846
    iget-object v0, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    if-eqz v0, :cond_2

    .line 2762847
    iget-object v0, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    .line 2762848
    iput-object p6, v0, LX/K1s;->c:LX/CmQ;

    .line 2762849
    :cond_2
    iput-boolean p7, v2, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->G:Z

    .line 2762850
    iget-object v0, p0, LX/K1t;->a:LX/1nL;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, p1, p3, v1}, LX/1nL;->a(LX/8qC;Landroid/content/Context;Lcom/facebook/ufiservices/flyout/PopoverParams;Z)V

    .line 2762851
    return-object v2

    :cond_3
    new-instance v0, LX/Cle;

    invoke-direct {v0, p1}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-interface {p4}, LX/Cly;->e()LX/8Z4;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object v0

    invoke-interface {p4}, LX/Cly;->f()LX/Clb;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cle;->a(LX/Clb;)LX/Cle;

    move-result-object v0

    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, LX/Cle;

    invoke-direct {v0, p1}, LX/Cle;-><init>(Landroid/content/Context;)V

    .line 2762852
    iget-object v1, p5, LX/CmD;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;

    move-object v1, v1

    .line 2762853
    invoke-virtual {v0, v1}, LX/Cle;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;)LX/Cle;

    move-result-object v0

    sget-object v1, LX/Clb;->BYLINE:LX/Clb;

    invoke-virtual {v0, v1}, LX/Cle;->a(LX/Clb;)LX/Cle;

    move-result-object v0

    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    goto :goto_1
.end method
