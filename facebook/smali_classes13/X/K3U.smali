.class public final LX/K3U;
.super LX/62U;
.source ""


# instance fields
.field public m:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

.field public final synthetic n:Lcom/facebook/slideshow/ui/SoundListAdapter;

.field public final o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

.field public final p:Lcom/facebook/widget/text/BetterTextView;

.field public final q:[I

.field public final r:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/ui/SoundListAdapter;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2765787
    iput-object p1, p0, LX/K3U;->n:Lcom/facebook/slideshow/ui/SoundListAdapter;

    .line 2765788
    invoke-direct {p0, p2}, LX/62U;-><init>(Landroid/view/View;)V

    .line 2765789
    const v0, 0x7f0d1b29

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iput-object v0, p0, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    .line 2765790
    const v0, 0x7f0d037b

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 2765791
    iget-object v0, p0, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f10004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, LX/K3U;->q:[I

    .line 2765792
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "res"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const v1, 0x7f020938

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/K3U;->r:Landroid/net/Uri;

    .line 2765793
    iget-object v0, p0, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2765794
    sget-object v1, LX/1Up;->e:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 2765795
    new-instance v1, Landroid/graphics/LightingColorFilter;

    const/high16 v2, -0x1000000

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/ColorFilter;)V

    .line 2765796
    new-instance v0, LX/K3T;

    invoke-direct {v0, p0, p1}, LX/K3T;-><init>(LX/K3U;Lcom/facebook/slideshow/ui/SoundListAdapter;)V

    .line 2765797
    iget-object v1, p0, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v1, v0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2765798
    iget-object v1, p0, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2765799
    iget-object v0, p0, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    const/4 v1, 0x1

    .line 2765800
    iput-boolean v1, v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->g:Z

    .line 2765801
    return-void
.end method
