.class public LX/Jqt;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public a:LX/JrY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740702
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqt;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2740700
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2740701
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqt;
    .locals 7

    .prologue
    .line 2740669
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740670
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740671
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740672
    if-nez v1, :cond_0

    .line 2740673
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740674
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740675
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740676
    sget-object v1, LX/Jqt;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740677
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740678
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740679
    :cond_1
    if-nez v1, :cond_4

    .line 2740680
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740681
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740682
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2740683
    new-instance p0, LX/Jqt;

    invoke-direct {p0}, LX/Jqt;-><init>()V

    .line 2740684
    invoke-static {v0}, LX/JrY;->a(LX/0QB;)LX/JrY;

    move-result-object v1

    check-cast v1, LX/JrY;

    .line 2740685
    iput-object v1, p0, LX/Jqt;->a:LX/JrY;

    .line 2740686
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2740687
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740688
    if-nez v1, :cond_2

    .line 2740689
    sget-object v0, LX/Jqt;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqt;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740690
    :goto_1
    if-eqz v0, :cond_3

    .line 2740691
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740692
    :goto_3
    check-cast v0, LX/Jqt;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740693
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740694
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740695
    :catchall_1
    move-exception v0

    .line 2740696
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740697
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740698
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740699
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqt;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqt;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static c(LX/6kW;)LX/6ku;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2740594
    invoke-virtual {p0}, LX/6kW;->I()LX/6jl;

    move-result-object v0

    iget-object v0, v0, LX/6jl;->payload:[B

    .line 2740595
    :try_start_0
    new-instance v1, LX/1sp;

    invoke-direct {v1}, LX/1sp;-><init>()V

    .line 2740596
    new-instance v2, LX/1sr;

    new-instance p0, Ljava/io/ByteArrayInputStream;

    invoke-direct {p0, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, p0}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v1, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v2

    move-object v0, v2

    .line 2740597
    const/4 v2, 0x0

    .line 2740598
    const/4 v1, 0x0

    .line 2740599
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 2740600
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v3

    .line 2740601
    iget-byte v4, v3, LX/1sw;->b:B

    if-eqz v4, :cond_5

    .line 2740602
    iget-short v4, v3, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_0

    .line 2740603
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {v0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2740604
    :pswitch_0
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0xf

    if-ne v4, v5, :cond_4

    .line 2740605
    invoke-virtual {v0}, LX/1su;->h()LX/1u3;

    move-result-object v4

    .line 2740606
    new-instance v3, Ljava/util/ArrayList;

    iget v1, v4, LX/1u3;->b:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    .line 2740607
    :goto_1
    iget v5, v4, LX/1u3;->b:I

    if-gez v5, :cond_2

    invoke-static {}, LX/1su;->t()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2740608
    :cond_0
    new-instance v5, LX/6kU;

    invoke-direct {v5}, LX/6kU;-><init>()V

    .line 2740609
    new-instance v5, LX/6kU;

    invoke-direct {v5}, LX/6kU;-><init>()V

    .line 2740610
    const/4 v6, 0x0

    iput v6, v5, LX/6kU;->setField_:I

    .line 2740611
    const/4 v6, 0x0

    iput-object v6, v5, LX/6kU;->value_:Ljava/lang/Object;

    .line 2740612
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 2740613
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v6

    .line 2740614
    invoke-virtual {v5, v0, v6}, LX/6kU;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object p0

    iput-object p0, v5, LX/6kU;->value_:Ljava/lang/Object;

    .line 2740615
    iget-object p0, v5, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz p0, :cond_1

    .line 2740616
    iget-short v6, v6, LX/1sw;->c:S

    iput v6, v5, LX/6kU;->setField_:I

    .line 2740617
    :cond_1
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    .line 2740618
    invoke-virtual {v0}, LX/1su;->e()V

    .line 2740619
    move-object v5, v5

    .line 2740620
    move-object v5, v5

    .line 2740621
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2740622
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2740623
    :cond_2
    iget v5, v4, LX/1u3;->b:I

    if-lt v1, v5, :cond_0

    :cond_3
    move-object v1, v3

    .line 2740624
    goto :goto_0

    .line 2740625
    :cond_4
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {v0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2740626
    :cond_5
    invoke-virtual {v0}, LX/1su;->e()V

    .line 2740627
    new-instance v2, LX/6ku;

    invoke-direct {v2, v1}, LX/6ku;-><init>(Ljava/util/List;)V

    .line 2740628
    invoke-static {v2}, LX/6ku;->a(LX/6ku;)V

    .line 2740629
    move-object v0, v2
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 2740630
    return-object v0

    .line 2740631
    :catch_0
    move-exception v0

    .line 2740632
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to deserialize delta client payload."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 4

    .prologue
    .line 2740661
    check-cast p1, LX/6kW;

    .line 2740662
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 2740663
    invoke-static {p1}, LX/Jqt;->c(LX/6kW;)LX/6ku;

    move-result-object v0

    .line 2740664
    if-eqz v0, :cond_0

    .line 2740665
    iget-object v0, v0, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kU;

    .line 2740666
    iget-object v3, p0, LX/Jqt;->a:LX/JrY;

    invoke-virtual {v3, v0}, LX/JrY;->a(LX/6kU;)LX/Jqi;

    move-result-object v3

    .line 2740667
    invoke-virtual {v3, v0}, LX/Jqi;->a(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    goto :goto_0

    .line 2740668
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740650
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2740651
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-static {v0}, LX/Jqt;->c(LX/6kW;)LX/6ku;

    move-result-object v3

    .line 2740652
    if-eqz v3, :cond_0

    .line 2740653
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v3, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2740654
    iget-object v0, v3, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kU;

    .line 2740655
    iget-object v4, p0, LX/Jqt;->a:LX/JrY;

    invoke-virtual {v4, v0}, LX/JrY;->a(LX/6kU;)LX/Jqi;

    move-result-object v4

    .line 2740656
    new-instance v5, LX/7GJ;

    iget-wide v6, p2, LX/7GJ;->b:J

    iget-object v8, p2, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-direct {v5, v0, v6, v7, v8}, LX/7GJ;-><init>(Ljava/lang/Object;JLcom/facebook/fbtrace/FbTraceNode;)V

    .line 2740657
    invoke-virtual {v4, p1, v5}, LX/Jqi;->a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;

    move-result-object v0

    .line 2740658
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "client_only_delta_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2740659
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2740660
    :cond_0
    return-object v2
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740641
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-static {v0}, LX/Jqt;->c(LX/6kW;)LX/6ku;

    move-result-object v2

    .line 2740642
    if-eqz v2, :cond_0

    .line 2740643
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v2, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2740644
    iget-object v0, v2, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kU;

    .line 2740645
    iget-object v3, p0, LX/Jqt;->a:LX/JrY;

    invoke-virtual {v3, v0}, LX/JrY;->a(LX/6kU;)LX/Jqi;

    move-result-object v3

    .line 2740646
    new-instance v4, LX/7GJ;

    iget-wide v6, p2, LX/7GJ;->b:J

    iget-object v5, p2, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-direct {v4, v0, v6, v7, v5}, LX/7GJ;-><init>(Ljava/lang/Object;JLcom/facebook/fbtrace/FbTraceNode;)V

    .line 2740647
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "client_only_delta_"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v3, v0, v4}, LX/7GG;->a(Landroid/os/Bundle;LX/7GJ;)V

    .line 2740648
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2740649
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 4

    .prologue
    .line 2740633
    check-cast p1, LX/6kW;

    .line 2740634
    invoke-static {p1}, LX/Jqt;->c(LX/6kW;)LX/6ku;

    move-result-object v0

    .line 2740635
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 2740636
    if-eqz v0, :cond_0

    .line 2740637
    iget-object v0, v0, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kU;

    .line 2740638
    iget-object v3, p0, LX/Jqt;->a:LX/JrY;

    invoke-virtual {v3, v0}, LX/JrY;->a(LX/6kU;)LX/Jqi;

    move-result-object v3

    .line 2740639
    invoke-virtual {v3, v0}, LX/Jqi;->b(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    goto :goto_0

    .line 2740640
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method
