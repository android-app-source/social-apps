.class public LX/Ju7;
.super LX/I4D;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2748547
    invoke-direct {p0, p1}, LX/I4D;-><init>(Landroid/content/Context;)V

    .line 2748548
    return-void
.end method


# virtual methods
.method public final a(LX/Jtu;)LX/Clo;
    .locals 8

    .prologue
    .line 2748549
    invoke-virtual {p1}, LX/Jtu;->e()LX/B5Y;

    move-result-object v0

    check-cast v0, LX/Jtt;

    .line 2748550
    new-instance v1, LX/Ju6;

    iget-object v2, p0, LX/I4D;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Ju6;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, LX/Jtu;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Ju6;->f(Ljava/lang/String;)LX/Ju6;

    move-result-object v1

    invoke-virtual {v0}, LX/Jtt;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Ju6;->g(Ljava/lang/String;)LX/Ju6;

    move-result-object v1

    invoke-virtual {v0}, LX/Jtt;->c()I

    move-result v2

    invoke-virtual {v1, v2}, LX/Ju6;->b(I)LX/Ju6;

    move-result-object v1

    invoke-virtual {v0}, LX/Jtt;->q()LX/8Z4;

    move-result-object v2

    invoke-interface {v2}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Ju6;->h(Ljava/lang/String;)LX/Ju6;

    move-result-object v1

    .line 2748551
    invoke-virtual {v0}, LX/Jtt;->g()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2748552
    iget-object v4, v0, LX/Jtt;->c:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-object v4, v4

    .line 2748553
    if-eqz v4, :cond_0

    .line 2748554
    iget-object v5, v1, LX/I4B;->s:Ljava/lang/String;

    iget-object v6, v1, LX/Ju6;->K:LX/0Ot;

    .line 2748555
    new-instance v7, LX/CmG;

    invoke-virtual {v4}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->a()LX/8Yr;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->NON_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-direct {v7, p0, p1, v6}, LX/CmG;-><init>(LX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/0Ot;)V

    .line 2748556
    iput-object v5, v7, LX/CmG;->g:Ljava/lang/String;

    .line 2748557
    move-object v7, v7

    .line 2748558
    const/4 p0, 0x1

    .line 2748559
    iput-boolean p0, v7, LX/CmG;->e:Z

    .line 2748560
    move-object v7, v7

    .line 2748561
    invoke-virtual {v7}, LX/CmG;->c()LX/Clw;

    move-result-object v7

    move-object v5, v7

    .line 2748562
    iput-object v5, v1, LX/Ju6;->y:LX/Clr;

    .line 2748563
    :cond_0
    move-object v4, v1

    .line 2748564
    iget-object v5, v0, LX/Jtt;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-object v5, v5

    .line 2748565
    iput-object v5, v4, LX/Ju6;->M:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    .line 2748566
    move-object v4, v4

    .line 2748567
    invoke-virtual {v0}, LX/Jtt;->q()LX/8Z4;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/Ju6;->f(LX/8Z4;)LX/Ju6;

    move-result-object v4

    invoke-virtual {v0}, LX/Jtt;->s()J

    move-result-wide v6

    .line 2748568
    new-instance v5, LX/Ju8;

    const/4 p0, 0x0

    invoke-virtual {v3, v2, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v5, v6, v7, p0}, LX/Ju8;-><init>(JLjava/lang/String;)V

    iput-object v5, v4, LX/Ju6;->N:LX/Ju8;

    .line 2748569
    invoke-virtual {v0}, LX/Jtt;->l()LX/B5X;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/I4B;->a(LX/B5X;)LX/I4B;

    .line 2748570
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v2, v2

    .line 2748571
    iget-object v3, v0, LX/Jtt;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v3

    .line 2748572
    iget-object v3, v1, LX/Ju6;->H:LX/Crz;

    invoke-static {v2, v0, v3}, LX/ClW;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/Crz;)LX/ClW;

    move-result-object v3

    .line 2748573
    if-eqz v3, :cond_1

    .line 2748574
    new-instance v4, LX/JuC;

    invoke-direct {v4, v3}, LX/JuC;-><init>(LX/ClW;)V

    iput-object v4, v1, LX/Ju6;->L:LX/Clr;

    .line 2748575
    :cond_1
    invoke-virtual {v1}, LX/I4B;->b()LX/Clo;

    move-result-object v0

    return-object v0
.end method
