.class public LX/K3f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:[I


# instance fields
.field public final c:Landroid/media/MediaCodec$BufferInfo;

.field private final d:LX/0TD;

.field public e:Landroid/media/MediaExtractor;

.field public f:Landroid/media/MediaCodec;

.field public g:Landroid/media/MediaMuxer;

.field public h:LX/K3g;

.field private i:LX/K3c;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2766066
    const-class v0, LX/K3f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K3f;->a:Ljava/lang/String;

    .line 2766067
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3038

    aput v2, v0, v1

    sput-object v0, LX/K3f;->b:[I

    return-void
.end method

.method public constructor <init>(LX/0TD;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/StorylineMuxerExecutor;
        .end annotation
    .end param

    .prologue
    .line 2766068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2766069
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, LX/K3f;->c:Landroid/media/MediaCodec$BufferInfo;

    .line 2766070
    iput-object p1, p0, LX/K3f;->d:LX/0TD;

    .line 2766071
    return-void
.end method

.method private static a(LX/K3g;)Landroid/media/MediaFormat;
    .locals 6

    .prologue
    .line 2766075
    const-string v0, "video/avc"

    iget v1, p0, LX/K3g;->d:I

    iget v2, p0, LX/K3g;->e:I

    invoke-static {v0, v1, v2}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 2766076
    const-string v1, "i-frame-interval"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 2766077
    const-string v1, "color-format"

    const v2, 0x7f000789

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 2766078
    const-string v1, "durationUs"

    iget v2, p0, LX/K3g;->f:I

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaFormat;->setLong(Ljava/lang/String;J)V

    .line 2766079
    iget v1, p0, LX/K3g;->g:F

    const/high16 v2, 0x3f800000    # 1.0f

    rem-float/2addr v1, v2

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 2766080
    const-string v1, "frame-rate"

    iget v2, p0, LX/K3g;->g:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 2766081
    :goto_0
    iget v1, p0, LX/K3g;->d:I

    iget v2, p0, LX/K3g;->e:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, LX/K3g;->g:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v1, v2

    float-to-double v2, v1

    const-wide v4, 0x3fb1eb851eb851ecL    # 0.07

    mul-double/2addr v2, v4

    .line 2766082
    const-string v1, "bitrate"

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 2766083
    const-string v1, "max-input-size"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 2766084
    return-object v0

    .line 2766085
    :cond_0
    const-string v1, "frame-rate"

    iget v2, p0, LX/K3g;->g:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setFloat(Ljava/lang/String;F)V

    goto :goto_0
.end method

.method private static b(LX/K3f;)Ljava/util/concurrent/Future;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2766072
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2766073
    iget-object v1, p0, LX/K3f;->d:LX/0TD;

    new-instance v2, LX/K3e;

    invoke-direct {v2, p0}, LX/K3e;-><init>(LX/K3f;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/K3d;

    invoke-direct {v2, p0, v0}, LX/K3d;-><init>(LX/K3f;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v3, p0, LX/K3f;->d:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2766074
    return-object v0
.end method

.method private static c(LX/K3f;)V
    .locals 2

    .prologue
    .line 2766061
    iget-object v0, p0, LX/K3f;->i:LX/K3c;

    .line 2766062
    iget-object v1, v0, LX/K3c;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    move v0, v1

    .line 2766063
    if-eqz v0, :cond_0

    .line 2766064
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Exporting job cancelled"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2766065
    :cond_0
    return-void
.end method

.method public static d(LX/K3f;)V
    .locals 4

    .prologue
    .line 2765968
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/K3f;->h:LX/K3g;

    iget-object v1, v1, LX/K3g;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2765969
    :goto_0
    return-void

    .line 2765970
    :catch_0
    move-exception v0

    .line 2765971
    sget-object v1, LX/K3f;->a:Ljava/lang/String;

    const-string v2, "Delete export file failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static e(LX/K3f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2766054
    iget-object v0, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    if-nez v0, :cond_0

    .line 2766055
    :goto_0
    return-void

    .line 2766056
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->release()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2766057
    iput-object v4, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    goto :goto_0

    .line 2766058
    :catch_0
    move-exception v0

    .line 2766059
    :try_start_1
    sget-object v1, LX/K3f;->a:Ljava/lang/String;

    const-string v2, "Extractor release failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2766060
    iput-object v4, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v4, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    throw v0
.end method

.method private static f(LX/K3f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2765972
    iget-object v0, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    .line 2765973
    :goto_0
    return-void

    .line 2765974
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2765975
    iput-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    goto :goto_0

    .line 2765976
    :catch_0
    move-exception v0

    .line 2765977
    :try_start_1
    sget-object v1, LX/K3f;->a:Ljava/lang/String;

    const-string v2, "Encoder release failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2765978
    iput-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    throw v0
.end method

.method private static g(LX/K3f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2765979
    iget-object v0, p0, LX/K3f;->g:Landroid/media/MediaMuxer;

    if-nez v0, :cond_0

    .line 2765980
    :goto_0
    return-void

    .line 2765981
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/K3f;->g:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->release()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2765982
    iput-object v4, p0, LX/K3f;->g:Landroid/media/MediaMuxer;

    goto :goto_0

    .line 2765983
    :catch_0
    move-exception v0

    .line 2765984
    :try_start_1
    sget-object v1, LX/K3f;->a:Ljava/lang/String;

    const-string v2, "Muxer release failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2765985
    iput-object v4, p0, LX/K3f;->g:Landroid/media/MediaMuxer;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v4, p0, LX/K3f;->g:Landroid/media/MediaMuxer;

    throw v0
.end method


# virtual methods
.method public final a(LX/K3g;LX/K3c;LX/K4e;LX/K4c;Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;Landroid/opengl/EGLConfig;)Z
    .locals 14

    .prologue
    .line 2765986
    iput-object p1, p0, LX/K3f;->h:LX/K3g;

    .line 2765987
    move-object/from16 v0, p2

    iput-object v0, p0, LX/K3f;->i:LX/K3c;

    .line 2765988
    const/4 v7, 0x0

    .line 2765989
    const/4 v6, 0x0

    .line 2765990
    const/4 v5, 0x0

    .line 2765991
    :try_start_0
    invoke-static {p0}, LX/K3f;->c(LX/K3f;)V

    .line 2765992
    new-instance v4, Landroid/media/MediaExtractor;

    invoke-direct {v4}, Landroid/media/MediaExtractor;-><init>()V

    iput-object v4, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    .line 2765993
    iget-object v4, p0, LX/K3f;->e:Landroid/media/MediaExtractor;

    iget-object v8, p1, LX/K3g;->b:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;)V

    .line 2765994
    const-string v4, "video/avc"

    invoke-static {v4}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v4

    iput-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    .line 2765995
    iget-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-static {p1}, LX/K3f;->a(LX/K3g;)Landroid/media/MediaFormat;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 2765996
    iget-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v4}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v7

    .line 2765997
    iget-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v4}, Landroid/media/MediaCodec;->start()V

    .line 2765998
    sget-object v4, LX/K3f;->b:[I

    const/4 v8, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, p8

    invoke-static {v0, v1, v7, v4, v8}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v6

    .line 2765999
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Export eglCreateWindowSurface, handle: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2766000
    move-object/from16 v0, p5

    move-object/from16 v1, p7

    invoke-static {v0, v6, v6, v1}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2766001
    const-string v4, "eglMakeCurrent"

    invoke-static {v4}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2766002
    :cond_0
    invoke-static {p0}, LX/K3f;->b(LX/K3f;)Ljava/util/concurrent/Future;

    move-result-object v5

    .line 2766003
    const/4 v4, 0x1

    iget-object v8, p1, LX/K3g;->a:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x42c80000    # 100.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 2766004
    const/4 v4, 0x0

    move v8, v4

    :goto_0
    iget-object v4, p1, LX/K3g;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v8, v4, :cond_2

    .line 2766005
    invoke-static {p0}, LX/K3f;->c(LX/K3f;)V

    .line 2766006
    iget-object v4, p1, LX/K3g;->a:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5pG;

    invoke-static {v4}, LX/K4Y;->a(LX/5pG;)LX/K51;

    move-result-object v4

    iget v10, p1, LX/K3g;->d:I

    iget v11, p1, LX/K3g;->e:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v10, v11}, LX/K4c;->a(LX/K51;II)V

    .line 2766007
    add-int/lit8 v4, v8, 0x1

    int-to-long v10, v4

    const-wide/32 v12, 0x3b9aca00

    mul-long/2addr v10, v12

    long-to-float v4, v10

    iget v10, p1, LX/K3g;->g:F

    div-float/2addr v4, v10

    float-to-long v10, v4

    move-object/from16 v0, p5

    invoke-static {v0, v6, v10, v11}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 2766008
    move-object/from16 v0, p5

    invoke-static {v0, v6}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 2766009
    rem-int v4, v8, v9

    if-nez v4, :cond_1

    .line 2766010
    int-to-float v4, v8

    iget-object v10, p1, LX/K3g;->a:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v4, v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/K4e;->a(F)V

    .line 2766011
    :cond_1
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_0

    .line 2766012
    :cond_2
    iget-object v4, p0, LX/K3f;->f:Landroid/media/MediaCodec;

    invoke-virtual {v4}, Landroid/media/MediaCodec;->signalEndOfInputStream()V

    .line 2766013
    invoke-static {p0}, LX/K3f;->c(LX/K3f;)V

    .line 2766014
    const v4, -0x33d3a139    # -4.518582E7f

    invoke-static {v5, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 2766015
    :try_start_1
    invoke-static {p0}, LX/K3f;->e(LX/K3f;)V

    .line 2766016
    invoke-static {p0}, LX/K3f;->f(LX/K3f;)V

    .line 2766017
    invoke-static {p0}, LX/K3f;->g(LX/K3f;)V

    .line 2766018
    if-eqz v6, :cond_3

    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v6, v5, :cond_3

    .line 2766019
    move-object/from16 v0, p5

    invoke-static {v0, v6}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 2766020
    :cond_3
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 2766021
    if-eqz v7, :cond_4

    .line 2766022
    invoke-virtual {v7}, Landroid/view/Surface;->release()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 2766023
    :cond_4
    :goto_1
    return v4

    .line 2766024
    :catch_0
    move-exception v5

    .line 2766025
    sget-object v6, LX/K3f;->a:Ljava/lang/String;

    const-string v7, "Exporting job teardown exception"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v5, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2766026
    :catch_1
    move-exception v4

    .line 2766027
    :try_start_2
    sget-object v8, LX/K3f;->a:Ljava/lang/String;

    const-string v9, "Exporting job exception"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v4, v9, v10}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2766028
    if-eqz v5, :cond_5

    .line 2766029
    const/4 v4, 0x1

    invoke-interface {v5, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2766030
    :cond_5
    invoke-static {p0}, LX/K3f;->d(LX/K3f;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2766031
    :try_start_3
    invoke-static {p0}, LX/K3f;->e(LX/K3f;)V

    .line 2766032
    invoke-static {p0}, LX/K3f;->f(LX/K3f;)V

    .line 2766033
    invoke-static {p0}, LX/K3f;->g(LX/K3f;)V

    .line 2766034
    if-eqz v6, :cond_6

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v6, v4, :cond_6

    .line 2766035
    move-object/from16 v0, p5

    invoke-static {v0, v6}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 2766036
    :cond_6
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 2766037
    if-eqz v7, :cond_7

    .line 2766038
    invoke-virtual {v7}, Landroid/view/Surface;->release()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 2766039
    :cond_7
    :goto_2
    const/4 v4, 0x0

    goto :goto_1

    .line 2766040
    :catch_2
    move-exception v4

    .line 2766041
    sget-object v5, LX/K3f;->a:Ljava/lang/String;

    const-string v6, "Exporting job teardown exception"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v4, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2766042
    :catchall_0
    move-exception v4

    .line 2766043
    :try_start_4
    invoke-static {p0}, LX/K3f;->e(LX/K3f;)V

    .line 2766044
    invoke-static {p0}, LX/K3f;->f(LX/K3f;)V

    .line 2766045
    invoke-static {p0}, LX/K3f;->g(LX/K3f;)V

    .line 2766046
    if-eqz v6, :cond_8

    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v6, v5, :cond_8

    .line 2766047
    move-object/from16 v0, p5

    invoke-static {v0, v6}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 2766048
    :cond_8
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 2766049
    if-eqz v7, :cond_9

    .line 2766050
    invoke-virtual {v7}, Landroid/view/Surface;->release()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 2766051
    :cond_9
    :goto_3
    throw v4

    .line 2766052
    :catch_3
    move-exception v5

    .line 2766053
    sget-object v6, LX/K3f;->a:Ljava/lang/String;

    const-string v7, "Exporting job teardown exception"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v5, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method
