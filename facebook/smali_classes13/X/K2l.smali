.class public LX/K2l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field private static final b:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2764593
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "remote_push_trigger"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2764594
    sput-object v0, LX/K2l;->b:LX/0Tn;

    const-string v1, "last_server_ping_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/K2l;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2764595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2764596
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2764597
    sget-object v0, LX/K2l;->a:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
