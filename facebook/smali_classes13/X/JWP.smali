.class public final LX/JWP;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JWR;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JWQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JWR",
            "<TE;>.FutureFriendingButtonComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JWR;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JWR;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2702871
    iput-object p1, p0, LX/JWP;->b:LX/JWR;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2702872
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "buttonType"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "item"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "contactId"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "controller"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JWP;->c:[Ljava/lang/String;

    .line 2702873
    iput v3, p0, LX/JWP;->d:I

    .line 2702874
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JWP;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JWP;LX/1De;IILX/JWQ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JWR",
            "<TE;>.FutureFriendingButtonComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2702875
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2702876
    iput-object p4, p0, LX/JWP;->a:LX/JWQ;

    .line 2702877
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2702878
    return-void
.end method


# virtual methods
.method public final a(LX/1Pc;)LX/JWP;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2702879
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    iput-object p1, v0, LX/JWQ;->d:LX/1Pc;

    .line 2702880
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2702881
    return-object p0
.end method

.method public final a(LX/3mj;)LX/JWP;
    .locals 2
    .param p1    # LX/3mj;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3mj;",
            ")",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2702882
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    iput-object p1, v0, LX/JWQ;->f:LX/3mj;

    .line 2702883
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2702884
    return-object p0
.end method

.method public final a(LX/JWT;)LX/JWP;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JWT;",
            ")",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2702885
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    iput-object p1, v0, LX/JWQ;->a:LX/JWT;

    .line 2702886
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2702887
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JWP;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;)",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2702888
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    iput-object p1, v0, LX/JWQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702889
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2702890
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)LX/JWP;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;",
            ")",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2702891
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    iput-object p1, v0, LX/JWQ;->c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 2702892
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2702893
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2702894
    invoke-super {p0}, LX/1X5;->a()V

    .line 2702895
    const/4 v0, 0x0

    iput-object v0, p0, LX/JWP;->a:LX/JWQ;

    .line 2702896
    iget-object v0, p0, LX/JWP;->b:LX/JWR;

    iget-object v0, v0, LX/JWR;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2702897
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/JWP;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2702898
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    iput-object p1, v0, LX/JWQ;->e:Ljava/lang/String;

    .line 2702899
    iget-object v0, p0, LX/JWP;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2702900
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JWR;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2702901
    iget-object v1, p0, LX/JWP;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JWP;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JWP;->d:I

    if-ge v1, v2, :cond_2

    .line 2702902
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2702903
    :goto_0
    iget v2, p0, LX/JWP;->d:I

    if-ge v0, v2, :cond_1

    .line 2702904
    iget-object v2, p0, LX/JWP;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2702905
    iget-object v2, p0, LX/JWP;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2702906
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2702907
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2702908
    :cond_2
    iget-object v0, p0, LX/JWP;->a:LX/JWQ;

    .line 2702909
    invoke-virtual {p0}, LX/JWP;->a()V

    .line 2702910
    return-object v0
.end method
