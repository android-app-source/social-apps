.class public final LX/JjC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/3uc;

.field public final synthetic b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;LX/3uc;)V
    .locals 0

    .prologue
    .line 2727336
    iput-object p1, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iput-object p2, p0, LX/JjC;->a:LX/3uc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 13

    .prologue
    .line 2727337
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    const/4 v1, 0x1

    .line 2727338
    iput-boolean v1, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->z:Z

    .line 2727339
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 2727340
    iget-object v2, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 2727341
    iget-object v2, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2727342
    iget-object v2, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->x:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2727343
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JjT;

    iget-object v1, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->x:Ljava/lang/String;

    iget-object v2, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->y:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iget-object v3, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v3, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    iget-object v4, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v4, v4, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    new-instance v5, LX/JjA;

    invoke-direct {v5, p0}, LX/JjA;-><init>(LX/JjC;)V

    .line 2727344
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    if-nez v3, :cond_3

    .line 2727345
    :cond_0
    :goto_0
    return-void

    .line 2727346
    :cond_1
    iget-object v2, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->w:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-wide v4, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->u:J

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2727347
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JjT;

    iget-object v1, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->w:Ljava/lang/String;

    iget-object v2, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    iget-object v3, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v3, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    new-instance v4, LX/JjB;

    invoke-direct {v4, p0}, LX/JjB;-><init>(LX/JjC;)V

    const/4 v8, 0x0

    .line 2727348
    move-object v6, v0

    move-object v7, v1

    move-object v9, v2

    move-object v10, v8

    move-object v11, v3

    move-object v12, v4

    invoke-static/range {v6 .. v12}, LX/JjT;->a(LX/JjT;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;LX/Jj7;)V

    .line 2727349
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->v:LX/Jjb;

    if-eqz v0, :cond_0

    .line 2727350
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->v:LX/Jjb;

    iget-object v1, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    .line 2727351
    iget-object v2, v0, LX/Jjb;->a:LX/Jjc;

    iget-object v2, v2, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    .line 2727352
    iput-object v1, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    .line 2727353
    iget-object v2, v0, LX/Jjb;->a:LX/Jjc;

    iget-object v2, v2, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iget-object v3, v0, LX/Jjb;->a:LX/Jjc;

    iget-object v3, v3, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-static {v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setText(Ljava/lang/CharSequence;)V

    .line 2727354
    goto :goto_0

    .line 2727355
    :cond_2
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->c(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;Z)V

    .line 2727356
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    invoke-static {v0}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->a(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;)V

    .line 2727357
    iget-object v0, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jjj;

    iget-object v2, p0, LX/JjC;->a:LX/3uc;

    iget-object v1, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jjx;

    .line 2727358
    sget-object v3, LX/Jjw;->a:[I

    invoke-static {v1}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object v4

    invoke-virtual {v4}, LX/Jk2;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2727359
    const v3, 0x7f082dd7

    :goto_1
    move v3, v3

    .line 2727360
    iget-object v1, p0, LX/JjC;->b:Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jjx;

    .line 2727361
    sget-object v4, LX/Jjw;->a:[I

    invoke-static {v1}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object v5

    invoke-virtual {v5}, LX/Jk2;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 2727362
    const v4, 0x7f082dda

    :goto_2
    move v1, v4

    .line 2727363
    invoke-virtual {v0, v2, v3, v1}, LX/Jjj;->a(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 2727364
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v1, v6, v3, v4}, LX/JjT;->a(LX/JjT;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/4Gf;

    move-result-object v6

    .line 2727365
    new-instance v7, LX/Jjk;

    invoke-direct {v7}, LX/Jjk;-><init>()V

    move-object v7, v7

    .line 2727366
    const-string v8, "input"

    invoke-virtual {v7, v8, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/Jjk;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 2727367
    iget-object v7, v0, LX/JjT;->c:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2727368
    new-instance v7, LX/JjS;

    invoke-direct {v7, v0, v5}, LX/JjS;-><init>(LX/JjT;LX/Jj7;)V

    .line 2727369
    iget-object v8, v0, LX/JjT;->d:LX/1Ck;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "tasks-createEvent:"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, p0, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    .line 2727370
    :pswitch_0
    const v3, 0x7f082dd8

    goto :goto_1

    .line 2727371
    :pswitch_1
    const v4, 0x7f082ddb

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
