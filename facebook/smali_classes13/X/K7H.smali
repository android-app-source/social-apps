.class public final LX/K7H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/facebook/tarot/data/FeedbackData;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2772945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772946
    iput-object p1, p0, LX/K7H;->a:Ljava/lang/String;

    .line 2772947
    return-void
.end method


# virtual methods
.method public final a(F)LX/K7H;
    .locals 0

    .prologue
    .line 2772948
    iput p1, p0, LX/K7H;->h:F

    .line 2772949
    return-object p0
.end method

.method public final a(Lcom/facebook/tarot/data/FeedbackData;)LX/K7H;
    .locals 0

    .prologue
    .line 2772950
    iput-object p1, p0, LX/K7H;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772951
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/K7H;
    .locals 0

    .prologue
    .line 2772942
    iput-object p1, p0, LX/K7H;->b:Ljava/lang/String;

    .line 2772943
    return-object p0
.end method

.method public final a()Lcom/facebook/tarot/data/TarotCardImageData;
    .locals 8

    .prologue
    .line 2772944
    new-instance v0, Lcom/facebook/tarot/data/TarotCardImageData;

    iget-object v1, p0, LX/K7H;->a:Ljava/lang/String;

    new-instance v2, Lcom/facebook/tarot/data/BackgroundImageData;

    iget-object v3, p0, LX/K7H;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/tarot/data/BackgroundImageData;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/facebook/tarot/data/DescriptionData;

    iget-object v4, p0, LX/K7H;->c:Ljava/lang/String;

    iget-object v5, p0, LX/K7H;->d:Ljava/lang/String;

    iget v6, p0, LX/K7H;->h:F

    iget v7, p0, LX/K7H;->i:F

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/tarot/data/DescriptionData;-><init>(Ljava/lang/String;Ljava/lang/String;FF)V

    iget-object v4, p0, LX/K7H;->e:Lcom/facebook/tarot/data/FeedbackData;

    iget-object v5, p0, LX/K7H;->f:Ljava/lang/String;

    iget-object v6, p0, LX/K7H;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tarot/data/TarotCardImageData;-><init>(Ljava/lang/String;Lcom/facebook/tarot/data/BackgroundImageData;Lcom/facebook/tarot/data/DescriptionData;Lcom/facebook/tarot/data/FeedbackData;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(F)LX/K7H;
    .locals 0

    .prologue
    .line 2772940
    iput p1, p0, LX/K7H;->i:F

    .line 2772941
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/K7H;
    .locals 0

    .prologue
    .line 2772938
    iput-object p1, p0, LX/K7H;->c:Ljava/lang/String;

    .line 2772939
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/K7H;
    .locals 0

    .prologue
    .line 2772936
    iput-object p1, p0, LX/K7H;->d:Ljava/lang/String;

    .line 2772937
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/K7H;
    .locals 0

    .prologue
    .line 2772934
    iput-object p1, p0, LX/K7H;->f:Ljava/lang/String;

    .line 2772935
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/K7H;
    .locals 0

    .prologue
    .line 2772932
    iput-object p1, p0, LX/K7H;->g:Ljava/lang/String;

    .line 2772933
    return-object p0
.end method
