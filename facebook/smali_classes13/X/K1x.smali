.class public final LX/K1x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7ll;


# instance fields
.field public final synthetic a:LX/K1z;

.field public final synthetic b:LX/K20;


# direct methods
.method public constructor <init>(LX/K20;LX/K1z;)V
    .locals 0

    .prologue
    .line 2763014
    iput-object p1, p0, LX/K1x;->b:LX/K20;

    iput-object p2, p0, LX/K1x;->a:LX/K1z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0

    .prologue
    .line 2763013
    return-void
.end method

.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 0

    .prologue
    .line 2763012
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 2

    .prologue
    .line 2762994
    iget-object v0, p0, LX/K1x;->a:LX/K1z;

    if-eqz v0, :cond_0

    .line 2762995
    iget-object v0, p0, LX/K1x;->a:LX/K1z;

    .line 2762996
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2762997
    const-string v1, "block_media_type"

    const-string p1, "article"

    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2762998
    const-string v1, "ia_source"

    const-string p1, "share_button"

    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2762999
    iget-object v1, v0, LX/K1z;->a:LX/K20;

    iget-object v1, v1, LX/K20;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const/4 p1, -0x1

    const-string p2, "feed_share_action"

    invoke-virtual {v1, p1, p2, p0}, LX/Ckw;->a(ILjava/lang/String;Ljava/util/Map;)V

    .line 2763000
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2763001
    iget-object v0, p0, LX/K1x;->a:LX/K1z;

    if-eqz v0, :cond_0

    .line 2763002
    iget-object v0, p0, LX/K1x;->a:LX/K1z;

    .line 2763003
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 2763004
    const-string p0, "error_code"

    .line 2763005
    iget-object p2, p3, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object p2, p2

    .line 2763006
    invoke-virtual {p2}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2763007
    const-string p0, "block_media_type"

    const-string p2, "article"

    invoke-interface {p1, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2763008
    const-string p0, "ia_source"

    const-string p2, "share_button"

    invoke-interface {p1, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2763009
    const-string p0, "share_type"

    const-string p2, "share_failed"

    invoke-interface {p1, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2763010
    iget-object p0, v0, LX/K1z;->a:LX/K20;

    iget-object p0, p0, LX/K20;->g:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ckw;

    const-string p2, "feed_share_action"

    invoke-virtual {p0, p2, p1}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 2763011
    :cond_0
    return-void
.end method
