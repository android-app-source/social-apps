.class public final enum LX/JcJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JcJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JcJ;

.field public static final enum INCOMPATIBLE_DEVICE:LX/JcJ;

.field public static final enum PERMISSION_DENIED:LX/JcJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2717748
    new-instance v0, LX/JcJ;

    const-string v1, "INCOMPATIBLE_DEVICE"

    invoke-direct {v0, v1, v2}, LX/JcJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcJ;->INCOMPATIBLE_DEVICE:LX/JcJ;

    .line 2717749
    new-instance v0, LX/JcJ;

    const-string v1, "PERMISSION_DENIED"

    invoke-direct {v0, v1, v3}, LX/JcJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JcJ;->PERMISSION_DENIED:LX/JcJ;

    .line 2717750
    const/4 v0, 0x2

    new-array v0, v0, [LX/JcJ;

    sget-object v1, LX/JcJ;->INCOMPATIBLE_DEVICE:LX/JcJ;

    aput-object v1, v0, v2

    sget-object v1, LX/JcJ;->PERMISSION_DENIED:LX/JcJ;

    aput-object v1, v0, v3

    sput-object v0, LX/JcJ;->$VALUES:[LX/JcJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2717751
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JcJ;
    .locals 1

    .prologue
    .line 2717747
    const-class v0, LX/JcJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JcJ;

    return-object v0
.end method

.method public static values()[LX/JcJ;
    .locals 1

    .prologue
    .line 2717746
    sget-object v0, LX/JcJ;->$VALUES:[LX/JcJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JcJ;

    return-object v0
.end method
