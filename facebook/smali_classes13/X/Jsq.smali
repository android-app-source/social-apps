.class public LX/Jsq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Jsq;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0lC;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2745899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745900
    iput-object p1, p0, LX/Jsq;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2745901
    iput-object p2, p0, LX/Jsq;->b:LX/0lC;

    .line 2745902
    iput-object p3, p0, LX/Jsq;->c:LX/03V;

    .line 2745903
    return-void
.end method

.method public static a(LX/0QB;)LX/Jsq;
    .locals 6

    .prologue
    .line 2745904
    sget-object v0, LX/Jsq;->d:LX/Jsq;

    if-nez v0, :cond_1

    .line 2745905
    const-class v1, LX/Jsq;

    monitor-enter v1

    .line 2745906
    :try_start_0
    sget-object v0, LX/Jsq;->d:LX/Jsq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2745907
    if-eqz v2, :cond_0

    .line 2745908
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2745909
    new-instance p0, LX/Jsq;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/Jsq;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;)V

    .line 2745910
    move-object v0, p0

    .line 2745911
    sput-object v0, LX/Jsq;->d:LX/Jsq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2745912
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2745913
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2745914
    :cond_1
    sget-object v0, LX/Jsq;->d:LX/Jsq;

    return-object v0

    .line 2745915
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2745916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/Jsq;)V
    .locals 2

    .prologue
    .line 2745917
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jsq;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Jow;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2745918
    monitor-exit p0

    return-void

    .line 2745919
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(ZLjava/lang/String;Lcom/facebook/user/model/User;)V
    .locals 6
    .param p3    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745920
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 2745921
    :cond_0
    invoke-static {p0}, LX/Jsq;->a(LX/Jsq;)V

    .line 2745922
    :goto_0
    return-void

    .line 2745923
    :cond_1
    new-instance v0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;

    .line 2745924
    iget-object v1, p3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v1

    .line 2745925
    invoke-virtual {p3}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v5

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/user/model/PicSquare;)V

    .line 2745926
    monitor-enter p0

    .line 2745927
    :try_start_0
    iget-object v1, p0, LX/Jsq;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Jow;->b:LX/0Tn;

    iget-object v3, p0, LX/Jsq;->b:LX/0lC;

    invoke-virtual {v3, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2745928
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2745929
    :catch_0
    move-exception v0

    .line 2745930
    :try_start_2
    iget-object v1, p0, LX/Jsq;->c:LX/03V;

    const-string v2, "Corrupt PhoneReconfirmationInfo Write"

    const-string v3, ""

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
