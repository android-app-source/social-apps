.class public LX/Jxh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final h:Ljava/lang/String;

.field private static final i:LX/0Tn;

.field public static final j:LX/0Tn;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:LX/03V;

.field private c:LX/J7f;

.field public d:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/JxY;

.field public g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2753314
    const-class v0, LX/Jxh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Jxh;->h:Ljava/lang/String;

    .line 2753315
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "rdc_pref_key/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2753316
    sput-object v0, LX/Jxh;->i:LX/0Tn;

    const-string v1, "rating_story_index_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Jxh;->j:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/J7f;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1

    .prologue
    .line 2753329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753330
    iput-object p3, p0, LX/Jxh;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2753331
    iput-object p1, p0, LX/Jxh;->b:LX/03V;

    .line 2753332
    iput-object p2, p0, LX/Jxh;->c:LX/J7f;

    .line 2753333
    new-instance v0, LX/Jxg;

    invoke-direct {v0, p0}, LX/Jxg;-><init>(LX/Jxh;)V

    move-object v0, v0

    .line 2753334
    iput-object v0, p0, LX/Jxh;->d:LX/0TF;

    .line 2753335
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2753327
    iget-object v0, p0, LX/Jxh;->c:LX/J7f;

    iget-object v1, p0, LX/Jxh;->d:LX/0TF;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/J7f;->a(LX/0TF;I)V

    .line 2753328
    return-void
.end method

.method public final c()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2753317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2753318
    iget v1, p0, LX/Jxh;->g:I

    iget-object v2, p0, LX/Jxh;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2753319
    iget v1, p0, LX/Jxh;->g:I

    iget-object v2, p0, LX/Jxh;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2753320
    iget-object v1, p0, LX/Jxh;->e:LX/0Px;

    iget v2, p0, LX/Jxh;->g:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;

    .line 2753321
    :goto_0
    move-object v1, v1

    .line 2753322
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2753323
    iget v1, p0, LX/Jxh;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/Jxh;->g:I

    .line 2753324
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2753325
    :cond_1
    iget-object v1, p0, LX/Jxh;->b:LX/03V;

    sget-object v2, LX/Jxh;->h:Ljava/lang/String;

    const-string v3, "survey unit index out of bound"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2753326
    const/4 v1, 0x0

    goto :goto_0
.end method
