.class public LX/JrA;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/2N4;

.field private final c:LX/Jrb;

.field private final d:LX/Jqb;

.field private final e:LX/Jrc;

.field private final f:LX/0SG;

.field public final g:LX/Do7;

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742171
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrA;->i:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/FDs;LX/2N4;LX/Jrb;LX/Jqb;LX/Jrc;LX/0SG;LX/Do7;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/2N4;",
            "LX/Jrb;",
            "LX/Jqb;",
            "LX/Jrc;",
            "LX/0SG;",
            "LX/Do7;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742075
    invoke-direct {p0, p8}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742076
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742077
    iput-object v0, p0, LX/JrA;->h:LX/0Ot;

    .line 2742078
    iput-object p1, p0, LX/JrA;->a:LX/FDs;

    .line 2742079
    iput-object p2, p0, LX/JrA;->b:LX/2N4;

    .line 2742080
    iput-object p3, p0, LX/JrA;->c:LX/Jrb;

    .line 2742081
    iput-object p4, p0, LX/JrA;->d:LX/Jqb;

    .line 2742082
    iput-object p5, p0, LX/JrA;->e:LX/Jrc;

    .line 2742083
    iput-object p6, p0, LX/JrA;->f:LX/0SG;

    .line 2742084
    iput-object p7, p0, LX/JrA;->g:LX/Do7;

    .line 2742085
    return-void
.end method

.method public static a(LX/0QB;)LX/JrA;
    .locals 7

    .prologue
    .line 2742144
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742145
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742146
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742147
    if-nez v1, :cond_0

    .line 2742148
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742149
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742150
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742151
    sget-object v1, LX/JrA;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742152
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742153
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742154
    :cond_1
    if-nez v1, :cond_4

    .line 2742155
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742156
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742157
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrA;->b(LX/0QB;)LX/JrA;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2742158
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742159
    if-nez v1, :cond_2

    .line 2742160
    sget-object v0, LX/JrA;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrA;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742161
    :goto_1
    if-eqz v0, :cond_3

    .line 2742162
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742163
    :goto_3
    check-cast v0, LX/JrA;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742164
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742165
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742166
    :catchall_1
    move-exception v0

    .line 2742167
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742168
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742169
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742170
    :cond_2
    :try_start_8
    sget-object v0, LX/JrA;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrA;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/6kC;Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 2742113
    iget-object v0, p1, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2742114
    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 2742115
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-direct {v0, v2, v4}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2742116
    iget-object v1, p0, LX/JrA;->c:LX/Jrb;

    .line 2742117
    iget-object v2, p1, LX/6kC;->messageMetadata:LX/6kn;

    invoke-static {v1, v2, p2}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v2

    sget-object v3, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    .line 2742118
    iput-object v3, v2, LX/6f7;->l:LX/2uW;

    .line 2742119
    move-object v2, v2

    .line 2742120
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2742121
    iput-object v3, v2, LX/6f7;->m:Ljava/util/List;

    .line 2742122
    move-object v2, v2

    .line 2742123
    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2742124
    iget-object v3, v1, LX/Jrb;->f:LX/3QS;

    sget-object v5, LX/6fK;->SYNC_PROTOCOL_PARTICIPANT_LEFT_GROUP_DELTA:LX/6fK;

    invoke-virtual {v3, v5, v2}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2742125
    move-object v3, v2

    .line 2742126
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/JrA;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742127
    iget-object v0, p0, LX/JrA;->g:LX/Do7;

    invoke-virtual {v0}, LX/Do7;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/JrA;->a:LX/FDs;

    iget-object v2, p1, LX/6kC;->messageMetadata:LX/6kn;

    invoke-static {v2}, LX/JrZ;->a(LX/6kn;)LX/6jT;

    move-result-object v2

    invoke-virtual {v0, v1, p3, p4, v2}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2742128
    :goto_0
    iget-object v1, p1, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 2742129
    iget-object v2, p0, LX/JrA;->a:LX/FDs;

    const/4 v5, 0x0

    .line 2742130
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2742131
    iget-object v7, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v5

    :goto_1
    if-ge v4, v8, :cond_1

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2742132
    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2742133
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2742134
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2742135
    :cond_1
    iget-object v3, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2, v6, v3}, LX/FDs;->a(LX/FDs;Ljava/util/List;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742136
    iget-object v3, v2, LX/FDs;->d:LX/2N4;

    iget-object v4, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, v4, v5}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v5, v3

    .line 2742137
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742138
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 2742139
    iget-object v3, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v3

    .line 2742140
    iget-object v4, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v4, v4

    .line 2742141
    iget-wide v9, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v9

    .line 2742142
    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    return-object v1

    .line 2742143
    :cond_2
    iget-object v0, p0, LX/JrA;->a:LX/FDs;

    invoke-virtual {v0, v1, p3, p4}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/JrA;
    .locals 9

    .prologue
    .line 2742172
    new-instance v0, LX/JrA;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v2

    check-cast v2, LX/2N4;

    invoke-static {p0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v3

    check-cast v3, LX/Jrb;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v4

    check-cast v4, LX/Jqb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v5

    check-cast v5, LX/Jrc;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p0}, LX/Do7;->a(LX/0QB;)LX/Do7;

    move-result-object v7

    check-cast v7, LX/Do7;

    const/16 v8, 0x35bd

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/JrA;-><init>(LX/FDs;LX/2N4;LX/Jrb;LX/Jqb;LX/Jrc;LX/0SG;LX/Do7;LX/0Ot;)V

    .line 2742173
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742174
    iput-object v1, v0, LX/JrA;->h:LX/0Ot;

    .line 2742175
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742111
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742112
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742104
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->j()LX/6kC;

    move-result-object v0

    .line 2742105
    iget-object v1, p0, LX/JrA;->b:LX/2N4;

    iget-object v2, p0, LX/JrA;->e:LX/Jrc;

    iget-object v3, v0, LX/6kC;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v2, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742106
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2742107
    if-eqz v1, :cond_0

    .line 2742108
    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-direct {p0, v0, v1, v4, v5}, LX/JrA;->a(LX/6kC;Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2742109
    const-string v1, "newMessageResult"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2742110
    :cond_0
    return-object v2
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742090
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742091
    if-eqz v0, :cond_0

    .line 2742092
    iget-object v1, p0, LX/JrA;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v4, p2, LX/7GJ;->b:J

    iget-object v2, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v2, LX/6kW;

    invoke-virtual {v2}, LX/6kW;->j()LX/6kC;

    move-result-object v2

    iget-object v2, v2, LX/6kC;->messageMetadata:LX/6kn;

    .line 2742093
    iget-object v3, p0, LX/JrA;->g:LX/Do7;

    invoke-virtual {v3}, LX/Do7;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, LX/JrZ;->a(LX/6kn;)LX/6jT;

    move-result-object v3

    :goto_0
    move-object v2, v3

    .line 2742094
    invoke-virtual {v1, v0, v4, v5, v2}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)V

    .line 2742095
    iget-object v1, p0, LX/JrA;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2742096
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2742097
    iget-wide v6, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v6

    .line 2742098
    invoke-virtual {v1, v2, v4, v5}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742099
    iget-object v1, p0, LX/JrA;->d:LX/Jqb;

    .line 2742100
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2742101
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742102
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2742103
    :cond_0
    return-void

    :cond_1
    sget-object v3, LX/6jT;->a:LX/6jT;

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2742086
    check-cast p1, LX/6kW;

    .line 2742087
    invoke-virtual {p1}, LX/6kW;->j()LX/6kC;

    move-result-object v0

    .line 2742088
    iget-object v1, p0, LX/JrA;->e:LX/Jrc;

    iget-object v0, v0, LX/6kC;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742089
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
