.class public LX/Jjx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2728657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2728658
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728659
    iput-object v0, p0, LX/Jjx;->a:LX/0Ot;

    .line 2728660
    return-void
.end method

.method public static k(LX/Jjx;)LX/Jk2;
    .locals 4

    .prologue
    .line 2728661
    iget-object v0, p0, LX/Jjx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->fG:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    invoke-static {v0}, LX/Jk2;->fromValue(I)LX/Jk2;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final h()I
    .locals 2

    .prologue
    .line 2728662
    sget-object v0, LX/Jjw;->a:[I

    invoke-static {p0}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object v1

    invoke-virtual {v1}, LX/Jk2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2728663
    const v0, 0x7f082dd4

    :goto_0
    return v0

    .line 2728664
    :pswitch_0
    const v0, 0x7f082dd5

    goto :goto_0

    .line 2728665
    :pswitch_1
    const v0, 0x7f082dd6

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
