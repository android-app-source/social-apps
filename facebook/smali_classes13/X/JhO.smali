.class public LX/JhO;
.super LX/3Nb;
.source ""

# interfaces
.implements LX/JhA;


# instance fields
.field public i:LX/Jgv;

.field public j:Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3LG;)V
    .locals 1

    .prologue
    .line 2724435
    const v0, 0x7f030cf6

    invoke-direct {p0, p1, p2, v0}, LX/3Nb;-><init>(Landroid/content/Context;LX/3LG;I)V

    .line 2724436
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    check-cast v0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;

    iput-object v0, p0, LX/JhO;->j:Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;

    .line 2724437
    iget-object v0, p0, LX/JhO;->j:Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;

    new-instance p1, LX/JhL;

    invoke-direct {p1, p0}, LX/JhL;-><init>(LX/JhO;)V

    .line 2724438
    iput-object p1, v0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->a:LX/DAi;

    .line 2724439
    const v0, 0x7f0d205d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    new-instance p1, LX/JhM;

    invoke-direct {p1, p0}, LX/JhM;-><init>(LX/JhO;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724440
    const v0, 0x7f0d205c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    new-instance p1, LX/JhN;

    invoke-direct {p1, p0}, LX/JhN;-><init>(LX/JhO;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724441
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2724442
    invoke-super {p0, p1}, LX/3Nb;->a(LX/0Px;)V

    .line 2724443
    return-void
.end method

.method public final a(LX/3Nd;)V
    .locals 1

    .prologue
    .line 2724444
    invoke-super {p0, p1}, LX/3Nb;->a(LX/3Nd;)V

    .line 2724445
    sget-object v0, LX/3Nd;->NONE:LX/3Nd;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/3Nd;->UNFILTERED:LX/3Nd;

    if-ne p1, v0, :cond_1

    .line 2724446
    :cond_0
    iget-object v0, p0, LX/JhO;->j:Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;

    invoke-virtual {v0}, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->g()V

    .line 2724447
    :goto_0
    return-void

    .line 2724448
    :cond_1
    iget-object v0, p0, LX/JhO;->j:Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;

    invoke-virtual {v0}, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->h()V

    goto :goto_0
.end method

.method public getDraggableList()Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;
    .locals 1

    .prologue
    .line 2724449
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    .line 2724450
    iget-object p0, v0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, p0

    .line 2724451
    move-object v0, v0

    .line 2724452
    check-cast v0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

    return-object v0
.end method

.method public getSearchBar()LX/3Nf;
    .locals 1

    .prologue
    .line 2724453
    iget-object v0, p0, LX/JhO;->j:Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;

    return-object v0
.end method

.method public bridge synthetic getSearchBar()LX/3Ng;
    .locals 1

    .prologue
    .line 2724454
    invoke-virtual {p0}, LX/JhO;->getSearchBar()LX/3Nf;

    move-result-object v0

    return-object v0
.end method

.method public getThisView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2724455
    return-object p0
.end method

.method public setOnButtonClickedListener(LX/Jgv;)V
    .locals 0

    .prologue
    .line 2724456
    iput-object p1, p0, LX/JhO;->i:LX/Jgv;

    .line 2724457
    return-void
.end method
