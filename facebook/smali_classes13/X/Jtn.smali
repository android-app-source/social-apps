.class public LX/Jtn;
.super LX/CH6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CH6",
        "<",
        "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Jtn;


# direct methods
.method public constructor <init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2747559
    invoke-direct {p0, p1, p2, p3, p4}, LX/CH6;-><init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;)V

    .line 2747560
    return-void
.end method

.method public static a(LX/0QB;)LX/Jtn;
    .locals 7

    .prologue
    .line 2747561
    sget-object v0, LX/Jtn;->a:LX/Jtn;

    if-nez v0, :cond_1

    .line 2747562
    const-class v1, LX/Jtn;

    monitor-enter v1

    .line 2747563
    :try_start_0
    sget-object v0, LX/Jtn;->a:LX/Jtn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2747564
    if-eqz v2, :cond_0

    .line 2747565
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2747566
    new-instance p0, LX/Jtn;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v4

    check-cast v4, LX/0t2;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Jtn;-><init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;)V

    .line 2747567
    move-object v0, p0

    .line 2747568
    sput-object v0, LX/Jtn;->a:LX/Jtn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2747569
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2747570
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2747571
    :cond_1
    sget-object v0, LX/Jtn;->a:LX/Jtn;

    return-object v0

    .line 2747572
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2747573
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
