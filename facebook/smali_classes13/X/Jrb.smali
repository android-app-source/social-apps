.class public LX/Jrb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/Jrb;


# instance fields
.field private final a:LX/Jrc;

.field private final b:LX/3QO;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/7GF;

.field private final e:LX/7G3;

.field public final f:LX/3QS;

.field private final g:LX/2Ns;

.field private final h:LX/3Mz;

.field private final i:LX/3Le;

.field private final j:LX/2Mk;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/Jol;


# direct methods
.method public constructor <init>(LX/Jrc;LX/3QO;LX/0Or;LX/7GF;LX/7G3;LX/3QS;LX/2Ns;LX/3Mz;LX/3Le;LX/2Mk;LX/0Ot;LX/0Ot;LX/Jol;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jrc;",
            "LX/3QO;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/7GF;",
            "LX/7G3;",
            "LX/3QS;",
            "LX/2Ns;",
            "LX/3Mz;",
            "LX/3Le;",
            "LX/2Mk;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/Jol;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2744141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2744142
    iput-object p1, p0, LX/Jrb;->a:LX/Jrc;

    .line 2744143
    iput-object p2, p0, LX/Jrb;->b:LX/3QO;

    .line 2744144
    iput-object p3, p0, LX/Jrb;->c:LX/0Or;

    .line 2744145
    iput-object p4, p0, LX/Jrb;->d:LX/7GF;

    .line 2744146
    iput-object p5, p0, LX/Jrb;->e:LX/7G3;

    .line 2744147
    iput-object p6, p0, LX/Jrb;->f:LX/3QS;

    .line 2744148
    iput-object p7, p0, LX/Jrb;->g:LX/2Ns;

    .line 2744149
    iput-object p8, p0, LX/Jrb;->h:LX/3Mz;

    .line 2744150
    iput-object p9, p0, LX/Jrb;->i:LX/3Le;

    .line 2744151
    iput-object p10, p0, LX/Jrb;->j:LX/2Mk;

    .line 2744152
    iput-object p11, p0, LX/Jrb;->k:LX/0Ot;

    .line 2744153
    iput-object p12, p0, LX/Jrb;->l:LX/0Ot;

    .line 2744154
    iput-object p13, p0, LX/Jrb;->m:LX/Jol;

    .line 2744155
    return-void
.end method

.method public static a(LX/Jrb;Ljava/util/List;Ljava/lang/String;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2744156
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2744157
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/6jZ;

    .line 2744158
    iget-object v0, v8, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2744159
    new-instance v0, LX/5dN;

    iget-object v1, v8, LX/6jZ;->id:Ljava/lang/String;

    invoke-direct {v0, v1, p2}, LX/5dN;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v8, LX/6jZ;->mimeType:Ljava/lang/String;

    .line 2744160
    iput-object v1, v0, LX/5dN;->d:Ljava/lang/String;

    .line 2744161
    move-object v0, v0

    .line 2744162
    iget-object v1, v8, LX/6jZ;->filename:Ljava/lang/String;

    .line 2744163
    iput-object v1, v0, LX/5dN;->e:Ljava/lang/String;

    .line 2744164
    move-object v12, v0

    .line 2744165
    iget-object v0, v8, LX/6jZ;->fbid:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2744166
    iget-object v0, v8, LX/6jZ;->fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 2744167
    iput-object v0, v12, LX/5dN;->c:Ljava/lang/String;

    .line 2744168
    :cond_1
    iget-object v0, v8, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2744169
    iget-object v0, v8, LX/6jZ;->fileSize:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    .line 2744170
    iput v0, v12, LX/5dN;->f:I

    .line 2744171
    :cond_2
    iget-object v0, v8, LX/6jZ;->imageMetadata:LX/6ke;

    if-eqz v0, :cond_3

    .line 2744172
    iget-object v0, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v0, v0, LX/6ke;->imageURIMap:Ljava/util/Map;

    invoke-direct {p0, p2, v0}, LX/Jrb;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v3

    .line 2744173
    iget-object v0, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v0, v0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    invoke-direct {p0, p2, v0}, LX/Jrb;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v4

    .line 2744174
    iget-object v0, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v0, v0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-nez v0, :cond_6

    sget-object v5, LX/5dT;->NONQUICKCAM:LX/5dT;

    .line 2744175
    :goto_1
    new-instance v0, Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v1, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v1, v1, LX/6ke;->width:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v2, v2, LX/6ke;->height:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v6, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v6, v6, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-eqz v6, :cond_7

    iget-object v6, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v6, v6, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    :goto_2
    iget-object v7, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v7, v7, LX/6ke;->miniPreview:[B

    if-eqz v7, :cond_8

    iget-object v7, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v7, v7, LX/6ke;->miniPreview:[B

    invoke-static {v7, v9}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    :goto_3
    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/ImageData;-><init>(IILcom/facebook/messaging/model/attachment/AttachmentImageMap;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;LX/5dT;ZLjava/lang/String;)V

    .line 2744176
    iput-object v0, v12, LX/5dN;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 2744177
    :cond_3
    iget-object v0, v8, LX/6jZ;->audioMetadata:LX/6jd;

    if-eqz v0, :cond_4

    .line 2744178
    new-instance v0, Lcom/facebook/messaging/model/attachment/AudioData;

    iget-object v1, v8, LX/6jZ;->audioMetadata:LX/6jd;

    iget-object v1, v1, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, v8, LX/6jZ;->audioMetadata:LX/6jd;

    iget-object v2, v2, LX/6jd;->callId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/attachment/AudioData;-><init>(ZLjava/lang/String;)V

    .line 2744179
    iput-object v0, v12, LX/5dN;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 2744180
    :cond_4
    iget-object v0, v8, LX/6jZ;->videoMetadata:LX/6lC;

    if-eqz v0, :cond_5

    .line 2744181
    new-instance v0, Lcom/facebook/messaging/model/attachment/VideoData;

    iget-object v1, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v1, v1, LX/6lC;->width:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v2, v2, LX/6lC;->height:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v3, v3, LX/6lC;->rotation:Ljava/lang/Integer;

    if-nez v3, :cond_9

    move v3, v9

    :goto_4
    iget-object v4, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v4, v4, LX/6lC;->durationMs:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    iget-object v5, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v5, v5, LX/6lC;->source:Ljava/lang/Integer;

    .line 2744182
    if-nez v5, :cond_b

    .line 2744183
    sget-object v6, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    .line 2744184
    :goto_5
    move-object v5, v6

    .line 2744185
    iget-object v6, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v6, v6, LX/6lC;->videoUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v7, v7, LX/6lC;->thumbnailUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/VideoData;-><init>(IIIILX/5dX;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 2744186
    iput-object v0, v12, LX/5dN;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2744187
    :cond_5
    invoke-virtual {v12}, LX/5dN;->m()Lcom/facebook/messaging/model/attachment/Attachment;

    move-result-object v0

    invoke-virtual {v10, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2744188
    :cond_6
    iget-object v0, v8, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v0, v0, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/5dT;->fromIntVal(I)LX/5dT;

    move-result-object v5

    goto/16 :goto_1

    :cond_7
    move v6, v9

    .line 2744189
    goto/16 :goto_2

    :cond_8
    const/4 v7, 0x0

    goto :goto_3

    .line 2744190
    :cond_9
    iget-object v3, v8, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v3, v3, LX/6lC;->rotation:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_4

    .line 2744191
    :cond_a
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2744192
    :cond_b
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_c

    .line 2744193
    sget-object v6, LX/5dX;->QUICKCAM:LX/5dX;

    goto :goto_5

    .line 2744194
    :cond_c
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_d

    .line 2744195
    sget-object v6, LX/5dX;->VIDEO_STICKER:LX/5dX;

    goto :goto_5

    .line 2744196
    :cond_d
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_e

    .line 2744197
    sget-object v6, LX/5dX;->VIDEO_MAIL:LX/5dX;

    goto :goto_5

    .line 2744198
    :cond_e
    sget-object v6, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    goto :goto_5
.end method

.method public static a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;
    .locals 6

    .prologue
    .line 2744199
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    .line 2744200
    iput-object p2, v0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2744201
    move-object v0, v0

    .line 2744202
    iget-object v1, p1, LX/6kn;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v0

    iget-object v1, p1, LX/6kn;->timestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2744203
    iput-wide v2, v0, LX/6f7;->c:J

    .line 2744204
    move-object v0, v0

    .line 2744205
    iget-object v1, p1, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 2744206
    iput-object v1, v0, LX/6f7;->n:Ljava/lang/String;

    .line 2744207
    move-object v0, v0

    .line 2744208
    iget-object v1, p1, LX/6kn;->adminText:Ljava/lang/String;

    .line 2744209
    iput-object v1, v0, LX/6f7;->f:Ljava/lang/String;

    .line 2744210
    move-object v0, v0

    .line 2744211
    iput-object p3, v0, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2744212
    move-object v0, v0

    .line 2744213
    sget-object v1, LX/6f2;->MQTT:LX/6f2;

    .line 2744214
    iput-object v1, v0, LX/6f7;->q:LX/6f2;

    .line 2744215
    move-object v0, v0

    .line 2744216
    iget-object v2, p1, LX/6kn;->tags:Ljava/util/List;

    const/4 v3, 0x0

    .line 2744217
    if-nez v2, :cond_1

    move-object v1, v3

    .line 2744218
    :goto_0
    move-object v1, v1

    .line 2744219
    iput-object v1, v0, LX/6f7;->p:Ljava/lang/String;

    .line 2744220
    move-object v0, v0

    .line 2744221
    iget-object v1, p1, LX/6kn;->tags:Ljava/util/List;

    invoke-static {v1}, LX/Do8;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/util/Map;)LX/6f7;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/model/messages/Publicity;->c:Lcom/facebook/messaging/model/messages/Publicity;

    .line 2744222
    iput-object v1, v0, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    .line 2744223
    move-object v0, v0

    .line 2744224
    iget-object v1, p1, LX/6kn;->timestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/6fa;->a(J)J

    move-result-wide v2

    .line 2744225
    iput-wide v2, v0, LX/6f7;->g:J

    .line 2744226
    iget-object v1, p1, LX/6kn;->messageId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2744227
    iget-object v1, p1, LX/6kn;->messageId:Ljava/lang/String;

    .line 2744228
    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    .line 2744229
    :goto_1
    return-object v0

    .line 2744230
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_6

    const/4 v4, 0x0

    :goto_2
    move-object v1, v4

    .line 2744231
    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    goto :goto_1

    .line 2744232
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2744233
    const-string v2, "messenger"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2744234
    :goto_3
    move-object v1, v1

    .line 2744235
    if-eqz v1, :cond_2

    goto :goto_0

    :cond_3
    move-object v1, v3

    .line 2744236
    goto :goto_0

    .line 2744237
    :cond_4
    const-string v2, "source:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2744238
    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2744239
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "m_action:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;
    .locals 2

    .prologue
    .line 2744240
    iget-object v0, p1, LX/6kn;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p2, v0, v1}, LX/Jrb;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 2744241
    iget-object v1, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2744242
    invoke-static {p0, p1, v1, v0}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jrb;
    .locals 3

    .prologue
    .line 2744243
    sget-object v0, LX/Jrb;->n:LX/Jrb;

    if-nez v0, :cond_1

    .line 2744244
    const-class v1, LX/Jrb;

    monitor-enter v1

    .line 2744245
    :try_start_0
    sget-object v0, LX/Jrb;->n:LX/Jrb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2744246
    if-eqz v2, :cond_0

    .line 2744247
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Jrb;->b(LX/0QB;)LX/Jrb;

    move-result-object v0

    sput-object v0, LX/Jrb;->n:LX/Jrb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2744248
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2744249
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2744250
    :cond_1
    sget-object v0, LX/Jrb;->n:LX/Jrb;

    return-object v0

    .line 2744251
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2744252
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/messaging/model/attachment/AttachmentImageMap;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, -0x1

    .line 2744253
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v2

    .line 2744254
    :cond_1
    :goto_0
    return-object v0

    .line 2744255
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->newBuilder()LX/5dP;

    move-result-object v3

    .line 2744256
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2744257
    iget-object v1, p0, LX/Jrb;->d:LX/7GF;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v1, v5}, LX/7GF;->a(I)I

    move-result v1

    .line 2744258
    iget-object v5, p0, LX/Jrb;->d:LX/7GF;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, LX/7GF;->b(I)I

    move-result v5

    .line 2744259
    if-eq v1, v7, :cond_3

    if-eq v5, v7, :cond_3

    .line 2744260
    new-instance v6, LX/5dM;

    invoke-direct {v6}, LX/5dM;-><init>()V

    .line 2744261
    iput v1, v6, LX/5dM;->a:I

    .line 2744262
    move-object v1, v6

    .line 2744263
    iput v5, v1, LX/5dM;->b:I

    .line 2744264
    move-object v5, v1

    .line 2744265
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2744266
    iput-object v1, v5, LX/5dM;->c:Ljava/lang/String;

    .line 2744267
    move-object v1, v5

    .line 2744268
    invoke-virtual {v1}, LX/5dM;->d()Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v1

    .line 2744269
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/5dQ;->fromPersistentIndex(I)LX/5dQ;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    goto :goto_1

    .line 2744270
    :cond_4
    invoke-virtual {v3}, LX/5dP;->b()Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v0

    .line 2744271
    invoke-static {v0}, LX/5dU;->a(Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2744272
    iget-object v1, p0, LX/Jrb;->e:LX/7G3;

    .line 2744273
    iget-object v3, v1, LX/7G3;->a:LX/03V;

    const-string v4, "sync_bad_cdn_attachment_info"

    invoke-static {p1, v0}, LX/5dU;->a(Ljava/lang/String;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2744274
    move-object v0, v2

    .line 2744275
    goto :goto_0
.end method

.method private a(Ljava/util/List;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;)",
            "Lcom/facebook/messaging/model/attribution/ContentAppAttribution;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2743973
    if-eqz p1, :cond_1

    .line 2743974
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jZ;

    .line 2743975
    iget-object v2, v0, LX/6jZ;->attributionInfo:LX/6ja;

    if-eqz v2, :cond_0

    .line 2743976
    iget-object v1, v0, LX/6jZ;->fbid:Ljava/lang/Long;

    iget-object v0, v0, LX/6jZ;->attributionInfo:LX/6ja;

    .line 2743977
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v2

    iget-object v3, v0, LX/6ja;->visibility:LX/6jU;

    iget-object v3, v3, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2743978
    iput-boolean v3, v2, LX/5dZ;->a:Z

    .line 2743979
    move-object v2, v2

    .line 2743980
    iget-object v3, v0, LX/6ja;->visibility:LX/6jU;

    iget-object v3, v3, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2743981
    iput-boolean v3, v2, LX/5dZ;->c:Z

    .line 2743982
    move-object v2, v2

    .line 2743983
    iget-object v3, v0, LX/6ja;->visibility:LX/6jU;

    iget-object v3, v3, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2743984
    iput-boolean v3, v2, LX/5dZ;->d:Z

    .line 2743985
    move-object v2, v2

    .line 2743986
    iget-object v3, v0, LX/6ja;->visibility:LX/6jU;

    iget-object v3, v3, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2743987
    iput-boolean v3, v2, LX/5dZ;->e:Z

    .line 2743988
    move-object v2, v2

    .line 2743989
    invoke-virtual {v2}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v2

    .line 2743990
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2743991
    iput-object v4, v3, LX/5dd;->a:Ljava/lang/String;

    .line 2743992
    move-object v3, v3

    .line 2743993
    iget-object v4, v0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2743994
    iput-object v4, v3, LX/5dd;->b:Ljava/lang/String;

    .line 2743995
    move-object v3, v3

    .line 2743996
    iget-object v4, v0, LX/6ja;->attributionAppName:Ljava/lang/String;

    .line 2743997
    iput-object v4, v3, LX/5dd;->c:Ljava/lang/String;

    .line 2743998
    move-object v3, v3

    .line 2743999
    iget-object v4, v0, LX/6ja;->androidPackageName:Ljava/lang/String;

    .line 2744000
    iput-object v4, v3, LX/5dd;->e:Ljava/lang/String;

    .line 2744001
    move-object v3, v3

    .line 2744002
    iget-object v4, v0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    .line 2744003
    iput-object v4, v3, LX/5dd;->f:Ljava/lang/String;

    .line 2744004
    move-object v3, v3

    .line 2744005
    iget-object v4, v0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-nez v4, :cond_2

    .line 2744006
    sget-object v4, LX/0Rg;->a:LX/0Rg;

    move-object v4, v4

    .line 2744007
    :goto_0
    move-object v4, v4

    .line 2744008
    invoke-virtual {v3, v4}, LX/5dd;->a(Ljava/util/Map;)LX/5dd;

    move-result-object v3

    iget-object v4, v0, LX/6ja;->attributionType:Ljava/lang/Long;

    .line 2744009
    if-eqz v4, :cond_4

    .line 2744010
    invoke-virtual {v4}, Ljava/lang/Long;->intValue()I

    move-result p0

    invoke-static {p0}, LX/5dc;->fromValue(I)LX/5dc;

    move-result-object p0

    .line 2744011
    :goto_1
    move-object v4, p0

    .line 2744012
    iput-object v4, v3, LX/5dd;->g:LX/5dc;

    .line 2744013
    move-object v3, v3

    .line 2744014
    iput-object v2, v3, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 2744015
    move-object v2, v3

    .line 2744016
    iget-object v3, v0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    .line 2744017
    iput-object v3, v2, LX/5dd;->h:Ljava/lang/String;

    .line 2744018
    move-object v2, v2

    .line 2744019
    invoke-virtual {v2}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v2

    move-object v0, v2

    .line 2744020
    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 2744021
    :cond_2
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object p0

    .line 2744022
    iget-object v4, v0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 2744023
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_3

    .line 2744024
    :cond_3
    invoke-virtual {p0}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    goto :goto_0

    :cond_4
    sget-object p0, LX/5dc;->UNRECOGNIZED:LX/5dc;

    goto :goto_1
.end method

.method public static a(LX/6jg;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;
    .locals 13

    .prologue
    .line 2744025
    iget-object v0, p0, LX/6jg;->type:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-result-object v0

    .line 2744026
    invoke-static {}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->newBuilder()LX/6ev;

    move-result-object v1

    .line 2744027
    iput-object v0, v1, LX/6ev;->a:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 2744028
    move-object v1, v1

    .line 2744029
    iget-object v2, p0, LX/6jg;->untypedData:Ljava/util/Map;

    if-eqz v2, :cond_18

    .line 2744030
    iget-object v2, p0, LX/6jg;->untypedData:Ljava/util/Map;

    .line 2744031
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_17

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744032
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 2744033
    const-string v6, "theme_color"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2744034
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/6ev;->a(Ljava/lang/String;)LX/6ev;

    goto :goto_0

    .line 2744035
    :cond_1
    const-string v6, "thread_icon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2744036
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744037
    iput-object v3, v1, LX/6ev;->c:Ljava/lang/String;

    .line 2744038
    goto :goto_0

    .line 2744039
    :cond_2
    const-string v6, "nickname"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2744040
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744041
    iput-object v3, v1, LX/6ev;->d:Ljava/lang/String;

    .line 2744042
    goto :goto_0

    .line 2744043
    :cond_3
    const-string v6, "participant_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2744044
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744045
    iput-object v3, v1, LX/6ev;->e:Ljava/lang/String;

    .line 2744046
    goto :goto_0

    .line 2744047
    :cond_4
    const-string v6, "ttl"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2744048
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 2744049
    iput v3, v1, LX/6ev;->f:I

    .line 2744050
    goto :goto_0

    .line 2744051
    :cond_5
    const-string v6, "color_choices"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2744052
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744053
    invoke-static {v3}, LX/6ev;->l(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 2744054
    iput-object v3, v1, LX/6ev;->g:LX/0Px;

    .line 2744055
    goto :goto_0

    .line 2744056
    :cond_6
    const-string v6, "emoji_choices"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2744057
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744058
    invoke-static {v3}, LX/6ev;->l(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 2744059
    iput-object v3, v1, LX/6ev;->h:LX/0Px;

    .line 2744060
    goto/16 :goto_0

    .line 2744061
    :cond_7
    const-string v6, "nickname_choices"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2744062
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744063
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2744064
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2744065
    invoke-virtual {v6}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v8

    .line 2744066
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 2744067
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2744068
    invoke-virtual {v6, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 2744069
    invoke-static {v9}, LX/16N;->b(Lorg/json/JSONArray;)LX/0Px;

    move-result-object v9

    .line 2744070
    new-instance v10, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;

    invoke-direct {v10, v5, v9}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v7, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2744071
    :catch_0
    const/4 v5, 0x0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v3, v5

    .line 2744072
    iput-object v3, v1, LX/6ev;->i:LX/0Px;

    .line 2744073
    goto/16 :goto_0

    .line 2744074
    :cond_8
    const-string v6, "bot_choices"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2744075
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744076
    invoke-static {v3}, LX/6ev;->n(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 2744077
    iput-object v3, v1, LX/6ev;->j:LX/0Px;

    .line 2744078
    goto/16 :goto_0

    .line 2744079
    :cond_9
    const-string v6, "event"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2744080
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744081
    iput-object v3, v1, LX/6ev;->k:Ljava/lang/String;

    .line 2744082
    goto/16 :goto_0

    .line 2744083
    :cond_a
    const-string v6, "server_info_data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2744084
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744085
    iput-object v3, v1, LX/6ev;->l:Ljava/lang/String;

    .line 2744086
    goto/16 :goto_0

    .line 2744087
    :cond_b
    const-string v6, "group_call_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2744088
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744089
    if-eqz v3, :cond_c

    const-string v5, "1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    :cond_c
    const/4 v3, 0x1

    .line 2744090
    :goto_3
    iput-boolean v3, v1, LX/6ev;->m:Z

    .line 2744091
    goto/16 :goto_0

    :cond_d
    const/4 v3, 0x0

    goto :goto_3

    .line 2744092
    :cond_e
    const-string v6, "is_video_call"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2744093
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 2744094
    iput-boolean v3, v1, LX/6ev;->m:Z

    .line 2744095
    goto/16 :goto_0

    .line 2744096
    :cond_f
    const-string v6, "ride_provider"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 2744097
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744098
    :try_start_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2744099
    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    .line 2744100
    :goto_4
    move-object v3, v5

    .line 2744101
    iput-object v3, v1, LX/6ev;->n:Ljava/lang/String;

    .line 2744102
    goto/16 :goto_0

    .line 2744103
    :cond_10
    const-string v6, "game_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 2744104
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744105
    iput-object v3, v1, LX/6ev;->p:Ljava/lang/String;

    .line 2744106
    goto/16 :goto_0

    .line 2744107
    :cond_11
    const-string v6, "score"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 2744108
    :try_start_2
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 2744109
    iput v3, v1, LX/6ev;->q:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2744110
    goto/16 :goto_0

    .line 2744111
    :catch_1
    goto/16 :goto_0

    .line 2744112
    :cond_12
    const-string v6, "new_high_score"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 2744113
    const-string v5, "1"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2744114
    iput-boolean v3, v1, LX/6ev;->r:Z

    .line 2744115
    goto/16 :goto_0

    .line 2744116
    :cond_13
    const-string v6, "joinable_event"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 2744117
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/6ev;->i(Ljava/lang/String;)LX/6ev;

    goto/16 :goto_0

    .line 2744118
    :cond_14
    const-string v6, "thread_joinable_promotion_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 2744119
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 2744120
    iput-boolean v3, v1, LX/6ev;->u:Z

    .line 2744121
    goto/16 :goto_0

    .line 2744122
    :cond_15
    const-string v6, "agent_intent_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 2744123
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744124
    iput-object v3, v1, LX/6ev;->v:Ljava/lang/String;

    .line 2744125
    goto/16 :goto_0

    .line 2744126
    :cond_16
    const-string v6, "request_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2744127
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2744128
    iput-object v3, v1, LX/6ev;->w:Ljava/lang/String;

    .line 2744129
    goto/16 :goto_0

    .line 2744130
    :cond_17
    invoke-static {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;)LX/6eo;

    move-result-object v3

    .line 2744131
    if-nez v3, :cond_1a

    .line 2744132
    const/4 v3, 0x0

    .line 2744133
    :goto_5
    move-object v3, v3

    .line 2744134
    iput-object v3, v1, LX/6ev;->x:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    .line 2744135
    const-string v3, "event_id"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "event_type"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "event_time"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "event_seconds_to_notify_before"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v7, "event_title"

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "event_creator_id"

    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v9, "guest_id"

    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v10, "guest_status"

    invoke-interface {v2, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v11, "event_track_rsvp"

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string v12, "event_location_name"

    invoke-interface {v2, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static/range {v3 .. v12}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    move-result-object v3

    .line 2744136
    iput-object v3, v1, LX/6ev;->s:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    .line 2744137
    const-string v3, "ad_preferences_url"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "ad_properties"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    move-result-object v3

    .line 2744138
    iput-object v3, v1, LX/6ev;->o:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    .line 2744139
    :cond_18
    invoke-virtual {v1}, LX/6ev;->a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v0

    return-object v0

    .line 2744140
    :cond_19
    :try_start_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v5

    goto/16 :goto_2

    :catch_2
    const/4 v5, 0x0

    goto/16 :goto_4

    :cond_1a
    invoke-interface {v3, v2}, LX/6eo;->a(Ljava/util/Map;)Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    move-result-object v3

    goto/16 :goto_5
.end method

.method private static a(LX/Jrb;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kn;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;ZLjava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;
    .locals 6
    .param p2    # LX/6kn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/6kn;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;",
            "Ljava/lang/Integer;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/Message;"
        }
    .end annotation

    .prologue
    .line 2743880
    invoke-static {p0, p2, p1}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v0

    invoke-static {p3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2743881
    iput-object v1, v0, LX/6f7;->f:Ljava/lang/String;

    .line 2743882
    move-object v3, v0

    .line 2743883
    if-eqz p4, :cond_0

    .line 2743884
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 2743885
    iput-object v0, v3, LX/6f7;->k:Ljava/lang/String;

    .line 2743886
    :cond_0
    if-eqz p5, :cond_1

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2743887
    invoke-static {p0, p5, v3}, LX/Jrb;->a(LX/Jrb;Ljava/util/List;LX/6f7;)V

    .line 2743888
    iget-object v0, p2, LX/6kn;->messageId:Ljava/lang/String;

    invoke-static {p0, p5, v0}, LX/Jrb;->a(LX/Jrb;Ljava/util/List;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 2743889
    iput-object v0, v3, LX/6f7;->i:Ljava/util/List;

    .line 2743890
    :cond_1
    iget-object v0, v3, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-object v0, v0

    .line 2743891
    if-nez v0, :cond_2

    .line 2743892
    invoke-direct {p0, p5}, LX/Jrb;->a(Ljava/util/List;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v0

    .line 2743893
    iput-object v0, v3, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 2743894
    :cond_2
    if-eqz p6, :cond_3

    .line 2743895
    iput-object p6, v3, LX/6f7;->J:Ljava/lang/Integer;

    .line 2743896
    :cond_3
    iput-boolean p7, v3, LX/6f7;->M:Z

    .line 2743897
    iput-object p8, v3, LX/6f7;->N:Ljava/lang/String;

    .line 2743898
    if-eqz p9, :cond_b

    .line 2743899
    const-string v0, "customization"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743900
    if-eqz v0, :cond_4

    .line 2743901
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2743902
    const-string v0, "border"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "flowers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2743903
    iput-boolean v0, v3, LX/6f7;->O:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2743904
    :cond_4
    :goto_0
    const-string v0, "montage_reply_data"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743905
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2743906
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2743907
    const-string v0, "message_id"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2743908
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2743909
    iput-object v0, v3, LX/6f7;->P:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2743910
    :cond_5
    :goto_1
    const-string v0, "message_source_data"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743911
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2743912
    :try_start_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2743913
    const-string v0, "message_source"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2743914
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2743915
    iput-object v0, v3, LX/6f7;->p:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2743916
    :cond_6
    :goto_2
    const-string v0, "meta_ranges"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743917
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2743918
    iget-object v1, p0, LX/Jrb;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    iget-object v2, p0, LX/Jrb;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v1, v2, v0}, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a(LX/0lC;LX/03V;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/6f7;->e(Ljava/util/List;)LX/6f7;

    .line 2743919
    :cond_7
    const-string v0, "prng"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743920
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2743921
    iget-object v1, p0, LX/Jrb;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-static {v1, v0}, Lcom/facebook/messaging/model/messages/ProfileRange;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    .line 2743922
    iget-object v1, p0, LX/Jrb;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-static {v1, v0}, Lcom/facebook/messaging/model/messages/ProfileRange;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 2743923
    iput-object v0, v3, LX/6f7;->V:LX/0Px;

    .line 2743924
    :cond_8
    const-string v0, "platform_xmd"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743925
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2743926
    iget-object v1, p0, LX/Jrb;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 2743927
    iget-object v1, p0, LX/Jrb;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    iget-object v2, p0, LX/Jrb;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-static {v1, v2, v0}, LX/5dt;->a(LX/03V;LX/0lC;Ljava/lang/String;)LX/0P1;

    move-result-object v1

    .line 2743928
    invoke-virtual {v3, v1}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    .line 2743929
    const/4 v2, 0x0

    .line 2743930
    :try_start_3
    iget-object v1, p0, LX/Jrb;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    .line 2743931
    :goto_3
    if-eqz v0, :cond_9

    .line 2743932
    const-string v1, "m_directives"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2743933
    if-eqz v0, :cond_9

    .line 2743934
    iget-object v1, p0, LX/Jrb;->m:LX/Jol;

    invoke-virtual {v1, v0}, LX/Jol;->a(LX/0lF;)V

    .line 2743935
    :cond_9
    const-string v0, "agent_quick_replies"

    invoke-interface {p9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2743936
    iget-object v1, p0, LX/Jrb;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    .line 2743937
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2743938
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2743939
    :goto_4
    move-object v0, v2

    .line 2743940
    if-eqz v0, :cond_b

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 2743941
    new-instance v1, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;-><init>(LX/0Px;)V

    .line 2743942
    iget-object v0, v3, LX/6f7;->R:Ljava/util/Map;

    move-object v0, v0

    .line 2743943
    if-nez v0, :cond_a

    .line 2743944
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2743945
    :cond_a
    sget-object v2, LX/5ds;->QUICK_REPLIES:LX/5ds;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2743946
    invoke-virtual {v3, v0}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    .line 2743947
    :cond_b
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2743948
    const/4 v5, 0x0

    .line 2743949
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v1, v1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    const-string v2, "1541184119468976"

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2743950
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    .line 2743951
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 2743952
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 2743953
    iget-object v2, v1, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz v2, :cond_c

    .line 2743954
    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    if-nez v1, :cond_c

    .line 2743955
    const-string v1, "MessageFromDeltaFactory"

    const-string v2, "Message %s from app \'%s\' is missing animated images"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v5, v5, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2743956
    :cond_c
    return-object v0

    .line 2743957
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2743958
    iget-object v0, p0, LX/Jrb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "MessageFromDeltaFactory"

    const-string v4, "Error parsing montage reply data"

    invoke-virtual {v0, v2, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 2743959
    :catch_1
    move-exception v0

    .line 2743960
    const-string v1, "MessageFromDeltaFactory"

    const-string v2, "Error parsing message source data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 2743961
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 2743962
    iget-object v0, p0, LX/Jrb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "MessageFromDeltaFactory"

    const-string v5, "Error parsing directives"

    invoke-virtual {v0, v4, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto/16 :goto_3

    :catch_3
    goto/16 :goto_0

    .line 2743963
    :cond_d
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2743964
    :try_start_4
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 2743965
    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result p0

    if-ge v2, p0, :cond_f

    .line 2743966
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object p0

    sget-object p1, LX/5dw;->M:LX/5dw;

    iget-object p1, p1, LX/5dw;->dbValue:Ljava/lang/String;

    const/4 p2, 0x0

    const/4 p3, 0x0

    const/4 p4, 0x0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    move-result-object p0

    .line 2743967
    if-eqz p0, :cond_e

    .line 2743968
    invoke-virtual {v4, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    .line 2743969
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 2743970
    :catch_4
    move-exception v2

    .line 2743971
    const-string v5, "QuickReplyItem"

    const-string p0, "Exception thrown when converting from M quick reply"

    invoke-virtual {v1, v5, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2743972
    :cond_f
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_4
.end method

.method public static a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 3

    .prologue
    .line 2743874
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/2Ou;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 2743875
    if-eqz v0, :cond_1

    .line 2743876
    :cond_0
    return-object v0

    .line 2743877
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/2Ou;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 2743878
    if-nez v0, :cond_0

    .line 2743879
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not contain participant with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/Jrb;Ljava/util/List;LX/6f7;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;",
            "LX/6f7;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2743869
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jZ;

    .line 2743870
    iget-object v2, v0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2743871
    iget-object v1, p0, LX/Jrb;->g:LX/2Ns;

    iget-object v0, v0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/2Ns;->b(Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    .line 2743872
    iget-object v1, p0, LX/Jrb;->h:LX/3Mz;

    invoke-virtual {v1, v0, p2}, LX/3Mz;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;LX/6f7;)V

    .line 2743873
    :cond_1
    return-void
.end method

.method private static b(LX/0QB;)LX/Jrb;
    .locals 14

    .prologue
    .line 2743867
    new-instance v0, LX/Jrb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v1

    check-cast v1, LX/Jrc;

    invoke-static {p0}, LX/3QO;->a(LX/0QB;)LX/3QO;

    move-result-object v2

    check-cast v2, LX/3QO;

    const/16 v3, 0x12ce

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/7GF;->a(LX/0QB;)LX/7GF;

    move-result-object v4

    check-cast v4, LX/7GF;

    invoke-static {p0}, LX/7G3;->a(LX/0QB;)LX/7G3;

    move-result-object v5

    check-cast v5, LX/7G3;

    invoke-static {p0}, LX/3QS;->a(LX/0QB;)LX/3QS;

    move-result-object v6

    check-cast v6, LX/3QS;

    invoke-static {p0}, LX/2Ns;->b(LX/0QB;)LX/2Ns;

    move-result-object v7

    check-cast v7, LX/2Ns;

    invoke-static {p0}, LX/3Mz;->b(LX/0QB;)LX/3Mz;

    move-result-object v8

    check-cast v8, LX/3Mz;

    invoke-static {p0}, LX/3Le;->a(LX/0QB;)LX/3Le;

    move-result-object v9

    check-cast v9, LX/3Le;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v10

    check-cast v10, LX/2Mk;

    const/16 v11, 0x2ba

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x259

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/Jol;->b(LX/0QB;)LX/Jol;

    move-result-object v13

    check-cast v13, LX/Jol;

    invoke-direct/range {v0 .. v13}, LX/Jrb;-><init>(LX/Jrc;LX/3QO;LX/0Or;LX/7GF;LX/7G3;LX/3QS;LX/2Ns;LX/3Mz;LX/3Le;LX/2Mk;LX/0Ot;LX/0Ot;LX/Jol;)V

    .line 2743868
    return-object v0
.end method


# virtual methods
.method public final a(LX/6jk;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2743864
    iget-object v3, p1, LX/6jk;->body:Ljava/lang/String;

    iget-object v4, p1, LX/6jk;->stickerId:Ljava/lang/Long;

    iget-object v5, p1, LX/6jk;->attachments:Ljava/util/List;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    move-object v8, v6

    move-object v9, v6

    invoke-static/range {v0 .. v9}, LX/Jrb;->a(LX/Jrb;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kn;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;ZLjava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2743865
    iget-object v1, p0, LX/Jrb;->f:LX/3QS;

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_BROADACST_DELTA:LX/6fK;

    invoke-virtual {v1, v2, v0}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2743866
    return-object v0
.end method

.method public final a(LX/6k4;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;
    .locals 10

    .prologue
    .line 2743857
    iget-object v0, p1, LX/6k4;->data:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/6k4;->data:Ljava/util/Map;

    const-string v1, "is_sponsored"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    .line 2743858
    :goto_0
    iget-object v0, p1, LX/6k4;->data:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/6k4;->data:Ljava/util/Map;

    const-string v1, "commerce_message_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v8, v0

    .line 2743859
    :goto_1
    iget-object v2, p1, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6k4;->body:Ljava/lang/String;

    iget-object v4, p1, LX/6k4;->stickerId:Ljava/lang/Long;

    iget-object v5, p1, LX/6k4;->attachments:Ljava/util/List;

    iget-object v6, p1, LX/6k4;->ttl:Ljava/lang/Integer;

    iget-object v9, p1, LX/6k4;->data:Ljava/util/Map;

    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v9}, LX/Jrb;->a(LX/Jrb;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kn;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;ZLjava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2743860
    iget-object v1, p0, LX/Jrb;->f:LX/3QS;

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_NEW_MESSAGE_DELTA:LX/6fK;

    invoke-virtual {v1, v2, v0}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2743861
    return-object v0

    .line 2743862
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 2743863
    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public final a(LX/6k9;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 2743842
    iget-object v0, p1, LX/6k9;->messageMetadata:LX/6kn;

    invoke-static {p0, v0, p2}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v0

    .line 2743843
    iget-object v1, p1, LX/6k9;->messageType:Ljava/lang/Integer;

    .line 2743844
    if-nez v1, :cond_0

    .line 2743845
    sget-object v4, LX/2uW;->P2P_PAYMENT:LX/2uW;

    .line 2743846
    :goto_0
    move-object v1, v4

    .line 2743847
    iput-object v1, v0, LX/6f7;->l:LX/2uW;

    .line 2743848
    move-object v8, v0

    .line 2743849
    new-instance v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    iget-object v1, p1, LX/6k9;->transferId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-wide v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/payment/PaymentTransactionData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    .line 2743850
    iput-object v0, v8, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 2743851
    move-object v0, v8

    .line 2743852
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0

    .line 2743853
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2743854
    sget-object v4, LX/2uW;->P2P_PAYMENT:LX/2uW;

    goto :goto_0

    .line 2743855
    :pswitch_0
    sget-object v4, LX/2uW;->P2P_PAYMENT_GROUP:LX/2uW;

    goto :goto_0

    .line 2743856
    :pswitch_1
    sget-object v4, LX/2uW;->P2P_PAYMENT_CANCELED:LX/2uW;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/6kG;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;
    .locals 17

    .prologue
    .line 2743831
    move-object/from16 v0, p1

    iget-object v2, v0, LX/6kG;->messageMetadata:LX/6kn;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v2, v1}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v2

    .line 2743832
    invoke-static/range {p1 .. p1}, LX/6hU;->b(LX/6kG;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v3

    .line 2743833
    invoke-static/range {p1 .. p1}, LX/6hU;->a(LX/6kG;)LX/6hT;

    move-result-object v14

    .line 2743834
    new-instance v4, LX/6hT;

    invoke-direct {v4}, LX/6hT;-><init>()V

    .line 2743835
    sget-object v4, LX/2uW;->CALL_LOG:LX/2uW;

    invoke-virtual {v2, v4}, LX/6f7;->a(LX/2uW;)LX/6f7;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/6f7;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6f7;

    .line 2743836
    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2743837
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Jrb;->f:LX/3QS;

    sget-object v4, LX/6fK;->SYNC_PROTOCOL_RTC_EVENT_LOG_DELTA:LX/6fK;

    invoke-virtual {v3, v4, v2}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2743838
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Jrb;->j:LX/2Mk;

    invoke-virtual {v3, v2}, LX/2Mk;->s(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v11

    .line 2743839
    iget-object v3, v14, LX/6hT;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 2743840
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Jrb;->i:LX/3Le;

    iget-object v4, v14, LX/6hT;->a:Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, v14, LX/6hT;->b:J

    iget-wide v8, v14, LX/6hT;->c:J

    iget-object v10, v14, LX/6hT;->d:LX/03R;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, LX/03R;->asBoolean(Z)Z

    move-result v10

    iget-object v12, v14, LX/6hT;->f:LX/03R;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, LX/03R;->asBoolean(Z)Z

    move-result v12

    const/4 v13, 0x0

    iget-object v14, v14, LX/6hT;->g:LX/03R;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, LX/03R;->asBoolean(Z)Z

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v3 .. v16}, LX/3Le;->a(Ljava/lang/String;Ljava/lang/String;JJZZZZZZZ)V

    .line 2743841
    :cond_0
    return-object v2
.end method
