.class public final LX/JWL;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

.field public final synthetic c:LX/2dx;

.field public final synthetic d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;LX/0Px;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;LX/2dx;)V
    .locals 0

    .prologue
    .line 2702756
    iput-object p1, p0, LX/JWL;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    iput-object p2, p0, LX/JWL;->a:LX/0Px;

    iput-object p3, p0, LX/JWL;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iput-object p4, p0, LX/JWL;->c:LX/2dx;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2702757
    iget-object v0, p0, LX/JWL;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/JWL;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 2702758
    iget-object v3, p0, LX/JWL;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;

    new-instance v4, LX/JWO;

    iget-object v5, p0, LX/JWL;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iget-object v6, p0, LX/JWL;->c:LX/2dx;

    invoke-direct {v4, v5, v0, v6}, LX/JWO;-><init>(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;LX/2dx;)V

    invoke-virtual {p1, v3, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2702759
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2702760
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2702761
    iget-object v0, p0, LX/JWL;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->b:LX/1LV;

    iget-object v1, p0, LX/JWL;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2702762
    iget-object v0, p0, LX/JWL;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iget-object v1, p0, LX/JWL;->a:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 2702763
    iget-object v0, p0, LX/JWL;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->c:LX/JWD;

    iget-object v1, p0, LX/JWL;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0, v1}, LX/JWD;->c(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    .line 2702764
    return-void
.end method
