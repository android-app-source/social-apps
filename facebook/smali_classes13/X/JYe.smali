.class public final LX/JYe;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JYf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JYf;


# direct methods
.method public constructor <init>(LX/JYf;)V
    .locals 1

    .prologue
    .line 2706792
    iput-object p1, p0, LX/JYe;->b:LX/JYf;

    .line 2706793
    move-object v0, p1

    .line 2706794
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2706795
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2706796
    const-string v0, "FundraiserAttachmentFacepileComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2706797
    if-ne p0, p1, :cond_1

    .line 2706798
    :cond_0
    :goto_0
    return v0

    .line 2706799
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2706800
    goto :goto_0

    .line 2706801
    :cond_3
    check-cast p1, LX/JYe;

    .line 2706802
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2706803
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2706804
    if-eq v2, v3, :cond_0

    .line 2706805
    iget-object v2, p0, LX/JYe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JYe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JYe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2706806
    goto :goto_0

    .line 2706807
    :cond_4
    iget-object v2, p1, LX/JYe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
