.class public LX/K7A;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/K7A;


# instance fields
.field public a:LX/K79;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2772755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772756
    return-void
.end method

.method public static a(LX/0QB;)LX/K7A;
    .locals 3

    .prologue
    .line 2772757
    sget-object v0, LX/K7A;->b:LX/K7A;

    if-nez v0, :cond_1

    .line 2772758
    const-class v1, LX/K7A;

    monitor-enter v1

    .line 2772759
    :try_start_0
    sget-object v0, LX/K7A;->b:LX/K7A;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2772760
    if-eqz v2, :cond_0

    .line 2772761
    :try_start_1
    new-instance v0, LX/K7A;

    invoke-direct {v0}, LX/K7A;-><init>()V

    .line 2772762
    move-object v0, v0

    .line 2772763
    sput-object v0, LX/K7A;->b:LX/K7A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2772764
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2772765
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2772766
    :cond_1
    sget-object v0, LX/K7A;->b:LX/K7A;

    return-object v0

    .line 2772767
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2772768
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
