.class public final LX/Jub;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

.field public final synthetic b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic c:J

.field public final synthetic d:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/notify/CalleeReadyNotification;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 0

    .prologue
    .line 2749019
    iput-object p1, p0, LX/Jub;->d:LX/3RG;

    iput-object p2, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iput-object p3, p0, LX/Jub;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-wide p4, p0, LX/Jub;->c:J

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 9
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 2748990
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_2

    const v0, 0x7f021abe

    .line 2748991
    :goto_0
    iget-object v1, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-boolean v1, v1, Lcom/facebook/messaging/notify/CalleeReadyNotification;->d:Z

    if-eqz v1, :cond_3

    .line 2748992
    const v0, 0x7f021ad3

    move v7, v0

    .line 2748993
    :goto_1
    iget-object v0, p0, LX/Jub;->d:LX/3RG;

    iget-object v1, p0, LX/Jub;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, p0, LX/Jub;->c:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, LX/3RG;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2748994
    iget-object v1, p0, LX/Jub;->d:LX/3RG;

    iget-wide v2, p0, LX/Jub;->c:J

    iget-object v4, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-object v4, v4, Lcom/facebook/messaging/notify/CalleeReadyNotification;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, LX/3RG;->a(JLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2748995
    iget-object v2, p0, LX/Jub;->d:LX/3RG;

    iget-wide v4, p0, LX/Jub;->c:J

    iget-object v3, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-boolean v3, v3, Lcom/facebook/messaging/notify/CalleeReadyNotification;->d:Z

    iget-object v8, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-object v8, v8, Lcom/facebook/messaging/notify/CalleeReadyNotification;->f:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3, v8}, LX/3RG;->a(JZLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 2748996
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/Jub;->d:LX/3RG;

    iget-object v4, v4, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-object v4, v4, Lcom/facebook/messaging/notify/CalleeReadyNotification;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-object v4, v4, Lcom/facebook/messaging/notify/CalleeReadyNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/2HB;->a(I)LX/2HB;

    move-result-object v3

    .line 2748997
    iput-object v0, v3, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748998
    move-object v3, v3

    .line 2748999
    invoke-virtual {v3, v1}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v1

    const/4 v3, 0x2

    .line 2749000
    iput v3, v1, LX/2HB;->j:I

    .line 2749001
    move-object v1, v1

    .line 2749002
    iget-object v3, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-wide v4, v3, Lcom/facebook/messaging/notify/CalleeReadyNotification;->e:J

    invoke-virtual {v1, v4, v5}, LX/2HB;->a(J)LX/2HB;

    move-result-object v1

    iget-object v3, p0, LX/Jub;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a02f6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2749003
    iput v3, v1, LX/2HB;->y:I

    .line 2749004
    move-object v1, v1

    .line 2749005
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v4

    .line 2749006
    if-eqz p1, :cond_0

    .line 2749007
    iput-object p1, v4, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2749008
    :cond_0
    iget-object v1, p0, LX/Jub;->d:LX/3RG;

    iget-object v1, v1, LX/3RG;->z:LX/00H;

    .line 2749009
    iget-object v3, v1, LX/00H;->j:LX/01T;

    move-object v1, v3

    .line 2749010
    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-ne v1, v3, :cond_1

    .line 2749011
    invoke-static {}, LX/10A;->a()I

    move-result v1

    iget-object v3, p0, LX/Jub;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    const v5, 0x7f08079e

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v1, v3, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2749012
    iget-object v0, p0, LX/Jub;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->b:Landroid/content/Context;

    const v1, 0x7f08075f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v7, v0, v2}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2749013
    :cond_1
    iget-object v0, p0, LX/Jub;->d:LX/3RG;

    const v1, 0x7f070070

    invoke-static {v0, v1}, LX/3RG;->a$redex0(LX/3RG;I)Landroid/net/Uri;

    move-result-object v8

    .line 2749014
    iget-object v0, p0, LX/Jub;->d:LX/3RG;

    iget-object v3, v0, LX/3RG;->f:LX/3RQ;

    new-instance v5, LX/Dhq;

    invoke-direct {v5}, LX/Dhq;-><init>()V

    iget-object v7, p0, LX/Jub;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual/range {v3 .. v8}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/net/Uri;)V

    .line 2749015
    iget-object v0, p0, LX/Jub;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->d:LX/3RK;

    iget-object v1, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/CalleeReadyNotification;->c:Ljava/lang/String;

    const/16 v2, 0x2729

    invoke-virtual {v4}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2749016
    iget-object v0, p0, LX/Jub;->a:Lcom/facebook/messaging/notify/CalleeReadyNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2749017
    return-void

    .line 2749018
    :cond_2
    const v0, 0x7f021ab9

    goto/16 :goto_0

    :cond_3
    move v7, v0

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748986
    invoke-direct {p0, p1}, LX/Jub;->b(Landroid/graphics/Bitmap;)V

    .line 2748987
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2748988
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Jub;->b(Landroid/graphics/Bitmap;)V

    .line 2748989
    return-void
.end method
