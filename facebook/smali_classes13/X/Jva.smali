.class public LX/Jva;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89S;


# instance fields
.field public final a:LX/1Kf;

.field public final b:LX/74n;

.field public final c:Landroid/app/Activity;

.field public final d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Kf;LX/74n;Landroid/app/Activity;Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;)V
    .locals 0
    .param p3    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2750555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750556
    iput-object p1, p0, LX/Jva;->a:LX/1Kf;

    .line 2750557
    iput-object p2, p0, LX/Jva;->b:LX/74n;

    .line 2750558
    iput-object p3, p0, LX/Jva;->c:Landroid/app/Activity;

    .line 2750559
    iput-object p4, p0, LX/Jva;->d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750560
    iput-object p5, p0, LX/Jva;->e:Ljava/lang/String;

    .line 2750561
    return-void
.end method

.method private a(Lcom/facebook/ipc/creativecam/CreativeCamResult;)V
    .locals 4

    .prologue
    .line 2750603
    iget-object v0, p0, LX/Jva;->c:Landroid/app/Activity;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "creative_cam_result_extra"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2750604
    iget-object v0, p0, LX/Jva;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2750605
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/89R;)V
    .locals 5

    .prologue
    .line 2750569
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2750570
    iget-object v3, p2, LX/89R;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 2750571
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2750572
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2750573
    :cond_0
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    .line 2750574
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFramePacks(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 2750575
    iget-object v1, p2, LX/89R;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFilterName(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 2750576
    iget-object v1, p2, LX/89R;->d:LX/0Px;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 2750577
    iget-object v1, p0, LX/Jva;->d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750578
    iget-object v2, v1, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v2

    .line 2750579
    if-eqz v1, :cond_2

    .line 2750580
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    const/4 v3, 0x1

    .line 2750581
    iget-object v1, p0, LX/Jva;->b:LX/74n;

    sget-object v2, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v1, p1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 2750582
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v1

    .line 2750583
    iput-object v0, v1, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2750584
    move-object v1, v1

    .line 2750585
    invoke-virtual {v1}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 2750586
    iget-object v2, p0, LX/Jva;->d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750587
    iget-object v4, v2, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v2, v4

    .line 2750588
    invoke-static {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2750589
    iget-object v2, p0, LX/Jva;->d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750590
    iget-object v3, v2, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v2, v3

    .line 2750591
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2750592
    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2750593
    :cond_1
    iget-object v2, p0, LX/Jva;->a:LX/1Kf;

    iget-object v3, p0, LX/Jva;->e:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/4 v4, 0x0

    iget-object p2, p0, LX/Jva;->c:Landroid/app/Activity;

    invoke-interface {v2, v3, v1, v4, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2750594
    :goto_1
    return-void

    .line 2750595
    :cond_2
    new-instance v1, LX/89Y;

    invoke-direct {v1}, LX/89Y;-><init>()V

    .line 2750596
    iput-object p1, v1, LX/89Y;->b:Landroid/net/Uri;

    .line 2750597
    move-object v1, v1

    .line 2750598
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 2750599
    iput-object v0, v1, LX/89Y;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2750600
    move-object v0, v1

    .line 2750601
    invoke-virtual {v0}, LX/89Y;->a()Lcom/facebook/ipc/creativecam/CreativeCamResult;

    move-result-object v0

    .line 2750602
    invoke-direct {p0, v0}, LX/Jva;->a(Lcom/facebook/ipc/creativecam/CreativeCamResult;)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2750568
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750567
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;LX/89R;)V
    .locals 4

    .prologue
    .line 2750606
    iget-object v0, p0, LX/Jva;->d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750607
    iget-object p2, v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, p2

    .line 2750608
    if-eqz v0, :cond_0

    .line 2750609
    iget-object v0, p0, LX/Jva;->d:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750610
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v1

    .line 2750611
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/Jva;->b:LX/74n;

    invoke-static {v1, v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/74n;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2750612
    iget-object v1, p0, LX/Jva;->a:LX/1Kf;

    iget-object v2, p0, LX/Jva;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/4 v3, 0x0

    iget-object p2, p0, LX/Jva;->c:Landroid/app/Activity;

    invoke-interface {v1, v2, v0, v3, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2750613
    :goto_0
    return-void

    .line 2750614
    :cond_0
    new-instance v0, LX/89Y;

    invoke-direct {v0}, LX/89Y;-><init>()V

    .line 2750615
    iput-object p1, v0, LX/89Y;->a:Landroid/net/Uri;

    .line 2750616
    move-object v0, v0

    .line 2750617
    invoke-virtual {v0}, LX/89Y;->a()Lcom/facebook/ipc/creativecam/CreativeCamResult;

    move-result-object v0

    .line 2750618
    invoke-direct {p0, v0}, LX/Jva;->a(Lcom/facebook/ipc/creativecam/CreativeCamResult;)V

    goto :goto_0
.end method

.method public final c()LX/89c;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750566
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/89f;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750565
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/BFW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750564
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()LX/B4P;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750563
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()LX/89e;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750562
    const/4 v0, 0x0

    return-object v0
.end method
