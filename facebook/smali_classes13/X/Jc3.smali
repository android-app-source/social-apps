.class public final LX/Jc3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/settings/activity/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V
    .locals 0

    .prologue
    .line 2717379
    iput-object p1, p0, LX/Jc3;->a:Lcom/facebook/katana/settings/activity/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2717380
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "hi_res_photo_upload_setting_change"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "photo"

    .line 2717381
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2717382
    move-object v0, v0

    .line 2717383
    const-string v1, "state"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2717384
    iget-object v1, p0, LX/Jc3;->a:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/settings/activity/SettingsActivity;->j:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2717385
    const/4 v0, 0x1

    return v0
.end method
