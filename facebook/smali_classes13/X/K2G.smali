.class public final LX/K2G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V
    .locals 0

    .prologue
    .line 2763186
    iput-object p1, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2763171
    sget-object v0, LX/K2H;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2763172
    :goto_0
    return-void

    .line 2763173
    :pswitch_0
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const-string v1, "ufi_like_clicked"

    iget-object v2, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    iget-object v2, v2, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->O:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V

    .line 2763174
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-static {v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->k(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    .line 2763175
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    .line 2763176
    iput-boolean v3, v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->z:Z

    .line 2763177
    goto :goto_0

    .line 2763178
    :pswitch_1
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const-string v1, "ufi_comment_clicked"

    iget-object v2, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    iget-object v2, v2, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->O:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V

    .line 2763179
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-static {v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->n(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    .line 2763180
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-static {v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->l(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    .line 2763181
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    .line 2763182
    iput-boolean v3, v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->z:Z

    .line 2763183
    goto :goto_0

    .line 2763184
    :pswitch_2
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const-string v1, "ufi_share_clicked"

    iget-object v2, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    iget-object v2, v2, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->O:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V

    .line 2763185
    iget-object v0, p0, LX/K2G;->a:Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-static {v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->p(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
