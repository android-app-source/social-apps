.class public final LX/JtO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JtU;


# direct methods
.method public constructor <init>(LX/JtU;)V
    .locals 0

    .prologue
    .line 2746711
    iput-object p1, p0, LX/JtO;->a:LX/JtU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746712
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "Unable to retrieve MRU sticker ids"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746713
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746689
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2746690
    if-eqz p1, :cond_0

    .line 2746691
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2746692
    if-eqz v0, :cond_0

    .line 2746693
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2746694
    check-cast v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel;->a()Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2746695
    :cond_0
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "updateRecentlyUsedStickers: couldn\'t retrieve user stickers"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2746696
    :goto_0
    return-void

    .line 2746697
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2746698
    check-cast v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel;->a()Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel;->a()LX/2uF;

    move-result-object v0

    .line 2746699
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v3

    .line 2746700
    if-nez v3, :cond_2

    .line 2746701
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "updateRecentlyUsedStickers: no sticker retrieved"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2746702
    :cond_2
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    .line 2746703
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    .line 2746704
    :goto_1
    if-ge v1, v3, :cond_3

    .line 2746705
    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2746706
    const-class v7, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;

    invoke-virtual {v6, v0, v2, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/JtS;->a(Ljava/lang/String;)LX/JtS;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2746707
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2746708
    :cond_3
    new-array v0, v3, [LX/JtS;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 2746709
    iget-object v0, p0, LX/JtO;->a:LX/JtU;

    invoke-virtual {v0, v5}, LX/JtU;->a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2746710
    new-instance v1, LX/JtN;

    invoke-direct {v1, p0}, LX/JtN;-><init>(LX/JtO;)V

    iget-object v2, p0, LX/JtO;->a:LX/JtU;

    iget-object v2, v2, LX/JtU;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
