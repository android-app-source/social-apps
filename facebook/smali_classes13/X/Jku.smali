.class public final LX/Jku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jkv;


# direct methods
.method public constructor <init>(LX/Jkv;)V
    .locals 0

    .prologue
    .line 2730619
    iput-object p1, p0, LX/Jku;->a:LX/Jkv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2730620
    check-cast p1, Lcom/facebook/user/model/User;

    check-cast p2, Lcom/facebook/user/model/User;

    .line 2730621
    iget-object v0, p0, LX/Jku;->a:LX/Jkv;

    iget-object v0, v0, LX/Jkv;->b:LX/0P1;

    .line 2730622
    iget-object v1, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2730623
    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2730624
    iget-object v1, p0, LX/Jku;->a:LX/Jkv;

    iget-object v1, v1, LX/Jkv;->b:LX/0P1;

    .line 2730625
    iget-object v2, p2, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2730626
    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2730627
    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2730628
    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2730629
    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    return v0

    .line 2730630
    :cond_0
    sget-object v0, LX/Jkv;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2730631
    :cond_1
    sget-object v1, LX/Jkv;->a:Ljava/lang/Integer;

    goto :goto_1
.end method
