.class public LX/K5g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K87;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0gc;

.field public c:I

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/K87;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2770552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770553
    iput-object p1, p0, LX/K5g;->a:LX/0Ot;

    .line 2770554
    return-void
.end method

.method public static a(LX/0QB;)LX/K5g;
    .locals 4

    .prologue
    .line 2770566
    const-class v1, LX/K5g;

    monitor-enter v1

    .line 2770567
    :try_start_0
    sget-object v0, LX/K5g;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2770568
    sput-object v2, LX/K5g;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2770569
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2770570
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2770571
    new-instance v3, LX/K5g;

    const/16 p0, 0x35ef

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/K5g;-><init>(LX/0Ot;)V

    .line 2770572
    move-object v0, v3

    .line 2770573
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2770574
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K5g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2770575
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2770576
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/K5U;)V
    .locals 3

    .prologue
    .line 2770555
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/K5g;->b:LX/0gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K5g;->b:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/K5g;->c:I

    if-nez v0, :cond_1

    .line 2770556
    :cond_0
    :goto_0
    return-void

    .line 2770557
    :cond_1
    new-instance v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    invoke-direct {v0}, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;-><init>()V

    .line 2770558
    iput-object p1, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->o:LX/K5U;

    .line 2770559
    iget-object v1, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->o:LX/K5U;

    invoke-interface {v1, v0}, LX/K5U;->a(Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;)V

    .line 2770560
    iget-object v1, p0, LX/K5g;->d:Ljava/lang/String;

    .line 2770561
    iput-object v1, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->r:Ljava/lang/String;

    .line 2770562
    iget-object v1, p0, LX/K5g;->b:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2770563
    iget v2, p0, LX/K5g;->c:I

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2770564
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2770565
    iget-object v0, p0, LX/K5g;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    new-instance v1, LX/K7v;

    invoke-direct {v1}, LX/K7v;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
