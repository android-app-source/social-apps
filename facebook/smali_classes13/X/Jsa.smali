.class public LX/Jsa;
.super LX/6lf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/JsZ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2745565
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2745566
    return-void
.end method


# virtual methods
.method public final a(LX/6le;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V
    .locals 5

    .prologue
    .line 2745567
    check-cast p1, LX/JsZ;

    .line 2745568
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745569
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745570
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->d()LX/0Px;

    move-result-object v0

    .line 2745571
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2745572
    if-eqz v0, :cond_0

    .line 2745573
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 2745574
    invoke-static {v1}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2745575
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2745576
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v2, v1

    .line 2745577
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN_()Ljava/lang/String;

    move-result-object v0

    .line 2745578
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2745579
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2745580
    iget-object v0, p1, LX/6le;->a:Landroid/view/View;

    check-cast v0, LX/Jsb;

    .line 2745581
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2745582
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/Jsb;->setVisibility(I)V

    .line 2745583
    :goto_2
    return-void

    .line 2745584
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 2745585
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/Jsb;->setVisibility(I)V

    .line 2745586
    iget-object v4, v0, LX/Jsb;->b:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->setVisibility(I)V

    .line 2745587
    iget-object v4, v0, LX/Jsb;->b:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    invoke-virtual {v4, v2, v1, v3}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->a(Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V

    .line 2745588
    goto :goto_2
.end method

.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2745589
    new-instance v0, LX/JsZ;

    new-instance v1, LX/Jsb;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Jsb;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/JsZ;-><init>(LX/Jsb;)V

    return-object v0
.end method
