.class public LX/Jo9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/3Lc",
        "<",
        "Ljava/lang/Object;",
        "Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/1Mv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2735040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2735041
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735042
    iput-object v0, p0, LX/Jo9;->d:LX/0Ot;

    .line 2735043
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/Jo9;->k:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2735044
    return-void
.end method
