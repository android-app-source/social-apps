.class public final LX/Jmu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2733438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2733439
    new-instance v0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2733440
    new-array v0, p1, [Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;

    return-object v0
.end method
