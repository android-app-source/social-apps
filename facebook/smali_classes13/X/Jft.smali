.class public LX/Jft;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Jfs;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2722371
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2722372
    return-void
.end method


# virtual methods
.method public final a(LX/Jfu;LX/Jfy;LX/Jg3;LX/JgM;Landroid/support/v7/widget/RecyclerView;Landroid/widget/ProgressBar;Landroid/view/MenuItem;Ljava/lang/String;LX/0gc;)LX/Jfs;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Jfu",
            "<TT;>;",
            "LX/Jfy",
            "<TT;>;",
            "Lcom/facebook/messaging/business/subscription/manage/common/loader/SubscriptionManageSearchLoader",
            "<TT;>;",
            "Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManageAdapterViewFactory;",
            "Landroid/support/v7/widget/RecyclerView;",
            "Landroid/widget/ProgressBar;",
            "Landroid/view/MenuItem;",
            "Ljava/lang/String;",
            "LX/0gc;",
            ")",
            "LX/Jfs",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2722373
    new-instance v0, LX/Jfs;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, LX/Jfl;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Jfl;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    invoke-direct/range {v0 .. v12}, LX/Jfs;-><init>(Landroid/content/Context;LX/Jfl;Landroid/view/inputmethod/InputMethodManager;LX/Jfu;LX/Jfy;LX/Jg3;LX/JgM;Landroid/support/v7/widget/RecyclerView;Landroid/widget/ProgressBar;Landroid/view/MenuItem;Ljava/lang/String;LX/0gc;)V

    .line 2722374
    return-object v0
.end method
