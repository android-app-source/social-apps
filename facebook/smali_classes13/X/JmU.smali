.class public LX/JmU;
.super LX/0gG;
.source ""


# instance fields
.field public a:I
    .annotation build Landroid/support/annotation/IntRange;
    .end annotation
.end field

.field public b:I
    .annotation build Landroid/support/annotation/IntRange;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/JmT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/JmT;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2732899
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2732900
    const/4 v0, 0x4

    iput v0, p0, LX/JmU;->a:I

    .line 2732901
    const/4 v0, 0x1

    iput v0, p0, LX/JmU;->b:I

    .line 2732902
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2732903
    iput-object v0, p0, LX/JmU;->c:LX/0Px;

    .line 2732904
    new-instance v0, LX/JmT;

    invoke-direct {v0, p0}, LX/JmT;-><init>(LX/JmU;)V

    iput-object v0, p0, LX/JmU;->e:LX/JmT;

    .line 2732905
    return-void
.end method

.method private a(LX/0Px;I)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 2732888
    invoke-virtual {p0}, LX/JmU;->d()I

    move-result v5

    .line 2732889
    mul-int v6, v5, p2

    move v4, v3

    .line 2732890
    :goto_0
    if-ge v4, v5, :cond_3

    .line 2732891
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    move-object v1, v0

    .line 2732892
    :goto_1
    add-int v0, v4, v6

    iget-object v7, p0, LX/JmU;->c:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    if-ge v0, v7, :cond_1

    iget-object v0, p0, LX/JmU;->c:LX/0Px;

    add-int v7, v4, v6

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    .line 2732893
    :goto_2
    invoke-static {v1, v0}, LX/JmU;->a(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    .line 2732894
    :goto_3
    return v0

    :cond_0
    move-object v1, v2

    .line 2732895
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 2732896
    goto :goto_2

    .line 2732897
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2732898
    :cond_3
    const/4 v0, 0x1

    goto :goto_3
.end method

.method private static a(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)Z
    .locals 6
    .param p0    # Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2732883
    if-eqz p0, :cond_0

    if-nez p1, :cond_3

    .line 2732884
    :cond_0
    if-nez p0, :cond_2

    if-nez p1, :cond_2

    .line 2732885
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 2732886
    goto :goto_0

    .line 2732887
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2732877
    check-cast p1, LX/3rL;

    .line 2732878
    iget-object v0, p1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;

    .line 2732879
    iget-object v1, v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    move-object v1, v1

    .line 2732880
    iget-object v0, p1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0}, LX/JmU;->a(LX/0Px;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2732881
    const/4 v0, -0x1

    .line 2732882
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2732859
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030898

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;

    .line 2732860
    iget v1, p0, LX/JmU;->a:I

    .line 2732861
    iput v1, v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    .line 2732862
    invoke-virtual {p0}, LX/JmU;->d()I

    move-result v1

    .line 2732863
    iget-object v2, p0, LX/JmU;->c:LX/0Px;

    mul-int v3, v1, p2

    iget-object v4, p0, LX/JmU;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v5, p2, 0x1

    mul-int/2addr v1, v5

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v2, v3, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2732864
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->setItems(LX/0Px;)V

    .line 2732865
    iget-object v1, p0, LX/JmU;->e:LX/JmT;

    .line 2732866
    iput-object v1, v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->e:LX/JmT;

    .line 2732867
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2732868
    new-instance v1, LX/3rL;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2732875
    check-cast p3, LX/3rL;

    iget-object v0, p3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2732876
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2732871
    check-cast p2, LX/3rL;

    iget-object v0, p2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2732872
    check-cast p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;

    .line 2732873
    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    move-object v1, v1

    .line 2732874
    invoke-direct {p0, v1, v0}, LX/JmU;->a(LX/0Px;I)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2732870
    iget-object v0, p0, LX/JmU;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, LX/JmU;->d()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2732869
    iget v0, p0, LX/JmU;->a:I

    iget v1, p0, LX/JmU;->b:I

    mul-int/2addr v0, v1

    return v0
.end method
