.class public final LX/Jg2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jfp;

.field public final synthetic b:LX/Jg3;


# direct methods
.method public constructor <init>(LX/Jg3;LX/Jfp;)V
    .locals 0

    .prologue
    .line 2722470
    iput-object p1, p0, LX/Jg2;->b:LX/Jg3;

    iput-object p2, p0, LX/Jg2;->a:LX/Jfp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2722471
    iget-object v0, p0, LX/Jg2;->a:LX/Jfp;

    if-eqz v0, :cond_0

    .line 2722472
    iget-object v0, p0, LX/Jg2;->a:LX/Jfp;

    invoke-interface {v0}, LX/Jfp;->a()V

    .line 2722473
    :cond_0
    iget-object v0, p0, LX/Jg2;->b:LX/Jg3;

    iget-object v0, v0, LX/Jg3;->c:LX/03V;

    const-string v1, "ManageSubstationsSearchLoader"

    const-string v2, "Substation search results is invalid"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722474
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2722475
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;

    .line 2722476
    iget-object v0, p0, LX/Jg2;->a:LX/Jfp;

    if-eqz v0, :cond_1

    .line 2722477
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2722478
    :cond_0
    iget-object v0, p0, LX/Jg2;->a:LX/Jfp;

    invoke-interface {v0}, LX/Jfp;->a()V

    .line 2722479
    iget-object v0, p0, LX/Jg2;->b:LX/Jg3;

    iget-object v0, v0, LX/Jg3;->c:LX/03V;

    const-string v1, "ManageSubstationsSearchLoader"

    const-string v2, "Substation search results is invalid"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722480
    :cond_1
    :goto_0
    return-void

    .line 2722481
    :cond_2
    iget-object v0, p0, LX/Jg2;->a:LX/Jfp;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Jfp;->a(LX/0Px;)V

    goto :goto_0
.end method
