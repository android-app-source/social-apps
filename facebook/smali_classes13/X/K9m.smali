.class public LX/K9m;
.super LX/68R;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:Landroid/animation/ValueAnimator;

.field private final D:Landroid/animation/ValueAnimator;

.field public final E:Landroid/animation/AnimatorSet;

.field private F:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/K9l;",
            ">;"
        }
    .end annotation
.end field

.field public G:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/K9l;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/K9l;

.field public I:Lcom/facebook/video/livemap/LiveMapFragment;

.field private final o:LX/31i;

.field public final p:LX/68L;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68L",
            "<",
            "LX/K9l;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/K9l;",
            ">;"
        }
    .end annotation
.end field

.field private final r:[F

.field private final s:Landroid/graphics/Paint;

.field private final t:Landroid/graphics/Paint;

.field private final u:F

.field private final v:F

.field private final w:F

.field private final x:LX/31i;

.field private final y:I

.field private final z:I


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    const/4 v10, 0x2

    .line 2777970
    invoke-direct {p0, p1}, LX/68R;-><init>(LX/680;)V

    .line 2777971
    new-instance v1, LX/31i;

    move-wide v4, v2

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, LX/31i;-><init>(DDDD)V

    iput-object v1, p0, LX/K9m;->o:LX/31i;

    .line 2777972
    new-instance v0, LX/68L;

    invoke-direct {v0}, LX/68L;-><init>()V

    iput-object v0, p0, LX/K9m;->p:LX/68L;

    .line 2777973
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/K9m;->q:Ljava/util/ArrayList;

    .line 2777974
    new-array v0, v10, [F

    iput-object v0, p0, LX/K9m;->r:[F

    .line 2777975
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/K9m;->s:Landroid/graphics/Paint;

    .line 2777976
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/K9m;->t:Landroid/graphics/Paint;

    .line 2777977
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/K9m;->x:LX/31i;

    .line 2777978
    new-array v0, v10, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/K9m;->C:Landroid/animation/ValueAnimator;

    .line 2777979
    new-array v0, v10, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    .line 2777980
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LX/K9m;->E:Landroid/animation/AnimatorSet;

    .line 2777981
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 2777982
    iget-object v1, v0, LX/680;->z:Landroid/content/Context;

    move-object v0, v1

    .line 2777983
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2777984
    const v1, 0x7f0b2702

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/K9m;->u:F

    .line 2777985
    const v1, 0x7f0b2703

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/K9m;->v:F

    .line 2777986
    const v1, 0x7f0b2704

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/K9m;->w:F

    .line 2777987
    const v1, 0x7f0a09c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/K9m;->y:I

    .line 2777988
    const v1, 0x7f0a09c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/K9m;->z:I

    .line 2777989
    const v1, 0x7f0a09c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/K9m;->A:I

    .line 2777990
    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, LX/K9m;->B:I

    .line 2777991
    iget-object v1, p0, LX/K9m;->C:Landroid/animation/ValueAnimator;

    const v2, 0x7f0c0079

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2777992
    iget-object v1, p0, LX/K9m;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2777993
    iget-object v1, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    const v2, 0x7f0c007b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2777994
    iget-object v0, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 2777995
    iget-object v0, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v11}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 2777996
    iget-object v0, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2777997
    iget-object v0, p0, LX/K9m;->E:Landroid/animation/AnimatorSet;

    new-array v1, v10, [Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v3, p0, LX/K9m;->C:Landroid/animation/ValueAnimator;

    aput-object v3, v1, v2

    iget-object v2, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    aput-object v2, v1, v11

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 2777998
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 2777999
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(LX/K9m;Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;)LX/K9l;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 2777944
    if-nez p1, :cond_1

    .line 2777945
    :cond_0
    :goto_0
    return-object v1

    .line 2777946
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2777947
    if-eqz v0, :cond_0

    .line 2777948
    new-instance v2, LX/K9l;

    invoke-direct {v2}, LX/K9l;-><init>()V

    .line 2777949
    invoke-virtual {v3, v0, v6}, LX/15i;->l(II)D

    move-result-wide v4

    iput-wide v4, v2, LX/K9l;->g:D

    .line 2777950
    invoke-virtual {v3, v0, v7}, LX/15i;->l(II)D

    move-result-wide v4

    iput-wide v4, v2, LX/K9l;->h:D

    .line 2777951
    invoke-virtual {v3, v0, v7}, LX/15i;->l(II)D

    move-result-wide v4

    invoke-static {v4, v5}, LX/31h;->d(D)F

    move-result v4

    float-to-double v4, v4

    iput-wide v4, v2, LX/K9l;->b:D

    .line 2777952
    invoke-virtual {v3, v0, v6}, LX/15i;->l(II)D

    move-result-wide v4

    invoke-static {v4, v5}, LX/31h;->b(D)F

    move-result v0

    float-to-double v4, v0

    iput-wide v4, v2, LX/K9l;->c:D

    .line 2777953
    invoke-virtual {p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->n()I

    move-result v0

    iput v0, v2, LX/K9l;->n:I

    .line 2777954
    iget v0, p0, LX/K9m;->u:F

    iget v3, p0, LX/K9m;->v:F

    iget v4, p0, LX/K9m;->u:F

    sub-float/2addr v3, v4

    iget v4, v2, LX/K9l;->n:I

    const/16 v5, 0xc8

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x43480000    # 200.0f

    div-float/2addr v3, v4

    add-float/2addr v0, v3

    iput v0, v2, LX/K9l;->d:F

    .line 2777955
    invoke-virtual {p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v2, LX/K9l;->i:Landroid/net/Uri;

    .line 2777956
    invoke-virtual {p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v2, LX/K9l;->j:Landroid/net/Uri;

    .line 2777957
    invoke-virtual {p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/K9l;->l:Ljava/lang/String;

    .line 2777958
    invoke-virtual {p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->m()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    move-result-object v3

    .line 2777959
    if-eqz v3, :cond_0

    .line 2777960
    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/K9l;->a:Ljava/lang/String;

    .line 2777961
    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, v2, LX/K9l;->k:Ljava/lang/String;

    .line 2777962
    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2777963
    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2777964
    invoke-virtual {v1, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    :cond_2
    iput-object v1, v2, LX/K9l;->m:Ljava/lang/String;

    .line 2777965
    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/K9l;->o:Ljava/lang/String;

    .line 2777966
    invoke-virtual {v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, v2, LX/K9l;->p:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v2

    .line 2777967
    goto/16 :goto_0

    .line 2777968
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object v0, v1

    .line 2777969
    goto :goto_1
.end method


# virtual methods
.method public final a(FF)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2777931
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget v2, p0, LX/K9m;->w:F

    sub-float v2, p1, v2

    iget v3, p0, LX/K9m;->w:F

    sub-float v3, p2, v3

    iget-object v4, p0, LX/K9m;->r:[F

    invoke-virtual {v1, v2, v3, v4}, LX/31h;->a(FF[F)V

    .line 2777932
    iget-object v1, p0, LX/K9m;->x:LX/31i;

    iget-object v2, p0, LX/K9m;->r:[F

    aget v2, v2, v0

    float-to-double v2, v2

    iput-wide v2, v1, LX/31i;->c:D

    .line 2777933
    iget-object v1, p0, LX/K9m;->x:LX/31i;

    iget-object v2, p0, LX/K9m;->r:[F

    aget v2, v2, v5

    float-to-double v2, v2

    iput-wide v2, v1, LX/31i;->a:D

    .line 2777934
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget v2, p0, LX/K9m;->w:F

    add-float/2addr v2, p1

    iget v3, p0, LX/K9m;->w:F

    add-float/2addr v3, p2

    iget-object v4, p0, LX/K9m;->r:[F

    invoke-virtual {v1, v2, v3, v4}, LX/31h;->a(FF[F)V

    .line 2777935
    iget-object v1, p0, LX/K9m;->x:LX/31i;

    iget-object v2, p0, LX/K9m;->r:[F

    aget v2, v2, v0

    float-to-double v2, v2

    iput-wide v2, v1, LX/31i;->d:D

    .line 2777936
    iget-object v1, p0, LX/K9m;->x:LX/31i;

    iget-object v2, p0, LX/K9m;->r:[F

    aget v2, v2, v5

    float-to-double v2, v2

    iput-wide v2, v1, LX/31i;->b:D

    .line 2777937
    iget-object v1, p0, LX/K9m;->p:LX/68L;

    iget-object v2, p0, LX/K9m;->x:LX/31i;

    .line 2777938
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2777939
    iget-object v4, v1, LX/68L;->a:LX/68K;

    invoke-static {v1, v4, v2, v3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    .line 2777940
    move-object v1, v3

    .line 2777941
    iput-object v1, p0, LX/K9m;->F:Ljava/util/Set;

    .line 2777942
    iget-object v1, p0, LX/K9m;->F:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/K9m;->F:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2777943
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(LX/K9l;)V
    .locals 6

    .prologue
    .line 2777928
    iput-object p1, p0, LX/K9m;->H:LX/K9l;

    .line 2777929
    iget-object v0, p0, LX/67m;->e:LX/680;

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, p1, LX/K9l;->g:D

    iget-wide v4, p1, LX/K9l;->h:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-static {v1}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;)LX/67d;

    move-result-object v1

    iget v2, p0, LX/K9m;->B:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 2777930
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v8, 0x3e800000    # 0.25f

    .line 2778000
    iget-object v0, p0, LX/K9m;->s:Landroid/graphics/Paint;

    iget v1, p0, LX/K9m;->y:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2778001
    iget-object v0, p0, LX/K9m;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v5

    .line 2778002
    iget-object v0, p0, LX/K9m;->s:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    iget-object v2, p0, LX/K9m;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2778003
    const/4 v0, 0x0

    iget-object v1, p0, LX/K9m;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_4

    .line 2778004
    iget-object v0, p0, LX/K9m;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K9l;

    .line 2778005
    if-nez v5, :cond_1

    iget v1, v0, LX/K9l;->n:I

    const/16 v2, 0x32

    if-le v1, v2, :cond_1

    .line 2778006
    iget-object v1, p0, LX/K9m;->D:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v3

    .line 2778007
    const/high16 v1, 0x40000000    # 2.0f

    iget v2, v0, LX/K9l;->d:F

    mul-float/2addr v2, v1

    .line 2778008
    const/16 v1, 0x64

    .line 2778009
    cmpg-float v7, v3, v8

    if-gez v7, :cond_2

    .line 2778010
    iget v2, v0, LX/K9l;->d:F

    iget v7, v0, LX/K9l;->d:F

    mul-float/2addr v7, v9

    mul-float/2addr v3, v7

    div-float/2addr v3, v8

    add-float/2addr v2, v3

    .line 2778011
    :cond_0
    :goto_1
    iget-object v7, p0, LX/K9m;->t:Landroid/graphics/Paint;

    iget-object v3, p0, LX/K9m;->H:LX/K9l;

    if-ne v0, v3, :cond_3

    iget v3, p0, LX/K9m;->z:I

    :goto_2
    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2778012
    iget-object v3, p0, LX/K9m;->t:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2778013
    iget v1, v0, LX/K9l;->e:F

    iget v3, v0, LX/K9l;->f:F

    iget-object v7, p0, LX/K9m;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v2, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2778014
    :cond_1
    iget v1, v0, LX/K9l;->e:F

    iget v2, v0, LX/K9l;->f:F

    iget v0, v0, LX/K9l;->d:F

    iget-object v3, p0, LX/K9m;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2778015
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2778016
    :cond_2
    const/high16 v7, 0x3f400000    # 0.75f

    cmpl-float v7, v3, v7

    if-lez v7, :cond_0

    .line 2778017
    const/high16 v1, 0x42c80000    # 100.0f

    sub-float v3, v9, v3

    div-float/2addr v3, v8

    mul-float/2addr v1, v3

    float-to-int v1, v1

    goto :goto_1

    .line 2778018
    :cond_3
    iget v3, p0, LX/K9m;->y:I

    goto :goto_2

    .line 2778019
    :cond_4
    iget-object v0, p0, LX/K9m;->G:Ljava/util/Set;

    if-eqz v0, :cond_5

    .line 2778020
    iget-object v0, p0, LX/K9m;->s:Landroid/graphics/Paint;

    iget v1, p0, LX/K9m;->A:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2778021
    iget-object v0, p0, LX/K9m;->G:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K9l;

    .line 2778022
    iget v2, v0, LX/K9l;->e:F

    iget v3, v0, LX/K9l;->f:F

    iget v0, v0, LX/K9l;->d:F

    iget-object v4, p0, LX/K9m;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v0, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 2778023
    :cond_5
    iget-object v0, p0, LX/K9m;->H:LX/K9l;

    if-eqz v0, :cond_6

    .line 2778024
    iget-object v0, p0, LX/K9m;->s:Landroid/graphics/Paint;

    iget v1, p0, LX/K9m;->z:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2778025
    iget-object v0, p0, LX/K9m;->H:LX/K9l;

    iget v0, v0, LX/K9l;->e:F

    iget-object v1, p0, LX/K9m;->H:LX/K9l;

    iget v1, v1, LX/K9l;->f:F

    iget-object v2, p0, LX/K9m;->H:LX/K9l;

    iget v2, v2, LX/K9l;->d:F

    iget-object v3, p0, LX/K9m;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2778026
    :cond_6
    return-void
.end method

.method public final b(FF)Z
    .locals 4

    .prologue
    .line 2777911
    iget-object v0, p0, LX/K9m;->F:Ljava/util/Set;

    iput-object v0, p0, LX/K9m;->G:Ljava/util/Set;

    .line 2777912
    const/4 v0, 0x0

    iput-object v0, p0, LX/K9m;->H:LX/K9l;

    .line 2777913
    iget-object v0, p0, LX/K9m;->I:Lcom/facebook/video/livemap/LiveMapFragment;

    if-eqz v0, :cond_0

    .line 2777914
    iget-object v0, p0, LX/K9m;->I:Lcom/facebook/video/livemap/LiveMapFragment;

    iget-object v1, p0, LX/K9m;->G:Ljava/util/Set;

    .line 2777915
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2777916
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2777917
    iget-object v2, v0, Lcom/facebook/video/livemap/LiveMapFragment;->n:Ljava/util/Comparator;

    invoke-static {v3, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2777918
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    div-int/lit8 p1, v2, 0x2

    .line 2777919
    iget-object p2, v0, Lcom/facebook/video/livemap/LiveMapFragment;->g:LX/K9m;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K9l;

    invoke-virtual {p2, v2}, LX/K9m;->a(LX/K9l;)V

    .line 2777920
    iget-object v2, v0, Lcom/facebook/video/livemap/LiveMapFragment;->h:Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    .line 2777921
    iget-object p2, v2, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 2777922
    iget-object p2, v2, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2777923
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2777924
    iget-object v2, v0, Lcom/facebook/video/livemap/LiveMapFragment;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v2, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setCurrentPosition(I)V

    .line 2777925
    iget-object v2, v0, Lcom/facebook/video/livemap/LiveMapFragment;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    .line 2777926
    :cond_0
    invoke-virtual {p0}, LX/67m;->f()V

    .line 2777927
    const/4 v0, 0x1

    return v0
.end method

.method public final c()LX/31i;
    .locals 1

    .prologue
    .line 2777910
    iget-object v0, p0, LX/K9m;->o:LX/31i;

    return-object v0
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 0

    .prologue
    .line 2777901
    invoke-virtual {p0}, LX/67m;->f()V

    .line 2777902
    return-void
.end method

.method public final p()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 2777903
    iget-object v0, p0, LX/K9m;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v7, v8

    :goto_0
    if-ge v7, v9, :cond_0

    .line 2777904
    iget-object v0, p0, LX/K9m;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K9l;

    .line 2777905
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-wide v2, v0, LX/K9l;->b:D

    iget-wide v4, v0, LX/K9l;->c:D

    iget-object v6, p0, LX/K9m;->r:[F

    invoke-virtual/range {v1 .. v6}, LX/31h;->a(DD[F)V

    .line 2777906
    iget-object v1, p0, LX/K9m;->r:[F

    aget v1, v1, v8

    iput v1, v0, LX/K9l;->e:F

    .line 2777907
    iget-object v1, p0, LX/K9m;->r:[F

    aget v1, v1, v10

    iput v1, v0, LX/K9l;->f:F

    .line 2777908
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2777909
    :cond_0
    return v10
.end method
