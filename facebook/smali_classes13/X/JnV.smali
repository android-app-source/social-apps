.class public final LX/JnV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;",
        ">;",
        "Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JnW;


# direct methods
.method public constructor <init>(LX/JnW;)V
    .locals 0

    .prologue
    .line 2734035
    iput-object p1, p0, LX/JnV;->a:LX/JnW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2734036
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2734037
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734038
    iget-object v0, p0, LX/JnV;->a:LX/JnW;

    iget-object v0, v0, LX/JnW;->f:LX/JnX;

    .line 2734039
    if-nez p1, :cond_0

    .line 2734040
    iget-object v1, v0, LX/JnX;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v2, "message_requests_count_query_result"

    const-string v3, "Graphql MessageRequestsSnipperQuery result is null"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2734041
    const/4 v1, 0x0

    .line 2734042
    :goto_0
    move-object v0, v1

    .line 2734043
    return-object v0

    .line 2734044
    :cond_0
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734045
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x1458aca4

    invoke-static {v2, v1, v3}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 2734046
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734047
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->j(II)I

    move-result v1

    move v2, v1

    .line 2734048
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734049
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v4, 0x1458aca4

    invoke-static {v3, v1, v4}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 2734050
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734051
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v4, 0x3

    invoke-virtual {v3, v1, v4}, LX/15i;->j(II)I

    move-result v1

    move v3, v1

    .line 2734052
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734053
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v5, 0x1458aca4

    invoke-static {v4, v1, v5}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 2734054
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734055
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 2734056
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734057
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->j()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v6, 0x408a8551

    invoke-static {v5, v1, v6}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 2734058
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2734059
    check-cast v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;->j()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, LX/15i;->j(II)I

    move-result v1

    move v5, v1

    .line 2734060
    invoke-static {v0, p1}, LX/JnX;->f(LX/JnX;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    const/4 v9, 0x0

    .line 2734061
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2734062
    const/4 v8, 0x1

    .line 2734063
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v12

    move v10, v9

    :goto_1
    if-ge v10, v12, :cond_2

    invoke-virtual {v1, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2734064
    iget-object v13, v0, LX/JnX;->d:LX/2Og;

    invoke-virtual {v13, v7}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v7

    .line 2734065
    if-eqz v7, :cond_1

    .line 2734066
    invoke-virtual {v11, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v7, v8

    .line 2734067
    :goto_2
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move v8, v7

    goto :goto_1

    :cond_1
    move v7, v9

    .line 2734068
    goto :goto_2

    .line 2734069
    :cond_2
    if-eqz v8, :cond_3

    .line 2734070
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2734071
    :goto_3
    move-object v6, v7

    .line 2734072
    new-instance v1, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;-><init>(IILjava/lang/String;ILX/0Px;)V

    goto/16 :goto_0

    .line 2734073
    :cond_3
    :try_start_0
    iget-object v7, v0, LX/JnX;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v8

    .line 2734074
    sget-object v9, LX/0Re;->a:LX/0Re;

    move-object v9, v9

    .line 2734075
    const-wide/16 v11, -0x1

    invoke-virtual {v7, v8, v9, v11, v12}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Ljava/util/Set;Ljava/util/Set;J)LX/JqV;

    move-result-object v7

    .line 2734076
    iget-object v7, v7, LX/JqV;->a:LX/0P1;

    invoke-virtual {v7}, LX/0P1;->values()LX/0Py;

    move-result-object v7

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_3

    .line 2734077
    :catch_0
    move-exception v7

    move-object v8, v7

    .line 2734078
    iget-object v7, v0, LX/JnX;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/03V;

    const-class v9, LX/JnX;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2734079
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 2734080
    goto :goto_3
.end method
