.class public LX/K6j;
.super LX/0gF;
.source ""

# interfaces
.implements LX/Che;
.implements LX/CsQ;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CqB;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/CqD;


# direct methods
.method public constructor <init>(LX/0gc;)V
    .locals 1

    .prologue
    .line 2772316
    invoke-direct {p0, p1}, LX/0gF;-><init>(LX/0gc;)V

    .line 2772317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K6j;->a:Ljava/util/List;

    .line 2772318
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)I
    .locals 2

    .prologue
    .line 2772311
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2772312
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    if-ne v0, p1, :cond_0

    .line 2772313
    :goto_1
    return v1

    .line 2772314
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2772315
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2772305
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2772306
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    .line 2772307
    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    if-ne v0, p1, :cond_0

    .line 2772308
    :goto_1
    return v1

    .line 2772309
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2772310
    :cond_1
    const/4 v1, -0x2

    goto :goto_1
.end method

.method public final a()LX/0gG;
    .locals 0

    .prologue
    .line 2772304
    return-object p0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2772303
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    return-object v0
.end method

.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V
    .locals 5

    .prologue
    .line 2772276
    if-eqz p1, :cond_1

    .line 2772277
    new-instance v0, LX/CqB;

    invoke-direct {v0, p1}, LX/CqB;-><init>(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    .line 2772278
    iget-object v1, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2772279
    iget-object v1, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2772280
    iget-object v1, p0, LX/K6j;->b:LX/CqD;

    invoke-interface {v1, v0}, LX/CqD;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v1

    .line 2772281
    if-eqz v1, :cond_0

    if-nez p1, :cond_3

    .line 2772282
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 2772283
    :cond_1
    :goto_1
    return-void

    .line 2772284
    :cond_2
    iget-object v1, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2772285
    invoke-virtual {p0}, LX/0gG;->kV_()V

    goto :goto_1

    .line 2772286
    :cond_3
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2772287
    if-eqz v2, :cond_0

    .line 2772288
    const-string v3, "extra_instant_articles_can_log_open_link_on_settle"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2772289
    :cond_4
    if-ltz p2, :cond_1

    iget-object v1, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt p2, v1, :cond_1

    .line 2772290
    iget-object v1, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CqB;

    .line 2772291
    iget-object v2, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le p2, v2, :cond_5

    .line 2772292
    iget-object v2, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2772293
    :goto_2
    invoke-virtual {p0}, LX/0gG;->kV_()V

    goto :goto_1

    .line 2772294
    :cond_5
    iget-object v2, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v2, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2772302
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;
    .locals 1

    .prologue
    .line 2772299
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2772300
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    .line 2772301
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getActiveFragmentIndex()I
    .locals 1

    .prologue
    .line 2772298
    const/4 v0, 0x0

    return v0
.end method

.method public final getFragmentCount()I
    .locals 1

    .prologue
    .line 2772297
    iget-object v0, p0, LX/K6j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 2772295
    iput-object p1, p0, LX/K6j;->b:LX/CqD;

    .line 2772296
    return-void
.end method
