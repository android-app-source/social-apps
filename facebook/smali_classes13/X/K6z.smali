.class public final enum LX/K6z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K6z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K6z;

.field public static final enum DRAWER:LX/K6z;

.field public static final enum EMBEDDED_FRAGMENT:LX/K6z;

.field public static final enum END_CARD:LX/K6z;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2772400
    new-instance v0, LX/K6z;

    const-string v1, "DRAWER"

    invoke-direct {v0, v1, v2}, LX/K6z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6z;->DRAWER:LX/K6z;

    .line 2772401
    new-instance v0, LX/K6z;

    const-string v1, "END_CARD"

    invoke-direct {v0, v1, v3}, LX/K6z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6z;->END_CARD:LX/K6z;

    .line 2772402
    new-instance v0, LX/K6z;

    const-string v1, "EMBEDDED_FRAGMENT"

    invoke-direct {v0, v1, v4}, LX/K6z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6z;->EMBEDDED_FRAGMENT:LX/K6z;

    .line 2772403
    const/4 v0, 0x3

    new-array v0, v0, [LX/K6z;

    sget-object v1, LX/K6z;->DRAWER:LX/K6z;

    aput-object v1, v0, v2

    sget-object v1, LX/K6z;->END_CARD:LX/K6z;

    aput-object v1, v0, v3

    sget-object v1, LX/K6z;->EMBEDDED_FRAGMENT:LX/K6z;

    aput-object v1, v0, v4

    sput-object v0, LX/K6z;->$VALUES:[LX/K6z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2772404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K6z;
    .locals 1

    .prologue
    .line 2772405
    const-class v0, LX/K6z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K6z;

    return-object v0
.end method

.method public static values()[LX/K6z;
    .locals 1

    .prologue
    .line 2772406
    sget-object v0, LX/K6z;->$VALUES:[LX/K6z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K6z;

    return-object v0
.end method
