.class public LX/K31;
.super LX/0gG;
.source ""


# instance fields
.field public final a:[LX/K2y;

.field private final b:LX/K2z;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>([LX/K2y;LX/K2z;Landroid/content/Context;)V
    .locals 0
    .param p1    # [LX/K2y;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/K2z;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2764896
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2764897
    iput-object p1, p0, LX/K31;->a:[LX/K2y;

    .line 2764898
    iput-object p2, p0, LX/K31;->b:LX/K2z;

    .line 2764899
    iput-object p3, p0, LX/K31;->c:Landroid/content/Context;

    .line 2764900
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2764892
    iget-object v0, p0, LX/K31;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, LX/K31;->a:[LX/K2y;

    aget-object v1, v1, p2

    iget v1, v1, LX/K2y;->layoutResId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2764893
    iget-object v1, p0, LX/K31;->b:LX/K2z;

    iget-object v2, p0, LX/K31;->a:[LX/K2y;

    aget-object v2, v2, p2

    invoke-virtual {v1, v0, v2}, LX/K2z;->a(Landroid/view/View;LX/K2y;)V

    .line 2764894
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2764895
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2764901
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2764902
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2764891
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2764890
    iget-object v0, p0, LX/K31;->a:[LX/K2y;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/K31;->a:[LX/K2y;

    array-length v0, v0

    goto :goto_0
.end method
