.class public final LX/JZT;
.super LX/8bH;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;)V
    .locals 0

    .prologue
    .line 2708645
    iput-object p1, p0, LX/JZT;->a:Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    invoke-direct {p0}, LX/8bH;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2708646
    iget-object v0, p0, LX/JZT;->a:Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    .line 2708647
    invoke-virtual {v0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->getCurrentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 2708648
    add-int/lit8 v1, v1, -0x1

    .line 2708649
    if-gez v1, :cond_0

    .line 2708650
    invoke-virtual {v0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2708651
    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2708652
    instance-of v1, v0, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;

    if-eqz v1, :cond_1

    .line 2708653
    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2708654
    :cond_1
    return-void
.end method
