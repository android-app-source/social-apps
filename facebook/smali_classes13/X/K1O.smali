.class public LX/K1O;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K1O;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2761944
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2761945
    iput-object p2, p0, LX/K1O;->a:Ljava/lang/String;

    .line 2761946
    return-void
.end method

.method private j()LX/5pH;
    .locals 3

    .prologue
    .line 2761937
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2761938
    const-string v1, "pageScrollState"

    iget-object v2, p0, LX/K1O;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761939
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2761941
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2761942
    invoke-virtual {p0}, LX/K1O;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K1O;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2761943
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761940
    const-string v0, "topPageScrollStateChanged"

    return-object v0
.end method
