.class public final LX/Jpt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/42q;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;)V
    .locals 0

    .prologue
    .line 2738217
    iput-object p1, p0, LX/Jpt;->a:Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2738218
    const-string v0, "com.facebook.messaging.sms.migration.END_FLOW"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2738219
    iget-object v0, p0, LX/Jpt;->a:Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->t:Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

    const/4 v1, 0x0

    .line 2738220
    iput-object v1, v0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->d:LX/42q;

    .line 2738221
    iget-object v0, p0, LX/Jpt;->a:Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->finish()V

    .line 2738222
    :goto_0
    return-void

    .line 2738223
    :cond_0
    iget-object v0, p0, LX/Jpt;->a:Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->a$redex0(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method
