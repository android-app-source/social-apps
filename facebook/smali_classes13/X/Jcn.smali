.class public LX/Jcn;
.super LX/Jcj;
.source ""


# instance fields
.field private final l:Landroid/content/Context;

.field public final m:LX/Jd3;

.field public final n:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final o:LX/0Sh;

.field public final p:LX/Jcq;

.field private final q:LX/6lJ;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public final t:Lcom/facebook/user/tiles/UserTileView;

.field private final u:Landroid/widget/TextView;

.field public final v:Landroid/view/View;

.field public w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

.field public x:LX/0Tn;

.field public final y:LX/0dN;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;LX/Jd3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/6lJ;LX/Jcq;)V
    .locals 1

    .prologue
    .line 2718327
    invoke-direct {p0, p1}, LX/Jcj;-><init>(Landroid/view/View;)V

    .line 2718328
    new-instance v0, LX/Jck;

    invoke-direct {v0, p0}, LX/Jck;-><init>(LX/Jcn;)V

    iput-object v0, p0, LX/Jcn;->y:LX/0dN;

    .line 2718329
    iput-object p2, p0, LX/Jcn;->l:Landroid/content/Context;

    .line 2718330
    iput-object p3, p0, LX/Jcn;->m:LX/Jd3;

    .line 2718331
    iput-object p4, p0, LX/Jcn;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2718332
    iput-object p5, p0, LX/Jcn;->o:LX/0Sh;

    .line 2718333
    iput-object p7, p0, LX/Jcn;->p:LX/Jcq;

    .line 2718334
    iput-object p6, p0, LX/Jcn;->q:LX/6lJ;

    .line 2718335
    const v0, 0x7f0d037b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jcn;->r:Landroid/widget/TextView;

    .line 2718336
    const v0, 0x7f0d037c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jcn;->s:Landroid/widget/TextView;

    .line 2718337
    const v0, 0x7f0d037a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Jcn;->t:Lcom/facebook/user/tiles/UserTileView;

    .line 2718338
    const v0, 0x7f0d037d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jcn;->u:Landroid/widget/TextView;

    .line 2718339
    const v0, 0x7f0d037e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jcn;->v:Landroid/view/View;

    .line 2718340
    iget-object v0, p0, LX/Jcn;->v:Landroid/view/View;

    new-instance p1, LX/Jcm;

    invoke-direct {p1, p0}, LX/Jcm;-><init>(LX/Jcn;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2718341
    return-void
.end method

.method public static c(LX/Jcn;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2718377
    if-lez p1, :cond_1

    .line 2718378
    const/16 v0, 0x9

    if-le p1, v0, :cond_0

    .line 2718379
    iget-object v0, p0, LX/Jcn;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0821

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2718380
    iget-object v1, p0, LX/Jcn;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2718381
    :goto_0
    iget-object v0, p0, LX/Jcn;->u:Landroid/widget/TextView;

    iget-object v1, p0, LX/Jcn;->q:LX/6lJ;

    invoke-virtual {v1, p1}, LX/6lJ;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718382
    iget-object v0, p0, LX/Jcn;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2718383
    :goto_1
    return-void

    .line 2718384
    :cond_0
    iget-object v0, p0, LX/Jcn;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    .line 2718385
    :cond_1
    iget-object v0, p0, LX/Jcn;->u:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 12

    .prologue
    .line 2718342
    iget-object v0, p0, LX/Jcn;->x:LX/0Tn;

    if-eqz v0, :cond_0

    .line 2718343
    iget-object v0, p0, LX/Jcn;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/Jcn;->x:LX/0Tn;

    iget-object v2, p0, LX/Jcn;->y:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 2718344
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jcn;->x:LX/0Tn;

    .line 2718345
    :cond_0
    iput-object p1, p0, LX/Jcn;->w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718346
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/Jcn;->w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2718347
    iget-object v0, p0, LX/Jcn;->w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    .line 2718348
    iget-object v1, p0, LX/Jcn;->t:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-static {v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2718349
    iget-object v0, p0, LX/Jcn;->w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    .line 2718350
    iget-object v1, p0, LX/Jcn;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718351
    iget-object v0, p0, LX/Jcn;->w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-wide v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastLogout:J

    .line 2718352
    iget-object v2, p0, LX/Jcn;->m:LX/Jd3;

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2718353
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-gtz v5, :cond_2

    .line 2718354
    const/4 v5, 0x0

    .line 2718355
    :goto_0
    move-object v2, v5

    .line 2718356
    if-nez v2, :cond_1

    .line 2718357
    iget-object v2, p0, LX/Jcn;->s:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2718358
    :goto_1
    iget-object v0, p0, LX/Jcn;->w:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-static {v0}, LX/2Vv;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, LX/Jcn;->x:LX/0Tn;

    .line 2718359
    iget-object v0, p0, LX/Jcn;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/Jcn;->x:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2718360
    invoke-static {p0, v0}, LX/Jcn;->c(LX/Jcn;I)V

    .line 2718361
    iget-object v0, p0, LX/Jcn;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/Jcn;->x:LX/0Tn;

    iget-object v2, p0, LX/Jcn;->y:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 2718362
    return-void

    .line 2718363
    :cond_1
    iget-object v3, p0, LX/Jcn;->s:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718364
    iget-object v2, p0, LX/Jcn;->s:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 2718365
    :cond_2
    iget-object v5, v2, LX/Jd3;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    sub-long/2addr v5, v0

    .line 2718366
    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    long-to-int v5, v5

    .line 2718367
    div-int/lit8 v5, v5, 0x3c

    .line 2718368
    div-int/lit8 v6, v5, 0x3c

    .line 2718369
    div-int/lit8 v7, v6, 0x18

    .line 2718370
    const/16 v8, 0x3c

    if-ge v5, v8, :cond_3

    .line 2718371
    iget-object v6, v2, LX/Jd3;->a:Landroid/content/res/Resources;

    const v7, 0x7f0f018b

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 2718372
    :cond_3
    const/16 v5, 0x18

    if-ge v6, v5, :cond_4

    .line 2718373
    iget-object v5, v2, LX/Jd3;->a:Landroid/content/res/Resources;

    const v7, 0x7f0f018c

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v7, v6, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 2718374
    :cond_4
    const/4 v5, 0x7

    if-ge v7, v5, :cond_5

    .line 2718375
    iget-object v5, v2, LX/Jd3;->a:Landroid/content/res/Resources;

    const v6, 0x7f0f018d

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 2718376
    :cond_5
    iget-object v5, v2, LX/Jd3;->a:Landroid/content/res/Resources;

    const v6, 0x7f083b44

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method
