.class public final LX/JsE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/users/username/EditUsernameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V
    .locals 0

    .prologue
    .line 2745195
    iput-object p1, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745188
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-static {v0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->d(Lcom/facebook/messaging/users/username/EditUsernameFragment;)I

    .line 2745189
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->b()V

    .line 2745190
    if-nez p1, :cond_0

    .line 2745191
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->e()V

    .line 2745192
    :goto_0
    return-void

    .line 2745193
    :cond_0
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->f()V

    .line 2745194
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->m:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2745183
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->b()V

    .line 2745184
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2745185
    iget-object v0, p0, LX/JsE;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->e()V

    .line 2745186
    :cond_0
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745187
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/JsE;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
