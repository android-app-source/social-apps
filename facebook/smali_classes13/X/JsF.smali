.class public final LX/JsF;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/users/username/EditUsernameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V
    .locals 0

    .prologue
    .line 2745196
    iput-object p1, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2745197
    iget-object v0, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "edit_username"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2745198
    iget-object v0, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0809a4

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2745199
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2745200
    if-nez p1, :cond_1

    .line 2745201
    iget-object v0, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0809a4

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2745202
    :cond_0
    :goto_0
    return-void

    .line 2745203
    :cond_1
    iget-object v0, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->d:LX/JsI;

    iget-object v1, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget v1, v1, Lcom/facebook/messaging/users/username/EditUsernameFragment;->q:I

    .line 2745204
    iget-object v2, v0, LX/JsI;->a:LX/0Zb;

    const-string v3, "android_messenger_edit_username_save_successful"

    const/4 p1, 0x1

    invoke-interface {v2, v3, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2745205
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2745206
    const-string v3, "android_messenger_number_of_availability_checks"

    invoke-virtual {v2, v3, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2745207
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2745208
    :cond_2
    iget-object v0, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    const/4 v1, 0x0

    .line 2745209
    iput v1, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->q:I

    .line 2745210
    iget-object v0, p0, LX/JsF;->a:Lcom/facebook/messaging/users/username/EditUsernameFragment;

    iget-object v0, v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->l:LX/JsH;

    if-eqz v0, :cond_0

    .line 2745211
    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2745212
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/JsF;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
