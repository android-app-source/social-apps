.class public LX/JWd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JWe;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWd",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JWe;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703263
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703264
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWd;->b:LX/0Zi;

    .line 2703265
    iput-object p1, p0, LX/JWd;->a:LX/0Ot;

    .line 2703266
    return-void
.end method

.method public static a(LX/0QB;)LX/JWd;
    .locals 4

    .prologue
    .line 2703282
    const-class v1, LX/JWd;

    monitor-enter v1

    .line 2703283
    :try_start_0
    sget-object v0, LX/JWd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703284
    sput-object v2, LX/JWd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703285
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703286
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703287
    new-instance v3, LX/JWd;

    const/16 p0, 0x20d1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWd;-><init>(LX/0Ot;)V

    .line 2703288
    move-object v0, v3

    .line 2703289
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703290
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703291
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703292
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2703269
    check-cast p2, LX/JWc;

    .line 2703270
    iget-object v0, p0, LX/JWd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWe;

    iget-object v1, p2, LX/JWc;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-boolean v2, p2, LX/JWc;->b:Z

    const/4 p0, 0x1

    .line 2703271
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    const v5, 0x7f0b0060

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f02032e

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1bbe

    invoke-interface {v4, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1bbe

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0203b2

    invoke-interface {v4, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x5

    const v6, 0x7f0b0065

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00ab

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    sget-object v6, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v7, LX/0xr;->MEDIUM:LX/0xr;

    const/4 v8, 0x0

    invoke-static {p1, v6, v7, v8}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v7, 0x2

    const/4 p2, 0x1

    .line 2703272
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2, v7}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    if-eqz v2, :cond_1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0828a0

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v7, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v7, 0x7f0b004e

    invoke-virtual {v5, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v7, 0x7f0a00a4

    invoke-virtual {v5, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    sget-object v7, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v8, LX/0xr;->REGULAR:LX/0xr;

    const/4 p0, 0x0

    invoke-static {p1, v7, v8, p0}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v6, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    .line 2703273
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->l()Lcom/facebook/graphql/model/GraphQLContactPoint;

    move-result-object v6

    .line 2703274
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLContactPoint;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2703275
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLContactPoint;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 2703276
    :cond_0
    :goto_1
    move-object v5, v5

    .line 2703277
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2703278
    return-object v0

    .line 2703279
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->k()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 2703280
    :sswitch_0
    const v6, 0x7f020934

    invoke-static {v0, p1, v6}, LX/JWe;->a(LX/JWe;LX/1De;I)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_1

    .line 2703281
    :sswitch_1
    const v6, 0x7f020850

    invoke-static {v0, p1, v6}, LX/JWe;->a(LX/JWe;LX/1De;I)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3609cb28 -> :sswitch_1
        0x1c4e6237 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703267
    invoke-static {}, LX/1dS;->b()V

    .line 2703268
    const/4 v0, 0x0

    return-object v0
.end method
