.class public final LX/Jhu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jhv;


# direct methods
.method public constructor <init>(LX/Jhv;)V
    .locals 0

    .prologue
    .line 2725584
    iput-object p1, p0, LX/Jhu;->a:LX/Jhv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2725585
    iget-object v0, p0, LX/Jhu;->a:LX/Jhv;

    iget-object v0, v0, LX/Jhv;->d:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2725586
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2725587
    check-cast p1, Ljava/lang/Integer;

    .line 2725588
    const-string v0, ""

    .line 2725589
    if-eqz p1, :cond_0

    .line 2725590
    iget-object v0, p0, LX/Jhu;->a:LX/Jhv;

    iget-object v0, v0, LX/Jhv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lJ;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/6lJ;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2725591
    :cond_0
    iget-object v1, p0, LX/Jhu;->a:LX/Jhv;

    iget-object v1, v1, LX/Jhv;->d:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2725592
    return-void
.end method
