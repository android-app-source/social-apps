.class public final LX/Jiq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2726746
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2726747
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2726748
    :goto_0
    return v1

    .line 2726749
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2726750
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2726751
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2726752
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2726753
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2726754
    const-string v4, "subtitles"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2726755
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2726756
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2726757
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2726758
    const/4 v4, 0x0

    .line 2726759
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_9

    .line 2726760
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2726761
    :goto_3
    move v3, v4

    .line 2726762
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2726763
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2726764
    goto :goto_1

    .line 2726765
    :cond_3
    const-string v4, "title"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2726766
    const/4 v3, 0x0

    .line 2726767
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_d

    .line 2726768
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2726769
    :goto_4
    move v0, v3

    .line 2726770
    goto :goto_1

    .line 2726771
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2726772
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2726773
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2726774
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2726775
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2726776
    :cond_7
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2726777
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2726778
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2726779
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 2726780
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2726781
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_5

    .line 2726782
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2726783
    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 2726784
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_9
    move v3, v4

    goto :goto_5

    .line 2726785
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2726786
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_c

    .line 2726787
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2726788
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2726789
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_b

    if-eqz v4, :cond_b

    .line 2726790
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2726791
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_6

    .line 2726792
    :cond_c
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2726793
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2726794
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_4

    :cond_d
    move v0, v3

    goto :goto_6
.end method
