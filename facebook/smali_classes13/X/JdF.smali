.class public final LX/JdF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

.field public final synthetic c:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;ZLcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 0

    .prologue
    .line 2719062
    iput-object p1, p0, LX/JdF;->c:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iput-boolean p2, p0, LX/JdF;->a:Z

    iput-object p3, p0, LX/JdF;->b:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2719063
    iget-boolean v0, p0, LX/JdF;->a:Z

    if-eqz v0, :cond_0

    .line 2719064
    iget-object v0, p0, LX/JdF;->c:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->l:LX/0aG;

    const-string v1, "get_dbl_nonce"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->x:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x6e53fcc4

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2719065
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2719066
    return-void

    .line 2719067
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2719068
    const-string v0, "account_id"

    iget-object v1, p0, LX/JdF;->b:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2719069
    iget-object v0, p0, LX/JdF;->c:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->l:LX/0aG;

    const-string v1, "expire_dbl_nonce"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->x:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x7d059341

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method
