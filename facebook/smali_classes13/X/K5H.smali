.class public LX/K5H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8nF;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8nP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8nP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2770186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770187
    iput-object p1, p0, LX/K5H;->a:LX/0Ot;

    .line 2770188
    return-void
.end method

.method public static a(LX/0QB;)LX/K5H;
    .locals 1

    .prologue
    .line 2770185
    invoke-static {p0}, LX/K5H;->b(LX/0QB;)LX/K5H;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/K5H;
    .locals 2

    .prologue
    .line 2770180
    new-instance v0, LX/K5H;

    const/16 v1, 0x35db

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/K5H;-><init>(LX/0Ot;)V

    .line 2770181
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)LX/8nB;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2770182
    instance-of v0, p1, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$Metadata;

    if-eqz v0, :cond_0

    .line 2770183
    iget-object v0, p0, LX/K5H;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nB;

    .line 2770184
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
