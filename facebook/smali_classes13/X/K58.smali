.class public final enum LX/K58;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K58;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K58;

.field public static final enum BOTTOM:LX/K58;

.field public static final enum CENTER:LX/K58;

.field public static final enum TOP:LX/K58;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2769922
    new-instance v0, LX/K58;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v2}, LX/K58;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K58;->TOP:LX/K58;

    .line 2769923
    new-instance v0, LX/K58;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, LX/K58;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K58;->CENTER:LX/K58;

    .line 2769924
    new-instance v0, LX/K58;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, LX/K58;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K58;->BOTTOM:LX/K58;

    .line 2769925
    const/4 v0, 0x3

    new-array v0, v0, [LX/K58;

    sget-object v1, LX/K58;->TOP:LX/K58;

    aput-object v1, v0, v2

    sget-object v1, LX/K58;->CENTER:LX/K58;

    aput-object v1, v0, v3

    sget-object v1, LX/K58;->BOTTOM:LX/K58;

    aput-object v1, v0, v4

    sput-object v0, LX/K58;->$VALUES:[LX/K58;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2769913
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convert(Ljava/lang/String;)LX/K58;
    .locals 2

    .prologue
    .line 2769916
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2769917
    sget-object v0, LX/K58;->CENTER:LX/K58;

    :goto_1
    return-object v0

    .line 2769918
    :sswitch_0
    const-string v1, "top"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "center"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "bottom"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 2769919
    :pswitch_0
    sget-object v0, LX/K58;->TOP:LX/K58;

    goto :goto_1

    .line 2769920
    :pswitch_1
    sget-object v0, LX/K58;->CENTER:LX/K58;

    goto :goto_1

    .line 2769921
    :pswitch_2
    sget-object v0, LX/K58;->BOTTOM:LX/K58;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x527265d5 -> :sswitch_2
        -0x514d33ab -> :sswitch_1
        0x1c155 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/K58;
    .locals 1

    .prologue
    .line 2769915
    const-class v0, LX/K58;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K58;

    return-object v0
.end method

.method public static values()[LX/K58;
    .locals 1

    .prologue
    .line 2769914
    sget-object v0, LX/K58;->$VALUES:[LX/K58;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K58;

    return-object v0
.end method
