.class public LX/K1w;
.super LX/Cos;
.source ""

# interfaces
.implements LX/CnE;
.implements LX/CnG;
.implements LX/CnH;
.implements LX/CnI;
.implements LX/CnJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/Co9;",
        "Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;",
        ">;",
        "Lcom/facebook/richdocument/optional/Image360PhotoBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ckq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/K1v;

.field public final d:LX/CuL;

.field public e:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:I

.field public m:I

.field public n:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private o:Z

.field private p:Z

.field public q:Z


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2762928
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2762929
    new-instance v0, LX/K1v;

    invoke-direct {v0, p0}, LX/K1v;-><init>(LX/K1w;)V

    iput-object v0, p0, LX/K1w;->c:LX/K1v;

    .line 2762930
    const-class v0, LX/K1w;

    invoke-static {v0, p0}, LX/K1w;->a(Ljava/lang/Class;LX/02k;)V

    .line 2762931
    new-instance v0, LX/CuL;

    invoke-direct {v0, p1}, LX/CuL;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/K1w;->d:LX/CuL;

    .line 2762932
    iget-object v0, p0, LX/K1w;->d:LX/CuL;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2762933
    new-instance v0, LX/CuA;

    invoke-direct {v0, p1}, LX/CuA;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2762934
    new-instance v0, LX/Cu1;

    invoke-direct {v0, p1}, LX/Cu1;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2762935
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/K1w;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/Ckq;->a(LX/0QB;)LX/Ckq;

    move-result-object p0

    check-cast p0, LX/Ckq;

    iput-object v1, p1, LX/K1w;->a:LX/Chv;

    iput-object p0, p1, LX/K1w;->b:LX/Ckq;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2762936
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 2762937
    iput-boolean v2, p0, LX/K1w;->p:Z

    .line 2762938
    iput-boolean v2, p0, LX/K1w;->o:Z

    .line 2762939
    iput-boolean v2, p0, LX/K1w;->q:Z

    .line 2762940
    iput-object v0, p0, LX/K1w;->k:Ljava/lang/String;

    .line 2762941
    iput-object v0, p0, LX/K1w;->e:Ljava/lang/String;

    .line 2762942
    iput v2, p0, LX/K1w;->l:I

    .line 2762943
    iput v2, p0, LX/K1w;->m:I

    .line 2762944
    iput-object v0, p0, LX/K1w;->n:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2762945
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    sget-object v1, LX/7Cm;->RENDER_AXIS_DEFAULT:LX/7Cm;

    invoke-virtual {v0, v1}, LX/8wv;->setRotatedRenderAxis(LX/7Cm;)V

    .line 2762946
    iget-object v0, p0, LX/K1w;->d:LX/CuL;

    if-eqz v0, :cond_0

    .line 2762947
    iget-object v0, p0, LX/K1w;->d:LX/CuL;

    invoke-virtual {v0, v2}, LX/CuL;->a(Z)V

    .line 2762948
    :cond_0
    const-string v0, "Image360PhotoBlockViewImpl.reset#reset RichDocument360PhotoView"

    const v1, 0x2a242855

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2762949
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->b()V

    .line 2762950
    const v0, 0x59221377

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2762951
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2762952
    invoke-super {p0, p1}, LX/Cos;->b(Landroid/os/Bundle;)V

    .line 2762953
    iget-object v0, p0, LX/K1w;->b:LX/Ckq;

    iget-object v1, p0, LX/K1w;->k:Ljava/lang/String;

    iget v2, p0, LX/K1w;->l:I

    iget v3, p0, LX/K1w;->m:I

    iget-object v4, p0, LX/K1w;->n:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iget-boolean v5, p0, LX/K1w;->o:Z

    iget-boolean v6, p0, LX/K1w;->p:Z

    invoke-virtual/range {v0 .. v6}, LX/Ckq;->a(Ljava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;ZZ)V

    .line 2762954
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2762955
    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    if-nez v1, :cond_0

    .line 2762956
    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    if-eqz v1, :cond_2

    .line 2762957
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 2762958
    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    sget-object v1, LX/ClT;->UFI:LX/ClT;

    invoke-virtual {v0, v1}, LX/Cs7;->a(LX/ClT;)LX/CnQ;

    move-result-object v0

    .line 2762959
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/CnR;

    if-eqz v1, :cond_0

    .line 2762960
    check-cast v0, LX/CnR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CnR;->setShowShareButton(Z)V

    .line 2762961
    :cond_0
    iget-object v0, p0, LX/K1w;->a:LX/Chv;

    iget-object v1, p0, LX/K1w;->c:LX/K1v;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2762962
    return-void

    .line 2762963
    :cond_1
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 2762964
    :cond_2
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2762965
    iget-object v0, p0, LX/K1w;->a:LX/Chv;

    iget-object v1, p0, LX/K1w;->c:LX/K1v;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2762966
    invoke-super {p0, p1}, LX/Cos;->c(Landroid/os/Bundle;)V

    .line 2762967
    return-void
.end method
