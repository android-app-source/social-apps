.class public LX/JhC;
.super LX/Idv;
.source ""


# instance fields
.field public b:LX/FJw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JhR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/JhD;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/user/tiles/UserTileView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2724208
    invoke-direct {p0, p1}, LX/Idv;-><init>(Landroid/content/Context;)V

    .line 2724209
    const-class v0, LX/JhC;

    invoke-static {v0, p0}, LX/JhC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2724210
    const v0, 0x7f030d0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2724211
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JhC;->e:Landroid/widget/TextView;

    .line 2724212
    const v0, 0x7f0d1e9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JhC;->f:Landroid/widget/TextView;

    .line 2724213
    const v0, 0x7f0d1e99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/JhC;->h:Lcom/facebook/user/tiles/UserTileView;

    .line 2724214
    const v0, 0x7f0d209a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JhC;->g:Landroid/view/View;

    .line 2724215
    iget-object v0, p0, LX/JhC;->g:Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 2724216
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/JhC;

    invoke-static {v2}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object v1

    check-cast v1, LX/FJw;

    const/16 p0, 0x270e

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/JhC;->b:LX/FJw;

    iput-object v2, p1, LX/JhC;->c:LX/0Ot;

    return-void
.end method


# virtual methods
.method public getInnerRow()Landroid/view/View;
    .locals 1

    .prologue
    .line 2724217
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JhC;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
