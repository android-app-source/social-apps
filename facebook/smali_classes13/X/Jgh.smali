.class public final LX/Jgh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Landroid/preference/Preference;

.field public final synthetic b:Landroid/preference/PreferenceCategory;

.field public final synthetic c:Landroid/preference/Preference;

.field public final synthetic d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Landroid/preference/Preference;Landroid/preference/PreferenceCategory;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 2723610
    iput-object p1, p0, LX/Jgh;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iput-object p2, p0, LX/Jgh;->a:Landroid/preference/Preference;

    iput-object p3, p0, LX/Jgh;->b:Landroid/preference/PreferenceCategory;

    iput-object p4, p0, LX/Jgh;->c:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2723611
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2723612
    :goto_0
    return-void

    .line 2723613
    :cond_0
    iget-object v0, p0, LX/Jgh;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v1, p0, LX/Jgh;->a:Landroid/preference/Preference;

    .line 2723614
    invoke-static {v0, p1, v1}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a$redex0(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;LX/0Px;Landroid/preference/Preference;)V

    .line 2723615
    iget-object v0, p0, LX/Jgh;->a:Landroid/preference/Preference;

    new-instance v1, LX/Jgg;

    invoke-direct {v1, p0}, LX/Jgg;-><init>(LX/Jgh;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2723616
    const/4 v1, 0x0

    .line 2723617
    iget-object v0, p0, LX/Jgh;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    iget-object v0, v0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    .line 2723618
    iget-object v2, v0, LX/Jgk;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Iid;->c:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2723619
    iget-object v0, p0, LX/Jgh;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2723620
    if-eqz v3, :cond_3

    .line 2723621
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;

    .line 2723622
    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2723623
    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;->j()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2723624
    :goto_2
    if-nez v0, :cond_1

    .line 2723625
    const v0, 0x7f08057d

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2723626
    :cond_1
    iget-object v1, p0, LX/Jgh;->d:Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    const v2, 0x7f08057b

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2723627
    iget-object v1, p0, LX/Jgh;->a:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2723628
    iget-object v0, p0, LX/Jgh;->b:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, LX/Jgh;->a:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2723629
    iget-object v0, p0, LX/Jgh;->a:Landroid/preference/Preference;

    iget-object v1, p0, LX/Jgh;->c:Landroid/preference/Preference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    goto :goto_0

    .line 2723630
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
