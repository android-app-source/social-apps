.class public LX/K1N;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K1N;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:F


# direct methods
.method public constructor <init>(IIF)V
    .locals 1

    .prologue
    .line 2761925
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2761926
    iput p2, p0, LX/K1N;->a:I

    .line 2761927
    invoke-static {p3}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p3, 0x0

    :cond_1
    iput p3, p0, LX/K1N;->b:F

    .line 2761928
    return-void
.end method

.method private j()LX/5pH;
    .locals 4

    .prologue
    .line 2761929
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2761930
    const-string v1, "position"

    iget v2, p0, LX/K1N;->a:I

    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761931
    const-string v1, "offset"

    iget v2, p0, LX/K1N;->b:F

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2761932
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2761933
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2761934
    invoke-virtual {p0}, LX/K1N;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K1N;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2761935
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761936
    const-string v0, "topPageScroll"

    return-object v0
.end method
