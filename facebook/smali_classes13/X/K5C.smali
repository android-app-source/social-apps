.class public final enum LX/K5C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K5C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K5C;

.field public static final enum ALWAYS:LX/K5C;

.field public static final enum EQUAL:LX/K5C;

.field public static final enum GEQUAL:LX/K5C;

.field public static final enum GREATER:LX/K5C;

.field public static final enum LEQUAL:LX/K5C;

.field public static final enum LESS:LX/K5C;

.field public static final enum NEVER:LX/K5C;

.field public static final enum NOTEQUAL:LX/K5C;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2769963
    new-instance v0, LX/K5C;

    const-string v1, "LEQUAL"

    invoke-direct {v0, v1, v3}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->LEQUAL:LX/K5C;

    .line 2769964
    new-instance v0, LX/K5C;

    const-string v1, "LESS"

    invoke-direct {v0, v1, v4}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->LESS:LX/K5C;

    .line 2769965
    new-instance v0, LX/K5C;

    const-string v1, "GEQUAL"

    invoke-direct {v0, v1, v5}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->GEQUAL:LX/K5C;

    .line 2769966
    new-instance v0, LX/K5C;

    const-string v1, "GREATER"

    invoke-direct {v0, v1, v6}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->GREATER:LX/K5C;

    .line 2769967
    new-instance v0, LX/K5C;

    const-string v1, "EQUAL"

    invoke-direct {v0, v1, v7}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->EQUAL:LX/K5C;

    .line 2769968
    new-instance v0, LX/K5C;

    const-string v1, "NOTEQUAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->NOTEQUAL:LX/K5C;

    .line 2769969
    new-instance v0, LX/K5C;

    const-string v1, "ALWAYS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->ALWAYS:LX/K5C;

    .line 2769970
    new-instance v0, LX/K5C;

    const-string v1, "NEVER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/K5C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K5C;->NEVER:LX/K5C;

    .line 2769971
    const/16 v0, 0x8

    new-array v0, v0, [LX/K5C;

    sget-object v1, LX/K5C;->LEQUAL:LX/K5C;

    aput-object v1, v0, v3

    sget-object v1, LX/K5C;->LESS:LX/K5C;

    aput-object v1, v0, v4

    sget-object v1, LX/K5C;->GEQUAL:LX/K5C;

    aput-object v1, v0, v5

    sget-object v1, LX/K5C;->GREATER:LX/K5C;

    aput-object v1, v0, v6

    sget-object v1, LX/K5C;->EQUAL:LX/K5C;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/K5C;->NOTEQUAL:LX/K5C;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/K5C;->ALWAYS:LX/K5C;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/K5C;->NEVER:LX/K5C;

    aput-object v2, v0, v1

    sput-object v0, LX/K5C;->$VALUES:[LX/K5C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2769972
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convert(Ljava/lang/String;)LX/K5C;
    .locals 2

    .prologue
    .line 2769973
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2769974
    sget-object v0, LX/K5C;->LEQUAL:LX/K5C;

    :goto_1
    return-object v0

    .line 2769975
    :sswitch_0
    const-string v1, "lessEqual"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "less"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "greaterEqual"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "greater"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "equal"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "notEqual"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "always"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "never"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    .line 2769976
    :pswitch_0
    sget-object v0, LX/K5C;->LEQUAL:LX/K5C;

    goto :goto_1

    .line 2769977
    :pswitch_1
    sget-object v0, LX/K5C;->LESS:LX/K5C;

    goto :goto_1

    .line 2769978
    :pswitch_2
    sget-object v0, LX/K5C;->GEQUAL:LX/K5C;

    goto :goto_1

    .line 2769979
    :pswitch_3
    sget-object v0, LX/K5C;->GREATER:LX/K5C;

    goto :goto_1

    .line 2769980
    :pswitch_4
    sget-object v0, LX/K5C;->EQUAL:LX/K5C;

    goto :goto_1

    .line 2769981
    :pswitch_5
    sget-object v0, LX/K5C;->NOTEQUAL:LX/K5C;

    goto :goto_1

    .line 2769982
    :pswitch_6
    sget-object v0, LX/K5C;->ALWAYS:LX/K5C;

    goto :goto_1

    .line 2769983
    :pswitch_7
    sget-object v0, LX/K5C;->NEVER:LX/K5C;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x54506df1 -> :sswitch_6
        -0x535f88a6 -> :sswitch_2
        0x32a199 -> :sswitch_1
        0x5c46734 -> :sswitch_4
        0x63dca8c -> :sswitch_7
        0x10e76bfa -> :sswitch_3
        0x1460ed1b -> :sswitch_0
        0x5c889821 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/K5C;
    .locals 1

    .prologue
    .line 2769984
    const-class v0, LX/K5C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K5C;

    return-object v0
.end method

.method public static values()[LX/K5C;
    .locals 1

    .prologue
    .line 2769985
    sget-object v0, LX/K5C;->$VALUES:[LX/K5C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K5C;

    return-object v0
.end method
