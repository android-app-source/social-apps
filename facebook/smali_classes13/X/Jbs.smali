.class public final LX/Jbs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jbt;


# direct methods
.method public constructor <init>(LX/Jbt;)V
    .locals 0

    .prologue
    .line 2717221
    iput-object p1, p0, LX/Jbs;->a:LX/Jbt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2717222
    check-cast p1, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    check-cast p2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    .line 2717223
    iget-object v0, p0, LX/Jbs;->a:LX/Jbt;

    iget-object v0, v0, LX/78l;->a:Landroid/content/Context;

    const v1, 0x7f081928

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2717224
    iget-object v1, p1, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2717225
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2717226
    const/4 v0, 0x1

    .line 2717227
    :goto_0
    return v0

    .line 2717228
    :cond_0
    iget-object v1, p2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2717229
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2717230
    const/4 v0, -0x1

    goto :goto_0

    .line 2717231
    :cond_1
    iget-object v0, p1, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2717232
    iget-object v1, p2, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2717233
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
