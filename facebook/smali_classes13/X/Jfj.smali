.class public final enum LX/Jfj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jfj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jfj;

.field public static final enum FOOTER:LX/Jfj;

.field public static final enum PUBLISHER:LX/Jfj;

.field public static final enum SUBSTATION:LX/Jfj;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2722210
    new-instance v0, LX/Jfj;

    const-string v1, "FOOTER"

    invoke-direct {v0, v1, v2}, LX/Jfj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jfj;->FOOTER:LX/Jfj;

    .line 2722211
    new-instance v0, LX/Jfj;

    const-string v1, "SUBSTATION"

    invoke-direct {v0, v1, v3}, LX/Jfj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jfj;->SUBSTATION:LX/Jfj;

    .line 2722212
    new-instance v0, LX/Jfj;

    const-string v1, "PUBLISHER"

    invoke-direct {v0, v1, v4}, LX/Jfj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jfj;->PUBLISHER:LX/Jfj;

    .line 2722213
    const/4 v0, 0x3

    new-array v0, v0, [LX/Jfj;

    sget-object v1, LX/Jfj;->FOOTER:LX/Jfj;

    aput-object v1, v0, v2

    sget-object v1, LX/Jfj;->SUBSTATION:LX/Jfj;

    aput-object v1, v0, v3

    sget-object v1, LX/Jfj;->PUBLISHER:LX/Jfj;

    aput-object v1, v0, v4

    sput-object v0, LX/Jfj;->$VALUES:[LX/Jfj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2722214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jfj;
    .locals 1

    .prologue
    .line 2722215
    const-class v0, LX/Jfj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jfj;

    return-object v0
.end method

.method public static values()[LX/Jfj;
    .locals 1

    .prologue
    .line 2722216
    sget-object v0, LX/Jfj;->$VALUES:[LX/Jfj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jfj;

    return-object v0
.end method
