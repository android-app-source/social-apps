.class public LX/Jsn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FFV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FFU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FFX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2745752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745753
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745754
    iput-object v0, p0, LX/Jsn;->a:LX/0Ot;

    .line 2745755
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745756
    iput-object v0, p0, LX/Jsn;->b:LX/0Ot;

    .line 2745757
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745758
    iput-object v0, p0, LX/Jsn;->c:LX/0Ot;

    .line 2745759
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745760
    iput-object v0, p0, LX/Jsn;->d:LX/0Ot;

    .line 2745761
    return-void
.end method

.method public static a(LX/Jsn;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Z
    .locals 3

    .prologue
    .line 2745762
    iget-object v0, p0, LX/Jsn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFV;

    const/4 v1, 0x0

    .line 2745763
    iget-object v2, v0, LX/FFV;->a:LX/0Uh;

    const/16 p0, 0x170

    invoke-virtual {v2, p0, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/FFV;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 2745764
    if-eqz v0, :cond_1

    .line 2745765
    invoke-static {p1}, LX/Jmy;->c(Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Ljava/lang/String;

    move-result-object v0

    .line 2745766
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2745767
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/Jsn;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V
    .locals 13

    .prologue
    .line 2745768
    invoke-static {p1}, LX/Jmy;->c(Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Ljava/lang/String;

    move-result-object v0

    .line 2745769
    const/4 v1, 0x0

    .line 2745770
    if-nez v0, :cond_3

    .line 2745771
    :cond_0
    :goto_0
    move-object v0, v1

    .line 2745772
    move-object v0, v0

    .line 2745773
    iget-object v1, p0, LX/Jsn;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FFU;

    .line 2745774
    const/4 v6, 0x0

    .line 2745775
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    move v5, v6

    .line 2745776
    :goto_1
    move v2, v5

    .line 2745777
    if-nez v2, :cond_4

    .line 2745778
    :cond_1
    :goto_2
    iget-object v1, p0, LX/Jsn;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2745779
    iget-object v1, p0, LX/Jsn;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p0, LX/Jsn;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2745780
    :cond_2
    return-void

    .line 2745781
    :cond_3
    sget-object v2, LX/3RH;->ab:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2745782
    const/4 p1, -0x1

    if-eq v2, p1, :cond_0

    .line 2745783
    sget-object v1, LX/3RH;->ab:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2745784
    :cond_4
    iget-object v2, v1, LX/FFU;->b:LX/0Zb;

    const-string v3, "messenger_instant_article_impression"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2745785
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2745786
    const-string v3, "article_id"

    invoke-virtual {v2, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2745787
    const-string v3, "native_article_story"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2745788
    invoke-virtual {v2}, LX/0oG;->d()V

    goto :goto_2

    .line 2745789
    :cond_5
    iget-object v5, v1, LX/FFU;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 2745790
    iget-object v5, v1, LX/FFU;->a:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v5, v1, LX/FFU;->a:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    sub-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(J)J

    move-result-wide v9

    const-wide/32 v11, 0xea60

    cmp-long v5, v9, v11

    if-gez v5, :cond_6

    move v5, v6

    .line 2745791
    goto :goto_1

    .line 2745792
    :cond_6
    iget-object v5, v1, LX/FFU;->a:Ljava/util/Map;

    invoke-interface {v5, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2745793
    const/4 v5, 0x1

    goto :goto_1
.end method
