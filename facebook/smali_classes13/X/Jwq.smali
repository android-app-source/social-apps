.class public final LX/Jwq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Jwp;

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/data/PulsarRecord;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Jwv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Jwv;)V
    .locals 1

    .prologue
    .line 2752580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752581
    sget-object v0, LX/Jwp;->FBLE:LX/Jwp;

    iput-object v0, p0, LX/Jwq;->a:LX/Jwp;

    .line 2752582
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Jwq;->b:LX/0am;

    .line 2752583
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Jwq;->c:LX/0am;

    .line 2752584
    return-void
.end method

.method public constructor <init>(Lcom/facebook/placetips/bootstrap/data/PulsarRecord;)V
    .locals 1

    .prologue
    .line 2752585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752586
    sget-object v0, LX/Jwp;->PULSAR:LX/Jwp;

    iput-object v0, p0, LX/Jwq;->a:LX/Jwp;

    .line 2752587
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Jwq;->b:LX/0am;

    .line 2752588
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Jwq;->c:LX/0am;

    .line 2752589
    return-void
.end method
