.class public final LX/Jv6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/8DZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;)V
    .locals 0

    .prologue
    .line 2749754
    iput-object p1, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/8DZ;)V
    .locals 6
    .param p1    # LX/8DZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749755
    iget-object v0, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    .line 2749756
    iput-object p1, v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->l:LX/8DZ;

    .line 2749757
    iget-object v0, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v0, v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->e:LX/Jv4;

    iget-object v1, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v1, v1, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->l:LX/8DZ;

    iget-object v2, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v2, v2, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->m:Landroid/preference/PreferenceScreen;

    sget-object v3, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->i:LX/0Tn;

    sget-object v4, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->j:LX/0Tn;

    sget-object v5, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->k:LX/0Tn;

    invoke-virtual/range {v0 .. v5}, LX/Jv4;->a(LX/8DZ;Landroid/preference/PreferenceScreen;LX/0Tn;LX/0Tn;LX/0Tn;)V

    .line 2749758
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2749759
    iget-object v0, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v0, v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->f:LX/03V;

    const-string v1, "omvp_app_updates"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2749760
    iget-object v0, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v0, v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->e:LX/Jv4;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v2, v2, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->m:Landroid/preference/PreferenceScreen;

    sget-object v3, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->i:LX/0Tn;

    sget-object v4, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->j:LX/0Tn;

    sget-object v5, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->k:LX/0Tn;

    invoke-virtual/range {v0 .. v5}, LX/Jv4;->a(LX/8DZ;Landroid/preference/PreferenceScreen;LX/0Tn;LX/0Tn;LX/0Tn;)V

    .line 2749761
    iget-object v0, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v0, v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->e:LX/Jv4;

    iget-object v1, p0, LX/Jv6;->a:Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    iget-object v1, v1, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->m:Landroid/preference/PreferenceScreen;

    const v2, 0x7f0200ab

    .line 2749762
    const/4 v5, 0x1

    .line 2749763
    new-instance v3, Landroid/preference/Preference;

    iget-object v4, v0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    .line 2749764
    iget-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    const v4, 0x7f030ff9

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2749765
    iget-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 2749766
    iget-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    .line 2749767
    iget-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setOrder(I)V

    .line 2749768
    if-lez v2, :cond_0

    .line 2749769
    iget-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setIcon(I)V

    .line 2749770
    :cond_0
    iget-object v3, v0, LX/Jv4;->l:Landroid/preference/Preference;

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2749771
    const/4 v4, 0x0

    .line 2749772
    iget-object v3, v0, LX/Jv4;->j:LX/4ok;

    invoke-virtual {v3, v4}, LX/4ok;->setEnabled(Z)V

    .line 2749773
    iget-object v3, v0, LX/Jv4;->m:LX/4ok;

    invoke-virtual {v3, v4}, LX/4ok;->setEnabled(Z)V

    .line 2749774
    iget-object v3, v0, LX/Jv4;->n:LX/4ok;

    invoke-virtual {v3, v4}, LX/4ok;->setEnabled(Z)V

    .line 2749775
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749776
    check-cast p1, LX/8DZ;

    invoke-direct {p0, p1}, LX/Jv6;->a(LX/8DZ;)V

    return-void
.end method
