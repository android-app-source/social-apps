.class public LX/Jfz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jfy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jfy",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/03V;

.field private final d:LX/3MX;

.field public e:LX/15i;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/03V;LX/3MX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2722420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722421
    iput-object p1, p0, LX/Jfz;->a:LX/0tX;

    .line 2722422
    iput-object p2, p0, LX/Jfz;->b:LX/1Ck;

    .line 2722423
    iput-object p3, p0, LX/Jfz;->c:LX/03V;

    .line 2722424
    iput-object p4, p0, LX/Jfz;->d:LX/3MX;

    .line 2722425
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2722404
    iget-object v0, p0, LX/Jfz;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2722405
    return-void
.end method

.method public final a(Ljava/lang/String;LX/Jfp;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/Jfp",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2722406
    new-instance v0, LX/CKu;

    invoke-direct {v0}, LX/CKu;-><init>()V

    move-object v0, v0

    .line 2722407
    const-string v1, "first"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2722408
    const-string v1, "square_profile_pic_size_big"

    iget-object v2, p0, LX/Jfz;->d:LX/3MX;

    sget-object v3, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v2, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2722409
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, LX/Jfz;->f:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 2722410
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, LX/Jfz;->e:LX/15i;

    iget v3, p0, LX/Jfz;->f:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2722411
    const-string v1, "after"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2722412
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2722413
    iget-object v1, p0, LX/Jfz;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2722414
    iget-object v1, p0, LX/Jfz;->b:LX/1Ck;

    const-string v2, "load_publishers"

    .line 2722415
    new-instance v3, LX/Jfx;

    invoke-direct {v3, p0, p2}, LX/Jfx;-><init>(LX/Jfz;LX/Jfp;)V

    move-object v3, v3

    .line 2722416
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2722417
    return-void

    .line 2722418
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2722419
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2722403
    iget-object v0, p0, LX/Jfz;->b:LX/1Ck;

    const-string v1, "load_publishers"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2722402
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, LX/Jfz;->f:I

    monitor-exit v1

    if-nez v2, :cond_0

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, LX/Jfz;->e:LX/15i;

    iget v3, p0, LX/Jfz;->f:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v2, v3, v0}, LX/15i;->h(II)Z

    move-result v0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2722401
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/Jfz;->e:LX/15i;

    const/4 v0, 0x0

    iput v0, p0, LX/Jfz;->f:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
