.class public final LX/JpQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Do1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Do1",
        "<",
        "LX/JpS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

.field public final synthetic b:LX/JpT;


# direct methods
.method public constructor <init>(LX/JpT;Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;)V
    .locals 0

    .prologue
    .line 2737385
    iput-object p1, p0, LX/JpQ;->b:LX/JpT;

    iput-object p2, p0, LX/JpQ;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;LX/0Rf;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/JpS;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2737386
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2737387
    new-instance v0, LX/Dnz;

    invoke-direct {v0, p2}, LX/Dnz;-><init>(Ljava/util/Collection;)V

    throw v0

    .line 2737388
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2737389
    const-wide/high16 v0, -0x8000000000000000L

    .line 2737390
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 2737391
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2737392
    new-instance v8, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iget-object v9, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/16 v10, 0x3c

    if-ge v1, v10, :cond_1

    move v1, v4

    :goto_1
    invoke-direct {v8, v9, v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737393
    iget-wide v0, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 2737394
    goto :goto_0

    :cond_1
    move v1, v5

    .line 2737395
    goto :goto_1

    .line 2737396
    :cond_2
    sget-object v0, LX/JpS;->FACEBOOK:LX/JpS;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2737397
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2737398
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2737399
    invoke-direct {v0, v1, v5}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737400
    :cond_3
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v0

    .line 2737401
    iput-boolean v4, v0, LX/6hv;->b:Z

    .line 2737402
    move-object v0, v0

    .line 2737403
    iput-wide v2, v0, LX/6hv;->d:J

    .line 2737404
    move-object v0, v0

    .line 2737405
    invoke-static {v6}, LX/DoE;->a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v1

    .line 2737406
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v1, v2

    .line 2737407
    iput-object v1, v0, LX/6hv;->a:Ljava/util/List;

    .line 2737408
    move-object v0, v0

    .line 2737409
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
