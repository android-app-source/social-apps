.class public LX/JxB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JwX;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/JwZ;

.field public final f:LX/Jws;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jwc;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0bW;

.field private final i:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public constructor <init>(LX/0tX;LX/0Ot;LX/0Ot;LX/0Ot;LX/JwZ;LX/Jws;LX/0Or;LX/0bW;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JwX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;",
            "LX/JwZ;",
            "LX/Jws;",
            "LX/0Or",
            "<",
            "LX/Jwc;",
            ">;",
            "LX/0bW;",
            "Lcom/facebook/reaction/ReactionUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2752892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752893
    iput-object p1, p0, LX/JxB;->a:LX/0tX;

    .line 2752894
    iput-object p2, p0, LX/JxB;->b:LX/0Ot;

    .line 2752895
    iput-object p3, p0, LX/JxB;->c:LX/0Ot;

    .line 2752896
    iput-object p4, p0, LX/JxB;->d:LX/0Ot;

    .line 2752897
    iput-object p5, p0, LX/JxB;->e:LX/JwZ;

    .line 2752898
    iput-object p6, p0, LX/JxB;->f:LX/Jws;

    .line 2752899
    iput-object p7, p0, LX/JxB;->g:LX/0Or;

    .line 2752900
    iput-object p8, p0, LX/JxB;->h:LX/0bW;

    .line 2752901
    iput-object p9, p0, LX/JxB;->i:Lcom/facebook/reaction/ReactionUtil;

    .line 2752902
    return-void
.end method
