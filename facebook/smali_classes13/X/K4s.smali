.class public final LX/K4s;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/renderer/TextureLoader;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/renderer/TextureLoader;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2769576
    iput-object p1, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    invoke-direct {p0}, LX/46Z;-><init>()V

    .line 2769577
    iput-object p2, p0, LX/K4s;->b:Ljava/lang/String;

    .line 2769578
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2769569
    iget-object v0, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    iget-object v0, v0, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    iget-object v1, p0, LX/K4s;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K4t;

    .line 2769570
    invoke-static {v0}, LX/K4t;->a(LX/K4t;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2769571
    iget-object v0, v0, LX/K4t;->b:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 2769572
    iget-object v0, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    .line 2769573
    iget v1, v0, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    add-int/lit8 p0, v1, -0x1

    iput p0, v0, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    .line 2769574
    return-void

    .line 2769575
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    .line 2769551
    iget-object v0, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    iget-object v0, v0, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    iget-object v1, p0, LX/K4s;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2769552
    :goto_0
    return-void

    .line 2769553
    :cond_0
    invoke-direct {p0}, LX/K4s;->a()V

    .line 2769554
    iget-object v0, p0, LX/K4s;->b:Ljava/lang/String;

    const v6, 0x812f

    const/16 v5, 0x2601

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xde1

    .line 2769555
    new-array v1, v2, [I

    .line 2769556
    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 2769557
    aget v2, v1, v4

    invoke-static {v3, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 2769558
    const/16 v2, 0x2801

    invoke-static {v3, v2, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769559
    const/16 v2, 0x2800

    invoke-static {v3, v2, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769560
    const/16 v2, 0x2802

    invoke-static {v3, v2, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769561
    const/16 v2, 0x2803

    invoke-static {v3, v2, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769562
    invoke-static {v3, v4, p1, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 2769563
    invoke-static {v3, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 2769564
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 2769565
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 2769566
    new-instance v2, LX/K5A;

    aget v1, v1, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v0, v1, v3, v4}, LX/K5A;-><init>(Ljava/lang/String;III)V

    move-object v0, v2

    .line 2769567
    iget-object v1, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    iget-object v1, v1, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    iget-object v2, p0, LX/K4s;->b:Ljava/lang/String;

    new-instance v3, LX/K4t;

    invoke-direct {v3, v0}, LX/K4t;-><init>(LX/K5A;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2769568
    iget-object v0, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    invoke-static {v0}, Lcom/facebook/storyline/renderer/TextureLoader;->c$redex0(Lcom/facebook/storyline/renderer/TextureLoader;)V

    goto :goto_0
.end method

.method public final c(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2769550
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2769544
    iget-object v0, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    iget-object v0, v0, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    iget-object v1, p0, LX/K4s;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2769545
    sget-object v0, Lcom/facebook/storyline/renderer/TextureLoader;->a:Ljava/lang/String;

    const-string v1, "Bitmap decoding failed but already cancelled: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/K4s;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2769546
    :goto_0
    return-void

    .line 2769547
    :cond_0
    invoke-direct {p0}, LX/K4s;->a()V

    .line 2769548
    sget-object v0, Lcom/facebook/storyline/renderer/TextureLoader;->a:Ljava/lang/String;

    const-string v1, "Bitmap decoding failed: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/K4s;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2769549
    iget-object v0, p0, LX/K4s;->a:Lcom/facebook/storyline/renderer/TextureLoader;

    invoke-static {v0}, Lcom/facebook/storyline/renderer/TextureLoader;->c$redex0(Lcom/facebook/storyline/renderer/TextureLoader;)V

    goto :goto_0
.end method
