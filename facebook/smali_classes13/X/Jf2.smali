.class public LX/Jf2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721419
    return-void
.end method

.method public static a(LX/Jf0;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2721420
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2721421
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p0, LX/Jf0;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2721422
    const-string v1, "messenger_inbox_ads"

    .line 2721423
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2721424
    sget-object v1, LX/Jf1;->CLIENT_TOKEN:LX/Jf1;

    iget-object v1, v1, LX/Jf1;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2721425
    return-object v0

    .line 2721426
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/Jf2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 2721427
    iget-object v0, p0, LX/Jf2;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2721428
    return-void
.end method

.method public static b(LX/0QB;)LX/Jf2;
    .locals 2

    .prologue
    .line 2721429
    new-instance v1, LX/Jf2;

    invoke-direct {v1}, LX/Jf2;-><init>()V

    .line 2721430
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2721431
    iput-object v0, v1, LX/Jf2;->a:LX/0Zb;

    .line 2721432
    return-object v1
.end method
