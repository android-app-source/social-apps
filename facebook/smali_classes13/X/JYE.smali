.class public LX/JYE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JYF;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYE",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JYF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705993
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2705994
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYE;->b:LX/0Zi;

    .line 2705995
    iput-object p1, p0, LX/JYE;->a:LX/0Ot;

    .line 2705996
    return-void
.end method

.method public static a(LX/0QB;)LX/JYE;
    .locals 4

    .prologue
    .line 2705997
    const-class v1, LX/JYE;

    monitor-enter v1

    .line 2705998
    :try_start_0
    sget-object v0, LX/JYE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705999
    sput-object v2, LX/JYE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706000
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706001
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706002
    new-instance v3, LX/JYE;

    const/16 p0, 0x2172

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYE;-><init>(LX/0Ot;)V

    .line 2706003
    move-object v0, v3

    .line 2706004
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706005
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706006
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706007
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2706008
    const v0, -0x20c09f3f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2706009
    check-cast p2, LX/JYD;

    .line 2706010
    iget-object v0, p0, LX/JYE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JYF;

    iget-object v1, p2, LX/JYD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JYD;->b:LX/1Po;

    .line 2706011
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x1

    .line 2706012
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2706013
    check-cast v4, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2706014
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a015d

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b0050

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f0b08fe

    invoke-interface {v4, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x0

    const/16 p2, 0x10

    invoke-interface {v4, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v4, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 2706015
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JYF;->a:LX/JYO;

    const/4 p0, 0x0

    .line 2706016
    new-instance p2, LX/JYM;

    invoke-direct {p2, v4}, LX/JYM;-><init>(LX/JYO;)V

    .line 2706017
    iget-object v0, v4, LX/JYO;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JYL;

    .line 2706018
    if-nez v0, :cond_0

    .line 2706019
    new-instance v0, LX/JYL;

    invoke-direct {v0, v4}, LX/JYL;-><init>(LX/JYO;)V

    .line 2706020
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/JYL;->a$redex0(LX/JYL;LX/1De;IILX/JYM;)V

    .line 2706021
    move-object p2, v0

    .line 2706022
    move-object p0, p2

    .line 2706023
    move-object v4, p0

    .line 2706024
    check-cast v2, LX/1Pq;

    .line 2706025
    iget-object p0, v4, LX/JYL;->a:LX/JYM;

    iput-object v2, p0, LX/JYM;->a:LX/1Pq;

    .line 2706026
    iget-object p0, v4, LX/JYL;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2706027
    move-object v4, v4

    .line 2706028
    iget-object p0, v4, LX/JYL;->a:LX/JYM;

    iput-object v1, p0, LX/JYM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706029
    iget-object p0, v4, LX/JYL;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2706030
    move-object v4, v4

    .line 2706031
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/3mm;->c(LX/1De;)LX/3mp;

    move-result-object v4

    const p0, 0x7f08107a

    invoke-virtual {v4, p0}, LX/3mp;->h(I)LX/3mp;

    move-result-object v4

    .line 2706032
    const p0, -0x20c09f3f

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2706033
    invoke-virtual {v4, p0}, LX/3mp;->a(LX/1dQ;)LX/3mp;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2706034
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2706035
    invoke-static {}, LX/1dS;->b()V

    .line 2706036
    iget v0, p1, LX/1dQ;->b:I

    .line 2706037
    packed-switch v0, :pswitch_data_0

    .line 2706038
    :goto_0
    return-object v2

    .line 2706039
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2706040
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2706041
    check-cast v1, LX/JYD;

    .line 2706042
    iget-object v3, p0, LX/JYE;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JYF;

    iget-object p1, v1, LX/JYD;->b:LX/1Po;

    .line 2706043
    iget-object p2, v3, LX/JYF;->b:LX/JY7;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    .line 2706044
    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 2706045
    if-eqz v1, :cond_2

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v3}, LX/1Qt;->name()Ljava/lang/String;

    move-result-object v3

    .line 2706046
    :goto_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sgny_see_all_click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "native_newsfeed"

    .line 2706047
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2706048
    move-object v0, v0

    .line 2706049
    if-eqz v3, :cond_0

    .line 2706050
    const-string v1, "feed_name"

    invoke-virtual {v0, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2706051
    :cond_0
    move-object v3, v0

    .line 2706052
    iget-object v0, p2, LX/JY7;->f:LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2706053
    iget-object v1, p2, LX/JY7;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v3, LX/JXx;->a:S

    const/4 v0, 0x0

    invoke-interface {v1, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2706054
    iget-object v1, p2, LX/JY7;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    sget-object v3, LX/0ax;->X:Ljava/lang/String;

    invoke-virtual {v1, p0, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2706055
    :goto_2
    goto :goto_0

    .line 2706056
    :cond_1
    iget-object v1, p2, LX/JY7;->i:LX/17Y;

    sget-object v3, LX/0ax;->hv:Ljava/lang/String;

    invoke-interface {v1, p0, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2706057
    iget-object v3, p2, LX/JY7;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    .line 2706058
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x20c09f3f
        :pswitch_0
    .end packed-switch
.end method
