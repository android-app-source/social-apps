.class public final LX/Jla;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;",
        "LX/6LZ",
        "<",
        "LX/JlX;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0rS;

.field public final synthetic b:LX/JlY;

.field public final synthetic c:LX/Jlc;

.field public final synthetic d:LX/Jld;


# direct methods
.method public constructor <init>(LX/Jld;LX/0rS;LX/JlY;LX/Jlc;)V
    .locals 0

    .prologue
    .line 2731621
    iput-object p1, p0, LX/Jla;->d:LX/Jld;

    iput-object p2, p0, LX/Jla;->a:LX/0rS;

    iput-object p3, p0, LX/Jla;->b:LX/JlY;

    iput-object p4, p0, LX/Jla;->c:LX/Jlc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2731622
    check-cast p1, LX/Jlv;

    .line 2731623
    if-nez p1, :cond_0

    .line 2731624
    const/4 v0, 0x0

    invoke-static {v0}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    .line 2731625
    :goto_0
    return-object v0

    .line 2731626
    :cond_0
    iget-object v0, p0, LX/Jla;->a:LX/0rS;

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v0, v1, :cond_1

    .line 2731627
    iget-object v0, p1, LX/Jlv;->b:LX/JlX;

    move-object v0, v0

    .line 2731628
    iget-object v0, v0, LX/JlX;->a:LX/0ta;

    sget-object v1, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    if-ne v0, v1, :cond_1

    .line 2731629
    iget-object v0, p1, LX/Jlv;->b:LX/JlX;

    move-object v0, v0

    .line 2731630
    invoke-static {v0}, LX/6LZ;->a(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_0

    .line 2731631
    :cond_1
    iget-object v0, p0, LX/Jla;->b:LX/JlY;

    sget-object v1, LX/JlY;->TOP:LX/JlY;

    if-ne v0, v1, :cond_3

    .line 2731632
    iget-object v0, p0, LX/Jla;->c:LX/Jlc;

    iget-object v0, v0, LX/Jlc;->a:LX/Jlb;

    sget-object v1, LX/Jlb;->TOP_UNITS_PRELOAD_ONLY:LX/Jlb;

    if-ne v0, v1, :cond_2

    .line 2731633
    iget-object v0, p1, LX/Jlv;->b:LX/JlX;

    move-object v0, v0

    .line 2731634
    invoke-static {v0}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_0

    .line 2731635
    :cond_2
    iget-object v0, p1, LX/Jlv;->b:LX/JlX;

    move-object v0, v0

    .line 2731636
    invoke-static {v0}, LX/6LZ;->a(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_0

    .line 2731637
    :cond_3
    iget-object v0, p1, LX/Jlv;->b:LX/JlX;

    move-object v0, v0

    .line 2731638
    invoke-static {v0}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_0
.end method
