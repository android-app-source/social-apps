.class public LX/Jfk;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/JgM;

.field private b:LX/0gc;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Jg4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JgM;LX/0gc;)V
    .locals 1
    .param p1    # LX/JgM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2722242
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2722243
    iput-object p1, p0, LX/Jfk;->a:LX/JgM;

    .line 2722244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    .line 2722245
    iput-object p2, p0, LX/Jfk;->b:LX/0gc;

    .line 2722246
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2722237
    sget-object v0, LX/Jfj;->FOOTER:LX/Jfj;

    invoke-virtual {v0}, LX/Jfj;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2722238
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2722239
    const v1, 0x7f030a76

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2722240
    new-instance v0, LX/Jfi;

    invoke-direct {v0, v1}, LX/Jfi;-><init>(Landroid/view/View;)V

    .line 2722241
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Jfk;->a:LX/JgM;

    invoke-static {}, LX/Jfj;->values()[LX/Jfj;

    move-result-object v1

    aget-object v1, v1, p2

    new-instance v2, LX/Jfh;

    invoke-direct {v2, p0}, LX/Jfh;-><init>(LX/Jfk;)V

    invoke-virtual {v0, p1, v1, v2}, LX/JgM;->a(Landroid/view/ViewGroup;LX/Jfj;LX/Jfh;)LX/1a1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 2722217
    iget-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/Jg5;

    if-eqz v0, :cond_0

    .line 2722218
    :goto_0
    return-void

    .line 2722219
    :cond_0
    check-cast p1, LX/JgL;

    iget-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jg4;

    .line 2722220
    move-object v5, v0

    check-cast v5, LX/Jg7;

    .line 2722221
    iget-object v1, v5, LX/Jg7;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    move-object v4, v1

    .line 2722222
    iget-object v1, p1, LX/JgL;->n:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CKT;

    .line 2722223
    iget-object v1, p1, LX/JgL;->m:LX/JgE;

    invoke-virtual {v4}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/JgE;->a(Ljava/lang/String;)V

    .line 2722224
    iget-object v1, p1, LX/JgL;->m:LX/JgE;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/JgE;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2722225
    iget-object v1, p1, LX/JgL;->m:LX/JgE;

    invoke-virtual {v4}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->l()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/JgE;->a(Z)V

    .line 2722226
    iget-object v7, p1, LX/JgL;->m:LX/JgE;

    new-instance v1, LX/JgK;

    move-object v2, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, LX/JgK;-><init>(LX/JgL;LX/CKT;Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;LX/Jg7;I)V

    invoke-virtual {v7, v1}, LX/JgE;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2722227
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2722229
    iget-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/Jg7;

    if-eqz v0, :cond_0

    .line 2722230
    sget-object v0, LX/Jfj;->SUBSTATION:LX/Jfj;

    invoke-virtual {v0}, LX/Jfj;->ordinal()I

    move-result v0

    .line 2722231
    :goto_0
    return v0

    .line 2722232
    :cond_0
    iget-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/Jg6;

    if-eqz v0, :cond_1

    .line 2722233
    sget-object v0, LX/Jfj;->PUBLISHER:LX/Jfj;

    invoke-virtual {v0}, LX/Jfj;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2722234
    :cond_1
    iget-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/Jg5;

    if-eqz v0, :cond_2

    .line 2722235
    sget-object v0, LX/Jfj;->FOOTER:LX/Jfj;

    invoke-virtual {v0}, LX/Jfj;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2722236
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2722228
    iget-object v0, p0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
