.class public final LX/K6V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/tarot/media/TarotVideoView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/tarot/media/TarotVideoView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2771700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2771701
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/K6V;->a:Z

    .line 2771702
    iput-object p1, p0, LX/K6V;->b:Ljava/lang/ref/WeakReference;

    .line 2771703
    iput-object p2, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    .line 2771704
    return-void

    .line 2771705
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/ref/WeakReference;)LX/K6V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;)",
            "LX/K6V;"
        }
    .end annotation

    .prologue
    .line 2771676
    new-instance v0, LX/K6V;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/K6V;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2771697
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2771698
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const v1, 0x3f4147ae    # 0.755f

    const v2, 0x3d4ccccd    # 0.05f

    const v3, 0x3f5ae148    # 0.855f

    const v4, 0x3d75c28f    # 0.06f

    invoke-static {v1, v2, v3, v4}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/K6T;

    invoke-direct {v1, p0, p1}, LX/K6T;-><init>(LX/K6V;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2771699
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 2771690
    iget-boolean v0, p0, LX/K6V;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2771691
    :goto_0
    if-nez v0, :cond_1

    .line 2771692
    :goto_1
    return-void

    .line 2771693
    :cond_0
    iget-object v0, p0, LX/K6V;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 2771694
    :cond_1
    if-eqz p1, :cond_2

    .line 2771695
    invoke-direct {p0, v0}, LX/K6V;->a(Landroid/view/View;)V

    goto :goto_1

    .line 2771696
    :cond_2
    invoke-direct {p0, v0}, LX/K6V;->b(Landroid/view/View;)V

    goto :goto_1
.end method

.method public static b(Ljava/lang/ref/WeakReference;)LX/K6V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/tarot/media/TarotVideoView;",
            ">;)",
            "LX/K6V;"
        }
    .end annotation

    .prologue
    .line 2771689
    new-instance v0, LX/K6V;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, LX/K6V;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2771687
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const v1, 0x3e6b851f    # 0.23f

    const v2, 0x3ea3d70a    # 0.32f

    invoke-static {v1, v4, v2, v4}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/K6U;

    invoke-direct {v1, p0, p1}, LX/K6U;-><init>(LX/K6V;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2771688
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2771685
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/K6V;->a(Z)V

    .line 2771686
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2771683
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/K6V;->a(Z)V

    .line 2771684
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2771680
    iget-boolean v0, p0, LX/K6V;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2771681
    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/media/TarotVideoView;

    invoke-virtual {v0}, LX/2oW;->b()V

    .line 2771682
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2771677
    iget-boolean v0, p0, LX/K6V;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2771678
    iget-object v0, p0, LX/K6V;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/media/TarotVideoView;

    invoke-virtual {v0}, LX/2oW;->jj_()V

    .line 2771679
    :cond_0
    return-void
.end method
