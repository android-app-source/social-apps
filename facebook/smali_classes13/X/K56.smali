.class public LX/K56;
.super LX/K4z;
.source ""


# static fields
.field private static final a:Landroid/text/TextPaint;

.field private static final b:Landroid/graphics/Typeface;

.field private static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:LX/K59;

.field public e:Z

.field public f:I

.field public g:LX/K4v;

.field public h:LX/K5C;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field private k:I

.field public l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[F>;"
        }
    .end annotation
.end field

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2769833
    const-string v0, "sans-serif"

    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, LX/K56;->b:Landroid/graphics/Typeface;

    .line 2769834
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "Helvetica"

    const-string v2, "sans-serif"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Helvetica-Bold"

    const-string v2, "sans-serif"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Helvetica-Light"

    const-string v2, "sans-serif-light"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "HelveticaNeue"

    const-string v2, "sans-serif"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "HelveticaNeue-Bold"

    const-string v2, "sans-serif"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "HelveticaNeue-Light"

    const-string v2, "sans-serif-light"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Georgia"

    const-string v2, "serif"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/K56;->c:LX/0P1;

    .line 2769835
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v4}, Landroid/text/TextPaint;-><init>(I)V

    .line 2769836
    sput-object v0, LX/K56;->a:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2769837
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2769838
    invoke-direct {p0}, LX/K4z;-><init>()V

    .line 2769839
    sget-object v0, LX/K4v;->NORMAL:LX/K4v;

    .line 2769840
    iput-object v0, p0, LX/K56;->g:LX/K4v;

    .line 2769841
    sget-object v0, LX/K5C;->LESS:LX/K5C;

    .line 2769842
    iput-object v0, p0, LX/K56;->h:LX/K5C;

    .line 2769843
    iput-boolean v1, p0, LX/K56;->e:Z

    .line 2769844
    iput v1, p0, LX/K56;->m:I

    .line 2769845
    return-void
.end method

.method public static b(LX/K59;)Landroid/util/Pair;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/K59;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "[F>;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x7fe

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 2769846
    sget-object v0, LX/K56;->c:LX/0P1;

    iget-object v1, p0, LX/K59;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 2769847
    sget-object v1, LX/K56;->a:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    sget-object v0, LX/K56;->b:Landroid/graphics/Typeface;

    :cond_0
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2769848
    sget-object v0, LX/K56;->a:Landroid/text/TextPaint;

    iget v1, p0, LX/K59;->c:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2769849
    sget-object v0, LX/K56;->a:Landroid/text/TextPaint;

    iget v1, p0, LX/K59;->e:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2769850
    iget-object v0, p0, LX/K59;->a:Ljava/lang/String;

    sget-object v1, LX/K56;->a:Landroid/text/TextPaint;

    invoke-static {v0, v1}, Landroid/text/StaticLayout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 2769851
    iget v0, p0, LX/K59;->f:I

    if-le v4, v0, :cond_2

    iget-boolean v0, p0, LX/K59;->j:Z

    if-eqz v0, :cond_2

    move v6, v7

    .line 2769852
    :goto_0
    if-eqz v6, :cond_3

    iget-object v0, p0, LX/K59;->a:Ljava/lang/String;

    sget-object v1, LX/K56;->a:Landroid/text/TextPaint;

    iget v2, p0, LX/K59;->f:I

    int-to-float v2, v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, v2, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2769853
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ne v0, v2, :cond_4

    move v3, v7

    .line 2769854
    :goto_2
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, LX/K56;->a:Landroid/text/TextPaint;

    if-eqz v6, :cond_1

    iget v4, p0, LX/K59;->f:I

    :cond_1
    invoke-static {v4, v9}, Ljava/lang/Math;->min(II)I

    move-result v4

    add-int/2addr v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v6, p0, LX/K59;->d:F

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2769855
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    add-int/lit8 v6, v1, 0x2

    .line 2769856
    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v1

    invoke-static {v9, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v9, v1, 0x2

    .line 2769857
    int-to-float v1, v6

    div-float v2, v1, v10

    .line 2769858
    int-to-float v1, v9

    div-float v4, v1, v10

    .line 2769859
    sget-object v1, LX/K55;->a:[I

    iget-object v3, p0, LX/K59;->h:Landroid/graphics/Paint$Align;

    invoke-virtual {v3}, Landroid/graphics/Paint$Align;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v1, v5

    .line 2769860
    :goto_3
    sget-object v3, LX/K55;->b:[I

    iget-object v10, p0, LX/K59;->i:LX/K58;

    invoke-virtual {v10}, LX/K58;->ordinal()I

    move-result v10

    aget v3, v3, v10

    packed-switch v3, :pswitch_data_1

    move v3, v5

    .line 2769861
    :goto_4
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 2769862
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2769863
    invoke-virtual {v11, v5, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2769864
    invoke-virtual {v0, v11}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 2769865
    const/4 v0, 0x6

    new-array v0, v0, [F

    .line 2769866
    int-to-float v6, v6

    aput v6, v0, v8

    .line 2769867
    int-to-float v6, v9

    aput v6, v0, v7

    .line 2769868
    const/4 v6, 0x2

    aput v2, v0, v6

    .line 2769869
    const/4 v6, 0x3

    aput v4, v0, v6

    .line 2769870
    const/4 v6, 0x4

    div-float/2addr v1, v2

    sub-float/2addr v1, v5

    neg-float v1, v1

    aput v1, v0, v6

    .line 2769871
    const/4 v1, 0x5

    div-float v2, v3, v4

    sub-float/2addr v2, v5

    neg-float v2, v2

    aput v2, v0, v1

    .line 2769872
    invoke-static {v10, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_2
    move v6, v8

    .line 2769873
    goto/16 :goto_0

    .line 2769874
    :cond_3
    iget-object v1, p0, LX/K59;->a:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    move v3, v8

    .line 2769875
    goto :goto_2

    :pswitch_0
    move v1, v5

    .line 2769876
    goto :goto_3

    :pswitch_1
    move v1, v2

    .line 2769877
    goto :goto_3

    .line 2769878
    :pswitch_2
    int-to-float v1, v6

    sub-float/2addr v1, v5

    goto :goto_3

    .line 2769879
    :pswitch_3
    int-to-float v3, v9

    sub-float/2addr v3, v5

    .line 2769880
    goto :goto_4

    :pswitch_4
    move v3, v4

    .line 2769881
    goto :goto_4

    :pswitch_5
    move v3, v5

    .line 2769882
    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/util/List;LX/K4y;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/K54;",
            ">;",
            "LX/K4y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2769883
    new-instance v1, LX/K54;

    invoke-direct {v1}, LX/K54;-><init>()V

    .line 2769884
    iget-object v0, p0, LX/K56;->g:LX/K4v;

    sget-object v2, LX/K4v;->NORMAL:LX/K4v;

    if-eq v0, v2, :cond_0

    const/high16 v0, 0x1000000

    :goto_0
    iget v2, p0, LX/K56;->f:I

    add-int/2addr v0, v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 2769885
    iput v0, v1, LX/K54;->m:I

    .line 2769886
    iget-boolean v0, p0, LX/K56;->e:Z

    .line 2769887
    iput-boolean v0, v1, LX/K54;->j:Z

    .line 2769888
    iget-object v0, p0, LX/K56;->h:LX/K5C;

    .line 2769889
    iput-object v0, v1, LX/K54;->c:LX/K5C;

    .line 2769890
    iget-object v0, p0, LX/K56;->g:LX/K4v;

    .line 2769891
    iput-object v0, v1, LX/K54;->b:LX/K4v;

    .line 2769892
    iget v0, p0, LX/K56;->k:I

    if-nez v0, :cond_1

    .line 2769893
    iget-object v0, p0, LX/K56;->i:Ljava/lang/String;

    .line 2769894
    iput-object v0, v1, LX/K54;->h:Ljava/lang/String;

    .line 2769895
    iget-object v0, p0, LX/K56;->j:Ljava/lang/String;

    .line 2769896
    iput-object v0, v1, LX/K54;->i:Ljava/lang/String;

    .line 2769897
    :goto_1
    iget-object v0, v1, LX/K54;->a:LX/K4y;

    move-object v0, v0

    .line 2769898
    iget-object v2, p0, LX/K4z;->a:LX/K4y;

    move-object v2, v2

    .line 2769899
    invoke-virtual {v0, p2, v2}, LX/K4y;->a(LX/K4y;LX/K4y;)V

    .line 2769900
    iget-object v0, p0, LX/K56;->l:Ljava/util/Map;

    .line 2769901
    iput-object v0, v1, LX/K54;->k:Ljava/util/Map;

    .line 2769902
    iget v0, p0, LX/K56;->m:I

    .line 2769903
    iput v0, v1, LX/K54;->n:I

    .line 2769904
    iget-object v0, p0, LX/K56;->d:LX/K59;

    .line 2769905
    iput-object v0, v1, LX/K54;->o:LX/K59;

    .line 2769906
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2769907
    return-void

    .line 2769908
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2769909
    :cond_1
    iget v0, p0, LX/K56;->k:I

    .line 2769910
    iput v0, v1, LX/K54;->g:I

    .line 2769911
    goto :goto_1
.end method
