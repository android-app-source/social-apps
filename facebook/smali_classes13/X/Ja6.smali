.class public final LX/Ja6;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Ja6;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ja4;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Ja7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2709553
    const/4 v0, 0x0

    sput-object v0, LX/Ja6;->a:LX/Ja6;

    .line 2709554
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ja6;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2709555
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2709556
    new-instance v0, LX/Ja7;

    invoke-direct {v0}, LX/Ja7;-><init>()V

    iput-object v0, p0, LX/Ja6;->c:LX/Ja7;

    .line 2709557
    return-void
.end method

.method public static declared-synchronized q()LX/Ja6;
    .locals 2

    .prologue
    .line 2709558
    const-class v1, LX/Ja6;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Ja6;->a:LX/Ja6;

    if-nez v0, :cond_0

    .line 2709559
    new-instance v0, LX/Ja6;

    invoke-direct {v0}, LX/Ja6;-><init>()V

    sput-object v0, LX/Ja6;->a:LX/Ja6;

    .line 2709560
    :cond_0
    sget-object v0, LX/Ja6;->a:LX/Ja6;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2709561
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2709562
    check-cast p2, LX/Ja5;

    .line 2709563
    iget-object v0, p2, LX/Ja5;->a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2709564
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const p0, 0x7f0a00e7

    invoke-virtual {v2, p0}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f0b0a6b

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const p0, 0x7f0b0034

    invoke-interface {v2, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const/4 p0, 0x4

    invoke-interface {v2, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne v0, v1, :cond_0

    const v1, 0x7f080f87

    :goto_0
    invoke-virtual {p0, v1}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b02b3

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a00bd

    invoke-virtual {v1, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const p0, 0x7f0b02ab

    invoke-interface {v1, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 p0, 0x7

    const p2, 0x7f0b0061

    invoke-interface {v1, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const p0, 0x7f0a004f

    invoke-interface {v1, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v1, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2709565
    return-object v0

    :cond_0
    const v1, 0x7f080f88

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2709566
    invoke-static {}, LX/1dS;->b()V

    .line 2709567
    const/4 v0, 0x0

    return-object v0
.end method
