.class public LX/Jtg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;

.field private final d:LX/0W3;

.field private final e:LX/0ad;

.field private final f:LX/33I;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2747376
    const-class v0, LX/Jtg;

    sput-object v0, LX/Jtg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Uh;LX/0W3;LX/0ad;LX/33I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0ad;",
            "LX/33I;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2747377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2747378
    iput-object p1, p0, LX/Jtg;->b:LX/0Or;

    .line 2747379
    iput-object p2, p0, LX/Jtg;->c:LX/0Uh;

    .line 2747380
    iput-object p3, p0, LX/Jtg;->d:LX/0W3;

    .line 2747381
    iput-object p4, p0, LX/Jtg;->e:LX/0ad;

    .line 2747382
    iput-object p5, p0, LX/Jtg;->f:LX/33I;

    .line 2747383
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    .line 2747384
    iget-object v0, p0, LX/Jtg;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 2747385
    const/16 v1, 0x110

    invoke-virtual {v0, v1}, LX/0W3;->a(I)LX/0W4;

    move-result-object v1

    .line 2747386
    if-nez v1, :cond_1

    .line 2747387
    :cond_0
    return v12

    .line 2747388
    :cond_1
    sget-wide v2, LX/0X5;->kD:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0xfa

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747389
    sget-wide v2, LX/0X5;->kE:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0xfc

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747390
    sget-wide v2, LX/0X5;->kG:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x10a

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747391
    sget-wide v2, LX/0X5;->kH:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x10b

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747392
    sget-wide v2, LX/0X5;->kI:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x11f

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747393
    sget-wide v2, LX/0X5;->kJ:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x120

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747394
    sget-wide v2, LX/0X5;->kK:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x131

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747395
    sget-wide v2, LX/0X5;->kL:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x132

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747396
    sget-wide v2, LX/0X5;->kM:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x133

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747397
    sget-wide v2, LX/0X5;->kN:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x174

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747398
    sget-wide v2, LX/0X5;->kO:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x176

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747399
    sget-wide v2, LX/0X5;->kP:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x18e

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747400
    sget-wide v2, LX/0X5;->kQ:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x194

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747401
    sget-wide v2, LX/0X5;->kR:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1a2

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747402
    sget-wide v2, LX/0X5;->kS:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1b0

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747403
    sget-wide v2, LX/0X5;->kT:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1b2

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747404
    sget-wide v2, LX/0X5;->kU:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1b3

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747405
    sget-wide v2, LX/0X5;->kV:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1c6

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747406
    sget-wide v2, LX/0X5;->kW:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1d1

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747407
    sget-wide v2, LX/0X5;->kX:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1d2

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747408
    sget-wide v2, LX/0X5;->kY:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1d3

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747409
    sget-wide v2, LX/0X5;->kZ:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x1ef

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747410
    sget-wide v2, LX/0X5;->la:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x217

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747411
    sget-wide v2, LX/0X5;->lb:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x25c

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747412
    sget-wide v2, LX/0X5;->lc:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x517

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747413
    sget-wide v2, LX/0X5;->ld:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x538

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747414
    sget-wide v2, LX/0X5;->le:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x53e

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747415
    sget-wide v2, LX/0X5;->lf:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x543

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747416
    sget-wide v2, LX/0X5;->lg:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x559

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747417
    sget-wide v2, LX/0X5;->lh:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x55f

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747418
    sget-wide v2, LX/0X5;->li:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x566

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747419
    sget-wide v2, LX/0X5;->lj:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x56b

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747420
    sget-wide v2, LX/0X5;->lk:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x59c

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747421
    sget-wide v2, LX/0X5;->ll:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x5d2

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747422
    sget-wide v2, LX/0X5;->lm:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x5d3

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747423
    sget-wide v2, LX/0X5;->ln:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x5d4

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747424
    sget-wide v2, LX/0X5;->lo:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x624

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747425
    sget-wide v2, LX/0X5;->lp:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x650

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747426
    sget-wide v2, LX/0X5;->lq:J

    iget-object v4, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v5, 0x67d

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->c(JZ)V

    .line 2747427
    sget-wide v2, LX/0X5;->lr:J

    iget-object v1, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v4, 0x374

    invoke-virtual {v1, v4, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747428
    sget-wide v2, LX/0X5;->ls:J

    iget-object v1, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v4, 0x375

    invoke-virtual {v1, v4, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747429
    sget-wide v2, LX/0X5;->lu:J

    iget-object v1, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v4, 0x474

    invoke-virtual {v1, v4, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747430
    sget-wide v2, LX/0X5;->lv:J

    iget-object v1, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v4, 0x218

    invoke-virtual {v1, v4, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747431
    sget-wide v2, LX/0X5;->lt:J

    iget-object v1, p0, LX/Jtg;->c:LX/0Uh;

    const/16 v4, 0x459

    invoke-virtual {v1, v4, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747432
    sget-wide v2, LX/0X5;->lx:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget-short v9, LX/1FD;->n:S

    invoke-interface {v1, v4, v5, v9, v8}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747433
    sget-wide v2, LX/0X5;->ly:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v9, LX/1FD;->r:F

    invoke-interface {v1, v4, v5, v9, v10}, LX/0ad;->a(LX/0c0;LX/0c1;FF)F

    move-result v1

    float-to-double v4, v1

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->b(JD)V

    .line 2747434
    sget-wide v2, LX/0X5;->lz:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v9, LX/1FD;->j:F

    invoke-interface {v1, v4, v5, v9, v10}, LX/0ad;->a(LX/0c0;LX/0c1;FF)F

    move-result v1

    float-to-double v4, v1

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->b(JD)V

    .line 2747435
    sget-wide v2, LX/0X5;->lD:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v9, LX/1FD;->q:I

    invoke-interface {v1, v4, v5, v9, v8}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    int-to-long v4, v1

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->b(JJ)V

    .line 2747436
    sget-wide v2, LX/0X5;->lE:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v9, LX/1FD;->o:I

    invoke-interface {v1, v4, v5, v9, v8}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    int-to-long v4, v1

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->b(JJ)V

    .line 2747437
    sget-wide v10, LX/0X5;->lA:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-wide v4, LX/1FD;->k:J

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v2

    invoke-interface {v0, v10, v11, v2, v3}, LX/0W4;->b(JJ)V

    .line 2747438
    sget-wide v10, LX/0X5;->lB:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-wide v4, LX/1FD;->l:J

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v2

    invoke-interface {v0, v10, v11, v2, v3}, LX/0W4;->b(JJ)V

    .line 2747439
    sget-wide v10, LX/0X5;->lC:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-wide v4, LX/1FD;->m:J

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v2

    invoke-interface {v0, v10, v11, v2, v3}, LX/0W4;->b(JJ)V

    .line 2747440
    sget-wide v2, LX/0X5;->lw:J

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget-short v6, LX/1FD;->p:S

    invoke-interface {v1, v4, v5, v6, v8}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->c(JZ)V

    .line 2747441
    iget-object v0, p0, LX/Jtg;->f:LX/33I;

    iget-object v1, p0, LX/Jtg;->e:LX/0ad;

    .line 2747442
    iput-object v1, v0, LX/33I;->f:LX/0ad;

    .line 2747443
    iget-object v0, p0, LX/Jtg;->f:LX/33I;

    .line 2747444
    iget-object v1, v0, LX/33I;->c:LX/33H;

    move-object v1, v1

    .line 2747445
    iget v0, v1, LX/33H;->b:I

    move v2, v0

    .line 2747446
    move v0, v8

    .line 2747447
    :goto_0
    if-ge v0, v2, :cond_0

    .line 2747448
    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v4

    .line 2747449
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    .line 2747450
    iget-object v3, p0, LX/Jtg;->f:LX/33I;

    invoke-virtual {v3, v0, v4, v5}, LX/33I;->a(IJ)V

    .line 2747451
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
