.class public LX/Jxn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/03V;

.field public final c:LX/1mR;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/Eny;


# direct methods
.method public constructor <init>(LX/0Or;LX/03V;LX/1mR;Ljava/util/concurrent/Executor;LX/Eny;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/Eny;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1mR;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/entitycards/model/EntityCardMutationService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2753509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753510
    iput-object p1, p0, LX/Jxn;->a:LX/0Or;

    .line 2753511
    iput-object p2, p0, LX/Jxn;->b:LX/03V;

    .line 2753512
    iput-object p3, p0, LX/Jxn;->c:LX/1mR;

    .line 2753513
    iput-object p4, p0, LX/Jxn;->d:Ljava/util/concurrent/Executor;

    .line 2753514
    iput-object p5, p0, LX/Jxn;->e:LX/Eny;

    .line 2753515
    return-void
.end method
