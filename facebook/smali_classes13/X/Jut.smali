.class public final LX/Jut;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Landroid/content/pm/Signature;

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLandroid/content/pm/Signature;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Landroid/content/pm/Signature;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2749544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2749545
    iput-boolean p1, p0, LX/Jut;->a:Z

    .line 2749546
    iput-object p2, p0, LX/Jut;->b:Landroid/content/pm/Signature;

    .line 2749547
    invoke-static {p3}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/Jut;->c:LX/0Rf;

    .line 2749548
    return-void
.end method

.method public static a(Landroid/content/pm/Signature;Ljava/util/Set;)LX/Jut;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/Signature;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/Jut;"
        }
    .end annotation

    .prologue
    .line 2749549
    new-instance v0, LX/Jut;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0, p1}, LX/Jut;-><init>(ZLandroid/content/pm/Signature;Ljava/util/Set;)V

    return-object v0
.end method
