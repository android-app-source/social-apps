.class public final enum LX/JvA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JvA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JvA;

.field public static final enum PRELOADED:LX/JvA;

.field public static final enum SIDELOADED:LX/JvA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2749834
    new-instance v0, LX/JvA;

    const-string v1, "PRELOADED"

    invoke-direct {v0, v1, v2}, LX/JvA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JvA;->PRELOADED:LX/JvA;

    .line 2749835
    new-instance v0, LX/JvA;

    const-string v1, "SIDELOADED"

    invoke-direct {v0, v1, v3}, LX/JvA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JvA;->SIDELOADED:LX/JvA;

    .line 2749836
    const/4 v0, 0x2

    new-array v0, v0, [LX/JvA;

    sget-object v1, LX/JvA;->PRELOADED:LX/JvA;

    aput-object v1, v0, v2

    sget-object v1, LX/JvA;->SIDELOADED:LX/JvA;

    aput-object v1, v0, v3

    sput-object v0, LX/JvA;->$VALUES:[LX/JvA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2749837
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JvA;
    .locals 1

    .prologue
    .line 2749833
    const-class v0, LX/JvA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JvA;

    return-object v0
.end method

.method public static values()[LX/JvA;
    .locals 1

    .prologue
    .line 2749832
    sget-object v0, LX/JvA;->$VALUES:[LX/JvA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JvA;

    return-object v0
.end method
