.class public LX/Jgm;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/FJw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JhR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/view/View;

.field public final f:Lcom/facebook/user/tiles/UserTileView;

.field public g:LX/Jgn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2723788
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/Jgm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2723789
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2723780
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2723781
    const-class v0, LX/Jgm;

    invoke-static {v0, p0}, LX/Jgm;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2723782
    const v0, 0x7f030c77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2723783
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jgm;->c:Landroid/widget/TextView;

    .line 2723784
    const v0, 0x7f0d1e9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jgm;->d:Landroid/widget/TextView;

    .line 2723785
    const v0, 0x7f0d1e99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Jgm;->f:Lcom/facebook/user/tiles/UserTileView;

    .line 2723786
    const v0, 0x7f0d1e97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jgm;->e:Landroid/view/View;

    .line 2723787
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/Jgm;

    invoke-static {v2}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object v1

    check-cast v1, LX/FJw;

    const/16 p0, 0x270e

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/Jgm;->a:LX/FJw;

    iput-object v2, p1, LX/Jgm;->b:LX/0Ot;

    return-void
.end method
