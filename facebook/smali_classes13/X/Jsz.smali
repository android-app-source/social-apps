.class public final enum LX/Jsz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jsz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jsz;

.field public static final enum ADD_MEMBERS:LX/Jsz;

.field public static final enum ADMIN:LX/Jsz;

.field public static final enum ATTACHEMENT:LX/Jsz;

.field public static final enum REGULAR:LX/Jsz;

.field public static final enum REMOVE_MEMBERS:LX/Jsz;

.field public static final enum SENDER_NAME:LX/Jsz;

.field public static final enum SET_IMAGE:LX/Jsz;

.field public static final enum SET_NAME:LX/Jsz;

.field public static final enum SNIPPET:LX/Jsz;

.field public static final enum STICKER:LX/Jsz;

.field public static final enum TIME_DIVIDER:LX/Jsz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2746135
    new-instance v0, LX/Jsz;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v3}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->REGULAR:LX/Jsz;

    .line 2746136
    new-instance v0, LX/Jsz;

    const-string v1, "TIME_DIVIDER"

    invoke-direct {v0, v1, v4}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->TIME_DIVIDER:LX/Jsz;

    .line 2746137
    new-instance v0, LX/Jsz;

    const-string v1, "SENDER_NAME"

    invoke-direct {v0, v1, v5}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->SENDER_NAME:LX/Jsz;

    .line 2746138
    new-instance v0, LX/Jsz;

    const-string v1, "ADD_MEMBERS"

    invoke-direct {v0, v1, v6}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->ADD_MEMBERS:LX/Jsz;

    .line 2746139
    new-instance v0, LX/Jsz;

    const-string v1, "SNIPPET"

    invoke-direct {v0, v1, v7}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->SNIPPET:LX/Jsz;

    .line 2746140
    new-instance v0, LX/Jsz;

    const-string v1, "STICKER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->STICKER:LX/Jsz;

    .line 2746141
    new-instance v0, LX/Jsz;

    const-string v1, "ATTACHEMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->ATTACHEMENT:LX/Jsz;

    .line 2746142
    new-instance v0, LX/Jsz;

    const-string v1, "REMOVE_MEMBERS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->REMOVE_MEMBERS:LX/Jsz;

    .line 2746143
    new-instance v0, LX/Jsz;

    const-string v1, "SET_NAME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->SET_NAME:LX/Jsz;

    .line 2746144
    new-instance v0, LX/Jsz;

    const-string v1, "SET_IMAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->SET_IMAGE:LX/Jsz;

    .line 2746145
    new-instance v0, LX/Jsz;

    const-string v1, "ADMIN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Jsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jsz;->ADMIN:LX/Jsz;

    .line 2746146
    const/16 v0, 0xb

    new-array v0, v0, [LX/Jsz;

    sget-object v1, LX/Jsz;->REGULAR:LX/Jsz;

    aput-object v1, v0, v3

    sget-object v1, LX/Jsz;->TIME_DIVIDER:LX/Jsz;

    aput-object v1, v0, v4

    sget-object v1, LX/Jsz;->SENDER_NAME:LX/Jsz;

    aput-object v1, v0, v5

    sget-object v1, LX/Jsz;->ADD_MEMBERS:LX/Jsz;

    aput-object v1, v0, v6

    sget-object v1, LX/Jsz;->SNIPPET:LX/Jsz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Jsz;->STICKER:LX/Jsz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Jsz;->ATTACHEMENT:LX/Jsz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Jsz;->REMOVE_MEMBERS:LX/Jsz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Jsz;->SET_NAME:LX/Jsz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Jsz;->SET_IMAGE:LX/Jsz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Jsz;->ADMIN:LX/Jsz;

    aput-object v2, v0, v1

    sput-object v0, LX/Jsz;->$VALUES:[LX/Jsz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2746134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jsz;
    .locals 1

    .prologue
    .line 2746133
    const-class v0, LX/Jsz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jsz;

    return-object v0
.end method

.method public static values()[LX/Jsz;
    .locals 1

    .prologue
    .line 2746132
    sget-object v0, LX/Jsz;->$VALUES:[LX/Jsz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jsz;

    return-object v0
.end method
