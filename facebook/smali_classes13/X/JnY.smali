.class public LX/JnY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:LX/0tX;

.field private final d:Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;LX/0tX;Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2734186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734187
    iput-object p1, p0, LX/JnY;->a:Ljava/util/concurrent/ExecutorService;

    .line 2734188
    iput-object p2, p0, LX/JnY;->b:Ljava/util/concurrent/Executor;

    .line 2734189
    iput-object p3, p0, LX/JnY;->c:LX/0tX;

    .line 2734190
    iput-object p4, p0, LX/JnY;->d:Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;

    .line 2734191
    return-void
.end method
