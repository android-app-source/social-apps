.class public LX/JmS;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:LX/JmM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private d:LX/1P1;

.field private e:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2732847
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732848
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732849
    iput-object v0, p0, LX/JmS;->b:LX/0Ot;

    .line 2732850
    invoke-direct {p0}, LX/JmS;->a()V

    .line 2732851
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732852
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732853
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732854
    iput-object v0, p0, LX/JmS;->b:LX/0Ot;

    .line 2732855
    invoke-direct {p0}, LX/JmS;->a()V

    .line 2732856
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732842
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732843
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732844
    iput-object v0, p0, LX/JmS;->b:LX/0Ot;

    .line 2732845
    invoke-direct {p0}, LX/JmS;->a()V

    .line 2732846
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2732834
    const-class v0, LX/JmS;

    invoke-static {v0, p0}, LX/JmS;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2732835
    const v0, 0x7f03089a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2732836
    const v0, 0x7f0d0b3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/JmS;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2732837
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/JmS;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/JmS;->d:LX/1P1;

    .line 2732838
    iget-object v0, p0, LX/JmS;->d:LX/1P1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2732839
    iget-object v0, p0, LX/JmS;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JmS;->d:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2732840
    iget-object v0, p0, LX/JmS;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JmS;->a:LX/JmM;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2732841
    return-void
.end method

.method private static a(LX/JmS;LX/JmM;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JmS;",
            "LX/JmM;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2732833
    iput-object p1, p0, LX/JmS;->a:LX/JmM;

    iput-object p2, p0, LX/JmS;->b:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/JmS;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/JmS;

    new-instance v0, LX/JmM;

    invoke-direct {v0}, LX/JmM;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/JmM;

    const/16 v2, 0xac0

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/JmS;->a(LX/JmS;LX/JmM;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public getInboxUnitItem()Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
    .locals 1

    .prologue
    .line 2732832
    iget-object v0, p0, LX/JmS;->e:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;

    return-object v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2732831
    iget-object v0, p0, LX/JmS;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public getTrackableItemAdapter()LX/Dct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Dct",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2732830
    iget-object v0, p0, LX/JmS;->a:LX/JmM;

    return-object v0
.end method

.method public setItem(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;)V
    .locals 3

    .prologue
    .line 2732818
    iget-object v0, p0, LX/JmS;->e:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JmS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x1ca

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JmS;->e:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;

    .line 2732819
    iget-object v1, v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    iget-object v2, p1, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;->a:LX/0Px;

    iget-object v2, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;->a:LX/0Px;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2732820
    if-eqz v0, :cond_0

    .line 2732821
    :goto_1
    return-void

    .line 2732822
    :cond_0
    iput-object p1, p0, LX/JmS;->e:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;

    .line 2732823
    iget-object v0, p0, LX/JmS;->a:LX/JmM;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;->a:LX/0Px;

    .line 2732824
    iput-object v1, v0, LX/JmM;->a:LX/0Px;

    .line 2732825
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2732826
    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setListener(LX/JmT;)V
    .locals 1
    .param p1    # LX/JmT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732827
    iget-object v0, p0, LX/JmS;->a:LX/JmM;

    .line 2732828
    iput-object p1, v0, LX/JmM;->b:LX/JmT;

    .line 2732829
    return-void
.end method
