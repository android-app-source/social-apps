.class public final LX/K3d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/K3f;


# direct methods
.method public constructor <init>(LX/K3f;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2765932
    iput-object p1, p0, LX/K3d;->b:LX/K3f;

    iput-object p2, p0, LX/K3d;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2765933
    sget-object v0, LX/K3f;->a:Ljava/lang/String;

    const-string v1, "Muxer job failed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2765934
    iget-object v0, p0, LX/K3d;->b:LX/K3f;

    invoke-static {v0}, LX/K3f;->d(LX/K3f;)V

    .line 2765935
    iget-object v0, p0, LX/K3d;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const v2, 0x66dacf6c

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2765936
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2765937
    check-cast p1, Ljava/lang/Boolean;

    .line 2765938
    iget-object v0, p0, LX/K3d;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x44f39548

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2765939
    return-void
.end method
