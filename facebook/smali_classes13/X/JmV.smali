.class public LX/JmV;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

.field public c:LX/JmU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2732952
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2732953
    invoke-direct {p0}, LX/JmV;->a()V

    .line 2732954
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732906
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2732907
    invoke-direct {p0}, LX/JmV;->a()V

    .line 2732908
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732949
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732950
    invoke-direct {p0}, LX/JmV;->a()V

    .line 2732951
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2732939
    const-class v0, LX/JmV;

    invoke-static {v0, p0}, LX/JmV;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2732940
    invoke-virtual {p0}, LX/JmV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b266f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JmV;->e:I

    .line 2732941
    invoke-virtual {p0}, LX/JmV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2670

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JmV;->d:I

    .line 2732942
    invoke-virtual {p0}, LX/JmV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b266e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JmV;->f:I

    .line 2732943
    const v0, 0x7f030ef1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2732944
    const v0, 0x7f0d1241

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/JmV;->a:Landroid/support/v4/view/ViewPager;

    .line 2732945
    const v0, 0x7f0d1240

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    iput-object v0, p0, LX/JmV;->b:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    .line 2732946
    iget-object v0, p0, LX/JmV;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/JmV;->c:LX/JmU;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2732947
    iget-object v0, p0, LX/JmV;->b:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    iget-object v1, p0, LX/JmV;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2732948
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/JmV;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    check-cast p0, LX/JmV;

    new-instance p1, LX/JmU;

    invoke-direct {p1}, LX/JmU;-><init>()V

    move-object p1, p1

    move-object v0, p1

    check-cast v0, LX/JmU;

    iput-object v0, p0, LX/JmV;->c:LX/JmU;

    return-void
.end method


# virtual methods
.method public getItemsInUnit()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/inbox2/items/InboxUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2732936
    iget-object v0, p0, LX/JmV;->g:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JmV;->g:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;->a:LX/0Px;

    :goto_0
    return-object v0

    .line 2732937
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2732938
    goto :goto_0
.end method

.method public getNumberOfItemsPerPage()I
    .locals 1

    .prologue
    .line 2732935
    iget-object v0, p0, LX/JmV;->c:LX/JmU;

    invoke-virtual {v0}, LX/JmU;->d()I

    move-result v0

    return v0
.end method

.method public getViewPager()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 2732934
    iget-object v0, p0, LX/JmV;->a:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, -0x1d5998ec

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2732926
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 2732927
    iget v1, p0, LX/JmV;->e:I

    iget v2, p0, LX/JmV;->d:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    div-int v1, p1, v1

    .line 2732928
    iget-object v2, p0, LX/JmV;->c:LX/JmU;

    const/4 v3, 0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2732929
    if-lez v1, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2732930
    iput v1, v2, LX/JmU;->a:I

    .line 2732931
    invoke-virtual {v2}, LX/0gG;->kV_()V

    .line 2732932
    const/16 v1, 0x2d

    const v2, -0x6144e815

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2732933
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public setItem(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;)V
    .locals 2

    .prologue
    .line 2732921
    iput-object p1, p0, LX/JmV;->g:Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;

    .line 2732922
    iget-object v0, p0, LX/JmV;->c:LX/JmU;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPaginatedUnitInboxItem;->a:LX/0Px;

    .line 2732923
    iput-object v1, v0, LX/JmU;->c:LX/0Px;

    .line 2732924
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2732925
    return-void
.end method

.method public setListener(LX/JmT;)V
    .locals 1
    .param p1    # LX/JmT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732918
    iget-object v0, p0, LX/JmV;->c:LX/JmU;

    .line 2732919
    iput-object p1, v0, LX/JmU;->d:LX/JmT;

    .line 2732920
    return-void
.end method

.method public setNumRows(I)V
    .locals 2

    .prologue
    .line 2732912
    iget-object v0, p0, LX/JmV;->c:LX/JmU;

    .line 2732913
    iput p1, v0, LX/JmU;->b:I

    .line 2732914
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2732915
    iget-object v0, p0, LX/JmV;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, LX/JmV;->f:I

    mul-int/2addr v1, p1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2732916
    invoke-virtual {p0}, LX/JmV;->requestLayout()V

    .line 2732917
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 1
    .param p1    # LX/0hc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732909
    iget-object v0, p0, LX/JmV;->b:Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;

    .line 2732910
    iput-object p1, v0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    .line 2732911
    return-void
.end method
