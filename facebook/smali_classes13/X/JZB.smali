.class public final LX/JZB;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JZC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pm;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public final synthetic e:LX/JZC;


# direct methods
.method public constructor <init>(LX/JZC;)V
    .locals 1

    .prologue
    .line 2707931
    iput-object p1, p0, LX/JZB;->e:LX/JZC;

    .line 2707932
    move-object v0, p1

    .line 2707933
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2707934
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2707955
    const-string v0, "TarotDigestAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2707935
    if-ne p0, p1, :cond_1

    .line 2707936
    :cond_0
    :goto_0
    return v0

    .line 2707937
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2707938
    goto :goto_0

    .line 2707939
    :cond_3
    check-cast p1, LX/JZB;

    .line 2707940
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2707941
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2707942
    if-eq v2, v3, :cond_0

    .line 2707943
    iget-object v2, p0, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2707944
    goto :goto_0

    .line 2707945
    :cond_5
    iget-object v2, p1, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2707946
    :cond_6
    iget-object v2, p0, LX/JZB;->b:LX/1Pm;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JZB;->b:LX/1Pm;

    iget-object v3, p1, LX/JZB;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2707947
    goto :goto_0

    .line 2707948
    :cond_8
    iget-object v2, p1, LX/JZB;->b:LX/1Pm;

    if-nez v2, :cond_7

    .line 2707949
    :cond_9
    iget-object v2, p0, LX/JZB;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JZB;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JZB;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2707950
    goto :goto_0

    .line 2707951
    :cond_b
    iget-object v2, p1, LX/JZB;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2707952
    :cond_c
    iget-object v2, p0, LX/JZB;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JZB;->d:Ljava/lang/Boolean;

    iget-object v3, p1, LX/JZB;->d:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2707953
    goto :goto_0

    .line 2707954
    :cond_d
    iget-object v2, p1, LX/JZB;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
