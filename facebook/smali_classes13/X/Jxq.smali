.class public LX/Jxq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

.field public b:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)V
    .locals 0
    .param p1    # Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2753655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753656
    iput-object p1, p0, LX/Jxq;->a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753657
    return-void
.end method

.method private static a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)LX/0lF;
    .locals 9

    .prologue
    .line 2753618
    sget-object v0, LX/Jxp;->a:[I

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2753619
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized card type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2753620
    :pswitch_0
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2753621
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2753622
    const-string v1, "selected_and_suggested_tags"

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v2

    .line 2753623
    new-instance v5, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v3}, LX/162;-><init>(LX/0mC;)V

    .line 2753624
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;

    .line 2753625
    new-instance v7, LX/0m9;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v8}, LX/0m9;-><init>(LX/0mC;)V

    .line 2753626
    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 2753627
    const-string v8, "curation_tag_name"

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2753628
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2753629
    const-string v8, "curation_tag_id"

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2753630
    :cond_1
    const-string v8, "curation_tag_visibility"

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v8, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2753631
    invoke-virtual {v5, v7}, LX/162;->a(LX/0lF;)LX/162;

    .line 2753632
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2753633
    :cond_2
    move-object v2, v5

    .line 2753634
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2753635
    :cond_3
    move-object v0, v0

    .line 2753636
    :goto_1
    return-object v0

    .line 2753637
    :pswitch_1
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2753638
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->r()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2753639
    const-string v1, "mutuality_items"

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->r()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v2

    .line 2753640
    new-instance v5, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v3}, LX/162;-><init>(LX/0mC;)V

    .line 2753641
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_6

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5vW;

    .line 2753642
    new-instance v7, LX/0m9;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v8}, LX/0m9;-><init>(LX/0mC;)V

    .line 2753643
    invoke-interface {v3}, LX/5vW;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 2753644
    const-string v8, "timeline_context_list_item_type"

    invoke-interface {v3}, LX/5vW;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2753645
    :cond_4
    invoke-interface {v3}, LX/5vW;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 2753646
    const-string v8, "title"

    invoke-interface {v3}, LX/5vW;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2753647
    :cond_5
    const-string v8, "url"

    invoke-interface {v3}, LX/5vW;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v8, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2753648
    invoke-virtual {v5, v7}, LX/162;->a(LX/0lF;)LX/162;

    .line 2753649
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 2753650
    :cond_6
    move-object v2, v5

    .line 2753651
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2753652
    :cond_7
    move-object v0, v0

    .line 2753653
    goto :goto_1

    .line 2753654
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0oG;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IZ)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/profile/discovery/DiscoveryCurationAnalyticsLogger$SectionType;
        .end annotation
    .end param

    .prologue
    .line 2753580
    const-string v0, "bucket_id"

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753581
    const-string v0, "candidate_position"

    invoke-virtual {p0, v0, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2753582
    const-string v0, "extra"

    const/4 v1, 0x0

    .line 2753583
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2753584
    const-string v3, "badge_count"

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lc_()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2753585
    const-string v3, "Highlighted"

    invoke-static {p2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2753586
    const-string v3, "photos"

    const/4 v6, 0x0

    .line 2753587
    new-instance v8, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v4}, LX/162;-><init>(LX/0mC;)V

    .line 2753588
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2753589
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object p3

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result p2

    move v7, v6

    .line 2753590
    :goto_0
    if-ge v7, p2, :cond_2

    invoke-virtual {p3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    .line 2753591
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePhotoModel;

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    .line 2753592
    :goto_1
    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePhotoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePhotoModel;->b()Ljava/lang/String;

    move-result-object v4

    :goto_2
    invoke-virtual {v8, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2753593
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_0

    :cond_0
    move v5, v6

    .line 2753594
    goto :goto_1

    .line 2753595
    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    .line 2753596
    :cond_2
    move-object v4, v8

    .line 2753597
    invoke-virtual {v2, v3, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2753598
    const-string v3, "unread_badge"

    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2753599
    :cond_3
    :goto_3
    move-object v1, v2

    .line 2753600
    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 2753601
    const-string v1, "is_last"

    if-eqz p4, :cond_4

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p0, v1, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2753602
    return-void

    .line 2753603
    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    .line 2753604
    :cond_5
    const-string v3, "Normal"

    invoke-static {p2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2753605
    const-string v3, "photos"

    .line 2753606
    new-instance v6, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/162;-><init>(LX/0mC;)V

    .line 2753607
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 2753608
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_5
    if-ge v5, v8, :cond_8

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    .line 2753609
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object p3

    if-eqz p3, :cond_6

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object p3

    if-eqz p3, :cond_6

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePhotoModel;

    move-result-object p3

    if-eqz p3, :cond_6

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePhotoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePhotoModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2753610
    :goto_6
    invoke-virtual {v6, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2753611
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 2753612
    :cond_6
    const/4 v4, 0x0

    goto :goto_6

    .line 2753613
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 2753614
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2753615
    :cond_8
    move-object v4, v6

    .line 2753616
    invoke-virtual {v2, v3, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2753617
    const-string v3, "unread_badge"

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lc_()I

    move-result v4

    if-lez v4, :cond_9

    const/4 v1, 0x1

    :cond_9
    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto/16 :goto_3
.end method

.method private a(LX/0oG;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2753577
    const-string v0, "people_discovery"

    invoke-virtual {p1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2753578
    invoke-static {p0, p1, p2}, LX/Jxq;->b(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753579
    return-void
.end method

.method public static a(LX/0oG;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/profile/discovery/DiscoveryCurationAnalyticsLogger$SectionType;
        .end annotation
    .end param

    .prologue
    .line 2753521
    const-string v0, "section_type"

    invoke-virtual {p0, v0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753522
    const-string v0, "section_position"

    invoke-virtual {p0, v0, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2753523
    return-void
.end method

.method public static a(LX/Jxq;LX/0oG;)V
    .locals 2

    .prologue
    .line 2753573
    const-string v0, "bucket_list_session_id"

    iget-object v1, p0, LX/Jxq;->a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753574
    iget-object p0, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    move-object v1, p0

    .line 2753575
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753576
    return-void
.end method

.method public static a(LX/Jxq;Ljava/lang/Object;LX/0oG;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2753566
    invoke-static {p0, p2, p3}, LX/Jxq;->b(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753567
    invoke-static {p0, p2}, LX/Jxq;->a(LX/Jxq;LX/0oG;)V

    .line 2753568
    invoke-static {p0, p2}, LX/Jxq;->b(LX/Jxq;LX/0oG;)V

    .line 2753569
    instance-of v0, p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    if-eqz v0, :cond_0

    .line 2753570
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2753571
    invoke-static {p1, p2}, LX/Jxq;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/0oG;)V

    .line 2753572
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/0oG;)V
    .locals 2

    .prologue
    .line 2753557
    const-string v0, "extra"

    invoke-static {p0}, LX/Jxq;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)LX/0lF;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 2753558
    const-string v0, "entity_fbid"

    .line 2753559
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2753560
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753561
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2753562
    const-string v0, "entity_card_type"

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753563
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2753564
    const-string v0, "userid"

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753565
    :cond_1
    return-void
.end method

.method public static b(LX/Jxq;LX/0oG;)V
    .locals 2

    .prologue
    .line 2753553
    const-string v0, "bucket_session_id"

    iget-object v1, p0, LX/Jxq;->a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753554
    iget-object p0, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    move-object v1, p0

    .line 2753555
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753556
    return-void
.end method

.method public static b(LX/Jxq;LX/0oG;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2753542
    const-string v0, "event_type"

    invoke-virtual {p1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753543
    const-string v0, "discovery_session_referrer_type"

    iget-object v1, p0, LX/Jxq;->a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753544
    iget-object p2, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    move-object v1, p2

    .line 2753545
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753546
    const-string v0, "discovery_session_referrer_id"

    iget-object v1, p0, LX/Jxq;->a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753547
    iget-object p2, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    move-object v1, p2

    .line 2753548
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753549
    const-string v0, "discovery_session_id"

    iget-object v1, p0, LX/Jxq;->a:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2753550
    iget-object p0, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    move-object v1, p0

    .line 2753551
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2753552
    return-void
.end method

.method public static c(LX/Jxq;LX/0oG;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2753539
    invoke-direct {p0, p1, p2}, LX/Jxq;->a(LX/0oG;Ljava/lang/String;)V

    .line 2753540
    invoke-static {p0, p1}, LX/Jxq;->a(LX/Jxq;LX/0oG;)V

    .line 2753541
    return-void
.end method

.method public static d(LX/Jxq;LX/0oG;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2753536
    invoke-static {p0, p1, p2}, LX/Jxq;->c(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753537
    invoke-static {p0, p1}, LX/Jxq;->b(LX/Jxq;LX/0oG;)V

    .line 2753538
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2753531
    iget-object v0, p0, LX/Jxq;->b:LX/0Zb;

    const-string v1, "profile_discovery_event"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2753532
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2753533
    const-string v1, "discovery_session_impression"

    invoke-direct {p0, v0, v1}, LX/Jxq;->a(LX/0oG;Ljava/lang/String;)V

    .line 2753534
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2753535
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IIZ)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/profile/discovery/DiscoveryCurationAnalyticsLogger$SectionType;
        .end annotation
    .end param

    .prologue
    .line 2753524
    iget-object v0, p0, LX/Jxq;->b:LX/0Zb;

    const-string v1, "profile_discovery_event"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2753525
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2753526
    const-string v1, "bucket_list_candidate_vpv"

    invoke-static {p0, v0, v1}, LX/Jxq;->c(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753527
    invoke-static {v0, p2, p3}, LX/Jxq;->a(LX/0oG;Ljava/lang/String;I)V

    .line 2753528
    invoke-static {v0, p1, p2, p4, p5}, LX/Jxq;->a(LX/0oG;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IZ)V

    .line 2753529
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2753530
    :cond_0
    return-void
.end method
