.class public LX/JrJ;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/Jrc;

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742863
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrJ;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDs;LX/Jrc;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2742857
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2742858
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742859
    iput-object v0, p0, LX/JrJ;->c:LX/0Ot;

    .line 2742860
    iput-object p1, p0, LX/JrJ;->a:LX/FDs;

    .line 2742861
    iput-object p2, p0, LX/JrJ;->b:LX/Jrc;

    .line 2742862
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742854
    invoke-virtual {p1}, LX/6kW;->o()LX/6kO;

    move-result-object v0

    .line 2742855
    iget-object v1, p0, LX/JrJ;->b:LX/Jrc;

    iget-object v0, v0, LX/6kO;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742856
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrJ;
    .locals 8

    .prologue
    .line 2742823
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742824
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742825
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742826
    if-nez v1, :cond_0

    .line 2742827
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742828
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742829
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742830
    sget-object v1, LX/JrJ;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742831
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742832
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742833
    :cond_1
    if-nez v1, :cond_4

    .line 2742834
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742835
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742836
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742837
    new-instance p0, LX/JrJ;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v7

    check-cast v7, LX/Jrc;

    invoke-direct {p0, v1, v7}, LX/JrJ;-><init>(LX/FDs;LX/Jrc;)V

    .line 2742838
    const/16 v1, 0xce5

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742839
    iput-object v1, p0, LX/JrJ;->c:LX/0Ot;

    .line 2742840
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742841
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742842
    if-nez v1, :cond_2

    .line 2742843
    sget-object v0, LX/JrJ;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742844
    :goto_1
    if-eqz v0, :cond_3

    .line 2742845
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742846
    :goto_3
    check-cast v0, LX/JrJ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742847
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742848
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742849
    :catchall_1
    move-exception v0

    .line 2742850
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742851
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742852
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742853
    :cond_2
    :try_start_8
    sget-object v0, LX/JrJ;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrJ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static c(LX/6kW;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2742864
    invoke-virtual {p0}, LX/6kW;->o()LX/6kO;

    move-result-object v1

    iget-object v1, v1, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742805
    check-cast p1, LX/6kW;

    .line 2742806
    invoke-static {p1}, LX/JrJ;->c(LX/6kW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2742807
    invoke-direct {p0, p1}, LX/JrJ;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    .line 2742808
    :goto_0
    return-object v0

    .line 2742809
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742810
    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742818
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-static {v0}, LX/JrJ;->c(LX/6kW;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2742819
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->o()LX/6kO;

    move-result-object v0

    .line 2742820
    iget-object v1, p0, LX/JrJ;->b:LX/Jrc;

    iget-object v0, v0, LX/6kO;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742821
    iget-object v1, p0, LX/JrJ;->a:LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742822
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742813
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->o()LX/6kO;

    move-result-object v1

    .line 2742814
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-static {v0}, LX/JrJ;->c(LX/6kW;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2742815
    iget-object v0, p0, LX/JrJ;->b:LX/Jrc;

    iget-object v1, v1, LX/6kO;->threadKey:LX/6l9;

    invoke-virtual {v0, v1}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2742816
    iget-object v0, p0, LX/JrJ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v0, v2, v1}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742817
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742812
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrJ;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2742811
    check-cast p1, LX/6kW;

    invoke-static {p1}, LX/JrJ;->c(LX/6kW;)Z

    move-result v0

    return v0
.end method
