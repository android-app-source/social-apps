.class public final enum LX/K6K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K6K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K6K;

.field public static final enum COVER:LX/K6K;

.field public static final enum END_CARD:LX/K6K;

.field public static final enum IMAGE:LX/K6K;

.field public static final enum UNKNOWN:LX/K6K;

.field public static final enum VIDEO:LX/K6K;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2771398
    new-instance v0, LX/K6K;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, LX/K6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6K;->IMAGE:LX/K6K;

    .line 2771399
    new-instance v0, LX/K6K;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/K6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6K;->VIDEO:LX/K6K;

    .line 2771400
    new-instance v0, LX/K6K;

    const-string v1, "COVER"

    invoke-direct {v0, v1, v4}, LX/K6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6K;->COVER:LX/K6K;

    .line 2771401
    new-instance v0, LX/K6K;

    const-string v1, "END_CARD"

    invoke-direct {v0, v1, v5}, LX/K6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6K;->END_CARD:LX/K6K;

    .line 2771402
    new-instance v0, LX/K6K;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/K6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K6K;->UNKNOWN:LX/K6K;

    .line 2771403
    const/4 v0, 0x5

    new-array v0, v0, [LX/K6K;

    sget-object v1, LX/K6K;->IMAGE:LX/K6K;

    aput-object v1, v0, v2

    sget-object v1, LX/K6K;->VIDEO:LX/K6K;

    aput-object v1, v0, v3

    sget-object v1, LX/K6K;->COVER:LX/K6K;

    aput-object v1, v0, v4

    sget-object v1, LX/K6K;->END_CARD:LX/K6K;

    aput-object v1, v0, v5

    sget-object v1, LX/K6K;->UNKNOWN:LX/K6K;

    aput-object v1, v0, v6

    sput-object v0, LX/K6K;->$VALUES:[LX/K6K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2771404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K6K;
    .locals 1

    .prologue
    .line 2771405
    const-class v0, LX/K6K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K6K;

    return-object v0
.end method

.method public static values()[LX/K6K;
    .locals 1

    .prologue
    .line 2771406
    sget-object v0, LX/K6K;->$VALUES:[LX/K6K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K6K;

    return-object v0
.end method
