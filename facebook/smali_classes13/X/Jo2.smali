.class public final LX/Jo2;
.super LX/6LW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6LW",
        "<",
        "Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;",
        "LX/Dh8;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jo3;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/Dh8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Jo3;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/Dh8;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2734859
    iput-object p1, p0, LX/Jo2;->a:LX/Jo3;

    invoke-direct {p0}, LX/6LW;-><init>()V

    .line 2734860
    iput-object p2, p0, LX/Jo2;->b:Ljava/lang/String;

    .line 2734861
    iput-object p3, p0, LX/Jo2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2734862
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2734863
    check-cast p2, LX/Dh8;

    .line 2734864
    iget-object v1, p0, LX/Jo2;->a:LX/Jo3;

    monitor-enter v1

    .line 2734865
    :try_start_0
    iget-object v0, p0, LX/Jo2;->a:LX/Jo3;

    iget-object v0, v0, LX/Jo3;->a:Ljava/util/Map;

    iget-object v2, p0, LX/Jo2;->b:Ljava/lang/String;

    sget-object v3, LX/Jnw;->COMPLETED:LX/Jnw;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734866
    iget-object v0, p0, LX/Jo2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x31aef806

    invoke-static {v0, p2, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2734867
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2734868
    check-cast p2, Ljava/lang/Throwable;

    .line 2734869
    iget-object v1, p0, LX/Jo2;->a:LX/Jo3;

    monitor-enter v1

    .line 2734870
    :try_start_0
    iget-object v0, p0, LX/Jo2;->a:LX/Jo3;

    iget-object v0, v0, LX/Jo3;->a:Ljava/util/Map;

    iget-object v2, p0, LX/Jo2;->b:Ljava/lang/String;

    sget-object v3, LX/Jnw;->NOT_STARTED:LX/Jnw;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734871
    iget-object v0, p0, LX/Jo2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2734872
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
