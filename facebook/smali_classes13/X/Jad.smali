.class public LX/Jad;
.super LX/DK1;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/JaX;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/DN3;


# direct methods
.method public constructor <init>(Landroid/content/Intent;LX/0gc;Landroid/content/res/Resources;LX/DN3;)V
    .locals 4
    .param p1    # Landroid/content/Intent;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715628
    invoke-direct {p0, p2}, LX/DK1;-><init>(LX/0gc;)V

    .line 2715629
    iput-object p3, p0, LX/Jad;->a:Landroid/content/res/Resources;

    .line 2715630
    iput-object p4, p0, LX/Jad;->c:LX/DN3;

    .line 2715631
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2715632
    iget-object v1, p0, LX/Jad;->c:LX/DN3;

    .line 2715633
    iget-object v2, v1, LX/DN3;->b:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short p2, LX/DN2;->f:S

    const/4 p3, 0x0

    invoke-interface {v2, v3, p2, p3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    move v1, v2

    .line 2715634
    if-eqz v1, :cond_0

    .line 2715635
    new-instance v1, LX/Jab;

    invoke-direct {v1}, LX/Jab;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2715636
    :cond_0
    new-instance v1, LX/Jaa;

    invoke-direct {v1}, LX/Jaa;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2715637
    iget-object v1, p0, LX/Jad;->c:LX/DN3;

    invoke-virtual {v1}, LX/DN3;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2715638
    new-instance v1, LX/JaY;

    invoke-direct {v1, p1}, LX/JaY;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2715639
    :cond_1
    new-instance v1, LX/JaZ;

    iget-object v2, p0, LX/Jad;->c:LX/DN3;

    invoke-virtual {v2}, LX/DN3;->b()Z

    move-result v2

    iget-object v3, p0, LX/Jad;->c:LX/DN3;

    invoke-virtual {v3}, LX/DN3;->c()Z

    move-result v3

    invoke-direct {v1, v2, v3}, LX/JaZ;-><init>(ZZ)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2715640
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Jad;->b:LX/0Px;

    .line 2715641
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2715649
    iget-object v1, p0, LX/Jad;->a:Landroid/content/res/Resources;

    iget-object v0, p0, LX/Jad;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaX;

    invoke-interface {v0}, LX/JaX;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Jac;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2715644
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/Jad;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2715645
    iget-object v0, p0, LX/Jad;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaX;

    invoke-interface {v0}, LX/JaX;->d()LX/Jac;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Jac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2715646
    :goto_1
    return v1

    .line 2715647
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2715648
    goto :goto_1
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2715643
    iget-object v0, p0, LX/Jad;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JaX;

    invoke-interface {v0}, LX/JaX;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2715642
    iget-object v0, p0, LX/Jad;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
