.class public final enum LX/Jfb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jfb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jfb;

.field public static final enum FETCH_REVIEW:LX/Jfb;

.field public static final enum POST_REVIEW:LX/Jfb;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2722100
    new-instance v0, LX/Jfb;

    const-string v1, "FETCH_REVIEW"

    invoke-direct {v0, v1, v2}, LX/Jfb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jfb;->FETCH_REVIEW:LX/Jfb;

    .line 2722101
    new-instance v0, LX/Jfb;

    const-string v1, "POST_REVIEW"

    invoke-direct {v0, v1, v3}, LX/Jfb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jfb;->POST_REVIEW:LX/Jfb;

    .line 2722102
    const/4 v0, 0x2

    new-array v0, v0, [LX/Jfb;

    sget-object v1, LX/Jfb;->FETCH_REVIEW:LX/Jfb;

    aput-object v1, v0, v2

    sget-object v1, LX/Jfb;->POST_REVIEW:LX/Jfb;

    aput-object v1, v0, v3

    sput-object v0, LX/Jfb;->$VALUES:[LX/Jfb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2722103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jfb;
    .locals 1

    .prologue
    .line 2722099
    const-class v0, LX/Jfb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jfb;

    return-object v0
.end method

.method public static values()[LX/Jfb;
    .locals 1

    .prologue
    .line 2722098
    sget-object v0, LX/Jfb;->$VALUES:[LX/Jfb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jfb;

    return-object v0
.end method
