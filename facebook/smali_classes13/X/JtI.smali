.class public final LX/JtI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<[B>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JtJ;


# direct methods
.method public constructor <init>(LX/JtJ;)V
    .locals 0

    .prologue
    .line 2746543
    iput-object p1, p0, LX/JtI;->a:LX/JtJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746544
    sget-object v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v1, "Failed to fetch sticker image(s)"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746545
    iget-object v0, p0, LX/JtI;->a:LX/JtJ;

    iget-object v0, v0, LX/JtJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746546
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746547
    check-cast p1, [B

    .line 2746548
    if-nez p1, :cond_0

    .line 2746549
    iget-object v0, p0, LX/JtI;->a:LX/JtJ;

    iget-object v0, v0, LX/JtJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x45202d11

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746550
    :goto_0
    return-void

    .line 2746551
    :cond_0
    iget-object v0, p0, LX/JtI;->a:LX/JtJ;

    iget-object v0, v0, LX/JtJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/JtI;->a:LX/JtJ;

    iget-object v1, v1, LX/JtJ;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v2, p0, LX/JtI;->a:LX/JtJ;

    iget-object v2, v2, LX/JtJ;->b:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a$redex0(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;[BLjava/lang/String;)LX/JtM;

    move-result-object v1

    const v2, 0x7d4a15eb

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method
