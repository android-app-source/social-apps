.class public LX/JZC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JZA;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2707956
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JZC;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707957
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2707958
    iput-object p1, p0, LX/JZC;->b:LX/0Ot;

    .line 2707959
    return-void
.end method

.method public static a(LX/0QB;)LX/JZC;
    .locals 4

    .prologue
    .line 2707960
    const-class v1, LX/JZC;

    monitor-enter v1

    .line 2707961
    :try_start_0
    sget-object v0, LX/JZC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707962
    sput-object v2, LX/JZC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707963
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707964
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707965
    new-instance v3, LX/JZC;

    const/16 p0, 0x21dc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JZC;-><init>(LX/0Ot;)V

    .line 2707966
    move-object v0, v3

    .line 2707967
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707968
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707969
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2707971
    check-cast p2, LX/JZB;

    .line 2707972
    iget-object v0, p0, LX/JZC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;

    iget-object v2, p2, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/JZB;->b:LX/1Pm;

    iget-object v4, p2, LX/JZB;->c:Ljava/lang/String;

    iget-object v5, p2, LX/JZB;->d:Ljava/lang/Boolean;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;Ljava/lang/String;Ljava/lang/Boolean;)LX/1Dg;

    move-result-object v0

    .line 2707973
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2707974
    invoke-static {}, LX/1dS;->b()V

    .line 2707975
    iget v0, p1, LX/1dQ;->b:I

    .line 2707976
    packed-switch v0, :pswitch_data_0

    .line 2707977
    :goto_0
    return-object v2

    .line 2707978
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2707979
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2707980
    check-cast v1, LX/JZB;

    .line 2707981
    iget-object v3, p0, LX/JZC;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;

    iget-object p1, v1, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/JZB;->c:Ljava/lang/String;

    .line 2707982
    invoke-static {v3, p1, p2}, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->b(Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 2707983
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x68d801cf
        :pswitch_0
    .end packed-switch
.end method
