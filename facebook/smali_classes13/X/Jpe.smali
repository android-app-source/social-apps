.class public LX/Jpe;
.super LX/3LH;
.source ""


# instance fields
.field private final a:LX/JqE;

.field public final b:LX/Jph;

.field public c:Landroid/widget/TextView;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field private f:I

.field public g:I


# direct methods
.method public constructor <init>(LX/JqE;LX/Jph;)V
    .locals 1
    .param p2    # LX/Jph;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2737889
    invoke-direct {p0}, LX/3LH;-><init>()V

    .line 2737890
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2737891
    iput-object v0, p0, LX/Jpe;->d:LX/0Px;

    .line 2737892
    iput-object p1, p0, LX/Jpe;->a:LX/JqE;

    .line 2737893
    iput-object p2, p0, LX/Jpe;->b:LX/Jph;

    .line 2737894
    return-void
.end method

.method public static declared-synchronized a(LX/Jpe;LX/3OQ;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2737881
    monitor-enter p0

    :try_start_0
    check-cast p1, LX/3OP;

    .line 2737882
    invoke-virtual {p1}, LX/3OP;->a()Z

    move-result v2

    .line 2737883
    iget v3, p0, LX/Jpe;->e:I

    if-eqz v2, :cond_0

    const/4 v1, -0x1

    :goto_0
    add-int/2addr v1, v3

    iput v1, p0, LX/Jpe;->e:I

    .line 2737884
    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {p1, v0}, LX/3OP;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2737885
    monitor-exit p0

    return-void

    :cond_0
    move v1, v0

    .line 2737886
    goto :goto_0

    .line 2737887
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2737888
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2737868
    iput-object p1, p0, LX/Jpe;->d:LX/0Px;

    .line 2737869
    iput v3, p0, LX/Jpe;->e:I

    .line 2737870
    iput v3, p0, LX/Jpe;->f:I

    .line 2737871
    iput v3, p0, LX/Jpe;->g:I

    .line 2737872
    iget-object v0, p0, LX/Jpe;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_3

    iget-object v0, p0, LX/Jpe;->d:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2737873
    iget v6, p0, LX/Jpe;->e:I

    move-object v1, v0

    check-cast v1, LX/3OP;

    invoke-virtual {v1}, LX/3OP;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_1
    add-int/2addr v1, v6

    iput v1, p0, LX/Jpe;->e:I

    .line 2737874
    iget v6, p0, LX/Jpe;->f:I

    instance-of v1, v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    if-eqz v1, :cond_1

    move v1, v2

    :goto_2
    add-int/2addr v1, v6

    iput v1, p0, LX/Jpe;->f:I

    .line 2737875
    iget v1, p0, LX/Jpe;->g:I

    instance-of v0, v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    if-eqz v0, :cond_2

    move v0, v2

    :goto_3
    add-int/2addr v0, v1

    iput v0, p0, LX/Jpe;->g:I

    .line 2737876
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move v1, v3

    .line 2737877
    goto :goto_1

    :cond_1
    move v1, v3

    .line 2737878
    goto :goto_2

    :cond_2
    move v0, v3

    .line 2737879
    goto :goto_3

    .line 2737880
    :cond_3
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2737867
    iget-object v0, p0, LX/Jpe;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2737895
    if-gtz p1, :cond_0

    .line 2737896
    const/4 v0, 0x0

    .line 2737897
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Jpe;->d:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2737866
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2737842
    if-gtz p1, :cond_1

    .line 2737843
    iget-object v0, p0, LX/Jpe;->c:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 2737844
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031513

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jpe;->c:Landroid/widget/TextView;

    .line 2737845
    iget-object v0, p0, LX/Jpe;->c:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2737846
    :cond_0
    iget-object v0, p0, LX/Jpe;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/Jpe;->b:LX/Jph;

    .line 2737847
    iget v2, p0, LX/Jpe;->g:I

    move v2, v2

    .line 2737848
    iget-object v3, v1, LX/Jph;->a:LX/Jpg;

    sget-object p1, LX/Jpg;->LOCAL:LX/Jpg;

    if-ne v3, p1, :cond_3

    .line 2737849
    iget-object v3, v1, LX/Jph;->b:Landroid/content/res/Resources;

    const p1, 0x7f082ea1

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2737850
    iget-object p1, v1, LX/Jph;->b:Landroid/content/res/Resources;

    const p2, 0x7f082e9c

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2737851
    iget-object p2, v1, LX/Jph;->c:LX/JqE;

    const-string p3, "{learn_more_link}"

    invoke-virtual {p2, v3, p3, p1}, LX/JqE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v3

    .line 2737852
    :goto_0
    move-object v1, v3

    .line 2737853
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2737854
    iget-object v0, p0, LX/Jpe;->c:Landroid/widget/TextView;

    move-object v0, v0

    .line 2737855
    :goto_1
    return-object v0

    .line 2737856
    :cond_1
    invoke-virtual {p0, p1}, LX/Jpe;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OP;

    .line 2737857
    instance-of v1, p2, Lcom/facebook/messaging/sms/migration/SMSContactItem;

    if-eqz v1, :cond_4

    check-cast p2, Lcom/facebook/messaging/sms/migration/SMSContactItem;

    move-object v1, p2

    .line 2737858
    :goto_2
    if-nez v1, :cond_2

    .line 2737859
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f031511

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/migration/SMSContactItem;

    .line 2737860
    :cond_2
    instance-of v2, v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    if-eqz v2, :cond_5

    .line 2737861
    check-cast v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->setContactRow(Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;)V

    .line 2737862
    :goto_3
    move-object v0, v1

    .line 2737863
    goto :goto_1

    :cond_3
    iget-object v3, v1, LX/Jph;->b:Landroid/content/res/Resources;

    const p1, 0x7f0f0144

    invoke-virtual {v3, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2737864
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 2737865
    :cond_5
    check-cast v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->setContactRow(Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;)V

    goto :goto_3
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2737834
    if-lez p1, :cond_2

    .line 2737835
    invoke-virtual {p0, p1}, LX/Jpe;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OP;

    .line 2737836
    instance-of v2, v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    if-nez v2, :cond_0

    check-cast v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    .line 2737837
    iget-boolean v2, v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->f:Z

    move v0, v2

    .line 2737838
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2737839
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2737840
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2737841
    goto :goto_0
.end method
