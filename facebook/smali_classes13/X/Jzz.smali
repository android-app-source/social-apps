.class public LX/Jzz;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "LocationObserver"
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Landroid/location/LocationListener;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2757135
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2757136
    new-instance v0, LX/Jzv;

    invoke-direct {v0, p0}, LX/Jzv;-><init>(LX/Jzz;)V

    iput-object v0, p0, LX/Jzz;->b:Landroid/location/LocationListener;

    .line 2757137
    return-void
.end method

.method public static synthetic a(LX/Jzz;)LX/5pY;
    .locals 1

    .prologue
    .line 2757125
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757126
    return-object v0
.end method

.method private static a(Landroid/location/LocationManager;Z)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2757127
    if-eqz p1, :cond_1

    const-string v0, "gps"

    .line 2757128
    :goto_0
    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2757129
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "network"

    .line 2757130
    :goto_1
    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2757131
    const/4 v0, 0x0

    .line 2757132
    :cond_0
    return-object v0

    .line 2757133
    :cond_1
    const-string v0, "network"

    goto :goto_0

    .line 2757134
    :cond_2
    const-string v0, "gps"

    goto :goto_1
.end method

.method private static a(Ljava/lang/SecurityException;)V
    .locals 2

    .prologue
    .line 2757121
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Looks like the app doesn\'t have the permission to access location.\nAdd the following line to your app\'s AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\" />"

    invoke-direct {v0, v1, p0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a$redex0(LX/Jzz;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2757122
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757123
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "geolocationError"

    invoke-static {p1, p2}, LX/K00;->a(ILjava/lang/String;)LX/5pH;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2757124
    return-void
.end method

.method public static b(Landroid/location/Location;)LX/5pH;
    .locals 6

    .prologue
    .line 2757108
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2757109
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2757110
    const-string v2, "latitude"

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757111
    const-string v2, "longitude"

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757112
    const-string v2, "altitude"

    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757113
    const-string v2, "accuracy"

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757114
    const-string v2, "heading"

    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757115
    const-string v2, "speed"

    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757116
    const-string v2, "coords"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2757117
    const-string v1, "timestamp"

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2757118
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 2757119
    const-string v1, "mocked"

    invoke-virtual {p0}, Landroid/location/Location;->isFromMockProvider()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2757120
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getCurrentPosition(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 9
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757095
    invoke-static {p1}, LX/Jzw;->b(LX/5pG;)LX/Jzw;

    move-result-object v0

    .line 2757096
    :try_start_0
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2757097
    const-string v2, "location"

    invoke-virtual {v1, v2}, LX/5pY;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 2757098
    iget-boolean v1, v0, LX/Jzw;->c:Z

    invoke-static {v2, v1}, LX/Jzz;->a(Landroid/location/LocationManager;Z)Ljava/lang/String;

    move-result-object v3

    .line 2757099
    if-nez v3, :cond_0

    .line 2757100
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "No available location provider."

    aput-object v2, v0, v1

    invoke-interface {p3, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2757101
    :goto_0
    return-void

    .line 2757102
    :cond_0
    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 2757103
    if-eqz v1, :cond_1

    invoke-static {}, LX/5pv;->a()J

    move-result-wide v4

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-double v4, v4

    iget-wide v6, v0, LX/Jzw;->b:D

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 2757104
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v1}, LX/Jzz;->b(Landroid/location/Location;)LX/5pH;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2757105
    :catch_0
    move-exception v0

    .line 2757106
    invoke-static {v0}, LX/Jzz;->a(Ljava/lang/SecurityException;)V

    goto :goto_0

    .line 2757107
    :cond_1
    :try_start_1
    new-instance v1, LX/Jzy;

    iget-wide v4, v0, LX/Jzw;->a:J

    const/4 v8, 0x0

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v1 .. v8}, LX/Jzy;-><init>(Landroid/location/LocationManager;Ljava/lang/String;JLcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;B)V

    invoke-virtual {v1}, LX/Jzy;->a()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757094
    const-string v0, "LocationObserver"

    return-object v0
.end method

.method public startObserving(LX/5pG;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757075
    const-string v0, "gps"

    iget-object v1, p0, LX/Jzz;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2757076
    :goto_0
    return-void

    .line 2757077
    :cond_0
    invoke-static {p1}, LX/Jzw;->b(LX/5pG;)LX/Jzw;

    move-result-object v4

    .line 2757078
    :try_start_0
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757079
    const-string v1, "location"

    invoke-virtual {v0, v1}, LX/5pY;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 2757080
    iget-boolean v1, v4, LX/Jzw;->c:Z

    invoke-static {v0, v1}, LX/Jzz;->a(Landroid/location/LocationManager;Z)Ljava/lang/String;

    move-result-object v1

    .line 2757081
    if-nez v1, :cond_1

    .line 2757082
    sget v0, LX/K00;->a:I

    const-string v1, "No location provider available."

    invoke-static {p0, v0, v1}, LX/Jzz;->a$redex0(LX/Jzz;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2757083
    :catch_0
    move-exception v0

    .line 2757084
    invoke-static {v0}, LX/Jzz;->a(Ljava/lang/SecurityException;)V

    goto :goto_0

    .line 2757085
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/Jzz;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2757086
    iget-object v2, p0, LX/Jzz;->b:Landroid/location/LocationListener;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 2757087
    const-wide/16 v2, 0x3e8

    iget v4, v4, LX/Jzw;->d:F

    iget-object v5, p0, LX/Jzz;->b:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 2757088
    :cond_2
    iput-object v1, p0, LX/Jzz;->a:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public stopObserving()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757089
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757090
    const-string v1, "location"

    invoke-virtual {v0, v1}, LX/5pY;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 2757091
    iget-object v1, p0, LX/Jzz;->b:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 2757092
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jzz;->a:Ljava/lang/String;

    .line 2757093
    return-void
.end method
