.class public final LX/K3J;
.super LX/3xj;
.source ""


# instance fields
.field public final synthetic a:LX/K3K;


# direct methods
.method public constructor <init>(LX/K3K;)V
    .locals 0

    .prologue
    .line 2765495
    iput-object p1, p0, LX/K3J;->a:LX/K3K;

    invoke-direct {p0}, LX/3xj;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/K3K;B)V
    .locals 0

    .prologue
    .line 2765500
    invoke-direct {p0, p1}, LX/K3J;-><init>(LX/K3K;)V

    return-void
.end method


# virtual methods
.method public final a(LX/1a1;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2765496
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 2765497
    if-nez v0, :cond_0

    .line 2765498
    invoke-static {v1, v1}, LX/3xj;->b(II)I

    move-result v0

    .line 2765499
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xf

    invoke-static {v0, v1}, LX/3xj;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/1a1;Ljava/util/List;II)LX/1a1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1a1;",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;II)",
            "LX/1a1;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765447
    const/4 v3, 0x0

    .line 2765448
    const v2, 0x7fffffff

    .line 2765449
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int v5, p4, v0

    .line 2765450
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int v6, p3, v0

    .line 2765451
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v7, p3, v0

    .line 2765452
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 2765453
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 2765454
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    if-gt p4, v1, :cond_3

    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-lt v5, v1, :cond_3

    .line 2765455
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v8, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v1

    .line 2765456
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, p3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2765457
    if-gez v7, :cond_0

    if-gt v6, v8, :cond_1

    :cond_0
    if-lez v7, :cond_3

    if-ge p3, v8, :cond_3

    :cond_1
    if-le v2, v1, :cond_3

    move v9, v1

    move-object v1, v0

    move v0, v9

    .line 2765458
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_0

    .line 2765459
    :cond_2
    return-object v3

    :cond_3
    move v0, v2

    move-object v1, v3

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2765485
    invoke-super {p0, p1, p2}, LX/3xj;->a(LX/1a1;I)V

    .line 2765486
    if-eqz p1, :cond_0

    .line 2765487
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 2765488
    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    .line 2765489
    :cond_0
    :goto_0
    return-void

    .line 2765490
    :cond_1
    check-cast p1, LX/K3G;

    iget-object v0, p1, LX/K3G;->m:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v0, v1}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setIsSelected(Z)V

    .line 2765491
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    .line 2765492
    iput-boolean v1, v0, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->i:Z

    .line 2765493
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->e:LX/K3I;

    if-eqz v0, :cond_0

    .line 2765494
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->e:LX/K3I;

    invoke-interface {v0}, LX/K3I;->a()V

    goto :goto_0
.end method

.method public final a(LX/1a1;LX/1a1;)Z
    .locals 3

    .prologue
    .line 2765501
    invoke-virtual {p1}, LX/1a1;->e()I

    move-result v0

    .line 2765502
    invoke-virtual {p2}, LX/1a1;->e()I

    move-result v1

    .line 2765503
    iget-object v2, p0, LX/K3J;->a:LX/K3K;

    iget-object v2, v2, LX/K3K;->c:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    .line 2765504
    iget-object p0, v2, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/ipc/media/MediaItem;

    .line 2765505
    iget-object p1, v2, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2765506
    invoke-virtual {v2, v0, v1}, LX/1OM;->b(II)V

    .line 2765507
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2765465
    invoke-super {p0, p1, p2}, LX/3xj;->b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V

    .line 2765466
    check-cast p2, LX/K3G;

    iget-object v1, p2, LX/K3G;->m:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v1, v0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setIsSelected(Z)V

    .line 2765467
    iget-object v1, p0, LX/K3J;->a:LX/K3K;

    iget-object v1, v1, LX/K3K;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    .line 2765468
    iput-boolean v0, v1, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->i:Z

    .line 2765469
    iget-object v1, p0, LX/K3J;->a:LX/K3K;

    iget-object v1, v1, LX/K3K;->e:LX/K3I;

    if-eqz v1, :cond_0

    .line 2765470
    iget-object v1, p0, LX/K3J;->a:LX/K3K;

    iget-object v1, v1, LX/K3K;->e:LX/K3I;

    invoke-interface {v1}, LX/K3I;->b()V

    .line 2765471
    :cond_0
    iget-object v1, p0, LX/K3J;->a:LX/K3K;

    iget-object v1, v1, LX/K3K;->c:Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;

    .line 2765472
    iget-object v2, v1, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    move-object v1, v2

    .line 2765473
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2765474
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LX/K3J;->a:LX/K3K;

    iget-object v3, v3, LX/K3K;->a:LX/0Px;

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v2, v3, :cond_2

    .line 2765475
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->d:LX/3l1;

    iget-object v2, p0, LX/K3J;->a:LX/K3K;

    iget-object v2, v2, LX/K3K;->f:Ljava/lang/String;

    .line 2765476
    iput-object v2, v0, LX/3l1;->b:Ljava/lang/String;

    .line 2765477
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->d:LX/3l1;

    .line 2765478
    sget-object v2, LX/BOR;->SLIDESHOW_PREVIEW_REORDER:LX/BOR;

    invoke-static {v2}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2765479
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2765480
    iput-object v1, v0, LX/K3K;->a:LX/0Px;

    .line 2765481
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->e:LX/K3I;

    if-eqz v0, :cond_1

    .line 2765482
    iget-object v0, p0, LX/K3J;->a:LX/K3K;

    iget-object v0, v0, LX/K3K;->e:LX/K3I;

    iget-object v1, p0, LX/K3J;->a:LX/K3K;

    iget-object v1, v1, LX/K3K;->a:LX/0Px;

    invoke-interface {v0, v1}, LX/K3I;->a(LX/0Px;)V

    .line 2765483
    :cond_1
    return-void

    .line 2765484
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2765464
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/1a1;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2765461
    iget v1, p1, LX/1a1;->e:I

    move v1, v1

    .line 2765462
    if-ne v1, v0, :cond_0

    .line 2765463
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2765460
    return-void
.end method
