.class public LX/Jgt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

.field public final b:LX/3Ku;

.field public final c:LX/3Kk;

.field private final d:LX/3Kl;

.field public final e:LX/0aG;

.field public final f:LX/0Sh;

.field public g:LX/3Lb;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;LX/3Ku;LX/3Kk;LX/3Kl;LX/0aG;LX/0Sh;)V
    .locals 0
    .param p1    # Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2723877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2723878
    iput-object p1, p0, LX/Jgt;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2723879
    iput-object p2, p0, LX/Jgt;->b:LX/3Ku;

    .line 2723880
    iput-object p3, p0, LX/Jgt;->c:LX/3Kk;

    .line 2723881
    iput-object p4, p0, LX/Jgt;->d:LX/3Kl;

    .line 2723882
    iput-object p5, p0, LX/Jgt;->e:LX/0aG;

    .line 2723883
    iput-object p6, p0, LX/Jgt;->f:LX/0Sh;

    .line 2723884
    return-void
.end method

.method public static e(LX/Jgt;)LX/3Lb;
    .locals 4

    .prologue
    .line 2723885
    iget-object v0, p0, LX/Jgt;->d:LX/3Kl;

    .line 2723886
    sget-object v1, LX/3LZ;->FAVORITE_FRIENDS:LX/3LZ;

    sget-object v2, LX/3LZ;->TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

    sget-object v3, LX/3LZ;->TOP_FRIENDS:LX/3LZ;

    sget-object p0, LX/3LZ;->TOP_PHONE_CONTACTS:LX/3LZ;

    invoke-static {v1, v2, v3, p0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    .line 2723887
    iget-object v1, v0, LX/3Kl;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Lb;

    .line 2723888
    new-instance v3, LX/3La;

    invoke-direct {v3, v2}, LX/3La;-><init>(Ljava/util/EnumSet;)V

    .line 2723889
    iput-object v3, v1, LX/3Lb;->z:LX/3La;

    .line 2723890
    move-object v0, v1

    .line 2723891
    return-object v0
.end method


# virtual methods
.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2723892
    iget-object v0, p0, LX/Jgt;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2723893
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2723894
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Jgt;->h:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2723895
    iget-object v0, p0, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
