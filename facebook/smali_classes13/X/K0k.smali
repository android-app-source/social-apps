.class public final LX/K0k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:LX/K0l;


# direct methods
.method public constructor <init>(LX/K0l;)V
    .locals 0

    .prologue
    .line 2759495
    iput-object p1, p0, LX/K0k;->a:LX/K0l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    .line 2759496
    sub-int v0, p9, p7

    .line 2759497
    sub-int v1, p5, p3

    .line 2759498
    if-eq v0, v1, :cond_0

    .line 2759499
    iget-object v2, p0, LX/K0k;->a:LX/K0l;

    sub-int v3, v1, v0

    invoke-static {v2, v3}, LX/K0l;->h(LX/K0l;I)V

    .line 2759500
    iget-object v2, p0, LX/K0k;->a:LX/K0l;

    iget-object v2, v2, LX/K0l;->b:LX/K0n;

    iget-object v3, p0, LX/K0k;->a:LX/K0l;

    iget-object v3, v3, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3, v0, v1}, LX/K0n;->a(III)V

    .line 2759501
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2759502
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2759503
    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    .line 2759504
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    .line 2759505
    :cond_0
    return-void
.end method
