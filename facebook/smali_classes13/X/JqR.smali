.class public LX/JqR;
.super LX/7G8;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final p:Ljava/lang/Object;


# instance fields
.field public final b:LX/JqU;

.field private final c:LX/2Sx;

.field private final d:LX/JqK;

.field private final e:LX/0Uh;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/7G1;

.field private final h:LX/7GB;

.field private final i:LX/0tX;

.field public final j:LX/03V;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Ge;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/7G0;

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/concurrent/ExecutorService;

.field public final o:LX/2My;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739068
    const-class v0, LX/JqR;

    sput-object v0, LX/JqR;->a:Ljava/lang/Class;

    .line 2739069
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JqR;->p:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6Po;LX/0SG;LX/7Ge;Ljava/util/concurrent/ScheduledExecutorService;LX/JqU;LX/2Sx;LX/JqK;LX/0Uh;LX/0Ot;LX/7G1;LX/7GB;LX/0tX;LX/03V;LX/0Ot;LX/7G0;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/2My;)V
    .locals 12
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p17    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6Po;",
            "LX/0SG;",
            "LX/7Ge;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/JqU;",
            "LX/2Sx;",
            "LX/JqK;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;",
            ">;",
            "LX/7G1;",
            "LX/7GB;",
            "LX/0tX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/7Ge;",
            ">;",
            "LX/7G0;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2My;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738922
    invoke-interface/range {p16 .. p16}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v1}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v10

    move-object v1, p0

    move-object/from16 v2, p11

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p10

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    move-object/from16 v11, p7

    invoke-direct/range {v1 .. v11}, LX/7G8;-><init>(LX/7GB;LX/7GD;LX/2Sx;LX/7G1;LX/6Po;LX/0SG;LX/7Ge;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/String;LX/7Fx;)V

    .line 2738923
    move-object/from16 v0, p5

    iput-object v0, p0, LX/JqR;->b:LX/JqU;

    .line 2738924
    move-object/from16 v0, p6

    iput-object v0, p0, LX/JqR;->c:LX/2Sx;

    .line 2738925
    move-object/from16 v0, p7

    iput-object v0, p0, LX/JqR;->d:LX/JqK;

    .line 2738926
    move-object/from16 v0, p8

    iput-object v0, p0, LX/JqR;->e:LX/0Uh;

    .line 2738927
    move-object/from16 v0, p9

    iput-object v0, p0, LX/JqR;->f:LX/0Ot;

    .line 2738928
    move-object/from16 v0, p10

    iput-object v0, p0, LX/JqR;->g:LX/7G1;

    .line 2738929
    move-object/from16 v0, p11

    iput-object v0, p0, LX/JqR;->h:LX/7GB;

    .line 2738930
    move-object/from16 v0, p12

    iput-object v0, p0, LX/JqR;->i:LX/0tX;

    .line 2738931
    move-object/from16 v0, p13

    iput-object v0, p0, LX/JqR;->j:LX/03V;

    .line 2738932
    move-object/from16 v0, p14

    iput-object v0, p0, LX/JqR;->k:LX/0Ot;

    .line 2738933
    move-object/from16 v0, p15

    iput-object v0, p0, LX/JqR;->l:LX/7G0;

    .line 2738934
    move-object/from16 v0, p16

    iput-object v0, p0, LX/JqR;->m:LX/0Or;

    .line 2738935
    move-object/from16 v0, p17

    iput-object v0, p0, LX/JqR;->n:Ljava/util/concurrent/ExecutorService;

    .line 2738936
    move-object/from16 v0, p18

    iput-object v0, p0, LX/JqR;->o:LX/2My;

    .line 2738937
    return-void
.end method

.method private a(JLjava/lang/String;)LX/76M;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "LX/76M",
            "<",
            "LX/7GC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2738938
    new-instance v6, LX/JqO;

    invoke-direct {v6, p0, p1, p2, p3}, LX/JqO;-><init>(LX/JqR;JLjava/lang/String;)V

    .line 2738939
    iget-object v0, p0, LX/JqR;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Ge;

    const-wide/32 v2, 0x493e0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v1 .. v6}, LX/7Ge;->a(JJLX/7G5;)LX/7Gd;

    move-result-object v0

    .line 2738940
    invoke-virtual {v0}, LX/7Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/76M;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JqR;
    .locals 7

    .prologue
    .line 2738941
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2738942
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2738943
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2738944
    if-nez v1, :cond_0

    .line 2738945
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2738946
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2738947
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2738948
    sget-object v1, LX/JqR;->p:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2738949
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2738950
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2738951
    :cond_1
    if-nez v1, :cond_4

    .line 2738952
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2738953
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2738954
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JqR;->b(LX/0QB;)LX/JqR;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2738955
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2738956
    if-nez v1, :cond_2

    .line 2738957
    sget-object v0, LX/JqR;->p:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqR;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2738958
    :goto_1
    if-eqz v0, :cond_3

    .line 2738959
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2738960
    :goto_3
    check-cast v0, LX/JqR;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2738961
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2738962
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2738963
    :catchall_1
    move-exception v0

    .line 2738964
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2738965
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2738966
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2738967
    :cond_2
    :try_start_8
    sget-object v0, LX/JqR;->p:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqR;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private b()LX/JqQ;
    .locals 6

    .prologue
    .line 2738968
    new-instance v0, LX/JqW;

    invoke-direct {v0}, LX/JqW;-><init>()V

    move-object v0, v0

    .line 2738969
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2738970
    iget-object v0, p0, LX/JqR;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2738971
    iput-object v0, v1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2738972
    iget-object v0, p0, LX/JqR;->i:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2738973
    const v1, -0x10f5ef5f

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2738974
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2738975
    check-cast v0, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2738976
    new-instance v2, LX/JqQ;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->j(II)I

    move-result v0

    invoke-direct {v2, v4, v5, v0}, LX/JqQ;-><init>(JI)V

    return-object v2

    .line 2738977
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static b(LX/0QB;)LX/JqR;
    .locals 21

    .prologue
    .line 2738978
    new-instance v2, LX/JqR;

    invoke-static/range {p0 .. p0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v3

    check-cast v3, LX/6Po;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/7Ge;->a(LX/0QB;)LX/7Ge;

    move-result-object v5

    check-cast v5, LX/7Ge;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/JqU;->a(LX/0QB;)LX/JqU;

    move-result-object v7

    check-cast v7, LX/JqU;

    invoke-static/range {p0 .. p0}, LX/2Sx;->a(LX/0QB;)LX/2Sx;

    move-result-object v8

    check-cast v8, LX/2Sx;

    invoke-static/range {p0 .. p0}, LX/JqK;->a(LX/0QB;)LX/JqK;

    move-result-object v9

    check-cast v9, LX/JqK;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v11, 0x29c3

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/7G1;->a(LX/0QB;)LX/7G1;

    move-result-object v12

    check-cast v12, LX/7G1;

    invoke-static/range {p0 .. p0}, LX/7GB;->a(LX/0QB;)LX/7GB;

    move-result-object v13

    check-cast v13, LX/7GB;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v14

    check-cast v14, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v15

    check-cast v15, LX/03V;

    const/16 v16, 0x35ca

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/7G0;->a(LX/0QB;)LX/7G0;

    move-result-object v17

    check-cast v17, LX/7G0;

    const/16 v18, 0x19e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v19

    check-cast v19, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v20

    check-cast v20, LX/2My;

    invoke-direct/range {v2 .. v20}, LX/JqR;-><init>(LX/6Po;LX/0SG;LX/7Ge;Ljava/util/concurrent/ScheduledExecutorService;LX/JqU;LX/2Sx;LX/JqK;LX/0Uh;LX/0Ot;LX/7G1;LX/7GB;LX/0tX;LX/03V;LX/0Ot;LX/7G0;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/2My;)V

    .line 2738979
    return-object v2
.end method

.method private d(Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Long;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2738980
    new-instance v6, LX/JqP;

    invoke-direct {v6, p0, p1}, LX/JqP;-><init>(LX/JqR;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2738981
    iget-object v0, p0, LX/JqR;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Ge;

    const-wide/32 v2, 0x493e0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v1 .. v6}, LX/7Ge;->a(JJLX/7G5;)LX/7Gd;

    move-result-object v0

    .line 2738982
    invoke-virtual {v0}, LX/7Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)J
    .locals 14
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2738983
    const/16 v1, 0xfa

    .line 2738984
    const/4 v0, 0x1

    move v2, v0

    move v3, v1

    :goto_0
    const/4 v0, 0x3

    if-gt v2, v0, :cond_6

    .line 2738985
    iget-object v0, p0, LX/JqR;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    .line 2738986
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a:LX/6Po;

    sget-object v6, LX/FDy;->a:LX/6Pr;

    const-string v7, "fetchThreadList (Sync)"

    invoke-virtual {v5, v6, v7}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2738987
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v5

    sget-object v6, LX/6ek;->INBOX:LX/6ek;

    .line 2738988
    iput-object v6, v5, LX/6iI;->b:LX/6ek;

    .line 2738989
    move-object v5, v5

    .line 2738990
    invoke-virtual {v5}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v7

    .line 2738991
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 2738992
    invoke-virtual {v8, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2738993
    const/4 v6, 0x0

    .line 2738994
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v9, 0x1e9

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2738995
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v5

    sget-object v6, LX/6ek;->MONTAGE:LX/6ek;

    .line 2738996
    iput-object v6, v5, LX/6iI;->b:LX/6ek;

    .line 2738997
    move-object v5, v5

    .line 2738998
    invoke-virtual {v5}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v5

    .line 2738999
    invoke-virtual {v8, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-object v6, v5

    .line 2739000
    :cond_0
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v5, v8, p1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/HashMap;

    move-result-object v8

    .line 2739001
    invoke-virtual {v8, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2739002
    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2739003
    if-eqz v6, :cond_7

    iget-wide v7, v6, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    iget-wide v9, v5, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    cmp-long v7, v7, v9

    if-gez v7, :cond_7

    iget-wide v7, v6, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-ltz v7, :cond_7

    .line 2739004
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/6iK;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)LX/6iK;

    move-result-object v5

    iget-wide v7, v6, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    .line 2739005
    iput-wide v7, v5, LX/6iK;->l:J

    .line 2739006
    move-object v5, v5

    .line 2739007
    invoke-virtual {v5}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v5

    move-object v7, v5

    .line 2739008
    :goto_1
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    invoke-virtual {v5, v7}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2739009
    if-eqz v6, :cond_1

    .line 2739010
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    invoke-virtual {v5, v6}, LX/FDs;->b(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2739011
    :cond_1
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Oe;

    invoke-virtual {v5}, LX/2Oe;->a()V

    .line 2739012
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Oe;

    invoke-virtual {v5, v7}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2739013
    if-eqz v6, :cond_2

    .line 2739014
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Oe;

    invoke-virtual {v5, v6}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2739015
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Ow;

    invoke-virtual {v5}, LX/2Ow;->b()V

    .line 2739016
    :cond_2
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Ow;

    invoke-virtual {v5}, LX/2Ow;->a()V

    .line 2739017
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Di5;

    iget-object v6, v7, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {v5, v6}, LX/Di5;->a(Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 2739018
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->h:LX/CLl;

    iget-object v6, v7, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2739019
    iget-object v8, v6, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v6, v8

    .line 2739020
    const-string v8, "FETCH_THREAD_LIST"

    invoke-virtual {v5, v6, v8}, LX/CLl;->a(LX/0Px;Ljava/lang/String;)V

    .line 2739021
    iget-object v5, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->i:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Iuh;

    iget-object v6, v7, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2739022
    iget-object v11, v5, LX/Iuh;->i:LX/Iui;

    .line 2739023
    iget-object v12, v11, LX/Iui;->c:LX/0Sh;

    invoke-virtual {v12}, LX/0Sh;->b()V

    .line 2739024
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2739025
    :cond_3
    :goto_2
    move-object v4, v7

    .line 2739026
    iget-wide v0, v4, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    .line 2739027
    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v4

    if-lez v4, :cond_4

    .line 2739028
    :goto_3
    return-wide v0

    .line 2739029
    :cond_4
    invoke-direct {p0}, LX/JqR;->b()LX/JqQ;

    move-result-object v0

    .line 2739030
    iget v1, v0, LX/JqQ;->b:I

    if-nez v1, :cond_5

    .line 2739031
    iget-wide v0, v0, LX/JqQ;->a:J

    goto :goto_3

    .line 2739032
    :cond_5
    iget-object v0, p0, LX/JqR;->l:LX/7G0;

    .line 2739033
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "sync_sequence_id_difference"

    invoke-direct {v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2739034
    const-string v4, "attemptNumber"

    invoke-virtual {v1, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2739035
    iget-object v4, v0, LX/7G0;->a:LX/7G1;

    sget-object v5, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v4, v1, v5}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 2739036
    int-to-long v0, v3

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 2739037
    mul-int/lit8 v1, v3, 0x2

    .line 2739038
    add-int/lit8 v0, v2, 0x1

    int-to-short v0, v0

    .line 2739039
    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    move v2, v0

    move v3, v1

    goto/16 :goto_0

    .line 2739040
    :cond_6
    const-wide/16 v0, -0x1

    goto :goto_3

    :cond_7
    move-object v7, v5

    goto/16 :goto_1

    .line 2739041
    :cond_8
    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v12

    iget-wide v12, v12, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2739042
    invoke-static {v11, v12, v13}, LX/Iui;->a(LX/Iui;J)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 2739043
    iget-object v0, p0, LX/JqR;->g:LX/7G1;

    sget-object v1, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v0, v1, p1}, LX/7G1;->a(LX/7GT;Lcom/facebook/sync/analytics/FullRefreshReason;)V

    .line 2739044
    iget-object v0, p0, LX/JqR;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2739045
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2739046
    invoke-direct {p0, p2}, LX/JqR;->d(Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Long;

    move-result-object v2

    .line 2739047
    if-nez v2, :cond_0

    .line 2739048
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Failed to fetch initial sequence id from the server.  viewerContextUserId = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2739049
    sget-object v1, LX/JqR;->a:Ljava/lang/Class;

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2739050
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2739051
    :goto_0
    return-object v0

    .line 2739052
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, v1}, LX/JqR;->a(JLjava/lang/String;)LX/76M;

    move-result-object v3

    .line 2739053
    iget-boolean v0, v3, LX/76M;->a:Z

    if-nez v0, :cond_1

    .line 2739054
    sget-object v0, LX/JqR;->a:Ljava/lang/Class;

    const-string v4, "Failed to create queue with sequenceId %d, viewerContextUserId = %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object v1, v5, v2

    invoke-static {v0, v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2739055
    invoke-virtual {v3}, LX/76M;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2739056
    :cond_1
    iget-object v0, v3, LX/76M;->b:Ljava/lang/Object;

    check-cast v0, LX/7GC;

    iget-object v0, v0, LX/7GC;->b:Ljava/lang/String;

    .line 2739057
    iget-object v4, p0, LX/JqR;->d:LX/JqK;

    .line 2739058
    iget-object v5, v4, LX/JqK;->b:LX/6cy;

    sget-object p1, LX/6cx;->k:LX/2bA;

    invoke-virtual {v5, p1, v0}, LX/48u;->b(LX/0To;Ljava/lang/String;)V

    .line 2739059
    iget-object v0, p0, LX/JqR;->d:LX/JqK;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, LX/JqK;->a(J)V

    .line 2739060
    iget-object v0, p0, LX/JqR;->h:LX/7GB;

    iget-object v2, p0, LX/JqR;->d:LX/JqK;

    invoke-virtual {v0, v2}, LX/7GB;->d(LX/7Fx;)V

    .line 2739061
    iget-object v0, p0, LX/JqR;->c:LX/2Sx;

    sget-object v2, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-static {v1, v2}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object v1

    iget-wide v2, v3, LX/76M;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/2Sx;->a(LX/7G9;J)V

    .line 2739062
    iget-object v0, p0, LX/JqR;->e:LX/0Uh;

    const/16 v1, 0x1e9

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2739063
    iget-object v0, p0, LX/JqR;->o:LX/2My;

    invoke-virtual {v0}, LX/2My;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2739064
    :cond_2
    :goto_1
    iget-object v0, p0, LX/JqR;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;

    invoke-direct {v1, p0, p2}, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;-><init>(LX/JqR;Lcom/facebook/common/callercontext/CallerContext;)V

    const v2, -0x1073f11d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2739065
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2739066
    goto :goto_0

    .line 2739067
    :cond_3
    iget-object v0, p0, LX/JqR;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;

    invoke-direct {v1, p0, p2}, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;-><init>(LX/JqR;Lcom/facebook/common/callercontext/CallerContext;)V

    const v2, 0x2e7c55e0

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method
