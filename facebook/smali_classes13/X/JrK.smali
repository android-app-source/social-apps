.class public LX/JrK;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field public final a:LX/2OT;

.field private final b:LX/0SG;

.field private final c:LX/FDs;

.field private final d:LX/Jrb;

.field private final e:LX/Jqb;

.field private final f:LX/Jrc;

.field private final g:LX/Jqf;

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742965
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrK;->i:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/2OT;LX/0SG;LX/FDs;LX/Jrb;LX/Jqb;LX/Jrc;LX/0Ot;LX/Jqf;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2OT;",
            "LX/0SG;",
            "LX/FDs;",
            "LX/Jrb;",
            "LX/Jqb;",
            "LX/Jrc;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jqf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2742954
    invoke-direct {p0, p7}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742955
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742956
    iput-object v0, p0, LX/JrK;->h:LX/0Ot;

    .line 2742957
    iput-object p1, p0, LX/JrK;->a:LX/2OT;

    .line 2742958
    iput-object p2, p0, LX/JrK;->b:LX/0SG;

    .line 2742959
    iput-object p3, p0, LX/JrK;->c:LX/FDs;

    .line 2742960
    iput-object p4, p0, LX/JrK;->d:LX/Jrb;

    .line 2742961
    iput-object p5, p0, LX/JrK;->e:LX/Jqb;

    .line 2742962
    iput-object p6, p0, LX/JrK;->f:LX/Jrc;

    .line 2742963
    iput-object p8, p0, LX/JrK;->g:LX/Jqf;

    .line 2742964
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742951
    invoke-virtual {p1}, LX/6kW;->l()LX/6kP;

    move-result-object v0

    .line 2742952
    iget-object v1, p0, LX/JrK;->f:LX/Jrc;

    iget-object v0, v0, LX/6kP;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742953
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrK;
    .locals 7

    .prologue
    .line 2742865
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742866
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742867
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742868
    if-nez v1, :cond_0

    .line 2742869
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742870
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742871
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742872
    sget-object v1, LX/JrK;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742873
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742874
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742875
    :cond_1
    if-nez v1, :cond_4

    .line 2742876
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742877
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742878
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrK;->b(LX/0QB;)LX/JrK;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2742879
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742880
    if-nez v1, :cond_2

    .line 2742881
    sget-object v0, LX/JrK;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742882
    :goto_1
    if-eqz v0, :cond_3

    .line 2742883
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742884
    :goto_3
    check-cast v0, LX/JrK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742885
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742886
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742887
    :catchall_1
    move-exception v0

    .line 2742888
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742889
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742890
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742891
    :cond_2
    :try_start_8
    sget-object v0, LX/JrK;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kP;J)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 2742915
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/JrK;->d:LX/Jrb;

    .line 2742916
    iget-object v3, p2, LX/6kP;->image:LX/6jZ;

    if-nez v3, :cond_2

    .line 2742917
    sget-object v3, LX/2uW;->REMOVED_IMAGE:LX/2uW;

    .line 2742918
    :goto_0
    iget-object v5, p2, LX/6kP;->messageMetadata:LX/6kn;

    invoke-static {v0, v5, p1}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v5

    .line 2742919
    iput-object v3, v5, LX/6f7;->l:LX/2uW;

    .line 2742920
    move-object v3, v5

    .line 2742921
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2742922
    iget-object v5, v0, LX/Jrb;->f:LX/3QS;

    sget-object v6, LX/6fK;->SYNC_PROTOCOL_THREAD_IMAGE_DELTA:LX/6fK;

    invoke-virtual {v5, v6, v3}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2742923
    move-object v3, v3

    .line 2742924
    iget-object v0, p0, LX/JrK;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742925
    iget-object v0, p0, LX/JrK;->c:LX/FDs;

    invoke-virtual {v0, v1, p3, p4}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2742926
    iget-object v1, p2, LX/6kP;->image:LX/6jZ;

    if-nez v1, :cond_1

    .line 2742927
    iget-object v1, p0, LX/JrK;->c:LX/FDs;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2, v4, v4}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    .line 2742928
    :goto_1
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742929
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 2742930
    iget-object v3, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v3

    .line 2742931
    iget-object v4, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v4, v4

    .line 2742932
    iget-wide v9, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v9

    .line 2742933
    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742934
    iget-object v0, p2, LX/6kP;->messageMetadata:LX/6kn;

    .line 2742935
    if-eqz v0, :cond_0

    .line 2742936
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v0, v0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2742937
    if-eqz v0, :cond_0

    iget-object v0, p2, LX/6kP;->image:LX/6jZ;

    if-eqz v0, :cond_0

    .line 2742938
    iget-object v0, p0, LX/JrK;->g:LX/Jqf;

    invoke-virtual {v0, v1}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2742939
    :cond_0
    return-object v1

    .line 2742940
    :cond_1
    iget-object v1, p0, LX/JrK;->c:LX/FDs;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, LX/6kP;->image:LX/6jZ;

    iget-object v5, v5, LX/6jZ;->filename:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2742941
    iget-object v4, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v4, v4

    .line 2742942
    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 2742943
    iget-object v5, p0, LX/JrK;->a:LX/2OT;

    invoke-virtual {v5}, LX/2OT;->a()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 2742944
    invoke-static {v4}, LX/2OT;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2742945
    const-string v7, "mid"

    invoke-virtual {v5, v7, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2742946
    const-string v6, "aid"

    const-string v7, "1"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2742947
    const-string v6, "format"

    const-string v7, "binary"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2742948
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    move-object v4, v5

    .line 2742949
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    goto :goto_1

    .line 2742950
    :cond_2
    sget-object v3, LX/2uW;->SET_IMAGE:LX/2uW;

    goto/16 :goto_0
.end method

.method private static b(LX/0QB;)LX/JrK;
    .locals 9

    .prologue
    .line 2742911
    new-instance v0, LX/JrK;

    invoke-static {p0}, LX/2OT;->b(LX/0QB;)LX/2OT;

    move-result-object v1

    check-cast v1, LX/2OT;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v3

    check-cast v3, LX/FDs;

    invoke-static {p0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v4

    check-cast v4, LX/Jrb;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v5

    check-cast v5, LX/Jqb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v6

    check-cast v6, LX/Jrc;

    const/16 v7, 0x35bd

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/Jqf;->a(LX/0QB;)LX/Jqf;

    move-result-object v8

    check-cast v8, LX/Jqf;

    invoke-direct/range {v0 .. v8}, LX/JrK;-><init>(LX/2OT;LX/0SG;LX/FDs;LX/Jrb;LX/Jqb;LX/Jrc;LX/0Ot;LX/Jqf;)V

    .line 2742912
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742913
    iput-object v1, v0, LX/JrK;->h:LX/0Ot;

    .line 2742914
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742909
    check-cast p1, LX/6kW;

    .line 2742910
    invoke-direct {p0, p1}, LX/JrK;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742905
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->l()LX/6kP;

    move-result-object v0

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-direct {p0, p1, v0, v2, v3}, LX/JrK;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kP;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2742906
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2742907
    const-string v2, "newMessageResult"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2742908
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742893
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742894
    if-eqz v0, :cond_0

    .line 2742895
    iget-object v1, p0, LX/JrK;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2742896
    iget-object v1, p0, LX/JrK;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2742897
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2742898
    iget-wide v6, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v6

    .line 2742899
    invoke-virtual {v1, v2, v4, v5}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742900
    iget-object v1, p0, LX/JrK;->e:LX/Jqb;

    .line 2742901
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2742902
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742903
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2742904
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742892
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrK;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
