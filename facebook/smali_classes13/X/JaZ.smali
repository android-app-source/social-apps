.class public final LX/JaZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JaX;


# instance fields
.field private a:Z

.field private b:Z

.field public c:I


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1

    .prologue
    .line 2715599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2715600
    iput-boolean p1, p0, LX/JaZ;->a:Z

    .line 2715601
    const v0, 0x7f083ab9

    iput v0, p0, LX/JaZ;->c:I

    .line 2715602
    iput-boolean p2, p0, LX/JaZ;->b:Z

    .line 2715603
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2715585
    iget-boolean v0, p0, LX/JaZ;->a:Z

    if-eqz v0, :cond_0

    .line 2715586
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2715587
    const-string v1, "is_creator_panel_enabled"

    iget-boolean v2, p0, LX/JaZ;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2715588
    invoke-static {}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->m()LX/F5b;

    move-result-object v1

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v2

    const-string v3, "FBGroupsCreationRoute"

    .line 2715589
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 2715590
    move-object v2, v2

    .line 2715591
    iput v4, v2, LX/98r;->h:I

    .line 2715592
    move-object v2, v2

    .line 2715593
    iput-boolean v4, v2, LX/98r;->m:Z

    .line 2715594
    move-object v2, v2

    .line 2715595
    iput-object v0, v2, LX/98r;->f:Landroid/os/Bundle;

    .line 2715596
    move-object v0, v2

    .line 2715597
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/F5b;->a(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    invoke-virtual {v0}, LX/F5b;->a()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    .line 2715598
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;-><init>()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2715604
    iget-boolean v0, p0, LX/JaZ;->a:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2715584
    iget v0, p0, LX/JaZ;->c:I

    return v0
.end method

.method public final d()LX/Jac;
    .locals 1

    .prologue
    .line 2715583
    sget-object v0, LX/Jac;->CREATE:LX/Jac;

    return-object v0
.end method
