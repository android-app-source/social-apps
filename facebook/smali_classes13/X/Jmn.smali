.class public LX/Jmn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;

.field public final i:LX/JpA;

.field public final j:I

.field public final k:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;IILjava/lang/String;LX/0Px;LX/0Px;LX/JpA;Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "II",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/JpA;",
            "Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 2733272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2733273
    iput-object p1, p0, LX/Jmn;->a:Ljava/lang/String;

    .line 2733274
    iput-object p2, p0, LX/Jmn;->b:Ljava/lang/String;

    .line 2733275
    iput-object p3, p0, LX/Jmn;->c:Landroid/net/Uri;

    .line 2733276
    iput p4, p0, LX/Jmn;->j:I

    .line 2733277
    iput p5, p0, LX/Jmn;->d:I

    .line 2733278
    iput-object p6, p0, LX/Jmn;->e:Ljava/lang/String;

    .line 2733279
    iput-object p7, p0, LX/Jmn;->f:LX/0Px;

    .line 2733280
    iput-object p8, p0, LX/Jmn;->g:LX/0Px;

    .line 2733281
    iput-object p9, p0, LX/Jmn;->i:LX/JpA;

    .line 2733282
    iput-object p10, p0, LX/Jmn;->h:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;

    .line 2733283
    iput-wide p11, p0, LX/Jmn;->k:J

    .line 2733284
    return-void
.end method
