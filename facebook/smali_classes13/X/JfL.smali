.class public LX/JfL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FD1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/JfL;


# instance fields
.field private final a:LX/0So;

.field private final b:LX/0Zb;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0W3;

.field private final e:J


# direct methods
.method public constructor <init>(LX/0So;LX/0Zb;LX/0W3;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721767
    iput-object p1, p0, LX/JfL;->a:LX/0So;

    .line 2721768
    iput-object p2, p0, LX/JfL;->b:LX/0Zb;

    .line 2721769
    iput-object p3, p0, LX/JfL;->d:LX/0W3;

    .line 2721770
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/JfL;->c:Ljava/util/Map;

    .line 2721771
    iget-object v0, p0, LX/JfL;->d:LX/0W3;

    sget-wide v2, LX/0X5;->ic:J

    const/16 v1, 0x3c

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/JfL;->e:J

    .line 2721772
    return-void
.end method

.method public static a(LX/0QB;)LX/JfL;
    .locals 6

    .prologue
    .line 2721773
    sget-object v0, LX/JfL;->f:LX/JfL;

    if-nez v0, :cond_1

    .line 2721774
    const-class v1, LX/JfL;

    monitor-enter v1

    .line 2721775
    :try_start_0
    sget-object v0, LX/JfL;->f:LX/JfL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2721776
    if-eqz v2, :cond_0

    .line 2721777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2721778
    new-instance p0, LX/JfL;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/JfL;-><init>(LX/0So;LX/0Zb;LX/0W3;)V

    .line 2721779
    move-object v0, p0

    .line 2721780
    sput-object v0, LX/JfL;->f:LX/JfL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2721782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2721783
    :cond_1
    sget-object v0, LX/JfL;->f:LX/JfL;

    return-object v0

    .line 2721784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2721785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
