.class public final LX/Jx0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/Jx9;",
        "LX/4Iv;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JxB;


# direct methods
.method public constructor <init>(LX/JxB;)V
    .locals 0

    .prologue
    .line 2752692
    iput-object p1, p0, LX/Jx0;->a:LX/JxB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 2752693
    check-cast p1, LX/Jx9;

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2752694
    iget-object v0, p1, LX/Jx9;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/Jx9;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2752695
    if-nez v0, :cond_0

    .line 2752696
    iget-object v0, p0, LX/Jx0;->a:LX/JxB;

    iget-object v0, v0, LX/JxB;->h:LX/0bW;

    const-string v1, "Non-empty ParsedPayload with %d pulsars and %d BLEs"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, LX/Jx9;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p1, LX/Jx9;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2752697
    invoke-virtual {p1}, LX/Jx9;->b()LX/4Iv;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2752698
    :goto_1
    return-object v0

    :cond_0
    new-array v1, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v0, p0, LX/Jx0;->a:LX/JxB;

    iget-object v0, v0, LX/JxB;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v0, v1, v4

    iget-object v0, p0, LX/Jx0;->a:LX/JxB;

    iget-object v0, v0, LX/JxB;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v0, v1, v5

    invoke-static {v1}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Jwz;

    invoke-direct {v1, p0, p1}, LX/Jwz;-><init>(LX/Jx0;LX/Jx9;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
