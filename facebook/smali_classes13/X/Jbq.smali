.class public LX/Jbq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Jbq;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2aN;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2aN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717153
    iput-object p1, p0, LX/Jbq;->a:Landroid/content/Context;

    .line 2717154
    iput-object p2, p0, LX/Jbq;->b:LX/2aN;

    .line 2717155
    return-void
.end method

.method public static a(LX/0QB;)LX/Jbq;
    .locals 5

    .prologue
    .line 2717156
    sget-object v0, LX/Jbq;->c:LX/Jbq;

    if-nez v0, :cond_1

    .line 2717157
    const-class v1, LX/Jbq;

    monitor-enter v1

    .line 2717158
    :try_start_0
    sget-object v0, LX/Jbq;->c:LX/Jbq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2717159
    if-eqz v2, :cond_0

    .line 2717160
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2717161
    new-instance p0, LX/Jbq;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2aN;->b(LX/0QB;)LX/2aN;

    move-result-object v4

    check-cast v4, LX/2aN;

    invoke-direct {p0, v3, v4}, LX/Jbq;-><init>(Landroid/content/Context;LX/2aN;)V

    .line 2717162
    move-object v0, p0

    .line 2717163
    sput-object v0, LX/Jbq;->c:LX/Jbq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2717164
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2717165
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2717166
    :cond_1
    sget-object v0, LX/Jbq;->c:LX/Jbq;

    return-object v0

    .line 2717167
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2717168
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 7

    .prologue
    .line 2717169
    iget-object v0, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v6, v0

    .line 2717170
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->METADATA:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;

    move-result-object v0

    .line 2717171
    const/4 v2, 0x0

    .line 2717172
    if-eqz v0, :cond_0

    .line 2717173
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/Jbq;->a:Landroid/content/Context;

    const-class v3, Lcom/facebook/auth/login/LoginApprovalNotificationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "extra_login_approval_notification_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "arg_action"

    const-string v2, "action_approve"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_show_toast"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_COMPONENT_TYPE"

    sget-object v2, LX/8D4;->SERVICE:LX/8D4;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    .line 2717174
    :cond_0
    iget-object v0, p0, LX/Jbq;->b:LX/2aN;

    .line 2717175
    iget-object v1, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v1, v1

    .line 2717176
    invoke-virtual {v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v3, LX/8D4;->SERVICE:LX/8D4;

    sget-object v4, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    .line 2717177
    iget v5, p1, LX/Drw;->a:I

    move v5, v5

    .line 2717178
    invoke-virtual/range {v0 .. v5}, LX/2aN;->b(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2717179
    new-instance v1, LX/3pX;

    const v2, 0x7f0207d7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v1}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2717180
    const/4 v0, 0x0

    return v0
.end method
