.class public LX/K4o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:J

.field public B:J

.field public C:I

.field public D:I

.field public final b:LX/K4W;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5pG;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Runnable;

.field public final e:Ljava/lang/String;

.field public f:LX/K4g;

.field public g:Landroid/os/Handler;

.field public h:Landroid/media/MediaPlayer;

.field public i:LX/K42;

.field public j:LX/K4q;

.field public k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:LX/K4l;

.field public q:LX/K3c;

.field public r:LX/0So;

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2769428
    const-class v0, LX/K4o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K4o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/K4h;LX/K4W;Landroid/content/Context;LX/0So;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2769429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769430
    iput-object p2, p0, LX/K4o;->b:LX/K4W;

    .line 2769431
    iget-object v0, p0, LX/K4o;->b:LX/K4W;

    new-instance v1, LX/K4k;

    invoke-direct {v1, p0}, LX/K4k;-><init>(LX/K4o;)V

    .line 2769432
    iput-object v1, v0, LX/K4W;->h:LX/K4k;

    .line 2769433
    new-instance v2, LX/K4V;

    invoke-direct {v2, v0}, LX/K4V;-><init>(LX/K4W;)V

    iput-object v2, v0, LX/K4W;->i:LX/K4V;

    .line 2769434
    iget-object v2, v0, LX/K4W;->c:LX/K4Z;

    iget-object v3, v0, LX/K4W;->i:LX/K4V;

    .line 2769435
    iget-object v1, v2, LX/K4Z;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2769436
    iget-object v2, v0, LX/K4W;->b:LX/33u;

    invoke-virtual {v2}, LX/33u;->c()LX/33y;

    move-result-object v2

    iput-object v2, v0, LX/K4W;->g:LX/33y;

    .line 2769437
    new-instance v2, LX/K4U;

    invoke-direct {v2, v0}, LX/K4U;-><init>(LX/K4W;)V

    iput-object v2, v0, LX/K4W;->j:LX/9mm;

    .line 2769438
    iget-object v2, v0, LX/K4W;->g:LX/33y;

    iget-object v3, v0, LX/K4W;->j:LX/9mm;

    invoke-virtual {v2, v3}, LX/33y;->a(LX/9mm;)V

    .line 2769439
    iget-object v2, v0, LX/K4W;->g:LX/33y;

    invoke-virtual {v2}, LX/33y;->k()LX/5pX;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2769440
    iget-object v2, v0, LX/K4W;->g:LX/33y;

    invoke-virtual {v2}, LX/33y;->k()LX/5pX;

    move-result-object v2

    invoke-static {v0, v2}, LX/K4W;->a$redex0(LX/K4W;LX/5pX;)V

    .line 2769441
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SlGlThread"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 2769442
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2769443
    new-instance v1, LX/K4m;

    invoke-direct {v1, p0}, LX/K4m;-><init>(LX/K4o;)V

    iput-object v1, p0, LX/K4o;->g:Landroid/os/Handler;

    .line 2769444
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, LX/K4n;

    invoke-direct {v1, p0}, LX/K4n;-><init>(LX/K4o;)V

    iget-object v2, p0, LX/K4o;->g:Landroid/os/Handler;

    .line 2769445
    new-instance v4, LX/K4g;

    const-class v5, LX/K4u;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/K4u;

    .line 2769446
    const/16 v5, 0x1423

    invoke-static {p1, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v5}, LX/K3b;->a(LX/0Or;)LX/K3f;

    move-result-object v5

    move-object v9, v5

    .line 2769447
    check-cast v9, LX/K3f;

    move-object v5, v0

    move-object v6, v1

    move-object v7, v2

    invoke-direct/range {v4 .. v9}, LX/K4g;-><init>(Landroid/os/Looper;LX/K4n;Landroid/os/Handler;LX/K4u;LX/K3f;)V

    .line 2769448
    move-object v0, v4

    .line 2769449
    iput-object v0, p0, LX/K4o;->f:LX/K4g;

    .line 2769450
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K4o;->c:Ljava/util/List;

    .line 2769451
    new-instance v0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;-><init>(LX/K4o;)V

    iput-object v0, p0, LX/K4o;->d:Ljava/lang/Runnable;

    .line 2769452
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2769453
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "https://lookaside.facebook.com/assets/"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2769454
    const-string v1, "friendsharing_storyline/moments-balloons@"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2769455
    const/high16 v1, 0x3f800000    # 1.0f

    rem-float v1, v0, v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-nez v1, :cond_1

    float-to-int v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2769456
    const-string v1, "x.png"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2769457
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2769458
    iput-object v0, p0, LX/K4o;->e:Ljava/lang/String;

    .line 2769459
    iput-object p4, p0, LX/K4o;->r:LX/0So;

    .line 2769460
    sget-object v0, LX/K4l;->UNINITIALIZED:LX/K4l;

    invoke-static {p0, v0}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2769461
    return-void

    .line 2769462
    :cond_1
    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static a$redex0(LX/K4o;LX/K4l;)V
    .locals 3

    .prologue
    .line 2769463
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    if-ne v0, p1, :cond_1

    .line 2769464
    :cond_0
    :goto_0
    return-void

    .line 2769465
    :cond_1
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->DESTROYED:LX/K4l;

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2769466
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    .line 2769467
    iput-object p1, p0, LX/K4o;->p:LX/K4l;

    .line 2769468
    iget-object v1, p0, LX/K4o;->i:LX/K42;

    if-eqz v1, :cond_0

    .line 2769469
    iget-object v1, p0, LX/K4o;->i:LX/K42;

    iget-object v2, p0, LX/K4o;->p:LX/K4l;

    invoke-virtual {v1, v2, v0}, LX/K42;->a(LX/K4l;LX/K4l;)V

    goto :goto_0

    .line 2769470
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(F)I
    .locals 1

    .prologue
    .line 2769471
    iget-object v0, p0, LX/K4o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method public static b(LX/0QB;)LX/K4o;
    .locals 6

    .prologue
    .line 2769478
    new-instance v4, LX/K4o;

    const-class v0, LX/K4h;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/K4h;

    .line 2769479
    new-instance v5, LX/K4W;

    invoke-static {p0}, LX/33u;->a(LX/0QB;)LX/33u;

    move-result-object v1

    check-cast v1, LX/33u;

    invoke-static {p0}, LX/K4Z;->a(LX/0QB;)LX/K4Z;

    move-result-object v2

    check-cast v2, LX/K4Z;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {v5, v1, v2, v3}, LX/K4W;-><init>(LX/33u;LX/K4Z;LX/0Sh;)V

    .line 2769480
    move-object v1, v5

    .line 2769481
    check-cast v1, LX/K4W;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {v4, v0, v1, v2, v3}, LX/K4o;-><init>(LX/K4h;LX/K4W;Landroid/content/Context;LX/0So;)V

    .line 2769482
    return-object v4
.end method

.method public static b(LX/K4o;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2769472
    iput-boolean v3, p0, LX/K4o;->t:Z

    .line 2769473
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 2769474
    :try_start_0
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 2769475
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2769476
    :goto_0
    return-void

    .line 2769477
    :catch_0
    sget-object v0, LX/K4o;->a:Ljava/lang/String;

    const-string v1, "Prepare music failed, uri: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static k(LX/K4o;)V
    .locals 7

    .prologue
    .line 2769484
    sget-object v0, LX/K4l;->INITIALIZED:LX/K4l;

    invoke-static {p0, v0}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2769485
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->INITIALIZED:LX/K4l;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/K4o;->j:LX/K4q;

    if-nez v0, :cond_2

    .line 2769486
    :cond_0
    :goto_0
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->INITIALIZED:LX/K4l;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/K4o;->j:LX/K4q;

    if-nez v0, :cond_7

    .line 2769487
    :cond_1
    :goto_1
    invoke-static {p0}, LX/K4o;->s(LX/K4o;)V

    .line 2769488
    return-void

    .line 2769489
    :cond_2
    iget-object v0, p0, LX/K4o;->j:LX/K4q;

    const/4 v2, 0x0

    .line 2769490
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2769491
    iget-object v5, v0, LX/K4q;->a:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyline/model/StorylinePhoto;

    .line 2769492
    iget-object v1, v1, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2769493
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 2769494
    :cond_3
    iget-object v3, v0, LX/K4q;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_3
    if-ge v2, v5, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyline/model/StorylineUser;

    .line 2769495
    iget-object v1, v1, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2769496
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2769497
    :cond_4
    iget-object v1, v0, LX/K4q;->g:LX/K4p;

    sget-object v2, LX/K4p;->MOMENTS:LX/K4p;

    if-ne v1, v2, :cond_5

    .line 2769498
    iget-object v1, p0, LX/K4o;->e:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2769499
    :cond_5
    iget-object v1, v0, LX/K4q;->d:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v1}, Lcom/facebook/storyline/model/Mood;->c()LX/0P1;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2769500
    iget-object v1, v0, LX/K4q;->d:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v1}, Lcom/facebook/storyline/model/Mood;->c()LX/0P1;

    move-result-object v1

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2769501
    :cond_6
    move-object v0, v4

    .line 2769502
    iget-object v1, p0, LX/K4o;->k:Ljava/util/Set;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2769503
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/K4o;->s:Z

    .line 2769504
    iput-object v0, p0, LX/K4o;->k:Ljava/util/Set;

    .line 2769505
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/K4o;->n:Ljava/lang/String;

    .line 2769506
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    iget-object v1, p0, LX/K4o;->f:LX/K4g;

    sget-object v2, LX/K4d;->UPDATE_TEXTURES:LX/K4d;

    iget-object v3, p0, LX/K4o;->k:Ljava/util/Set;

    iget-object v4, p0, LX/K4o;->n:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/K4g;->a(LX/K4d;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K4g;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2769507
    :cond_7
    iget-object v0, p0, LX/K4o;->j:LX/K4q;

    iget-object v0, v0, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    iget-object v0, v0, Lcom/facebook/storyline/model/Cutdown;->c:Ljava/lang/String;

    .line 2769508
    iget-object v1, p0, LX/K4o;->o:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2769509
    iput-object v0, p0, LX/K4o;->o:Ljava/lang/String;

    .line 2769510
    const/4 v0, 0x0

    iput v0, p0, LX/K4o;->D:I

    .line 2769511
    iget-object v0, p0, LX/K4o;->o:Ljava/lang/String;

    invoke-static {p0, v0}, LX/K4o;->b(LX/K4o;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static n(LX/K4o;)F
    .locals 3

    .prologue
    .line 2769483
    iget v0, p0, LX/K4o;->y:I

    int-to-float v0, v0

    iget-object v1, p0, LX/K4o;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public static p$redex0(LX/K4o;)V
    .locals 7

    .prologue
    .line 2769376
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    iget-object v1, p0, LX/K4o;->f:LX/K4g;

    sget-object v2, LX/K4d;->DRAW_FRAME:LX/K4d;

    iget v3, p0, LX/K4o;->w:I

    iget v4, p0, LX/K4o;->x:I

    iget-object v5, p0, LX/K4o;->c:Ljava/util/List;

    iget v6, p0, LX/K4o;->y:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 2769377
    invoke-virtual {v2}, LX/K4d;->ordinal()I

    move-result v6

    invoke-virtual {v1, v6, v3, v4, v5}, LX/K4g;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    move-object v1, v6

    .line 2769378
    invoke-virtual {v0, v1}, LX/K4g;->sendMessage(Landroid/os/Message;)Z

    .line 2769379
    iget-object v0, p0, LX/K4o;->i:LX/K42;

    if-eqz v0, :cond_0

    .line 2769380
    iget-object v0, p0, LX/K4o;->i:LX/K42;

    invoke-static {p0}, LX/K4o;->n(LX/K4o;)F

    move-result v1

    invoke-virtual {v0, v1}, LX/K42;->a(F)V

    .line 2769381
    :cond_0
    return-void
.end method

.method public static s(LX/K4o;)V
    .locals 11

    .prologue
    .line 2769382
    iget-boolean v0, p0, LX/K4o;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K4o;->j:LX/K4q;

    if-nez v0, :cond_1

    .line 2769383
    :cond_0
    :goto_0
    return-void

    .line 2769384
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K4o;->v:Z

    .line 2769385
    iget-object v0, p0, LX/K4o;->b:LX/K4W;

    iget-object v1, p0, LX/K4o;->j:LX/K4q;

    .line 2769386
    iget-object v2, v0, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 2769387
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    .line 2769388
    const-string v2, "launchSurface"

    iget-object v4, v1, LX/K4q;->g:LX/K4p;

    invoke-virtual {v4}, LX/K4p;->getReactNativeLaunchSurface()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769389
    const-string v2, "photos"

    iget-object v4, v1, LX/K4q;->a:LX/0Px;

    invoke-static {v4}, LX/K4Y;->a(Ljava/util/List;)LX/5pD;

    move-result-object v4

    invoke-interface {v3, v2, v4}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2769390
    const-string v2, "title"

    iget-object v4, v1, LX/K4q;->c:Ljava/lang/String;

    invoke-interface {v3, v2, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769391
    const-string v4, "users"

    iget-object v2, v1, LX/K4q;->b:LX/0Px;

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v2

    :goto_1
    invoke-interface {v3, v4, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2769392
    const-string v2, "selectedPhotoIDs"

    iget-object v4, v1, LX/K4q;->a:LX/0Px;

    .line 2769393
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v6

    .line 2769394
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/storyline/model/StorylinePhoto;

    .line 2769395
    iget-object v5, v5, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    invoke-interface {v6, v5}, LX/5pD;->pushString(Ljava/lang/String;)V

    goto :goto_2

    .line 2769396
    :cond_2
    move-object v4, v6

    .line 2769397
    invoke-interface {v3, v2, v4}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2769398
    const-string v2, "musicData"

    iget-object v4, v1, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    .line 2769399
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v5

    .line 2769400
    const-string v6, "maxPhotos"

    iget v7, v4, Lcom/facebook/storyline/model/Cutdown;->f:I

    invoke-interface {v5, v6, v7}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2769401
    const-string v6, "minPhotos"

    iget v7, v4, Lcom/facebook/storyline/model/Cutdown;->e:I

    invoke-interface {v5, v6, v7}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2769402
    const-string v6, "duration"

    iget v7, v4, Lcom/facebook/storyline/model/Cutdown;->b:I

    int-to-double v7, v7

    const-wide v9, 0x408f400000000000L    # 1000.0

    div-double/2addr v7, v9

    invoke-interface {v5, v6, v7, v8}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2769403
    const-string v6, "filename"

    iget-object v7, v4, Lcom/facebook/storyline/model/Cutdown;->c:Ljava/lang/String;

    invoke-interface {v5, v6, v7}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769404
    const-string v6, "audioIdentifier"

    iget-object v7, v4, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v7}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769405
    const-string v6, "audioCacheName"

    iget-object v7, v4, Lcom/facebook/storyline/model/Cutdown;->d:Ljava/lang/String;

    invoke-interface {v5, v6, v7}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769406
    const-string v6, "sections"

    iget-object v7, v4, Lcom/facebook/storyline/model/Cutdown;->g:LX/0Px;

    invoke-static {v7}, LX/K4Y;->a(Ljava/util/List;)LX/5pD;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2769407
    move-object v4, v5

    .line 2769408
    invoke-interface {v3, v2, v4}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2769409
    const-string v2, "selectedMoodKey"

    iget-object v4, v1, LX/K4q;->d:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v4}, Lcom/facebook/storyline/model/Mood;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769410
    const-string v4, "animationDescriptorID"

    iget-object v2, v0, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v4, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769411
    const-string v2, "fps"

    iget v4, v1, LX/K4q;->f:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-interface {v3, v2, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2769412
    const-string v2, "animationData"

    iget-object v4, v1, LX/K4q;->d:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v4}, Lcom/facebook/storyline/model/Mood;->a()Lcom/facebook/storyline/model/VisualData;

    move-result-object v4

    .line 2769413
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v5

    .line 2769414
    if-nez v4, :cond_4

    .line 2769415
    :goto_3
    move-object v4, v5

    .line 2769416
    invoke-interface {v3, v2, v4}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2769417
    iget-object v2, v0, LX/K4W;->f:Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v4, "storylineCalculateAnimationDescriptor"

    invoke-interface {v2, v4, v3}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2769418
    iget-object v2, v0, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 2769419
    goto/16 :goto_0

    .line 2769420
    :cond_3
    iget-object v2, v1, LX/K4q;->b:LX/0Px;

    invoke-static {v2}, LX/K4Y;->a(Ljava/util/List;)LX/5pD;

    move-result-object v2

    goto/16 :goto_1

    .line 2769421
    :cond_4
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->beginningEffect:Lcom/facebook/storyline/model/VisualData$Effect;

    const-string v7, "beginningEffect"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(Lcom/facebook/storyline/model/VisualData$Effect;LX/5pH;Ljava/lang/String;)V

    .line 2769422
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->itemEffect:Lcom/facebook/storyline/model/VisualData$Effect;

    const-string v7, "itemEffect"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(Lcom/facebook/storyline/model/VisualData$Effect;LX/5pH;Ljava/lang/String;)V

    .line 2769423
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->endingEffect:Lcom/facebook/storyline/model/VisualData$Effect;

    const-string v7, "endingEffect"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(Lcom/facebook/storyline/model/VisualData$Effect;LX/5pH;Ljava/lang/String;)V

    .line 2769424
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->primaryTransitions:LX/0Px;

    const-string v7, "primaryTransitions"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(LX/0Px;LX/5pH;Ljava/lang/String;)V

    .line 2769425
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->secondaryTransitions:LX/0Px;

    const-string v7, "secondaryTransitions"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(LX/0Px;LX/5pH;Ljava/lang/String;)V

    .line 2769426
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->tertiaryTransitions:LX/0Px;

    const-string v7, "tertiaryTransitions"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(LX/0Px;LX/5pH;Ljava/lang/String;)V

    .line 2769427
    iget-object v6, v4, Lcom/facebook/storyline/model/VisualData;->additionalTransitions:LX/0Px;

    const-string v7, "additionalTransitions"

    invoke-static {v6, v5, v7}, LX/K4Y;->a(LX/0Px;LX/5pH;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static t(LX/K4o;)V
    .locals 5

    .prologue
    .line 2769372
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->INITIALIZED:LX/K4l;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/K4o;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/K4o;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/K4o;->v:Z

    if-nez v0, :cond_1

    .line 2769373
    :cond_0
    :goto_0
    return-void

    .line 2769374
    :cond_1
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    iget-object v1, p0, LX/K4o;->f:LX/K4g;

    sget-object v2, LX/K4d;->INIT_DATA:LX/K4d;

    iget-object v3, p0, LX/K4o;->l:LX/0P1;

    iget-object v4, p0, LX/K4o;->j:LX/K4q;

    iget-object v4, v4, LX/K4q;->d:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v4}, Lcom/facebook/storyline/model/Mood;->c()LX/0P1;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/K4g;->a(LX/K4d;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K4g;->sendMessage(Landroid/os/Message;)Z

    .line 2769375
    sget-object v0, LX/K4l;->READY_TO_PLAY:LX/K4l;

    invoke-static {p0, v0}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2769367
    sget-object v0, LX/K4i;->a:[I

    iget-object v1, p0, LX/K4o;->p:LX/K4l;

    invoke-virtual {v1}, LX/K4l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2769368
    :goto_0
    return-void

    .line 2769369
    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, LX/K4o;->y:I

    .line 2769370
    invoke-virtual {p0}, LX/K4o;->b()V

    goto :goto_0

    .line 2769371
    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/K4o;->a(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 2769353
    sget-object v0, LX/K4i;->a:[I

    iget-object v1, p0, LX/K4o;->p:LX/K4l;

    invoke-virtual {v1}, LX/K4l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2769354
    :goto_0
    return-void

    .line 2769355
    :pswitch_0
    invoke-direct {p0, p1}, LX/K4o;->b(F)I

    move-result v0

    iput v0, p0, LX/K4o;->y:I

    .line 2769356
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    sget-object v1, LX/K4d;->DRAW_FRAME:LX/K4d;

    invoke-virtual {v0, v1}, LX/K4g;->b(LX/K4d;)V

    .line 2769357
    invoke-static {p0}, LX/K4o;->p$redex0(LX/K4o;)V

    goto :goto_0

    .line 2769358
    :pswitch_1
    invoke-direct {p0, p1}, LX/K4o;->b(F)I

    move-result v0

    iput v0, p0, LX/K4o;->y:I

    .line 2769359
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    sget-object v1, LX/K4d;->DRAW_FRAME:LX/K4d;

    invoke-virtual {v0, v1}, LX/K4g;->b(LX/K4d;)V

    .line 2769360
    iget-object v0, p0, LX/K4o;->g:Landroid/os/Handler;

    iget-object v1, p0, LX/K4o;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2769361
    iget v2, p0, LX/K4o;->y:I

    iput v2, p0, LX/K4o;->C:I

    .line 2769362
    iget-object v2, p0, LX/K4o;->r:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/K4o;->B:J

    .line 2769363
    iget-object v2, p0, LX/K4o;->d:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 2769364
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    .line 2769365
    invoke-static {p0}, LX/K4o;->n(LX/K4o;)F

    move-result v1

    iget-object v2, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move v1, v1

    .line 2769366
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 2769350
    iput p1, p0, LX/K4o;->w:I

    .line 2769351
    iput p2, p0, LX/K4o;->x:I

    .line 2769352
    return-void
.end method

.method public final a(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2769336
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->UNINITIALIZED:LX/K4l;

    if-eq v0, v1, :cond_0

    .line 2769337
    :goto_0
    return-void

    .line 2769338
    :cond_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    .line 2769339
    new-instance v0, LX/K4j;

    invoke-direct {v0, p0}, LX/K4j;-><init>(LX/K4o;)V

    .line 2769340
    iget-object v1, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2769341
    iget-object v1, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2769342
    iget-object v1, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 2769343
    iget-object v1, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2769344
    iget-object v1, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 2769345
    iget-object v1, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 2769346
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 2769347
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2769348
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    iget-object v1, p0, LX/K4o;->f:LX/K4g;

    sget-object v2, LX/K4d;->CREATE_EGL_CONTEXT:LX/K4d;

    invoke-virtual {v1, v2, p1}, LX/K4g;->a(LX/K4d;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/K4g;->sendMessage(Landroid/os/Message;)Z

    .line 2769349
    invoke-static {p0}, LX/K4o;->k(LX/K4o;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2769331
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->READY_TO_PLAY:LX/K4l;

    if-eq v0, v1, :cond_0

    .line 2769332
    :goto_0
    return-void

    .line 2769333
    :cond_0
    sget-object v0, LX/K4l;->PLAYING:LX/K4l;

    invoke-static {p0, v0}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2769334
    invoke-static {p0}, LX/K4o;->n(LX/K4o;)F

    move-result v0

    invoke-virtual {p0, v0}, LX/K4o;->a(F)V

    .line 2769335
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2769326
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->PLAYING:LX/K4l;

    if-eq v0, v1, :cond_0

    .line 2769327
    :goto_0
    return-void

    .line 2769328
    :cond_0
    sget-object v0, LX/K4l;->READY_TO_PLAY:LX/K4l;

    invoke-static {p0, v0}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2769329
    iget-object v0, p0, LX/K4o;->g:Landroid/os/Handler;

    iget-object v1, p0, LX/K4o;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2769330
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2769321
    iget-object v0, p0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->EXPORTING:LX/K4l;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/K4o;->q:LX/K3c;

    if-nez v0, :cond_1

    .line 2769322
    :cond_0
    :goto_0
    return-void

    .line 2769323
    :cond_1
    iget-object v0, p0, LX/K4o;->q:LX/K3c;

    .line 2769324
    iget-object v1, v0, LX/K3c;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2769325
    const/4 v0, 0x0

    iput-object v0, p0, LX/K4o;->q:LX/K3c;

    goto :goto_0
.end method

.method public final f()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2769301
    sget-object v0, LX/K4i;->a:[I

    iget-object v1, p0, LX/K4o;->p:LX/K4l;

    invoke-virtual {v1}, LX/K4l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2769302
    :goto_0
    iput v3, p0, LX/K4o;->y:I

    .line 2769303
    iget-object v0, p0, LX/K4o;->g:Landroid/os/Handler;

    iget-object v1, p0, LX/K4o;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2769304
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    sget-object v1, LX/K4d;->DRAW_FRAME:LX/K4d;

    invoke-virtual {v0, v1}, LX/K4g;->b(LX/K4d;)V

    .line 2769305
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    .line 2769306
    iget-object v1, v0, LX/K4g;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2769307
    sget-object v0, LX/K4l;->UNINITIALIZED:LX/K4l;

    invoke-static {p0, v0}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2769308
    iget-object v0, p0, LX/K4o;->f:LX/K4g;

    sget-object v1, LX/K4d;->DESTROY_EGL_CONTEXT:LX/K4d;

    invoke-virtual {v0, v1}, LX/K4g;->a(LX/K4d;)Z

    .line 2769309
    iput-object v2, p0, LX/K4o;->j:LX/K4q;

    .line 2769310
    iput-boolean v3, p0, LX/K4o;->s:Z

    .line 2769311
    iput-object v2, p0, LX/K4o;->k:Ljava/util/Set;

    .line 2769312
    iput-object v2, p0, LX/K4o;->n:Ljava/lang/String;

    .line 2769313
    iput-object v2, p0, LX/K4o;->o:Ljava/lang/String;

    .line 2769314
    iput-boolean v3, p0, LX/K4o;->t:Z

    .line 2769315
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 2769316
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 2769317
    iget-object v0, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 2769318
    iput-object v2, p0, LX/K4o;->h:Landroid/media/MediaPlayer;

    .line 2769319
    :pswitch_0
    return-void

    .line 2769320
    :pswitch_1
    invoke-virtual {p0}, LX/K4o;->e()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
