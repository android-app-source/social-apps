.class public LX/JuE;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/notes/view/block/TimestampAndPrivacyBlockView;",
        "Lcom/facebook/notes/model/data/TimestampAndPrivacyBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/JuG;)V
    .locals 0

    .prologue
    .line 2748614
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2748615
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 9

    .prologue
    .line 2748616
    check-cast p1, LX/JuB;

    .line 2748617
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2748618
    check-cast v0, LX/JuG;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 2748619
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2748620
    check-cast v0, LX/JuG;

    .line 2748621
    iget-wide v4, p1, LX/JuB;->b:J

    move-wide v2, v4

    .line 2748622
    iget-object v1, p1, LX/JuB;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2748623
    iget-object v4, v0, LX/JuG;->a:LX/11R;

    sget-object v5, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v2

    invoke-virtual {v4, v5, v6, v7}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    .line 2748624
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f083bfa

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v4, 0x1

    aput-object v1, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2748625
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2748626
    iget-object v6, v0, LX/JuG;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2748627
    return-void
.end method
