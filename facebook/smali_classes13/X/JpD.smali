.class public LX/JpD;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field public final a:Landroid/graphics/Rect;

.field private b:[Landroid/graphics/drawable/Drawable;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2737063
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2737064
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/JpD;->a:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 2737096
    invoke-virtual {p0}, LX/JpD;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 2737097
    iget v0, v3, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    .line 2737098
    iget-object v4, p0, LX/JpD;->b:[Landroid/graphics/drawable/Drawable;

    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 2737099
    if-eqz v6, :cond_2

    .line 2737100
    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v7

    .line 2737101
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v8

    .line 2737102
    sub-int v8, v7, v0

    .line 2737103
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    .line 2737104
    if-le v8, v9, :cond_0

    .line 2737105
    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v0, v8

    .line 2737106
    :cond_0
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v6, v1, v0, v8, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2737107
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2737108
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget v6, p0, LX/JpD;->c:I

    add-int/2addr v0, v6

    add-int/2addr v0, v1

    .line 2737109
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2737110
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final getIntrinsicHeight()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2737091
    iget-object v2, p0, LX/JpD;->b:[Landroid/graphics/drawable/Drawable;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2737092
    if-eqz v4, :cond_0

    .line 2737093
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2737094
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2737095
    :cond_1
    iget-object v1, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iget-object v1, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2737084
    iget-object v2, p0, LX/JpD;->b:[Landroid/graphics/drawable/Drawable;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2737085
    if-eqz v4, :cond_0

    .line 2737086
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget v5, p0, LX/JpD;->c:I

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    .line 2737087
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2737088
    :cond_1
    if-lez v0, :cond_2

    .line 2737089
    iget v1, p0, LX/JpD;->c:I

    sub-int/2addr v0, v1

    .line 2737090
    :cond_2
    iget-object v1, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, LX/JpD;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getOpacity()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2737079
    iget-object v2, p0, LX/JpD;->b:[Landroid/graphics/drawable/Drawable;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2737080
    if-eqz v4, :cond_0

    .line 2737081
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v4

    invoke-static {v0, v4}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    move-result v0

    .line 2737082
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2737083
    :cond_1
    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2737111
    invoke-virtual {p0}, LX/JpD;->invalidateSelf()V

    .line 2737112
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 2737077
    invoke-virtual {p0, p2, p3, p4}, LX/JpD;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 2737078
    return-void
.end method

.method public final setAlpha(I)V
    .locals 4

    .prologue
    .line 2737072
    iget-object v1, p0, LX/JpD;->b:[Landroid/graphics/drawable/Drawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2737073
    if-eqz v3, :cond_0

    .line 2737074
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2737075
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2737076
    :cond_1
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 4

    .prologue
    .line 2737067
    iget-object v1, p0, LX/JpD;->b:[Landroid/graphics/drawable/Drawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2737068
    if-eqz v3, :cond_0

    .line 2737069
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2737070
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2737071
    :cond_1
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2737065
    invoke-virtual {p0, p2}, LX/JpD;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 2737066
    return-void
.end method
