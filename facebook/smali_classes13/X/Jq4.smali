.class public final LX/Jq4;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Jpb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V
    .locals 0

    .prologue
    .line 2738449
    iput-object p1, p0, LX/Jq4;->a:Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2738450
    iget-object v0, p0, LX/Jq4;->a:Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->e(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V

    .line 2738451
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2738452
    check-cast p1, LX/Jpb;

    const/4 v3, 0x5

    .line 2738453
    iget-object v1, p0, LX/Jq4;->a:Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;

    iget-object v0, p1, LX/Jpb;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    iget-object v0, p1, LX/Jpb;->b:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 2738454
    :goto_0
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2738455
    invoke-static {v1}, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->e(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V

    .line 2738456
    :goto_1
    return-void

    .line 2738457
    :cond_0
    iget-object v0, p1, LX/Jpb;->b:LX/0Px;

    goto :goto_0

    .line 2738458
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2738459
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2738460
    const-string p0, "picker_mode_param"

    sget-object p1, LX/Jpg;->MATCHED:LX/Jpg;

    invoke-virtual {p1}, LX/Jpg;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2738461
    const-string p0, "matched_contacts_param"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2738462
    sget-object v2, LX/Jpu;->a:Landroid/content/Intent;

    .line 2738463
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2738464
    invoke-virtual {v1, v2}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method
