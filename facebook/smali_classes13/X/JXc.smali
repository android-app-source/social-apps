.class public final LX/JXc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V
    .locals 0

    .prologue
    .line 2704793
    iput-object p1, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 2704783
    iget-object v0, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v0, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    if-eqz v0, :cond_0

    .line 2704784
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2704785
    iget-object v1, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    invoke-static {v1, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;F)V

    .line 2704786
    iget-object v1, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    const/4 v2, 0x1

    iget-object v3, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v3, v3, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a$redex0(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;ZFLandroid/view/View;)V

    .line 2704787
    iget-object v1, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    const/4 v2, 0x0

    iget-object v3, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v3, v3, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2704788
    iget-object p0, v3, LX/FAe;->a:Landroid/view/View;

    move-object v3, p0

    .line 2704789
    invoke-static {v1, v2, v0, v3}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a$redex0(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;ZFLandroid/view/View;)V

    .line 2704790
    :cond_0
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 2704794
    iget-object v0, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    iget-object v0, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    if-eqz v0, :cond_0

    .line 2704795
    iget-object v0, p0, LX/JXc;->a:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    const/high16 p1, 0x3f800000    # 1.0f

    const/4 p0, 0x0

    .line 2704796
    iget-object v1, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->k:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 2704797
    iput p0, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    .line 2704798
    iget-object v1, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setAlpha(F)V

    .line 2704799
    iget-object v1, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setX(F)V

    .line 2704800
    iget-object v1, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2704801
    iget-object p0, v1, LX/FAe;->a:Landroid/view/View;

    move-object v1, p0

    .line 2704802
    invoke-virtual {v1, p1}, Landroid/view/View;->setAlpha(F)V

    .line 2704803
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->x:Z

    .line 2704804
    :cond_0
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 2704792
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 0

    .prologue
    .line 2704791
    return-void
.end method
