.class public LX/JlI;
.super LX/1OM;
.source ""

# interfaces
.implements LX/Dct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/JlJ;",
        ">;",
        "LX/Dct",
        "<",
        "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/view/View$OnClickListener;

.field public c:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

.field public d:LX/JlK;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2730970
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2730971
    new-instance v0, LX/JlH;

    invoke-direct {v0, p0}, LX/JlH;-><init>(LX/JlI;)V

    iput-object v0, p0, LX/JlI;->b:Landroid/view/View$OnClickListener;

    .line 2730972
    iput-object p1, p0, LX/JlI;->a:Landroid/view/LayoutInflater;

    .line 2730973
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2730974
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2730969
    iget-object v0, p0, LX/JlI;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2730966
    iget-object v0, p0, LX/JlI;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0308e9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2730967
    iget-object v1, p0, LX/JlI;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2730968
    new-instance v1, LX/JlJ;

    invoke-direct {v1, v0}, LX/JlJ;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2730961
    check-cast p1, LX/JlJ;

    .line 2730962
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;

    .line 2730963
    iget-object v1, p0, LX/JlI;->e:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    .line 2730964
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->a(Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;)V

    .line 2730965
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2730960
    iget-object v0, p0, LX/JlI;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
