.class public final LX/JjW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:J

.field public d:J

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2727838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727839
    const-string v0, "messaging"

    iput-object v0, p0, LX/JjW;->l:Ljava/lang/String;

    .line 2727840
    const-string v0, "unknown"

    iput-object v0, p0, LX/JjW;->m:Ljava/lang/String;

    .line 2727841
    const-string v0, "messaging"

    iput-object v0, p0, LX/JjW;->n:Ljava/lang/String;

    .line 2727842
    const-string v0, "unknown"

    iput-object v0, p0, LX/JjW;->o:Ljava/lang/String;

    .line 2727843
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderParams;)V
    .locals 2

    .prologue
    .line 2727844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727845
    const-string v0, "messaging"

    iput-object v0, p0, LX/JjW;->l:Ljava/lang/String;

    .line 2727846
    const-string v0, "unknown"

    iput-object v0, p0, LX/JjW;->m:Ljava/lang/String;

    .line 2727847
    const-string v0, "messaging"

    iput-object v0, p0, LX/JjW;->n:Ljava/lang/String;

    .line 2727848
    const-string v0, "unknown"

    iput-object v0, p0, LX/JjW;->o:Ljava/lang/String;

    .line 2727849
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iput-object v0, p0, LX/JjW;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727850
    iget v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    iput v0, p0, LX/JjW;->b:I

    .line 2727851
    iget-wide v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    iput-wide v0, p0, LX/JjW;->c:J

    .line 2727852
    iget-wide v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->d:J

    iput-wide v0, p0, LX/JjW;->d:J

    .line 2727853
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->e:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->e:Ljava/lang/String;

    .line 2727854
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->f:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->f:Ljava/lang/String;

    .line 2727855
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->g:Ljava/lang/String;

    .line 2727856
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->h:Ljava/lang/String;

    .line 2727857
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->i:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->i:Ljava/lang/String;

    .line 2727858
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->j:Ljava/lang/String;

    .line 2727859
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->k:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->k:Ljava/lang/String;

    .line 2727860
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->l:Ljava/lang/String;

    .line 2727861
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->m:Ljava/lang/String;

    .line 2727862
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->n:Ljava/lang/String;

    .line 2727863
    iget-object v0, p1, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    iput-object v0, p0, LX/JjW;->o:Ljava/lang/String;

    .line 2727864
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/JjW;
    .locals 0

    .prologue
    .line 2727865
    iput-object p1, p0, LX/JjW;->l:Ljava/lang/String;

    .line 2727866
    iput-object p2, p0, LX/JjW;->m:Ljava/lang/String;

    .line 2727867
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/events/banner/EventReminderParams;
    .locals 2

    .prologue
    .line 2727868
    new-instance v0, Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/events/banner/EventReminderParams;-><init>(LX/JjW;)V

    return-object v0
.end method
