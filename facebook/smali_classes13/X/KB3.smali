.class public final LX/KB3;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/JtC;

.field private b:Z

.field private c:LX/KB2;


# direct methods
.method public constructor <init>(LX/JtC;Landroid/os/Looper;)V
    .locals 3

    iput-object p1, p0, LX/KB3;->a:LX/JtC;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, LX/KB2;

    iget-object v1, p0, LX/KB3;->a:LX/JtC;

    invoke-direct {v0, v1}, LX/KB2;-><init>(LX/JtC;)V

    iput-object v0, p0, LX/KB3;->c:LX/KB2;

    return-void
.end method


# virtual methods
.method public final dispatchMessage(Landroid/os/Message;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UntrackedBindService"
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, LX/KB3;->b:Z

    if-nez v0, :cond_0

    const-string v0, "bindService: "

    iget-object v1, p0, LX/KB3;->a:LX/JtC;

    invoke-static {v1}, LX/JtC;->a$redex0(LX/JtC;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    iget-object v0, p0, LX/KB3;->a:LX/JtC;

    iget-object v1, p0, LX/KB3;->a:LX/JtC;

    iget-object v1, v1, LX/JtC;->c:Landroid/content/Intent;

    iget-object v2, p0, LX/KB3;->c:LX/KB2;

    const v3, -0x42bf8624

    invoke-static {v0, v1, v2, v5, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    iput-boolean v5, p0, LX/KB3;->b:Z

    :cond_0
    :try_start_0
    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v4}, LX/KB3;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "unbindService: "

    iget-object v1, p0, LX/KB3;->a:LX/JtC;

    invoke-static {v1}, LX/JtC;->a$redex0(LX/JtC;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    :try_start_1
    iget-object v0, p0, LX/KB3;->a:LX/JtC;

    iget-object v1, p0, LX/KB3;->c:LX/KB2;

    const v2, -0x11182963

    invoke-static {v0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    iput-boolean v4, p0, LX/KB3;->b:Z

    :cond_1
    return-void

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "WearableLS"

    const-string v2, "Exception when unbinding from local service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catchall_0
    move-exception v1

    invoke-virtual {p0, v4}, LX/KB3;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "unbindService: "

    iget-object v2, p0, LX/KB3;->a:LX/JtC;

    invoke-static {v2}, LX/JtC;->a$redex0(LX/JtC;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    :goto_3
    :try_start_2
    iget-object v0, p0, LX/KB3;->a:LX/JtC;

    iget-object v2, p0, LX/KB3;->c:LX/KB2;

    const v3, -0x2552e34a

    invoke-static {v0, v2, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_4
    iput-boolean v4, p0, LX/KB3;->b:Z

    :cond_4
    throw v1

    :cond_5
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v2, "WearableLS"

    const-string v3, "Exception when unbinding from local service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4
.end method
