.class public LX/Jcc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;I)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2718108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2718109
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 2718110
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    const-string v3, "pm list packages"

    invoke-virtual {v1, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 2718111
    :try_start_1
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2718112
    :try_start_2
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 2718113
    :goto_0
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2718114
    const/16 v6, 0x3a

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 2718115
    invoke-virtual {v5, v2, p1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 2718116
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2718117
    :catchall_0
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    :goto_1
    if-eqz v3, :cond_0

    .line 2718118
    invoke-virtual {v3}, Ljava/lang/Process;->waitFor()I

    .line 2718119
    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    .line 2718120
    :cond_0
    if-eqz v2, :cond_1

    .line 2718121
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 2718122
    :cond_1
    if-eqz v1, :cond_2

    .line 2718123
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    :cond_2
    throw v0

    .line 2718124
    :cond_3
    if-eqz v4, :cond_4

    .line 2718125
    invoke-virtual {v4}, Ljava/lang/Process;->waitFor()I

    .line 2718126
    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    .line 2718127
    :cond_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 2718128
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    .line 2718129
    return-object v0

    .line 2718130
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v3, v4

    goto :goto_1
.end method
