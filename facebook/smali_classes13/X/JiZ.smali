.class public final enum LX/JiZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JiZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JiZ;

.field public static final enum ADD_CONTACTS_ROW:LX/JiZ;

.field public static final enum CHAT_AVAILABILITY_TOGGLE_ROW:LX/JiZ;

.field public static final enum CONTACT_ROW:LX/JiZ;

.field public static final enum CONTACT_SYNC_PERMANENT_ROW:LX/JiZ;

.field public static final enum CONTACT_UPLOAD_ROW:LX/JiZ;

.field public static final enum CYMK_ROW:LX/JiZ;

.field public static final enum FAVORITES_HEADER:LX/JiZ;

.field public static final enum H_SCROLL_ROW:LX/JiZ;

.field public static final enum IMAGE_CODE_ROW:LX/JiZ;

.field public static final enum INVITE_FRIENDS_UPSELL_ROW:LX/JiZ;

.field public static final enum INVITE_PERMANENT_ROW:LX/JiZ;

.field public static final enum MESSAGE_REQUESTS_ROW:LX/JiZ;

.field public static final enum MESSENGER_CONTACTS_ROW:LX/JiZ;

.field public static final enum NEW_GROUPS_ROW:LX/JiZ;

.field public static final enum SECTION_HEADER:LX/JiZ;

.field public static final enum SECTION_SPLITTER:LX/JiZ;

.field public static final enum SMS_BRIDGE_PERMANENT_ROW:LX/JiZ;

.field public static final enum SMS_TAKEOVER_PERMANENT_ROW:LX/JiZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2726033
    new-instance v0, LX/JiZ;

    const-string v1, "CONTACT_ROW"

    invoke-direct {v0, v1, v3}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->CONTACT_ROW:LX/JiZ;

    .line 2726034
    new-instance v0, LX/JiZ;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v4}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->SECTION_HEADER:LX/JiZ;

    .line 2726035
    new-instance v0, LX/JiZ;

    const-string v1, "SECTION_SPLITTER"

    invoke-direct {v0, v1, v5}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->SECTION_SPLITTER:LX/JiZ;

    .line 2726036
    new-instance v0, LX/JiZ;

    const-string v1, "FAVORITES_HEADER"

    invoke-direct {v0, v1, v6}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->FAVORITES_HEADER:LX/JiZ;

    .line 2726037
    new-instance v0, LX/JiZ;

    const-string v1, "CONTACT_UPLOAD_ROW"

    invoke-direct {v0, v1, v7}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->CONTACT_UPLOAD_ROW:LX/JiZ;

    .line 2726038
    new-instance v0, LX/JiZ;

    const-string v1, "CONTACT_SYNC_PERMANENT_ROW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->CONTACT_SYNC_PERMANENT_ROW:LX/JiZ;

    .line 2726039
    new-instance v0, LX/JiZ;

    const-string v1, "CHAT_AVAILABILITY_TOGGLE_ROW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->CHAT_AVAILABILITY_TOGGLE_ROW:LX/JiZ;

    .line 2726040
    new-instance v0, LX/JiZ;

    const-string v1, "INVITE_PERMANENT_ROW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->INVITE_PERMANENT_ROW:LX/JiZ;

    .line 2726041
    new-instance v0, LX/JiZ;

    const-string v1, "INVITE_FRIENDS_UPSELL_ROW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->INVITE_FRIENDS_UPSELL_ROW:LX/JiZ;

    .line 2726042
    new-instance v0, LX/JiZ;

    const-string v1, "NEW_GROUPS_ROW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->NEW_GROUPS_ROW:LX/JiZ;

    .line 2726043
    new-instance v0, LX/JiZ;

    const-string v1, "SMS_TAKEOVER_PERMANENT_ROW"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->SMS_TAKEOVER_PERMANENT_ROW:LX/JiZ;

    .line 2726044
    new-instance v0, LX/JiZ;

    const-string v1, "H_SCROLL_ROW"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->H_SCROLL_ROW:LX/JiZ;

    .line 2726045
    new-instance v0, LX/JiZ;

    const-string v1, "IMAGE_CODE_ROW"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->IMAGE_CODE_ROW:LX/JiZ;

    .line 2726046
    new-instance v0, LX/JiZ;

    const-string v1, "CYMK_ROW"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->CYMK_ROW:LX/JiZ;

    .line 2726047
    new-instance v0, LX/JiZ;

    const-string v1, "MESSAGE_REQUESTS_ROW"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->MESSAGE_REQUESTS_ROW:LX/JiZ;

    .line 2726048
    new-instance v0, LX/JiZ;

    const-string v1, "MESSENGER_CONTACTS_ROW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->MESSENGER_CONTACTS_ROW:LX/JiZ;

    .line 2726049
    new-instance v0, LX/JiZ;

    const-string v1, "SMS_BRIDGE_PERMANENT_ROW"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->SMS_BRIDGE_PERMANENT_ROW:LX/JiZ;

    .line 2726050
    new-instance v0, LX/JiZ;

    const-string v1, "ADD_CONTACTS_ROW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/JiZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JiZ;->ADD_CONTACTS_ROW:LX/JiZ;

    .line 2726051
    const/16 v0, 0x12

    new-array v0, v0, [LX/JiZ;

    sget-object v1, LX/JiZ;->CONTACT_ROW:LX/JiZ;

    aput-object v1, v0, v3

    sget-object v1, LX/JiZ;->SECTION_HEADER:LX/JiZ;

    aput-object v1, v0, v4

    sget-object v1, LX/JiZ;->SECTION_SPLITTER:LX/JiZ;

    aput-object v1, v0, v5

    sget-object v1, LX/JiZ;->FAVORITES_HEADER:LX/JiZ;

    aput-object v1, v0, v6

    sget-object v1, LX/JiZ;->CONTACT_UPLOAD_ROW:LX/JiZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/JiZ;->CONTACT_SYNC_PERMANENT_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/JiZ;->CHAT_AVAILABILITY_TOGGLE_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/JiZ;->INVITE_PERMANENT_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/JiZ;->INVITE_FRIENDS_UPSELL_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/JiZ;->NEW_GROUPS_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/JiZ;->SMS_TAKEOVER_PERMANENT_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/JiZ;->H_SCROLL_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/JiZ;->IMAGE_CODE_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/JiZ;->CYMK_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/JiZ;->MESSAGE_REQUESTS_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/JiZ;->MESSENGER_CONTACTS_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/JiZ;->SMS_BRIDGE_PERMANENT_ROW:LX/JiZ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/JiZ;->ADD_CONTACTS_ROW:LX/JiZ;

    aput-object v2, v0, v1

    sput-object v0, LX/JiZ;->$VALUES:[LX/JiZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2726052
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JiZ;
    .locals 1

    .prologue
    .line 2726053
    const-class v0, LX/JiZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JiZ;

    return-object v0
.end method

.method public static values()[LX/JiZ;
    .locals 1

    .prologue
    .line 2726054
    sget-object v0, LX/JiZ;->$VALUES:[LX/JiZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JiZ;

    return-object v0
.end method
