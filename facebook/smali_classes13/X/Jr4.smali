.class public LX/Jr4;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kU;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public a:LX/2Og;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2N4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/6cs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Jqb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Jrc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/2OQ;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741677
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr4;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741675
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2741676
    return-void
.end method

.method public static a(LX/0QB;)LX/Jr4;
    .locals 13

    .prologue
    .line 2741644
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741645
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741646
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741647
    if-nez v1, :cond_0

    .line 2741648
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741649
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741650
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741651
    sget-object v1, LX/Jr4;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741652
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741653
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741654
    :cond_1
    if-nez v1, :cond_4

    .line 2741655
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741656
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741657
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741658
    new-instance v1, LX/Jr4;

    const/16 v7, 0x35bd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct {v1, v7}, LX/Jr4;-><init>(LX/0Ot;)V

    .line 2741659
    invoke-static {v0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v7

    check-cast v7, LX/2Og;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v8

    check-cast v8, LX/2N4;

    invoke-static {v0}, LX/6cs;->a(LX/0QB;)LX/6cs;

    move-result-object v9

    check-cast v9, LX/6cs;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v10

    check-cast v10, LX/Jqb;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v11

    check-cast v11, LX/Jrc;

    invoke-static {v0}, LX/2Ok;->a(LX/0QB;)LX/2OQ;

    move-result-object v12

    check-cast v12, LX/2OQ;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 2741660
    iput-object v7, v1, LX/Jr4;->a:LX/2Og;

    iput-object v8, v1, LX/Jr4;->b:LX/2N4;

    iput-object v9, v1, LX/Jr4;->c:LX/6cs;

    iput-object v10, v1, LX/Jr4;->d:LX/Jqb;

    iput-object v11, v1, LX/Jr4;->e:LX/Jrc;

    iput-object v12, v1, LX/Jr4;->f:LX/2OQ;

    iput-object p0, v1, LX/Jr4;->g:Ljava/lang/String;

    .line 2741661
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741662
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741663
    if-nez v1, :cond_2

    .line 2741664
    sget-object v0, LX/Jr4;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr4;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741665
    :goto_1
    if-eqz v0, :cond_3

    .line 2741666
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741667
    :goto_3
    check-cast v0, LX/Jr4;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741668
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741669
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741670
    :catchall_1
    move-exception v0

    .line 2741671
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741672
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741673
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741674
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr4;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr4;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741642
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741643
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741587
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kU;

    invoke-virtual {v0}, LX/6kU;->e()LX/6jz;

    move-result-object v0

    .line 2741588
    iget-object v1, p0, LX/Jr4;->b:LX/2N4;

    iget-object v2, v0, LX/6jz;->messageId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2741589
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2741590
    :goto_0
    return-object v0

    .line 2741591
    :cond_0
    iget-object v1, v0, LX/6jz;->userId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 2741592
    iget-object v2, v0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2741593
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got unexpected action while processing DeltaMessageReaction: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2741594
    :pswitch_0
    iget-object v2, p0, LX/Jr4;->c:LX/6cs;

    iget-object v3, v0, LX/6jz;->messageId:Ljava/lang/String;

    iget-object v4, v0, LX/6jz;->reaction:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, v4}, LX/6cs;->a(Ljava/lang/String;Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2741595
    :goto_1
    iget-object v1, p0, LX/Jr4;->c:LX/6cs;

    iget-object v0, v0, LX/6jz;->messageId:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/6cs;->a(Ljava/lang/String;)LX/0vV;

    move-result-object v0

    .line 2741596
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2741597
    if-eqz v0, :cond_2

    .line 2741598
    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v2

    .line 2741599
    const/4 v1, 0x0

    .line 2741600
    const-string v4, "reactions_keys"

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v4, p0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2741601
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2741602
    new-instance p0, Ljava/util/ArrayList;

    invoke-interface {v0, v1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2741603
    invoke-virtual {v3, v1, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2741604
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v2, v1

    .line 2741605
    goto :goto_2

    .line 2741606
    :cond_1
    const-string v1, "max_num_values_per_key"

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2741607
    :cond_2
    move-object v0, v3

    .line 2741608
    goto :goto_0

    .line 2741609
    :pswitch_1
    iget-object v2, p0, LX/Jr4;->c:LX/6cs;

    iget-object v3, v0, LX/6jz;->messageId:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/6cs;->a(Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741613
    const/4 v0, 0x0

    .line 2741614
    const-string v1, "reactions_keys"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2741615
    const-string v0, "reactions_keys"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2741616
    const-string v0, "max_num_values_per_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2741617
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2741618
    new-instance v2, LX/0vV;

    invoke-direct {v2, v1, v0}, LX/0vV;-><init>(II)V

    move-object v1, v2

    .line 2741619
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2741620
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2741621
    invoke-interface {v1, v0, v5}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    .line 2741622
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 2741623
    :cond_1
    move-object v1, v0

    .line 2741624
    if-nez v1, :cond_2

    .line 2741625
    :goto_1
    return-void

    .line 2741626
    :cond_2
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kU;

    invoke-virtual {v0}, LX/6kU;->e()LX/6jz;

    move-result-object v0

    .line 2741627
    iget-object v2, p0, LX/Jr4;->g:Ljava/lang/String;

    iget-object v3, v0, LX/6jz;->userId:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2741628
    iget-object v2, p0, LX/Jr4;->a:LX/2Og;

    iget-object v3, v0, LX/6jz;->reaction:Ljava/lang/String;

    iget-object v4, v0, LX/6jz;->messageId:Ljava/lang/String;

    .line 2741629
    new-instance v5, LX/DdC;

    invoke-direct {v5, v3, v4}, LX/DdC;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741630
    iget-object p1, v2, LX/2Og;->k:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->peekLast()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v5, p1}, LX/DdC;->equals(Ljava/lang/Object;)Z

    move-result v5

    move v2, v5

    .line 2741631
    if-eqz v2, :cond_4

    .line 2741632
    iget-object v2, p0, LX/Jr4;->a:LX/2Og;

    .line 2741633
    iget-object v3, v2, LX/2Og;->k:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 2741634
    :cond_3
    :goto_2
    iget-object v2, p0, LX/Jr4;->e:LX/Jrc;

    iget-object v3, v0, LX/6jz;->threadKey:LX/6l9;

    invoke-virtual {v2, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2741635
    iget-object v3, p0, LX/Jr4;->f:LX/2OQ;

    iget-object v0, v0, LX/6jz;->messageId:Ljava/lang/String;

    invoke-virtual {v3, v2, v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/0Xu;)V

    .line 2741636
    iget-object v0, p0, LX/Jr4;->d:LX/Jqb;

    .line 2741637
    invoke-static {v0, v2}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741638
    goto :goto_1

    .line 2741639
    :cond_4
    iget-object v2, p0, LX/Jr4;->a:LX/2Og;

    .line 2741640
    iget-object v3, v2, LX/2Og;->k:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->clear()V

    .line 2741641
    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2741610
    check-cast p1, LX/6kU;

    .line 2741611
    invoke-virtual {p1}, LX/6kU;->e()LX/6jz;

    move-result-object v0

    .line 2741612
    iget-object v1, p0, LX/Jr4;->e:LX/Jrc;

    iget-object v0, v0, LX/6jz;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
