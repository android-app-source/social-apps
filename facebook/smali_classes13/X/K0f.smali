.class public final LX/K0f;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/5pG;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private b:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;[LX/5pG;)V
    .locals 1

    .prologue
    .line 2759192
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 2759193
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LX/K0f;->a:Landroid/view/LayoutInflater;

    .line 2759194
    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 4

    .prologue
    .line 2759195
    invoke-virtual {p0, p1}, LX/K0f;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pG;

    .line 2759196
    if-nez p2, :cond_3

    .line 2759197
    if-eqz p4, :cond_1

    const v1, 0x1090009

    .line 2759198
    :goto_0
    iget-object v2, p0, LX/K0f;->a:Landroid/view/LayoutInflater;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_1
    move-object v1, v2

    .line 2759199
    check-cast v1, Landroid/widget/TextView;

    .line 2759200
    const-string v3, "label"

    invoke-interface {v0, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2759201
    if-nez p4, :cond_2

    iget-object v3, p0, LX/K0f;->b:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 2759202
    iget-object v0, p0, LX/K0f;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2759203
    :cond_0
    :goto_2
    return-object v2

    .line 2759204
    :cond_1
    const v1, 0x1090008

    goto :goto_0

    .line 2759205
    :cond_2
    const-string v3, "color"

    invoke-interface {v0, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "color"

    invoke-interface {v0, v3}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2759206
    const-string v3, "color"

    invoke-interface {v0, v3}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    :cond_3
    move-object v2, p2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2759207
    iput-object p1, p0, LX/K0f;->b:Ljava/lang/Integer;

    .line 2759208
    const v0, 0x63b6efef

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2759209
    return-void
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2759210
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/K0f;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2759211
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/K0f;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
