.class public final LX/Jzw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:D

.field public final c:Z

.field public final d:F


# direct methods
.method private constructor <init>(JDZF)V
    .locals 1

    .prologue
    .line 2757027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2757028
    iput-wide p1, p0, LX/Jzw;->a:J

    .line 2757029
    iput-wide p3, p0, LX/Jzw;->b:D

    .line 2757030
    iput-boolean p5, p0, LX/Jzw;->c:Z

    .line 2757031
    iput p6, p0, LX/Jzw;->d:F

    .line 2757032
    return-void
.end method

.method public static b(LX/5pG;)LX/Jzw;
    .locals 8

    .prologue
    .line 2757033
    const-string v0, "timeout"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "timeout"

    invoke-interface {p0, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v2, v0

    .line 2757034
    :goto_0
    const-string v0, "maximumAge"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "maximumAge"

    invoke-interface {p0, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 2757035
    :goto_1
    const-string v0, "enableHighAccuracy"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "enableHighAccuracy"

    invoke-interface {p0, v0}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v6, 0x1

    .line 2757036
    :goto_2
    const-string v0, "distanceFilter"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "distanceFilter"

    invoke-interface {p0, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-float v7, v0

    .line 2757037
    :goto_3
    new-instance v1, LX/Jzw;

    invoke-direct/range {v1 .. v7}, LX/Jzw;-><init>(JDZF)V

    return-object v1

    .line 2757038
    :cond_0
    const-wide v2, 0x7fffffffffffffffL

    goto :goto_0

    .line 2757039
    :cond_1
    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    goto :goto_1

    .line 2757040
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 2757041
    :cond_3
    const/high16 v7, 0x42c80000    # 100.0f

    goto :goto_3
.end method
