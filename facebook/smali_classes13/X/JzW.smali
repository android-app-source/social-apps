.class public LX/JzW;
.super LX/Jyw;
.source ""


# instance fields
.field private final e:LX/JzS;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 5

    .prologue
    .line 2756288
    invoke-direct {p0}, LX/Jyw;-><init>()V

    .line 2756289
    const-string v0, "style"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 2756290
    invoke-interface {v0}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v1

    .line 2756291
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, LX/JzW;->f:Ljava/util/Map;

    .line 2756292
    :goto_0
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2756293
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v2

    .line 2756294
    invoke-interface {v0, v2}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2756295
    iget-object v4, p0, LX/JzW;->f:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2756296
    :cond_0
    iput-object p2, p0, LX/JzW;->e:LX/JzS;

    .line 2756297
    return-void
.end method


# virtual methods
.method public final a(LX/5pI;)V
    .locals 6

    .prologue
    .line 2756298
    iget-object v0, p0, LX/JzW;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2756299
    iget-object v3, p0, LX/JzW;->e:LX/JzS;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v1

    .line 2756300
    if-nez v1, :cond_0

    .line 2756301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Mapped style node does not exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756302
    :cond_0
    instance-of v3, v1, LX/Jza;

    if-eqz v3, :cond_1

    move-object v0, v1

    .line 2756303
    check-cast v0, LX/Jza;

    invoke-virtual {v0, p1}, LX/Jza;->a(LX/5pI;)V

    goto :goto_0

    .line 2756304
    :cond_1
    instance-of v3, v1, LX/Jyx;

    if-eqz v3, :cond_2

    .line 2756305
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, LX/Jyx;

    invoke-virtual {v1}, LX/Jyx;->b()D

    move-result-wide v4

    invoke-virtual {p1, v0, v4, v5}, LX/5pI;->putDouble(Ljava/lang/String;D)V

    goto :goto_0

    .line 2756306
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported type of node used in property node "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756307
    :cond_3
    return-void
.end method
