.class public LX/Jlx;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/Jlv;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0kd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/0sg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JlZ;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JlV;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/Jm3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private l:LX/0t2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:LX/2Ow;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fX;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/InboxLoaderExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fX;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/Jm5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/Jm6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/MessengerInbox2Service;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2732203
    const-class v0, LX/Jlx;

    sput-object v0, LX/Jlx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/MessengerInbox2Service;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2732191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2732192
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->e()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/Jlx;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 2732193
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732194
    iput-object v0, p0, LX/Jlx;->i:LX/0Ot;

    .line 2732195
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732196
    iput-object v0, p0, LX/Jlx;->j:LX/0Ot;

    .line 2732197
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732198
    iput-object v0, p0, LX/Jlx;->n:LX/0Ot;

    .line 2732199
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2732200
    iput-object v0, p0, LX/Jlx;->o:LX/0Ot;

    .line 2732201
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Jlx;->r:Ljava/lang/String;

    .line 2732202
    return-void
.end method

.method private a(LX/0ta;LX/JlY;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;)LX/Jlv;
    .locals 9

    .prologue
    .line 2732174
    const-string v0, "convertInboxV2QueryModel"

    const v1, -0x75a4d2e8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2732175
    :try_start_0
    iget-object v0, p0, LX/Jlx;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlV;

    .line 2732176
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2732177
    invoke-virtual {p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2732178
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732179
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->l()I

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, v0, LX/JlV;->t:LX/Jre;

    const/4 v7, 0x0

    .line 2732180
    iget-object v8, v6, LX/Jre;->b:LX/0Uh;

    const/16 p3, 0x55a

    invoke-virtual {v8, p3, v7}, LX/0Uh;->a(IZ)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, v6, LX/Jre;->a:LX/0ad;

    sget-short p3, LX/Jrd;->a:S

    invoke-interface {v8, p3, v7}, LX/0ad;->a(SZ)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    const/4 v7, 0x1

    :cond_1
    move v6, v7

    .line 2732181
    if-nez v6, :cond_3

    .line 2732182
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 2732183
    :cond_2
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2732184
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 2732185
    invoke-static {v0, v1}, LX/JlV;->a(LX/JlV;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/JlS;

    move-result-object v1

    .line 2732186
    if-eqz v1, :cond_2

    .line 2732187
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2732188
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v6, v1

    .line 2732189
    new-instance v0, LX/Jlv;

    new-instance v1, LX/JlX;

    iget-object v2, p0, LX/Jlx;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, LX/JlX;-><init>(LX/0ta;LX/JlY;JLX/0Px;)V

    invoke-direct {v0, p0, v1}, LX/Jlv;-><init>(LX/Jlx;LX/JlX;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2732190
    const v1, 0x6bcf2d10

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x3bca729d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/Jlx;LX/0ta;LX/JlY;)LX/Jlv;
    .locals 2

    .prologue
    .line 2732171
    iget-object v0, p0, LX/Jlx;->r:Ljava/lang/String;

    const-string v1, "MESSENGER_INBOX2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2732172
    invoke-direct {p0, p2}, LX/Jlx;->a(LX/JlY;)Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;

    move-result-object v0

    .line 2732173
    invoke-direct {p0, p1, p2, v0}, LX/Jlx;->a(LX/0ta;LX/JlY;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;)LX/Jlv;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Jlx;Ljava/util/Map;Lcom/facebook/graphql/executor/GraphQLResult;)LX/Jlv;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;",
            ">;)",
            "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2732128
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2732129
    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;

    .line 2732130
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2732131
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2732132
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    .line 2732133
    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732134
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->r()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitUpdateStatus;

    move-result-object v7

    .line 2732135
    sget-object v8, LX/Jlt;->c:[I

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitUpdateStatus;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    .line 2732136
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    .line 2732137
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2732138
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2732139
    :pswitch_0
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    .line 2732140
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2732141
    :cond_0
    iget-object v1, p0, LX/Jlx;->p:LX/Jm5;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2732142
    const v2, 0x64a32b51

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2732143
    :try_start_0
    iget-object v2, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v2, v4}, LX/Jm3;->a(Ljava/util/Set;)LX/0P1;

    move-result-object v2

    .line 2732144
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0, v3, v2}, LX/Jlx;->a(LX/Jlx;LX/0Px;Ljava/util/Set;LX/0P1;)LX/Jlw;

    move-result-object v4

    .line 2732145
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2732146
    invoke-virtual {v2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2732147
    iget-object v6, p0, LX/Jlx;->k:LX/Jm3;

    .line 2732148
    sget-object v7, LX/Jm9;->a:LX/0U1;

    .line 2732149
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 2732150
    invoke-static {v7, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v7

    .line 2732151
    iget-object v8, v6, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v8}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 2732152
    const-string v9, "units"

    invoke-virtual {v7}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2732153
    goto :goto_2

    .line 2732154
    :catchall_0
    move-exception v0

    const v2, 0x44e04e27

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 2732155
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v2, LX/Jm1;->a:LX/Jm7;

    .line 2732156
    iget-wide v9, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v9

    .line 2732157
    invoke-virtual {v0, v2, v6, v7}, LX/2Iu;->b(LX/0To;J)V

    .line 2732158
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v2, LX/Jm1;->d:LX/Jm7;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/2Iu;->b(LX/0To;Z)V

    .line 2732159
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2732160
    const v0, 0x1bd0203a

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2732161
    iget-object v0, p0, LX/Jlx;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0db;->aO:LX/0Tn;

    iget-boolean v2, v4, LX/Jlw;->b:Z

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2732162
    iget-object v0, p0, LX/Jlx;->m:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->c()V

    .line 2732163
    new-instance v0, LX/Der;

    invoke-direct {v0}, LX/Der;-><init>()V

    new-instance v1, LX/Des;

    invoke-direct {v1}, LX/Des;-><init>()V

    iget-object v2, v4, LX/Jlw;->a:LX/0Px;

    .line 2732164
    iput-object v2, v1, LX/Des;->a:LX/0Px;

    .line 2732165
    move-object v1, v1

    .line 2732166
    invoke-virtual {v1}, LX/Des;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    move-result-object v1

    .line 2732167
    iput-object v1, v0, LX/Der;->a:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    .line 2732168
    move-object v0, v0

    .line 2732169
    invoke-virtual {v0}, LX/Der;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;

    move-result-object v0

    .line 2732170
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    sget-object v2, LX/JlY;->ALL:LX/JlY;

    invoke-direct {p0, v1, v2, v0}, LX/Jlx;->a(LX/0ta;LX/JlY;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;)LX/Jlv;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(LX/Jlx;LX/0Px;Ljava/util/Set;LX/0P1;)LX/Jlw;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/Jm2;",
            ">;)",
            "LX/Jlw;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2732110
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2732111
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    move v3, v5

    move v4, v2

    move v7, v5

    :goto_0
    if-ge v6, v9, :cond_3

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732112
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2732113
    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2732114
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    .line 2732115
    iget-object v1, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v1, v0, v7, v4}, LX/Jm3;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;IZ)V

    .line 2732116
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2732117
    :cond_0
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v0

    .line 2732118
    if-eqz v4, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    if-eq v0, v1, :cond_2

    move v0, v2

    move v1, v4

    .line 2732119
    :goto_2
    add-int/lit8 v4, v7, 0x1

    .line 2732120
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v7, v4

    move v4, v1

    move v3, v0

    goto :goto_0

    .line 2732121
    :cond_1
    invoke-virtual {p3, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2732122
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    .line 2732123
    iget-object v10, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v10, v1, v7, v4}, LX/Jm3;->a(Ljava/lang/String;IZ)V

    .line 2732124
    invoke-virtual {p3, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jm2;

    iget-object v1, v1, LX/Jm2;->a:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2732125
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    if-ne v0, v1, :cond_4

    move v0, v3

    move v1, v5

    .line 2732126
    goto :goto_2

    .line 2732127
    :cond_3
    new-instance v0, LX/Jlw;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, v3}, LX/Jlw;-><init>(LX/0Px;Z)V

    return-object v0

    :cond_4
    move v0, v3

    move v1, v4

    goto :goto_2
.end method

.method private a(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;",
            ">;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2731959
    const-string v0, "makeRequest"

    const v1, 0x39d854d2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2731960
    :try_start_0
    iget-object v0, p0, LX/Jlx;->h:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const-class v1, LX/JlT;

    invoke-static {v0, v1}, LX/52O;->a(Ljava/util/concurrent/Future;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731961
    const v1, 0x3d05f941

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x608e206

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(LX/JlY;)Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;
    .locals 3

    .prologue
    .line 2732096
    const-string v0, "doDbFetch"

    const v1, 0x44c12141

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2732097
    :try_start_0
    sget-object v0, LX/Jlt;->b:[I

    invoke-virtual {p1}, LX/JlY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2732098
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2732099
    :catchall_0
    move-exception v0

    const v1, -0x19966e6c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2732100
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v0}, LX/Jm3;->c()LX/0Px;

    move-result-object v0

    .line 2732101
    :goto_0
    new-instance v1, LX/Der;

    invoke-direct {v1}, LX/Der;-><init>()V

    new-instance v2, LX/Des;

    invoke-direct {v2}, LX/Des;-><init>()V

    .line 2732102
    iput-object v0, v2, LX/Des;->a:LX/0Px;

    .line 2732103
    move-object v0, v2

    .line 2732104
    invoke-virtual {v0}, LX/Des;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    move-result-object v0

    .line 2732105
    iput-object v0, v1, LX/Der;->a:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    .line 2732106
    move-object v0, v1

    .line 2732107
    invoke-virtual {v0}, LX/Der;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2732108
    const v1, 0x6a4e97c9

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2732109
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v0}, LX/Jm3;->a()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2732062
    iget-object v0, p0, LX/Jlx;->r:Ljava/lang/String;

    const-string v2, "MESSENGER_INBOX2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2732063
    :goto_0
    return-void

    .line 2732064
    :cond_0
    const-string v0, "writeFullResultToDatabase"

    const v2, -0x695c19

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2732065
    :try_start_0
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2732066
    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;

    .line 2732067
    iget-object v2, p0, LX/Jlx;->p:LX/Jm5;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 2732068
    const v2, -0x38a76462

    invoke-static {v7, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2732069
    :try_start_1
    iget-object v2, p0, LX/Jlx;->k:LX/Jm3;

    .line 2732070
    iget-object v3, v2, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2732071
    const-string v5, "units"

    const-string v6, ""

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v6, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2732072
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    move v2, v4

    move v3, v1

    move v6, v4

    .line 2732073
    :goto_1
    if-ge v5, v9, :cond_2

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732074
    iget-object v10, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v10, v0, v6, v3}, LX/Jm3;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;IZ)V

    .line 2732075
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v0

    .line 2732076
    if-eqz v3, :cond_1

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    if-eq v0, v10, :cond_1

    move v0, v1

    move v2, v3

    .line 2732077
    :goto_2
    add-int/lit8 v6, v6, 0x1

    .line 2732078
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 2732079
    :cond_1
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    if-ne v0, v10, :cond_3

    move v0, v2

    move v2, v4

    .line 2732080
    goto :goto_2

    .line 2732081
    :cond_2
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->a:LX/Jm7;

    .line 2732082
    iget-wide v11, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v11

    .line 2732083
    invoke-virtual {v0, v1, v4, v5}, LX/2Iu;->b(LX/0To;J)V

    .line 2732084
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->b:LX/Jm7;

    .line 2732085
    iget-wide v11, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v11

    .line 2732086
    invoke-virtual {v0, v1, v4, v5}, LX/2Iu;->b(LX/0To;J)V

    .line 2732087
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->c:LX/Jm7;

    iget-object v3, p0, LX/Jlx;->l:LX/0t2;

    invoke-virtual {p1, v3}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 2732088
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->d:LX/Jm7;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/2Iu;->b(LX/0To;Z)V

    .line 2732089
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2732090
    iget-object v0, p0, LX/Jlx;->m:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->c()V

    .line 2732091
    iget-object v0, p0, LX/Jlx;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0db;->aO:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2732092
    const v0, 0xb53487e

    :try_start_2
    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2732093
    const v0, 0x1b99d378

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 2732094
    :catchall_0
    move-exception v0

    const v1, 0x6489875c

    :try_start_3
    invoke-static {v7, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2732095
    :catchall_1
    move-exception v0

    const v1, 0x9ba19cd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_3
    move v0, v2

    move v2, v3

    goto :goto_2
.end method

.method public static a(LX/Jlx;LX/0kd;LX/0SG;LX/0sg;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0tX;LX/0Ot;LX/0Ot;LX/Jm3;LX/0t2;LX/2Ow;LX/0Ot;LX/0Ot;LX/Jm5;LX/Jm6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jlx;",
            "LX/0kd;",
            "LX/0SG;",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "LX/JlZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JlV;",
            ">;",
            "LX/Jm3;",
            "LX/0t2;",
            "LX/2Ow;",
            "LX/0Ot",
            "<",
            "LX/1fX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1fX;",
            ">;",
            "LX/Jm5;",
            "LX/Jm6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2732061
    iput-object p1, p0, LX/Jlx;->c:LX/0kd;

    iput-object p2, p0, LX/Jlx;->d:LX/0SG;

    iput-object p3, p0, LX/Jlx;->e:LX/0sg;

    iput-object p4, p0, LX/Jlx;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, LX/Jlx;->g:LX/0Uh;

    iput-object p6, p0, LX/Jlx;->h:LX/0tX;

    iput-object p7, p0, LX/Jlx;->i:LX/0Ot;

    iput-object p8, p0, LX/Jlx;->j:LX/0Ot;

    iput-object p9, p0, LX/Jlx;->k:LX/Jm3;

    iput-object p10, p0, LX/Jlx;->l:LX/0t2;

    iput-object p11, p0, LX/Jlx;->m:LX/2Ow;

    iput-object p12, p0, LX/Jlx;->n:LX/0Ot;

    iput-object p13, p0, LX/Jlx;->o:LX/0Ot;

    iput-object p14, p0, LX/Jlx;->p:LX/Jm5;

    iput-object p15, p0, LX/Jlx;->q:LX/Jm6;

    return-void
.end method

.method public static b(LX/Jlx;LX/JlW;)LX/Jlv;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2732010
    const-string v2, "InboxUnitFetcherWithUnitStore.fetchInternal(%s, %s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, LX/JlW;->a:LX/0rS;

    aput-object v4, v3, v1

    iget-object v4, p1, LX/JlW;->b:LX/JlY;

    aput-object v4, v3, v0

    const v4, -0x2eafc6e7

    invoke-static {v2, v3, v4}, LX/02m;->a(Ljava/lang/String;[Ljava/lang/Object;I)V

    .line 2732011
    :try_start_0
    invoke-static {p0}, LX/Jlx;->e(LX/Jlx;)LX/Jlu;

    move-result-object v2

    .line 2732012
    iget-object v3, p1, LX/JlW;->a:LX/0rS;

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v3, v4, :cond_0

    .line 2732013
    invoke-static {p0}, LX/Jlx;->c(LX/Jlx;)LX/Jlv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2732014
    const v1, 0x60baffc5

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732015
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    :goto_0
    return-object v0

    .line 2732016
    :cond_0
    :try_start_1
    iget-object v3, p1, LX/JlW;->a:LX/0rS;

    sget-object v4, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq v3, v4, :cond_1

    iget-object v3, p1, LX/JlW;->a:LX/0rS;

    sget-object v4, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v3, v4, :cond_5

    .line 2732017
    :cond_1
    sget-object v0, LX/Jlu;->UP_TO_DATE:LX/Jlu;

    if-ne v2, v0, :cond_2

    .line 2732018
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v1, p1, LX/JlW;->b:LX/JlY;

    invoke-static {p0, v0, v1}, LX/Jlx;->a(LX/Jlx;LX/0ta;LX/JlY;)LX/Jlv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2732019
    const v1, 0x7a129263

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732020
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 2732021
    :cond_2
    :try_start_2
    sget-object v0, LX/Jlu;->NO_DATA:LX/Jlu;

    if-eq v2, v0, :cond_3

    .line 2732022
    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    iget-object v1, p1, LX/JlW;->b:LX/JlY;

    invoke-static {p0, v0, v1}, LX/Jlx;->a(LX/Jlx;LX/0ta;LX/JlY;)LX/Jlv;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2732023
    const v1, -0x55a797ba

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732024
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 2732025
    :cond_3
    :try_start_3
    iget-object v0, p1, LX/JlW;->a:LX/0rS;

    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq v0, v1, :cond_4

    .line 2732026
    invoke-static {p0}, LX/Jlx;->c(LX/Jlx;)LX/Jlv;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2732027
    const v1, -0x3cdc5920

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732028
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 2732029
    :cond_4
    :try_start_4
    new-instance v0, LX/Jlv;

    new-instance v1, LX/JlX;

    sget-object v2, LX/0ta;->NO_DATA:LX/0ta;

    iget-object v3, p1, LX/JlW;->b:LX/JlY;

    iget-object v4, p0, LX/Jlx;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 2732030
    sget-object v6, LX/0Q7;->a:LX/0Px;

    move-object v6, v6

    .line 2732031
    invoke-direct/range {v1 .. v6}, LX/JlX;-><init>(LX/0ta;LX/JlY;JLX/0Px;)V

    invoke-direct {v0, p0, v1}, LX/Jlv;-><init>(LX/Jlx;LX/JlX;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2732032
    const v1, -0x72e9cc2c

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732033
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 2732034
    :cond_5
    :try_start_5
    iget-object v3, p1, LX/JlW;->b:LX/JlY;

    sget-object v4, LX/JlY;->ALL:LX/JlY;

    if-ne v3, v4, :cond_6

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2732035
    sget-object v0, LX/Jlt;->a:[I

    invoke-virtual {v2}, LX/Jlu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2732036
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2732037
    :catchall_0
    move-exception v0

    const v1, -0x2bd0afb1

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732038
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    throw v0

    :cond_6
    move v0, v1

    .line 2732039
    goto :goto_1

    .line 2732040
    :pswitch_0
    :try_start_6
    invoke-static {p0}, LX/Jlx;->c(LX/Jlx;)LX/Jlv;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    .line 2732041
    const v1, 0x3d87b891    # 0.06627f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732042
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 2732043
    :pswitch_1
    :try_start_7
    iget-object v0, p0, LX/Jlx;->k:LX/Jm3;

    invoke-virtual {v0}, LX/Jm3;->d()Ljava/util/Map;

    move-result-object v1

    .line 2732044
    iget-object v0, p0, LX/Jlx;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlZ;

    invoke-virtual {v0}, LX/JlZ;->b()LX/Dem;

    move-result-object v3

    .line 2732045
    new-instance v4, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2732046
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2732047
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v4, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    goto :goto_2

    .line 2732048
    :cond_7
    const-string v0, "formattedUnitUpdateTimestamps"

    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2732049
    const-string v0, "serviceType"

    iget-object v2, p0, LX/Jlx;->r:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2732050
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v2, 0x1

    .line 2732051
    iput-boolean v2, v0, LX/0zO;->p:Z

    .line 2732052
    move-object v0, v0

    .line 2732053
    move-object v0, v0

    .line 2732054
    iget-object v2, p0, LX/Jlx;->h:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const-class v2, LX/JlT;

    invoke-static {v0, v2}, LX/52O;->a(Ljava/util/concurrent/Future;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2732055
    invoke-static {p0, v1, v0}, LX/Jlx;->a(LX/Jlx;Ljava/util/Map;Lcom/facebook/graphql/executor/GraphQLResult;)LX/Jlv;

    move-result-object v0

    move-object v0, v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2732056
    const v1, -0xada557e    # -2.1000666E32f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732057
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 2732058
    :pswitch_2
    :try_start_8
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    sget-object v1, LX/JlY;->ALL:LX/JlY;

    invoke-static {p0, v0, v1}, LX/Jlx;->a(LX/Jlx;LX/0ta;LX/JlY;)LX/Jlv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v0

    .line 2732059
    const v1, 0x31af30da

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2732060
    sget-object v1, LX/Jlx;->a:Ljava/lang/Class;

    invoke-static {v1}, LX/0PR;->a(Ljava/lang/Class;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static c(LX/Jlx;)LX/Jlv;
    .locals 4

    .prologue
    .line 2732004
    invoke-direct {p0}, LX/Jlx;->d()LX/0zO;

    move-result-object v0

    .line 2732005
    invoke-direct {p0, v0}, LX/Jlx;->a(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 2732006
    invoke-direct {p0, v0, v1}, LX/Jlx;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2732007
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    sget-object v3, LX/JlY;->ALL:LX/JlY;

    .line 2732008
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2732009
    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;

    invoke-direct {p0, v2, v3, v0}, LX/Jlx;->a(LX/0ta;LX/JlY;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;)LX/Jlv;

    move-result-object v0

    return-object v0
.end method

.method private d()LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2731998
    iget-object v0, p0, LX/Jlx;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlZ;

    invoke-virtual {v0}, LX/JlZ;->b()LX/Dem;

    move-result-object v0

    .line 2731999
    const-string v1, "serviceType"

    iget-object v2, p0, LX/Jlx;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2732000
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2732001
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2732002
    move-object v0, v0

    .line 2732003
    return-object v0
.end method

.method private static e(LX/Jlx;)LX/Jlu;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 2731970
    iget-object v0, p0, LX/Jlx;->r:Ljava/lang/String;

    const-string v1, "MESSENGER_INBOX2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2731971
    sget-object v0, LX/Jlu;->STALE_DATA_NEEDS_FULL_SERVER_FETCH:LX/Jlu;

    .line 2731972
    :goto_0
    return-object v0

    .line 2731973
    :cond_0
    invoke-direct {p0}, LX/Jlx;->d()LX/0zO;

    move-result-object v0

    .line 2731974
    iget-object v1, p0, LX/Jlx;->l:LX/0t2;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    .line 2731975
    iget-object v1, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v2, LX/Jm1;->c:LX/Jm7;

    invoke-virtual {v1, v2}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v1

    .line 2731976
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2731977
    sget-object v0, LX/Jlu;->NO_DATA:LX/Jlu;

    goto :goto_0

    .line 2731978
    :cond_1
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->b:LX/Jm7;

    invoke-virtual {v0, v1, v6, v7}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v0

    .line 2731979
    cmp-long v2, v0, v6

    if-nez v2, :cond_2

    .line 2731980
    sget-object v0, LX/Jlu;->NO_DATA:LX/Jlu;

    goto :goto_0

    .line 2731981
    :cond_2
    iget-object v2, p0, LX/Jlx;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2731982
    const-wide/32 v4, 0xf731400

    sub-long v4, v2, v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_3

    .line 2731983
    sget-object v0, LX/Jlu;->STALE_DATA_NEEDS_FULL_SERVER_FETCH:LX/Jlu;

    goto :goto_0

    .line 2731984
    :cond_3
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->d:LX/Jm7;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, LX/2Iu;->a(LX/0To;Z)Z

    move-result v0

    .line 2731985
    if-eqz v0, :cond_4

    .line 2731986
    sget-object v0, LX/Jlu;->STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH:LX/Jlu;

    goto :goto_0

    .line 2731987
    :cond_4
    iget-object v0, p0, LX/Jlx;->q:LX/Jm6;

    sget-object v1, LX/Jm1;->a:LX/Jm7;

    invoke-virtual {v0, v1, v6, v7}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v4

    .line 2731988
    iget-object v0, p0, LX/Jlx;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlZ;

    const/4 v1, 0x1

    .line 2731989
    iget-object v6, v0, LX/JlZ;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v6

    .line 2731990
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    if-ne v6, v1, :cond_8

    :goto_1
    move v0, v1

    .line 2731991
    if-eqz v0, :cond_6

    .line 2731992
    const-wide/32 v0, 0x36ee80

    sub-long v0, v2, v0

    cmp-long v0, v4, v0

    if-gez v0, :cond_5

    .line 2731993
    sget-object v0, LX/Jlu;->STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH:LX/Jlu;

    goto/16 :goto_0

    .line 2731994
    :cond_5
    sget-object v0, LX/Jlu;->UP_TO_DATE:LX/Jlu;

    goto/16 :goto_0

    .line 2731995
    :cond_6
    const-wide/32 v0, 0x1499700

    sub-long v0, v2, v0

    cmp-long v0, v4, v0

    if-gez v0, :cond_7

    .line 2731996
    sget-object v0, LX/Jlu;->STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH:LX/Jlu;

    goto/16 :goto_0

    .line 2731997
    :cond_7
    sget-object v0, LX/Jlu;->UP_TO_DATE:LX/Jlu;

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/JlW;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JlW;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2731962
    sget-object v0, LX/JlY;->ALL:LX/JlY;

    sget-object v1, LX/JlY;->TOP:LX/JlY;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    move-object v0, v0

    .line 2731963
    iget-object v1, p1, LX/JlW;->b:LX/JlY;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2731964
    iget-object v0, p1, LX/JlW;->b:LX/JlY;

    sget-object v1, LX/JlY;->ALL:LX/JlY;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/JlW;->a:LX/0rS;

    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2731965
    iget-object v0, p0, LX/Jlx;->g:LX/0Uh;

    const/16 v1, 0x166

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2731966
    iget-object v0, p0, LX/Jlx;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fX;

    .line 2731967
    :goto_1
    move-object v0, v0

    .line 2731968
    new-instance v1, LX/Jls;

    invoke-direct {v1, p0, p1}, LX/Jls;-><init>(LX/Jlx;LX/JlW;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2731969
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/Jlx;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fX;

    goto :goto_1
.end method
