.class public final enum LX/K8y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K8y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K8y;

.field public static final enum COVER_CARD_READ_STORY_CTA:LX/K8y;

.field public static final enum END_SCREEN_ALREADY_CONNECTED_TITLE:LX/K8y;

.field public static final enum END_SCREEN_ALREADY_SUBSCRIBED_DESCRIPTION:LX/K8y;

.field public static final enum END_SCREEN_CONNECTED_TITLE:LX/K8y;

.field public static final enum END_SCREEN_FOLLOWED_DESCRIPTION:LX/K8y;

.field public static final enum END_SCREEN_FOLLOW_CTA_OFF:LX/K8y;

.field public static final enum END_SCREEN_FOLLOW_CTA_ON:LX/K8y;

.field public static final enum END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_DESCRIPTION:LX/K8y;

.field public static final enum END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_TITLE:LX/K8y;

.field public static final enum END_SCREEN_SUBSCRIBED_DESCRIPTION:LX/K8y;

.field public static final enum END_SCREEN_SUBSCRIBE_CTA_OFF:LX/K8y;

.field public static final enum END_SCREEN_SUBSCRIBE_CTA_ON:LX/K8y;

.field public static final enum END_SCREEN_UNCONNECTED_TITLE:LX/K8y;

.field public static final enum END_SCREEN_UNFOLLOWED_DESCRIPTION:LX/K8y;

.field public static final enum END_SCREEN_UNSUBSCRIBED_DESCRIPTION:LX/K8y;

.field public static final enum FEED_UNIT_CTA:LX/K8y;

.field public static final enum FEED_UNIT_CTA_BLUE_CLUE_DESCRIPTION:LX/K8y;

.field public static final enum FEED_UNIT_CTA_BLUE_CLUE_TITLE:LX/K8y;

.field public static final enum MANAGE_SCREEN_DESCRIPTION:LX/K8y;

.field public static final enum MANAGE_SCREEN_TITLE:LX/K8y;

.field public static final enum MANAGE_SCREEN_TURN_OFF_DIALOG_DESCRIPTION:LX/K8y;

.field public static final enum MANAGE_SCREEN_TURN_OFF_DIALOG_PRIMARY_BUTTON:LX/K8y;

.field public static final enum MANAGE_SCREEN_TURN_OFF_DIALOG_TITLE:LX/K8y;

.field public static final enum MANAGE_SCREEN_TURN_ON_DIALOG_DESCRIPTION:LX/K8y;

.field public static final enum MANAGE_SCREEN_TURN_ON_DIALOG_PRIMARY_BUTTON:LX/K8y;

.field public static final enum MANAGE_SCREEN_TURN_ON_DIALOG_TITLE:LX/K8y;

.field public static final enum PRODUCT_NAME:LX/K8y;

.field public static final enum PRODUCT_NAME_ALL_CAPS:LX/K8y;

.field public static final enum PRODUCT_NAME_LOWER_CASE:LX/K8y;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2776477
    new-instance v0, LX/K8y;

    const-string v1, "PRODUCT_NAME"

    invoke-direct {v0, v1, v3}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->PRODUCT_NAME:LX/K8y;

    .line 2776478
    new-instance v0, LX/K8y;

    const-string v1, "PRODUCT_NAME_LOWER_CASE"

    invoke-direct {v0, v1, v4}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->PRODUCT_NAME_LOWER_CASE:LX/K8y;

    .line 2776479
    new-instance v0, LX/K8y;

    const-string v1, "PRODUCT_NAME_ALL_CAPS"

    invoke-direct {v0, v1, v5}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->PRODUCT_NAME_ALL_CAPS:LX/K8y;

    .line 2776480
    new-instance v0, LX/K8y;

    const-string v1, "FEED_UNIT_CTA"

    invoke-direct {v0, v1, v6}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->FEED_UNIT_CTA:LX/K8y;

    .line 2776481
    new-instance v0, LX/K8y;

    const-string v1, "COVER_CARD_READ_STORY_CTA"

    invoke-direct {v0, v1, v7}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->COVER_CARD_READ_STORY_CTA:LX/K8y;

    .line 2776482
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_UNCONNECTED_TITLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_UNCONNECTED_TITLE:LX/K8y;

    .line 2776483
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_UNFOLLOWED_DESCRIPTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_UNFOLLOWED_DESCRIPTION:LX/K8y;

    .line 2776484
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_FOLLOW_CTA_OFF"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_FOLLOW_CTA_OFF:LX/K8y;

    .line 2776485
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_FOLLOW_CTA_ON"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_FOLLOW_CTA_ON:LX/K8y;

    .line 2776486
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_SUBSCRIBE_CTA_OFF"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_SUBSCRIBE_CTA_OFF:LX/K8y;

    .line 2776487
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_SUBSCRIBE_CTA_ON"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_SUBSCRIBE_CTA_ON:LX/K8y;

    .line 2776488
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_CONNECTED_TITLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_CONNECTED_TITLE:LX/K8y;

    .line 2776489
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_FOLLOWED_DESCRIPTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_FOLLOWED_DESCRIPTION:LX/K8y;

    .line 2776490
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_UNSUBSCRIBED_DESCRIPTION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_UNSUBSCRIBED_DESCRIPTION:LX/K8y;

    .line 2776491
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_SUBSCRIBED_DESCRIPTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_SUBSCRIBED_DESCRIPTION:LX/K8y;

    .line 2776492
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_ALREADY_CONNECTED_TITLE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_ALREADY_CONNECTED_TITLE:LX/K8y;

    .line 2776493
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_ALREADY_SUBSCRIBED_DESCRIPTION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_ALREADY_SUBSCRIBED_DESCRIPTION:LX/K8y;

    .line 2776494
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_TITLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_TITLE:LX/K8y;

    .line 2776495
    new-instance v0, LX/K8y;

    const-string v1, "END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_DESCRIPTION"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_DESCRIPTION:LX/K8y;

    .line 2776496
    new-instance v0, LX/K8y;

    const-string v1, "FEED_UNIT_CTA_BLUE_CLUE_TITLE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->FEED_UNIT_CTA_BLUE_CLUE_TITLE:LX/K8y;

    .line 2776497
    new-instance v0, LX/K8y;

    const-string v1, "FEED_UNIT_CTA_BLUE_CLUE_DESCRIPTION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->FEED_UNIT_CTA_BLUE_CLUE_DESCRIPTION:LX/K8y;

    .line 2776498
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TITLE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TITLE:LX/K8y;

    .line 2776499
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_DESCRIPTION"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_DESCRIPTION:LX/K8y;

    .line 2776500
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TURN_OFF_DIALOG_TITLE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_TITLE:LX/K8y;

    .line 2776501
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TURN_OFF_DIALOG_DESCRIPTION"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_DESCRIPTION:LX/K8y;

    .line 2776502
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TURN_OFF_DIALOG_PRIMARY_BUTTON"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_PRIMARY_BUTTON:LX/K8y;

    .line 2776503
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TURN_ON_DIALOG_TITLE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_TITLE:LX/K8y;

    .line 2776504
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TURN_ON_DIALOG_DESCRIPTION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_DESCRIPTION:LX/K8y;

    .line 2776505
    new-instance v0, LX/K8y;

    const-string v1, "MANAGE_SCREEN_TURN_ON_DIALOG_PRIMARY_BUTTON"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/K8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_PRIMARY_BUTTON:LX/K8y;

    .line 2776506
    const/16 v0, 0x1d

    new-array v0, v0, [LX/K8y;

    sget-object v1, LX/K8y;->PRODUCT_NAME:LX/K8y;

    aput-object v1, v0, v3

    sget-object v1, LX/K8y;->PRODUCT_NAME_LOWER_CASE:LX/K8y;

    aput-object v1, v0, v4

    sget-object v1, LX/K8y;->PRODUCT_NAME_ALL_CAPS:LX/K8y;

    aput-object v1, v0, v5

    sget-object v1, LX/K8y;->FEED_UNIT_CTA:LX/K8y;

    aput-object v1, v0, v6

    sget-object v1, LX/K8y;->COVER_CARD_READ_STORY_CTA:LX/K8y;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/K8y;->END_SCREEN_UNCONNECTED_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/K8y;->END_SCREEN_UNFOLLOWED_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/K8y;->END_SCREEN_FOLLOW_CTA_OFF:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/K8y;->END_SCREEN_FOLLOW_CTA_ON:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/K8y;->END_SCREEN_SUBSCRIBE_CTA_OFF:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/K8y;->END_SCREEN_SUBSCRIBE_CTA_ON:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/K8y;->END_SCREEN_CONNECTED_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/K8y;->END_SCREEN_FOLLOWED_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/K8y;->END_SCREEN_UNSUBSCRIBED_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/K8y;->END_SCREEN_SUBSCRIBED_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/K8y;->END_SCREEN_ALREADY_CONNECTED_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/K8y;->END_SCREEN_ALREADY_SUBSCRIBED_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/K8y;->FEED_UNIT_CTA_BLUE_CLUE_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/K8y;->FEED_UNIT_CTA_BLUE_CLUE_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/K8y;->MANAGE_SCREEN_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TURN_OFF_DIALOG_PRIMARY_BUTTON:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_TITLE:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_DESCRIPTION:LX/K8y;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/K8y;->MANAGE_SCREEN_TURN_ON_DIALOG_PRIMARY_BUTTON:LX/K8y;

    aput-object v2, v0, v1

    sput-object v0, LX/K8y;->$VALUES:[LX/K8y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2776474
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K8y;
    .locals 1

    .prologue
    .line 2776476
    const-class v0, LX/K8y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K8y;

    return-object v0
.end method

.method public static values()[LX/K8y;
    .locals 1

    .prologue
    .line 2776475
    sget-object v0, LX/K8y;->$VALUES:[LX/K8y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K8y;

    return-object v0
.end method
