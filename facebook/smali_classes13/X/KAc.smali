.class public final LX/KAc;
.super LX/4u6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4u6",
        "<",
        "LX/KAc;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile e:[LX/KAc;


# instance fields
.field public c:I

.field public d:LX/KAb;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, LX/4u6;-><init>()V

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput v0, p0, LX/KAc;->c:I

    iput-object v1, p0, LX/KAc;->d:LX/KAb;

    iput-object v1, p0, LX/KAc;->a:LX/4u8;

    const/4 v0, -0x1

    iput v0, p0, LX/KAc;->b:I

    return-void
.end method

.method public static e()[LX/KAc;
    .locals 2

    sget-object v0, LX/KAc;->e:[LX/KAc;

    if-nez v0, :cond_1

    sget-object v1, LX/4uA;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/KAc;->e:[LX/KAc;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [LX/KAc;

    sput-object v0, LX/KAc;->e:[LX/KAc;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, LX/KAc;->e:[LX/KAc;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, LX/4u6;->a()I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, LX/KAc;->c:I

    invoke-static {v1, v2}, LX/4u4;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/KAc;->d:LX/KAb;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, LX/KAc;->d:LX/KAb;

    invoke-static {v1, v2}, LX/4u4;->c(ILX/4u5;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public final a(LX/4u2;)LX/4u5;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/4u2;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1, v0}, LX/4u6;->a(LX/4u2;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, LX/4u2;->e()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, LX/KAc;->c:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, LX/KAc;->d:LX/KAb;

    if-nez v0, :cond_1

    new-instance v0, LX/KAb;

    invoke-direct {v0}, LX/KAb;-><init>()V

    iput-object v0, p0, LX/KAc;->d:LX/KAb;

    :cond_1
    iget-object v0, p0, LX/KAc;->d:LX/KAb;

    invoke-virtual {p1, v0}, LX/4u2;->a(LX/4u5;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/4u4;)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, LX/KAc;->c:I

    invoke-virtual {p1, v0, v1}, LX/4u4;->a(II)V

    iget-object v0, p0, LX/KAc;->d:LX/KAb;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, LX/KAc;->d:LX/KAb;

    invoke-virtual {p1, v0, v1}, LX/4u4;->a(ILX/4u5;)V

    :cond_0
    invoke-super {p0, p1}, LX/4u6;->a(LX/4u4;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LX/KAc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, LX/KAc;

    iget v2, p0, LX/KAc;->c:I

    iget v3, p1, LX/KAc;->c:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, LX/KAc;->d:LX/KAb;

    if-nez v2, :cond_4

    iget-object v2, p1, LX/KAc;->d:LX/KAb;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, LX/KAc;->d:LX/KAb;

    iget-object v3, p1, LX/KAc;->d:LX/KAb;

    invoke-virtual {v2, v3}, LX/KAb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, LX/4u6;->a:LX/4u8;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/4u6;->a:LX/4u8;

    invoke-virtual {v2}, LX/4u8;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    iget-object v2, p1, LX/4u6;->a:LX/4u8;

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/4u6;->a:LX/4u8;

    invoke-virtual {v2}, LX/4u8;->b()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v0, p0, LX/4u6;->a:LX/4u8;

    iget-object v1, p1, LX/4u6;->a:LX/4u8;

    invoke-virtual {v0, v1}, LX/4u8;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/KAc;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/KAc;->d:LX/KAb;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/4u6;->a:LX/4u8;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/4u6;->a:LX/4u8;

    invoke-virtual {v2}, LX/4u8;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, LX/KAc;->d:LX/KAb;

    invoke-virtual {v0}, LX/KAb;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, LX/4u6;->a:LX/4u8;

    invoke-virtual {v1}, LX/4u8;->hashCode()I

    move-result v1

    goto :goto_1
.end method
