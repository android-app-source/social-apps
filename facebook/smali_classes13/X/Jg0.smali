.class public final LX/Jg0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jfp;

.field public final synthetic b:LX/Jg1;


# direct methods
.method public constructor <init>(LX/Jg1;LX/Jfp;)V
    .locals 0

    .prologue
    .line 2722426
    iput-object p1, p0, LX/Jg0;->b:LX/Jg1;

    iput-object p2, p0, LX/Jg0;->a:LX/Jfp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2722427
    iget-object v0, p0, LX/Jg0;->a:LX/Jfp;

    if-eqz v0, :cond_0

    .line 2722428
    iget-object v0, p0, LX/Jg0;->a:LX/Jfp;

    invoke-interface {v0}, LX/Jfp;->a()V

    .line 2722429
    :cond_0
    iget-object v0, p0, LX/Jg0;->b:LX/Jg1;

    iget-object v0, v0, LX/Jg1;->c:LX/03V;

    const-string v1, "ManageSubstationsNullStateLoader"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2722430
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2722431
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;

    .line 2722432
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2722433
    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, LX/Jg0;->b:LX/Jg1;

    .line 2722434
    iput-object v1, v3, LX/Jg1;->d:LX/15i;

    .line 2722435
    iget-object v1, p0, LX/Jg0;->b:LX/Jg1;

    .line 2722436
    iput v0, v1, LX/Jg1;->e:I

    .line 2722437
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2722438
    :cond_0
    iget-object v0, p0, LX/Jg0;->a:LX/Jfp;

    if-nez v0, :cond_1

    .line 2722439
    :goto_0
    return-void

    .line 2722440
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2722441
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2722442
    iget-object v0, p0, LX/Jg0;->a:LX/Jfp;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$MessengerContentBroadcastSubStationsModel;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Jfp;->a(LX/0Px;)V

    goto :goto_0

    .line 2722443
    :cond_2
    iget-object v0, p0, LX/Jg0;->a:LX/Jfp;

    invoke-interface {v0}, LX/Jfp;->a()V

    goto :goto_0
.end method
