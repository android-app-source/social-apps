.class public LX/K08;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "StatusBarManager"
.end annotation


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2757335
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2757336
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2757361
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757362
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 2757363
    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 2757364
    :goto_0
    const-string v1, "HEIGHT"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1, v0}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 2757365
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757360
    const-string v0, "StatusBarManager"

    return-object v0
.end method

.method public setColor(IZ)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757354
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2757355
    if-nez v0, :cond_1

    .line 2757356
    const-string v0, "React"

    const-string v1, "StatusBarModule: Ignored status bar change, current activity is null."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757357
    :cond_0
    :goto_0
    return-void

    .line 2757358
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 2757359
    new-instance v1, Lcom/facebook/react/modules/statusbar/StatusBarModule$1;

    invoke-direct {v1, p0, p2, v0, p1}, Lcom/facebook/react/modules/statusbar/StatusBarModule$1;-><init>(LX/K08;ZLandroid/app/Activity;I)V

    invoke-static {v1}, LX/5pe;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setHidden(Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757349
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2757350
    if-nez v0, :cond_0

    .line 2757351
    const-string v0, "React"

    const-string v1, "StatusBarModule: Ignored status bar change, current activity is null."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757352
    :goto_0
    return-void

    .line 2757353
    :cond_0
    new-instance v1, Lcom/facebook/react/modules/statusbar/StatusBarModule$3;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/react/modules/statusbar/StatusBarModule$3;-><init>(LX/K08;ZLandroid/app/Activity;)V

    invoke-static {v1}, LX/5pe;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757343
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2757344
    if-nez v0, :cond_1

    .line 2757345
    const-string v0, "React"

    const-string v1, "StatusBarModule: Ignored status bar change, current activity is null."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757346
    :cond_0
    :goto_0
    return-void

    .line 2757347
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    .line 2757348
    new-instance v1, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;-><init>(LX/K08;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-static {v1}, LX/5pe;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setTranslucent(Z)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757337
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2757338
    if-nez v0, :cond_1

    .line 2757339
    const-string v0, "React"

    const-string v1, "StatusBarModule: Ignored status bar change, current activity is null."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757340
    :cond_0
    :goto_0
    return-void

    .line 2757341
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 2757342
    new-instance v1, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;-><init>(LX/K08;Landroid/app/Activity;Z)V

    invoke-static {v1}, LX/5pe;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
