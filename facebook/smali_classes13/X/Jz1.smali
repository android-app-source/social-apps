.class public LX/Jz1;
.super LX/Jz0;
.source ""


# instance fields
.field private final e:D

.field private final f:D

.field private g:J

.field private h:D

.field private i:D


# direct methods
.method public constructor <init>(LX/5pG;)V
    .locals 2

    .prologue
    .line 2755643
    invoke-direct {p0}, LX/Jz0;-><init>()V

    .line 2755644
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Jz1;->g:J

    .line 2755645
    const-string v0, "velocity"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz1;->e:D

    .line 2755646
    const-string v0, "deceleration"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz1;->f:D

    .line 2755647
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 13

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2755648
    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    .line 2755649
    iget-wide v2, p0, LX/Jz1;->g:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2755650
    const-wide/16 v2, 0x10

    sub-long v2, v0, v2

    iput-wide v2, p0, LX/Jz1;->g:J

    .line 2755651
    iget-object v2, p0, LX/Jz0;->b:LX/Jyx;

    iget-wide v2, v2, LX/Jyx;->e:D

    iput-wide v2, p0, LX/Jz1;->h:D

    .line 2755652
    iget-object v2, p0, LX/Jz0;->b:LX/Jyx;

    iget-wide v2, v2, LX/Jyx;->e:D

    iput-wide v2, p0, LX/Jz1;->i:D

    .line 2755653
    :cond_0
    iget-wide v2, p0, LX/Jz1;->h:D

    iget-wide v4, p0, LX/Jz1;->e:D

    iget-wide v6, p0, LX/Jz1;->f:D

    sub-double v6, v10, v6

    div-double/2addr v4, v6

    iget-wide v6, p0, LX/Jz1;->f:D

    sub-double v6, v10, v6

    neg-double v6, v6

    iget-wide v8, p0, LX/Jz1;->g:J

    sub-long/2addr v0, v8

    long-to-double v0, v0

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    sub-double v0, v10, v0

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    .line 2755654
    iget-wide v2, p0, LX/Jz1;->i:D

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 2755655
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Jz1;->a:Z

    .line 2755656
    :goto_0
    return-void

    .line 2755657
    :cond_1
    iput-wide v0, p0, LX/Jz1;->i:D

    .line 2755658
    iget-object v2, p0, LX/Jz0;->b:LX/Jyx;

    iput-wide v0, v2, LX/Jyx;->e:D

    goto :goto_0
.end method
