.class public LX/Jr9;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/FDs;

.field private final c:LX/Jrb;

.field private final d:LX/Jqb;

.field private final e:LX/Jrc;

.field private final f:LX/Jqf;

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742008
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr9;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/FDs;LX/Jrb;LX/Jqb;LX/Jrc;LX/0Ot;LX/Jqf;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/FDs;",
            "LX/Jrb;",
            "LX/Jqb;",
            "LX/Jrc;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jqf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2742009
    invoke-direct {p0, p6}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742010
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742011
    iput-object v0, p0, LX/Jr9;->g:LX/0Ot;

    .line 2742012
    iput-object p1, p0, LX/Jr9;->a:LX/0SG;

    .line 2742013
    iput-object p2, p0, LX/Jr9;->b:LX/FDs;

    .line 2742014
    iput-object p3, p0, LX/Jr9;->c:LX/Jrb;

    .line 2742015
    iput-object p4, p0, LX/Jr9;->d:LX/Jqb;

    .line 2742016
    iput-object p5, p0, LX/Jr9;->e:LX/Jrc;

    .line 2742017
    iput-object p7, p0, LX/Jr9;->f:LX/Jqf;

    .line 2742018
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742019
    invoke-virtual {p1}, LX/6kW;->w()LX/6k9;

    move-result-object v0

    .line 2742020
    iget-object v1, p0, LX/Jr9;->e:LX/Jrc;

    iget-object v0, v0, LX/6k9;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742021
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jr9;
    .locals 15

    .prologue
    .line 2742022
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742023
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742024
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742025
    if-nez v1, :cond_0

    .line 2742026
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742027
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742028
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742029
    sget-object v1, LX/Jr9;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742030
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742031
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742032
    :cond_1
    if-nez v1, :cond_4

    .line 2742033
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742034
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742035
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742036
    new-instance v7, LX/Jr9;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v9

    check-cast v9, LX/FDs;

    invoke-static {v0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v10

    check-cast v10, LX/Jrb;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v11

    check-cast v11, LX/Jqb;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v12

    check-cast v12, LX/Jrc;

    const/16 v13, 0x35bd

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v0}, LX/Jqf;->a(LX/0QB;)LX/Jqf;

    move-result-object v14

    check-cast v14, LX/Jqf;

    invoke-direct/range {v7 .. v14}, LX/Jr9;-><init>(LX/0SG;LX/FDs;LX/Jrb;LX/Jqb;LX/Jrc;LX/0Ot;LX/Jqf;)V

    .line 2742037
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2742038
    iput-object v8, v7, LX/Jr9;->g:LX/0Ot;

    .line 2742039
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742040
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742041
    if-nez v1, :cond_2

    .line 2742042
    sget-object v0, LX/Jr9;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr9;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742043
    :goto_1
    if-eqz v0, :cond_3

    .line 2742044
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742045
    :goto_3
    check-cast v0, LX/Jr9;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742046
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742047
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742048
    :catchall_1
    move-exception v0

    .line 2742049
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742050
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742051
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742052
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr9;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr9;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742053
    check-cast p1, LX/6kW;

    .line 2742054
    invoke-direct {p0, p1}, LX/Jr9;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2742055
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->w()LX/6k9;

    move-result-object v0

    .line 2742056
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/Jr9;->c:LX/Jrb;

    invoke-virtual {v3, v0, p1}, LX/Jrb;->a(LX/6k9;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    iget-object v5, p0, LX/Jr9;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742057
    iget-object v2, p0, LX/Jr9;->b:LX/FDs;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v2, v1, v4, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v1

    .line 2742058
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2742059
    const-string v3, "newMessageResult"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2742060
    iget-object v0, v0, LX/6k9;->messageMetadata:LX/6kn;

    .line 2742061
    if-eqz v0, :cond_0

    .line 2742062
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v0, v0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v3, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2742063
    if-eqz v0, :cond_0

    .line 2742064
    iget-object v0, p0, LX/Jr9;->f:LX/Jqf;

    invoke-virtual {v0, v1}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2742065
    :cond_0
    return-object v2
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742066
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742067
    if-eqz v0, :cond_0

    .line 2742068
    iget-object v1, p0, LX/Jr9;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2742069
    iget-object v1, p0, LX/Jr9;->d:LX/Jqb;

    .line 2742070
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2742071
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742072
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2742073
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742074
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/Jr9;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
