.class public final enum LX/JXg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JXg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JXg;

.field public static final enum CARDLESS:LX/JXg;

.field public static final enum HAS_HEADER:LX/JXg;

.field public static final enum NATIVE_STORY_ATTACHMENT:LX/JXg;

.field public static final enum VIDEO_TRANSITION:LX/JXg;


# instance fields
.field private final mRichMediaStyleInfo:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2705120
    new-instance v0, LX/JXg;

    const-string v1, "VIDEO_TRANSITION"

    const-string v2, "VIDEO_TRANSITION"

    invoke-direct {v0, v1, v3, v2}, LX/JXg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JXg;->VIDEO_TRANSITION:LX/JXg;

    .line 2705121
    new-instance v0, LX/JXg;

    const-string v1, "HAS_HEADER"

    const-string v2, "HAS_HEADER"

    invoke-direct {v0, v1, v4, v2}, LX/JXg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JXg;->HAS_HEADER:LX/JXg;

    .line 2705122
    new-instance v0, LX/JXg;

    const-string v1, "CARDLESS"

    const-string v2, "CARDLESS"

    invoke-direct {v0, v1, v5, v2}, LX/JXg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JXg;->CARDLESS:LX/JXg;

    .line 2705123
    new-instance v0, LX/JXg;

    const-string v1, "NATIVE_STORY_ATTACHMENT"

    const-string v2, "NATIVE_STORY_ATTACHMENT"

    invoke-direct {v0, v1, v6, v2}, LX/JXg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JXg;->NATIVE_STORY_ATTACHMENT:LX/JXg;

    .line 2705124
    const/4 v0, 0x4

    new-array v0, v0, [LX/JXg;

    sget-object v1, LX/JXg;->VIDEO_TRANSITION:LX/JXg;

    aput-object v1, v0, v3

    sget-object v1, LX/JXg;->HAS_HEADER:LX/JXg;

    aput-object v1, v0, v4

    sget-object v1, LX/JXg;->CARDLESS:LX/JXg;

    aput-object v1, v0, v5

    sget-object v1, LX/JXg;->NATIVE_STORY_ATTACHMENT:LX/JXg;

    aput-object v1, v0, v6

    sput-object v0, LX/JXg;->$VALUES:[LX/JXg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2705117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2705118
    iput-object p3, p0, LX/JXg;->mRichMediaStyleInfo:Ljava/lang/String;

    .line 2705119
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JXg;
    .locals 1

    .prologue
    .line 2705116
    const-class v0, LX/JXg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JXg;

    return-object v0
.end method

.method public static values()[LX/JXg;
    .locals 1

    .prologue
    .line 2705115
    sget-object v0, LX/JXg;->$VALUES:[LX/JXg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JXg;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2705114
    iget-object v0, p0, LX/JXg;->mRichMediaStyleInfo:Ljava/lang/String;

    return-object v0
.end method
