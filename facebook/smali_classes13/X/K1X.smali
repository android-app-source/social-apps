.class public final LX/K1X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/webkit/ValueCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/webkit/ValueCallback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/K1Z;


# direct methods
.method public constructor <init>(LX/K1Z;)V
    .locals 0

    .prologue
    .line 2762178
    iput-object p1, p0, LX/K1X;->a:LX/K1Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2762174
    const-string v0, "true"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2762175
    const-string v0, "React"

    const-string v1, "Setting onMessage on a WebView overrides existing values of window.postMessage, but a previous value was defined"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2762176
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic onReceiveValue(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2762177
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, LX/K1X;->a(Ljava/lang/String;)V

    return-void
.end method
