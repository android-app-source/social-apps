.class public final LX/Jhb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Jhc;


# direct methods
.method public constructor <init>(LX/Jhc;Z)V
    .locals 0

    .prologue
    .line 2724599
    iput-object p1, p0, LX/Jhb;->b:LX/Jhc;

    iput-boolean p2, p0, LX/Jhb;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x4fb27ae5

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2724600
    iget-object v0, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v0, v0, LX/Jhc;->j:LX/FCl;

    invoke-virtual {v0}, LX/FCl;->a()Z

    .line 2724601
    iget-boolean v0, p0, LX/Jhb;->a:Z

    if-eqz v0, :cond_0

    .line 2724602
    iget-object v0, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v0, v0, LX/Jhc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3A0;

    iget-object v1, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v1, v1, LX/Jhc;->A:LX/DAU;

    .line 2724603
    iget-object v2, v1, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v1, v2

    .line 2724604
    iget-object v2, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v2, v2, LX/Jhc;->A:LX/DAU;

    .line 2724605
    iget-object v4, v2, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v4

    .line 2724606
    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    iget-object v4, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v4, v4, LX/Jhc;->A:LX/DAU;

    .line 2724607
    iget-object v5, v4, LX/DAU;->o:Ljava/lang/String;

    move-object v4, v5

    .line 2724608
    iget-object v5, p0, LX/Jhb;->b:LX/Jhc;

    invoke-virtual {v5}, LX/Jhc;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V

    .line 2724609
    :goto_0
    const v0, -0x2c508058

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2724610
    :cond_0
    iget-object v0, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v0, v0, LX/Jhc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3A0;

    iget-object v1, p0, LX/Jhb;->b:LX/Jhc;

    invoke-virtual {v1}, LX/Jhc;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v2, v2, LX/Jhc;->A:LX/DAU;

    .line 2724611
    iget-object v4, v2, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v4

    .line 2724612
    iget-object v4, p0, LX/Jhb;->b:LX/Jhc;

    iget-object v4, v4, LX/Jhc;->A:LX/DAU;

    .line 2724613
    iget-object v5, v4, LX/DAU;->o:Ljava/lang/String;

    move-object v4, v5

    .line 2724614
    invoke-virtual {v0, v1, v2, v3, v4}, LX/3A0;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;)V

    goto :goto_0
.end method
