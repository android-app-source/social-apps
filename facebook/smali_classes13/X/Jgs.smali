.class public final LX/Jgs;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/Jgt;


# direct methods
.method public constructor <init>(LX/Jgt;)V
    .locals 0

    .prologue
    .line 2723876
    iput-object p1, p0, LX/Jgs;->a:LX/Jgt;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2723850
    iget-object v0, p0, LX/Jgs;->a:LX/Jgt;

    iget-object v0, v0, LX/Jgt;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2723851
    iget-object v1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->d:LX/0Zb;

    const-string v2, "update_favorites_failed"

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2723852
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2723853
    const-string v2, "reason"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2723854
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2723855
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->f:LX/4At;

    invoke-virtual {v1}, LX/4At;->stopShowingProgress()V

    .line 2723856
    iget-object v1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->e:LX/1CX;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p0

    .line 2723857
    iput-object p0, v2, LX/4mn;->b:Ljava/lang/String;

    .line 2723858
    move-object v2, v2

    .line 2723859
    const p0, 0x7f0802e6

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2723860
    iput-object p0, v2, LX/4mn;->c:Ljava/lang/String;

    .line 2723861
    move-object v2, v2

    .line 2723862
    new-instance p0, LX/Jh5;

    invoke-direct {p0, v0}, LX/Jh5;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723863
    iput-object p0, v2, LX/4mn;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 2723864
    move-object v2, v2

    .line 2723865
    invoke-virtual {v2}, LX/4mn;->l()LX/4mm;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2723866
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2723867
    iget-object v0, p0, LX/Jgs;->a:LX/Jgt;

    iget-object v0, v0, LX/Jgt;->b:LX/3Ku;

    iget-object v1, p0, LX/Jgs;->a:LX/Jgt;

    iget-object v1, v1, LX/Jgt;->h:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ku;->a(LX/0Px;)V

    .line 2723868
    iget-object v0, p0, LX/Jgs;->a:LX/Jgt;

    iget-object v0, v0, LX/Jgt;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    const/4 p1, 0x0

    .line 2723869
    iget-object v1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->d:LX/0Zb;

    const-string p0, "update_favorites_success"

    invoke-interface {v1, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2723870
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2723871
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2723872
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->f:LX/4At;

    invoke-virtual {v1}, LX/4At;->stopShowingProgress()V

    .line 2723873
    iput-boolean p1, v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    .line 2723874
    invoke-virtual {v0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->l()V

    .line 2723875
    return-void
.end method
