.class public LX/JhF;
.super LX/Idv;
.source ""


# instance fields
.field public b:LX/JhG;

.field public c:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field public d:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field public e:Landroid/view/View;

.field public f:Lcom/facebook/widget/tiles/ThreadTileView;

.field public g:LX/3Ky;

.field public h:LX/FJv;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2724224
    invoke-direct {p0, p1}, LX/Idv;-><init>(Landroid/content/Context;)V

    .line 2724225
    const v0, 0x7f030d0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2724226
    const v0, 0x7f0d0a22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/JhF;->c:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2724227
    const v0, 0x7f0d1575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/JhF;->d:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2724228
    const v0, 0x7f0d1e98    # 1.8758E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/JhF;->f:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2724229
    const v0, 0x7f0d209a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JhF;->e:Landroid/view/View;

    .line 2724230
    iget-object v0, p0, LX/JhF;->e:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2724231
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2724232
    invoke-static {v1}, LX/3Ky;->a(LX/0QB;)LX/3Ky;

    move-result-object v0

    check-cast v0, LX/3Ky;

    iput-object v0, p0, LX/JhF;->g:LX/3Ky;

    .line 2724233
    invoke-static {v1}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v0

    check-cast v0, LX/FJv;

    iput-object v0, p0, LX/JhF;->h:LX/FJv;

    .line 2724234
    return-void
.end method


# virtual methods
.method public getGroupRow()LX/JhG;
    .locals 1

    .prologue
    .line 2724235
    iget-object v0, p0, LX/JhF;->b:LX/JhG;

    return-object v0
.end method

.method public getInnerRow()Landroid/view/View;
    .locals 1

    .prologue
    .line 2724236
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JhF;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
