.class public LX/Jg8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2722494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722495
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Jg8;->a:Ljava/util/ArrayList;

    .line 2722496
    return-void
.end method

.method public static a(LX/0QB;)LX/Jg8;
    .locals 3

    .prologue
    .line 2722497
    const-class v1, LX/Jg8;

    monitor-enter v1

    .line 2722498
    :try_start_0
    sget-object v0, LX/Jg8;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2722499
    sput-object v2, LX/Jg8;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2722500
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2722501
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2722502
    new-instance v0, LX/Jg8;

    invoke-direct {v0}, LX/Jg8;-><init>()V

    .line 2722503
    move-object v0, v0

    .line 2722504
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2722505
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Jg8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2722506
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2722507
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
