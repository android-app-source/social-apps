.class public final LX/Juw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:LX/Jv4;


# direct methods
.method public constructor <init>(LX/Jv4;)V
    .locals 0

    .prologue
    .line 2749565
    iput-object p1, p0, LX/Juw;->a:LX/Jv4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2749566
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2749567
    if-eqz v0, :cond_0

    .line 2749568
    iget-object v0, p0, LX/Juw;->a:LX/Jv4;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/Jv4;->a$redex0(LX/Jv4;Z)V

    .line 2749569
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2749570
    :cond_0
    iget-object v0, p0, LX/Juw;->a:LX/Jv4;

    .line 2749571
    new-instance v1, LX/31Y;

    iget-object p0, v0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v1, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2749572
    const p0, 0x7f083af3

    invoke-virtual {v1, p0}, LX/0ju;->a(I)LX/0ju;

    .line 2749573
    const p0, 0x7f083af4

    invoke-virtual {v1, p0}, LX/0ju;->b(I)LX/0ju;

    .line 2749574
    const p0, 0x7f083af5

    new-instance p1, LX/Jux;

    invoke-direct {p1, v0}, LX/Jux;-><init>(LX/Jv4;)V

    invoke-virtual {v1, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2749575
    const/high16 p0, 0x1040000

    new-instance p1, LX/Juy;

    invoke-direct {p1, v0}, LX/Juy;-><init>(LX/Jv4;)V

    invoke-virtual {v1, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2749576
    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/0ju;->a(Z)LX/0ju;

    .line 2749577
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2749578
    goto :goto_0
.end method
