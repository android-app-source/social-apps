.class public final LX/Jpm;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "LX/DAC;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 0

    .prologue
    .line 2737959
    iput-object p1, p0, LX/Jpm;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2737960
    iget-object v0, p0, LX/Jpm;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->o(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2737961
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2737962
    check-cast p1, LX/0Px;

    .line 2737963
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2737964
    if-eqz p1, :cond_0

    .line 2737965
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAC;

    .line 2737966
    invoke-static {}, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d()LX/Jpx;

    move-result-object v4

    .line 2737967
    iget-object v5, v0, LX/DAC;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2737968
    iput-object v5, v4, LX/Jpx;->b:Ljava/lang/String;

    .line 2737969
    move-object v4, v4

    .line 2737970
    iget-object v5, v0, LX/DAC;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2737971
    iput-object v5, v4, LX/Jpx;->a:Ljava/lang/String;

    .line 2737972
    move-object v4, v4

    .line 2737973
    iget v5, v0, LX/DAC;->c:I

    move v5, v5

    .line 2737974
    iput v5, v4, LX/Jpx;->c:I

    .line 2737975
    move-object v4, v4

    .line 2737976
    iget v5, v0, LX/DAC;->d:I

    move v5, v5

    .line 2737977
    iput v5, v4, LX/Jpx;->d:I

    .line 2737978
    move-object v4, v4

    .line 2737979
    invoke-virtual {v4}, LX/Jpx;->b()Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    move-result-object v4

    move-object v0, v4

    .line 2737980
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/3OP;->a(Z)V

    .line 2737981
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2737982
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2737983
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2737984
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2737985
    iget-object v0, p0, LX/Jpm;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->o(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2737986
    :goto_1
    return-void

    .line 2737987
    :cond_1
    iget-object v1, p0, LX/Jpm;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    invoke-virtual {v1, v0}, LX/3Ne;->a(LX/0Px;)V

    .line 2737988
    iget-object v0, p0, LX/Jpm;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    const v1, 0x729f6f10

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2737989
    iget-object v0, p0, LX/Jpm;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->l(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    goto :goto_1
.end method
