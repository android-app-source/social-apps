.class public LX/Jni;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field public a:LX/2Ud;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Dhj;
    .annotation runtime Lcom/facebook/messaging/montage/blocking/MontageHiddenUsers;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734321
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jni;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2734353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734354
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734355
    iput-object v0, p0, LX/Jni;->b:LX/0Ot;

    .line 2734356
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734357
    iput-object v0, p0, LX/Jni;->d:LX/0Ot;

    .line 2734358
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734359
    iput-object v0, p0, LX/Jni;->e:LX/0Ot;

    .line 2734360
    return-void
.end method

.method public static a(LX/0QB;)LX/Jni;
    .locals 11

    .prologue
    .line 2734322
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2734323
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2734324
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2734325
    if-nez v1, :cond_0

    .line 2734326
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734327
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2734328
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2734329
    sget-object v1, LX/Jni;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2734330
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2734331
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2734332
    :cond_1
    if-nez v1, :cond_4

    .line 2734333
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2734334
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2734335
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2734336
    new-instance v1, LX/Jni;

    invoke-direct {v1}, LX/Jni;-><init>()V

    .line 2734337
    invoke-static {v0}, LX/2Ud;->a(LX/0QB;)LX/2Ud;

    move-result-object v7

    check-cast v7, LX/2Ud;

    const/16 v8, 0x140d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/Jng;->a(LX/0QB;)LX/Dhj;

    move-result-object v9

    check-cast v9, LX/Dhj;

    const/16 v10, 0xb83

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0xf9a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2734338
    iput-object v7, v1, LX/Jni;->a:LX/2Ud;

    iput-object v8, v1, LX/Jni;->b:LX/0Ot;

    iput-object v9, v1, LX/Jni;->c:LX/Dhj;

    iput-object v10, v1, LX/Jni;->d:LX/0Ot;

    iput-object p0, v1, LX/Jni;->e:LX/0Ot;

    .line 2734339
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2734340
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2734341
    if-nez v1, :cond_2

    .line 2734342
    sget-object v0, LX/Jni;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jni;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2734343
    :goto_1
    if-eqz v0, :cond_3

    .line 2734344
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2734345
    :goto_3
    check-cast v0, LX/Jni;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2734346
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2734347
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2734348
    :catchall_1
    move-exception v0

    .line 2734349
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2734350
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2734351
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2734352
    :cond_2
    :try_start_8
    sget-object v0, LX/Jni;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jni;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
