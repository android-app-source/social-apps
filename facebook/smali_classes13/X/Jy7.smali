.class public LX/Jy7;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JyD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/JyG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final e:LX/JyA;

.field private final f:LX/1De;

.field private final g:LX/Jxq;

.field private final h:LX/Jy3;


# direct methods
.method public constructor <init>(LX/0Px;LX/1De;LX/Jxq;LX/Jy3;)V
    .locals 1
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1De;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Jxq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Jy3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
            ">;",
            "LX/1De;",
            "LX/Jxq;",
            "Lcom/facebook/profile/discovery/DiscoveryDashboardAdapter$ListItemClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2754197
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2754198
    new-instance v0, LX/JyA;

    invoke-direct {v0, p1}, LX/JyA;-><init>(LX/0Px;)V

    iput-object v0, p0, LX/Jy7;->e:LX/JyA;

    .line 2754199
    iput-object p3, p0, LX/Jy7;->g:LX/Jxq;

    .line 2754200
    iput-object p2, p0, LX/Jy7;->f:LX/1De;

    .line 2754201
    iput-object p4, p0, LX/Jy7;->h:LX/Jy3;

    .line 2754202
    return-void
.end method

.method public static b(Lcom/facebook/components/ComponentView;LX/1De;LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/components/ComponentView;",
            "LX/1De;",
            "LX/1X1",
            "<+",
            "LX/1S3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2754191
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    move-object v0, v0

    .line 2754192
    if-nez v0, :cond_0

    .line 2754193
    invoke-static {p1, p2}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 2754194
    invoke-virtual {p0, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2754195
    :goto_0
    return-void

    .line 2754196
    :cond_0
    invoke-virtual {v0, p2}, LX/1dV;->a(LX/1X1;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5
    .param p2    # I
        .annotation build Lcom/facebook/profile/discovery/DiscoveryDashboardAdapter$ItemViewType;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2754203
    packed-switch p2, :pswitch_data_0

    .line 2754204
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received invalid bucket view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2754205
    :pswitch_0
    iget-object v0, p0, LX/Jy7;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030432

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 2754206
    new-instance v1, LX/Jy5;

    iget-object v2, p0, LX/Jy7;->f:LX/1De;

    iget-object v3, p0, LX/Jy7;->a:LX/JyD;

    iget-object v4, p0, LX/Jy7;->h:LX/Jy3;

    invoke-direct {v1, v0, v2, v3, v4}, LX/Jy5;-><init>(Lcom/facebook/components/ComponentView;LX/1De;LX/JyD;LX/Jy3;)V

    move-object v0, v1

    .line 2754207
    :goto_0
    return-object v0

    .line 2754208
    :pswitch_1
    iget-object v0, p0, LX/Jy7;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030433

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 2754209
    new-instance v1, LX/Jy6;

    iget-object v2, p0, LX/Jy7;->f:LX/1De;

    iget-object v3, p0, LX/Jy7;->b:LX/JyG;

    iget-object v4, p0, LX/Jy7;->h:LX/Jy3;

    invoke-direct {v1, v0, v2, v3, v4}, LX/Jy6;-><init>(Lcom/facebook/components/ComponentView;LX/1De;LX/JyG;LX/Jy3;)V

    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v0, 0x0

    .line 2754149
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    .line 2754150
    iget-object v1, p0, LX/Jy7;->e:LX/JyA;

    .line 2754151
    invoke-static {v1, p2}, LX/JyA;->d(LX/JyA;I)I

    move-result v4

    .line 2754152
    iget-object v3, v1, LX/JyA;->a:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    .line 2754153
    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v1, p2, v4}, LX/JyA;->a(LX/JyA;II)I

    move-result v4

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    move-object v1, v3

    .line 2754154
    iget-object v3, p0, LX/Jy7;->e:LX/JyA;

    .line 2754155
    invoke-static {v3, p2}, LX/JyA;->d(LX/JyA;I)I

    move-result v4

    invoke-static {v3, p2, v4}, LX/JyA;->a(LX/JyA;II)I

    move-result v4

    move v3, v4

    .line 2754156
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_0

    move v5, v11

    .line 2754157
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 2754158
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Received invalid bucket view type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v5, v0

    .line 2754159
    goto :goto_0

    .line 2754160
    :pswitch_0
    check-cast p1, LX/Jy5;

    .line 2754161
    iget-object v0, p0, LX/Jy7;->d:Landroid/content/res/Resources;

    .line 2754162
    iput-object v1, p1, LX/Jy5;->p:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2754163
    iget-object v2, p1, LX/Jy5;->n:LX/JyD;

    iget-object v4, p1, LX/Jy5;->m:LX/1De;

    const/4 v6, 0x0

    .line 2754164
    new-instance v7, LX/JyC;

    invoke-direct {v7, v2}, LX/JyC;-><init>(LX/JyD;)V

    .line 2754165
    sget-object v8, LX/JyD;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/JyB;

    .line 2754166
    if-nez v8, :cond_1

    .line 2754167
    new-instance v8, LX/JyB;

    invoke-direct {v8}, LX/JyB;-><init>()V

    .line 2754168
    :cond_1
    invoke-static {v8, v4, v6, v6, v7}, LX/JyB;->a$redex0(LX/JyB;LX/1De;IILX/JyC;)V

    .line 2754169
    move-object v7, v8

    .line 2754170
    move-object v6, v7

    .line 2754171
    move-object v2, v6

    .line 2754172
    iget-object v4, v2, LX/JyB;->a:LX/JyC;

    iput-object v1, v4, LX/JyC;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2754173
    iget-object v4, v2, LX/JyB;->d:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/BitSet;->set(I)V

    .line 2754174
    move-object v2, v2

    .line 2754175
    iget-object v4, v2, LX/JyB;->a:LX/JyC;

    iput-object p1, v4, LX/JyC;->b:Landroid/view/View$OnClickListener;

    .line 2754176
    iget-object v4, v2, LX/JyB;->d:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/BitSet;->set(I)V

    .line 2754177
    move-object v2, v2

    .line 2754178
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    .line 2754179
    iget-object v4, p1, LX/Jy5;->l:Lcom/facebook/components/ComponentView;

    iget-object v6, p1, LX/Jy5;->m:LX/1De;

    invoke-static {v4, v6, v2}, LX/Jy7;->b(Lcom/facebook/components/ComponentView;LX/1De;LX/1X1;)V

    .line 2754180
    iput p2, p1, LX/Jy5;->q:I

    .line 2754181
    iput-boolean v5, p1, LX/Jy5;->r:Z

    .line 2754182
    iget-object v2, p1, LX/Jy5;->l:Lcom/facebook/components/ComponentView;

    move-object v2, v2

    .line 2754183
    const v4, 0x7f0b26d1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v2, v4}, LX/Jy9;->a(Landroid/view/View;I)V

    .line 2754184
    iget-object v0, p0, LX/Jy7;->g:LX/Jxq;

    const-string v2, "Highlighted"

    move v4, p2

    invoke-virtual/range {v0 .. v5}, LX/Jxq;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IIZ)V

    .line 2754185
    :goto_1
    return-void

    :pswitch_1
    move-object v6, p1

    .line 2754186
    check-cast v6, LX/Jy6;

    .line 2754187
    add-int/lit8 v2, p2, -0x1

    invoke-virtual {p0, v2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    if-nez v2, :cond_2

    .line 2754188
    :goto_2
    iget-object v12, p0, LX/Jy7;->d:Landroid/content/res/Resources;

    move-object v7, v1

    move v8, v3

    move v9, p2

    move v10, v5

    invoke-static/range {v6 .. v12}, LX/Jy9;->a(LX/Jy6;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;IIZZLandroid/content/res/Resources;)V

    .line 2754189
    iget-object v0, p0, LX/Jy7;->g:LX/Jxq;

    const-string v2, "Normal"

    move v4, p2

    invoke-virtual/range {v0 .. v5}, LX/Jxq;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IIZ)V

    goto :goto_1

    :cond_2
    move v11, v0

    .line 2754190
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 2
    .annotation build Lcom/facebook/profile/discovery/DiscoveryDashboardAdapter$ItemViewType;
    .end annotation

    .prologue
    .line 2754142
    iget-object v0, p0, LX/Jy7;->e:LX/JyA;

    .line 2754143
    invoke-static {v0, p1}, LX/JyA;->d(LX/JyA;I)I

    move-result v1

    .line 2754144
    iget-object p0, v0, LX/JyA;->a:LX/0Px;

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    move-object v0, v1

    .line 2754145
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2754146
    iget-object v0, p0, LX/Jy7;->e:LX/JyA;

    .line 2754147
    iget p0, v0, LX/JyA;->b:I

    move v0, p0

    .line 2754148
    return v0
.end method
