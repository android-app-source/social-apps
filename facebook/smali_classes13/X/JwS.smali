.class public LX/JwS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Jwa;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JxJ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/placetips/pulsarcore/presence/PulsarBleScanProcessor;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JxO;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2cw;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Tf;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/1SR;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2cw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1SR;)V
    .locals 3
    .param p9    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1SP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jwc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JxJ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/placetips/pulsarcore/presence/PulsarBleScanProcessor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JxO;",
            ">;",
            "LX/2cw;",
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Tf;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;",
            "LX/1SR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2751843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2751844
    iput-object p3, p0, LX/JwS;->b:LX/0Ot;

    .line 2751845
    iput-object p4, p0, LX/JwS;->c:LX/0Ot;

    .line 2751846
    iput-object p5, p0, LX/JwS;->d:LX/0Ot;

    .line 2751847
    iput-object p6, p0, LX/JwS;->e:LX/2cw;

    .line 2751848
    iput-object p7, p0, LX/JwS;->f:LX/0Ot;

    .line 2751849
    iput-object p8, p0, LX/JwS;->g:LX/0Ot;

    .line 2751850
    iput-object p9, p0, LX/JwS;->h:LX/0Ot;

    .line 2751851
    iput-object p10, p0, LX/JwS;->i:LX/0Ot;

    .line 2751852
    iput-object p11, p0, LX/JwS;->j:LX/0Ot;

    .line 2751853
    iput-object p12, p0, LX/JwS;->k:LX/1SR;

    .line 2751854
    invoke-static {}, LX/1SK;->a()LX/4fd;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4fd;->a(LX/0Ot;)LX/4fd;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4fd;->a(LX/0Ot;)LX/4fd;

    move-result-object v0

    new-instance v1, LX/JwI;

    invoke-direct {v1, p0}, LX/JwI;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    .line 2751855
    invoke-static {v0}, LX/4fd;->a(LX/4fd;)LX/0Ot;

    move-result-object p1

    .line 2751856
    new-instance v0, LX/4fZ;

    invoke-direct {v0, p1, v1, v2}, LX/4fZ;-><init>(LX/0Ot;LX/0Vj;Ljava/util/concurrent/Executor;)V

    move-object p1, v0

    .line 2751857
    move-object v0, p1

    .line 2751858
    iput-object v0, p0, LX/JwS;->a:LX/0Ot;

    .line 2751859
    return-void
.end method

.method public static b(LX/0QB;)LX/JwS;
    .locals 13

    .prologue
    .line 2751860
    new-instance v0, LX/JwS;

    const/16 v1, 0xf5d

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2f72

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2f7a

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2f77

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2f7d

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/2cw;->a(LX/0QB;)LX/2cw;

    move-result-object v6

    check-cast v6, LX/2cw;

    const/16 v7, 0xf66

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x245

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1430

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x23

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xf5a

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/1SR;->b(LX/0QB;)LX/1SR;

    move-result-object v12

    check-cast v12, LX/1SR;

    invoke-direct/range {v0 .. v12}, LX/JwS;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2cw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1SR;)V

    .line 2751861
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0bZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2751862
    iget-object v0, p0, LX/JwS;->e:LX/2cw;

    sget-object v1, LX/3FC;->START_SCAN_SESSION:LX/3FC;

    sget-object v2, LX/2cx;->BLE:LX/2cx;

    invoke-virtual {v0, v1, v2}, LX/2cw;->a(LX/3FC;LX/2cx;)V

    .line 2751863
    iget-object v0, p0, LX/JwS;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/JwJ;

    invoke-direct {v1, p0}, LX/JwJ;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2751864
    new-instance v1, LX/JwK;

    invoke-direct {v1, p0}, LX/JwK;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2751865
    new-instance v1, LX/JwN;

    invoke-direct {v1, p0}, LX/JwN;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2751866
    new-instance v1, LX/JwL;

    invoke-direct {v1, p0}, LX/JwL;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2751867
    new-instance v1, LX/JwO;

    invoke-direct {v1, p0}, LX/JwO;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2751868
    new-instance v1, LX/JwP;

    invoke-direct {v1, p0}, LX/JwP;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2751869
    new-instance v1, LX/JwR;

    invoke-direct {v1, p0}, LX/JwR;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2751870
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v2, 0x1

    iget-object v0, p0, LX/JwS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/JwM;

    invoke-direct {v1, p0}, LX/JwM;-><init>(LX/JwS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
