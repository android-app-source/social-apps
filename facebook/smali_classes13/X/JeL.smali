.class public LX/JeL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/messaging/blocking/api/BlockedPerson;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2720597
    const-class v0, LX/JeL;

    sput-object v0, LX/JeL;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720599
    iput-object p1, p0, LX/JeL;->a:LX/0lC;

    .line 2720600
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2720601
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2720602
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fields"

    const-string v3, "%1$s,%2$s,%3$s,%4$s"

    const-string v4, "fbid"

    const-string v5, "name"

    const-string v6, "block_type"

    const-string v7, "block_time"

    invoke-static {v3, v4, v5, v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2720603
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "getBlockedPeople"

    .line 2720604
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2720605
    move-object v1, v1

    .line 2720606
    const-string v2, "GET"

    .line 2720607
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2720608
    move-object v1, v1

    .line 2720609
    const-string v2, "me/blocked"

    .line 2720610
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2720611
    move-object v1, v1

    .line 2720612
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2720613
    move-object v0, v1

    .line 2720614
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2720615
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2720616
    move-object v0, v0

    .line 2720617
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2720618
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2720619
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2720620
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2720621
    if-nez v0, :cond_0

    .line 2720622
    sget-object v0, LX/JeL;->b:Ljava/lang/Class;

    const-string v2, "Missing data node in response"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v1

    .line 2720623
    :goto_0
    return-object v0

    .line 2720624
    :cond_0
    invoke-virtual {v0}, LX/0lF;->h()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2720625
    sget-object v2, LX/JeL;->b:Ljava/lang/Class;

    const-string v3, "Unexpected type of json type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 2720626
    goto :goto_0

    .line 2720627
    :cond_1
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2720628
    iget-object v3, p0, LX/JeL;->a:LX/0lC;

    const-class v4, Lcom/facebook/messaging/blocking/api/BlockedPerson;

    invoke-virtual {v3, v0, v4}, LX/0lC;->a(LX/0lG;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2720629
    goto :goto_0
.end method
