.class public final LX/Jcf;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Jcf;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Jcd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Jcg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2718190
    const/4 v0, 0x0

    sput-object v0, LX/Jcf;->a:LX/Jcf;

    .line 2718191
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Jcf;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2718232
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2718233
    new-instance v0, LX/Jcg;

    invoke-direct {v0}, LX/Jcg;-><init>()V

    iput-object v0, p0, LX/Jcf;->c:LX/Jcg;

    .line 2718234
    return-void
.end method

.method public static c(LX/1De;)LX/Jcd;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2718224
    new-instance v1, LX/Jce;

    invoke-direct {v1}, LX/Jce;-><init>()V

    .line 2718225
    sget-object v2, LX/Jcf;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Jcd;

    .line 2718226
    if-nez v2, :cond_0

    .line 2718227
    new-instance v2, LX/Jcd;

    invoke-direct {v2}, LX/Jcd;-><init>()V

    .line 2718228
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Jcd;->a$redex0(LX/Jcd;LX/1De;IILX/Jce;)V

    .line 2718229
    move-object v1, v2

    .line 2718230
    move-object v0, v1

    .line 2718231
    return-object v0
.end method

.method public static declared-synchronized q()LX/Jcf;
    .locals 2

    .prologue
    .line 2718220
    const-class v1, LX/Jcf;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Jcf;->a:LX/Jcf;

    if-nez v0, :cond_0

    .line 2718221
    new-instance v0, LX/Jcf;

    invoke-direct {v0}, LX/Jcf;-><init>()V

    sput-object v0, LX/Jcf;->a:LX/Jcf;

    .line 2718222
    :cond_0
    sget-object v0, LX/Jcf;->a:LX/Jcf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2718223
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2718218
    invoke-static {}, LX/1dS;->b()V

    .line 2718219
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x79089236

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2718214
    const/4 v1, 0x0

    .line 2718215
    iput v1, p5, LX/1no;->a:I

    .line 2718216
    iput v1, p5, LX/1no;->b:I

    .line 2718217
    const/16 v1, 0x1f

    const v2, -0x25cfcfb2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2718212
    new-instance v0, Lcom/facebook/maps/FbStaticMapView;

    invoke-direct {v0, p1}, Lcom/facebook/maps/FbStaticMapView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2718213
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 3

    .prologue
    .line 2718202
    check-cast p2, LX/Jce;

    .line 2718203
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2718204
    iget-boolean v0, p2, LX/Jce;->a:Z

    .line 2718205
    if-eqz v0, :cond_0

    .line 2718206
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v2

    const p0, 0x7f020e90

    invoke-virtual {v2, p0}, LX/1nm;->h(I)LX/1nm;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2718207
    iput-object v2, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2718208
    :cond_0
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2718209
    check-cast v0, LX/1dc;

    iput-object v0, p2, LX/Jce;->b:LX/1dc;

    .line 2718210
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2718211
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2718201
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 2718181
    check-cast p3, LX/Jce;

    .line 2718182
    check-cast p2, Lcom/facebook/maps/FbStaticMapView;

    iget-object v0, p3, LX/Jce;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v1, p3, LX/Jce;->b:LX/1dc;

    .line 2718183
    invoke-virtual {p2, v0}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2718184
    const/16 p0, 0x8

    invoke-virtual {p2, p0}, LX/3BP;->setReportButtonVisibility(I)V

    .line 2718185
    if-eqz v1, :cond_0

    .line 2718186
    invoke-static {p1, v1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p0}, LX/3BP;->setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2718187
    :goto_0
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2718188
    return-void

    .line 2718189
    :cond_0
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/3BP;->setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2718200
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 2718193
    check-cast p3, LX/Jce;

    .line 2718194
    check-cast p2, Lcom/facebook/maps/FbStaticMapView;

    iget-object v0, p3, LX/Jce;->b:LX/1dc;

    .line 2718195
    if-eqz v0, :cond_0

    .line 2718196
    iget-object p0, p2, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    move-object p0, p0

    .line 2718197
    invoke-static {p1, p0, v0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 2718198
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/3BP;->setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2718199
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2718192
    const/16 v0, 0xf

    return v0
.end method
