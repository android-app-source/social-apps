.class public final LX/K9c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/DatePicker$OnDateChangedListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/K9d;


# direct methods
.method public constructor <init>(LX/K9d;I)V
    .locals 0

    .prologue
    .line 2777645
    iput-object p1, p0, LX/K9c;->b:LX/K9d;

    iput p2, p0, LX/K9c;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 4

    .prologue
    .line 2777646
    iget-object v0, p0, LX/K9c;->b:LX/K9d;

    iget-object v0, v0, LX/K9d;->i:Ljava/util/Calendar;

    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 2777647
    iget-object v0, p0, LX/K9c;->b:LX/K9d;

    iget v1, p0, LX/K9c;->a:I

    iget-object v2, p0, LX/K9c;->b:LX/K9d;

    iget-object v2, v2, LX/K9d;->d:Ljava/text/DateFormat;

    iget-object v3, p0, LX/K9c;->b:LX/K9d;

    iget-object v3, v3, LX/K9d;->i:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/K9d;->a(ILjava/lang/String;)V

    .line 2777648
    return-void
.end method
