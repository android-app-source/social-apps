.class public LX/Jw1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2751332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/location/ImmutableLocation;)Lcom/facebook/placetips/bootstrap/PresenceSource;
    .locals 6
    .param p0    # Lcom/facebook/location/ImmutableLocation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2751333
    if-nez p0, :cond_0

    .line 2751334
    sget-object v0, LX/2cx;->GPS:LX/2cx;

    invoke-static {v0}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a(LX/2cx;)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v0

    .line 2751335
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-static {v2, v3, v4, v5, v0}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a(DDLjava/lang/Float;)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v0

    goto :goto_0
.end method
