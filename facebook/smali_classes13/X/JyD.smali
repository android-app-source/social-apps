.class public LX/JyD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JyB;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2754306
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JyD;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2754307
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2754308
    iput-object p1, p0, LX/JyD;->b:LX/0Ot;

    .line 2754309
    return-void
.end method

.method public static a(LX/0QB;)LX/JyD;
    .locals 4

    .prologue
    .line 2754310
    const-class v1, LX/JyD;

    monitor-enter v1

    .line 2754311
    :try_start_0
    sget-object v0, LX/JyD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2754312
    sput-object v2, LX/JyD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2754313
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754314
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2754315
    new-instance v3, LX/JyD;

    const/16 p0, 0x3009

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JyD;-><init>(LX/0Ot;)V

    .line 2754316
    move-object v0, v3

    .line 2754317
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2754318
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JyD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2754319
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2754320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2754321
    const v0, -0x59000342

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2754322
    check-cast p2, LX/JyC;

    .line 2754323
    iget-object v0, p0, LX/JyD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;

    iget-object v1, p2, LX/JyC;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2754324
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    .line 2754325
    const v3, -0x59000342

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2754326
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    iget-object v2, v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->a(Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;LX/1Ad;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 p2, 0x2

    .line 2754327
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b26d6

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b26d5

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    .line 2754328
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lb_()Ljava/lang/String;

    move-result-object v4

    .line 2754329
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2754330
    const/4 v5, 0x0

    const p0, 0x7f0e011e

    invoke-static {p1, v5, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x3

    const p0, 0x7f0b26d1

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2754331
    :cond_0
    iget-object v4, v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->d:LX/JyJ;

    invoke-virtual {v4, p1}, LX/JyJ;->c(LX/1De;)LX/JyH;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/JyH;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;)LX/JyH;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/JyH;->h(I)LX/JyH;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 2754332
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2754333
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a009f

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b26d0

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2754334
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2754335
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b26d6

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    .line 2754336
    const/4 v3, 0x0

    const v5, 0x7f0e0124

    invoke-static {p1, v3, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    iget-object p0, v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    invoke-static {v1}, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;)Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f083c21

    :goto_0
    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 2754337
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2754338
    return-object v0

    :cond_1
    const v3, 0x7f083c20

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2754339
    invoke-static {}, LX/1dS;->b()V

    .line 2754340
    iget v0, p1, LX/1dQ;->b:I

    .line 2754341
    packed-switch v0, :pswitch_data_0

    .line 2754342
    :goto_0
    return-object v2

    .line 2754343
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2754344
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2754345
    check-cast v1, LX/JyC;

    .line 2754346
    iget-object p1, p0, LX/JyD;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/JyC;->b:Landroid/view/View$OnClickListener;

    .line 2754347
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2754348
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x59000342
        :pswitch_0
    .end packed-switch
.end method
