.class public final LX/JX8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic b:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 2704001
    iput-object p1, p0, LX/JX8;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    iput-object p2, p0, LX/JX8;->a:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6ff54a3c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704002
    iget-object v1, p0, LX/JX8;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    iget-object v2, p0, LX/JX8;->a:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2704003
    iget-object v5, v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->e:LX/1nG;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2704004
    if-nez v5, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v5

    :cond_0
    move-object v1, v5

    .line 2704005
    if-nez v1, :cond_1

    .line 2704006
    const v1, -0x1c79814f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2704007
    :goto_0
    return-void

    .line 2704008
    :cond_1
    iget-object v2, p0, LX/JX8;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v1, v4, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2704009
    const v1, -0xb9b54c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
