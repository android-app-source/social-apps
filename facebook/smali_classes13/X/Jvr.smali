.class public final LX/Jvr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750850
    iput-object p1, p0, LX/Jvr;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x52b680c6

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750851
    iget-object v1, p0, LX/Jvr;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 2750852
    iget-object v1, p0, LX/Jvr;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    .line 2750853
    const/4 p0, 0x1

    iput-boolean p0, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->X:Z

    .line 2750854
    iget-object p0, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2750855
    iget-object p0, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2750856
    const v1, -0x51f2b328

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2750857
    :goto_0
    return-void

    .line 2750858
    :cond_0
    iget-object v1, p0, LX/Jvr;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, p0, LX/Jvr;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;Landroid/net/Uri;)V

    .line 2750859
    const v1, -0x2b95548d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
