.class public final LX/Jzo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic a:LX/Jzq;

.field private final b:Lcom/facebook/react/bridge/Callback;

.field private c:Z


# direct methods
.method public constructor <init>(LX/Jzq;Lcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 2756731
    iput-object p1, p0, LX/Jzo;->a:LX/Jzq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756732
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Jzo;->c:Z

    .line 2756733
    iput-object p2, p0, LX/Jzo;->b:Lcom/facebook/react/bridge/Callback;

    .line 2756734
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2756735
    iget-boolean v0, p0, LX/Jzo;->c:Z

    if-nez v0, :cond_0

    .line 2756736
    iget-object v0, p0, LX/Jzo;->a:LX/Jzq;

    invoke-static {v0}, LX/Jzq;->a(LX/Jzq;)LX/5pY;

    move-result-object v0

    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756737
    iget-object v0, p0, LX/Jzo;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "buttonClicked"

    aput-object v3, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2756738
    iput-boolean v4, p0, LX/Jzo;->c:Z

    .line 2756739
    :cond_0
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2756740
    iget-boolean v0, p0, LX/Jzo;->c:Z

    if-nez v0, :cond_0

    .line 2756741
    iget-object v0, p0, LX/Jzo;->a:LX/Jzq;

    invoke-static {v0}, LX/Jzq;->b(LX/Jzq;)LX/5pY;

    move-result-object v0

    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756742
    iget-object v0, p0, LX/Jzo;->b:Lcom/facebook/react/bridge/Callback;

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dismissed"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2756743
    iput-boolean v4, p0, LX/Jzo;->c:Z

    .line 2756744
    :cond_0
    return-void
.end method
