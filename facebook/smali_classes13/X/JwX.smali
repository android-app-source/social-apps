.class public final LX/JwX;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "LX/JwV;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/JwV;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JwV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2751893
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 2751894
    new-instance v0, LX/JwW;

    invoke-direct {v0, p0, p1}, LX/JwW;-><init>(LX/JwX;LX/0Ot;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p2, v0, v1}, LX/1SK;->a(LX/0Ot;LX/0QK;Ljava/util/concurrent/Executor;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/JwX;->a:LX/0Ot;

    .line 2751895
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 2751896
    invoke-virtual {p0}, LX/JwX;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/JwV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2751892
    iget-object v0, p0, LX/JwX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2751891
    invoke-virtual {p0}, LX/JwX;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
