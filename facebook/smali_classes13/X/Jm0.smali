.class public LX/Jm0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Jm0;


# instance fields
.field public a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jm3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2732267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Jm0;
    .locals 4

    .prologue
    .line 2732245
    sget-object v0, LX/Jm0;->b:LX/Jm0;

    if-nez v0, :cond_1

    .line 2732246
    const-class v1, LX/Jm0;

    monitor-enter v1

    .line 2732247
    :try_start_0
    sget-object v0, LX/Jm0;->b:LX/Jm0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2732248
    if-eqz v2, :cond_0

    .line 2732249
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2732250
    new-instance v3, LX/Jm0;

    invoke-direct {v3}, LX/Jm0;-><init>()V

    .line 2732251
    const/16 p0, 0x27a9

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2732252
    iput-object p0, v3, LX/Jm0;->a:LX/0Or;

    .line 2732253
    move-object v0, v3

    .line 2732254
    sput-object v0, LX/Jm0;->b:LX/Jm0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2732255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2732256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2732257
    :cond_1
    sget-object v0, LX/Jm0;->b:LX/Jm0;

    return-object v0

    .line 2732258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2732259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2732260
    new-instance v1, Ljava/io/File;

    const-string v0, "inbox_units_json.txt"

    invoke-direct {v1, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2732261
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2732262
    :try_start_0
    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 2732263
    iget-object v0, p0, LX/Jm0;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jm3;

    invoke-virtual {v0}, LX/Jm3;->b()LX/162;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 2732264
    invoke-virtual {v3}, Ljava/io/PrintWriter;->flush()V

    .line 2732265
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2732266
    invoke-static {v2, v4}, LX/1md;->a(Ljava/io/Closeable;Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v4}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2732268
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2732269
    const-string v1, "inbox_units_json.txt"

    invoke-direct {p0, p1}, LX/Jm0;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732270
    return-object v0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2732240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2732241
    new-instance v1, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v2, "inbox_units_json.txt"

    invoke-direct {p0, p1}, LX/Jm0;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "text/plain"

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2732242
    return-object v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 2732243
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 1

    .prologue
    .line 2732244
    const/4 v0, 0x1

    return v0
.end method
