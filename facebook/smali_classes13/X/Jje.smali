.class public final LX/Jje;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V
    .locals 0

    .prologue
    .line 2727998
    iput-object p1, p0, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x52503ecc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2727999
    iget-object v1, p0, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->s:Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-static {v1}, Lcom/facebook/messaging/events/banner/EventReminderParams;->a(Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/JjW;

    move-result-object v1

    iget-object v2, p0, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728000
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v2, v3

    .line 2728001
    iput-object v2, v1, LX/JjW;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2728002
    move-object v1, v1

    .line 2728003
    iget-object v2, p0, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728004
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2728005
    iput-object v2, v1, LX/JjW;->h:Ljava/lang/String;

    .line 2728006
    move-object v1, v1

    .line 2728007
    iget-object v2, p0, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    .line 2728008
    iput-object v2, v1, LX/JjW;->f:Ljava/lang/String;

    .line 2728009
    move-object v1, v1

    .line 2728010
    invoke-virtual {v1}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v1

    .line 2728011
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2728012
    iget-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2728013
    iget-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->EVENT:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2728014
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2728015
    const-string v3, "reminder_params"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2728016
    new-instance v3, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;

    invoke-direct {v3}, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;-><init>()V

    .line 2728017
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2728018
    move-object v1, v3

    .line 2728019
    new-instance v2, LX/Jjd;

    invoke-direct {v2, p0}, LX/Jjd;-><init>(LX/Jje;)V

    .line 2728020
    iput-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->r:LX/Jjd;

    .line 2728021
    iget-object v2, p0, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "edit_event_reminder_location"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->b(LX/0gc;Ljava/lang/String;)V

    .line 2728022
    const v1, -0x149c6fa5

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2728023
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
