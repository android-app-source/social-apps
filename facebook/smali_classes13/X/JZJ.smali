.class public final LX/JZJ;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JZK;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field public d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xb

    .line 2708336
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2708337
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "callerContext"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "logoUri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "imageUri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "publishTime"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "containerWidth"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "containerHeight"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ctaOnClickListener"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "titleTypeface"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "digest"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "isSlideshowDisabled"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JZJ;->b:[Ljava/lang/String;

    .line 2708338
    iput v3, p0, LX/JZJ;->c:I

    .line 2708339
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JZJ;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JZJ;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JZJ;LX/1De;IILcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;)V
    .locals 1

    .prologue
    .line 2708332
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2708333
    iput-object p4, p0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    .line 2708334
    iget-object v0, p0, LX/JZJ;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2708335
    return-void
.end method


# virtual methods
.method public final a(J)LX/JZJ;
    .locals 3

    .prologue
    .line 2708315
    iget-object v0, p0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-wide p1, v0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->e:J

    .line 2708316
    iget-object v0, p0, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2708317
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2708328
    invoke-super {p0}, LX/1X5;->a()V

    .line 2708329
    const/4 v0, 0x0

    iput-object v0, p0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    .line 2708330
    sget-object v0, LX/JZK;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2708331
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JZK;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2708318
    iget-object v1, p0, LX/JZJ;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JZJ;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JZJ;->c:I

    if-ge v1, v2, :cond_2

    .line 2708319
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2708320
    :goto_0
    iget v2, p0, LX/JZJ;->c:I

    if-ge v0, v2, :cond_1

    .line 2708321
    iget-object v2, p0, LX/JZJ;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2708322
    iget-object v2, p0, LX/JZJ;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2708323
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2708324
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2708325
    :cond_2
    iget-object v0, p0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    .line 2708326
    invoke-virtual {p0}, LX/JZJ;->a()V

    .line 2708327
    return-object v0
.end method
