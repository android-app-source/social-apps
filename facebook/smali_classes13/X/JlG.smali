.class public LX/JlG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2730938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;I)Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2730939
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v0

    .line 2730940
    if-nez v0, :cond_0

    .line 2730941
    const/4 v0, 0x0

    .line 2730942
    :goto_0
    return-object v0

    .line 2730943
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->z()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;

    move-result-object v0

    .line 2730944
    new-instance v1, LX/JlQ;

    invoke-direct {v1}, LX/JlQ;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2730945
    iput-object v2, v1, LX/JlQ;->a:Ljava/lang/String;

    .line 2730946
    move-object v1, v1

    .line 2730947
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    .line 2730948
    iput-object v2, v1, LX/JlQ;->b:Ljava/lang/String;

    .line 2730949
    move-object v1, v1

    .line 2730950
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$Inbox2PageItemFragmentModel$PageModel$BestDescriptionModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$Inbox2PageItemFragmentModel$PageModel$BestDescriptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$Inbox2PageItemFragmentModel$PageModel$BestDescriptionModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2730951
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->j()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$Inbox2PageItemFragmentModel$PageModel$BestDescriptionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$Inbox2PageItemFragmentModel$PageModel$BestDescriptionModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2730952
    iput-object v0, v1, LX/JlQ;->c:Ljava/lang/String;

    .line 2730953
    :cond_1
    new-instance v2, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    invoke-direct {v2, p0, p1}, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2730954
    invoke-virtual {v2, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2730955
    new-instance v0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    .line 2730956
    new-instance p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;-><init>(LX/JlQ;)V

    move-object v1, p0

    .line 2730957
    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;-><init>(Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;)V

    goto :goto_0
.end method
