.class public final LX/K0W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field public final synthetic a:LX/K0Y;


# direct methods
.method public constructor <init>(LX/K0Y;)V
    .locals 0

    .prologue
    .line 2758853
    iput-object p1, p0, LX/K0W;->a:LX/K0Y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2758854
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 2758855
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 2758856
    iget-object v1, p0, LX/K0W;->a:LX/K0Y;

    iget-object v1, v1, LX/K0Y;->g:LX/K0T;

    const-string v2, "setOnRequestCloseListener must be called by the manager"

    invoke-static {v1, v2}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2758857
    iget-object v1, p0, LX/K0W;->a:LX/K0Y;

    iget-object v1, v1, LX/K0Y;->g:LX/K0T;

    invoke-interface {v1}, LX/K0T;->a()V

    .line 2758858
    :goto_0
    return v0

    .line 2758859
    :cond_0
    iget-object v0, p0, LX/K0W;->a:LX/K0Y;

    invoke-virtual {v0}, LX/K0Y;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    invoke-virtual {v0}, LX/5pX;->j()Landroid/app/Activity;

    move-result-object v0

    .line 2758860
    if-eqz v0, :cond_1

    .line 2758861
    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 2758862
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
