.class public LX/JrZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2743794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/6kn;)LX/6jT;
    .locals 2
    .param p0    # LX/6kn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2743795
    if-nez p0, :cond_0

    .line 2743796
    sget-object v0, LX/6jT;->a:LX/6jT;

    .line 2743797
    :goto_0
    return-object v0

    .line 2743798
    :cond_0
    new-instance v0, LX/6jS;

    invoke-direct {v0}, LX/6jS;-><init>()V

    .line 2743799
    iget-object v1, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 2743800
    iget-object v1, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2743801
    iput-boolean v1, v0, LX/6jS;->a:Z

    .line 2743802
    :cond_1
    iget-object v1, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2743803
    iget-object v1, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2743804
    iput v1, v0, LX/6jS;->b:I

    .line 2743805
    :cond_2
    invoke-virtual {v0}, LX/6jS;->a()LX/6jT;

    move-result-object v0

    goto :goto_0
.end method
