.class public LX/Jqs;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/Ifg;

.field private final b:LX/Jrc;

.field private final c:LX/Jrb;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740562
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqs;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Ifg;LX/Jrc;LX/Jrb;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ifg;",
            "LX/Jrc;",
            "LX/Jrb;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2740554
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2740555
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740556
    iput-object v0, p0, LX/Jqs;->e:LX/0Ot;

    .line 2740557
    iput-object p1, p0, LX/Jqs;->a:LX/Ifg;

    .line 2740558
    iput-object p2, p0, LX/Jqs;->b:LX/Jrc;

    .line 2740559
    iput-object p3, p0, LX/Jqs;->c:LX/Jrb;

    .line 2740560
    iput-object p4, p0, LX/Jqs;->d:LX/0Ot;

    .line 2740561
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2740548
    invoke-virtual {p1}, LX/6kW;->t()LX/6jk;

    move-result-object v0

    .line 2740549
    iget-object v0, v0, LX/6jk;->messageMetadatas:Ljava/util/List;

    .line 2740550
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 2740551
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kn;

    .line 2740552
    iget-object v3, p0, LX/Jqs;->b:LX/Jrc;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v3, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2740553
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/6kn;Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/6jk;J)LX/Jqq;
    .locals 4

    .prologue
    .line 2740535
    iget-object v0, p0, LX/Jqs;->b:LX/Jrc;

    iget-object v1, p1, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v0, v1}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2740536
    invoke-virtual {p2, v0}, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2740537
    iget-object v2, p2, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2740538
    new-instance v0, LX/Jqq;

    sget-object v1, LX/Jqr;->THREAD_FETCHED:LX/Jqr;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/Jqq;-><init>(LX/Jqr;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    move-object v0, v0

    .line 2740539
    :goto_0
    return-object v0

    .line 2740540
    :cond_0
    iget-object v2, p2, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->d:LX/0Rf;

    invoke-virtual {v2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2740541
    new-instance v0, LX/Jqq;

    sget-object v1, LX/Jqr;->THREAD_NONEXISTENT:LX/Jqr;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/Jqq;-><init>(LX/Jqr;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    move-object v0, v0

    .line 2740542
    goto :goto_0

    .line 2740543
    :cond_1
    iget-object v0, p0, LX/Jqs;->c:LX/Jrb;

    invoke-virtual {v0, p3, p1, v1}, LX/Jrb;->a(LX/6jk;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2740544
    iget-object v0, p0, LX/Jqs;->a:LX/Ifg;

    invoke-virtual {v0, v1}, LX/Ifg;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2740545
    iget-object v0, p0, LX/Jqs;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqf;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, p5, v2}, LX/Jqf;->a(Lcom/facebook/messaging/model/messages/Message;JZ)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2740546
    new-instance v1, LX/Jqq;

    sget-object v2, LX/Jqr;->NOT_PASSED:LX/Jqr;

    invoke-direct {v1, v2, v0}, LX/Jqq;-><init>(LX/Jqr;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    move-object v0, v1

    .line 2740547
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Jqs;
    .locals 10

    .prologue
    .line 2740563
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740564
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740565
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740566
    if-nez v1, :cond_0

    .line 2740567
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740568
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740569
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740570
    sget-object v1, LX/Jqs;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740571
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740572
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740573
    :cond_1
    if-nez v1, :cond_4

    .line 2740574
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740575
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740576
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2740577
    new-instance v9, LX/Jqs;

    invoke-static {v0}, LX/Ifg;->b(LX/0QB;)LX/Ifg;

    move-result-object v1

    check-cast v1, LX/Ifg;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v7

    check-cast v7, LX/Jrc;

    invoke-static {v0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v8

    check-cast v8, LX/Jrb;

    const/16 p0, 0x35bd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v9, v1, v7, v8, p0}, LX/Jqs;-><init>(LX/Ifg;LX/Jrc;LX/Jrb;LX/0Ot;)V

    .line 2740578
    const/16 v1, 0x29c9

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2740579
    iput-object v1, v9, LX/Jqs;->e:LX/0Ot;

    .line 2740580
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2740581
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740582
    if-nez v1, :cond_2

    .line 2740583
    sget-object v0, LX/Jqs;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqs;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740584
    :goto_1
    if-eqz v0, :cond_3

    .line 2740585
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740586
    :goto_3
    check-cast v0, LX/Jqs;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740587
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740588
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740589
    :catchall_1
    move-exception v0

    .line 2740590
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740591
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740592
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740593
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqs;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqs;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740533
    check-cast p1, LX/6kW;

    .line 2740534
    invoke-direct {p0, p1}, LX/Jqs;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2740513
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->t()LX/6jk;

    move-result-object v3

    .line 2740514
    iget-object v0, v3, LX/6jk;->messageMetadatas:Ljava/util/List;

    .line 2740515
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2740516
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v1

    move v7, v1

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6kn;

    .line 2740517
    iget-wide v4, p2, LX/7GJ;->b:J

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/Jqs;->a(LX/6kn;Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/6jk;J)LX/Jqq;

    move-result-object v0

    .line 2740518
    sget-object v1, LX/Jqp;->a:[I

    iget-object v2, v0, LX/Jqq;->a:LX/Jqr;

    invoke-virtual {v2}, LX/Jqr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2740519
    :pswitch_0
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    .line 2740520
    goto :goto_0

    .line 2740521
    :pswitch_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    .line 2740522
    goto :goto_0

    .line 2740523
    :pswitch_2
    iget-object v0, v0, LX/Jqq;->b:Lcom/facebook/messaging/service/model/NewMessageResult;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2740524
    :cond_0
    if-nez v7, :cond_1

    if-eqz v6, :cond_2

    .line 2740525
    :cond_1
    iget-object v0, p0, LX/Jqs;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7G0;

    .line 2740526
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "sync_broadcast_thread_passed_over"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2740527
    const-string v2, "num_passed_because_of_fetch"

    invoke-virtual {v1, v2, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2740528
    const-string v2, "num_passed_because_of_nonexistent"

    invoke-virtual {v1, v2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2740529
    iget-object v2, v0, LX/7G0;->a:LX/7G1;

    sget-object v3, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v2, v1, v3}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 2740530
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2740531
    const-string v1, "newMessageResults"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2740532
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740507
    const-string v0, "newMessageResults"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2740508
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2740509
    :cond_0
    return-void

    .line 2740510
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2740511
    iget-object v1, p0, LX/Jqs;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jqf;

    const/4 v5, 0x0

    iget-wide v6, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v5, v6, v7}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;LX/6k4;J)V

    .line 2740512
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740506
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/Jqs;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
