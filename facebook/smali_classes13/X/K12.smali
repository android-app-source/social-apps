.class public LX/K12;
.super Landroid/text/style/CharacterStyle;
.source ""


# instance fields
.field private final a:F

.field private final b:F

.field private final c:F

.field private final d:I


# direct methods
.method public constructor <init>(FFFI)V
    .locals 0

    .prologue
    .line 2760931
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    .line 2760932
    iput p1, p0, LX/K12;->a:F

    .line 2760933
    iput p2, p0, LX/K12;->b:F

    .line 2760934
    iput p3, p0, LX/K12;->c:F

    .line 2760935
    iput p4, p0, LX/K12;->d:I

    .line 2760936
    return-void
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 4

    .prologue
    .line 2760937
    iget v0, p0, LX/K12;->c:F

    iget v1, p0, LX/K12;->a:F

    iget v2, p0, LX/K12;->b:F

    iget v3, p0, LX/K12;->d:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 2760938
    return-void
.end method
