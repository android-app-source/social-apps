.class public final LX/JoS;
.super LX/4Ae;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;)V
    .locals 0

    .prologue
    .line 2735608
    iput-object p1, p0, LX/JoS;->a:Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;

    invoke-direct {p0}, LX/4Ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailed(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2735609
    iget-object v0, p0, LX/JoS;->a:Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;

    .line 2735610
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2735611
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2735612
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v1, v1

    .line 2735613
    if-nez v1, :cond_1

    .line 2735614
    :cond_0
    :goto_0
    return-void

    .line 2735615
    :cond_1
    iget-object v1, v0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->m:LX/1CX;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p0

    .line 2735616
    iput-object p0, v2, LX/4mn;->b:Ljava/lang/String;

    .line 2735617
    move-object v2, v2

    .line 2735618
    iput-object p1, v2, LX/4mn;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 2735619
    move-object v2, v2

    .line 2735620
    iput-object v0, v2, LX/4mn;->j:Landroid/support/v4/app/DialogFragment;

    .line 2735621
    move-object v2, v2

    .line 2735622
    invoke-virtual {v2}, LX/4mn;->l()LX/4mm;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->q:LX/2EJ;

    goto :goto_0
.end method

.method public final onSucceeded(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 1

    .prologue
    .line 2735623
    iget-object v0, p0, LX/JoS;->a:Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;

    .line 2735624
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2735625
    return-void
.end method
