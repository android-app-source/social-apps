.class public final LX/Jc1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/settings/activity/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V
    .locals 0

    .prologue
    .line 2717364
    iput-object p1, p0, LX/Jc1;->a:Lcom/facebook/katana/settings/activity/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2717365
    iget-object v0, p0, LX/Jc1;->a:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    invoke-virtual {v0, p1, p2}, LX/CIm;->a(Landroid/preference/Preference;Ljava/lang/Object;)V

    .line 2717366
    iget-object v0, p0, LX/Jc1;->a:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iget-object v1, v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->D:LX/0iZ;

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2717367
    if-eqz v0, :cond_0

    .line 2717368
    iget-object v2, v1, LX/0iZ;->c:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "videos_sound_toggle_opt_out_undo"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2717369
    :goto_0
    iget-object v0, p0, LX/Jc1;->a:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->C:LX/0iX;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1, v2}, LX/0iX;->a(ZLX/04g;)V

    .line 2717370
    const/4 v0, 0x1

    return v0

    .line 2717371
    :cond_0
    iget-object v2, v1, LX/0iZ;->c:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "videos_sound_toggle_opt_out"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
