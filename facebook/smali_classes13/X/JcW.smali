.class public final LX/JcW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 11

    .prologue
    .line 2717956
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2717957
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2717958
    if-eqz v0, :cond_5

    .line 2717959
    const-string v1, "last_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717960
    const/4 v4, 0x0

    .line 2717961
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2717962
    invoke-virtual {p0, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2717963
    if-eqz v2, :cond_3

    .line 2717964
    const-string v3, "coordinate"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717965
    const-wide/16 v9, 0x0

    .line 2717966
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2717967
    const/4 v5, 0x0

    invoke-virtual {p0, v2, v5, v9, v10}, LX/15i;->a(IID)D

    move-result-wide v5

    .line 2717968
    cmpl-double v7, v5, v9

    if-eqz v7, :cond_0

    .line 2717969
    const-string v7, "accuracy_meters"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717970
    invoke-virtual {p2, v5, v6}, LX/0nX;->a(D)V

    .line 2717971
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {p0, v2, v5, v9, v10}, LX/15i;->a(IID)D

    move-result-wide v5

    .line 2717972
    cmpl-double v7, v5, v9

    if-eqz v7, :cond_1

    .line 2717973
    const-string v7, "latitude"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717974
    invoke-virtual {p2, v5, v6}, LX/0nX;->a(D)V

    .line 2717975
    :cond_1
    const/4 v5, 0x2

    invoke-virtual {p0, v2, v5, v9, v10}, LX/15i;->a(IID)D

    move-result-wide v5

    .line 2717976
    cmpl-double v7, v5, v9

    if-eqz v7, :cond_2

    .line 2717977
    const-string v7, "longitude"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717978
    invoke-virtual {p2, v5, v6}, LX/0nX;->a(D)V

    .line 2717979
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2717980
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 2717981
    if-eqz v2, :cond_4

    .line 2717982
    const-string v3, "stale_time_seconds"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717983
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2717984
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2717985
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2717986
    return-void
.end method
