.class public LX/Jy4;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Jy3;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2754119
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2754120
    return-void
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;LX/Jxq;)LX/Jy3;
    .locals 11

    .prologue
    .line 2754121
    new-instance v0, LX/Jy3;

    invoke-direct {v0, p1, p2, p3}, LX/Jy3;-><init>(LX/1De;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;LX/Jxq;)V

    .line 2754122
    const/16 v1, 0x2eb

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x3694

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1b

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1430

    invoke-static {p0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x3007

    invoke-static {p0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const-class v9, LX/Jy8;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Jy8;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v0 .. v10}, LX/Jy3;->a(LX/Jy3;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/Jy8;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 2754123
    return-object v0
.end method
