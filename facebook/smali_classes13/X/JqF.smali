.class public LX/JqF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

.field private b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

.field private c:LX/94q;


# direct methods
.method public constructor <init>(Lcom/facebook/contactlogs/ContactLogsUploadRunner;Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;LX/94q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738585
    iput-object p1, p0, LX/JqF;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    .line 2738586
    iput-object p2, p0, LX/JqF;->b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    .line 2738587
    iput-object p3, p0, LX/JqF;->c:LX/94q;

    .line 2738588
    return-void
.end method

.method public static b(LX/0QB;)LX/JqF;
    .locals 4

    .prologue
    .line 2738589
    new-instance v3, LX/JqF;

    invoke-static {p0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->a(LX/0QB;)Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    move-result-object v0

    check-cast v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static {p0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b(LX/0QB;)Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    move-result-object v1

    check-cast v1, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-static {p0}, LX/94q;->a(LX/0QB;)LX/94q;

    move-result-object v2

    check-cast v2, LX/94q;

    invoke-direct {v3, v0, v1, v2}, LX/JqF;-><init>(Lcom/facebook/contactlogs/ContactLogsUploadRunner;Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;LX/94q;)V

    .line 2738590
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2738591
    iget-object v0, p0, LX/JqF;->c:LX/94q;

    sget-object v1, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->SHOW:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    invoke-virtual {v0, v1}, LX/94q;->a(Lcom/facebook/contacts/upload/ContactsUploadVisibility;)LX/1ML;

    .line 2738592
    iget-object v0, p0, LX/JqF;->b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    const/4 v1, 0x1

    .line 2738593
    iget-object v2, v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2738594
    sget-object v2, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2738595
    :goto_0
    iget-object v0, p0, LX/JqF;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-virtual {v0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->a()LX/1ML;

    .line 2738596
    return-void

    .line 2738597
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->a()Z

    move-result v2

    if-ne v2, v1, :cond_1

    .line 2738598
    sget-object v2, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v2, v2

    .line 2738599
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2738600
    :cond_1
    invoke-static {v0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b(Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;)LX/0Tn;

    move-result-object v2

    .line 2738601
    if-eqz v2, :cond_2

    .line 2738602
    iget-object v3, v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-interface {v3, v2, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2738603
    :cond_2
    iget-object v2, v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->e:LX/2UL;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2738604
    iget-object v4, v2, LX/2UL;->a:LX/0Zb;

    sget-object v5, LX/DA8;->UPLOAD_SETTING_SET:LX/DA8;

    invoke-static {v5}, LX/2UL;->b(LX/DA8;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "enabled"

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2738605
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2738606
    const-string v3, "set_contact_logs_upload_setting_param_key"

    if-eqz v1, :cond_3

    sget-object v2, LX/DAH;->ON:LX/DAH;

    :goto_1
    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2738607
    iget-object v2, v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->c:LX/0aG;

    const-string v3, "set_contact_logs_upload_setting"

    sget-object v5, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v6, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v7, 0x5f76400

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    goto :goto_0

    .line 2738608
    :cond_3
    sget-object v2, LX/DAH;->OFF:LX/DAH;

    goto :goto_1
.end method
