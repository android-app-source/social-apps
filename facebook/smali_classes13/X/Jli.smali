.class public final LX/Jli;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jln;


# direct methods
.method public constructor <init>(LX/Jln;)V
    .locals 0

    .prologue
    .line 2731692
    iput-object p1, p0, LX/Jli;->a:LX/Jln;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2731693
    iget-object v1, p0, LX/Jli;->a:LX/Jln;

    monitor-enter v1

    .line 2731694
    :try_start_0
    iget-object v0, p0, LX/Jli;->a:LX/Jln;

    const/4 v2, 0x0

    .line 2731695
    iput-object v2, v0, LX/Jln;->f:LX/Jll;

    .line 2731696
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2731697
    check-cast p1, LX/Jlv;

    .line 2731698
    iget-object v1, p0, LX/Jli;->a:LX/Jln;

    monitor-enter v1

    .line 2731699
    :try_start_0
    iget-object v0, p0, LX/Jli;->a:LX/Jln;

    .line 2731700
    iput-object p1, v0, LX/Jln;->g:LX/Jlv;

    .line 2731701
    iget-object v0, p0, LX/Jli;->a:LX/Jln;

    const/4 v2, 0x0

    .line 2731702
    iput-object v2, v0, LX/Jln;->f:LX/Jll;

    .line 2731703
    iget-object v0, p0, LX/Jli;->a:LX/Jln;

    const/4 v2, 0x0

    .line 2731704
    iput-boolean v2, v0, LX/Jln;->h:Z

    .line 2731705
    iget-object v0, p0, LX/Jli;->a:LX/Jln;

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/Jln;->a(LX/Jln;Z)V

    .line 2731706
    iget-object v0, p0, LX/Jli;->a:LX/Jln;

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/Jln;->b$redex0(LX/Jln;Z)V

    .line 2731707
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
