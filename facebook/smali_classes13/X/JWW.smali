.class public final LX/JWW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JWX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JWX;


# direct methods
.method public constructor <init>(LX/JWX;)V
    .locals 1

    .prologue
    .line 2703078
    iput-object p1, p0, LX/JWW;->c:LX/JWX;

    .line 2703079
    move-object v0, p1

    .line 2703080
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2703081
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2703077
    const-string v0, "FutureFriendingComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2703082
    if-ne p0, p1, :cond_1

    .line 2703083
    :cond_0
    :goto_0
    return v0

    .line 2703084
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2703085
    goto :goto_0

    .line 2703086
    :cond_3
    check-cast p1, LX/JWW;

    .line 2703087
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2703088
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2703089
    if-eq v2, v3, :cond_0

    .line 2703090
    iget-object v2, p0, LX/JWW;->a:LX/1Pc;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JWW;->a:LX/1Pc;

    iget-object v3, p1, LX/JWW;->a:LX/1Pc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2703091
    goto :goto_0

    .line 2703092
    :cond_5
    iget-object v2, p1, LX/JWW;->a:LX/1Pc;

    if-nez v2, :cond_4

    .line 2703093
    :cond_6
    iget-object v2, p0, LX/JWW;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JWW;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JWW;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2703094
    goto :goto_0

    .line 2703095
    :cond_7
    iget-object v2, p1, LX/JWW;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
