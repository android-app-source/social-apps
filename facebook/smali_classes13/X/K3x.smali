.class public final LX/K3x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/K3z;


# direct methods
.method public constructor <init>(LX/K3z;)V
    .locals 0

    .prologue
    .line 2766257
    iput-object p1, p0, LX/K3x;->a:LX/K3z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x16fe572d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2766247
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/storyline/model/Mood;

    if-eqz v0, :cond_1

    .line 2766248
    iget-object v0, p0, LX/K3x;->a:LX/K3z;

    iget-object v2, v0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/Mood;

    .line 2766249
    invoke-static {v2, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Lcom/facebook/storyline/model/Mood;)V

    .line 2766250
    :cond_0
    :goto_0
    const v0, 0x5a9248b6

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2766251
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/storyline/model/StorylinePhoto;

    if-eqz v0, :cond_2

    .line 2766252
    iget-object v0, p0, LX/K3x;->a:LX/K3z;

    iget-object v2, v0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/StorylinePhoto;

    invoke-static {v2, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->b(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Lcom/facebook/storyline/model/StorylinePhoto;)V

    goto :goto_0

    .line 2766253
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/K4K;

    if-eqz v0, :cond_0

    .line 2766254
    iget-object v0, p0, LX/K3x;->a:LX/K3z;

    iget-object v2, v0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K4K;

    .line 2766255
    invoke-static {v2, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/K4K;)V

    .line 2766256
    goto :goto_0
.end method
