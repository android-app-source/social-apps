.class public abstract LX/Jev;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2721332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;)Z
    .locals 2

    .prologue
    .line 2721333
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v0

    .line 2721334
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->j()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2721335
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->a()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;

    move-result-object v0

    .line 2721336
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->d()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->d()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2721337
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;)Lcom/facebook/user/model/User;
    .locals 5

    .prologue
    .line 2721338
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2721339
    iput-object v1, v0, LX/0XI;->h:Ljava/lang/String;

    .line 2721340
    move-object v0, v0

    .line 2721341
    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    const-string v1, "page"

    .line 2721342
    iput-object v1, v0, LX/0XI;->y:Ljava/lang/String;

    .line 2721343
    move-object v0, v0

    .line 2721344
    new-instance v1, Lcom/facebook/user/model/PicSquare;

    new-instance v2, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->d()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;->b()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel;->d()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$AdvertiserPageModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    .line 2721345
    iput-object v1, v0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 2721346
    move-object v0, v0

    .line 2721347
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method
