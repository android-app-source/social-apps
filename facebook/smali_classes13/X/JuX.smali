.class public final LX/JuX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic b:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic c:Lcom/facebook/messaging/notify/MentionNotification;

.field public final synthetic d:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/notify/MentionNotification;)V
    .locals 0

    .prologue
    .line 2748895
    iput-object p1, p0, LX/JuX;->d:LX/3RG;

    iput-object p2, p0, LX/JuX;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object p3, p0, LX/JuX;->b:Lcom/facebook/messaging/model/messages/Message;

    iput-object p4, p0, LX/JuX;->c:Lcom/facebook/messaging/notify/MentionNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748903
    iget-object v0, p0, LX/JuX;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v1, p0, LX/JuX;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2748904
    iget-object v1, p0, LX/JuX;->d:LX/3RG;

    iget-object v1, v1, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v2, p0, LX/JuX;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v0

    .line 2748905
    iget-object v1, p0, LX/JuX;->d:LX/3RG;

    iget-object v2, p0, LX/JuX;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/3RG;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2748906
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/JuX;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v2, p0, LX/JuX;->c:Lcom/facebook/messaging/notify/MentionNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/MentionNotification;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-static {}, LX/10A;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 2748907
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748908
    move-object v0, v0

    .line 2748909
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 2748910
    if-eqz p1, :cond_0

    .line 2748911
    iput-object p1, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2748912
    :cond_0
    iget-object v1, p0, LX/JuX;->d:LX/3RG;

    iget-object v1, v1, LX/3RG;->d:LX/3RK;

    iget-object v2, p0, LX/JuX;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x2734

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2748913
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2748914
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuX;->a(Landroid/graphics/Bitmap;)V

    .line 2748915
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2748896
    const/4 v0, 0x0

    .line 2748897
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2748898
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2748899
    :cond_0
    invoke-direct {p0, v0}, LX/JuX;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748900
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 2748901
    return-void

    .line 2748902
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
