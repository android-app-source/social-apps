.class public LX/Jj4;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

.field public b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public c:Lcom/facebook/messaging/events/banner/EventReminderMembers;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/events/banner/EventReminderParams;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/LinearLayout;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field public h:Lcom/facebook/widget/text/BetterTextView;

.field public i:Lcom/facebook/widget/text/BetterTextView;

.field public j:Lcom/facebook/widget/text/BetterTextView;

.field public k:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/Jiz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ifg;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JjT;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/Jjj;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2727230
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2727231
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727232
    iput-object v0, p0, LX/Jj4;->m:LX/0Ot;

    .line 2727233
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727234
    iput-object v0, p0, LX/Jj4;->n:LX/0Ot;

    .line 2727235
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727236
    iput-object v0, p0, LX/Jj4;->o:LX/0Ot;

    .line 2727237
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Jj4;

    invoke-static {v0}, LX/Jiz;->b(LX/0QB;)LX/Jiz;

    move-result-object v3

    check-cast v3, LX/Jiz;

    const/16 v4, 0x2775

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2772

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p1, 0x455

    invoke-static {v0, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/Jjj;->a(LX/0QB;)LX/Jjj;

    move-result-object v0

    check-cast v0, LX/Jjj;

    iput-object v3, v2, LX/Jj4;->l:LX/Jiz;

    iput-object v4, v2, LX/Jj4;->m:LX/0Ot;

    iput-object v5, v2, LX/Jj4;->n:LX/0Ot;

    iput-object p1, v2, LX/Jj4;->o:LX/0Ot;

    iput-object v0, v2, LX/Jj4;->p:LX/Jjj;

    .line 2727238
    const v0, 0x7f03050c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2727239
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Jj4;->setOrientation(I)V

    .line 2727240
    invoke-static {}, Lcom/facebook/messaging/events/banner/EventReminderParams;->newBuilder()LX/JjW;

    move-result-object v0

    const-string v1, "messaging"

    const-string v2, "reminder_banner"

    invoke-virtual {v0, v1, v2}, LX/JjW;->a(Ljava/lang/String;Ljava/lang/String;)LX/JjW;

    move-result-object v0

    invoke-virtual {v0}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v0

    iput-object v0, p0, LX/Jj4;->d:Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2727241
    const v0, 0x7f0d0e53

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Jj4;->e:Landroid/widget/LinearLayout;

    .line 2727242
    const v0, 0x7f0d0e54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Jj4;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2727243
    const v0, 0x7f0d0e57

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Jj4;->f:Landroid/widget/LinearLayout;

    .line 2727244
    const v0, 0x7f0d0e56

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Jj4;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2727245
    const v0, 0x7f0d0e59

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Jj4;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2727246
    const v0, 0x7f0d0e58

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Jj4;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2727247
    const v0, 0x7f0d0e55

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jj4;->k:LX/4ob;

    .line 2727248
    return-void
.end method

.method public static e(LX/Jj4;)V
    .locals 2

    .prologue
    .line 2727249
    iget-object v0, p0, LX/Jj4;->f:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2727250
    iget-object v0, p0, LX/Jj4;->e:Landroid/widget/LinearLayout;

    const v1, 0x7f02065a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2727251
    return-void
.end method
