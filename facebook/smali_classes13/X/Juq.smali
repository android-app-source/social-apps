.class public final LX/Juq;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;


# direct methods
.method public constructor <init>(Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;)V
    .locals 0

    .prologue
    .line 2749484
    iput-object p1, p0, LX/Juq;->a:Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2749485
    sget-object v0, LX/2Q0;->a:Ljava/lang/String;

    const-string v1, "Fetch threads failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2749486
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2749487
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2749488
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2749489
    iget-object v1, p0, LX/Juq;->a:Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;

    iget-object v1, v1, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    const/4 v4, 0x0

    .line 2749490
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v6, v2

    .line 2749491
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    move v3, v4

    :goto_0
    if-ge v5, v7, :cond_1

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2749492
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2749493
    iget-object p1, v1, LX/2Q0;->h:LX/297;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v2}, LX/297;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    .line 2749494
    :goto_1
    if-nez v2, :cond_3

    .line 2749495
    add-int/lit8 v2, v3, 0x1

    .line 2749496
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_0

    :cond_0
    move v2, v4

    .line 2749497
    goto :goto_1

    .line 2749498
    :cond_1
    move v0, v3

    .line 2749499
    iget-object v1, p0, LX/Juq;->a:Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;

    iget-object v1, v1, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    iget-object v1, v1, LX/2Q0;->b:LX/0Uh;

    const/16 v2, 0x10d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2749500
    iget-object v1, p0, LX/Juq;->a:Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;

    iget-object v1, v1, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    .line 2749501
    new-instance v3, LX/JlW;

    sget-object v2, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    sget-object v4, LX/JlY;->ALL:LX/JlY;

    invoke-direct {v3, v2, v4}, LX/JlW;-><init>(LX/0rS;LX/JlY;)V

    .line 2749502
    iget-object v2, v1, LX/2Q0;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Jln;

    invoke-virtual {v2, v3}, LX/Jln;->a(LX/JlW;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 2749503
    new-instance v3, LX/Jur;

    invoke-direct {v3, v1}, LX/Jur;-><init>(LX/2Q0;)V

    .line 2749504
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 2749505
    invoke-static {v2, v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2749506
    new-instance v3, LX/Jus;

    invoke-direct {v3, v1, v0}, LX/Jus;-><init>(LX/2Q0;I)V

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2749507
    :goto_3
    return-void

    .line 2749508
    :cond_2
    iget-object v1, p0, LX/Juq;->a:Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;

    iget-object v1, v1, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    iget-object v1, v1, LX/2Q0;->f:LX/2Ow;

    invoke-virtual {v1, v0}, LX/2Ow;->a(I)V

    goto :goto_3

    :cond_3
    move v2, v3

    goto :goto_2
.end method
