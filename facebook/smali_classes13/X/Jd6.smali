.class public LX/Jd6;
.super LX/Jcj;
.source ""


# instance fields
.field public final l:Landroid/content/Context;

.field public final m:LX/Jcq;

.field public final n:LX/4Ad;

.field public final o:Z

.field public final p:Landroid/widget/TextView;

.field public final q:Landroid/widget/TextView;

.field public final r:Lcom/facebook/user/tiles/UserTileView;

.field public final s:Landroid/view/View;

.field public t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;LX/Jcq;LX/4Ad;Z)V
    .locals 1

    .prologue
    .line 2718824
    invoke-direct {p0, p1}, LX/Jcj;-><init>(Landroid/view/View;)V

    .line 2718825
    iput-object p2, p0, LX/Jd6;->l:Landroid/content/Context;

    .line 2718826
    iput-object p3, p0, LX/Jd6;->m:LX/Jcq;

    .line 2718827
    iput-object p4, p0, LX/Jd6;->n:LX/4Ad;

    .line 2718828
    iput-boolean p5, p0, LX/Jd6;->o:Z

    .line 2718829
    const v0, 0x7f0d037b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jd6;->p:Landroid/widget/TextView;

    .line 2718830
    const v0, 0x7f0d037c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Jd6;->q:Landroid/widget/TextView;

    .line 2718831
    const v0, 0x7f0d037a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Jd6;->r:Lcom/facebook/user/tiles/UserTileView;

    .line 2718832
    const v0, 0x7f0d037e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jd6;->s:Landroid/view/View;

    .line 2718833
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 4

    .prologue
    .line 2718804
    iput-object p1, p0, LX/Jd6;->t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718805
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/Jd6;->t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2718806
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2718807
    iget-object v0, p0, LX/Jd6;->t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    .line 2718808
    iget-object v1, p0, LX/Jd6;->r:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    sget-object v3, LX/8ue;->MESSENGER:LX/8ue;

    invoke-static {v2, v3}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2718809
    iget-object v0, p0, LX/Jd6;->t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    .line 2718810
    iget-object v1, p0, LX/Jd6;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718811
    iget-object v0, p0, LX/Jd6;->q:Landroid/widget/TextView;

    iget-object v1, p0, LX/Jd6;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083b43

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718812
    iget-object v0, p0, LX/Jd6;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2718813
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/Jd6;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a023a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2718814
    const/16 v2, 0x8

    .line 2718815
    iget-boolean v0, p0, LX/Jd6;->o:Z

    if-nez v0, :cond_0

    .line 2718816
    iget-object v0, p0, LX/Jd6;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2718817
    :goto_0
    return-void

    .line 2718818
    :cond_0
    iget-object v0, p0, LX/Jd6;->n:LX/4Ad;

    iget-object v1, p0, LX/Jd6;->l:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/4Ad;->a(Landroid/content/Context;)LX/3dh;

    move-result-object v0

    .line 2718819
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/Jd6;->t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Jd6;->t:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iget-object v0, v0, LX/3dh;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2718820
    :goto_1
    if-eqz v0, :cond_2

    .line 2718821
    iget-object v0, p0, LX/Jd6;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2718822
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2718823
    :cond_2
    iget-object v0, p0, LX/Jd6;->s:Landroid/view/View;

    new-instance v1, LX/Jd5;

    invoke-direct {v1, p0}, LX/Jd5;-><init>(LX/Jd6;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
