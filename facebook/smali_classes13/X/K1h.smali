.class public LX/K1h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2762467
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2762468
    const-string v0, "reaction_unit_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2762469
    new-instance v1, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;

    invoke-direct {v1}, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;-><init>()V

    .line 2762470
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2762471
    const-string p1, "reaction_unit_id"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2762472
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2762473
    move-object v0, v1

    .line 2762474
    return-object v0
.end method
