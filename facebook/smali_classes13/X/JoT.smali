.class public final LX/JoT;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;)V
    .locals 0

    .prologue
    .line 2735690
    iput-object p1, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2735691
    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    const/4 v1, 0x0

    .line 2735692
    iput-object v1, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2735693
    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->x:LX/JoV;

    if-eqz v0, :cond_0

    .line 2735694
    :cond_0
    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    .line 2735695
    iget-object v1, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->o:Landroid/content/Context;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, LX/FMR;

    if-eqz v1, :cond_2

    .line 2735696
    :cond_1
    :goto_0
    return-void

    .line 2735697
    :cond_2
    iget-object v1, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->p:LX/1CX;

    iget-object v2, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->o:Landroid/content/Context;

    invoke-static {v2}, LX/4mm;->a(Landroid/content/Context;)LX/4mn;

    move-result-object v2

    invoke-static {}, LX/5MF;->a()I

    move-result p0

    invoke-virtual {v2, p0}, LX/4mn;->a(I)LX/4mn;

    move-result-object v2

    .line 2735698
    iput-object p1, v2, LX/4mn;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 2735699
    move-object v2, v2

    .line 2735700
    new-instance p0, LX/JoU;

    invoke-direct {p0, v0}, LX/JoU;-><init>(Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;)V

    .line 2735701
    iput-object p0, v2, LX/4mn;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 2735702
    move-object v2, v2

    .line 2735703
    invoke-virtual {v2}, LX/4mn;->l()LX/4mm;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2735704
    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->x:LX/JoV;

    if-eqz v0, :cond_0

    .line 2735705
    :cond_0
    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735706
    iget-object v1, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2735707
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2735708
    :cond_1
    iget-object v0, p0, LX/JoT;->a:Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2735709
    return-void
.end method
