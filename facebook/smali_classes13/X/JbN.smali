.class public LX/JbN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/JbN;


# instance fields
.field private final a:LX/6VX;


# direct methods
.method public constructor <init>(LX/6VX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2716765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2716766
    iput-object p1, p0, LX/JbN;->a:LX/6VX;

    .line 2716767
    return-void
.end method

.method public static a(LX/0QB;)LX/JbN;
    .locals 4

    .prologue
    .line 2716768
    sget-object v0, LX/JbN;->b:LX/JbN;

    if-nez v0, :cond_1

    .line 2716769
    const-class v1, LX/JbN;

    monitor-enter v1

    .line 2716770
    :try_start_0
    sget-object v0, LX/JbN;->b:LX/JbN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2716771
    if-eqz v2, :cond_0

    .line 2716772
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2716773
    new-instance p0, LX/JbN;

    invoke-static {v0}, LX/6VX;->a(LX/0QB;)LX/6VX;

    move-result-object v3

    check-cast v3, LX/6VX;

    invoke-direct {p0, v3}, LX/JbN;-><init>(LX/6VX;)V

    .line 2716774
    move-object v0, p0

    .line 2716775
    sput-object v0, LX/JbN;->b:LX/JbN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2716776
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2716777
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2716778
    :cond_1
    sget-object v0, LX/JbN;->b:LX/JbN;

    return-object v0

    .line 2716779
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2716780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Ljava/util/Map;)V
    .locals 8
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 2716781
    const-string v0, "application_link_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2716782
    if-eqz v0, :cond_0

    const-string v1, "app_store"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2716783
    :cond_0
    :goto_0
    return-void

    .line 2716784
    :cond_1
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2716785
    const-string v0, "app_id"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2716786
    if-nez p2, :cond_3

    const/4 v4, 0x0

    .line 2716787
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    .line 2716788
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v5

    .line 2716789
    check-cast p1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    .line 2716790
    if-eqz p1, :cond_2

    .line 2716791
    iget-object v0, p1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 2716792
    if-eqz v0, :cond_2

    .line 2716793
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2716794
    :cond_2
    iget-object v0, p0, LX/JbN;->a:LX/6VX;

    invoke-virtual/range {v0 .. v5}, LX/6VX;->a(Ljava/lang/String;JLX/162;Ljava/lang/String;)V

    goto :goto_0

    .line 2716795
    :cond_3
    const-string v0, "tracking"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/162;

    move-object v4, v0

    goto :goto_1
.end method
