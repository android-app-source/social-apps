.class public final LX/JtS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/attachments/ImageAttachmentData;

.field public final b:Ljava/lang/String;

.field public final c:LX/Jt0;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/attachments/ImageAttachmentData;Ljava/lang/String;LX/Jt0;)V
    .locals 0

    .prologue
    .line 2746774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746775
    iput-object p1, p0, LX/JtS;->a:Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 2746776
    iput-object p2, p0, LX/JtS;->b:Ljava/lang/String;

    .line 2746777
    iput-object p3, p0, LX/JtS;->c:LX/Jt0;

    .line 2746778
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/JtS;
    .locals 3

    .prologue
    .line 2746779
    new-instance v0, LX/JtS;

    const/4 v1, 0x0

    sget-object v2, LX/Jt0;->STICKER:LX/Jt0;

    invoke-direct {v0, v1, p0, v2}, LX/JtS;-><init>(Lcom/facebook/messaging/attachments/ImageAttachmentData;Ljava/lang/String;LX/Jt0;)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2746780
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FetchRequest{type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/JtS;->c:LX/Jt0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/JtS;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
