.class public final LX/JtD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/JtT;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

.field public final synthetic b:[B

.field public final synthetic c:LX/JtE;


# direct methods
.method public constructor <init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[B)V
    .locals 0

    .prologue
    .line 2746348
    iput-object p1, p0, LX/JtD;->c:LX/JtE;

    iput-object p2, p0, LX/JtD;->a:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    iput-object p3, p0, LX/JtD;->b:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746349
    sget-object v0, LX/JtE;->a:Ljava/lang/Class;

    const-string v1, "Exception occured processing the media fetch"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746350
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746351
    check-cast p1, Ljava/util/List;

    .line 2746352
    iget-object v0, p0, LX/JtD;->c:LX/JtE;

    iget-object v0, v0, LX/JtE;->f:Ljava/util/Map;

    iget-object v1, p0, LX/JtD;->a:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    iget-object v1, v1, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2746353
    iget-object v2, p0, LX/JtD;->a:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    iget-object v2, v2, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->d:Lcom/facebook/messengerwear/shared/Message;

    iget-wide v2, v2, Lcom/facebook/messengerwear/shared/Message;->a:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 2746354
    :goto_0
    return-void

    .line 2746355
    :cond_0
    iget-object v0, p0, LX/JtD;->c:LX/JtE;

    iget-object v0, v0, LX/JtE;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;

    iget-object v2, p0, LX/JtD;->c:LX/JtE;

    iget-object v3, p0, LX/JtD;->a:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    iget-object v4, p0, LX/JtD;->b:[B

    invoke-direct {v1, v2, v3, v4, p1}, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;-><init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[BLjava/util/List;)V

    const v2, 0x5b7718c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
