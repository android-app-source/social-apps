.class public LX/Jno;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:J


# instance fields
.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2734531
    const-string v0, "BASIC"

    const-string v1, "TIMESTAMP"

    const-string v2, "USER_PROMPT"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Jno;->a:Ljava/util/List;

    .line 2734532
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, LX/Jno;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2734528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734529
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Jno;->i:Ljava/util/List;

    .line 2734530
    return-void
.end method

.method public static a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;LX/15i;ILX/0Px;LX/0Px;LX/0Px;LX/0Px;)Lcom/facebook/messaging/montage/model/art/ArtItem;
    .locals 11
    .param p0    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "loadArtItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/15i;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;",
            ">;)",
            "Lcom/facebook/messaging/montage/model/art/ArtItem;"
        }
    .end annotation

    .prologue
    .line 2734507
    invoke-static/range {p7 .. p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734508
    invoke-static/range {p8 .. p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734509
    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-static {p0, v0, v1}, LX/Jno;->a(Ljava/util/Map;LX/0Px;LX/0Px;)Ljava/util/List;

    move-result-object v5

    .line 2734510
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-static {p0, v0, v1}, LX/Jno;->a(Ljava/util/Map;LX/0Px;LX/0Px;)Ljava/util/List;

    move-result-object v6

    .line 2734511
    const/4 v7, 0x0

    .line 2734512
    if-eqz p6, :cond_0

    .line 2734513
    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2734514
    :cond_0
    new-instance v3, Lcom/facebook/messaging/montage/model/art/ArtItem;

    move-object v4, p4

    move-wide v8, p1

    move-object v10, p3

    invoke-direct/range {v3 .. v10}, Lcom/facebook/messaging/montage/model/art/ArtItem;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;JLjava/lang/String;)V

    return-object v3
.end method

.method private static a(Ljava/util/Map;LX/0Px;LX/0Px;)Ljava/util/List;
    .locals 7
    .param p0    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2734515
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2734516
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 2734517
    if-eqz p0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2734518
    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/Sticker;

    .line 2734519
    if-eqz v1, :cond_0

    .line 2734520
    new-instance v6, Lcom/facebook/messaging/montage/model/art/StickerAsset;

    invoke-direct {v6, v1, v0}, Lcom/facebook/messaging/montage/model/art/StickerAsset;-><init>(Lcom/facebook/stickers/model/Sticker;Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2734521
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2734522
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    .line 2734523
    new-instance v1, Lcom/facebook/messaging/montage/model/art/ImageAsset;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/montage/model/art/ImageAsset;-><init>(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2734524
    :cond_2
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_2
    if-ge v1, v3, :cond_3

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;

    .line 2734525
    new-instance v2, Lcom/facebook/messaging/montage/model/art/TextAsset;

    invoke-direct {v2, v0}, Lcom/facebook/messaging/montage/model/art/TextAsset;-><init>(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2734526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2734527
    :cond_3
    return-object v4
.end method
