.class public LX/JWv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JWy;


# direct methods
.method public constructor <init>(LX/JWy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703686
    iput-object p1, p0, LX/JWv;->a:LX/JWy;

    .line 2703687
    return-void
.end method

.method public static a(LX/0QB;)LX/JWv;
    .locals 4

    .prologue
    .line 2703674
    const-class v1, LX/JWv;

    monitor-enter v1

    .line 2703675
    :try_start_0
    sget-object v0, LX/JWv;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703676
    sput-object v2, LX/JWv;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703677
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703678
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703679
    new-instance p0, LX/JWv;

    invoke-static {v0}, LX/JWy;->a(LX/0QB;)LX/JWy;

    move-result-object v3

    check-cast v3, LX/JWy;

    invoke-direct {p0, v3}, LX/JWv;-><init>(LX/JWy;)V

    .line 2703680
    move-object v0, p0

    .line 2703681
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703682
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703683
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
