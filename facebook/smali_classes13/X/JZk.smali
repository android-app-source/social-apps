.class public final LX/JZk;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/ref/WeakReference;

.field public final synthetic b:Ljava/lang/ref/WeakReference;

.field public final synthetic c:Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .prologue
    .line 2708984
    iput-object p1, p0, LX/JZk;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;

    iput-object p2, p0, LX/JZk;->a:Ljava/lang/ref/WeakReference;

    iput-object p3, p0, LX/JZk;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2708985
    check-cast p1, LX/2f2;

    .line 2708986
    iget-object v0, p0, LX/JZk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2708987
    iget-object v1, p0, LX/JZk;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Pn;

    .line 2708988
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v2, :cond_0

    iget-boolean v2, p1, LX/2f2;->c:Z

    if-nez v2, :cond_0

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2708989
    :cond_0
    :goto_0
    return-void

    .line 2708990
    :cond_1
    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v1

    .line 2708991
    check-cast v2, LX/1Pr;

    new-instance v4, LX/JZo;

    invoke-direct {v4, v3}, LX/JZo;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JZp;

    .line 2708992
    sget-object v3, LX/JZm;->b:[I

    iget-object v4, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2708993
    :goto_1
    check-cast v1, LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 2708994
    iget-object v1, p0, LX/JZk;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;

    invoke-static {v1, v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->e(Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    goto :goto_0

    .line 2708995
    :pswitch_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2708996
    iget-object v4, v2, LX/JZp;->b:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-object v4, v4

    .line 2708997
    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2708998
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    invoke-virtual {v2, v3}, LX/JZp;->a(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    goto :goto_1

    .line 2708999
    :pswitch_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2709000
    iget-object v4, v2, LX/JZp;->b:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-object v4, v4

    .line 2709001
    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2709002
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    invoke-virtual {v2, v3}, LX/JZp;->a(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
