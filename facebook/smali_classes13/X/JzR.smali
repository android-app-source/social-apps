.class public LX/JzR;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;
.implements LX/5pV;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "NativeAnimatedModule"
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:LX/5qn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/5r6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Jz8;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Jz8;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2755923
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2755924
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/JzR;->a:Ljava/lang/Object;

    .line 2755925
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    .line 2755926
    const/4 v0, 0x0

    iput-object v0, p0, LX/JzR;->e:Ljava/util/ArrayList;

    .line 2755927
    return-void
.end method

.method public static synthetic e(LX/JzR;)LX/5pY;
    .locals 1

    .prologue
    .line 2755910
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2755911
    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2755912
    iget-object v0, p0, LX/JzR;->c:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->NATIVE_ANIMATED_MODULE:LX/5r4;

    iget-object v2, p0, LX/JzR;->b:LX/5qn;

    invoke-virtual {v0, v1, v2}, LX/5r6;->b(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 2755913
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 2755914
    iget-object v0, p0, LX/JzR;->c:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->NATIVE_ANIMATED_MODULE:LX/5r4;

    iget-object v2, p0, LX/JzR;->b:LX/5qn;

    invoke-virtual {v0, v1, v2}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 2755915
    return-void
.end method


# virtual methods
.method public addAnimatedEventToView(ILjava/lang/String;LX/5pG;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755916
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzG;

    invoke-direct {v1, p0, p1, p2, p3}, LX/JzG;-><init>(LX/JzR;ILjava/lang/String;LX/5pG;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755917
    return-void
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2755918
    invoke-direct {p0}, LX/JzR;->i()V

    .line 2755919
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2755921
    invoke-direct {p0}, LX/JzR;->h()V

    .line 2755922
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2755920
    return-void
.end method

.method public connectAnimatedNodeToView(II)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755930
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzE;

    invoke-direct {v1, p0, p1, p2}, LX/JzE;-><init>(LX/JzR;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755931
    return-void
.end method

.method public connectAnimatedNodes(II)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755932
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzC;

    invoke-direct {v1, p0, p1, p2}, LX/JzC;-><init>(LX/JzR;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755933
    return-void
.end method

.method public createAnimatedNode(ILX/5pG;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755928
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzJ;

    invoke-direct {v1, p0, p1, p2}, LX/JzJ;-><init>(LX/JzR;ILX/5pG;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755929
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2755866
    invoke-static {}, LX/5r6;->a()LX/5r6;

    move-result-object v0

    iput-object v0, p0, LX/JzR;->c:LX/5r6;

    .line 2755867
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v0

    .line 2755868
    const-class v0, LX/5rQ;

    invoke-virtual {v1, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2755869
    new-instance v2, LX/JzS;

    invoke-direct {v2, v0}, LX/JzS;-><init>(LX/5rQ;)V

    .line 2755870
    new-instance v0, LX/JzI;

    invoke-direct {v0, p0, v1, v2}, LX/JzI;-><init>(LX/JzR;LX/5pX;LX/JzS;)V

    iput-object v0, p0, LX/JzR;->b:LX/5qn;

    .line 2755871
    invoke-virtual {v1, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2755872
    return-void
.end method

.method public disconnectAnimatedNodeFromView(II)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755906
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzF;

    invoke-direct {v1, p0, p1, p2}, LX/JzF;-><init>(LX/JzR;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755907
    return-void
.end method

.method public disconnectAnimatedNodes(II)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755908
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzD;

    invoke-direct {v1, p0, p1, p2}, LX/JzD;-><init>(LX/JzR;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755909
    return-void
.end method

.method public dropAnimatedNode(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755904
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzN;

    invoke-direct {v1, p0, p1}, LX/JzN;-><init>(LX/JzR;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755905
    return-void
.end method

.method public extractAnimatedNodeOffset(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755902
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/Jz9;

    invoke-direct {v1, p0, p1}, LX/Jz9;-><init>(LX/JzR;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755903
    return-void
.end method

.method public flattenAnimatedNodeOffset(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755900
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzQ;

    invoke-direct {v1, p0, p1}, LX/JzQ;-><init>(LX/JzR;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755901
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2755889
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 2755890
    :goto_0
    if-eqz v0, :cond_0

    .line 2755891
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/JzR;->d:Ljava/util/ArrayList;

    .line 2755892
    iget-object v1, p0, LX/JzR;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2755893
    :try_start_0
    iget-object v2, p0, LX/JzR;->e:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    .line 2755894
    iput-object v0, p0, LX/JzR;->e:Ljava/util/ArrayList;

    .line 2755895
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2755896
    :cond_0
    return-void

    .line 2755897
    :cond_1
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    goto :goto_0

    .line 2755898
    :cond_2
    :try_start_1
    iget-object v2, p0, LX/JzR;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2755899
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2755888
    const-string v0, "NativeAnimatedModule"

    return-object v0
.end method

.method public removeAnimatedEventFromView(ILjava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755886
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzH;

    invoke-direct {v1, p0, p1, p2}, LX/JzH;-><init>(LX/JzR;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755887
    return-void
.end method

.method public setAnimatedNodeOffset(ID)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755884
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzP;

    invoke-direct {v1, p0, p1, p2, p3}, LX/JzP;-><init>(LX/JzR;ID)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755885
    return-void
.end method

.method public setAnimatedNodeValue(ID)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755882
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzO;

    invoke-direct {v1, p0, p1, p2, p3}, LX/JzO;-><init>(LX/JzR;ID)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755883
    return-void
.end method

.method public startAnimatingNode(IILX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 7
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755880
    iget-object v6, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v0, LX/JzA;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/JzA;-><init>(LX/JzR;IILX/5pG;Lcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755881
    return-void
.end method

.method public startListeningToAnimatedNodeValue(I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755877
    new-instance v0, LX/JzK;

    invoke-direct {v0, p0, p1}, LX/JzK;-><init>(LX/JzR;I)V

    .line 2755878
    iget-object v1, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v2, LX/JzL;

    invoke-direct {v2, p0, p1, v0}, LX/JzL;-><init>(LX/JzR;ILX/Jyz;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755879
    return-void
.end method

.method public stopAnimation(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755875
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzB;

    invoke-direct {v1, p0, p1}, LX/JzB;-><init>(LX/JzR;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755876
    return-void
.end method

.method public stopListeningToAnimatedNodeValue(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2755873
    iget-object v0, p0, LX/JzR;->d:Ljava/util/ArrayList;

    new-instance v1, LX/JzM;

    invoke-direct {v1, p0, p1}, LX/JzM;-><init>(LX/JzR;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2755874
    return-void
.end method
