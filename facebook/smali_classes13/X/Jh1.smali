.class public final LX/Jh1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61y;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V
    .locals 0

    .prologue
    .line 2723930
    iput-object p1, p0, LX/Jh1;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 2723931
    iget-object v0, p0, LX/Jh1;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-static {v0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;

    move-result-object v0

    invoke-virtual {v0}, LX/JhJ;->b()I

    move-result v0

    sub-int v0, p1, v0

    .line 2723932
    iget-object v1, p0, LX/Jh1;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-static {v1}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;

    move-result-object v1

    invoke-virtual {v1}, LX/JhJ;->b()I

    move-result v1

    sub-int v1, p2, v1

    .line 2723933
    iget-object v2, p0, LX/Jh1;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    iget-object v2, v2, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2723934
    if-ltz v1, :cond_0

    iget-object p1, v2, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ge v1, p1, :cond_0

    if-ne v0, v1, :cond_2

    .line 2723935
    :cond_0
    const/4 p1, 0x0

    .line 2723936
    :goto_0
    move v0, p1

    .line 2723937
    if-eqz v0, :cond_1

    .line 2723938
    iget-object v0, p0, LX/Jh1;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-static {v0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->u(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723939
    iget-object v0, p0, LX/Jh1;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-static {v0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->v(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723940
    :cond_1
    return-void

    .line 2723941
    :cond_2
    iget-object p1, v2, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/user/model/User;

    .line 2723942
    iget-object p2, v2, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2723943
    iget-object p2, v2, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {p2, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2723944
    const/4 p1, 0x1

    goto :goto_0
.end method
