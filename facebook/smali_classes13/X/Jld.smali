.class public LX/Jld;
.super LX/6Lb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "LX/Jlc;",
        "LX/JlX;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Jln;

.field private final b:LX/Jlx;

.field public final c:LX/0SG;

.field private d:J


# direct methods
.method public constructor <init>(LX/Jln;LX/Jlx;LX/0SG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param

    .prologue
    .line 2731647
    invoke-direct {p0, p4}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 2731648
    iput-object p1, p0, LX/Jld;->a:LX/Jln;

    .line 2731649
    iput-object p2, p0, LX/Jld;->b:LX/Jlx;

    .line 2731650
    iput-object p3, p0, LX/Jld;->c:LX/0SG;

    .line 2731651
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 7

    .prologue
    .line 2731652
    check-cast p1, LX/Jlc;

    .line 2731653
    iget-object v0, p1, LX/Jlc;->a:LX/Jlb;

    sget-object v1, LX/Jlb;->FORCE_SERVER_FETCH:LX/Jlb;

    if-ne v0, v1, :cond_0

    .line 2731654
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    .line 2731655
    :goto_0
    return-object v0

    .line 2731656
    :cond_0
    iget-object v0, p0, LX/Jld;->a:LX/Jln;

    invoke-virtual {v0}, LX/Jln;->a()LX/Jlk;

    move-result-object v0

    .line 2731657
    if-eqz v0, :cond_3

    .line 2731658
    iget-object v1, v0, LX/Jlk;->a:LX/Jlv;

    .line 2731659
    iget-object v2, v1, LX/Jlv;->b:LX/JlX;

    move-object v1, v2

    .line 2731660
    iget-boolean v0, v0, LX/Jlk;->b:Z

    if-nez v0, :cond_1

    .line 2731661
    iget-object v3, p0, LX/Jld;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-wide v5, v1, LX/JlX;->c:J

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x124f80

    cmp-long v3, v3, v5

    if-lez v3, :cond_4

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 2731662
    if-nez v0, :cond_1

    iget-object v0, v1, LX/JlX;->a:LX/0ta;

    sget-object v2, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    .line 2731663
    :goto_2
    if-eqz v0, :cond_2

    invoke-static {v1}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_0

    .line 2731664
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 2731665
    :cond_2
    invoke-static {v1}, LX/6LZ;->a(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_0

    .line 2731666
    :cond_3
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 2731667
    check-cast p1, LX/Jlc;

    .line 2731668
    iget-object v0, p1, LX/Jlc;->a:LX/Jlb;

    sget-object v1, LX/Jlb;->FORCE_SERVER_FETCH:LX/Jlb;

    if-ne v0, v1, :cond_0

    .line 2731669
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2731670
    sget-object v0, LX/JlY;->ALL:LX/JlY;

    .line 2731671
    :goto_0
    new-instance v2, LX/JlW;

    invoke-direct {v2, v1, v0}, LX/JlW;-><init>(LX/0rS;LX/JlY;)V

    .line 2731672
    iget-object v3, p0, LX/Jld;->a:LX/Jln;

    invoke-virtual {v3, v2}, LX/Jln;->a(LX/JlW;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2731673
    new-instance v3, LX/Jla;

    invoke-direct {v3, p0, v1, v0, p1}, LX/Jla;-><init>(LX/Jld;LX/0rS;LX/JlY;LX/Jlc;)V

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2731674
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p2, LX/6LZ;->b:LX/6La;

    sget-object v1, LX/6La;->INTERMEDIATE:LX/6La;

    if-ne v0, v1, :cond_1

    .line 2731675
    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    .line 2731676
    sget-object v0, LX/JlY;->ALL:LX/JlY;

    goto :goto_0

    .line 2731677
    :cond_1
    iget-object v0, p1, LX/Jlc;->a:LX/Jlb;

    sget-object v1, LX/Jlb;->DEFAULT_WITH_TOP_UNITS_FIRST:LX/Jlb;

    if-eq v0, v1, :cond_2

    iget-object v0, p1, LX/Jlc;->a:LX/Jlb;

    sget-object v1, LX/Jlb;->TOP_UNITS_PRELOAD_ONLY:LX/Jlb;

    if-ne v0, v1, :cond_3

    .line 2731678
    :cond_2
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 2731679
    sget-object v0, LX/JlY;->TOP:LX/JlY;

    goto :goto_0

    .line 2731680
    :cond_3
    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 2731681
    sget-object v0, LX/JlY;->ALL:LX/JlY;

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2731682
    iget-object v0, p0, LX/Jld;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Jld;->d:J

    .line 2731683
    return-void
.end method
