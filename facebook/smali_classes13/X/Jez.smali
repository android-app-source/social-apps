.class public LX/Jez;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Jez;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/Jew;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jf2;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721349
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2721350
    iput-object v0, p0, LX/Jez;->d:LX/0Ot;

    .line 2721351
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Jez;->a:Ljava/util/Map;

    .line 2721352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Jez;->b:Ljava/util/Map;

    .line 2721353
    return-void
.end method

.method public static a(LX/0QB;)LX/Jez;
    .locals 5

    .prologue
    .line 2721354
    sget-object v0, LX/Jez;->e:LX/Jez;

    if-nez v0, :cond_1

    .line 2721355
    const-class v1, LX/Jez;

    monitor-enter v1

    .line 2721356
    :try_start_0
    sget-object v0, LX/Jez;->e:LX/Jez;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2721357
    if-eqz v2, :cond_0

    .line 2721358
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2721359
    new-instance v4, LX/Jez;

    invoke-direct {v4}, LX/Jez;-><init>()V

    .line 2721360
    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    const/16 p0, 0x269a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2721361
    iput-object v3, v4, LX/Jez;->c:LX/0So;

    iput-object p0, v4, LX/Jez;->d:LX/0Ot;

    .line 2721362
    move-object v0, v4

    .line 2721363
    sput-object v0, LX/Jez;->e:LX/Jez;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721364
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2721365
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2721366
    :cond_1
    sget-object v0, LX/Jez;->e:LX/Jez;

    return-object v0

    .line 2721367
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2721368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Jez;LX/Jew;LX/Dcr;)V
    .locals 4
    .param p1    # LX/Jew;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721369
    invoke-interface {p1}, LX/Jew;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2721370
    iget-object v1, p0, LX/Jez;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2721371
    if-nez v0, :cond_0

    .line 2721372
    :goto_0
    return-void

    .line 2721373
    :cond_0
    if-eqz p2, :cond_1

    .line 2721374
    iget-wide v0, p2, LX/Dcr;->a:J

    move-wide v2, v0

    .line 2721375
    :goto_1
    iget-object v0, p0, LX/Jez;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jf2;

    invoke-interface {p1}, LX/Jew;->c()Ljava/lang/String;

    move-result-object v1

    .line 2721376
    sget-object p0, LX/Jf0;->AD_IMPRESSION_TIMESPENT:LX/Jf0;

    invoke-static {p0, v1}, LX/Jf2;->a(LX/Jf0;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2721377
    sget-object p1, LX/Jf1;->TIME_ON_SCREEN:LX/Jf1;

    iget-object p1, p1, LX/Jf1;->value:Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2721378
    invoke-static {v0, p0}, LX/Jf2;->a(LX/Jf2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2721379
    goto :goto_0

    .line 2721380
    :cond_1
    iget-object v1, p0, LX/Jez;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    move-wide v2, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/inbox2/items/InboxUnitItem;)V
    .locals 4

    .prologue
    .line 2721381
    instance-of v0, p1, LX/Jew;

    if-eqz v0, :cond_0

    .line 2721382
    iget-object v0, p0, LX/Jez;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    check-cast p1, LX/Jew;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721383
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/messaging/inbox2/items/InboxUnitItem;)V
    .locals 8
    .param p1    # Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 2721384
    if-nez p1, :cond_1

    .line 2721385
    :cond_0
    :goto_0
    return-void

    .line 2721386
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a()LX/DfY;

    move-result-object v0

    sget-object v1, LX/DfY;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/DfY;

    if-ne v0, v1, :cond_2

    .line 2721387
    check-cast p1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;

    .line 2721388
    iget-object v0, p1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;->a:LX/0Px;

    move-object v2, v0

    .line 2721389
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;

    .line 2721390
    invoke-static {p0, v0, v5}, LX/Jez;->a(LX/Jez;LX/Jew;LX/Dcr;)V

    .line 2721391
    iget-object v4, p0, LX/Jez;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721392
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2721393
    :cond_2
    instance-of v0, p1, LX/Jew;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2721394
    check-cast v0, LX/Jew;

    invoke-static {p0, v0, v5}, LX/Jez;->a(LX/Jez;LX/Jew;LX/Dcr;)V

    .line 2721395
    iget-object v0, p0, LX/Jez;->a:Ljava/util/Map;

    check-cast p1, LX/Jew;

    invoke-interface {p1}, LX/Jew;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
