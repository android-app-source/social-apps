.class public final LX/K6I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K5m;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardImage;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardImage;)V
    .locals 0

    .prologue
    .line 2771365
    iput-object p1, p0, LX/K6I;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardImage;B)V
    .locals 0

    .prologue
    .line 2771366
    invoke-direct {p0, p1}, LX/K6I;-><init>(Lcom/facebook/tarot/cards/TarotCardImage;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771367
    iget-object v0, p0, LX/K6I;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setTitleFont(Landroid/graphics/Typeface;)V

    .line 2771368
    return-void
.end method

.method public final b(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771369
    iget-object v0, p0, LX/K6I;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionFont(Landroid/graphics/Typeface;)V

    .line 2771370
    return-void
.end method
