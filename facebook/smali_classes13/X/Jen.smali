.class public final LX/Jen;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/JeV;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Jdt;

.field public final synthetic d:Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;LX/JeV;Ljava/lang/String;LX/Jdt;)V
    .locals 0

    .prologue
    .line 2720944
    iput-object p1, p0, LX/Jen;->d:Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;

    iput-object p2, p0, LX/Jen;->a:LX/JeV;

    iput-object p3, p0, LX/Jen;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Jen;->c:LX/Jdt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 2720945
    if-eqz p2, :cond_2

    .line 2720946
    iget-object v0, p0, LX/Jen;->a:LX/JeV;

    if-eqz v0, :cond_0

    .line 2720947
    iget-object v0, p0, LX/Jen;->a:LX/JeV;

    iget-object v1, p0, LX/Jen;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/JeV;->a(Ljava/lang/String;)V

    .line 2720948
    :cond_0
    iget-object v0, p0, LX/Jen;->c:LX/Jdt;

    if-eqz v0, :cond_1

    .line 2720949
    iget-object v0, p0, LX/Jen;->c:LX/Jdt;

    invoke-interface {v0}, LX/Jdt;->a()V

    .line 2720950
    :cond_1
    :goto_0
    return-void

    .line 2720951
    :cond_2
    iget-object v0, p0, LX/Jen;->a:LX/JeV;

    if-eqz v0, :cond_3

    .line 2720952
    iget-object v0, p0, LX/Jen;->a:LX/JeV;

    iget-object v1, p0, LX/Jen;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/JeV;->b(Ljava/lang/String;)V

    .line 2720953
    :cond_3
    iget-object v0, p0, LX/Jen;->c:LX/Jdt;

    if-eqz v0, :cond_1

    .line 2720954
    iget-object v0, p0, LX/Jen;->c:LX/Jdt;

    invoke-interface {v0}, LX/Jdt;->b()V

    goto :goto_0
.end method
