.class public final LX/K8F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K8D;


# instance fields
.field public a:LX/1ca;

.field public b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/8Ya;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/K8G;


# direct methods
.method public constructor <init>(LX/K8G;)V
    .locals 1

    .prologue
    .line 2773717
    iput-object p1, p0, LX/K8F;->d:LX/K8G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773718
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/K8F;->c:Ljava/util/List;

    .line 2773719
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2773720
    iget-object v0, p0, LX/K8F;->a:LX/1ca;

    if-eqz v0, :cond_0

    .line 2773721
    iget-object v0, p0, LX/K8F;->a:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 2773722
    :cond_0
    iget-object v0, p0, LX/K8F;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 2773723
    iget-object v0, p0, LX/K8F;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2773724
    :cond_1
    iget-object v0, p0, LX/K8F;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2773725
    iget-object v0, p0, LX/K8F;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2773726
    iget-object v2, p0, LX/K8F;->d:LX/K8G;

    iget-object v2, v2, LX/K8G;->c:LX/8bW;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/8bW;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2773727
    :cond_2
    return-void
.end method
