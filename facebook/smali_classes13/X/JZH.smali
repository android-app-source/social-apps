.class public final LX/JZH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JZI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pm;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:LX/K8D;

.field public final synthetic e:LX/JZI;


# direct methods
.method public constructor <init>(LX/JZI;)V
    .locals 1

    .prologue
    .line 2708208
    iput-object p1, p0, LX/JZH;->e:LX/JZI;

    .line 2708209
    move-object v0, p1

    .line 2708210
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2708211
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2708212
    const-string v0, "TarotDigestPrefetchMountComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/JZI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2708213
    check-cast p1, LX/JZH;

    .line 2708214
    iget-object v0, p1, LX/JZH;->d:LX/K8D;

    iput-object v0, p0, LX/JZH;->d:LX/K8D;

    .line 2708215
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2708216
    if-ne p0, p1, :cond_1

    .line 2708217
    :cond_0
    :goto_0
    return v0

    .line 2708218
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2708219
    goto :goto_0

    .line 2708220
    :cond_3
    check-cast p1, LX/JZH;

    .line 2708221
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2708222
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2708223
    if-eq v2, v3, :cond_0

    .line 2708224
    iget-object v2, p0, LX/JZH;->a:LX/1Pm;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JZH;->a:LX/1Pm;

    iget-object v3, p1, LX/JZH;->a:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2708225
    goto :goto_0

    .line 2708226
    :cond_5
    iget-object v2, p1, LX/JZH;->a:LX/1Pm;

    if-nez v2, :cond_4

    .line 2708227
    :cond_6
    iget-object v2, p0, LX/JZH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JZH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JZH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2708228
    goto :goto_0

    .line 2708229
    :cond_8
    iget-object v2, p1, LX/JZH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2708230
    :cond_9
    iget-object v2, p0, LX/JZH;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JZH;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JZH;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2708231
    goto :goto_0

    .line 2708232
    :cond_a
    iget-object v2, p1, LX/JZH;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2708233
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/JZH;

    .line 2708234
    const/4 v1, 0x0

    iput-object v1, v0, LX/JZH;->d:LX/K8D;

    .line 2708235
    return-object v0
.end method
