.class public LX/JXr;
.super Lcom/facebook/feedplugins/video/RichVideoAttachmentView;
.source ""

# interfaces
.implements LX/JXQ;


# instance fields
.field public g:Landroid/widget/ImageView;

.field public h:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2705387
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JXr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2705388
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2705389
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JXr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2705390
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 2705391
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2705392
    const/4 p3, 0x3

    const/4 p2, 0x1

    const/4 v3, -0x1

    const/4 v5, 0x0

    .line 2705393
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/JXr;->g:Landroid/widget/ImageView;

    .line 2705394
    iget-object v0, p0, LX/JXr;->g:Landroid/widget/ImageView;

    const v1, 0x7f02165b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2705395
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2705396
    const/16 v1, 0x8

    const v2, 0x7f0d0917

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2705397
    const v1, 0x7f0b25c8

    invoke-static {p0, v1}, LX/JXr;->c(LX/JXr;I)I

    move-result v1

    invoke-virtual {v0, v5, v5, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2705398
    new-instance v1, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {v1, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2705399
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const v2, 0x7f0b25c6

    invoke-static {p0, v2}, LX/JXr;->c(LX/JXr;I)I

    move-result v2

    invoke-direct {v1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2705400
    const v2, 0x7f0b00d2

    invoke-static {p0, v2}, LX/JXr;->c(LX/JXr;I)I

    move-result v2

    const v3, 0x7f0b00d2

    invoke-static {p0, v3}, LX/JXr;->c(LX/JXr;I)I

    move-result v3

    invoke-virtual {v1, v2, v5, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2705401
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0b00d2

    invoke-static {p0, v3}, LX/JXr;->c(LX/JXr;I)I

    move-result v3

    const v4, 0x7f0b00d2

    invoke-static {p0, v4}, LX/JXr;->c(LX/JXr;I)I

    move-result v4

    invoke-virtual {v2, v5, v3, v5, v4}, Lcom/facebook/resources/ui/FbTextView;->setPadding(IIII)V

    .line 2705402
    const v2, 0x7f0d0917

    invoke-virtual {v1, p3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2705403
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/JXr;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0051

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 2705404
    invoke-virtual {p0}, LX/JXr;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 2705405
    div-float v4, v3, v4

    move v3, v4

    .line 2705406
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 2705407
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/JXr;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0161

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2705408
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, p2}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 2705409
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, p2}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2705410
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, p3}, Lcom/facebook/resources/ui/FbTextView;->setGravity(I)V

    .line 2705411
    iget-object v2, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2705412
    iget-object v2, p0, LX/JXr;->g:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v0}, LX/JXr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2705413
    iget-object v0, p0, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0, v1}, LX/JXr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2705414
    return-void
.end method

.method public static c(LX/JXr;I)I
    .locals 1

    .prologue
    .line 2705415
    invoke-virtual {p0}, LX/JXr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final nk_()V
    .locals 0

    .prologue
    .line 2705416
    return-void
.end method
