.class public LX/Jra;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2743806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2743807
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2743808
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kt;

    .line 2743809
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v5, v0, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 2743810
    new-instance v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, LX/6kt;->fullName:Ljava/lang/String;

    invoke-direct {v4, v3, v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2743811
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2743812
    :cond_0
    return-object v1
.end method

.method public static c(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2743813
    const/4 v0, 0x0

    .line 2743814
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2743815
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6kt;

    .line 2743816
    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v6, v1, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v4

    iget-object v5, v1, LX/6kt;->firstName:Ljava/lang/String;

    .line 2743817
    iput-object v5, v4, LX/0XI;->i:Ljava/lang/String;

    .line 2743818
    move-object v4, v4

    .line 2743819
    iget-object v5, v1, LX/6kt;->fullName:Ljava/lang/String;

    .line 2743820
    iput-object v5, v4, LX/0XI;->h:Ljava/lang/String;

    .line 2743821
    move-object v4, v4

    .line 2743822
    iget-object v1, v1, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2743823
    iput-boolean v1, v4, LX/0XI;->z:Z

    .line 2743824
    move-object v1, v4

    .line 2743825
    iput-boolean v0, v1, LX/0XI;->H:Z

    .line 2743826
    move-object v1, v1

    .line 2743827
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2743828
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2743829
    :cond_0
    move-object v0, v2

    .line 2743830
    return-object v0
.end method
