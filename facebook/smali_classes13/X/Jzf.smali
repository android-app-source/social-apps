.class public LX/Jzf;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKCameraRollManager"
.end annotation


# static fields
.field public static final a:Z

.field public static final b:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2756541
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_0

    move v0, v1

    .line 2756542
    :goto_0
    sput-boolean v0, LX/Jzf;->a:Z

    if-eqz v0, :cond_1

    .line 2756543
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v2

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v4

    const-string v1, "datetaken"

    aput-object v1, v0, v5

    const-string v1, "width"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "latitude"

    aput-object v2, v0, v1

    sput-object v0, LX/Jzf;->b:[Ljava/lang/String;

    .line 2756544
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 2756545
    goto :goto_0

    .line 2756546
    :cond_1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v2

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v4

    const-string v1, "datetaken"

    aput-object v1, v0, v5

    const-string v1, "longitude"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "latitude"

    aput-object v2, v0, v1

    sput-object v0, LX/Jzf;->b:[Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2756539
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2756540
    return-void
.end method

.method private static a(Landroid/database/Cursor;LX/5pH;II)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2756531
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 2756532
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 2756533
    cmpl-double v4, v0, v6

    if-gtz v4, :cond_0

    cmpl-double v4, v2, v6

    if-lez v4, :cond_1

    .line 2756534
    :cond_0
    new-instance v4, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v4}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756535
    const-string v5, "longitude"

    invoke-interface {v4, v5, v0, v1}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2756536
    const-string v0, "latitude"

    invoke-interface {v4, v0, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2756537
    const-string v0, "location"

    invoke-interface {p1, v0, v4}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2756538
    :cond_1
    return-void
.end method

.method private static a(Landroid/database/Cursor;LX/5pH;III)V
    .locals 6

    .prologue
    .line 2756527
    const-string v0, "type"

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756528
    const-string v0, "group_name"

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756529
    const-string v0, "timestamp"

    invoke-interface {p0, p4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-interface {p1, v0, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2756530
    return-void
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/database/Cursor;LX/5pH;III)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2756451
    new-instance v4, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v4}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756452
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2756453
    const-string v0, "uri"

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756454
    const/high16 v1, -0x40800000    # -1.0f

    .line 2756455
    sget-boolean v0, LX/Jzf;->a:Z

    if-eqz v0, :cond_2

    .line 2756456
    invoke-interface {p1, p4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-float v1, v0

    .line 2756457
    invoke-interface {p1, p5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    .line 2756458
    :goto_0
    cmpg-float v6, v1, v3

    if-lez v6, :cond_0

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_1

    .line 2756459
    :cond_0
    :try_start_0
    const-string v0, "r"

    invoke-virtual {p0, v5, v0}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 2756460
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2756461
    const/4 v1, 0x1

    iput-boolean v1, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2756462
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    const/4 v6, 0x0

    invoke-static {v1, v6, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2756463
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 2756464
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v1, v0

    .line 2756465
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-float v0, v0

    .line 2756466
    :cond_1
    const-string v3, "width"

    float-to-double v6, v1

    invoke-interface {v4, v3, v6, v7}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2756467
    const-string v1, "height"

    float-to-double v6, v0

    invoke-interface {v4, v1, v6, v7}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2756468
    const-string v0, "image"

    invoke-interface {p2, v0, v4}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    move v0, v2

    .line 2756469
    :goto_1
    return v0

    .line 2756470
    :catch_0
    move-exception v0

    .line 2756471
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not get width/height for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2756472
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;Landroid/database/Cursor;LX/5pH;I)V
    .locals 15

    .prologue
    .line 2756501
    new-instance v8, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v8}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 2756502
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2756503
    const-string v1, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 2756504
    const-string v1, "mime_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 2756505
    const-string v1, "bucket_display_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 2756506
    const-string v1, "datetaken"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 2756507
    sget-boolean v1, LX/Jzf;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "width"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 2756508
    :goto_0
    sget-boolean v1, LX/Jzf;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "height"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 2756509
    :goto_1
    const-string v1, "longitude"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 2756510
    const-string v1, "latitude"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 2756511
    const/4 v7, 0x0

    :goto_2
    move/from16 v0, p3

    if-ge v7, v0, :cond_3

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2756512
    new-instance v14, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v14}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756513
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    move-object v1, p0

    move-object/from16 v2, p1

    .line 2756514
    invoke-static/range {v1 .. v6}, LX/Jzf;->a(Landroid/content/ContentResolver;Landroid/database/Cursor;LX/5pH;III)Z

    move-result v1

    .line 2756515
    if-eqz v1, :cond_2

    .line 2756516
    move-object/from16 v0, p1

    invoke-static {v0, v3, v9, v10, v11}, LX/Jzf;->a(Landroid/database/Cursor;LX/5pH;III)V

    .line 2756517
    move-object/from16 v0, p1

    invoke-static {v0, v3, v12, v13}, LX/Jzf;->a(Landroid/database/Cursor;LX/5pH;II)V

    .line 2756518
    const-string v1, "node"

    invoke-interface {v14, v1, v3}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2756519
    invoke-interface {v8, v14}, LX/5pD;->a(LX/5pH;)V

    move v1, v7

    .line 2756520
    :goto_3
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 2756521
    add-int/lit8 v7, v1, 0x1

    goto :goto_2

    .line 2756522
    :cond_0
    const/4 v5, -0x1

    goto :goto_0

    .line 2756523
    :cond_1
    const/4 v6, -0x1

    goto :goto_1

    .line 2756524
    :cond_2
    add-int/lit8 v1, v7, -0x1

    goto :goto_3

    .line 2756525
    :cond_3
    const-string v1, "edges"

    move-object/from16 v0, p2

    invoke-interface {v0, v1, v8}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    .line 2756526
    return-void
.end method

.method public static b(Landroid/database/Cursor;LX/5pH;I)V
    .locals 3

    .prologue
    .line 2756493
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756494
    const-string v2, "has_next_page"

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2756495
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2756496
    add-int/lit8 v0, p2, -0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2756497
    const-string v0, "end_cursor"

    const-string v2, "datetaken"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756498
    :cond_0
    const-string v0, "page_info"

    invoke-interface {p1, v0, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2756499
    return-void

    .line 2756500
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2756492
    const-string v0, "RKCameraRollManager"

    return-object v0
.end method

.method public getPhotos(LX/5pG;LX/5pW;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    .line 2756479
    const-string v1, "first"

    invoke-interface {p1, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2756480
    const-string v1, "after"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "after"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2756481
    :goto_0
    const-string v1, "groupName"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "groupName"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2756482
    :goto_1
    const-string v1, "mimeTypes"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "mimeTypes"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v5

    .line 2756483
    :goto_2
    const-string v0, "groupTypes"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2756484
    new-instance v0, LX/5pA;

    const-string v1, "groupTypes is not supported on Android"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v3, v0

    .line 2756485
    goto :goto_0

    :cond_1
    move-object v4, v0

    .line 2756486
    goto :goto_1

    :cond_2
    move-object v5, v0

    .line 2756487
    goto :goto_2

    .line 2756488
    :cond_3
    new-instance v0, LX/Jzb;

    .line 2756489
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2756490
    move-object v6, p2

    invoke-direct/range {v0 .. v7}, LX/Jzb;-><init>(LX/5pX;ILjava/lang/String;Ljava/lang/String;LX/5pC;LX/5pW;B)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/Jzb;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2756491
    return-void
.end method

.method public saveToCameraRoll(Ljava/lang/String;Ljava/lang/String;LX/5pW;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2756473
    const-string v0, "video"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/Jzc;->VIDEO:LX/Jzc;

    .line 2756474
    :goto_0
    new-instance v1, LX/Jze;

    .line 2756475
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2756476
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0, p3}, LX/Jze;-><init>(LX/5pX;Landroid/net/Uri;LX/Jzc;LX/5pW;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0, v2}, LX/Jze;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2756477
    return-void

    .line 2756478
    :cond_0
    sget-object v0, LX/Jzc;->PHOTO:LX/Jzc;

    goto :goto_0
.end method
