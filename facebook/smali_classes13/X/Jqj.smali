.class public abstract LX/Jqj;
.super LX/Jqi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DE:",
        "LX/6kT;",
        ">",
        "LX/Jqi",
        "<TDE;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740087
    const-class v0, LX/Jqj;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Jqj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740088
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2740089
    iput-object p1, p0, LX/Jqj;->b:LX/0Ot;

    .line 2740090
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<TDE;>;)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<TDE;>;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740091
    const/4 v0, 0x1

    .line 2740092
    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    invoke-virtual {p0, v1}, LX/Jqj;->b(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 2740093
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740094
    invoke-virtual {v1}, LX/0Rf;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    const-string v2, "Classes extending SingleThreadDeltaHandler should not ensure more than one thread."

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2740095
    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2740096
    iget-object v1, p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2740097
    iget-object v1, p0, LX/Jqj;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7G0;

    iget-object v2, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v2, LX/6kT;

    const-string v3, "thread_up_to_date"

    invoke-virtual {v1, v2, v3}, LX/7G0;->a(LX/6kT;Ljava/lang/String;)V

    .line 2740098
    invoke-virtual {p1, v0}, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2740099
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740100
    invoke-virtual {p0, p2, v0}, LX/Jqj;->a(LX/7GJ;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2740101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2740102
    :goto_1
    move-object v0, v0

    .line 2740103
    return-object v0

    .line 2740104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2740105
    :cond_1
    iget-object v1, p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->d:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2740106
    iget-object v0, p0, LX/Jqj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7G0;

    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kT;

    const-string v2, "thread_non_existing"

    invoke-virtual {v0, v1, v2}, LX/7G0;->a(LX/6kT;Ljava/lang/String;)V

    .line 2740107
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_1

    .line 2740108
    :cond_2
    invoke-virtual {p1, v0}, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2740109
    invoke-virtual {p0, v0, p2}, LX/Jqj;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;

    move-result-object v1

    .line 2740110
    const-string v0, "threadSummary"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2740111
    const-string v0, "threadSummary"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740112
    const-string v2, "threadSummary"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2740113
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 2740114
    iget-object v2, p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->b:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2740115
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object p2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, p2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v3, v0

    .line 2740116
    :goto_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 2740117
    :cond_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/threads/ThreadSummary;

    goto :goto_3

    .line 2740118
    :cond_4
    new-instance v2, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    iget-object p0, p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->d:LX/0Rf;

    invoke-direct {v2, v3, v4, p0}, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;-><init>(LX/0P1;LX/0Px;LX/0Rf;)V

    move-object v0, v2

    .line 2740119
    const-string v2, "updatedPrefetchData"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_5
    move-object v0, v1

    .line 2740120
    goto/16 :goto_1
.end method

.method public a(LX/7GJ;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<TDE;>;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2740121
    return-void
.end method
