.class public LX/K0E;
.super LX/3tW;
.source ""


# instance fields
.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/5pX;)V
    .locals 1

    .prologue
    .line 2758054
    invoke-direct {p0, p1}, LX/3tW;-><init>(Landroid/content/Context;)V

    .line 2758055
    const v0, 0x800003

    iput v0, p0, LX/K0E;->b:I

    .line 2758056
    const/4 v0, -0x1

    iput v0, p0, LX/K0E;->c:I

    .line 2758057
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 2758052
    iget v0, p0, LX/K0E;->b:I

    invoke-virtual {p0, v0}, LX/3tW;->c(I)V

    .line 2758053
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2758058
    iget v0, p0, LX/K0E;->b:I

    invoke-virtual {p0, v0}, LX/3tW;->d(I)V

    .line 2758059
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 2758049
    iput p1, p0, LX/K0E;->b:I

    .line 2758050
    invoke-virtual {p0}, LX/K0E;->f()V

    .line 2758051
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2758041
    invoke-virtual {p0}, LX/K0E;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2758042
    invoke-virtual {p0, v3}, LX/K0E;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2758043
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 2758044
    iget v2, p0, LX/K0E;->b:I

    iput v2, v0, LX/3tS;->a:I

    .line 2758045
    iget v2, p0, LX/K0E;->c:I

    iput v2, v0, LX/3tS;->width:I

    .line 2758046
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2758047
    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 2758048
    :cond_0
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 2758038
    iput p1, p0, LX/K0E;->c:I

    .line 2758039
    invoke-virtual {p0}, LX/K0E;->f()V

    .line 2758040
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2758034
    invoke-super {p0, p1}, LX/3tW;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2758035
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2758036
    const/4 v0, 0x1

    .line 2758037
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
