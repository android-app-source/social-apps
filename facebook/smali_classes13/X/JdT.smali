.class public final LX/JdT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;)V
    .locals 0

    .prologue
    .line 2719587
    iput-object p1, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 2719588
    iget-object v0, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719589
    iget-object v0, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->m:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v1, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    .line 2719590
    iget-object v2, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2719591
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->i()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2719592
    sget-object v2, LX/FMK;->SMS_THREAD_COMPOSER:LX/FMK;

    .line 2719593
    iget-object p0, v0, Lcom/facebook/messaging/blocking/BlockingUtils;->l:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Lcom/facebook/messaging/blocking/BlockingUtils$5;

    invoke-direct {p1, v0, v1, v2}, Lcom/facebook/messaging/blocking/BlockingUtils$5;-><init>(Lcom/facebook/messaging/blocking/BlockingUtils;Ljava/lang/String;LX/FMK;)V

    const p2, 0x6c0a30ce

    invoke-static {p0, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2719594
    :goto_0
    return-void

    .line 2719595
    :cond_0
    iget-object v0, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->m:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v1, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    .line 2719596
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2719597
    iget-object v2, p0, LX/JdT;->a:Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->p:LX/2h0;

    .line 2719598
    const/4 v3, 0x1

    .line 2719599
    invoke-static {v0, v1, v3}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/messaging/blocking/BlockingUtils;Ljava/lang/String;Z)LX/1MF;

    move-result-object p0

    .line 2719600
    if-eqz v3, :cond_1

    .line 2719601
    iget-object v4, v0, Lcom/facebook/messaging/blocking/BlockingUtils;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f082b3a    # 1.8099945E38f

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2719602
    :goto_1
    new-instance p1, LX/4At;

    iget-object p2, v0, Lcom/facebook/messaging/blocking/BlockingUtils;->e:Landroid/content/Context;

    invoke-direct {p1, p2, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {p0, p1}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    .line 2719603
    invoke-interface {p0}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2719604
    iget-object p0, v0, Lcom/facebook/messaging/blocking/BlockingUtils;->b:LX/0TD;

    invoke-static {v4, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2719605
    goto :goto_0

    .line 2719606
    :cond_1
    iget-object v4, v0, Lcom/facebook/messaging/blocking/BlockingUtils;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f082b3b    # 1.8099947E38f

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method
