.class public final LX/Jo0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2734779
    iput-object p1, p0, LX/Jo0;->a:Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734780
    iput-object p2, p0, LX/Jo0;->b:Ljava/lang/String;

    .line 2734781
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2734782
    iget-object v1, p0, LX/Jo0;->a:Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    monitor-enter v1

    .line 2734783
    :try_start_0
    iget-object v0, p0, LX/Jo0;->a:Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b:Ljava/util/Map;

    iget-object v2, p0, LX/Jo0;->b:Ljava/lang/String;

    sget-object v3, LX/Jnw;->NOT_STARTED:LX/Jnw;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734784
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734785
    iget-object v1, p0, LX/Jo0;->a:Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    monitor-enter v1

    .line 2734786
    :try_start_0
    iget-object v0, p0, LX/Jo0;->a:Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b:Ljava/util/Map;

    iget-object v2, p0, LX/Jo0;->b:Ljava/lang/String;

    sget-object v3, LX/Jnw;->COMPLETED:LX/Jnw;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734787
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
