.class public LX/Ji2;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/FJw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/DAY;

.field public c:Lcom/facebook/user/tiles/UserTileView;

.field public d:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

.field public e:Landroid/widget/TextView;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725687
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725688
    const-class v0, LX/Ji2;

    invoke-static {v0, p0}, LX/Ji2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725689
    const v0, 0x7f030ce9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725690
    const v0, 0x7f0d1e99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Ji2;->c:Lcom/facebook/user/tiles/UserTileView;

    .line 2725691
    const v0, 0x7f0d1ab7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iput-object v0, p0, LX/Ji2;->d:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2725692
    iget-object v0, p0, LX/Ji2;->d:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v0}, LX/2Wj;->getTextColor()I

    move-result v0

    iput v0, p0, LX/Ji2;->f:I

    .line 2725693
    const v0, 0x7f0d1913

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Ji2;->e:Landroid/widget/TextView;

    .line 2725694
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ji2;

    invoke-static {p0}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object p0

    check-cast p0, LX/FJw;

    iput-object p0, p1, LX/Ji2;->a:LX/FJw;

    return-void
.end method


# virtual methods
.method public getContactRow()LX/DAY;
    .locals 1

    .prologue
    .line 2725686
    iget-object v0, p0, LX/Ji2;->b:LX/DAY;

    return-object v0
.end method
