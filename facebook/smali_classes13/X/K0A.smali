.class public LX/K0A;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "WebSocketModule"
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/66l;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/5pX;

.field private c:LX/9nC;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2757503
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2757504
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/K0A;->a:Ljava/util/Map;

    .line 2757505
    iput-object p1, p0, LX/K0A;->b:LX/5pX;

    .line 2757506
    new-instance v0, LX/9nC;

    invoke-direct {v0, p1}, LX/9nC;-><init>(LX/5pX;)V

    iput-object v0, p0, LX/K0A;->c:LX/9nC;

    .line 2757507
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2757492
    :try_start_0
    const-string v0, ""

    .line 2757493
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 2757494
    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "wss"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2757495
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2757496
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/net/URI;->getPort()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 2757497
    const-string v2, "%s://%s:%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {v1}, Ljava/net/URI;->getPort()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2757498
    :goto_1
    return-object v0

    .line 2757499
    :cond_1
    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ws"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2757500
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2757501
    :cond_2
    const-string v2, "%s://%s/"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 2757502
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to set "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as default origin header"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a$redex0(LX/K0A;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2757487
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2757488
    const-string v1, "id"

    invoke-interface {v0, v1, p1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2757489
    const-string v1, "message"

    invoke-interface {v0, v1, p2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757490
    const-string v1, "websocketFailed"

    invoke-static {p0, v1, v0}, LX/K0A;->a$redex0(LX/K0A;Ljava/lang/String;LX/5pH;)V

    .line 2757491
    return-void
.end method

.method public static a$redex0(LX/K0A;Ljava/lang/String;LX/5pH;)V
    .locals 2

    .prologue
    .line 2757485
    iget-object v0, p0, LX/K0A;->b:LX/5pX;

    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2757486
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2757478
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-static {p1}, LX/K0A;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 2757479
    iget-object v1, p0, LX/K0A;->c:LX/9nC;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, v0, v2}, LX/9nC;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 2757480
    const-string v1, "Cookie"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2757481
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2757482
    :cond_0
    const/4 v0, 0x0

    .line 2757483
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2757484
    :catch_0
    :goto_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to get cookie from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public close(ILjava/lang/String;I)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757508
    iget-object v0, p0, LX/K0A;->a:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/66l;

    .line 2757509
    if-nez v0, :cond_0

    .line 2757510
    :goto_0
    return-void

    .line 2757511
    :cond_0
    :try_start_0
    invoke-interface {v0, p1, p2}, LX/66l;->a(ILjava/lang/String;)V

    .line 2757512
    iget-object v0, p0, LX/K0A;->a:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2757513
    :catch_0
    move-exception v0

    .line 2757514
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not close WebSocket connection for id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public connect(Ljava/lang/String;LX/5pC;LX/5pG;I)V
    .locals 7
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const-wide/16 v2, 0xa

    .line 2757440
    new-instance v0, LX/64v;

    invoke-direct {v0}, LX/64v;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/64v;->a(JLjava/util/concurrent/TimeUnit;)LX/64v;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/64v;->c(JLjava/util/concurrent/TimeUnit;)LX/64v;

    move-result-object v0

    const-wide/16 v2, 0x0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/64v;->b(JLjava/util/concurrent/TimeUnit;)LX/64v;

    move-result-object v0

    invoke-virtual {v0}, LX/64v;->a()LX/64w;

    move-result-object v1

    .line 2757441
    new-instance v0, LX/64z;

    invoke-direct {v0}, LX/64z;-><init>()V

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2757442
    iput-object v2, v0, LX/64z;->e:Ljava/lang/Object;

    .line 2757443
    move-object v0, v0

    .line 2757444
    invoke-virtual {v0, p1}, LX/64z;->a(Ljava/lang/String;)LX/64z;

    move-result-object v2

    .line 2757445
    invoke-direct {p0, p1}, LX/K0A;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2757446
    if-eqz v0, :cond_0

    .line 2757447
    const-string v3, "Cookie"

    invoke-virtual {v2, v3, v0}, LX/64z;->b(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 2757448
    :cond_0
    if-eqz p3, :cond_3

    .line 2757449
    invoke-interface {p3}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v0

    .line 2757450
    const-string v3, "origin"

    invoke-interface {p3, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2757451
    const-string v3, "origin"

    invoke-static {p1}, LX/K0A;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/64z;->b(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 2757452
    :cond_1
    :goto_0
    invoke-interface {v0}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2757453
    invoke-interface {v0}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v3

    .line 2757454
    sget-object v4, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    invoke-interface {p3, v3}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/react/bridge/ReadableType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2757455
    invoke-interface {p3, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/64z;->b(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    goto :goto_0

    .line 2757456
    :cond_2
    const-string v4, "React"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Ignoring: requested "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", value not a string"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2757457
    :cond_3
    const-string v0, "origin"

    invoke-static {p1}, LX/K0A;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/64z;->b(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 2757458
    :cond_4
    if-eqz p2, :cond_7

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 2757459
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2757460
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v4

    if-ge v0, v4, :cond_6

    .line 2757461
    invoke-interface {p2, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 2757462
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2757463
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2757464
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2757465
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2757466
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 2757467
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    const-string v5, ""

    invoke-virtual {v3, v0, v4, v5}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2757468
    const-string v0, "Sec-WebSocket-Protocol"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/64z;->b(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 2757469
    :cond_7
    invoke-virtual {v2}, LX/64z;->b()LX/650;

    move-result-object v0

    .line 2757470
    new-instance v2, LX/66v;

    invoke-direct {v2, v1, v0}, LX/66v;-><init>(LX/64w;LX/650;)V

    move-object v0, v2

    .line 2757471
    new-instance v2, LX/K09;

    invoke-direct {v2, p0, p4}, LX/K09;-><init>(LX/K0A;I)V

    .line 2757472
    new-instance v3, LX/66t;

    invoke-direct {v3, v0, v2}, LX/66t;-><init>(LX/66v;LX/K09;)V

    .line 2757473
    sget-object v4, LX/64t;->a:LX/64t;

    iget-object v5, v0, LX/66v;->a:LX/64y;

    invoke-virtual {v4, v5}, LX/64t;->b(LX/64y;)V

    .line 2757474
    iget-object v4, v0, LX/66v;->a:LX/64y;

    invoke-virtual {v4, v3}, LX/64y;->a(LX/64X;)V

    .line 2757475
    iget-object v0, v1, LX/64w;->a:LX/64i;

    move-object v0, v0

    .line 2757476
    invoke-virtual {v0}, LX/64i;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 2757477
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757439
    const-string v0, "WebSocketModule"

    return-object v0
.end method

.method public ping(I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757430
    iget-object v0, p0, LX/K0A;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/66l;

    .line 2757431
    if-nez v0, :cond_0

    .line 2757432
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot send a message. Unknown WebSocket id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757433
    :cond_0
    :try_start_0
    new-instance v1, LX/672;

    invoke-direct {v1}, LX/672;-><init>()V

    .line 2757434
    invoke-interface {v0, v1}, LX/66l;->a(LX/672;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2757435
    :goto_0
    return-void

    .line 2757436
    :catch_0
    move-exception v0

    .line 2757437
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/K0A;->a$redex0(LX/K0A;ILjava/lang/String;)V

    goto :goto_0

    .line 2757438
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public send(Ljava/lang/String;I)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757412
    iget-object v0, p0, LX/K0A;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/66l;

    .line 2757413
    if-nez v0, :cond_0

    .line 2757414
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot send a message. Unknown WebSocket id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757415
    :cond_0
    :try_start_0
    sget-object v1, LX/66l;->a:LX/64s;

    .line 2757416
    sget-object v2, LX/65A;->c:Ljava/nio/charset/Charset;

    .line 2757417
    if-eqz v1, :cond_1

    .line 2757418
    iget-object v2, v1, LX/64s;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/64s;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    :goto_0
    move-object v2, v2

    .line 2757419
    if-nez v2, :cond_1

    .line 2757420
    sget-object v2, LX/65A;->c:Ljava/nio/charset/Charset;

    .line 2757421
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; charset=utf-8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/64s;->a(Ljava/lang/String;)LX/64s;

    move-result-object v1

    .line 2757422
    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 2757423
    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v1, v2, v3, v4}, LX/651;->a(LX/64s;[BII)LX/651;

    move-result-object v3

    move-object v2, v3

    .line 2757424
    move-object v1, v2

    .line 2757425
    invoke-interface {v0, v1}, LX/66l;->a(LX/651;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2757426
    :goto_1
    return-void

    .line 2757427
    :catch_0
    move-exception v0

    .line 2757428
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, LX/K0A;->a$redex0(LX/K0A;ILjava/lang/String;)V

    goto :goto_1

    .line 2757429
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public sendBinary(Ljava/lang/String;I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757402
    iget-object v0, p0, LX/K0A;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/66l;

    .line 2757403
    if-nez v0, :cond_0

    .line 2757404
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot send a message. Unknown WebSocket id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757405
    :cond_0
    :try_start_0
    sget-object v1, LX/66l;->b:LX/64s;

    invoke-static {p1}, LX/673;->b(Ljava/lang/String;)LX/673;

    move-result-object v2

    .line 2757406
    new-instance p1, LX/652;

    invoke-direct {p1, v1, v2}, LX/652;-><init>(LX/64s;LX/673;)V

    move-object v1, p1

    .line 2757407
    invoke-interface {v0, v1}, LX/66l;->a(LX/651;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2757408
    :goto_0
    return-void

    .line 2757409
    :catch_0
    move-exception v0

    .line 2757410
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, LX/K0A;->a$redex0(LX/K0A;ILjava/lang/String;)V

    goto :goto_0

    .line 2757411
    :catch_1
    move-exception v0

    goto :goto_1
.end method
