.class public final LX/K3O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;)V
    .locals 0

    .prologue
    .line 2765643
    iput-object p1, p0, LX/K3O;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x6fc6aa19

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2765644
    iget-object v0, p0, LX/K3O;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2765645
    iget-object v0, p0, LX/K3O;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2765646
    :try_start_0
    iget-object v0, p0, LX/K3O;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2765647
    invoke-static {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V

    .line 2765648
    iget-object v0, p0, LX/K3O;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2765649
    :goto_0
    const v0, 0x5e48c761

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2765650
    :catch_0
    move-exception v0

    .line 2765651
    const-class v2, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    const-string v3, "Media player cannot prepare audio"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2765652
    :cond_0
    iget-object v0, p0, LX/K3O;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e()V

    goto :goto_0
.end method
