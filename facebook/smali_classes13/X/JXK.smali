.class public LX/JXK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2704333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2704334
    return-void
.end method

.method public static a(LX/0Px;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2704326
    const/4 v1, -0x1

    .line 2704327
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 2704328
    invoke-static {v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)LX/0Px;

    move-result-object v0

    .line 2704329
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 2704330
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2704331
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2704332
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2704300
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    .line 2704301
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2704302
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v0

    .line 2704303
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2704304
    return-object v0
.end method

.method public static a(LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2704317
    sget-object v0, LX/JXJ;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v1, v3

    .line 2704318
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return-object v3

    .line 2704319
    :pswitch_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v1

    goto :goto_0

    .line 2704320
    :pswitch_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v4

    .line 2704321
    const/4 v0, 0x0

    move v2, v0

    move-object v1, v3

    :goto_2
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 2704322
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/JXB;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2704323
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 2704324
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_2

    .line 2704325
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v3

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .locals 5

    .prologue
    .line 2704311
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)LX/0Px;

    move-result-object v2

    .line 2704312
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 2704313
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2704314
    return-object v0

    .line 2704315
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2704316
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "questionId not in unit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z
    .locals 1

    .prologue
    .line 2704305
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    .line 2704306
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2704307
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 2704308
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2704309
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 2704310
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->j()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
