.class public final LX/Jdi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/Jdl;


# direct methods
.method public constructor <init>(LX/Jdl;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2719912
    iput-object p1, p0, LX/Jdi;->b:LX/Jdl;

    iput-object p2, p0, LX/Jdi;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x43c96098

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719913
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2719914
    sget-object v2, LX/Jdl;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2719915
    iget-object v2, p0, LX/Jdi;->b:LX/Jdl;

    iget-object v2, v2, LX/Jdl;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Jdi;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2719916
    iget-object v1, p0, LX/Jdi;->b:LX/Jdl;

    iget-object v1, v1, LX/Jdl;->k:LX/JdU;

    iget-object v2, p0, LX/Jdi;->b:LX/Jdl;

    iget-object v2, v2, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719917
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2719918
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "message_block_tap_unblock_on_facebook"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2719919
    invoke-static {v1, v3, v2}, LX/JdU;->a(LX/JdU;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2719920
    const v1, 0x490e807d

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
