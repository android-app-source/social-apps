.class public LX/JtE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static volatile g:LX/JtE;


# instance fields
.field public final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/JtU;

.field public final d:LX/2wX;

.field public final e:LX/0SG;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746395
    const-class v0, LX/JtE;

    sput-object v0, LX/JtE;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/JtU;LX/JtZ;LX/0SG;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2746396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746397
    iput-object p4, p0, LX/JtE;->e:LX/0SG;

    .line 2746398
    invoke-virtual {p3}, LX/JtZ;->a()LX/2wX;

    move-result-object v0

    iput-object v0, p0, LX/JtE;->d:LX/2wX;

    .line 2746399
    iput-object p1, p0, LX/JtE;->b:Ljava/util/concurrent/ExecutorService;

    .line 2746400
    iput-object p2, p0, LX/JtE;->c:LX/JtU;

    .line 2746401
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/JtE;->f:Ljava/util/Map;

    .line 2746402
    return-void
.end method

.method public static a(LX/0QB;)LX/JtE;
    .locals 7

    .prologue
    .line 2746403
    sget-object v0, LX/JtE;->g:LX/JtE;

    if-nez v0, :cond_1

    .line 2746404
    const-class v1, LX/JtE;

    monitor-enter v1

    .line 2746405
    :try_start_0
    sget-object v0, LX/JtE;->g:LX/JtE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2746406
    if-eqz v2, :cond_0

    .line 2746407
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2746408
    new-instance p0, LX/JtE;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/JtU;->a(LX/0QB;)LX/JtU;

    move-result-object v4

    check-cast v4, LX/JtU;

    invoke-static {v0}, LX/JtZ;->a(LX/0QB;)LX/JtZ;

    move-result-object v5

    check-cast v5, LX/JtZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/JtE;-><init>(Ljava/util/concurrent/ExecutorService;LX/JtU;LX/JtZ;LX/0SG;)V

    .line 2746409
    move-object v0, p0

    .line 2746410
    sput-object v0, LX/JtE;->g:LX/JtE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2746411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2746412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2746413
    :cond_1
    sget-object v0, LX/JtE;->g:LX/JtE;

    return-object v0

    .line 2746414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2746415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[BLX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;",
            "[B",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/attachments/ImageAttachmentData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2746416
    iget-object v0, p0, LX/JtE;->f:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->d:Lcom/facebook/messengerwear/shared/Message;

    iget-wide v2, v2, Lcom/facebook/messengerwear/shared/Message;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2746417
    iget-object v0, p1, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/shared/Message;

    .line 2746418
    invoke-virtual {v0}, Lcom/facebook/messengerwear/shared/Message;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2746419
    const/4 v0, 0x1

    .line 2746420
    :goto_0
    move v0, v0

    .line 2746421
    if-nez v0, :cond_1

    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2746422
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2746423
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2746424
    iget-object v0, p1, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/shared/Message;

    .line 2746425
    invoke-virtual {v0}, Lcom/facebook/messengerwear/shared/Message;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2746426
    iget-object v0, v0, Lcom/facebook/messengerwear/shared/Message;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2746427
    :cond_3
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2746428
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2746429
    invoke-static {v0}, LX/JtS;->a(Ljava/lang/String;)LX/JtS;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2746430
    :cond_4
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_5

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 2746431
    new-instance v4, LX/JtS;

    iget-object v5, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    sget-object v6, LX/Jt0;->IMAGE:LX/Jt0;

    invoke-direct {v4, v0, v5, v6}, LX/JtS;-><init>(Lcom/facebook/messaging/attachments/ImageAttachmentData;Ljava/lang/String;LX/Jt0;)V

    move-object v0, v4

    .line 2746432
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2746433
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2746434
    :cond_5
    iget-object v0, p0, LX/JtE;->c:LX/JtU;

    invoke-virtual {v0, v2}, LX/JtU;->a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2746435
    new-instance v1, LX/JtD;

    invoke-direct {v1, p0, p1, p2}, LX/JtD;-><init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[B)V

    iget-object v2, p0, LX/JtE;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2746436
    :goto_4
    return-void

    .line 2746437
    :cond_6
    iget-object v0, p0, LX/JtE;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;-><init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[B)V

    const v2, -0x576f3209

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
