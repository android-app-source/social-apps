.class public final LX/JXA;
.super LX/37T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

.field private final b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

.field private final c:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V
    .locals 2

    .prologue
    .line 2704063
    iput-object p1, p0, LX/JXA;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    invoke-direct {p0}, LX/37T;-><init>()V

    .line 2704064
    iput-object p2, p0, LX/JXA;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704065
    new-instance v0, LX/JX7;

    iget-object v1, p1, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->b:LX/1Uf;

    invoke-direct {v0, p2, v1}, LX/JX7;-><init>(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Uf;)V

    iput-object v0, p0, LX/JXA;->c:LX/1KL;

    .line 2704066
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 2704067
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2704068
    iget-object v0, p0, LX/JXA;->c:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2704061
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 2704062
    iget-object v0, p0, LX/JXA;->b:Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    return-object v0
.end method
