.class public LX/JyA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field private final c:[I


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2754241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754242
    iput-object p1, p0, LX/JyA;->a:LX/0Px;

    .line 2754243
    const/4 v0, 0x0

    .line 2754244
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    .line 2754245
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v2, v0

    .line 2754246
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2754247
    :cond_0
    move v0, v2

    .line 2754248
    iput v0, p0, LX/JyA;->b:I

    .line 2754249
    const/4 v0, 0x0

    .line 2754250
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    new-array v3, v1, [I

    move v1, v0

    move v2, v0

    .line 2754251
    :goto_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2754252
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;

    .line 2754253
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoverySectionFieldsModel$BucketsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v2, v0

    .line 2754254
    aput v2, v3, v1

    .line 2754255
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2754256
    :cond_1
    move-object v0, v3

    .line 2754257
    iput-object v0, p0, LX/JyA;->c:[I

    .line 2754258
    return-void
.end method

.method public static a(LX/JyA;II)I
    .locals 2

    .prologue
    .line 2754259
    if-nez p2, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, LX/JyA;->c:[I

    add-int/lit8 v1, p2, -0x1

    aget v0, v0, v1

    sub-int/2addr p1, v0

    goto :goto_0
.end method

.method public static d(LX/JyA;I)I
    .locals 3

    .prologue
    .line 2754260
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/JyA;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2754261
    iget-object v1, p0, LX/JyA;->c:[I

    aget v1, v1, v0

    if-le v1, p1, :cond_0

    .line 2754262
    return v0

    .line 2754263
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2754264
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received invalid position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
