.class public LX/Jt6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1qf;",
        "LX/1qg;",
        "LX/27U",
        "<",
        "LX/KAf;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final g:Ljava/lang/Class;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JtX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Ri;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/2wX;

.field public i:LX/0a8;

.field private final j:LX/0aB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746312
    const-class v0, LX/Jt6;

    sput-object v0, LX/Jt6;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2746313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746314
    new-instance v0, LX/Jt5;

    invoke-direct {v0, p0}, LX/Jt5;-><init>(LX/Jt6;)V

    iput-object v0, p0, LX/Jt6;->j:LX/0aB;

    .line 2746315
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 2746316
    return-void
.end method

.method public final a(LX/2NW;)V
    .locals 5

    .prologue
    .line 2746317
    check-cast p1, LX/KAf;

    .line 2746318
    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2746319
    sget-object v0, LX/Jt6;->g:Ljava/lang/Class;

    const-string v1, "Capability API call failed: %d - %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v4

    iget p0, v4, Lcom/google/android/gms/common/api/Status;->i:I

    move v4, p0

    .line 2746320
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v4

    iget-object p0, v4, Lcom/google/android/gms/common/api/Status;->j:Ljava/lang/String;

    move-object v4, p0

    .line 2746321
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2746322
    :goto_0
    return-void

    .line 2746323
    :cond_0
    iget-object v0, p0, LX/Jt6;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ri;

    .line 2746324
    invoke-interface {p1}, LX/KAf;->b()LX/KAh;

    move-result-object v1

    .line 2746325
    const-string v2, "messenger_wear"

    invoke-interface {v1}, LX/KAh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2746326
    :goto_1
    invoke-virtual {v0, v1}, LX/3Ri;->a(LX/KAh;)V

    .line 2746327
    invoke-virtual {v0}, LX/3Ri;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2746328
    iget-object v0, p0, LX/Jt6;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JtX;

    .line 2746329
    invoke-virtual {v0}, LX/JtX;->a()V

    .line 2746330
    :cond_1
    iget-object v0, p0, LX/Jt6;->h:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    goto :goto_0

    .line 2746331
    :cond_2
    invoke-interface {v1}, LX/KAh;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    .line 2746332
    iget-object v2, v0, LX/3Ri;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/JtV;->c:LX/0Tn;

    invoke-interface {v1}, LX/KAh;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-interface {v3, v4, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2746333
    sget-object v0, LX/KB1;->b:LX/KAg;

    iget-object v1, p0, LX/Jt6;->h:LX/2wX;

    const-string v2, "messenger_wear"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/KAg;->a(LX/2wX;Ljava/lang/String;I)LX/2wg;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/2wg;->a(LX/27U;)V

    .line 2746334
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0

    .prologue
    .line 2746335
    return-void
.end method
