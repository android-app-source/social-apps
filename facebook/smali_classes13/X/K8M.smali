.class public LX/K8M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/K8M;


# instance fields
.field public final a:LX/8Yg;

.field public final b:LX/K7m;

.field public final c:LX/0TD;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/8Ya;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/K8L;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8Yg;LX/K7m;LX/0TD;)V
    .locals 1
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2773798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773799
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/K8M;->d:Ljava/util/Map;

    .line 2773800
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/K8M;->e:Ljava/util/Map;

    .line 2773801
    iput-object p1, p0, LX/K8M;->a:LX/8Yg;

    .line 2773802
    iput-object p2, p0, LX/K8M;->b:LX/K7m;

    .line 2773803
    iput-object p3, p0, LX/K8M;->c:LX/0TD;

    .line 2773804
    return-void
.end method

.method public static a(LX/0QB;)LX/K8M;
    .locals 6

    .prologue
    .line 2773805
    sget-object v0, LX/K8M;->f:LX/K8M;

    if-nez v0, :cond_1

    .line 2773806
    const-class v1, LX/K8M;

    monitor-enter v1

    .line 2773807
    :try_start_0
    sget-object v0, LX/K8M;->f:LX/K8M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2773808
    if-eqz v2, :cond_0

    .line 2773809
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2773810
    new-instance p0, LX/K8M;

    invoke-static {v0}, LX/8Yg;->a(LX/0QB;)LX/8Yg;

    move-result-object v3

    check-cast v3, LX/8Yg;

    invoke-static {v0}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v4

    check-cast v4, LX/K7m;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-direct {p0, v3, v4, v5}, LX/K8M;-><init>(LX/8Yg;LX/K7m;LX/0TD;)V

    .line 2773811
    move-object v0, p0

    .line 2773812
    sput-object v0, LX/K8M;->f:LX/K8M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2773813
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2773814
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2773815
    :cond_1
    sget-object v0, LX/K8M;->f:LX/K8M;

    return-object v0

    .line 2773816
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2773817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Pz;LX/8GE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/8GE;",
            ">;",
            "LX/8GE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2773818
    if-eqz p1, :cond_0

    .line 2773819
    invoke-virtual {p0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2773820
    :cond_0
    return-void
.end method
