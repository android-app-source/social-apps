.class public LX/JqE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Ien;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/Ien;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738581
    iput-object p1, p0, LX/JqE;->a:LX/Ien;

    .line 2738582
    iput-object p2, p0, LX/JqE;->b:Landroid/content/res/Resources;

    .line 2738583
    return-void
.end method

.method public static b(LX/0QB;)LX/JqE;
    .locals 3

    .prologue
    .line 2738572
    new-instance v2, LX/JqE;

    invoke-static {p0}, LX/Ien;->b(LX/0QB;)LX/Ien;

    move-result-object v0

    check-cast v0, LX/Ien;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v1}, LX/JqE;-><init>(LX/Ien;Landroid/content/res/Resources;)V

    .line 2738573
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2738574
    if-nez p3, :cond_0

    .line 2738575
    iget-object v0, p0, LX/JqE;->b:Landroid/content/res/Resources;

    const v1, 0x7f082e9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object p3, v0

    .line 2738576
    :cond_0
    new-instance v0, LX/47x;

    iget-object v1, p0, LX/JqE;->b:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2738577
    invoke-virtual {v0, p1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2738578
    new-instance v1, LX/JqD;

    invoke-direct {v1, p0}, LX/JqD;-><init>(LX/JqE;)V

    const/16 v2, 0x21

    invoke-virtual {v0, p2, p3, v1, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2738579
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method
