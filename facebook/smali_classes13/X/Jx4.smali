.class public final LX/Jx4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/JxA;",
        "LX/0bZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2ct;

.field public final synthetic b:LX/JxB;


# direct methods
.method public constructor <init>(LX/JxB;LX/2ct;)V
    .locals 0

    .prologue
    .line 2752821
    iput-object p1, p0, LX/Jx4;->b:LX/JxB;

    iput-object p2, p0, LX/Jx4;->a:LX/2ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2752822
    check-cast p1, LX/JxA;

    .line 2752823
    if-nez p1, :cond_0

    .line 2752824
    iget-object v0, p0, LX/Jx4;->a:LX/2ct;

    sget-object v1, LX/2cx;->BLE:LX/2cx;

    invoke-virtual {v0, v1}, LX/2ct;->a(LX/2cx;)LX/0bZ;

    move-result-object v0

    .line 2752825
    :goto_0
    return-object v0

    .line 2752826
    :cond_0
    iget-object v0, p0, LX/Jx4;->b:LX/JxB;

    iget-object v0, v0, LX/JxB;->h:LX/0bW;

    const-string v1, "Updating with bestGuess from server"

    invoke-interface {v0, v1}, LX/0bW;->a(Ljava/lang/String;)V

    .line 2752827
    iget v0, p1, LX/JxA;->c:I

    invoke-static {v0}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a(I)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v0

    .line 2752828
    iget-object v1, p0, LX/Jx4;->a:LX/2ct;

    invoke-virtual {v1, v0}, LX/2ct;->a(Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/CeG;

    move-result-object v0

    iget-object v1, p1, LX/JxA;->a:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2752829
    iput-object v1, v0, LX/CeG;->c:Ljava/lang/String;

    .line 2752830
    move-object v0, v0

    .line 2752831
    iget-object v1, p1, LX/JxA;->a:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2752832
    iput-object v1, v0, LX/CeG;->d:Ljava/lang/String;

    .line 2752833
    move-object v0, v0

    .line 2752834
    iget-object v1, p1, LX/JxA;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    .line 2752835
    iput-object v1, v0, LX/CeG;->g:LX/175;

    .line 2752836
    move-object v0, v0

    .line 2752837
    iget-object v1, p1, LX/JxA;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    .line 2752838
    iput-object v1, v0, LX/CeG;->h:LX/175;

    .line 2752839
    move-object v0, v0

    .line 2752840
    iget-object v1, p1, LX/JxA;->d:LX/Cdj;

    .line 2752841
    iput-object v1, v0, LX/CeG;->p:LX/Cdj;

    .line 2752842
    move-object v0, v0

    .line 2752843
    invoke-virtual {v0}, LX/CeG;->a()LX/0bZ;

    move-result-object v0

    goto :goto_0
.end method
