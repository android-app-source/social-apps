.class public final enum LX/Jod;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jod;

.field public static final enum BOT_CANONICAL:LX/Jod;

.field public static final enum CANONICAL:LX/Jod;

.field public static final enum COMMERCE_CANONICAL:LX/Jod;

.field public static final enum GROUP:LX/Jod;

.field public static final enum SMS:LX/Jod;

.field public static final enum TINCAN:LX/Jod;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2735918
    new-instance v0, LX/Jod;

    const-string v1, "CANONICAL"

    invoke-direct {v0, v1, v3}, LX/Jod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jod;->CANONICAL:LX/Jod;

    .line 2735919
    new-instance v0, LX/Jod;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v4}, LX/Jod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jod;->GROUP:LX/Jod;

    .line 2735920
    new-instance v0, LX/Jod;

    const-string v1, "COMMERCE_CANONICAL"

    invoke-direct {v0, v1, v5}, LX/Jod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jod;->COMMERCE_CANONICAL:LX/Jod;

    .line 2735921
    new-instance v0, LX/Jod;

    const-string v1, "BOT_CANONICAL"

    invoke-direct {v0, v1, v6}, LX/Jod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jod;->BOT_CANONICAL:LX/Jod;

    .line 2735922
    new-instance v0, LX/Jod;

    const-string v1, "TINCAN"

    invoke-direct {v0, v1, v7}, LX/Jod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jod;->TINCAN:LX/Jod;

    .line 2735923
    new-instance v0, LX/Jod;

    const-string v1, "SMS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Jod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jod;->SMS:LX/Jod;

    .line 2735924
    const/4 v0, 0x6

    new-array v0, v0, [LX/Jod;

    sget-object v1, LX/Jod;->CANONICAL:LX/Jod;

    aput-object v1, v0, v3

    sget-object v1, LX/Jod;->GROUP:LX/Jod;

    aput-object v1, v0, v4

    sget-object v1, LX/Jod;->COMMERCE_CANONICAL:LX/Jod;

    aput-object v1, v0, v5

    sget-object v1, LX/Jod;->BOT_CANONICAL:LX/Jod;

    aput-object v1, v0, v6

    sget-object v1, LX/Jod;->TINCAN:LX/Jod;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Jod;->SMS:LX/Jod;

    aput-object v2, v0, v1

    sput-object v0, LX/Jod;->$VALUES:[LX/Jod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2735925
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jod;
    .locals 1

    .prologue
    .line 2735926
    const-class v0, LX/Jod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jod;

    return-object v0
.end method

.method public static values()[LX/Jod;
    .locals 1

    .prologue
    .line 2735927
    sget-object v0, LX/Jod;->$VALUES:[LX/Jod;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jod;

    return-object v0
.end method
