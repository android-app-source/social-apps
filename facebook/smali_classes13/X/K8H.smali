.class public final LX/K8H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/os/Handler;

.field public final synthetic b:LX/K7m;

.field public final synthetic c:LX/K8I;


# direct methods
.method public constructor <init>(LX/K8I;Landroid/os/Handler;LX/K7m;)V
    .locals 0

    .prologue
    .line 2773758
    iput-object p1, p0, LX/K8H;->c:LX/K8I;

    iput-object p2, p0, LX/K8H;->a:Landroid/os/Handler;

    iput-object p3, p0, LX/K8H;->b:LX/K7m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2773759
    iget-object v0, p0, LX/K8H;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/tarot/fetcher/TarotSubscribedPagesAndSuggestionsFetcher$1$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/tarot/fetcher/TarotSubscribedPagesAndSuggestionsFetcher$1$1;-><init>(LX/K8H;Ljava/lang/Throwable;)V

    const v2, -0x48313585

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2773760
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2773761
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2773762
    iget-object v0, p0, LX/K8H;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/tarot/fetcher/TarotSubscribedPagesAndSuggestionsFetcher$1$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/tarot/fetcher/TarotSubscribedPagesAndSuggestionsFetcher$1$2;-><init>(LX/K8H;Lcom/facebook/graphql/executor/GraphQLResult;)V

    const v2, 0x444f137

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2773763
    return-void
.end method
