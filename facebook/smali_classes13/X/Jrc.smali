.class public LX/Jrc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Jrc;


# instance fields
.field private final b:LX/3Ec;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2744276
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "invalid"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "inbox"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "other"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "spam"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Jrc;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/3Ec;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2744277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2744278
    iput-object p1, p0, LX/Jrc;->b:LX/3Ec;

    .line 2744279
    return-void
.end method

.method public static a(I)LX/6ek;
    .locals 2

    .prologue
    .line 2744280
    sget-object v0, LX/Jrc;->a:LX/0P1;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Jrc;
    .locals 4

    .prologue
    .line 2744281
    sget-object v0, LX/Jrc;->c:LX/Jrc;

    if-nez v0, :cond_1

    .line 2744282
    const-class v1, LX/Jrc;

    monitor-enter v1

    .line 2744283
    :try_start_0
    sget-object v0, LX/Jrc;->c:LX/Jrc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2744284
    if-eqz v2, :cond_0

    .line 2744285
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2744286
    new-instance p0, LX/Jrc;

    invoke-static {v0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v3

    check-cast v3, LX/3Ec;

    invoke-direct {p0, v3}, LX/Jrc;-><init>(LX/3Ec;)V

    .line 2744287
    move-object v0, p0

    .line 2744288
    sput-object v0, LX/Jrc;->c:LX/Jrc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2744289
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2744290
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2744291
    :cond_1
    sget-object v0, LX/Jrc;->c:LX/Jrc;

    return-object v0

    .line 2744292
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2744293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2744294
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2744295
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2744296
    :goto_0
    return-object v0

    .line 2744297
    :cond_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2744298
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6l9;

    .line 2744299
    invoke-virtual {p0, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2744300
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4

    .prologue
    .line 2744301
    iget-object v0, p1, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2744302
    iget-object v0, p0, LX/Jrc;->b:LX/3Ec;

    iget-object v1, p1, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/3Ec;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2744303
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p1, LX/6l9;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2744304
    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    move-object v0, v1

    .line 2744305
    goto :goto_0
.end method
