.class public final LX/Jgl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jgm;


# direct methods
.method public constructor <init>(LX/Jgm;)V
    .locals 0

    .prologue
    .line 2723766
    iput-object p1, p0, LX/Jgl;->a:LX/Jgm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x58afc503

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2723767
    iget-object v1, p0, LX/Jgl;->a:LX/Jgm;

    iget-object v1, v1, LX/Jgm;->g:LX/Jgn;

    .line 2723768
    iget-object v3, v1, LX/Jgn;->b:LX/Jgw;

    if-eqz v3, :cond_0

    .line 2723769
    iget-object v3, v1, LX/Jgn;->b:LX/Jgw;

    iget-object p0, v1, LX/Jgn;->a:Lcom/facebook/user/model/User;

    .line 2723770
    iget-object p1, v3, LX/Jgw;->a:LX/Jgy;

    iget-object p1, p1, LX/Jgy;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-static {p1}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723771
    iget-object p1, v3, LX/Jgw;->a:LX/Jgy;

    iget-object p1, p1, LX/Jgy;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2723772
    iget-object v1, p1, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2723773
    iget-object v3, v1, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2723774
    invoke-static {p1}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->u(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723775
    invoke-static {p1}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->v(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2723776
    iget-object v1, p1, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    invoke-virtual {v1}, LX/Jgt;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2723777
    iget-object v3, p1, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->h:Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

    invoke-virtual {v3}, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->getFirstVisiblePosition()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2723778
    iget-object v3, p1, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->h:Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

    invoke-virtual {v3, v1}, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->smoothScrollToPosition(I)V

    .line 2723779
    :cond_0
    const v1, 0x3b87e668

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
