.class public LX/K02;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "NetInfo"
.end annotation


# instance fields
.field private final a:Landroid/net/ConnectivityManager;

.field private final b:LX/K01;

.field private c:Z

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2757205
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2757206
    iput-boolean v1, p0, LX/K02;->c:Z

    .line 2757207
    const-string v0, ""

    iput-object v0, p0, LX/K02;->d:Ljava/lang/String;

    .line 2757208
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, LX/5pY;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, LX/K02;->a:Landroid/net/ConnectivityManager;

    .line 2757209
    new-instance v0, LX/K01;

    invoke-direct {v0, p0}, LX/K01;-><init>(LX/K02;)V

    iput-object v0, p0, LX/K02;->b:LX/K01;

    .line 2757210
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2757198
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2757199
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2757200
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2757201
    iget-object v2, p0, LX/K02;->b:LX/K01;

    invoke-virtual {v1, v2, v0}, LX/5pY;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2757202
    iget-object v0, p0, LX/K02;->b:LX/K01;

    const/4 v1, 0x1

    .line 2757203
    iput-boolean v1, v0, LX/K01;->b:Z

    .line 2757204
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2757190
    iget-object v0, p0, LX/K02;->b:LX/K01;

    .line 2757191
    iget-boolean v1, v0, LX/K01;->b:Z

    move v0, v1

    .line 2757192
    if-eqz v0, :cond_0

    .line 2757193
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757194
    iget-object v1, p0, LX/K02;->b:LX/K01;

    invoke-virtual {v0, v1}, LX/5pY;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2757195
    iget-object v0, p0, LX/K02;->b:LX/K01;

    const/4 v1, 0x0

    .line 2757196
    iput-boolean v1, v0, LX/K01;->b:Z

    .line 2757197
    :cond_0
    return-void
.end method

.method public static u(LX/K02;)V
    .locals 2

    .prologue
    .line 2757185
    invoke-direct {p0}, LX/K02;->v()Ljava/lang/String;

    move-result-object v0

    .line 2757186
    iget-object v1, p0, LX/K02;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2757187
    iput-object v0, p0, LX/K02;->d:Ljava/lang/String;

    .line 2757188
    invoke-direct {p0}, LX/K02;->w()V

    .line 2757189
    :cond_0
    return-void
.end method

.method private v()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2757176
    :try_start_0
    iget-object v0, p0, LX/K02;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2757177
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2757178
    :cond_0
    const-string v0, "NONE"

    .line 2757179
    :goto_0
    return-object v0

    .line 2757180
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    invoke-static {v1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2757181
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2757182
    :cond_2
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 2757183
    :catch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K02;->c:Z

    .line 2757184
    const-string v0, "UNKNOWN"

    goto :goto_0
.end method

.method private w()V
    .locals 3

    .prologue
    .line 2757173
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757174
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "networkStatusDidChange"

    invoke-direct {p0}, LX/K02;->x()LX/5pH;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2757175
    return-void
.end method

.method private x()LX/5pH;
    .locals 3

    .prologue
    .line 2757170
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2757171
    const-string v1, "network_info"

    iget-object v2, p0, LX/K02;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757172
    return-object v0
.end method


# virtual methods
.method public final bM_()V
    .locals 0

    .prologue
    .line 2757153
    invoke-direct {p0}, LX/K02;->h()V

    .line 2757154
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2757168
    invoke-direct {p0}, LX/K02;->i()V

    .line 2757169
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2757167
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2757164
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2757165
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2757166
    return-void
.end method

.method public getCurrentConnectivity(LX/5pW;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757160
    iget-boolean v0, p0, LX/K02;->c:Z

    if-eqz v0, :cond_0

    .line 2757161
    const-string v0, "E_MISSING_PERMISSION"

    const-string v1, "To use NetInfo on Android, add the following to your AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />"

    invoke-interface {p1, v0, v1}, LX/5pW;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757162
    :goto_0
    return-void

    .line 2757163
    :cond_0
    invoke-direct {p0}, LX/K02;->x()LX/5pH;

    move-result-object v0

    invoke-interface {p1, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757159
    const-string v0, "NetInfo"

    return-object v0
.end method

.method public isConnectionMetered(LX/5pW;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2757155
    iget-boolean v0, p0, LX/K02;->c:Z

    if-eqz v0, :cond_0

    .line 2757156
    const-string v0, "E_MISSING_PERMISSION"

    const-string v1, "To use NetInfo on Android, add the following to your AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />"

    invoke-interface {p1, v0, v1}, LX/5pW;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757157
    :goto_0
    return-void

    .line 2757158
    :cond_0
    iget-object v0, p0, LX/K02;->a:Landroid/net/ConnectivityManager;

    invoke-static {v0}, LX/0kf;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
