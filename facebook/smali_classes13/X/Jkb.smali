.class public LX/Jkb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/JkP;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/FFR;

.field private final f:LX/03V;

.field private final g:LX/0aG;

.field private final h:LX/JkR;

.field private final i:LX/3QW;


# direct methods
.method public constructor <init>(LX/JkP;Landroid/content/res/Resources;LX/0Or;LX/FFR;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0aG;LX/JkR;LX/3QW;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JkP;",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/FFR;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0aG;",
            "LX/JkR;",
            "LX/3QW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2730119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730120
    iput-object p1, p0, LX/Jkb;->a:LX/JkP;

    .line 2730121
    iput-object p2, p0, LX/Jkb;->b:Landroid/content/res/Resources;

    .line 2730122
    iput-object p3, p0, LX/Jkb;->c:LX/0Or;

    .line 2730123
    iput-object p4, p0, LX/Jkb;->e:LX/FFR;

    .line 2730124
    iput-object p5, p0, LX/Jkb;->d:Ljava/util/concurrent/ExecutorService;

    .line 2730125
    iput-object p6, p0, LX/Jkb;->f:LX/03V;

    .line 2730126
    iput-object p7, p0, LX/Jkb;->g:LX/0aG;

    .line 2730127
    iput-object p8, p0, LX/Jkb;->h:LX/JkR;

    .line 2730128
    iput-object p9, p0, LX/Jkb;->i:LX/3QW;

    .line 2730129
    return-void
.end method
