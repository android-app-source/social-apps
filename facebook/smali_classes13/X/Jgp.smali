.class public LX/Jgp;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field public final b:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field public final c:Landroid/view/View;

.field public final d:Lcom/facebook/widget/tiles/ThreadTileView;

.field public final e:LX/3Ky;

.field public final f:LX/FJv;

.field public g:LX/Jgq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2723800
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/Jgp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2723801
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2723802
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2723803
    const v0, 0x7f030c76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2723804
    const v0, 0x7f0d0a22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/Jgp;->a:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2723805
    const v0, 0x7f0d1575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/Jgp;->b:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2723806
    const v0, 0x7f0d1e98    # 1.8758E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/Jgp;->d:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2723807
    const v0, 0x7f0d1e97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jgp;->c:Landroid/view/View;

    .line 2723808
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2723809
    invoke-static {v1}, LX/3Ky;->a(LX/0QB;)LX/3Ky;

    move-result-object v0

    check-cast v0, LX/3Ky;

    iput-object v0, p0, LX/Jgp;->e:LX/3Ky;

    .line 2723810
    invoke-static {v1}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v0

    check-cast v0, LX/FJv;

    iput-object v0, p0, LX/Jgp;->f:LX/FJv;

    .line 2723811
    return-void
.end method


# virtual methods
.method public getContactRow()LX/Jgq;
    .locals 1

    .prologue
    .line 2723812
    iget-object v0, p0, LX/Jgp;->g:LX/Jgq;

    return-object v0
.end method
