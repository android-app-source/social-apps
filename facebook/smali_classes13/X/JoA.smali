.class public final LX/JoA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V
    .locals 0

    .prologue
    .line 2735045
    iput-object p1, p0, LX/JoA;->a:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x4d1e5ea4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2735046
    const-string v0, "resource"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2735047
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/JoA;->a:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    iget-object v2, v2, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    sget-object v3, LX/JoM;->PENDING:LX/JoM;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LX/JoA;->a:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    iget-object v2, v2, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    if-nez v2, :cond_1

    .line 2735048
    :cond_0
    const/16 v0, 0x27

    const v2, 0x51ae032e

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2735049
    :goto_0
    return-void

    .line 2735050
    :cond_1
    invoke-static {v0}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    iget-object v2, p0, LX/JoA;->a:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    iget-object v2, v2, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v2}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2735051
    const v0, 0xb51c0fd

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 2735052
    :cond_2
    iget-object v0, p0, LX/JoA;->a:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    iget-object v0, v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->s:LX/JoD;

    invoke-interface {v0}, LX/JoD;->a()V

    .line 2735053
    const v0, -0x44d39402

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0
.end method
