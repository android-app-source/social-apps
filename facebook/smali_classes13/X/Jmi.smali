.class public LX/Jmi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2733221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)LX/Jme;
    .locals 22

    .prologue
    .line 2733222
    new-instance v18, Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)V

    .line 2733223
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2733224
    const/16 v16, 0x1

    .line 2733225
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v19

    .line 2733226
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->m()LX/0Px;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, LX/0Px;->size()I

    move-result v21

    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_0
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2733227
    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v3

    if-eqz v3, :cond_7

    .line 2733228
    invoke-static {v2}, LX/Jmi;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    move-result-object v9

    .line 2733229
    if-eqz v9, :cond_7

    .line 2733230
    invoke-virtual {v9}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->k()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    move-result-object v14

    .line 2733231
    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2733232
    :cond_0
    invoke-virtual {v9}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/JpA;->fromString(Ljava/lang/String;)LX/JpA;

    move-result-object v12

    .line 2733233
    const/4 v4, 0x0

    .line 2733234
    sget-object v3, LX/JpA;->FRIENDS_ARE_ADMINS:LX/JpA;

    if-eq v12, v3, :cond_1

    sget-object v3, LX/JpA;->SHARED_FROM_FRIENDS:LX/JpA;

    if-ne v12, v3, :cond_2

    .line 2733235
    :cond_1
    invoke-virtual {v9}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/FFT;->a(LX/0Px;)Landroid/util/Pair;

    move-result-object v4

    .line 2733236
    :cond_2
    if-eqz v4, :cond_3

    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2733237
    :cond_3
    sget-object v12, LX/JpA;->FRIEND_JOINED:LX/JpA;

    .line 2733238
    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->p()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    move-result-object v3

    invoke-static {v3}, LX/FFT;->a(Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;)Landroid/util/Pair;

    move-result-object v3

    move-object v11, v3

    .line 2733239
    :goto_1
    new-instance v13, Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v2}, Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2733240
    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2733241
    new-instance v3, LX/Jmn;

    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v14}, LX/Jmi;->a(Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v2

    invoke-static {v2}, LX/FFS;->a(Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;)I

    move-result v7

    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->n()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v2

    invoke-static {v2}, LX/FFT;->a(Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;)I

    move-result v8

    invoke-virtual {v9}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->l()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, LX/0Px;

    iget-object v11, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, LX/0Px;

    invoke-virtual {v14}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-direct/range {v3 .. v15}, LX/Jmn;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;IILjava/lang/String;LX/0Px;LX/0Px;LX/JpA;Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;J)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2733242
    add-int/lit8 v2, v16, 0x1

    .line 2733243
    :goto_2
    add-int/lit8 v3, v17, 0x1

    move/from16 v17, v3

    move/from16 v16, v2

    goto/16 :goto_0

    .line 2733244
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->p()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2733245
    new-instance v2, Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)V

    .line 2733246
    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(I)V

    .line 2733247
    :goto_3
    new-instance v3, LX/Jme;

    invoke-virtual/range {v19 .. v19}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v3, v4, v0, v2}, LX/Jme;-><init>(LX/0Px;Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;)V

    return-object v3

    .line 2733248
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    move-object v11, v4

    goto :goto_1

    :cond_7
    move/from16 v2, v16

    goto :goto_2
.end method

.method private static a(Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;)Landroid/net/Uri;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2733249
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    move-result-object v0

    .line 2733250
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2733251
    :cond_0
    const/4 v0, 0x0

    .line 2733252
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2733253
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v0

    .line 2733254
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->v()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    move-result-object v0

    goto :goto_0
.end method
