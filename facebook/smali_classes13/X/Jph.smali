.class public LX/Jph;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Jpg;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/JqE;


# direct methods
.method public constructor <init>(LX/Jpg;Landroid/content/res/Resources;LX/JqE;)V
    .locals 0
    .param p1    # LX/Jpg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2737906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737907
    iput-object p1, p0, LX/Jph;->a:LX/Jpg;

    .line 2737908
    iput-object p2, p0, LX/Jph;->b:Landroid/content/res/Resources;

    .line 2737909
    iput-object p3, p0, LX/Jph;->c:LX/JqE;

    .line 2737910
    return-void
.end method


# virtual methods
.method public final a(II)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2737911
    iget-object v0, p0, LX/Jph;->a:LX/Jpg;

    sget-object v1, LX/Jpg;->LOCAL:LX/Jpg;

    if-ne v0, v1, :cond_0

    .line 2737912
    iget-object v0, p0, LX/Jph;->b:Landroid/content/res/Resources;

    const v1, 0x7f082ea0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2737913
    :goto_0
    return-object v0

    .line 2737914
    :cond_0
    if-ne p1, p2, :cond_1

    .line 2737915
    iget-object v0, p0, LX/Jph;->b:Landroid/content/res/Resources;

    const v1, 0x7f0f0143

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2737916
    :cond_1
    iget-object v0, p0, LX/Jph;->b:Landroid/content/res/Resources;

    const v1, 0x7f0f0142

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(II)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2737917
    iget-object v0, p0, LX/Jph;->a:LX/Jpg;

    sget-object v1, LX/Jpg;->LOCAL:LX/Jpg;

    if-ne v0, v1, :cond_0

    .line 2737918
    iget-object v0, p0, LX/Jph;->b:Landroid/content/res/Resources;

    const v1, 0x7f082ea2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2737919
    :goto_0
    return-object v0

    .line 2737920
    :cond_0
    if-nez p1, :cond_1

    .line 2737921
    iget-object v0, p0, LX/Jph;->b:Landroid/content/res/Resources;

    const v1, 0x7f0f0145

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2737922
    :cond_1
    iget-object v0, p0, LX/Jph;->b:Landroid/content/res/Resources;

    const v1, 0x7f0f0146

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
