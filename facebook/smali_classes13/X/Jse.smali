.class public final LX/Jse;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

.field public final synthetic b:Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V
    .locals 0

    .prologue
    .line 2745623
    iput-object p1, p0, LX/Jse;->b:Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;

    iput-object p2, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x4ea67f0f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2745624
    iget-object v1, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iget-object v1, v1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->n:Landroid/net/Uri;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iget-object v1, v1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    if-eqz v1, :cond_1

    .line 2745625
    :cond_0
    new-instance v1, LX/6lk;

    const-string v2, "xma_action_cta_clicked"

    iget-object v3, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iget-object v3, v3, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iget-object v4, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iget-object v4, v4, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->o:Landroid/net/Uri;

    iget-object v5, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iget-object v5, v5, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->n:Landroid/net/Uri;

    iget-object v6, p0, LX/Jse;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iget-object v6, v6, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, LX/CJo;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/6lk;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2745626
    :cond_1
    const v1, -0x12862ec5

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
