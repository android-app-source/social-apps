.class public LX/Jsb;
.super LX/6mC;
.source ""


# instance fields
.field public b:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2745597
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Jsb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2745598
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745599
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Jsb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745600
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745593
    invoke-direct {p0, p1, p2, p3}, LX/6mC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745594
    const p1, 0x7f0315f9

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2745595
    const p1, 0x7f0d3174

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    iput-object p1, p0, LX/Jsb;->b:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    .line 2745596
    return-void
.end method


# virtual methods
.method public final a(LX/6ll;)V
    .locals 1
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745590
    invoke-super {p0, p1}, LX/6mC;->a(LX/6ll;)V

    .line 2745591
    iget-object v0, p0, LX/Jsb;->b:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->setXMACallback(LX/6ll;)V

    .line 2745592
    return-void
.end method
