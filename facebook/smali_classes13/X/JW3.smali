.class public LX/JW3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JW4;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JW3",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JW4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702317
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2702318
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JW3;->b:LX/0Zi;

    .line 2702319
    iput-object p1, p0, LX/JW3;->a:LX/0Ot;

    .line 2702320
    return-void
.end method

.method public static a(LX/0QB;)LX/JW3;
    .locals 4

    .prologue
    .line 2702296
    const-class v1, LX/JW3;

    monitor-enter v1

    .line 2702297
    :try_start_0
    sget-object v0, LX/JW3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702298
    sput-object v2, LX/JW3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702299
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702300
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702301
    new-instance v3, LX/JW3;

    const/16 p0, 0x20be

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JW3;-><init>(LX/0Ot;)V

    .line 2702302
    move-object v0, v3

    .line 2702303
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702304
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JW3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702305
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2702307
    check-cast p2, LX/JW2;

    .line 2702308
    iget-object v0, p0, LX/JW3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JW4;

    iget-object v1, p2, LX/JW2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x3

    const/4 p2, 0x0

    const/4 p0, 0x1

    .line 2702309
    const v0, 0x7f0e0180

    invoke-static {p1, p2, v0}, LX/1n8;->a(LX/1De;II)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b105a

    invoke-interface {v0, p0, v2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b1058

    invoke-interface {v0, v3, v2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b105b

    invoke-interface {v0, p0, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b105b

    invoke-interface {v0, v3, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f020799

    invoke-interface {v0, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    .line 2702310
    const v2, 0x196ed2ba

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2702311
    invoke-interface {v0, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    .line 2702312
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-static {v1}, LX/JVy;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v3, 0x7f0b0050

    invoke-virtual {v0, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    const v3, 0x7f0a09f4

    invoke-virtual {v0, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2702313
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v3, 0x7f020a7e

    invoke-interface {v0, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    sget-object v4, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v5, LX/0xr;->MEDIUM:LX/0xr;

    const/4 v6, 0x0

    invoke-static {p1, v4, v5, v6}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    .line 2702314
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702315
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v4, 0x7f0b0050

    invoke-virtual {v0, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v4, 0x7f0a00e1

    invoke-virtual {v0, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v4, 0x7f0b105c

    invoke-interface {v0, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v0, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2702316
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2702267
    invoke-static {}, LX/1dS;->b()V

    .line 2702268
    iget v0, p1, LX/1dQ;->b:I

    .line 2702269
    packed-switch v0, :pswitch_data_0

    .line 2702270
    :goto_0
    return-object v2

    .line 2702271
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2702272
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2702273
    check-cast v1, LX/JW2;

    .line 2702274
    iget-object v3, p0, LX/JW3;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JW4;

    iget-object v4, v1, LX/JW2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702275
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2702276
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2702277
    invoke-static {v4}, LX/JVy;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p1

    .line 2702278
    if-nez p1, :cond_0

    .line 2702279
    iget-object p1, v3, LX/JW4;->b:LX/JVx;

    const-string p2, "PYMA footer CTA click: can\'t handle NULL link"

    invoke-virtual {p1, v5, p2}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    .line 2702280
    :goto_1
    goto :goto_0

    .line 2702281
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object p1

    .line 2702282
    iget-object p2, v3, LX/JW4;->a:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p2, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    .line 2702283
    if-eqz p2, :cond_1

    .line 2702284
    iget-object p1, v3, LX/JW4;->b:LX/JVx;

    .line 2702285
    invoke-static {v5}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)LX/162;

    move-result-object p2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v5}, LX/4Zk;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, LX/4Zk;->b(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v3

    .line 2702286
    invoke-static {p2}, LX/17Q;->F(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2702287
    const/4 v0, 0x0

    .line 2702288
    :goto_2
    move-object p2, v0

    .line 2702289
    iget-object p0, p1, LX/JVx;->b:LX/0Zb;

    invoke-interface {p0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2702290
    goto :goto_1

    .line 2702291
    :cond_1
    iget-object p2, v3, LX/JW4;->b:LX/JVx;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string v1, "PYMA footer CTA click: can\'t handle URI: "

    invoke-direct {p0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v5, p1}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    goto :goto_1

    .line 2702292
    :cond_2
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "pyma_promote"

    invoke-direct {v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tracking"

    invoke-virtual {v0, v5, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v5, "pyma_category"

    invoke-virtual {v0, v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v5, "page_id"

    invoke-virtual {v0, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v5, "pyma_additional_info"

    invoke-virtual {v0, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v5, "native_newsfeed"

    .line 2702293
    iput-object v5, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2702294
    move-object v0, v0

    .line 2702295
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x196ed2ba
        :pswitch_0
    .end packed-switch
.end method
