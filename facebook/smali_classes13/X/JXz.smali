.class public final LX/JXz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/JY2;


# direct methods
.method public constructor <init>(LX/JY2;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 2705595
    iput-object p1, p0, LX/JXz;->c:LX/JY2;

    iput-object p2, p0, LX/JXz;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iput-object p3, p0, LX/JXz;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2705596
    iget-object v0, p0, LX/JXz;->c:LX/JY2;

    iget-object v0, v0, LX/JY2;->a:Ljava/util/Set;

    iget-object v1, p0, LX/JXz;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2705597
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2705598
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 2705599
    iget-object v0, p0, LX/JXz;->c:LX/JY2;

    iget-object v0, v0, LX/JY2;->a:Ljava/util/Set;

    iget-object v1, p0, LX/JXz;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2705600
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2705601
    :cond_0
    :goto_0
    return-void

    .line 2705602
    :cond_1
    iget-object v0, p0, LX/JXz;->c:LX/JY2;

    iget-object v0, v0, LX/JY2;->e:LX/189;

    iget-object v1, p0, LX/JXz;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 2705603
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2705604
    invoke-static {v1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;

    move-result-object v6

    invoke-virtual {v8, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2705605
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 2705606
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v6, 0x0

    move v7, v6

    :goto_1
    if-ge v7, v10, :cond_3

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 2705607
    invoke-static {v6}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2705608
    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2705609
    :cond_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 2705610
    :cond_3
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 2705611
    invoke-static {v1}, LX/4Yb;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/4Yb;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v8

    invoke-static {v8}, LX/4Yc;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;)LX/4Yc;

    move-result-object v8

    .line 2705612
    iput-object v6, v8, LX/4Yc;->b:LX/0Px;

    .line 2705613
    move-object v6, v8

    .line 2705614
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v8

    .line 2705615
    iput-object v8, v6, LX/4Yc;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2705616
    move-object v6, v6

    .line 2705617
    invoke-virtual {v6}, LX/4Yc;->a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v6

    .line 2705618
    iput-object v6, v7, LX/4Yb;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 2705619
    move-object v6, v7

    .line 2705620
    iget-object v7, v0, LX/189;->i:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    .line 2705621
    iput-wide v8, v6, LX/4Yb;->e:J

    .line 2705622
    move-object v6, v6

    .line 2705623
    invoke-virtual {v6}, LX/4Yb;->a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    move-result-object v6

    .line 2705624
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->I_()I

    move-result v7

    invoke-static {v6, v7}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2705625
    const/4 v7, 0x0

    invoke-static {v6, v7}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V

    .line 2705626
    move-object v0, v6

    .line 2705627
    if-eqz v0, :cond_0

    .line 2705628
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2705629
    iget-object v2, p0, LX/JXz;->c:LX/JY2;

    iget-object v2, v2, LX/JY2;->d:LX/03V;

    const-class v3, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/JXz;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705630
    iget-object v1, p0, LX/JXz;->c:LX/JY2;

    iget-object v1, v1, LX/JY2;->h:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2705631
    iget-object v1, p0, LX/JXz;->c:LX/JY2;

    iget-object v1, v1, LX/JY2;->h:LX/0bH;

    new-instance v2, LX/1Nf;

    invoke-direct {v2, v0}, LX/1Nf;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0
.end method
