.class public final LX/KA7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/wearlistener/WearNodeListener;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/wearlistener/WearNodeListener;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2778972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2778973
    iput-object p1, p0, LX/KA7;->a:LX/0QB;

    .line 2778974
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/wearlistener/WearNodeListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2778975
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/KA7;

    invoke-direct {v2, p0}, LX/KA7;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2778976
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/KA7;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2778977
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2778978
    const/4 v0, 0x0

    return v0
.end method
