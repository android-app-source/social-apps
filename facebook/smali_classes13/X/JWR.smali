.class public LX/JWR;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JWU;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWR",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JWU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702942
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2702943
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWR;->b:LX/0Zi;

    .line 2702944
    iput-object p1, p0, LX/JWR;->a:LX/0Ot;

    .line 2702945
    return-void
.end method

.method public static a(LX/0QB;)LX/JWR;
    .locals 4

    .prologue
    .line 2702946
    const-class v1, LX/JWR;

    monitor-enter v1

    .line 2702947
    :try_start_0
    sget-object v0, LX/JWR;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702948
    sput-object v2, LX/JWR;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702949
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702950
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702951
    new-instance v3, LX/JWR;

    const/16 p0, 0x20cc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWR;-><init>(LX/0Ot;)V

    .line 2702952
    move-object v0, v3

    .line 2702953
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702954
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702955
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702956
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 7

    .prologue
    .line 2702957
    check-cast p1, LX/JWQ;

    .line 2702958
    iget-object v0, p0, LX/JWR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWU;

    iget-object v1, p1, LX/JWQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p1, LX/JWQ;->c:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v3, p1, LX/JWQ;->a:LX/JWT;

    iget-object v4, p1, LX/JWQ;->d:LX/1Pc;

    iget-object v5, p1, LX/JWQ;->e:Ljava/lang/String;

    iget-object v6, p1, LX/JWQ;->f:LX/3mj;

    invoke-virtual/range {v0 .. v6}, LX/JWU;->onClick(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;LX/JWT;LX/1Pc;Ljava/lang/String;LX/3mj;)V

    .line 2702959
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2702960
    const v0, 0x5f617610

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2702961
    check-cast p2, LX/JWQ;

    .line 2702962
    iget-object v0, p0, LX/JWR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWU;

    iget-object v1, p2, LX/JWQ;->a:LX/JWT;

    const/4 p2, 0x0

    const/4 p0, 0x1

    .line 2702963
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    const v4, 0x7f0b0060

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    .line 2702964
    const v3, 0x5f617610

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2702965
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v4, v0, LX/JWU;->i:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    .line 2702966
    sget-object v5, LX/JWT;->ADD_FRIEND:LX/JWT;

    if-ne v1, v5, :cond_0

    .line 2702967
    const v5, 0x7f020b77

    .line 2702968
    :goto_0
    move v5, v5

    .line 2702969
    invoke-virtual {v4, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    const v5, 0x7f0a00a4

    invoke-virtual {v4, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    const v5, 0x7f0b0061

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    iget-object v4, v0, LX/JWU;->a:LX/23P;

    iget-object v5, v0, LX/JWU;->j:Landroid/content/res/Resources;

    .line 2702970
    sget-object v6, LX/JWT;->ADD_FRIEND:LX/JWT;

    if-ne v1, v6, :cond_2

    .line 2702971
    const v6, 0x7f080f7b

    .line 2702972
    :goto_1
    move v6, v6

    .line 2702973
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00a4

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object v4, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v5, LX/0xr;->MEDIUM:LX/0xr;

    invoke-static {p1, v4, v5, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2702974
    return-object v0

    .line 2702975
    :cond_0
    sget-object v5, LX/JWT;->REMOVE:LX/JWT;

    if-ne v1, v5, :cond_1

    .line 2702976
    const v5, 0x7f02081c

    goto :goto_0

    .line 2702977
    :cond_1
    const v5, 0x7f02089f

    goto :goto_0

    .line 2702978
    :cond_2
    sget-object v6, LX/JWT;->REMOVE:LX/JWT;

    if-ne v1, v6, :cond_3

    .line 2702979
    const v6, 0x7f08289f

    goto :goto_1

    .line 2702980
    :cond_3
    const v6, 0x7f080f7d

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2702981
    invoke-static {}, LX/1dS;->b()V

    .line 2702982
    iget v0, p1, LX/1dQ;->b:I

    .line 2702983
    packed-switch v0, :pswitch_data_0

    .line 2702984
    :goto_0
    return-object v1

    .line 2702985
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/JWR;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5f617610
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/JWP;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/JWR",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2702986
    new-instance v1, LX/JWQ;

    invoke-direct {v1, p0}, LX/JWQ;-><init>(LX/JWR;)V

    .line 2702987
    iget-object v2, p0, LX/JWR;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JWP;

    .line 2702988
    if-nez v2, :cond_0

    .line 2702989
    new-instance v2, LX/JWP;

    invoke-direct {v2, p0}, LX/JWP;-><init>(LX/JWR;)V

    .line 2702990
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JWP;->a$redex0(LX/JWP;LX/1De;IILX/JWQ;)V

    .line 2702991
    move-object v1, v2

    .line 2702992
    move-object v0, v1

    .line 2702993
    return-object v0
.end method
