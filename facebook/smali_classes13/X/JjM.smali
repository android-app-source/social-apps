.class public final LX/JjM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2727614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2727615
    check-cast p1, Lcom/facebook/user/model/User;

    check-cast p2, Lcom/facebook/user/model/User;

    .line 2727616
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2727617
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2727618
    :goto_0
    return v0

    .line 2727619
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2727620
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2727621
    const/4 v0, -0x1

    goto :goto_0

    .line 2727622
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
