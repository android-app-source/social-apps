.class public final LX/JWJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JWK;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;LX/JWK;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2702697
    iput-object p1, p0, LX/JWJ;->c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    iput-object p2, p0, LX/JWJ;->a:LX/JWK;

    iput-object p3, p0, LX/JWJ;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v0, 0x2

    const v1, 0x507a394

    invoke-static {v0, v9, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2702698
    iget-object v0, p0, LX/JWJ;->c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    iget-object v1, p0, LX/JWJ;->a:LX/JWK;

    .line 2702699
    iget-object v2, v1, LX/JWK;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v3, v1, LX/JWK;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-static {v2, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 2702700
    iget-boolean v3, v1, LX/JWK;->e:Z

    if-eqz v3, :cond_3

    invoke-static {v2}, LX/17Q;->c(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2702701
    :goto_0
    iget-object v3, v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->c:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2702702
    iget-object v0, p0, LX/JWJ;->a:LX/JWK;

    iget-object v0, v0, LX/JWK;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 2702703
    new-instance v12, LX/JWN;

    invoke-direct {v12, v0, v10}, LX/JWN;-><init>(Ljava/lang/String;Z)V

    .line 2702704
    iget-object v0, p0, LX/JWJ;->a:LX/JWK;

    iget-boolean v13, v0, LX/JWK;->e:Z

    .line 2702705
    iget-object v0, p0, LX/JWJ;->c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->e:LX/2dp;

    sget-object v2, LX/2iQ;->FEED:LX/2iQ;

    if-nez v13, :cond_1

    move v3, v9

    :goto_1
    iget-object v0, p0, LX/JWJ;->a:LX/JWK;

    iget-object v0, v0, LX/JWK;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, p0, LX/JWJ;->a:LX/JWK;

    iget-object v0, v0, LX/JWK;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->k()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, LX/JWJ;->a:LX/JWK;

    iget-object v0, v0, LX/JWK;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->a()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/JWI;

    invoke-direct {v8, p0, v12, v13}, LX/JWI;-><init>(LX/JWJ;LX/JWN;Z)V

    invoke-virtual/range {v1 .. v8}, LX/2dp;->a(LX/2iQ;ZJLjava/lang/String;Ljava/lang/String;LX/84Y;)V

    .line 2702706
    if-nez v13, :cond_0

    .line 2702707
    iget-object v0, p0, LX/JWJ;->a:LX/JWK;

    iget-object v0, v0, LX/JWK;->c:LX/2dx;

    invoke-virtual {v0}, LX/2dx;->a()V

    .line 2702708
    :cond_0
    iget-object v0, p0, LX/JWJ;->b:LX/1Pq;

    check-cast v0, LX/1Pr;

    if-nez v13, :cond_2

    move v1, v9

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v12, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2702709
    iget-object v0, p0, LX/JWJ;->b:LX/1Pq;

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, LX/JWJ;->a:LX/JWK;

    iget-object v2, v2, LX/JWK;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    aput-object v2, v1, v10

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2702710
    const v0, -0x56939346

    invoke-static {v0, v11}, LX/02F;->a(II)V

    return-void

    :cond_1
    move v3, v10

    .line 2702711
    goto :goto_1

    :cond_2
    move v1, v10

    .line 2702712
    goto :goto_2

    .line 2702713
    :cond_3
    invoke-static {v2}, LX/17Q;->b(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    goto :goto_0
.end method
