.class public LX/Jm3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Jm5;


# direct methods
.method private constructor <init>(LX/Jm5;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2732414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2732415
    iput-object p1, p0, LX/Jm3;->a:LX/Jm5;

    .line 2732416
    return-void
.end method

.method public static a(LX/0QB;)LX/Jm3;
    .locals 1

    .prologue
    .line 2732413
    invoke-static {p0}, LX/Jm3;->b(LX/0QB;)LX/Jm3;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Jm3;
    .locals 2

    .prologue
    .line 2732411
    new-instance v1, LX/Jm3;

    invoke-static {p0}, LX/Jm5;->a(LX/0QB;)LX/Jm5;

    move-result-object v0

    check-cast v0, LX/Jm5;

    invoke-direct {v1, v0}, LX/Jm3;-><init>(LX/Jm5;)V

    .line 2732412
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/util/Set;)LX/0P1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/Jm2;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2732384
    iget-object v0, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2732385
    sget-object v1, LX/Jm9;->a:LX/0U1;

    .line 2732386
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2732387
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v4

    .line 2732388
    const-string v1, "units"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, LX/Jm9;->a:LX/0U1;

    .line 2732389
    iget-object p0, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, p0

    .line 2732390
    aput-object v3, v2, v9

    sget-object v3, LX/Jm9;->b:LX/0U1;

    .line 2732391
    iget-object p0, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, p0

    .line 2732392
    aput-object v3, v2, v8

    sget-object v3, LX/Jm9;->c:LX/0U1;

    .line 2732393
    iget-object p0, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, p0

    .line 2732394
    aput-object v3, v2, v6

    sget-object v3, LX/Jm9;->e:LX/0U1;

    .line 2732395
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 2732396
    aput-object v3, v2, v7

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/Jm9;->c:LX/0U1;

    .line 2732397
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2732398
    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2732399
    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 2732400
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2732401
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2732402
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 2732403
    const/4 v1, 0x2

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 2732404
    const/4 v1, 0x3

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_0

    move v1, v8

    .line 2732405
    :goto_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    const-class v6, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    const/4 v7, 0x0

    invoke-static {v0, v6, v7}, LX/15i;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732406
    new-instance v6, LX/Jm2;

    invoke-direct {v6, v0, v5, v1}, LX/Jm2;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;IZ)V

    invoke-virtual {v3, v4, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2732407
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v1, v9

    .line 2732408
    goto :goto_1

    .line 2732409
    :cond_1
    :try_start_1
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2732410
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final a()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2732417
    iget-object v0, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2732418
    const-string v1, "units"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v4, LX/Jm9;->b:LX/0U1;

    .line 2732419
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 2732420
    aput-object v4, v2, v5

    sget-object v4, LX/Jm9;->c:LX/0U1;

    .line 2732421
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v5

    .line 2732422
    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2732423
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2732424
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2732425
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 2732426
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    const-class v3, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, LX/15i;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732427
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2732428
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2732429
    :cond_0
    :try_start_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2732430
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;IZ)V
    .locals 6

    .prologue
    .line 2732361
    iget-object v0, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2732362
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2732363
    invoke-static {p1}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v2

    .line 2732364
    sget-object v3, LX/Jm9;->a:LX/0U1;

    .line 2732365
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732366
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2732367
    sget-object v3, LX/Jm9;->c:LX/0U1;

    .line 2732368
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732369
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2732370
    sget-object v3, LX/Jm9;->e:LX/0U1;

    .line 2732371
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732372
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2732373
    sget-object v3, LX/Jm9;->b:LX/0U1;

    .line 2732374
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732375
    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2732376
    sget-object v2, LX/Jm9;->d:LX/0U1;

    .line 2732377
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2732378
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->s()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2732379
    sget-object v2, LX/Jm9;->f:LX/0U1;

    .line 2732380
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2732381
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2732382
    const-string v2, "units"

    const/4 v3, 0x0

    const v4, 0x5f5a5ec3

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x6ffac3d1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2732383
    return-void
.end method

.method public final a(Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    .line 2732348
    sget-object v0, LX/Jm9;->a:LX/0U1;

    .line 2732349
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2732350
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 2732351
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2732352
    sget-object v2, LX/Jm9;->c:LX/0U1;

    .line 2732353
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2732354
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2732355
    sget-object v2, LX/Jm9;->e:LX/0U1;

    .line 2732356
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2732357
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2732358
    iget-object v2, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2732359
    const-string v3, "units"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2732360
    return-void
.end method

.method public final b()LX/162;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2732314
    iget-object v0, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2732315
    const-string v1, "units"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    sget-object v4, LX/Jm9;->c:LX/0U1;

    .line 2732316
    iget-object p0, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p0

    .line 2732317
    aput-object v4, v2, v5

    sget-object v4, LX/Jm9;->a:LX/0U1;

    .line 2732318
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2732319
    aput-object v4, v2, v6

    sget-object v4, LX/Jm9;->f:LX/0U1;

    .line 2732320
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2732321
    aput-object v4, v2, v7

    sget-object v4, LX/Jm9;->d:LX/0U1;

    .line 2732322
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2732323
    aput-object v4, v2, v8

    const/4 v4, 0x4

    sget-object v5, LX/Jm9;->e:LX/0U1;

    .line 2732324
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2732325
    aput-object v5, v2, v4

    sget-object v4, LX/Jm9;->c:LX/0U1;

    .line 2732326
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v5

    .line 2732327
    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2732328
    :try_start_0
    new-instance v0, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/162;-><init>(LX/0mC;)V

    .line 2732329
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2732330
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2732331
    sget-object v3, LX/Jm9;->c:LX/0U1;

    .line 2732332
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732333
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2732334
    sget-object v3, LX/Jm9;->a:LX/0U1;

    .line 2732335
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732336
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2732337
    sget-object v3, LX/Jm9;->f:LX/0U1;

    .line 2732338
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732339
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2732340
    sget-object v3, LX/Jm9;->d:LX/0U1;

    .line 2732341
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732342
    const/4 v4, 0x3

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2732343
    sget-object v3, LX/Jm9;->e:LX/0U1;

    .line 2732344
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2732345
    const/4 v4, 0x4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2732346
    invoke-virtual {v0, v2}, LX/162;->a(LX/0lF;)LX/162;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2732347
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2732293
    iget-object v0, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2732294
    sget-object v1, LX/Jm9;->e:LX/0U1;

    .line 2732295
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2732296
    const-string v2, "1"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 2732297
    sget-object v2, LX/Jm9;->f:LX/0U1;

    .line 2732298
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2732299
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_REMAINING_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 2732300
    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    aput-object v1, v3, v6

    aput-object v2, v3, v7

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 2732301
    const-string v1, "units"

    new-array v2, v7, [Ljava/lang/String;

    sget-object v3, LX/Jm9;->b:LX/0U1;

    .line 2732302
    iget-object v7, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v7

    .line 2732303
    aput-object v3, v2, v6

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/Jm9;->c:LX/0U1;

    .line 2732304
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2732305
    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2732306
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2732307
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2732308
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 2732309
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    const-class v3, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, LX/15i;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2732310
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2732311
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2732312
    :cond_0
    :try_start_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2732313
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2732281
    iget-object v0, p0, LX/Jm3;->a:LX/Jm5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2732282
    const-string v1, "units"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    sget-object v4, LX/Jm9;->a:LX/0U1;

    .line 2732283
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 2732284
    aput-object v4, v2, v5

    sget-object v4, LX/Jm9;->d:LX/0U1;

    .line 2732285
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2732286
    aput-object v4, v2, v6

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2732287
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2732288
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2732289
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2732290
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2732291
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2732292
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method
