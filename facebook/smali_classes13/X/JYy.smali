.class public LX/JYy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2707612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    .line 2707613
    invoke-static {p0}, LX/JYy;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    const v1, -0x4e6785e3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    .line 2707614
    invoke-static {p0}, LX/JYy;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2707615
    invoke-static {p0}, LX/JYy;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    const v1, 0x23637cfe

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2707616
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2707617
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707618
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2707619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2707621
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->n()Ljava/lang/String;

    move-result-object v0

    .line 2707622
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2707623
    goto :goto_0

    .line 2707624
    :cond_1
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2707625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2707626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2707627
    goto :goto_2

    .line 2707628
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2707643
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707644
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707647
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->j()Ljava/lang/String;

    move-result-object v0

    .line 2707648
    :goto_0
    return-object v0

    .line 2707649
    :cond_0
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2707650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707651
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aE()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2707652
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2707629
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707630
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2707631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2707634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2707635
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2707636
    goto :goto_0

    .line 2707637
    :cond_1
    invoke-static {p0}, LX/JYy;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2707638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dy()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dy()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2707640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dy()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2707641
    goto :goto_2

    .line 2707642
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 2

    .prologue
    .line 2707664
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707665
    invoke-static {p0}, LX/JYy;->u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2707666
    if-eqz v0, :cond_1

    .line 2707667
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2707668
    const v0, 0x7f021a25

    .line 2707669
    :goto_0
    return v0

    .line 2707670
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2707671
    const v0, 0x7f021a23

    goto :goto_0

    .line 2707672
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 2707653
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707654
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707655
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707657
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2707658
    :goto_0
    return-object v0

    .line 2707659
    :cond_0
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2707660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707661
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 2707663
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 2707601
    invoke-static {p0}, LX/1VO;->s(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707602
    invoke-static {p0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2707603
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 2707604
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707605
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707607
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->m()Z

    move-result v0

    .line 2707608
    :goto_0
    return v0

    .line 2707609
    :cond_0
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2707610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dL()Z

    move-result v0

    goto :goto_0

    .line 2707611
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)D
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 2707543
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707544
    invoke-static {p0}, LX/JYy;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2707545
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2707546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->p()D

    move-result-wide v0

    .line 2707548
    :cond_0
    :goto_0
    return-wide v0

    .line 2707549
    :cond_1
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2707550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gp()D

    move-result-wide v0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2707551
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707552
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2707555
    :goto_0
    return-object v0

    .line 2707556
    :cond_0
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2707557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2707558
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2707559
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707560
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/JYy;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2707561
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {p0}, LX/JYy;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2707562
    :goto_0
    return-object v0

    .line 2707563
    :cond_0
    invoke-static {p0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2707564
    goto :goto_0

    .line 2707565
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2707567
    invoke-static {p0}, LX/JYy;->o(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    const v2, 0x285feb

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2707568
    if-eqz v0, :cond_3

    .line 2707569
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    .line 2707570
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2707571
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2707572
    :cond_3
    invoke-static {p0}, LX/JYy;->o(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    const v2, 0x25d6af

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2707573
    if-eqz v0, :cond_4

    .line 2707574
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 2707575
    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2707576
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707579
    return-void
.end method

.method public static n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 1

    .prologue
    .line 2707580
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    return v0
.end method

.method public static o(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 1

    .prologue
    .line 2707582
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707583
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707584
    const v0, 0x25d6af

    .line 2707585
    :goto_0
    return v0

    .line 2707586
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    .line 2707589
    invoke-static {p0}, LX/JYy;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    const v1, 0x4462365a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2707590
    invoke-static {p0}, LX/JYy;->m(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2707591
    invoke-static {p0}, LX/JYy;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2707592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->q()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    .line 2707594
    :goto_0
    move-object v0, v0

    .line 2707595
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCharity;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    goto :goto_1

    .line 2707596
    :cond_1
    invoke-static {p0}, LX/JYy;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2707597
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    goto :goto_0

    .line 2707598
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2707599
    invoke-static {p0}, LX/JYy;->u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2707600
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
