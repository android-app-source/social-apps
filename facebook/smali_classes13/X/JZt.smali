.class public final LX/JZt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JZu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final synthetic c:LX/JZu;


# direct methods
.method public constructor <init>(LX/JZu;)V
    .locals 1

    .prologue
    .line 2709261
    iput-object p1, p0, LX/JZt;->c:LX/JZu;

    .line 2709262
    move-object v0, p1

    .line 2709263
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2709264
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2709265
    const-string v0, "FriendRequestActionAcceptedComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2709266
    if-ne p0, p1, :cond_1

    .line 2709267
    :cond_0
    :goto_0
    return v0

    .line 2709268
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2709269
    goto :goto_0

    .line 2709270
    :cond_3
    check-cast p1, LX/JZt;

    .line 2709271
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2709272
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2709273
    if-eq v2, v3, :cond_0

    .line 2709274
    iget-object v2, p0, LX/JZt;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JZt;->a:Ljava/lang/String;

    iget-object v3, p1, LX/JZt;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2709275
    goto :goto_0

    .line 2709276
    :cond_5
    iget-object v2, p1, LX/JZt;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2709277
    :cond_6
    iget-object v2, p0, LX/JZt;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JZt;->b:Ljava/lang/String;

    iget-object v3, p1, LX/JZt;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2709278
    goto :goto_0

    .line 2709279
    :cond_7
    iget-object v2, p1, LX/JZt;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
