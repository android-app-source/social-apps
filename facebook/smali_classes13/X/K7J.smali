.class public final LX/K7J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/facebook/tarot/data/FeedbackData;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2772998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772999
    iput-object p1, p0, LX/K7J;->a:Ljava/lang/String;

    .line 2773000
    return-void
.end method


# virtual methods
.method public final a(F)LX/K7J;
    .locals 0

    .prologue
    .line 2773016
    iput p1, p0, LX/K7J;->h:F

    .line 2773017
    return-object p0
.end method

.method public final a(Lcom/facebook/tarot/data/FeedbackData;)LX/K7J;
    .locals 0

    .prologue
    .line 2773014
    iput-object p1, p0, LX/K7J;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2773015
    return-object p0
.end method

.method public final a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;)LX/K7J;
    .locals 0

    .prologue
    .line 2773012
    iput-object p1, p0, LX/K7J;->b:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;

    .line 2773013
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/K7J;
    .locals 0

    .prologue
    .line 2773010
    iput-object p1, p0, LX/K7J;->c:Ljava/lang/String;

    .line 2773011
    return-object p0
.end method

.method public final a()Lcom/facebook/tarot/data/TarotCardVideoData;
    .locals 8

    .prologue
    .line 2773009
    new-instance v0, Lcom/facebook/tarot/data/TarotCardVideoData;

    iget-object v1, p0, LX/K7J;->a:Ljava/lang/String;

    new-instance v2, Lcom/facebook/tarot/data/VideoData;

    iget-object v3, p0, LX/K7J;->b:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;

    invoke-direct {v2, v3}, Lcom/facebook/tarot/data/VideoData;-><init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;)V

    new-instance v3, Lcom/facebook/tarot/data/DescriptionData;

    iget-object v4, p0, LX/K7J;->c:Ljava/lang/String;

    iget-object v5, p0, LX/K7J;->d:Ljava/lang/String;

    iget v6, p0, LX/K7J;->h:F

    iget v7, p0, LX/K7J;->i:F

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/tarot/data/DescriptionData;-><init>(Ljava/lang/String;Ljava/lang/String;FF)V

    iget-object v4, p0, LX/K7J;->e:Lcom/facebook/tarot/data/FeedbackData;

    iget-object v5, p0, LX/K7J;->f:Ljava/lang/String;

    iget-object v6, p0, LX/K7J;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tarot/data/TarotCardVideoData;-><init>(Ljava/lang/String;Lcom/facebook/tarot/data/VideoData;Lcom/facebook/tarot/data/DescriptionData;Lcom/facebook/tarot/data/FeedbackData;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(F)LX/K7J;
    .locals 0

    .prologue
    .line 2773007
    iput p1, p0, LX/K7J;->i:F

    .line 2773008
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/K7J;
    .locals 0

    .prologue
    .line 2773005
    iput-object p1, p0, LX/K7J;->d:Ljava/lang/String;

    .line 2773006
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/K7J;
    .locals 0

    .prologue
    .line 2773003
    iput-object p1, p0, LX/K7J;->f:Ljava/lang/String;

    .line 2773004
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/K7J;
    .locals 0

    .prologue
    .line 2773001
    iput-object p1, p0, LX/K7J;->g:Ljava/lang/String;

    .line 2773002
    return-object p0
.end method
