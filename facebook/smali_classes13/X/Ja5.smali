.class public final LX/Ja5;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ja6;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2709539
    invoke-static {}, LX/Ja6;->q()LX/Ja6;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2709540
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2709541
    const-string v0, "FriendRequestRespondedComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2709542
    if-ne p0, p1, :cond_1

    .line 2709543
    :cond_0
    :goto_0
    return v0

    .line 2709544
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2709545
    goto :goto_0

    .line 2709546
    :cond_3
    check-cast p1, LX/Ja5;

    .line 2709547
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2709548
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2709549
    if-eq v2, v3, :cond_0

    .line 2709550
    iget-object v2, p0, LX/Ja5;->a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Ja5;->a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    iget-object v3, p1, LX/Ja5;->a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2709551
    goto :goto_0

    .line 2709552
    :cond_4
    iget-object v2, p1, LX/Ja5;->a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
