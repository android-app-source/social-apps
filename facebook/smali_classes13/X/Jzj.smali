.class public final LX/Jzj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic a:LX/Jzk;

.field private final b:LX/5pW;

.field private c:Z


# direct methods
.method public constructor <init>(LX/Jzk;LX/5pW;)V
    .locals 1

    .prologue
    .line 2756618
    iput-object p1, p0, LX/Jzj;->a:LX/Jzk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756619
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Jzj;->c:Z

    .line 2756620
    iput-object p2, p0, LX/Jzj;->b:LX/5pW;

    .line 2756621
    return-void
.end method


# virtual methods
.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 3

    .prologue
    .line 2756622
    iget-boolean v0, p0, LX/Jzj;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Jzj;->a:LX/Jzk;

    invoke-static {v0}, LX/Jzk;->a(LX/Jzk;)LX/5pY;

    move-result-object v0

    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756623
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756624
    const-string v1, "action"

    const-string v2, "dateSetAction"

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756625
    const-string v1, "year"

    invoke-interface {v0, v1, p2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2756626
    const-string v1, "month"

    invoke-interface {v0, v1, p3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2756627
    const-string v1, "day"

    invoke-interface {v0, v1, p4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2756628
    iget-object v1, p0, LX/Jzj;->b:LX/5pW;

    invoke-interface {v1, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2756629
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Jzj;->c:Z

    .line 2756630
    :cond_0
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 2756631
    iget-boolean v0, p0, LX/Jzj;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Jzj;->a:LX/Jzk;

    invoke-static {v0}, LX/Jzk;->b(LX/Jzk;)LX/5pY;

    move-result-object v0

    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756632
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756633
    const-string v1, "action"

    const-string v2, "dismissedAction"

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756634
    iget-object v1, p0, LX/Jzj;->b:LX/5pW;

    invoke-interface {v1, v0}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2756635
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Jzj;->c:Z

    .line 2756636
    :cond_0
    return-void
.end method
