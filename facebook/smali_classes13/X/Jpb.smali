.class public LX/Jpb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/Jpb;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2737728
    new-instance v0, LX/Jpb;

    .line 2737729
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2737730
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2737731
    invoke-direct {v0, v1, v2}, LX/Jpb;-><init>(LX/0Px;LX/0Px;)V

    sput-object v0, LX/Jpb;->a:LX/Jpb;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2737732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737733
    iput-object p1, p0, LX/Jpb;->b:LX/0Px;

    .line 2737734
    iput-object p2, p0, LX/Jpb;->c:LX/0Px;

    .line 2737735
    return-void
.end method
