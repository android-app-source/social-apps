.class public LX/JYF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/JYO;

.field public final b:LX/JY7;


# direct methods
.method public constructor <init>(LX/JYO;LX/JY7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2706071
    iput-object p1, p0, LX/JYF;->a:LX/JYO;

    .line 2706072
    iput-object p2, p0, LX/JYF;->b:LX/JY7;

    .line 2706073
    return-void
.end method

.method public static a(LX/0QB;)LX/JYF;
    .locals 5

    .prologue
    .line 2706059
    const-class v1, LX/JYF;

    monitor-enter v1

    .line 2706060
    :try_start_0
    sget-object v0, LX/JYF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706061
    sput-object v2, LX/JYF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706062
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706063
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706064
    new-instance p0, LX/JYF;

    invoke-static {v0}, LX/JYO;->a(LX/0QB;)LX/JYO;

    move-result-object v3

    check-cast v3, LX/JYO;

    invoke-static {v0}, LX/JY7;->a(LX/0QB;)LX/JY7;

    move-result-object v4

    check-cast v4, LX/JY7;

    invoke-direct {p0, v3, v4}, LX/JYF;-><init>(LX/JYO;LX/JY7;)V

    .line 2706065
    move-object v0, p0

    .line 2706066
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706067
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706068
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706069
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
