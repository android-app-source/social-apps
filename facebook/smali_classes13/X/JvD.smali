.class public final enum LX/JvD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JvD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JvD;

.field public static final enum INVALID_RESPONSE:LX/JvD;

.field public static final enum NON_CANCELATION_ERROR:LX/JvD;

.field public static final enum TAB_NOT_SUPPORTED:LX/JvD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2749894
    new-instance v0, LX/JvD;

    const-string v1, "TAB_NOT_SUPPORTED"

    invoke-direct {v0, v1, v2}, LX/JvD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JvD;->TAB_NOT_SUPPORTED:LX/JvD;

    .line 2749895
    new-instance v0, LX/JvD;

    const-string v1, "INVALID_RESPONSE"

    invoke-direct {v0, v1, v3}, LX/JvD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JvD;->INVALID_RESPONSE:LX/JvD;

    .line 2749896
    new-instance v0, LX/JvD;

    const-string v1, "NON_CANCELATION_ERROR"

    invoke-direct {v0, v1, v4}, LX/JvD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JvD;->NON_CANCELATION_ERROR:LX/JvD;

    .line 2749897
    const/4 v0, 0x3

    new-array v0, v0, [LX/JvD;

    sget-object v1, LX/JvD;->TAB_NOT_SUPPORTED:LX/JvD;

    aput-object v1, v0, v2

    sget-object v1, LX/JvD;->INVALID_RESPONSE:LX/JvD;

    aput-object v1, v0, v3

    sget-object v1, LX/JvD;->NON_CANCELATION_ERROR:LX/JvD;

    aput-object v1, v0, v4

    sput-object v0, LX/JvD;->$VALUES:[LX/JvD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2749898
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JvD;
    .locals 1

    .prologue
    .line 2749899
    const-class v0, LX/JvD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JvD;

    return-object v0
.end method

.method public static values()[LX/JvD;
    .locals 1

    .prologue
    .line 2749900
    sget-object v0, LX/JvD;->$VALUES:[LX/JvD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JvD;

    return-object v0
.end method
