.class public final LX/JfJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;)V
    .locals 0

    .prologue
    .line 2721729
    iput-object p1, p0, LX/JfJ;->a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x24279d70

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2721730
    iget-object v0, p0, LX/JfJ;->a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    iget-object v0, v0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->c:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721731
    iget-object v0, p0, LX/JfJ;->a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    iget-object v0, v0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JfG;

    new-instance v2, LX/JfI;

    invoke-direct {v2, p0}, LX/JfI;-><init>(LX/JfJ;)V

    .line 2721732
    iput-object v2, v0, LX/JfG;->c:LX/JfC;

    .line 2721733
    iget-object v0, p0, LX/JfJ;->a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    iget-object v0, v0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JfG;

    iget-object v2, p0, LX/JfJ;->a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    iget-object v2, v2, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->e:LX/0gc;

    iget-object v3, p0, LX/JfJ;->a:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    iget-object v3, v3, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->c:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;

    invoke-virtual {v0, v2, v3}, LX/JfG;->a(LX/0gc;LX/Jew;)V

    .line 2721734
    const v0, -0x2d2b9bf3

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
