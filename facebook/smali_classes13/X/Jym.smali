.class public LX/Jym;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/facebook/fbui/glyph/GlyphView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2755432
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Jym;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2755433
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2755434
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Jym;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755435
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2755436
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755437
    invoke-virtual {p0}, LX/Jym;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03105c

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2755438
    iput-object p1, p0, LX/Jym;->a:Landroid/content/Context;

    .line 2755439
    const v0, 0x7f0d2731

    invoke-virtual {p0, v0}, LX/Jym;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Jym;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2755440
    const v0, 0x7f0d2732

    invoke-virtual {p0, v0}, LX/Jym;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Jym;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2755441
    const v0, 0x7f0d2733

    invoke-virtual {p0, v0}, LX/Jym;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Jym;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2755442
    const v0, 0x7f0d2734

    invoke-virtual {p0, v0}, LX/Jym;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Jym;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2755443
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2755444
    invoke-virtual {p0, v0}, LX/Jym;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2755445
    invoke-virtual {p0}, LX/Jym;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b26e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, LX/Jym;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b26e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v3, v0, v3, v1}, LX/Jym;->setPadding(IIII)V

    .line 2755446
    return-void
.end method
