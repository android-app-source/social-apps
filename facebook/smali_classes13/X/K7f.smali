.class public final LX/K7f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public final synthetic b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

.field private final c:J

.field private final d:J

.field private e:Landroid/os/Handler;

.field private f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V
    .locals 2

    .prologue
    .line 2773378
    iput-object p1, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773379
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, LX/K7f;->c:J

    .line 2773380
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, LX/K7f;->d:J

    .line 2773381
    new-instance v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer$HideDrawerPeriodicChecker$1;

    invoke-direct {v0, p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer$HideDrawerPeriodicChecker$1;-><init>(LX/K7f;)V

    iput-object v0, p0, LX/K7f;->f:Ljava/lang/Runnable;

    .line 2773382
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/K7f;->e:Landroid/os/Handler;

    .line 2773383
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xbb8

    .line 2773374
    invoke-virtual {p0}, LX/K7f;->b()V

    .line 2773375
    iget-object v0, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iget-object v0, v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/K7f;->a:J

    .line 2773376
    iget-object v0, p0, LX/K7f;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/K7f;->f:Ljava/lang/Runnable;

    const v2, -0x9c358bc

    invoke-static {v0, v1, v4, v5, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2773377
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2773363
    iget-object v0, p0, LX/K7f;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/K7f;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2773364
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2773370
    iget-object v0, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iget-object v0, v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    const-wide/16 v2, 0x1388

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/K7f;->a:J

    .line 2773371
    iget-object v0, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iget-object v0, v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    iget-boolean v0, v0, LX/K7e;->b:Z

    if-nez v0, :cond_0

    .line 2773372
    iget-object v0, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    sget-object v1, LX/K72;->USER:LX/K72;

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(LX/K72;)V

    .line 2773373
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 2773365
    iget-object v0, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iget-object v0, v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2773366
    iget-wide v2, p0, LX/K7f;->a:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 2773367
    iget-object v0, p0, LX/K7f;->b:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    sget-object v1, LX/K72;->TIMEOUT:LX/K72;

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b(LX/K72;)V

    .line 2773368
    :goto_0
    return-void

    .line 2773369
    :cond_0
    iget-object v2, p0, LX/K7f;->e:Landroid/os/Handler;

    iget-object v3, p0, LX/K7f;->f:Ljava/lang/Runnable;

    iget-wide v4, p0, LX/K7f;->a:J

    sub-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const v4, 0x6291e6dc

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
