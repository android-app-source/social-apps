.class public LX/JrG;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final m:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Og;

.field private final b:LX/2N4;

.field private final c:LX/FDs;

.field private final d:LX/Jrb;

.field private final e:LX/Jrc;

.field private final f:LX/Jqb;

.field private final g:LX/2Ow;

.field private final h:Lcom/facebook/messaging/send/client/SendMessageManager;

.field private final i:LX/2Lw;

.field private final j:LX/0Uh;

.field private final k:LX/FO4;

.field public l:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742580
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrG;->m:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/2Og;LX/2N4;LX/FDs;LX/Jrb;LX/Jrc;LX/0Ot;LX/Jqb;LX/2Ow;Lcom/facebook/messaging/send/client/SendMessageManager;LX/2Lw;LX/0Uh;LX/FO4;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Og;",
            "LX/2N4;",
            "LX/FDs;",
            "LX/Jrb;",
            "LX/Jrc;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jqb;",
            "LX/2Ow;",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            "LX/2Lw;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/FO4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2742657
    invoke-direct {p0, p6}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742658
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742659
    iput-object v0, p0, LX/JrG;->l:LX/0Ot;

    .line 2742660
    iput-object p1, p0, LX/JrG;->a:LX/2Og;

    .line 2742661
    iput-object p2, p0, LX/JrG;->b:LX/2N4;

    .line 2742662
    iput-object p3, p0, LX/JrG;->c:LX/FDs;

    .line 2742663
    iput-object p4, p0, LX/JrG;->d:LX/Jrb;

    .line 2742664
    iput-object p5, p0, LX/JrG;->e:LX/Jrc;

    .line 2742665
    iput-object p7, p0, LX/JrG;->f:LX/Jqb;

    .line 2742666
    iput-object p8, p0, LX/JrG;->g:LX/2Ow;

    .line 2742667
    iput-object p9, p0, LX/JrG;->h:Lcom/facebook/messaging/send/client/SendMessageManager;

    .line 2742668
    iput-object p10, p0, LX/JrG;->i:LX/2Lw;

    .line 2742669
    iput-object p11, p0, LX/JrG;->j:LX/0Uh;

    .line 2742670
    iput-object p12, p0, LX/JrG;->k:LX/FO4;

    .line 2742671
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742654
    invoke-virtual {p1}, LX/6kW;->u()LX/6kL;

    move-result-object v0

    .line 2742655
    iget-object v1, p0, LX/JrG;->e:LX/Jrc;

    iget-object v0, v0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742656
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrG;
    .locals 7

    .prologue
    .line 2742627
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742628
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742629
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742630
    if-nez v1, :cond_0

    .line 2742631
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742632
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742633
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742634
    sget-object v1, LX/JrG;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742635
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742636
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742637
    :cond_1
    if-nez v1, :cond_4

    .line 2742638
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742639
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742640
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrG;->b(LX/0QB;)LX/JrG;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2742641
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742642
    if-nez v1, :cond_2

    .line 2742643
    sget-object v0, LX/JrG;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrG;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742644
    :goto_1
    if-eqz v0, :cond_3

    .line 2742645
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742646
    :goto_3
    check-cast v0, LX/JrG;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742647
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742648
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742649
    :catchall_1
    move-exception v0

    .line 2742650
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742651
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742652
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742653
    :cond_2
    :try_start_8
    sget-object v0, LX/JrG;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrG;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/JrG;
    .locals 13

    .prologue
    .line 2742623
    new-instance v0, LX/JrG;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v1

    check-cast v1, LX/2Og;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v2

    check-cast v2, LX/2N4;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v3

    check-cast v3, LX/FDs;

    invoke-static {p0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v4

    check-cast v4, LX/Jrb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v5

    check-cast v5, LX/Jrc;

    const/16 v6, 0x35bd

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v7

    check-cast v7, LX/Jqb;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v8

    check-cast v8, LX/2Ow;

    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;

    move-result-object v9

    check-cast v9, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {p0}, LX/2Lw;->a(LX/0QB;)LX/2Lw;

    move-result-object v10

    check-cast v10, LX/2Lw;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/FO4;->a(LX/0QB;)LX/FO4;

    move-result-object v12

    check-cast v12, LX/FO4;

    invoke-direct/range {v0 .. v12}, LX/JrG;-><init>(LX/2Og;LX/2N4;LX/FDs;LX/Jrb;LX/Jrc;LX/0Ot;LX/Jqb;LX/2Ow;Lcom/facebook/messaging/send/client/SendMessageManager;LX/2Lw;LX/0Uh;LX/FO4;)V

    .line 2742624
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742625
    iput-object v1, v0, LX/JrG;->l:LX/0Ot;

    .line 2742626
    return-object v0
.end method

.method private c(LX/6kW;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2742618
    invoke-virtual {p1}, LX/6kW;->u()LX/6kL;

    move-result-object v1

    .line 2742619
    iget-object v2, p0, LX/JrG;->e:LX/Jrc;

    iget-object v3, v1, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v2, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2742620
    iget-object v1, v1, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v1, v1, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2742621
    iget-object v3, p0, LX/JrG;->a:LX/2Og;

    invoke-virtual {v3, v2, v1}, LX/2Og;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2742622
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, LX/JrG;->b:LX/2N4;

    invoke-virtual {v2, v1}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742614
    check-cast p1, LX/6kW;

    .line 2742615
    invoke-direct {p0, p1}, LX/JrG;->c(LX/6kW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2742616
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742617
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, LX/JrG;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742597
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->u()LX/6kL;

    move-result-object v0

    .line 2742598
    iget-object v1, p0, LX/JrG;->d:LX/Jrb;

    .line 2742599
    iget-object v4, v1, LX/Jrb;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2742600
    invoke-static {p1, v4, v5}, LX/Jrb;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v4

    .line 2742601
    iget-object v5, v0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1, v5, v6, v4}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    move-result-object v4

    .line 2742602
    iget-object v5, v0, LX/6kL;->attachments:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/6kL;->attachments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2742603
    iget-object v5, v0, LX/6kL;->attachments:Ljava/util/List;

    invoke-static {v1, v5, v4}, LX/Jrb;->a(LX/Jrb;Ljava/util/List;LX/6f7;)V

    .line 2742604
    iget-object v5, v0, LX/6kL;->attachments:Ljava/util/List;

    iget-object v6, v0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v6, v6, LX/6kn;->messageId:Ljava/lang/String;

    invoke-static {v1, v5, v6}, LX/Jrb;->a(LX/Jrb;Ljava/util/List;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 2742605
    iput-object v5, v4, LX/6f7;->i:Ljava/util/List;

    .line 2742606
    :cond_0
    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2742607
    iget-object v5, v1, LX/Jrb;->f:LX/3QS;

    sget-object v6, LX/6fK;->SYNC_PROTOCOL_SENT_MESSAGE_DELTA:LX/6fK;

    invoke-virtual {v5, v6, v4}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2742608
    move-object v0, v4

    .line 2742609
    iget-object v1, p0, LX/JrG;->c:LX/FDs;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/Message;J)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2742610
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2742611
    const-string v2, "sentMessage"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2742612
    iget-object v2, p0, LX/JrG;->g:LX/2Ow;

    invoke-virtual {v2, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2742613
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742587
    const-string v0, "sentMessage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2742588
    if-eqz v0, :cond_0

    .line 2742589
    iget-object v1, p0, LX/JrG;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/model/messages/Message;J)V

    .line 2742590
    iget-object v1, p0, LX/JrG;->f:LX/Jqb;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v2, v4, v5}, LX/Jqb;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2742591
    iget-object v1, p0, LX/JrG;->j:LX/0Uh;

    const/16 v2, 0x20f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2742592
    iget-object v1, p0, LX/JrG;->h:Lcom/facebook/messaging/send/client/SendMessageManager;

    .line 2742593
    iget-object v2, v1, Lcom/facebook/messaging/send/client/SendMessageManager;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/messaging/send/client/SendMessageManager$9;

    invoke-direct {v3, v1, v0}, Lcom/facebook/messaging/send/client/SendMessageManager$9;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    const v4, -0x33b35dc2    # -5.3643512E7f

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2742594
    :cond_0
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->u()LX/6kL;

    move-result-object v0

    .line 2742595
    iget-object v1, p0, LX/JrG;->i:LX/2Lw;

    sget-object v2, LX/FCY;->MQTT:LX/FCY;

    iget-object v0, v0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V

    .line 2742596
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742586
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrG;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)LX/0P1;
    .locals 3

    .prologue
    .line 2742582
    check-cast p1, LX/6kW;

    .line 2742583
    invoke-virtual {p1}, LX/6kW;->u()LX/6kL;

    move-result-object v0

    .line 2742584
    iget-object v1, p0, LX/JrG;->e:LX/Jrc;

    iget-object v2, v0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v2, v2, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2742585
    iget-object v0, v0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->messageId:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2742581
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrG;->c(LX/6kW;)Z

    move-result v0

    return v0
.end method
