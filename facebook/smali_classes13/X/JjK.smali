.class public LX/JjK;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/FJw;

.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FJw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2727510
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2727511
    iput-object p1, p0, LX/JjK;->a:LX/FJw;

    .line 2727512
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2727513
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03050a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2727514
    new-instance v1, LX/JjH;

    invoke-direct {v1, p0, v0}, LX/JjH;-><init>(LX/JjK;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2727515
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 2727516
    :cond_0
    :goto_0
    return-void

    .line 2727517
    :cond_1
    iget-object v0, p0, LX/JjK;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    .line 2727518
    new-instance v1, LX/JjJ;

    iget-object v0, p0, LX/JjK;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-direct {v1, v0, v2}, LX/JjJ;-><init>(Lcom/facebook/user/model/User;Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;)V

    move-object v0, v1

    .line 2727519
    :goto_1
    move-object v1, v0

    .line 2727520
    if-eqz v1, :cond_0

    .line 2727521
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    .line 2727522
    const v0, 0x7f0d0e4e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    .line 2727523
    iget-object v3, v1, LX/JjJ;->a:Lcom/facebook/user/model/User;

    iget-object v4, v1, LX/JjJ;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 2727524
    sget-object p0, LX/JjI;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2727525
    sget-object p0, LX/8ue;->NONE:LX/8ue;

    :goto_2
    move-object p0, p0

    .line 2727526
    invoke-virtual {v3}, Lcom/facebook/user/model/User;->ax()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-static {v3, p0}, LX/8t9;->b(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object p0

    :goto_3
    move-object v3, p0

    .line 2727527
    invoke-virtual {v0, v3}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2727528
    const v0, 0x7f0d0e4f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2727529
    iget-object v1, v1, LX/JjJ;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2727530
    :cond_2
    iget-object v0, p0, LX/JjK;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int v0, p2, v0

    .line 2727531
    iget-object v1, p0, LX/JjK;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 2727532
    new-instance v1, LX/JjJ;

    iget-object v2, p0, LX/JjK;->d:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-direct {v1, v0, v2}, LX/JjJ;-><init>(Lcom/facebook/user/model/User;Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;)V

    move-object v0, v1

    goto :goto_1

    .line 2727533
    :cond_3
    iget-object v1, p0, LX/JjK;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2727534
    iget-object v1, p0, LX/JjK;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 2727535
    new-instance v1, LX/JjJ;

    iget-object v2, p0, LX/JjK;->e:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-direct {v1, v0, v2}, LX/JjJ;-><init>(Lcom/facebook/user/model/User;Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 2727536
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    invoke-static {v3, p0}, LX/8t9;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object p0

    goto :goto_3

    .line 2727537
    :pswitch_0
    sget-object p0, LX/8ue;->EVENT_REMINDER_GOING:LX/8ue;

    goto :goto_2

    .line 2727538
    :pswitch_1
    sget-object p0, LX/8ue;->EVENT_REMINDER_DECLINED:LX/8ue;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2727539
    iget v0, p0, LX/JjK;->b:I

    return v0
.end method
