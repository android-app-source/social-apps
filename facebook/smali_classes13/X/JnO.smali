.class public LX/JnO;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/JnJ;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/JnN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2733962
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2733963
    iput-object p1, p0, LX/JnO;->a:Landroid/content/Context;

    .line 2733964
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2733965
    new-instance v0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;

    iget-object v1, p0, LX/JnO;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;-><init>(Landroid/content/Context;)V

    .line 2733966
    new-instance v1, LX/JnJ;

    invoke-direct {v1, v0}, LX/JnJ;-><init>(Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2733967
    check-cast p1, LX/JnJ;

    .line 2733968
    iget-object v0, p0, LX/JnO;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;

    .line 2733969
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;

    .line 2733970
    iget-object v3, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->e:Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->e:Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;

    .line 2733971
    iget-wide v7, v3, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->a:J

    move-wide v3, v7

    .line 2733972
    iget-wide v7, v0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->a:J

    move-wide v5, v7

    .line 2733973
    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    .line 2733974
    :cond_0
    iput-object v0, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->e:Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;

    .line 2733975
    iget-object v3, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2733976
    iget-object v4, v0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2733977
    invoke-static {v4}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2733978
    iget v3, v0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->c:I

    move v3, v3

    .line 2733979
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 2733980
    invoke-virtual {v1}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0077

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2733981
    iget v4, v0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->c:I

    move v4, v4

    .line 2733982
    invoke-static {v4}, LX/1bX;->a(I)LX/1bX;

    move-result-object v4

    new-instance v5, LX/1o9;

    invoke-direct {v5, v3, v3}, LX/1o9;-><init>(II)V

    .line 2733983
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 2733984
    move-object v3, v4

    .line 2733985
    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    .line 2733986
    iget-object v5, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->a:LX/1Ad;

    sget-object v6, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2733987
    :cond_1
    :goto_0
    new-instance v2, LX/JnM;

    invoke-direct {v2, p0, v0}, LX/JnM;-><init>(LX/JnO;Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;)V

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733988
    return-void

    .line 2733989
    :cond_2
    iget-object v3, v1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2733990
    iget-object v0, p0, LX/JnO;->b:LX/0Px;

    if-nez v0, :cond_0

    .line 2733991
    const/4 v0, 0x0

    .line 2733992
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/JnO;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
