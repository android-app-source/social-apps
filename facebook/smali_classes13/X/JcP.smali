.class public final LX/JcP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/foreground/GeoPixelController;


# direct methods
.method public constructor <init>(Lcom/facebook/location/foreground/GeoPixelController;)V
    .locals 0

    .prologue
    .line 2717797
    iput-object p1, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2717798
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2717799
    iget-object v0, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v0, v0, Lcom/facebook/location/foreground/GeoPixelController;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mk;

    invoke-virtual {v0}, LX/1Mk;->b()Ljava/util/Map;

    move-result-object v0

    .line 2717800
    if-nez v0, :cond_5

    .line 2717801
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v6, v0

    .line 2717802
    :goto_0
    const-string v0, ","

    invoke-static {v0}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0}, LX/2Cb;->trimResults()LX/2Cb;

    move-result-object v0

    invoke-virtual {v0}, LX/2Cb;->omitEmptyStrings()LX/2Cb;

    move-result-object v0

    iget-object v1, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v1, v1, Lcom/facebook/location/foreground/GeoPixelController;->f:LX/0W3;

    sget-wide v2, LX/0X5;->ia:J

    const-string v4, ""

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Cb;->splitToList(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v3

    .line 2717803
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2717804
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 2717805
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2717806
    :try_start_0
    const-string v1, "http://scontent-%s.xx.fbcdn.net/?rand=%d"

    sget-object v5, Lcom/facebook/location/foreground/GeoPixelController;->c:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextLong()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v1, v0, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2717807
    iget-object v5, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v5, v5, Lcom/facebook/location/foreground/GeoPixelController;->p:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v7

    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v8, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2717808
    iput-object v8, v7, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2717809
    move-object v1, v7

    .line 2717810
    sget-object v7, Lcom/facebook/location/foreground/GeoPixelController;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2717811
    iput-object v7, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2717812
    move-object v1, v1

    .line 2717813
    sget-object v7, Lcom/facebook/location/foreground/GeoPixelController;->a:Ljava/lang/String;

    .line 2717814
    iput-object v7, v1, LX/15E;->c:Ljava/lang/String;

    .line 2717815
    move-object v1, v1

    .line 2717816
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2717817
    iput-object v7, v1, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2717818
    move-object v1, v1

    .line 2717819
    new-instance v7, LX/JcO;

    invoke-direct {v7, p0}, LX/JcO;-><init>(LX/JcP;)V

    .line 2717820
    iput-object v7, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2717821
    move-object v1, v1

    .line 2717822
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2717823
    if-eqz v1, :cond_0

    .line 2717824
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    const-string v7, "region_code"

    invoke-virtual {v5, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v5, "rtt_ms"

    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2717825
    :catch_0
    move-exception v0

    .line 2717826
    iget-object v1, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v1, v1, Lcom/facebook/location/foreground/GeoPixelController;->j:LX/03V;

    sget-object v5, Lcom/facebook/location/foreground/GeoPixelController;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2717827
    :cond_1
    const-string v0, "rtt_results"

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717828
    :cond_2
    const-string v2, "facebook.com"

    .line 2717829
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2717830
    const-string v0, "scontent-%s.xx.fbcdn.net"

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2717831
    :cond_3
    iget-object v0, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v0, v0, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    sget-short v1, LX/1rm;->p:S

    invoke-interface {v0, v1, v10}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2717832
    iget-object v0, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v0, v0, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    sget v1, LX/1rm;->q:I

    const/4 v3, 0x2

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    .line 2717833
    iget-object v1, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v1, v1, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    sget v3, LX/1rm;->o:I

    const/4 v4, 0x5

    invoke-interface {v1, v3, v4}, LX/0ad;->a(II)I

    move-result v1

    .line 2717834
    iget-object v3, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v3, v3, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    sget v4, LX/1rm;->s:F

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-interface {v3, v4, v5}, LX/0ad;->a(FF)F

    move-result v3

    .line 2717835
    iget-object v4, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v4, v4, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    sget v5, LX/1rm;->r:F

    const v7, 0x3e4ccccd    # 0.2f

    invoke-interface {v4, v5, v7}, LX/0ad;->a(FF)F

    move-result v4

    .line 2717836
    iget-object v5, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    iget-object v5, v5, Lcom/facebook/location/foreground/GeoPixelController;->h:LX/0Zb;

    invoke-static/range {v0 .. v5}, Lcom/facebook/location/foreground/GeoPixelController;->a(IILjava/lang/String;FFLX/0Zb;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2717837
    const-string v1, "ping_results"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717838
    :cond_4
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 2717839
    const-string v1, "time_zone_short_name"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v10, v10, v2}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717840
    const-string v1, "time_zone_long_name"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v10, v11, v2}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717841
    const-string v0, "phone_line_one_area_code"

    iget-object v1, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    invoke-static {v1, v10}, Lcom/facebook/location/foreground/GeoPixelController;->a$redex0(Lcom/facebook/location/foreground/GeoPixelController;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717842
    const-string v0, "phone_line_two_area_code"

    iget-object v1, p0, LX/JcP;->a:Lcom/facebook/location/foreground/GeoPixelController;

    invoke-static {v1, v11}, Lcom/facebook/location/foreground/GeoPixelController;->a$redex0(Lcom/facebook/location/foreground/GeoPixelController;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717843
    return-object v6

    :cond_5
    move-object v6, v0

    goto/16 :goto_0
.end method
