.class public final enum LX/K6F;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K6F;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K6F;

.field public static final enum ALL_CAUGHT_UP:LX/K6F;

.field public static final enum FOLLOWING_INITIALLY_BUT_JUST_SUBSCRIBED:LX/K6F;

.field public static final enum FOLLOWING_INITIALLY_BUT_JUST_UNSUBSCRIBED:LX/K6F;

.field public static final enum NOT_FOLLOWING_INITIALLY:LX/K6F;

.field public static final enum NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_SUBSCRIBED:LX/K6F;

.field public static final enum NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_UNSUBSCRIBED:LX/K6F;


# instance fields
.field private final mLayoutResourceId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2771217
    new-instance v0, LX/K6F;

    const-string v1, "ALL_CAUGHT_UP"

    const v2, 0x7f03047f

    invoke-direct {v0, v1, v4, v2}, LX/K6F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/K6F;->ALL_CAUGHT_UP:LX/K6F;

    .line 2771218
    new-instance v0, LX/K6F;

    const-string v1, "FOLLOWING_INITIALLY_BUT_JUST_SUBSCRIBED"

    const v2, 0x7f030480

    invoke-direct {v0, v1, v5, v2}, LX/K6F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_SUBSCRIBED:LX/K6F;

    .line 2771219
    new-instance v0, LX/K6F;

    const-string v1, "FOLLOWING_INITIALLY_BUT_JUST_UNSUBSCRIBED"

    const v2, 0x7f030481

    invoke-direct {v0, v1, v6, v2}, LX/K6F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_UNSUBSCRIBED:LX/K6F;

    .line 2771220
    new-instance v0, LX/K6F;

    const-string v1, "NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_UNSUBSCRIBED"

    const v2, 0x7f030484

    invoke-direct {v0, v1, v7, v2}, LX/K6F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_UNSUBSCRIBED:LX/K6F;

    .line 2771221
    new-instance v0, LX/K6F;

    const-string v1, "NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_SUBSCRIBED"

    const v2, 0x7f030483

    invoke-direct {v0, v1, v8, v2}, LX/K6F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_SUBSCRIBED:LX/K6F;

    .line 2771222
    new-instance v0, LX/K6F;

    const-string v1, "NOT_FOLLOWING_INITIALLY"

    const/4 v2, 0x5

    const v3, 0x7f030482

    invoke-direct {v0, v1, v2, v3}, LX/K6F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/K6F;->NOT_FOLLOWING_INITIALLY:LX/K6F;

    .line 2771223
    const/4 v0, 0x6

    new-array v0, v0, [LX/K6F;

    sget-object v1, LX/K6F;->ALL_CAUGHT_UP:LX/K6F;

    aput-object v1, v0, v4

    sget-object v1, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_SUBSCRIBED:LX/K6F;

    aput-object v1, v0, v5

    sget-object v1, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_UNSUBSCRIBED:LX/K6F;

    aput-object v1, v0, v6

    sget-object v1, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_UNSUBSCRIBED:LX/K6F;

    aput-object v1, v0, v7

    sget-object v1, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_SUBSCRIBED:LX/K6F;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/K6F;->NOT_FOLLOWING_INITIALLY:LX/K6F;

    aput-object v2, v0, v1

    sput-object v0, LX/K6F;->$VALUES:[LX/K6F;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2771224
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2771225
    iput p3, p0, LX/K6F;->mLayoutResourceId:I

    .line 2771226
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K6F;
    .locals 1

    .prologue
    .line 2771227
    const-class v0, LX/K6F;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K6F;

    return-object v0
.end method

.method public static values()[LX/K6F;
    .locals 1

    .prologue
    .line 2771228
    sget-object v0, LX/K6F;->$VALUES:[LX/K6F;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K6F;

    return-object v0
.end method


# virtual methods
.method public final getLayoutResourceId()I
    .locals 1

    .prologue
    .line 2771229
    iget v0, p0, LX/K6F;->mLayoutResourceId:I

    return v0
.end method
