.class public final LX/Jhi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EFb;

.field public final synthetic b:LX/Jho;


# direct methods
.method public constructor <init>(LX/Jho;LX/EFb;)V
    .locals 0

    .prologue
    .line 2724853
    iput-object p1, p0, LX/Jhi;->b:LX/Jho;

    iput-object p2, p0, LX/Jhi;->a:LX/EFb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3e5dfef1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2724854
    iget-object v1, p0, LX/Jhi;->a:LX/EFb;

    check-cast p1, Landroid/widget/Button;

    .line 2724855
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2724856
    iget-object v4, v1, LX/EFb;->b:LX/3A0;

    iget-object v5, v1, LX/EFb;->a:Ljava/lang/String;

    .line 2724857
    iget-object v6, v4, LX/3A0;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EDx;

    .line 2724858
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2724859
    const-string v7, "WebrtcUiHandler"

    const-string p0, "Invalid user id for addNewParticipantToGroupCall"

    invoke-static {v7, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2724860
    :cond_0
    :goto_0
    const v1, -0x7b74390c

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2724861
    :cond_1
    iget-object v7, v6, LX/EDx;->N:LX/2Oi;

    invoke-static {v5}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object p0

    invoke-virtual {v7, p0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v7

    .line 2724862
    if-eqz v7, :cond_0

    .line 2724863
    iget-object p0, v7, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    move-object v7, p0

    .line 2724864
    new-instance p0, Lcom/facebook/messaging/service/model/AddMembersParams;

    iget-object v1, v6, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance p1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/user/model/UserIdentifier;

    const/4 v4, 0x0

    aput-object v7, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {p1, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v1, p1}, Lcom/facebook/messaging/service/model/AddMembersParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;)V

    .line 2724865
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2724866
    const-string v7, "addMembersParams"

    invoke-virtual {v1, v7, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2724867
    iget-object v7, v6, LX/EDx;->cx:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0aG;

    const-string p0, "add_members"

    const p1, -0x6d6bf338

    invoke-static {v7, p0, v1, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v7

    .line 2724868
    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    iput-object v7, v6, LX/EDx;->co:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2724869
    iget-object v7, v6, LX/EDx;->co:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p0, LX/EDg;

    invoke-direct {p0, v6, v5}, LX/EDg;-><init>(LX/EDx;Ljava/lang/String;)V

    invoke-static {v7, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
