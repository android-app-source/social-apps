.class public final LX/K8g;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationSubscribeMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 2775686
    const-class v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationSubscribeMutationModel;

    const v0, -0x3e0f2da0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FBTarotNotificationSubscribeMutation"

    const-string v6, "ea6731fe61b94db5db4f412308ee12f5"

    const-string v7, "tarot_notification_subscribe"

    const-string v8, "0"

    const-string v9, "10155180842736729"

    const/4 v10, 0x0

    .line 2775687
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 2775688
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2775689
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2775690
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2775691
    sparse-switch v0, :sswitch_data_0

    .line 2775692
    :goto_0
    return-object p1

    .line 2775693
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2775694
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5fb57ca -> :sswitch_0
        0x51a05c65 -> :sswitch_1
    .end sparse-switch
.end method
