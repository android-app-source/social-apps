.class public abstract LX/Jqi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DE:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/messaging/sync/delta/handlerbase/MessagesDeltaHandler",
        "<TDE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2740086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDE;)",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<TDE;>;)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;)LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDE;)",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDE;)",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2740084
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2740085
    return-object v0
.end method

.method public d(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDE;)Z"
        }
    .end annotation

    .prologue
    .line 2740083
    const/4 v0, 0x0

    return v0
.end method

.method public e(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDE;)Z"
        }
    .end annotation

    .prologue
    .line 2740082
    const/4 v0, 0x0

    return v0
.end method
