.class public final LX/JyP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JyQ;


# direct methods
.method public constructor <init>(LX/JyQ;)V
    .locals 0

    .prologue
    .line 2754779
    iput-object p1, p0, LX/JyP;->a:LX/JyQ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2754780
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2754781
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2754782
    if-eqz p1, :cond_0

    .line 2754783
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2754784
    if-nez v0, :cond_1

    .line 2754785
    :cond_0
    :goto_0
    return-void

    .line 2754786
    :cond_1
    iget-object v0, p0, LX/JyP;->a:LX/JyQ;

    iget-object v1, v0, LX/JyQ;->d:LX/JyO;

    .line 2754787
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2754788
    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    invoke-interface {v1, v0}, LX/JyO;->a(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;)V

    goto :goto_0
.end method
