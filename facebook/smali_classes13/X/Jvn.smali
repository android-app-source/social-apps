.class public final LX/Jvn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jvo;


# direct methods
.method public constructor <init>(LX/Jvo;)V
    .locals 0

    .prologue
    .line 2750813
    iput-object p1, p0, LX/Jvn;->a:LX/Jvo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2750814
    sget-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->e:Ljava/lang/String;

    const-string v1, "loadMoreFrames failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2750815
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2750816
    check-cast p1, LX/0Px;

    .line 2750817
    iget-object v0, p0, LX/Jvn;->a:LX/Jvo;

    iget-object v0, v0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v0, p1}, LX/89e;->a(LX/0Px;)V

    .line 2750818
    return-void
.end method
