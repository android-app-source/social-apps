.class public final LX/Jj5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbEditText;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;Lcom/facebook/resources/ui/FbEditText;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2727252
    iput-object p1, p0, LX/Jj5;->c:Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;

    iput-object p2, p0, LX/Jj5;->a:Lcom/facebook/resources/ui/FbEditText;

    iput-object p3, p0, LX/Jj5;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    .line 2727253
    iget-object v0, p0, LX/Jj5;->c:Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;

    iget-object v1, p0, LX/Jj5;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2727254
    iput-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    .line 2727255
    iget-object v0, p0, LX/Jj5;->c:Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;

    iget-object v1, p0, LX/Jj5;->b:Landroid/content/Context;

    .line 2727256
    iget-object v2, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->r:LX/Jjd;

    if-eqz v2, :cond_0

    .line 2727257
    iget-object v2, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->r:LX/Jjd;

    iget-object v3, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    .line 2727258
    iget-object v4, v2, LX/Jjd;->a:LX/Jje;

    iget-object v4, v4, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    .line 2727259
    iput-object v3, v4, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    .line 2727260
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2727261
    iget-object v4, v2, LX/Jjd;->a:LX/Jje;

    iget-object v4, v4, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v4, v4, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iget-object v5, v2, LX/Jjd;->a:LX/Jje;

    iget-object v5, v5, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-virtual {v5}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f082dbe

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setPlaceholderText(Ljava/lang/String;)V

    .line 2727262
    :cond_0
    :goto_0
    iget-object v2, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderParams;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2727263
    iget-object v3, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2727264
    iget-object v2, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JjT;

    iget-object v3, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    new-instance v5, LX/Jj9;

    invoke-direct {v5, v0, v1}, LX/Jj9;-><init>(Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;Landroid/content/Context;)V

    const/4 v8, 0x0

    .line 2727265
    iget-object v7, v4, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    move-object v6, v2

    move-object v9, v8

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, LX/JjT;->a(LX/JjT;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;LX/Jj7;)V

    .line 2727266
    :cond_1
    return-void

    .line 2727267
    :cond_2
    iget-object v4, v2, LX/Jjd;->a:LX/Jje;

    iget-object v4, v4, LX/Jje;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v4, v4, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-virtual {v4, v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
