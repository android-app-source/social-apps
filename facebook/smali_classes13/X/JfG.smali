.class public LX/JfG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Jf2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/JfC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721657
    return-void
.end method


# virtual methods
.method public final a(LX/0gc;LX/Jew;)V
    .locals 3

    .prologue
    .line 2721658
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721659
    new-instance v0, LX/6e5;

    invoke-direct {v0}, LX/6e5;-><init>()V

    .line 2721660
    const v1, 0x7f083b52

    .line 2721661
    iput v1, v0, LX/6e5;->a:I

    .line 2721662
    iput-object p2, v0, LX/6e5;->d:Ljava/lang/Object;

    .line 2721663
    new-instance v1, LX/6e3;

    invoke-direct {v1}, LX/6e3;-><init>()V

    const/4 v2, 0x0

    .line 2721664
    iput v2, v1, LX/6e3;->a:I

    .line 2721665
    move-object v1, v1

    .line 2721666
    const v2, 0x7f083b53

    .line 2721667
    iput v2, v1, LX/6e3;->b:I

    .line 2721668
    move-object v1, v1

    .line 2721669
    invoke-virtual {v1}, LX/6e3;->g()Lcom/facebook/messaging/dialog/MenuDialogItem;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6e5;->a(Lcom/facebook/messaging/dialog/MenuDialogItem;)LX/6e5;

    .line 2721670
    new-instance v1, LX/6e3;

    invoke-direct {v1}, LX/6e3;-><init>()V

    const/4 v2, 0x1

    .line 2721671
    iput v2, v1, LX/6e3;->a:I

    .line 2721672
    move-object v1, v1

    .line 2721673
    const v2, 0x7f083b54

    .line 2721674
    iput v2, v1, LX/6e3;->b:I

    .line 2721675
    move-object v1, v1

    .line 2721676
    invoke-virtual {v1}, LX/6e3;->g()Lcom/facebook/messaging/dialog/MenuDialogItem;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6e5;->a(Lcom/facebook/messaging/dialog/MenuDialogItem;)LX/6e5;

    .line 2721677
    new-instance v1, LX/6e3;

    invoke-direct {v1}, LX/6e3;-><init>()V

    const/4 v2, 0x2

    .line 2721678
    iput v2, v1, LX/6e3;->a:I

    .line 2721679
    move-object v1, v1

    .line 2721680
    const v2, 0x7f083b55

    .line 2721681
    iput v2, v1, LX/6e3;->b:I

    .line 2721682
    move-object v1, v1

    .line 2721683
    invoke-virtual {v1}, LX/6e3;->g()Lcom/facebook/messaging/dialog/MenuDialogItem;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6e5;->a(Lcom/facebook/messaging/dialog/MenuDialogItem;)LX/6e5;

    .line 2721684
    invoke-virtual {v0}, LX/6e5;->e()Lcom/facebook/messaging/dialog/MenuDialogParams;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/dialog/MenuDialogFragment;->a(Lcom/facebook/messaging/dialog/MenuDialogParams;)Lcom/facebook/messaging/dialog/MenuDialogFragment;

    move-result-object v0

    .line 2721685
    iget-object v1, p0, LX/JfG;->c:LX/JfC;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721686
    new-instance v1, LX/JfF;

    invoke-direct {v1, p0}, LX/JfF;-><init>(LX/JfG;)V

    move-object v1, v1

    .line 2721687
    iput-object v1, v0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->n:LX/6e1;

    .line 2721688
    const-string v1, "messenger_inbox_ads_setting_dialog"

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2721689
    return-void
.end method
