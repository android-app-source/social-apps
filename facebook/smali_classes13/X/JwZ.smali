.class public LX/JwZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/JwZ;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cw;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2cw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2751901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2751902
    iput-object p1, p0, LX/JwZ;->a:LX/0Ot;

    .line 2751903
    iput-object p2, p0, LX/JwZ;->b:LX/0Ot;

    .line 2751904
    return-void
.end method

.method public static a(LX/0QB;)LX/JwZ;
    .locals 5

    .prologue
    .line 2751905
    sget-object v0, LX/JwZ;->c:LX/JwZ;

    if-nez v0, :cond_1

    .line 2751906
    const-class v1, LX/JwZ;

    monitor-enter v1

    .line 2751907
    :try_start_0
    sget-object v0, LX/JwZ;->c:LX/JwZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2751908
    if-eqz v2, :cond_0

    .line 2751909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2751910
    new-instance v3, LX/JwZ;

    const/16 v4, 0xf57

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2ba

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/JwZ;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2751911
    move-object v0, v3

    .line 2751912
    sput-object v0, LX/JwZ;->c:LX/JwZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2751913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2751914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2751915
    :cond_1
    sget-object v0, LX/JwZ;->c:LX/JwZ;

    return-object v0

    .line 2751916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2751917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/4Iv;)V
    .locals 4

    .prologue
    .line 2751918
    :try_start_0
    iget-object v0, p0, LX/JwZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cw;

    iget-object v1, p0, LX/JwZ;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-virtual {v1, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2cx;->BLE:LX/2cx;

    .line 2751919
    sget-object v3, LX/3FC;->START_PAGE_LOOKUP:LX/3FC;

    .line 2751920
    invoke-virtual {v3}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2751921
    const-string p1, "input_params"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2751922
    invoke-static {v0, v3, p0, v2}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 2751923
    return-void

    .line 2751924
    :catch_0
    move-exception v0

    .line 2751925
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
