.class public LX/Jik;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/JhS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/text/Collator;


# direct methods
.method public constructor <init>(Ljava/text/Collator;)V
    .locals 0
    .param p1    # Ljava/text/Collator;
        .annotation runtime Lcom/facebook/common/locale/ForPhonebookBucketSorting;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2726402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2726403
    iput-object p1, p0, LX/Jik;->a:Ljava/text/Collator;

    .line 2726404
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2726405
    check-cast p1, LX/JhS;

    check-cast p2, LX/JhS;

    .line 2726406
    iget-object v0, p1, LX/JhS;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2726407
    iget-object v1, p2, LX/JhS;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2726408
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2726409
    const/4 v0, 0x0

    .line 2726410
    :goto_0
    return v0

    .line 2726411
    :cond_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2726412
    const/4 v0, 0x1

    goto :goto_0

    .line 2726413
    :cond_1
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2726414
    const/4 v0, -0x1

    goto :goto_0

    .line 2726415
    :cond_2
    iget-object v2, p0, LX/Jik;->a:Ljava/text/Collator;

    invoke-virtual {v2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
