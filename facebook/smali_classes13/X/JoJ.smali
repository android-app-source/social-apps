.class public final LX/JoJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic b:Z

.field public final synthetic c:LX/JoM;

.field public final synthetic d:LX/JoN;


# direct methods
.method public constructor <init>(LX/JoN;Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V
    .locals 0

    .prologue
    .line 2735352
    iput-object p1, p0, LX/JoJ;->d:LX/JoN;

    iput-object p2, p0, LX/JoJ;->a:Lcom/facebook/messaging/model/messages/Message;

    iput-boolean p3, p0, LX/JoJ;->b:Z

    iput-object p4, p0, LX/JoJ;->c:LX/JoM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 4

    .prologue
    .line 2735353
    iget-object v0, p0, LX/JoJ;->d:LX/JoN;

    invoke-virtual {v0}, LX/JoN;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2735354
    iget-object v0, p0, LX/JoJ;->d:LX/JoN;

    iget-object v0, v0, LX/JoN;->c:Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    iget-object v1, p0, LX/JoJ;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-boolean v2, p0, LX/JoJ;->b:Z

    iget-object v3, p0, LX/JoJ;->c:LX/JoM;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V

    .line 2735355
    const/4 v0, 0x1

    return v0
.end method
