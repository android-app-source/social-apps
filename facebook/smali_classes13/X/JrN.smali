.class public LX/JrN;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/2N4;

.field private final b:LX/FDs;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/Jrc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743167
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrN;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2N4;LX/FDs;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Jrc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2743161
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2743162
    iput-object p1, p0, LX/JrN;->a:LX/2N4;

    .line 2743163
    iput-object p2, p0, LX/JrN;->b:LX/FDs;

    .line 2743164
    iput-object p3, p0, LX/JrN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2743165
    iput-object p4, p0, LX/JrN;->d:LX/Jrc;

    .line 2743166
    return-void
.end method

.method public static a(LX/0QB;)LX/JrN;
    .locals 10

    .prologue
    .line 2743132
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2743133
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2743134
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2743135
    if-nez v1, :cond_0

    .line 2743136
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743137
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2743138
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2743139
    sget-object v1, LX/JrN;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2743140
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2743141
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2743142
    :cond_1
    if-nez v1, :cond_4

    .line 2743143
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2743144
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2743145
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2743146
    new-instance p0, LX/JrN;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v1

    check-cast v1, LX/2N4;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v7

    check-cast v7, LX/FDs;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v9

    check-cast v9, LX/Jrc;

    invoke-direct {p0, v1, v7, v8, v9}, LX/JrN;-><init>(LX/2N4;LX/FDs;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Jrc;)V

    .line 2743147
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2743148
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2743149
    if-nez v1, :cond_2

    .line 2743150
    sget-object v0, LX/JrN;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrN;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2743151
    :goto_1
    if-eqz v0, :cond_3

    .line 2743152
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743153
    :goto_3
    check-cast v0, LX/JrN;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2743154
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2743155
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2743156
    :catchall_1
    move-exception v0

    .line 2743157
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743158
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2743159
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2743160
    :cond_2
    :try_start_8
    sget-object v0, LX/JrN;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrN;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2743168
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2743169
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2743119
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->m()LX/6kQ;

    move-result-object v0

    .line 2743120
    iget-object v1, p0, LX/JrN;->a:LX/2N4;

    iget-object v2, p0, LX/JrN;->d:LX/Jrc;

    iget-object v3, v0, LX/6kQ;->threadKey:LX/6l9;

    invoke-virtual {v2, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    .line 2743121
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2743122
    if-eqz v2, :cond_0

    .line 2743123
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3}, LX/0db;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v3

    .line 2743124
    iget-object v4, p0, LX/JrN;->b:LX/FDs;

    iget-object v5, v0, LX/6kQ;->expireTime:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v5

    iget-wide v6, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    .line 2743125
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v1

    .line 2743126
    iput-object v5, v1, LX/6g6;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2743127
    move-object v1, v1

    .line 2743128
    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2743129
    invoke-static {v4, v1, v6, v7}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2743130
    iget-object v1, p0, LX/JrN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v0, v0, LX/6kQ;->expireTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v1, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2743131
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2743118
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2743116
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2743117
    return-object v0
.end method
