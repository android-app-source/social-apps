.class public LX/JnW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation
.end field

.field private final b:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation
.end field

.field public final c:LX/0tX;

.field private final d:LX/1mR;

.field private final e:LX/IiC;

.field public final f:LX/JnX;

.field private g:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734081
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JnW;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0tX;LX/1mR;LX/IiC;LX/JnX;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2734156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734157
    const/4 v0, 0x0

    iput-object v0, p0, LX/JnW;->h:LX/1Mv;

    .line 2734158
    iput-object p1, p0, LX/JnW;->a:Ljava/util/concurrent/ExecutorService;

    .line 2734159
    iput-object p2, p0, LX/JnW;->b:LX/0Xl;

    .line 2734160
    iput-object p3, p0, LX/JnW;->c:LX/0tX;

    .line 2734161
    iput-object p4, p0, LX/JnW;->d:LX/1mR;

    .line 2734162
    iput-object p5, p0, LX/JnW;->e:LX/IiC;

    .line 2734163
    iput-object p6, p0, LX/JnW;->f:LX/JnX;

    .line 2734164
    iget-object v0, p0, LX/JnW;->g:LX/0Yb;

    if-nez v0, :cond_0

    .line 2734165
    iget-object v0, p0, LX/JnW;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.messaging.sync.delta.handler.DeltaFolderCountHandler.pending_folder_count_change"

    new-instance v2, LX/JnS;

    invoke-direct {v2, p0}, LX/JnS;-><init>(LX/JnW;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/JnW;->g:LX/0Yb;

    .line 2734166
    :cond_0
    iget-object v0, p0, LX/JnW;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2734167
    return-void
.end method

.method public static a(LX/0QB;)LX/JnW;
    .locals 7

    .prologue
    .line 2734129
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2734130
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2734131
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2734132
    if-nez v1, :cond_0

    .line 2734133
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734134
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2734135
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2734136
    sget-object v1, LX/JnW;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2734137
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2734138
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2734139
    :cond_1
    if-nez v1, :cond_4

    .line 2734140
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2734141
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2734142
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JnW;->b(LX/0QB;)LX/JnW;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2734143
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2734144
    if-nez v1, :cond_2

    .line 2734145
    sget-object v0, LX/JnW;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JnW;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2734146
    :goto_1
    if-eqz v0, :cond_3

    .line 2734147
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2734148
    :goto_3
    check-cast v0, LX/JnW;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2734149
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2734150
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2734151
    :catchall_1
    move-exception v0

    .line 2734152
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2734153
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2734154
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2734155
    :cond_2
    :try_start_8
    sget-object v0, LX/JnW;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JnW;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/JnW;
    .locals 11

    .prologue
    .line 2734122
    new-instance v0, LX/JnW;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v4

    check-cast v4, LX/1mR;

    invoke-static {p0}, LX/IiC;->a(LX/0QB;)LX/IiC;

    move-result-object v5

    check-cast v5, LX/IiC;

    .line 2734123
    new-instance v7, LX/JnX;

    invoke-direct {v7}, LX/JnX;-><init>()V

    .line 2734124
    const/16 v6, 0x259

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v6, 0x15e7

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v6, 0x29c3

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v6

    check-cast v6, LX/2Og;

    .line 2734125
    iput-object v8, v7, LX/JnX;->a:LX/0Ot;

    iput-object v9, v7, LX/JnX;->b:LX/0Or;

    iput-object v10, v7, LX/JnX;->c:LX/0Ot;

    iput-object v6, v7, LX/JnX;->d:LX/2Og;

    .line 2734126
    move-object v6, v7

    .line 2734127
    check-cast v6, LX/JnX;

    invoke-direct/range {v0 .. v6}, LX/JnW;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0tX;LX/1mR;LX/IiC;LX/JnX;)V

    .line 2734128
    return-object v0
.end method

.method private static declared-synchronized c(LX/JnW;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2734106
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/JnW;->d(LX/JnW;)Z

    move-result v0

    .line 2734107
    if-eqz v0, :cond_0

    .line 2734108
    iget-object v0, p0, LX/JnW;->h:LX/1Mv;

    .line 2734109
    iget-object v1, v0, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 2734110
    invoke-static {v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2734111
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2734112
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/JnW;->f(LX/JnW;)LX/0zO;

    move-result-object v3

    .line 2734113
    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 2734114
    const-wide/32 v5, 0xa8c0

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    .line 2734115
    iget-object v4, p0, LX/JnW;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2734116
    new-instance v4, LX/JnV;

    invoke-direct {v4, p0}, LX/JnV;-><init>(LX/JnW;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2734117
    new-instance v1, LX/JnU;

    invoke-direct {v1, p0}, LX/JnU;-><init>(LX/JnW;)V

    .line 2734118
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    iput-object v2, p0, LX/JnW;->h:LX/1Mv;

    .line 2734119
    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2734120
    invoke-static {v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 2734121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static d(LX/JnW;)Z
    .locals 1
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 2734105
    iget-object v0, p0, LX/JnW;->h:LX/1Mv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/JnW;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2734098
    new-instance v0, LX/IiD;

    invoke-direct {v0}, LX/IiD;-><init>()V

    move-object v1, v0

    .line 2734099
    iget-object v0, p0, LX/JnW;->e:LX/IiC;

    .line 2734100
    iget-object v2, v0, LX/IiC;->c:LX/0ad;

    sget-short v3, LX/IiB;->a:S

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 2734101
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2734102
    :goto_0
    const-string v2, "max_names_count"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2734103
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    .line 2734104
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2734090
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/JnW;->i:LX/0QK;

    if-nez v0, :cond_0

    .line 2734091
    new-instance v0, LX/JnT;

    invoke-direct {v0, p0}, LX/JnT;-><init>(LX/JnW;)V

    iput-object v0, p0, LX/JnW;->i:LX/0QK;

    .line 2734092
    :cond_0
    invoke-static {p0}, LX/JnW;->d(LX/JnW;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2734093
    iget-object v0, p0, LX/JnW;->h:LX/1Mv;

    .line 2734094
    iget-object v1, v0, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 2734095
    iget-object v1, p0, LX/JnW;->i:LX/0QK;

    iget-object v2, p0, LX/JnW;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2734096
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {p0}, LX/JnW;->c(LX/JnW;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, LX/JnW;->i:LX/0QK;

    iget-object v2, p0, LX/JnW;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 2734097
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2734082
    monitor-enter p0

    .line 2734083
    :try_start_0
    iget-object v0, p0, LX/JnW;->h:LX/1Mv;

    if-eqz v0, :cond_0

    .line 2734084
    iget-object v0, p0, LX/JnW;->h:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 2734085
    const/4 v0, 0x0

    iput-object v0, p0, LX/JnW;->h:LX/1Mv;

    .line 2734086
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734087
    iget-object v0, p0, LX/JnW;->d:LX/1mR;

    invoke-static {p0}, LX/JnW;->f(LX/JnW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2734088
    return-void

    .line 2734089
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
