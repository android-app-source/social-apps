.class public LX/Jqd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7GK",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Jqd;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739825
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqd;
    .locals 3

    .prologue
    .line 2739826
    sget-object v0, LX/Jqd;->a:LX/Jqd;

    if-nez v0, :cond_1

    .line 2739827
    const-class v1, LX/Jqd;

    monitor-enter v1

    .line 2739828
    :try_start_0
    sget-object v0, LX/Jqd;->a:LX/Jqd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2739829
    if-eqz v2, :cond_0

    .line 2739830
    :try_start_1
    new-instance v0, LX/Jqd;

    invoke-direct {v0}, LX/Jqd;-><init>()V

    .line 2739831
    move-object v0, v0

    .line 2739832
    sput-object v0, LX/Jqd;->a:LX/Jqd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2739833
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2739834
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2739835
    :cond_1
    sget-object v0, LX/Jqd;->a:LX/Jqd;

    return-object v0

    .line 2739836
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2739837
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2739838
    check-cast p1, LX/6kW;

    const/4 v0, 0x1

    .line 2739839
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 2739840
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2739841
    check-cast p1, LX/6kW;

    .line 2739842
    invoke-virtual {p1}, LX/6kW;->c()LX/6k5;

    move-result-object v0

    iget-object v0, v0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    return-object v0
.end method
