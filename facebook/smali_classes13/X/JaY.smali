.class public final LX/JaY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JaX;


# instance fields
.field private a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2715552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2715553
    iput-object p1, p0, LX/JaY;->a:Landroid/content/Intent;

    .line 2715554
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2715555
    iget-object v0, p0, LX/JaY;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_navigation_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2715556
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2715557
    if-eqz v0, :cond_0

    .line 2715558
    const-string v2, "navSource"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2715559
    :cond_0
    invoke-static {}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->m()LX/F5b;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v2

    const-string v3, "/groups_discovery"

    .line 2715560
    iput-object v3, v2, LX/98r;->a:Ljava/lang/String;

    .line 2715561
    move-object v2, v2

    .line 2715562
    const-string v3, "FBGroupsDiscoveryRoute"

    .line 2715563
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 2715564
    move-object v2, v2

    .line 2715565
    const/4 v3, 0x1

    .line 2715566
    iput-boolean v3, v2, LX/98r;->m:Z

    .line 2715567
    move-object v2, v2

    .line 2715568
    invoke-virtual {v2}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/F5b;->a(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/F5b;->b(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    const v1, 0x200010

    .line 2715569
    const-string v2, "cold_perf_marker"

    const-string v3, "perfMarkerColdStart"

    .line 2715570
    iget-object v4, v0, LX/F5b;->a:Landroid/os/Bundle;

    const-string v5, "init_props"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 2715571
    if-nez v4, :cond_1

    .line 2715572
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2715573
    :cond_1
    if-eqz v1, :cond_2

    .line 2715574
    iget-object v5, v0, LX/F5b;->a:Landroid/os/Bundle;

    invoke-virtual {v5, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2715575
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2715576
    :cond_2
    iget-object v5, v0, LX/F5b;->a:Landroid/os/Bundle;

    const-string p0, "init_props"

    invoke-virtual {v5, p0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2715577
    move-object v2, v0

    .line 2715578
    move-object v0, v2

    .line 2715579
    invoke-virtual {v0}, LX/F5b;->a()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2715580
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2715581
    const v0, 0x7f083ab8

    return v0
.end method

.method public final d()LX/Jac;
    .locals 1

    .prologue
    .line 2715582
    sget-object v0, LX/Jac;->DISCOVER:LX/Jac;

    return-object v0
.end method
