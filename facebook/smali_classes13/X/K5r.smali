.class public final LX/K5r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardCover;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardCover;)V
    .locals 0

    .prologue
    .line 2770692
    iput-object p1, p0, LX/K5r;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardCover;B)V
    .locals 0

    .prologue
    .line 2770691
    invoke-direct {p0, p1}, LX/K5r;-><init>(Lcom/facebook/tarot/cards/TarotCardCover;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2770683
    iget-object v0, p0, LX/K5r;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->a()V

    .line 2770684
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2770689
    iget-object v0, p0, LX/K5r;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/SlideshowView;->a(Ljava/util/Collection;)V

    .line 2770690
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2770687
    iget-object v0, p0, LX/K5r;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->b()V

    .line 2770688
    return-void
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/tarot/data/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2770685
    iget-object v0, p0, LX/K5r;->a:Lcom/facebook/tarot/cards/TarotCardCover;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/SlideshowView;->b(Ljava/util/Collection;)V

    .line 2770686
    return-void
.end method
