.class public final LX/K5h;
.super LX/8qY;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;)V
    .locals 0

    .prologue
    .line 2770577
    iput-object p1, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    invoke-direct {p0, p1}, LX/8qY;-><init>(Lcom/facebook/widget/popover/PopoverFragment;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2770578
    invoke-super {p0}, LX/8qY;->a()V

    .line 2770579
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    if-eqz v0, :cond_0

    .line 2770580
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    .line 2770581
    invoke-virtual {v0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->r()V

    .line 2770582
    :cond_0
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 2770583
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->b()LX/K5U;

    move-result-object v0

    .line 2770584
    if-eqz v0, :cond_0

    .line 2770585
    invoke-interface {v0, p3}, LX/K5U;->a(LX/31M;)Z

    move-result v0

    .line 2770586
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2770587
    invoke-super {p0}, LX/8qY;->b()V

    .line 2770588
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    if-eqz v0, :cond_0

    .line 2770589
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    .line 2770590
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->n()V

    .line 2770591
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2770592
    invoke-super {p0}, LX/8qY;->c()V

    .line 2770593
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    if-eqz v0, :cond_0

    .line 2770594
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2770595
    invoke-super {p0}, LX/8qY;->d()V

    .line 2770596
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    if-eqz v0, :cond_0

    .line 2770597
    iget-object v0, p0, LX/K5h;->b:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    iget-object v0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    .line 2770598
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->o()V

    .line 2770599
    invoke-virtual {v0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->s()V

    .line 2770600
    :cond_0
    return-void
.end method
