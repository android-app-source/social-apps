.class public LX/Js0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Js0;


# instance fields
.field private final a:LX/Jrt;

.field private final b:LX/2Mk;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0SG;


# direct methods
.method public constructor <init>(LX/Jrt;LX/2Mk;Landroid/content/res/Resources;LX/0SG;)V
    .locals 0
    .param p4    # LX/0SG;
        .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2744962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2744963
    iput-object p1, p0, LX/Js0;->a:LX/Jrt;

    .line 2744964
    iput-object p2, p0, LX/Js0;->b:LX/2Mk;

    .line 2744965
    iput-object p3, p0, LX/Js0;->c:Landroid/content/res/Resources;

    .line 2744966
    iput-object p4, p0, LX/Js0;->d:LX/0SG;

    .line 2744967
    return-void
.end method

.method public static a(LX/0QB;)LX/Js0;
    .locals 7

    .prologue
    .line 2744979
    sget-object v0, LX/Js0;->e:LX/Js0;

    if-nez v0, :cond_1

    .line 2744980
    const-class v1, LX/Js0;

    monitor-enter v1

    .line 2744981
    :try_start_0
    sget-object v0, LX/Js0;->e:LX/Js0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2744982
    if-eqz v2, :cond_0

    .line 2744983
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2744984
    new-instance p0, LX/Js0;

    invoke-static {v0}, LX/Jrt;->a(LX/0QB;)LX/Jrt;

    move-result-object v3

    check-cast v3, LX/Jrt;

    invoke-static {v0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v4

    check-cast v4, LX/2Mk;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Js0;-><init>(LX/Jrt;LX/2Mk;Landroid/content/res/Resources;LX/0SG;)V

    .line 2744985
    move-object v0, p0

    .line 2744986
    sput-object v0, LX/Js0;->e:LX/Js0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2744987
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2744988
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2744989
    :cond_1
    sget-object v0, LX/Js0;->e:LX/Js0;

    return-object v0

    .line 2744990
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2744991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Js0;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 6
    .param p0    # LX/Js0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2744974
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/2Mk;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/2Mk;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/2Mk;->t(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/2Mk;->t(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Js0;->a:LX/Jrt;

    invoke-virtual {v0, p1}, LX/Jrt;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Js0;->a:LX/Jrt;

    invoke-virtual {v0, p2}, LX/Jrt;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2744975
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 2744976
    if-eqz v0, :cond_0

    .line 2744977
    invoke-static {p1, p2}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2744978
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/messaging/model/messages/Message;)Z
    .locals 6
    .param p2    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2744969
    if-nez p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    .line 2744970
    :cond_1
    :goto_0
    return v0

    .line 2744971
    :cond_2
    invoke-static {p0}, LX/2Mk;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2744972
    invoke-static {p0, p2}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v2

    .line 2744973
    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)LX/Jrz;
    .locals 3
    .param p3    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2744968
    invoke-static {p0, p1, p4}, LX/Js0;->a(LX/Js0;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    invoke-static {p0, p1, p3}, LX/Js0;->a(LX/Js0;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    invoke-static {p1, p2, p3}, LX/Js0;->a(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    invoke-static {v0, v1, v2}, LX/Jrz;->forGrouping(ZZZ)LX/Jrz;

    move-result-object v0

    return-object v0
.end method
