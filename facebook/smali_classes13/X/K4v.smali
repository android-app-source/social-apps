.class public final enum LX/K4v;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K4v;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K4v;

.field public static final enum ADD:LX/K4v;

.field public static final enum BLEND:LX/K4v;

.field public static final enum MULTIPLY:LX/K4v;

.field public static final enum NORMAL:LX/K4v;

.field public static final enum PREMULT_ALPHA:LX/K4v;

.field public static final enum REVSUBTRACT:LX/K4v;

.field public static final enum SCREEN:LX/K4v;

.field public static final enum SUBTRACT:LX/K4v;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2769615
    new-instance v0, LX/K4v;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->NORMAL:LX/K4v;

    .line 2769616
    new-instance v0, LX/K4v;

    const-string v1, "BLEND"

    invoke-direct {v0, v1, v4}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->BLEND:LX/K4v;

    .line 2769617
    new-instance v0, LX/K4v;

    const-string v1, "MULTIPLY"

    invoke-direct {v0, v1, v5}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->MULTIPLY:LX/K4v;

    .line 2769618
    new-instance v0, LX/K4v;

    const-string v1, "SCREEN"

    invoke-direct {v0, v1, v6}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->SCREEN:LX/K4v;

    .line 2769619
    new-instance v0, LX/K4v;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v7}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->ADD:LX/K4v;

    .line 2769620
    new-instance v0, LX/K4v;

    const-string v1, "SUBTRACT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->SUBTRACT:LX/K4v;

    .line 2769621
    new-instance v0, LX/K4v;

    const-string v1, "REVSUBTRACT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->REVSUBTRACT:LX/K4v;

    .line 2769622
    new-instance v0, LX/K4v;

    const-string v1, "PREMULT_ALPHA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/K4v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4v;->PREMULT_ALPHA:LX/K4v;

    .line 2769623
    const/16 v0, 0x8

    new-array v0, v0, [LX/K4v;

    sget-object v1, LX/K4v;->NORMAL:LX/K4v;

    aput-object v1, v0, v3

    sget-object v1, LX/K4v;->BLEND:LX/K4v;

    aput-object v1, v0, v4

    sget-object v1, LX/K4v;->MULTIPLY:LX/K4v;

    aput-object v1, v0, v5

    sget-object v1, LX/K4v;->SCREEN:LX/K4v;

    aput-object v1, v0, v6

    sget-object v1, LX/K4v;->ADD:LX/K4v;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/K4v;->SUBTRACT:LX/K4v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/K4v;->REVSUBTRACT:LX/K4v;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/K4v;->PREMULT_ALPHA:LX/K4v;

    aput-object v2, v0, v1

    sput-object v0, LX/K4v;->$VALUES:[LX/K4v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2769624
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convert(Ljava/lang/String;)LX/K4v;
    .locals 2

    .prologue
    .line 2769625
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2769626
    sget-object v0, LX/K4v;->NORMAL:LX/K4v;

    :goto_1
    return-object v0

    .line 2769627
    :sswitch_0
    const-string v1, "normal"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "blend"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "multiply"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "screen"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "add"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "subtract"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "revsubtract"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "premultiply"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    .line 2769628
    :pswitch_0
    sget-object v0, LX/K4v;->NORMAL:LX/K4v;

    goto :goto_1

    .line 2769629
    :pswitch_1
    sget-object v0, LX/K4v;->BLEND:LX/K4v;

    goto :goto_1

    .line 2769630
    :pswitch_2
    sget-object v0, LX/K4v;->MULTIPLY:LX/K4v;

    goto :goto_1

    .line 2769631
    :pswitch_3
    sget-object v0, LX/K4v;->SCREEN:LX/K4v;

    goto :goto_1

    .line 2769632
    :pswitch_4
    sget-object v0, LX/K4v;->ADD:LX/K4v;

    goto :goto_1

    .line 2769633
    :pswitch_5
    sget-object v0, LX/K4v;->SUBTRACT:LX/K4v;

    goto :goto_1

    .line 2769634
    :pswitch_6
    sget-object v0, LX/K4v;->REVSUBTRACT:LX/K4v;

    goto :goto_1

    .line 2769635
    :pswitch_7
    sget-object v0, LX/K4v;->PREMULT_ALPHA:LX/K4v;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7acce4ec -> :sswitch_5
        -0x74707f49 -> :sswitch_6
        -0x6d44fa39 -> :sswitch_7
        -0x3df94319 -> :sswitch_0
        -0x361a3f94 -> :sswitch_3
        0x178a1 -> :sswitch_4
        0x597a051 -> :sswitch_1
        0x26f8a624 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/K4v;
    .locals 1

    .prologue
    .line 2769636
    const-class v0, LX/K4v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K4v;

    return-object v0
.end method

.method public static values()[LX/K4v;
    .locals 1

    .prologue
    .line 2769637
    sget-object v0, LX/K4v;->$VALUES:[LX/K4v;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K4v;

    return-object v0
.end method
