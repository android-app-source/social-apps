.class public LX/Ja1;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ja3;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ja1",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ja3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709422
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2709423
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Ja1;->b:LX/0Zi;

    .line 2709424
    iput-object p1, p0, LX/Ja1;->a:LX/0Ot;

    .line 2709425
    return-void
.end method

.method public static a(LX/0QB;)LX/Ja1;
    .locals 4

    .prologue
    .line 2709426
    const-class v1, LX/Ja1;

    monitor-enter v1

    .line 2709427
    :try_start_0
    sget-object v0, LX/Ja1;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709428
    sput-object v2, LX/Ja1;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709429
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709430
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709431
    new-instance v3, LX/Ja1;

    const/16 p0, 0x2255

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ja1;-><init>(LX/0Ot;)V

    .line 2709432
    move-object v0, v3

    .line 2709433
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709434
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ja1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709435
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709436
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2709437
    check-cast p2, LX/Ja0;

    .line 2709438
    iget-object v0, p0, LX/Ja1;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ja3;

    iget-object v1, p2, LX/Ja0;->a:Ljava/lang/String;

    iget-object v2, p2, LX/Ja0;->b:Ljava/lang/String;

    const/4 p2, 0x5

    const/4 p0, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 2709439
    const v3, 0x7f0a028a

    .line 2709440
    const v4, 0x49010ec8    # 528620.5f

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2709441
    invoke-static {v0, p1, v1, v3, v4}, LX/Ja3;->a(LX/Ja3;LX/1De;Ljava/lang/String;ILX/1dQ;)LX/1Di;

    move-result-object v3

    const v4, 0x7f020b08

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b262b

    invoke-interface {v3, v6, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b262b

    invoke-interface {v3, p2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    .line 2709442
    const v4, 0x7f0a0a09

    .line 2709443
    const v5, 0x49012057

    const/4 v1, 0x0

    invoke-static {p1, v5, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2709444
    invoke-static {v0, p1, v2, v4, v5}, LX/Ja3;->a(LX/Ja3;LX/1De;Ljava/lang/String;ILX/1dQ;)LX/1Di;

    move-result-object v4

    const v5, 0x7f020b0b

    invoke-interface {v4, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b262b

    invoke-interface {v4, v6, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b262b

    invoke-interface {v4, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    .line 2709445
    iget-object v5, v0, LX/Ja3;->c:LX/0xW;

    invoke-virtual {v5}, LX/0xW;->t()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2709446
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v7, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v6, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, p0, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    .line 2709447
    :goto_0
    move-object v0, v3

    .line 2709448
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v7, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v6, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, p0, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2709449
    invoke-static {}, LX/1dS;->b()V

    .line 2709450
    iget v0, p1, LX/1dQ;->b:I

    .line 2709451
    sparse-switch v0, :sswitch_data_0

    .line 2709452
    :goto_0
    return-object v2

    .line 2709453
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2709454
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2709455
    check-cast v1, LX/Ja0;

    .line 2709456
    iget-object v3, p0, LX/Ja1;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ja3;

    iget-object v4, v1, LX/Ja0;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object p1, v1, LX/Ja0;->d:LX/1Pr;

    iget-object p2, v1, LX/Ja0;->e:Ljava/lang/String;

    .line 2709457
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    invoke-static {v4, p1, p2, p0}, LX/Ja3;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    .line 2709458
    iget-object p0, v3, LX/Ja3;->c:LX/0xW;

    invoke-virtual {p0}, LX/0xW;->u()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2709459
    invoke-static {v3, v4, p1}, LX/Ja3;->a(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;)V

    .line 2709460
    :cond_0
    sget-object p0, LX/2na;->CONFIRM:LX/2na;

    invoke-static {v3, v4, p1, p2, p0}, LX/Ja3;->a(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;LX/2na;)V

    .line 2709461
    check-cast p1, LX/2kk;

    sget-object p0, LX/Cfc;->CONFIRM_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

    invoke-virtual {p0}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, v4, p0}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2709462
    goto :goto_0

    .line 2709463
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2709464
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2709465
    check-cast v1, LX/Ja0;

    .line 2709466
    iget-object v3, p0, LX/Ja1;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ja3;

    iget-object v4, v1, LX/Ja0;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object p1, v1, LX/Ja0;->d:LX/1Pr;

    iget-object p2, v1, LX/Ja0;->e:Ljava/lang/String;

    .line 2709467
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    invoke-static {v4, p1, p2, p0}, LX/Ja3;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    .line 2709468
    iget-object p0, v3, LX/Ja3;->c:LX/0xW;

    invoke-virtual {p0}, LX/0xW;->u()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2709469
    invoke-static {v3, v4, p1}, LX/Ja3;->a(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;)V

    .line 2709470
    :cond_1
    sget-object p0, LX/2na;->REJECT:LX/2na;

    invoke-static {v3, v4, p1, p2, p0}, LX/Ja3;->a(LX/Ja3;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pr;Ljava/lang/String;LX/2na;)V

    .line 2709471
    check-cast p1, LX/2kk;

    sget-object p0, LX/Cfc;->DELETE_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

    invoke-virtual {p0}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, v4, p0}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2709472
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x49010ec8 -> :sswitch_0
        0x49012057 -> :sswitch_1
    .end sparse-switch
.end method
