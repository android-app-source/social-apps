.class public LX/JrX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final m:Ljava/lang/Object;


# instance fields
.field public a:Ljava/util/Set;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Jqh;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqx;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqz;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr4;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr8;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrF;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrL;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrM;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JrQ;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743651
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrX;->m:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2743652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2743653
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743654
    iput-object v0, p0, LX/JrX;->b:LX/0Ot;

    .line 2743655
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743656
    iput-object v0, p0, LX/JrX;->c:LX/0Ot;

    .line 2743657
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743658
    iput-object v0, p0, LX/JrX;->d:LX/0Ot;

    .line 2743659
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743660
    iput-object v0, p0, LX/JrX;->e:LX/0Ot;

    .line 2743661
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743662
    iput-object v0, p0, LX/JrX;->f:LX/0Ot;

    .line 2743663
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743664
    iput-object v0, p0, LX/JrX;->g:LX/0Ot;

    .line 2743665
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743666
    iput-object v0, p0, LX/JrX;->h:LX/0Ot;

    .line 2743667
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743668
    iput-object v0, p0, LX/JrX;->i:LX/0Ot;

    .line 2743669
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743670
    iput-object v0, p0, LX/JrX;->j:LX/0Ot;

    .line 2743671
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743672
    iput-object v0, p0, LX/JrX;->k:LX/0Ot;

    .line 2743673
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743674
    iput-object v0, p0, LX/JrX;->l:LX/0Ot;

    .line 2743675
    return-void
.end method

.method public static a(LX/0QB;)LX/JrX;
    .locals 7

    .prologue
    .line 2743676
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2743677
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2743678
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2743679
    if-nez v1, :cond_0

    .line 2743680
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743681
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2743682
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2743683
    sget-object v1, LX/JrX;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2743684
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2743685
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2743686
    :cond_1
    if-nez v1, :cond_4

    .line 2743687
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2743688
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2743689
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrX;->b(LX/0QB;)LX/JrX;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2743690
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2743691
    if-nez v1, :cond_2

    .line 2743692
    sget-object v0, LX/JrX;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrX;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2743693
    :goto_1
    if-eqz v0, :cond_3

    .line 2743694
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743695
    :goto_3
    check-cast v0, LX/JrX;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2743696
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2743697
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2743698
    :catchall_1
    move-exception v0

    .line 2743699
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743700
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2743701
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2743702
    :cond_2
    :try_start_8
    sget-object v0, LX/JrX;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrX;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/JrX;
    .locals 13

    .prologue
    .line 2743703
    new-instance v0, LX/JrX;

    invoke-direct {v0}, LX/JrX;-><init>()V

    .line 2743704
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/JrS;

    invoke-direct {v3, p0}, LX/JrS;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 2743705
    const/16 v2, 0x29d5

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x29d7

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x29db

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x29df

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x29e6

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x29ec

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x29ed

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x29f1

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x259

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xac0

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xdf4

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    .line 2743706
    iput-object v1, v0, LX/JrX;->a:Ljava/util/Set;

    iput-object v2, v0, LX/JrX;->b:LX/0Ot;

    iput-object v3, v0, LX/JrX;->c:LX/0Ot;

    iput-object v4, v0, LX/JrX;->d:LX/0Ot;

    iput-object v5, v0, LX/JrX;->e:LX/0Ot;

    iput-object v6, v0, LX/JrX;->f:LX/0Ot;

    iput-object v7, v0, LX/JrX;->g:LX/0Ot;

    iput-object v8, v0, LX/JrX;->h:LX/0Ot;

    iput-object v9, v0, LX/JrX;->i:LX/0Ot;

    iput-object v10, v0, LX/JrX;->j:LX/0Ot;

    iput-object v11, v0, LX/JrX;->k:LX/0Ot;

    iput-object v12, v0, LX/JrX;->l:LX/0Ot;

    .line 2743707
    return-object v0
.end method


# virtual methods
.method public final a(I)LX/Jqi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/facebook/messaging/sync/delta/handlerbase/MessagesDeltaHandler",
            "<",
            "LX/6kU;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2743708
    packed-switch p1, :pswitch_data_0

    .line 2743709
    :pswitch_0
    iget-object v0, p0, LX/JrX;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqh;

    .line 2743710
    iget-object v2, v0, LX/Jqh;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne p1, v2, :cond_0

    .line 2743711
    iget-object v0, v0, LX/Jqh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    .line 2743712
    :goto_0
    return-object v0

    .line 2743713
    :pswitch_1
    iget-object v0, p0, LX/JrX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743714
    :pswitch_2
    iget-object v0, p0, LX/JrX;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743715
    :pswitch_3
    iget-object v0, p0, LX/JrX;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743716
    :pswitch_4
    iget-object v0, p0, LX/JrX;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743717
    :pswitch_5
    iget-object v0, p0, LX/JrX;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743718
    :pswitch_6
    iget-object v0, p0, LX/JrX;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743719
    :pswitch_7
    iget-object v0, p0, LX/JrX;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743720
    :pswitch_8
    iget-object v0, p0, LX/JrX;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqi;

    goto :goto_0

    .line 2743721
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method
