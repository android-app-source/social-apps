.class public final LX/JoO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;)V
    .locals 0

    .prologue
    .line 2735510
    iput-object p1, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    .line 2735511
    iget-object v0, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->m:LX/JdU;

    .line 2735512
    iget-object v1, v0, LX/JdU;->a:LX/0Zb;

    const-string v2, "message_block_select_leave_group_from_blocked_warning_alert"

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2735513
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2735514
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2735515
    :cond_0
    iget-object v0, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->o:LX/JkQ;

    iget-object v1, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->p:Lcom/facebook/messaging/model/threads/ThreadSummary;

    const-string v2, "thread_blocking_flow"

    .line 2735516
    if-eqz v1, :cond_1

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/JkQ;->b:LX/3QW;

    invoke-virtual {v3, v1}, LX/3QW;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2735517
    :cond_1
    :goto_0
    iget-object v0, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jkb;

    iget-object v1, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    .line 2735518
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v2

    .line 2735519
    iget-object v2, p0, LX/JoO;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->p:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2735520
    new-instance v3, LX/JkZ;

    invoke-direct {v3}, LX/JkZ;-><init>()V

    iget-object v4, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735521
    iput-object v4, v3, LX/JkZ;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735522
    move-object v4, v3

    .line 2735523
    iget-object v3, v0, LX/Jkb;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserKey;

    .line 2735524
    iput-object v3, v4, LX/JkZ;->b:Lcom/facebook/user/model/UserKey;

    .line 2735525
    move-object v3, v4

    .line 2735526
    const-string v4, "remove_member"

    .line 2735527
    iput-object v4, v3, LX/JkZ;->g:Ljava/lang/String;

    .line 2735528
    move-object v3, v3

    .line 2735529
    iget-object v4, v0, LX/Jkb;->b:Landroid/content/res/Resources;

    const v5, 0x7f0805c9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2735530
    iput-object v4, v3, LX/JkZ;->c:Ljava/lang/String;

    .line 2735531
    move-object v3, v3

    .line 2735532
    iget-object v4, v0, LX/Jkb;->b:Landroid/content/res/Resources;

    const v5, 0x7f0805cc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2735533
    iput-object v4, v3, LX/JkZ;->e:Ljava/lang/String;

    .line 2735534
    move-object v3, v3

    .line 2735535
    iget-object v4, v0, LX/Jkb;->b:Landroid/content/res/Resources;

    const v5, 0x7f0805cd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2735536
    iput-object v4, v3, LX/JkZ;->f:Ljava/lang/String;

    .line 2735537
    move-object v3, v3

    .line 2735538
    iget-object v4, v0, LX/Jkb;->a:LX/JkP;

    const/4 v6, 0x0

    .line 2735539
    if-nez v2, :cond_6

    move v5, v6

    .line 2735540
    :goto_1
    move v4, v5

    .line 2735541
    if-eqz v4, :cond_5

    .line 2735542
    iget-object v4, v0, LX/Jkb;->b:Landroid/content/res/Resources;

    const v5, 0x7f0805ca

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2735543
    iput-object v4, v3, LX/JkZ;->d:Ljava/lang/String;

    .line 2735544
    :goto_2
    new-instance v4, LX/Jka;

    invoke-direct {v4, v3}, LX/Jka;-><init>(LX/JkZ;)V

    move-object v3, v4

    .line 2735545
    new-instance v4, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;

    invoke-direct {v4}, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;-><init>()V

    .line 2735546
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2735547
    const-string v6, "thread_key"

    iget-object v0, v3, LX/Jka;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2735548
    const-string v6, "user_key"

    iget-object v0, v3, LX/Jka;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2735549
    const-string v6, "title_text"

    iget-object v0, v3, LX/Jka;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735550
    const-string v6, "body_text"

    iget-object v0, v3, LX/Jka;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735551
    const-string v6, "confirm_button_text"

    iget-object v0, v3, LX/Jka;->e:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735552
    const-string v6, "loading_text"

    iget-object v0, v3, LX/Jka;->f:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735553
    const-string v6, "operation_type"

    iget-object v0, v3, LX/Jka;->g:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735554
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2735555
    move-object v3, v4

    .line 2735556
    const-string v4, "leaveThreadDialog"

    invoke-virtual {v3, v1, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2735557
    return-void

    .line 2735558
    :cond_2
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    .line 2735559
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2735560
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v7, :cond_4

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2735561
    iget-boolean v8, v4, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    if-eqz v8, :cond_3

    .line 2735562
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2735563
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 2735564
    :cond_4
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2735565
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    .line 2735566
    if-eqz v3, :cond_1

    .line 2735567
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "group_admin_rollout_exposure"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tfbid"

    iget-object v6, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v6}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v7

    invoke-virtual {v4, v5, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "num_admin_in_thread"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "exposure_point"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2735568
    iget-object v4, v0, LX/JkQ;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2735569
    :cond_5
    iget-object v4, v0, LX/Jkb;->b:Landroid/content/res/Resources;

    const v5, 0x7f0805cb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2735570
    iput-object v4, v3, LX/JkZ;->d:Ljava/lang/String;

    .line 2735571
    goto/16 :goto_2

    .line 2735572
    :cond_6
    iget-object v5, v4, LX/JkP;->b:LX/3QW;

    invoke-virtual {v5, v2}, LX/3QW;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v5

    move v5, v5

    .line 2735573
    if-nez v5, :cond_7

    move v5, v6

    .line 2735574
    goto/16 :goto_1

    .line 2735575
    :cond_7
    iget-object v5, v4, LX/JkP;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2, v5}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v5

    .line 2735576
    if-nez v5, :cond_8

    move v5, v6

    .line 2735577
    goto/16 :goto_1

    .line 2735578
    :cond_8
    iget-boolean v5, v5, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    goto/16 :goto_1
.end method
