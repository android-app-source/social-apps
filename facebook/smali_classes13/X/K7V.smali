.class public LX/K7V;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/CsR;
.implements LX/Csi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2773162
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2773163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2773160
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2773161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2773158
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773159
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 0

    .prologue
    .line 2773157
    return-void
.end method

.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 2773156
    return-object p0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 2773155
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2773164
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2773154
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2773153
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2773148
    return-void
.end method

.method public getFragmentPager()LX/CqD;
    .locals 1

    .prologue
    .line 2773152
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x25341ecd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2773150
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2773151
    const/16 v1, 0x2d

    const v2, 0x27dfb5d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 2773149
    return-void
.end method
