.class public final LX/JgK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/CKT;

.field public final synthetic b:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

.field public final synthetic c:LX/Jg7;

.field public final synthetic d:I

.field public final synthetic e:LX/JgL;


# direct methods
.method public constructor <init>(LX/JgL;LX/CKT;Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;LX/Jg7;I)V
    .locals 0

    .prologue
    .line 2722607
    iput-object p1, p0, LX/JgK;->e:LX/JgL;

    iput-object p2, p0, LX/JgK;->a:LX/CKT;

    iput-object p3, p0, LX/JgK;->b:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    iput-object p4, p0, LX/JgK;->c:LX/Jg7;

    iput p5, p0, LX/JgK;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 2722608
    if-eqz p2, :cond_0

    .line 2722609
    iget-object v0, p0, LX/JgK;->a:LX/CKT;

    iget-object v1, p0, LX/JgK;->b:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/JgI;

    invoke-direct {v2, p0}, LX/JgI;-><init>(LX/JgK;)V

    invoke-virtual {v0, v1, v2}, LX/CKT;->a(Ljava/lang/String;LX/CKR;)V

    .line 2722610
    iget-object v0, p0, LX/JgK;->e:LX/JgL;

    iget-object v1, p0, LX/JgK;->c:LX/Jg7;

    const/4 v2, 0x1

    iget v3, p0, LX/JgK;->d:I

    invoke-static {v0, v1, v2, v3}, LX/JgL;->a$redex0(LX/JgL;LX/Jg7;ZI)V

    .line 2722611
    :goto_0
    return-void

    .line 2722612
    :cond_0
    iget-object v0, p0, LX/JgK;->a:LX/CKT;

    iget-object v1, p0, LX/JgK;->b:Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/JgJ;

    invoke-direct {v2, p0}, LX/JgJ;-><init>(LX/JgK;)V

    invoke-virtual {v0, v1, v2}, LX/CKT;->b(Ljava/lang/String;LX/CKS;)V

    .line 2722613
    iget-object v0, p0, LX/JgK;->e:LX/JgL;

    iget-object v1, p0, LX/JgK;->c:LX/Jg7;

    const/4 v2, 0x0

    iget v3, p0, LX/JgK;->d:I

    invoke-static {v0, v1, v2, v3}, LX/JgL;->a$redex0(LX/JgL;LX/Jg7;ZI)V

    goto :goto_0
.end method
