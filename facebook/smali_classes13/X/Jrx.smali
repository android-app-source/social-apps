.class public final enum LX/Jrx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jrx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jrx;

.field public static final enum DELIVERED:LX/Jrx;

.field public static final enum FAILED_TO_SEND:LX/Jrx;

.field public static final enum GROUP_READ:LX/Jrx;

.field public static final enum PENDING:LX/Jrx;

.field public static final enum READ:LX/Jrx;

.field public static final enum SENT_BY_ME_TO_SERVER:LX/Jrx;

.field public static final enum SENT_FROM_RECEIPT:LX/Jrx;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2744924
    new-instance v0, LX/Jrx;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->PENDING:LX/Jrx;

    .line 2744925
    new-instance v0, LX/Jrx;

    const-string v1, "SENT_BY_ME_TO_SERVER"

    invoke-direct {v0, v1, v4}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->SENT_BY_ME_TO_SERVER:LX/Jrx;

    .line 2744926
    new-instance v0, LX/Jrx;

    const-string v1, "FAILED_TO_SEND"

    invoke-direct {v0, v1, v5}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->FAILED_TO_SEND:LX/Jrx;

    .line 2744927
    new-instance v0, LX/Jrx;

    const-string v1, "SENT_FROM_RECEIPT"

    invoke-direct {v0, v1, v6}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->SENT_FROM_RECEIPT:LX/Jrx;

    .line 2744928
    new-instance v0, LX/Jrx;

    const-string v1, "READ"

    invoke-direct {v0, v1, v7}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->READ:LX/Jrx;

    .line 2744929
    new-instance v0, LX/Jrx;

    const-string v1, "DELIVERED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->DELIVERED:LX/Jrx;

    .line 2744930
    new-instance v0, LX/Jrx;

    const-string v1, "GROUP_READ"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Jrx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jrx;->GROUP_READ:LX/Jrx;

    .line 2744931
    const/4 v0, 0x7

    new-array v0, v0, [LX/Jrx;

    sget-object v1, LX/Jrx;->PENDING:LX/Jrx;

    aput-object v1, v0, v3

    sget-object v1, LX/Jrx;->SENT_BY_ME_TO_SERVER:LX/Jrx;

    aput-object v1, v0, v4

    sget-object v1, LX/Jrx;->FAILED_TO_SEND:LX/Jrx;

    aput-object v1, v0, v5

    sget-object v1, LX/Jrx;->SENT_FROM_RECEIPT:LX/Jrx;

    aput-object v1, v0, v6

    sget-object v1, LX/Jrx;->READ:LX/Jrx;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Jrx;->DELIVERED:LX/Jrx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Jrx;->GROUP_READ:LX/Jrx;

    aput-object v2, v0, v1

    sput-object v0, LX/Jrx;->$VALUES:[LX/Jrx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2744932
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jrx;
    .locals 1

    .prologue
    .line 2744933
    const-class v0, LX/Jrx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jrx;

    return-object v0
.end method

.method public static values()[LX/Jrx;
    .locals 1

    .prologue
    .line 2744934
    sget-object v0, LX/Jrx;->$VALUES:[LX/Jrx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jrx;

    return-object v0
.end method
