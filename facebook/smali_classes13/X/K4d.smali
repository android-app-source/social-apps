.class public final enum LX/K4d;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K4d;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K4d;

.field public static final enum CREATE_EGL_CONTEXT:LX/K4d;

.field public static final enum DESTROY_EGL_CONTEXT:LX/K4d;

.field public static final enum DRAW_FRAME:LX/K4d;

.field public static final enum EXPORT_VIDEO:LX/K4d;

.field public static final enum INIT_DATA:LX/K4d;

.field public static final enum QUIT_THREAD:LX/K4d;

.field public static final enum UPDATE_TEXTURES:LX/K4d;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2769065
    new-instance v0, LX/K4d;

    const-string v1, "CREATE_EGL_CONTEXT"

    invoke-direct {v0, v1, v3}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->CREATE_EGL_CONTEXT:LX/K4d;

    .line 2769066
    new-instance v0, LX/K4d;

    const-string v1, "UPDATE_TEXTURES"

    invoke-direct {v0, v1, v4}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->UPDATE_TEXTURES:LX/K4d;

    .line 2769067
    new-instance v0, LX/K4d;

    const-string v1, "INIT_DATA"

    invoke-direct {v0, v1, v5}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->INIT_DATA:LX/K4d;

    .line 2769068
    new-instance v0, LX/K4d;

    const-string v1, "DRAW_FRAME"

    invoke-direct {v0, v1, v6}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->DRAW_FRAME:LX/K4d;

    .line 2769069
    new-instance v0, LX/K4d;

    const-string v1, "DESTROY_EGL_CONTEXT"

    invoke-direct {v0, v1, v7}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->DESTROY_EGL_CONTEXT:LX/K4d;

    .line 2769070
    new-instance v0, LX/K4d;

    const-string v1, "EXPORT_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->EXPORT_VIDEO:LX/K4d;

    .line 2769071
    new-instance v0, LX/K4d;

    const-string v1, "QUIT_THREAD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/K4d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K4d;->QUIT_THREAD:LX/K4d;

    .line 2769072
    const/4 v0, 0x7

    new-array v0, v0, [LX/K4d;

    sget-object v1, LX/K4d;->CREATE_EGL_CONTEXT:LX/K4d;

    aput-object v1, v0, v3

    sget-object v1, LX/K4d;->UPDATE_TEXTURES:LX/K4d;

    aput-object v1, v0, v4

    sget-object v1, LX/K4d;->INIT_DATA:LX/K4d;

    aput-object v1, v0, v5

    sget-object v1, LX/K4d;->DRAW_FRAME:LX/K4d;

    aput-object v1, v0, v6

    sget-object v1, LX/K4d;->DESTROY_EGL_CONTEXT:LX/K4d;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/K4d;->EXPORT_VIDEO:LX/K4d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/K4d;->QUIT_THREAD:LX/K4d;

    aput-object v2, v0, v1

    sput-object v0, LX/K4d;->$VALUES:[LX/K4d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2769073
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K4d;
    .locals 1

    .prologue
    .line 2769074
    const-class v0, LX/K4d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K4d;

    return-object v0
.end method

.method public static values()[LX/K4d;
    .locals 1

    .prologue
    .line 2769075
    sget-object v0, LX/K4d;->$VALUES:[LX/K4d;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K4d;

    return-object v0
.end method
