.class public final LX/K03;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/Callback;


# instance fields
.field public final synthetic a:LX/5pW;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/K05;


# direct methods
.method public constructor <init>(LX/K05;LX/5pW;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757211
    iput-object p1, p0, LX/K03;->c:LX/K05;

    iput-object p2, p0, LX/K03;->a:LX/5pW;

    iput-object p3, p0, LX/K03;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2757212
    aget-object v0, p1, v1

    check-cast v0, [I

    check-cast v0, [I

    .line 2757213
    aget v0, v0, v1

    if-nez v0, :cond_0

    .line 2757214
    iget-object v0, p0, LX/K03;->a:LX/5pW;

    const-string v1, "granted"

    invoke-interface {v0, v1}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2757215
    :goto_0
    return-void

    .line 2757216
    :cond_0
    const/4 v0, 0x1

    aget-object v0, p1, v0

    check-cast v0, LX/2lD;

    .line 2757217
    iget-object v1, p0, LX/K03;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/2lD;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2757218
    iget-object v0, p0, LX/K03;->a:LX/5pW;

    const-string v1, "denied"

    invoke-interface {v0, v1}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 2757219
    :cond_1
    iget-object v0, p0, LX/K03;->a:LX/5pW;

    const-string v1, "never_ask_again"

    invoke-interface {v0, v1}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
