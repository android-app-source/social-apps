.class public LX/JrT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/7GW;

.field private final d:LX/7GV;

.field private final e:LX/0aG;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:LX/0Uh;

.field private final h:LX/7G2;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/2Sx;

.field private final k:LX/7G3;

.field private final l:LX/Jqd;

.field private final m:LX/7GL;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jqc;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/2Ow;

.field private final p:Ljava/util/concurrent/Executor;

.field private final q:LX/1sj;

.field private final r:LX/2N6;

.field private final s:LX/JqM;

.field private final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FO4;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743443
    const-class v0, LX/JrT;

    sput-object v0, LX/JrT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/7GW;LX/7GV;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/7G2;LX/0Or;LX/2Sx;LX/7G3;LX/Jqd;LX/7GL;LX/0Or;LX/2Ow;Ljava/util/concurrent/Executor;LX/1sj;LX/2N6;LX/JqM;LX/0Or;)V
    .locals 2
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p15    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;",
            "LX/7GW;",
            "LX/7GV;",
            "LX/0aG;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/7G2;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Sx;",
            "LX/7G3;",
            "LX/Jqd;",
            "LX/7GL;",
            "LX/0Or",
            "<",
            "LX/Jqc;",
            ">;",
            "LX/2Ow;",
            "Ljava/util/concurrent/Executor;",
            "LX/1sj;",
            "LX/2N6;",
            "Lcom/facebook/messaging/sync/push/SyncOperationContextSupplier;",
            "LX/0Or",
            "<",
            "LX/FO4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2743444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2743445
    const/4 v1, 0x0

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/JrT;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2743446
    iput-object p1, p0, LX/JrT;->b:LX/0Or;

    .line 2743447
    iput-object p2, p0, LX/JrT;->c:LX/7GW;

    .line 2743448
    iput-object p3, p0, LX/JrT;->d:LX/7GV;

    .line 2743449
    iput-object p4, p0, LX/JrT;->e:LX/0aG;

    .line 2743450
    iput-object p5, p0, LX/JrT;->f:Ljava/util/concurrent/ExecutorService;

    .line 2743451
    iput-object p6, p0, LX/JrT;->g:LX/0Uh;

    .line 2743452
    iput-object p7, p0, LX/JrT;->h:LX/7G2;

    .line 2743453
    iput-object p8, p0, LX/JrT;->i:LX/0Or;

    .line 2743454
    iput-object p9, p0, LX/JrT;->j:LX/2Sx;

    .line 2743455
    iput-object p10, p0, LX/JrT;->k:LX/7G3;

    .line 2743456
    iput-object p11, p0, LX/JrT;->l:LX/Jqd;

    .line 2743457
    iput-object p12, p0, LX/JrT;->m:LX/7GL;

    .line 2743458
    iput-object p13, p0, LX/JrT;->n:LX/0Or;

    .line 2743459
    move-object/from16 v0, p14

    iput-object v0, p0, LX/JrT;->o:LX/2Ow;

    .line 2743460
    move-object/from16 v0, p15

    iput-object v0, p0, LX/JrT;->p:Ljava/util/concurrent/Executor;

    .line 2743461
    move-object/from16 v0, p16

    iput-object v0, p0, LX/JrT;->q:LX/1sj;

    .line 2743462
    move-object/from16 v0, p17

    iput-object v0, p0, LX/JrT;->r:LX/2N6;

    .line 2743463
    move-object/from16 v0, p18

    iput-object v0, p0, LX/JrT;->s:LX/JqM;

    .line 2743464
    move-object/from16 v0, p19

    iput-object v0, p0, LX/JrT;->t:LX/0Or;

    .line 2743465
    return-void
.end method
