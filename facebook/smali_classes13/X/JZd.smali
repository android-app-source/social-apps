.class public final LX/JZd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JZe;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;LX/JZe;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2708839
    iput-object p1, p0, LX/JZd;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    iput-object p2, p0, LX/JZd;->a:LX/JZe;

    iput-object p3, p0, LX/JZd;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x786684f0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2708840
    iget-object v0, p0, LX/JZd;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    iget-object v1, v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->c:LX/3UJ;

    iget-object v0, p0, LX/JZd;->a:LX/JZe;

    iget-object v0, v0, LX/JZe;->c:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    iget-object v0, p0, LX/JZd;->a:LX/JZe;

    iget-object v5, v0, LX/JZe;->d:LX/2na;

    new-instance v6, LX/JZc;

    invoke-direct {v6, p0}, LX/JZc;-><init>(LX/JZd;)V

    invoke-virtual/range {v1 .. v6}, LX/3UJ;->a(JLX/2hA;LX/2na;LX/84H;)V

    .line 2708841
    iget-object v0, p0, LX/JZd;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v1, p0, LX/JZd;->a:LX/JZe;

    iget-object v2, v1, LX/JZe;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v1, p0, LX/JZd;->a:LX/JZe;

    iget-object v1, v1, LX/JZe;->d:LX/2na;

    sget-object v3, LX/2na;->CONFIRM:LX/2na;

    if-ne v1, v3, :cond_0

    sget-object v1, LX/Cfc;->CONFIRM_FRIEND_REQUEST_TAP:LX/Cfc;

    invoke-virtual {v1}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v2, v1}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2708842
    iget-object v1, p0, LX/JZd;->b:LX/1Pq;

    iget-object v2, p0, LX/JZd;->a:LX/JZe;

    iget-object v0, p0, LX/JZd;->a:LX/JZe;

    iget-object v0, v0, LX/JZe;->d:LX/2na;

    sget-object v3, LX/2na;->CONFIRM:LX/2na;

    if-ne v0, v3, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    :goto_1
    invoke-static {v1, v2, v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->b(LX/1Pq;LX/JZe;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V

    .line 2708843
    const v0, -0x4941b04b

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 2708844
    :cond_0
    sget-object v1, LX/Cfc;->DELETE_FRIEND_REQUEST_TAP:LX/Cfc;

    invoke-virtual {v1}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2708845
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    goto :goto_1
.end method
