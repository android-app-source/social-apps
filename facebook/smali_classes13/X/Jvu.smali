.class public final LX/Jvu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;",
        ">;",
        "Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryInterfaces$GravitySuggestifierQuery$Suggestions$Edges;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Jvw;


# direct methods
.method public constructor <init>(LX/Jvw;I)V
    .locals 0

    .prologue
    .line 2751227
    iput-object p1, p0, LX/Jvu;->b:LX/Jvw;

    iput p2, p0, LX/Jvu;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 2751228
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2751229
    if-nez p1, :cond_0

    .line 2751230
    new-instance v0, LX/CeF;

    const-string v1, "result is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751231
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2751232
    check-cast v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;

    .line 2751233
    if-nez v0, :cond_1

    .line 2751234
    new-instance v0, LX/CeF;

    const-string v1, "model is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751235
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->a()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    move-result-object v0

    .line 2751236
    if-nez v0, :cond_2

    .line 2751237
    new-instance v0, LX/CeF;

    const-string v1, "suggestions is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751238
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;->a()I

    move-result v1

    if-gtz v1, :cond_3

    .line 2751239
    new-instance v0, LX/CeF;

    const-string v1, "suggestions count is < 1"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751240
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;->b()LX/0Px;

    move-result-object v0

    .line 2751241
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2751242
    new-instance v0, LX/CeF;

    const-string v1, "edges is empty"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751243
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;

    .line 2751244
    if-nez v0, :cond_5

    .line 2751245
    new-instance v0, LX/CeF;

    const-string v1, "first edge is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751246
    :cond_5
    iget-object v1, p0, LX/Jvu;->b:LX/Jvw;

    iget-object v1, v1, LX/Jvw;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x1e0005

    iget v3, p0, LX/Jvu;->a:I

    const-string v4, "logging_id"

    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 2751247
    invoke-virtual {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel;->b()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 2751248
    if-nez v1, :cond_6

    .line 2751249
    new-instance v0, LX/CeF;

    const-string v1, "first edge.getNode() is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751250
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v1

    .line 2751251
    if-nez v1, :cond_7

    .line 2751252
    new-instance v0, LX/CeF;

    const-string v1, "reactionStories\' feedUnitFragment is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751253
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;->c()LX/175;

    move-result-object v1

    if-nez v1, :cond_8

    .line 2751254
    new-instance v0, LX/CeF;

    const-string v1, "reactionStories\' feedUnitFragment\'s title is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2751255
    :cond_8
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
