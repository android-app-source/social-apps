.class public LX/JxM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JxJ;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1SQ;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/os/Handler;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JxI;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0bT;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/0Or;LX/0bT;)V
    .locals 0
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1SQ;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0Or",
            "<",
            "LX/JxI;",
            ">;",
            "LX/0bT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2753004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753005
    iput-object p1, p0, LX/JxM;->a:Landroid/content/Context;

    .line 2753006
    iput-object p2, p0, LX/JxM;->b:LX/0Ot;

    .line 2753007
    iput-object p3, p0, LX/JxM;->c:LX/0Ot;

    .line 2753008
    iput-object p4, p0, LX/JxM;->d:Landroid/os/Handler;

    .line 2753009
    iput-object p5, p0, LX/JxM;->e:LX/0Or;

    .line 2753010
    iput-object p6, p0, LX/JxM;->f:LX/0bT;

    .line 2753011
    return-void
.end method

.method private a()LX/JxL;
    .locals 2

    .prologue
    .line 2752995
    iget-object v0, p0, LX/JxM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1SQ;

    invoke-virtual {v0}, LX/1SQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JxM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2752996
    :cond_0
    new-instance v0, LX/JxE;

    invoke-direct {v0}, LX/JxE;-><init>()V

    throw v0

    .line 2752997
    :cond_1
    iget-object v0, p0, LX/JxM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1SQ;

    invoke-virtual {v0}, LX/1SQ;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2752998
    new-instance v0, LX/JxD;

    invoke-direct {v0}, LX/JxD;-><init>()V

    throw v0

    .line 2752999
    :cond_2
    iget-object v0, p0, LX/JxM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2753000
    new-instance v0, LX/JxG;

    invoke-direct {v0}, LX/JxG;-><init>()V

    throw v0

    .line 2753001
    :cond_3
    iget-object v0, p0, LX/JxM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2753002
    new-instance v0, LX/JxF;

    invoke-direct {v0}, LX/JxF;-><init>()V

    throw v0

    .line 2753003
    :cond_4
    new-instance v1, LX/JxL;

    iget-object v0, p0, LX/JxM;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JxI;

    invoke-direct {v1, p0, v0}, LX/JxL;-><init>(LX/JxM;LX/JxI;)V

    return-object v1
.end method


# virtual methods
.method public final a(J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cdn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2752978
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "Tried to perform a BLE scan for <0 millis"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2752979
    invoke-direct {p0}, LX/JxM;->a()LX/JxL;

    move-result-object v0

    .line 2752980
    iget-object v3, v0, LX/JxL;->a:LX/JxM;

    iget-object v3, v3, LX/JxM;->f:LX/0bT;

    new-instance v4, LX/Cdq;

    invoke-direct {v4}, LX/Cdq;-><init>()V

    invoke-virtual {v3, v4}, LX/0bT;->a(LX/0bY;)V

    .line 2752981
    iget-object v3, v0, LX/JxL;->a:LX/JxM;

    iget-object v3, v3, LX/JxM;->a:Landroid/content/Context;

    iget-object v4, v0, LX/JxL;->d:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2752982
    iget-object v3, v0, LX/JxL;->c:LX/JxI;

    .line 2752983
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v8

    iput-object v8, v3, LX/JxI;->c:Ljava/util/Map;

    .line 2752984
    const/4 v8, 0x0

    iput v8, v3, LX/JxI;->d:I

    .line 2752985
    iget-object v8, v3, LX/JxI;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    iput-wide v8, v3, LX/JxI;->e:J

    .line 2752986
    iget-object v8, v3, LX/JxI;->b:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iput-wide v8, v3, LX/JxI;->f:J

    .line 2752987
    iget-object v3, v0, LX/JxL;->a:LX/JxM;

    iget-object v3, v3, LX/JxM;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2752988
    iget-object v3, v0, LX/JxL;->a:LX/JxM;

    iget-object v3, v3, LX/JxM;->d:Landroid/os/Handler;

    iget-object v4, v0, LX/JxL;->e:Ljava/lang/Runnable;

    const v5, 0x74cdb1f6

    invoke-static {v3, v4, p1, p2, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2752989
    :goto_1
    iget-object v3, v0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2752990
    :goto_2
    return-object v0

    .line 2752991
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2752992
    :catch_0
    move-exception v0

    .line 2752993
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 2752994
    :cond_1
    new-instance v3, LX/JxC;

    const-string v4, "Unknown error occurred"

    invoke-direct {v3, v4}, LX/JxC;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/JxL;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
