.class public LX/Jqz;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kU;",
        ">;"
    }
.end annotation


# static fields
.field private static final o:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jr3;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqf;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/user/model/UserKey;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/2N4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/Jqb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/Ifw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/Ig9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/2Ow;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/Jrc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/2Ns;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741287
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqz;->o:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741279
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2741280
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741281
    iput-object v0, p0, LX/Jqz;->a:LX/0Ot;

    .line 2741282
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741283
    iput-object v0, p0, LX/Jqz;->c:LX/0Ot;

    .line 2741284
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741285
    iput-object v0, p0, LX/Jqz;->d:LX/0Ot;

    .line 2741286
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqz;
    .locals 7

    .prologue
    .line 2741252
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741253
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741254
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741255
    if-nez v1, :cond_0

    .line 2741256
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741257
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741258
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741259
    sget-object v1, LX/Jqz;->o:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741260
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741261
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741262
    :cond_1
    if-nez v1, :cond_4

    .line 2741263
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741264
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741265
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqz;->b(LX/0QB;)LX/Jqz;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2741266
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741267
    if-nez v1, :cond_2

    .line 2741268
    sget-object v0, LX/Jqz;->o:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqz;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741269
    :goto_1
    if-eqz v0, :cond_3

    .line 2741270
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741271
    :goto_3
    check-cast v0, LX/Jqz;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741272
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741273
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741274
    :catchall_1
    move-exception v0

    .line 2741275
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741276
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741277
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741278
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqz;->o:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqz;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Jqz;
    .locals 15

    .prologue
    .line 2741248
    new-instance v0, LX/Jqz;

    const/16 v1, 0x35bd

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Jqz;-><init>(LX/0Ot;)V

    .line 2741249
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    const/16 v3, 0x29da

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x29c9

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/2Ot;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/UserKey;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v7

    check-cast v7, LX/2N4;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v8

    check-cast v8, LX/Jqb;

    invoke-static {p0}, LX/Ifw;->a(LX/0QB;)LX/Ifw;

    move-result-object v9

    check-cast v9, LX/Ifw;

    invoke-static {p0}, LX/Ig9;->a(LX/0QB;)LX/Ig9;

    move-result-object v10

    check-cast v10, LX/Ig9;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v11

    check-cast v11, LX/2Ow;

    const/16 v12, 0x274b

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v13

    check-cast v13, LX/Jrc;

    invoke-static {p0}, LX/2Ns;->b(LX/0QB;)LX/2Ns;

    move-result-object v14

    check-cast v14, LX/2Ns;

    .line 2741250
    iput-object v1, v0, LX/Jqz;->a:LX/0Ot;

    iput-object v2, v0, LX/Jqz;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v3, v0, LX/Jqz;->c:LX/0Ot;

    iput-object v4, v0, LX/Jqz;->d:LX/0Ot;

    iput-object v5, v0, LX/Jqz;->e:Lcom/facebook/user/model/UserKey;

    iput-object v6, v0, LX/Jqz;->f:LX/0SG;

    iput-object v7, v0, LX/Jqz;->g:LX/2N4;

    iput-object v8, v0, LX/Jqz;->h:LX/Jqb;

    iput-object v9, v0, LX/Jqz;->i:LX/Ifw;

    iput-object v10, v0, LX/Jqz;->j:LX/Ig9;

    iput-object v11, v0, LX/Jqz;->k:LX/2Ow;

    iput-object v12, v0, LX/Jqz;->l:LX/0Or;

    iput-object v13, v0, LX/Jqz;->m:LX/Jrc;

    iput-object v14, v0, LX/Jqz;->n:LX/2Ns;

    .line 2741251
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741200
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741201
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2741217
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kU;

    invoke-virtual {v0}, LX/6kU;->d()LX/6jt;

    move-result-object v0

    iget-object v3, v0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    .line 2741218
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2741219
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kl;

    .line 2741220
    iget-object v1, p0, LX/Jqz;->g:LX/2N4;

    iget-object v2, v0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2741221
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2741222
    if-nez v1, :cond_1

    move-object v0, v8

    .line 2741223
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 2741224
    goto :goto_0

    .line 2741225
    :cond_1
    new-instance v2, LX/FFr;

    invoke-direct {v2}, LX/FFr;-><init>()V

    .line 2741226
    iput-object v1, v2, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2741227
    move-object v1, v2

    .line 2741228
    iget-object v2, v0, LX/6kl;->expirationTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2741229
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v1, LX/FFr;->a:Ljava/lang/Long;

    .line 2741230
    move-object v1, v1

    .line 2741231
    iget-object v2, v0, LX/6kl;->coordinate:LX/6km;

    if-eqz v2, :cond_2

    .line 2741232
    iget-object v2, v0, LX/6kl;->coordinate:LX/6km;

    .line 2741233
    iput-object v2, v1, LX/FFr;->c:LX/6km;

    .line 2741234
    move-object v2, v1

    .line 2741235
    iget-object v0, v0, LX/6kl;->locationTitle:Ljava/lang/String;

    .line 2741236
    iput-object v0, v2, LX/FFr;->d:Ljava/lang/String;

    .line 2741237
    :cond_2
    invoke-virtual {v1}, LX/FFr;->b()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2741238
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2741239
    const-string v0, "xma"

    iget-object v5, p0, LX/Jqz;->n:LX/2Ns;

    iget-object v6, v3, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v5, v6}, LX/2Ns;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741240
    iget-object v0, p0, LX/Jqz;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2741241
    const-string v5, "msg_id"

    .line 2741242
    iget-object v6, v1, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    move-object v1, v6

    .line 2741243
    invoke-static {v5, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 2741244
    const-string v5, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v2, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2741245
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/Jqz;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741246
    const-string v0, "dbResult"

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v0, v8

    .line 2741247
    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741205
    const-string v0, "dbResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2741206
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2741207
    if-eqz v0, :cond_0

    .line 2741208
    iget-object v1, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v1

    .line 2741209
    iget-object v3, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741210
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2741211
    iget-object v1, p0, LX/Jqz;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v0, v4, v5}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2741212
    iget-object v0, p0, LX/Jqz;->h:LX/Jqb;

    .line 2741213
    invoke-static {v0, v3}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741214
    iget-object v0, p0, LX/Jqz;->k:LX/2Ow;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Ow;->a(LX/0Px;)V

    .line 2741215
    :cond_0
    iget-object v0, p0, LX/Jqz;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;

    invoke-direct {v1, p0, p2}, Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;-><init>(LX/Jqz;LX/7GJ;)V

    const v2, -0x2d82b0f7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2741216
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2741202
    check-cast p1, LX/6kU;

    .line 2741203
    invoke-virtual {p1}, LX/6kU;->d()LX/6jt;

    move-result-object v0

    .line 2741204
    iget-object v1, p0, LX/Jqz;->m:LX/Jrc;

    iget-object v0, v0, LX/6jt;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
