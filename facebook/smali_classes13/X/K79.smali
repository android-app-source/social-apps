.class public LX/K79;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:LX/0lF;

.field public final g:LX/K73;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/util/List;LX/0Px;Ljava/lang/String;LX/0lF;LX/K73;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;",
            ">;",
            "Ljava/lang/String;",
            "LX/0lF;",
            "LX/K73;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2772733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772734
    iput-object p1, p0, LX/K79;->a:Ljava/lang/String;

    .line 2772735
    iput-wide p2, p0, LX/K79;->b:J

    .line 2772736
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/K79;->c:Ljava/util/List;

    .line 2772737
    iput-object p5, p0, LX/K79;->d:LX/0Px;

    .line 2772738
    iput-object p6, p0, LX/K79;->e:Ljava/lang/String;

    .line 2772739
    iput-object p7, p0, LX/K79;->f:LX/0lF;

    .line 2772740
    iput-object p8, p0, LX/K79;->g:LX/K73;

    .line 2772741
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2772742
    const-string v0, "tarot_session_id"

    iget-object v1, p0, LX/K79;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772743
    const-string v0, "tarot_session_length"

    iget-wide v2, p0, LX/K79;->b:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2772744
    const-string v1, "chained_digest_ids"

    iget-object v0, p0, LX/K79;->c:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2772745
    const-string v0, "click_source"

    iget-object v1, p0, LX/K79;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772746
    iget-object v0, p0, LX/K79;->f:LX/0lF;

    if-eqz v0, :cond_0

    .line 2772747
    const-string v0, "tracking_codes"

    iget-object v1, p0, LX/K79;->f:LX/0lF;

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772748
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2772749
    const-string v0, "tarot_session_id"

    iget-object v1, p0, LX/K79;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772750
    const-string v0, "tarot_session_length"

    iget-wide v2, p0, LX/K79;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772751
    const-string v0, "chained_digest_ids"

    iget-object v1, p0, LX/K79;->c:Ljava/util/List;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772752
    const-string v0, "click_source"

    iget-object v1, p0, LX/K79;->e:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772753
    const-string v0, "tracking_codes"

    iget-object v1, p0, LX/K79;->f:LX/0lF;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772754
    return-void
.end method
