.class public final LX/K6e;
.super LX/K6d;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V
    .locals 0

    .prologue
    .line 2772028
    iput-object p1, p0, LX/K6e;->a:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    invoke-direct {p0}, LX/K6d;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2772029
    check-cast p1, LX/K7w;

    .line 2772030
    iget-object v0, p0, LX/K6e;->a:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    iget-object v1, p1, LX/K7w;->a:Ljava/lang/String;

    .line 2772031
    iget-object p0, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object p1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {p1}, LX/CsS;->getActiveFragmentIndex()I

    move-result p1

    invoke-virtual {p0, p1}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object p0

    check-cast p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2772032
    if-eqz p0, :cond_1

    .line 2772033
    if-nez v1, :cond_2

    .line 2772034
    const/4 p1, 0x0

    .line 2772035
    :goto_0
    move p1, p1

    .line 2772036
    if-eqz p1, :cond_1

    .line 2772037
    iget-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    if-eqz p1, :cond_0

    .line 2772038
    iget-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    .line 2772039
    iget-object v0, p1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    if-eqz v0, :cond_0

    .line 2772040
    iget-object v0, p1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v0}, LX/K5b;->c()V

    .line 2772041
    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->v:Z

    .line 2772042
    :cond_1
    return-void

    .line 2772043
    :cond_2
    iget-object p1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, p1

    .line 2772044
    invoke-static {p1}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0
.end method
