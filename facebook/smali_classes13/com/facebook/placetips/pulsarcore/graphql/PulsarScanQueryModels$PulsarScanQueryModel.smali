.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x65de98cc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752181
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752180
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2752178
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2752179
    return-void
.end method

.method private a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752176
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    .line 2752177
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752174
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    .line 2752175
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    return-object v0
.end method

.method private k()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752172
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->g:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->g:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 2752173
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->g:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752170
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 2752171
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2752140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752141
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2752142
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2752143
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->k()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2752144
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2752145
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2752146
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2752147
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2752148
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2752149
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2752150
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752151
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2752157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752158
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2752159
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    .line 2752160
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2752161
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    .line 2752162
    iput-object v0, v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    .line 2752163
    :cond_0
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->k()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2752164
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->k()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 2752165
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->k()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2752166
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    .line 2752167
    iput-object v0, v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;->g:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 2752168
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752169
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2752154
    new-instance v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    invoke-direct {v0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;-><init>()V

    .line 2752155
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2752156
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2752153
    const v0, -0x6d0230d7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2752152
    const v0, -0x7e371071

    return v0
.end method
