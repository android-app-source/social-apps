.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x211b0fc5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752327
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752328
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2752336
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2752337
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2752329
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752330
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2752331
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2752332
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2752333
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2752334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752335
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2752317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752318
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2752319
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752320
    invoke-virtual {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2752321
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;

    .line 2752322
    iput-object v0, v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752323
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752324
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752325
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752326
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel$PulsarModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2752314
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2752315
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->f:I

    .line 2752316
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2752311
    new-instance v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;

    invoke-direct {v0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;-><init>()V

    .line 2752312
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2752313
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2752310
    const v0, -0x5bba6221

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2752309
    const v0, 0x5c5af1dd

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 2752307
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2752308
    iget v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$BestGuessModel;->f:I

    return v0
.end method
