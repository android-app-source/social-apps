.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2752080
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    new-instance v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2752081
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2752082
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2752083
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2752084
    const/4 v2, 0x0

    .line 2752085
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 2752086
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2752087
    :goto_0
    move v1, v2

    .line 2752088
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2752089
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2752090
    new-instance v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    invoke-direct {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;-><init>()V

    .line 2752091
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2752092
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2752093
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2752094
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2752095
    :cond_0
    return-object v1

    .line 2752096
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2752097
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_6

    .line 2752098
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2752099
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2752100
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v6, :cond_2

    .line 2752101
    const-string p0, "best_guess"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2752102
    invoke-static {p1, v0}, LX/Jwk;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2752103
    :cond_3
    const-string p0, "confidence_level"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2752104
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2752105
    :cond_4
    const-string p0, "gravity_settings"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2752106
    invoke-static {p1, v0}, LX/2dS;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2752107
    :cond_5
    const-string p0, "result_code"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2752108
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_1

    .line 2752109
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2752110
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2752111
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2752112
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2752113
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2752114
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
