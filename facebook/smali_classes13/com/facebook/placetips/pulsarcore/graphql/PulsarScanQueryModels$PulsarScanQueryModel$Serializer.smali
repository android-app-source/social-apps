.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2752137
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    new-instance v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2752138
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2752139
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2752115
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2752116
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v4, 0x1

    .line 2752117
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2752118
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2752119
    if-eqz v2, :cond_0

    .line 2752120
    const-string v3, "best_guess"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752121
    invoke-static {v1, v2, p1, p2}, LX/Jwk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2752122
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2752123
    if-eqz v2, :cond_1

    .line 2752124
    const-string v2, "confidence_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752125
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2752126
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2752127
    if-eqz v2, :cond_2

    .line 2752128
    const-string v3, "gravity_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752129
    invoke-static {v1, v2, p1}, LX/2b4;->a(LX/15i;ILX/0nX;)V

    .line 2752130
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2752131
    if-eqz v2, :cond_3

    .line 2752132
    const-string v2, "result_code"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752133
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2752134
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2752135
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2752136
    check-cast p1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$Serializer;->a(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
