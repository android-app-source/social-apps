.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7732dd4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752051
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752079
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2752077
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2752078
    return-void
.end method

.method private a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752075
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752076
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2752068
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752069
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2752070
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2752071
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2752072
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2752073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752074
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2752060
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752061
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2752062
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752063
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->a()Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2752064
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    .line 2752065
    iput-object v0, v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->e:Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752066
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752067
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2752057
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2752058
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;->f:I

    .line 2752059
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2752054
    new-instance v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;

    invoke-direct {v0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel;-><init>()V

    .line 2752055
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2752056
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2752053
    const v0, -0x673f536d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2752052
    const v0, 0x5c5af1dd

    return v0
.end method
