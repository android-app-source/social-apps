.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2752373
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;

    new-instance v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2752374
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2752375
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2752376
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2752377
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v4, 0x1

    .line 2752378
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2752379
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2752380
    if-eqz v2, :cond_0

    .line 2752381
    const-string v3, "best_guess"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752382
    invoke-static {v1, v2, p1, p2}, LX/Jwn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2752383
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2752384
    if-eqz v2, :cond_1

    .line 2752385
    const-string v2, "confidence_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752386
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2752387
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2752388
    if-eqz v2, :cond_2

    .line 2752389
    const-string v3, "gravity_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752390
    invoke-static {v1, v2, p1}, LX/2b4;->a(LX/15i;ILX/0nX;)V

    .line 2752391
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2752392
    if-eqz v2, :cond_3

    .line 2752393
    const-string v2, "result_code"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752394
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2752395
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2752396
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2752397
    check-cast p1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel$Serializer;->a(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
