.class public final Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6e26b4ca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752043
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2752019
    const-class v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2752020
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2752021
    return-void
.end method

.method private a()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752022
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->e:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->e:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    .line 2752023
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->e:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2752024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752025
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->a()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2752026
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2752027
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2752028
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752029
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2752030
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2752031
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->a()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2752032
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->a()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    .line 2752033
    invoke-direct {p0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->a()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2752034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    .line 2752035
    iput-object v0, v1, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;->e:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    .line 2752036
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2752037
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2752038
    new-instance v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;

    invoke-direct {v0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$PulsarScanQueryModel$BestGuessModel$PulsarModel;-><init>()V

    .line 2752039
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2752040
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2752041
    const v0, -0x32c630ef

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2752042
    const v0, -0x70d8bc63

    return v0
.end method
