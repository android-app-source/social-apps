.class public final Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JxM;

.field public final synthetic b:LX/JxL;


# direct methods
.method public constructor <init>(LX/JxL;LX/JxM;)V
    .locals 0

    .prologue
    .line 2752923
    iput-object p1, p0, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$1;->b:LX/JxL;

    iput-object p2, p0, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$1;->a:LX/JxM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 2752924
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/scan/BleScannerImpl$BleScanCallback$1;->b:LX/JxL;

    .line 2752925
    iget-object v1, v0, LX/JxL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1}, LX/0SQ;->isDone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2752926
    :goto_0
    return-void

    .line 2752927
    :cond_0
    iget-object v1, v0, LX/JxL;->a:LX/JxM;

    iget-object v1, v1, LX/JxM;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 2752928
    iget-object v1, v0, LX/JxL;->c:LX/JxI;

    const/4 v6, 0x0

    .line 2752929
    iget-object v7, v1, LX/JxI;->b:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v8

    iget-wide v10, v1, LX/JxI;->f:J

    sub-long/2addr v8, v10

    .line 2752930
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gtz v7, :cond_3

    .line 2752931
    :cond_1
    :goto_1
    move v1, v6

    .line 2752932
    if-eqz v1, :cond_2

    .line 2752933
    invoke-virtual {v0}, LX/JxL;->b()V

    goto :goto_0

    .line 2752934
    :cond_2
    iget-object v1, v0, LX/JxL;->a:LX/JxM;

    iget-object v1, v1, LX/JxM;->d:Landroid/os/Handler;

    iget-object v2, v0, LX/JxL;->f:Ljava/lang/Runnable;

    const-wide/16 v3, 0xbb8

    const v5, 0x2d586f7c

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 2752935
    :cond_3
    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v7, v8

    .line 2752936
    iget v8, v1, LX/JxI;->d:I

    div-int v7, v8, v7

    const/4 v8, 0x4

    if-lt v7, v8, :cond_1

    const/4 v6, 0x1

    goto :goto_1
.end method
