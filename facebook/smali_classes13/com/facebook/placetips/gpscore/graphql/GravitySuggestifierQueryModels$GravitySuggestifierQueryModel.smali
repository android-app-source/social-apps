.class public final Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x22024200
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2751619
    const-class v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2751622
    const-class v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2751620
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2751621
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2751605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2751606
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->a()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2751607
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2751608
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2751609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2751610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2751611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2751612
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->a()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2751613
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->a()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    .line 2751614
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->a()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2751615
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;

    .line 2751616
    iput-object v0, v1, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->e:Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    .line 2751617
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2751618
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751603
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->e:Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    iput-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->e:Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    .line 2751604
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;->e:Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2751600
    new-instance v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;

    invoke-direct {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel;-><init>()V

    .line 2751601
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2751602
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2751599
    const v0, 0x7912f969

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2751598
    const v0, -0x3e224e4d

    return v0
.end method
