.class public final Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x650177af
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2751489
    const-class v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2751517
    const-class v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2751515
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2751516
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2751509
    iput-object p1, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2751510
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2751511
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2751512
    if-eqz v0, :cond_0

    .line 2751513
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2751514
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751506
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2751507
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2751508
    :cond_0
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751504
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 2751505
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2751490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2751491
    invoke-direct {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2751492
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 2751493
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2751494
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2751495
    invoke-direct {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2751496
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2751497
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2751498
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2751499
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2751500
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2751501
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2751502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2751503
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2751481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2751482
    invoke-direct {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2751483
    invoke-direct {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 2751484
    invoke-direct {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2751485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 2751486
    iput-object v0, v1, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 2751487
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2751488
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2751480
    new-instance v0, LX/Jw7;

    invoke-direct {v0, p1}, LX/Jw7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751518
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2751474
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2751475
    invoke-virtual {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2751476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2751477
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 2751478
    :goto_0
    return-void

    .line 2751479
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2751471
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2751472
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->a(Ljava/lang/String;)V

    .line 2751473
    :cond_0
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2751459
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    .line 2751460
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2751468
    new-instance v0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;-><init>()V

    .line 2751469
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2751470
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751466
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 2751467
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751464
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2751465
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2751463
    const v0, 0x235a0d36

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2751462
    invoke-direct {p0}, Lcom/facebook/placetips/gpscore/graphql/GravitySuggestifierQueryModels$GravitySuggestifierQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2751461
    const v0, 0x499e8e7

    return v0
.end method
