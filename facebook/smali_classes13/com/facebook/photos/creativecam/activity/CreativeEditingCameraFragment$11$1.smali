.class public final Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Jvd;


# direct methods
.method public constructor <init>(LX/Jvd;)V
    .locals 0

    .prologue
    .line 2750648
    iput-object p1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;->a:LX/Jvd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2750649
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 2750650
    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2750651
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;->a:LX/Jvd;

    iget-object v0, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->C:LX/BFq;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;->a:LX/Jvd;

    iget-object v1, v1, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 2750652
    iget-object v2, v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v2}, Lcom/facebook/optic/CameraPreviewView;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v2

    move-object v1, v2

    .line 2750653
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, v0, LX/BFq;->d:Z

    if-nez v2, :cond_2

    .line 2750654
    :cond_0
    iget-object v2, v0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setVisibility(I)V

    .line 2750655
    :goto_0
    return-void

    .line 2750656
    :cond_1
    sget-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->e:Ljava/lang/String;

    const-string v1, "Flash not ready"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2750657
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;->a:LX/Jvd;

    iget-object v0, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->C:LX/BFq;

    .line 2750658
    iget-object v1, v0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setVisibility(I)V

    .line 2750659
    goto :goto_0

    .line 2750660
    :cond_2
    iget-object v2, v0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setVisibility(I)V

    .line 2750661
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2750662
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2750663
    const-string v5, "auto"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2750664
    const v5, 0x7f020b3b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v5}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2750665
    :cond_4
    const-string v5, "on"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2750666
    const v5, 0x7f020b3d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v5}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2750667
    :cond_5
    const-string v5, "off"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2750668
    const v5, 0x7f020b3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v5}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2750669
    :cond_6
    const-string v5, "red-eye"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2750670
    const v5, 0x7f020b3e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v5}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2750671
    :cond_7
    iget-object v2, v0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setStates(Ljava/util/List;)V

    .line 2750672
    iget-object v2, v0, LX/BFq;->b:LX/Jvg;

    .line 2750673
    iget-object v3, v2, LX/Jvg;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v3, v3, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v3}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e()Z

    move-result v3

    move v2, v3

    .line 2750674
    if-eqz v2, :cond_8

    .line 2750675
    iget-object v2, v0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    const-string v3, "off"

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setCurrentState(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2750676
    :cond_8
    iget-object v2, v0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    iget-object v3, v0, LX/BFq;->b:LX/Jvg;

    .line 2750677
    iget-object v4, v3, LX/Jvg;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v4, v4, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 2750678
    sget-object v3, LX/5fQ;->b:LX/5fQ;

    move-object v3, v3

    .line 2750679
    invoke-virtual {v3}, LX/5fQ;->h()Z

    move-result v3

    if-nez v3, :cond_9

    .line 2750680
    const-string v3, "off"

    .line 2750681
    :goto_2
    move-object v4, v3

    .line 2750682
    move-object v3, v4

    .line 2750683
    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setCurrentState(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget-object v3, v4, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v3}, Lcom/facebook/optic/CameraPreviewView;->getFlashMode()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method
