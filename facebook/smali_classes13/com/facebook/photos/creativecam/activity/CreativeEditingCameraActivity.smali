.class public Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/Jvb;

.field private q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2750621
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;

    const-class v1, LX/Jvb;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Jvb;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->p:LX/Jvb;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2750622
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2750623
    invoke-static {p0, p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2750624
    const v0, 0x7f0303ad

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->setContentView(I)V

    .line 2750625
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 2750626
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 2750627
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_creativecam_launch_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750628
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_creativecam_composer_session_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2750629
    if-nez p1, :cond_0

    .line 2750630
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a(Landroid/content/Intent;)Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    .line 2750631
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f0d0ba2

    iget-object v4, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v2, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v2

    .line 2750632
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2750633
    :goto_0
    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v3, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->p:LX/Jvb;

    .line 2750634
    new-instance v5, LX/Jva;

    invoke-static {v3}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v6

    check-cast v6, LX/1Kf;

    invoke-static {v3}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v7

    check-cast v7, LX/74n;

    move-object v8, p0

    move-object v9, v0

    move-object v10, v1

    invoke-direct/range {v5 .. v10}, LX/Jva;-><init>(LX/1Kf;LX/74n;Landroid/app/Activity;Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;)V

    .line 2750635
    move-object v0, v5

    .line 2750636
    iput-object v0, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    .line 2750637
    return-void

    .line 2750638
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const v3, 0x7f0d0ba2

    invoke-virtual {v2, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iput-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2750639
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2750640
    if-nez p1, :cond_0

    .line 2750641
    invoke-virtual {p0, p2, p3}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->setResult(ILandroid/content/Intent;)V

    .line 2750642
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->finish()V

    .line 2750643
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2750644
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;->q:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2750645
    :goto_0
    return-void

    .line 2750646
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
