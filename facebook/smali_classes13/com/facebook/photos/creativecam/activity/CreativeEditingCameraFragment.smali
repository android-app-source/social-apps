.class public Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final e:Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field public static final g:[Ljava/lang/String;


# instance fields
.field public A:LX/89e;

.field public B:LX/BFr;

.field public C:LX/BFq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:LX/BGD;

.field public E:LX/BGC;

.field public F:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;",
            ">;"
        }
    .end annotation
.end field

.field public G:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:LX/23P;

.field public J:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public K:LX/8GN;

.field public L:Z

.field private M:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:LX/4gI;

.field public O:LX/89S;

.field public P:LX/89Z;

.field public Q:LX/89f;

.field public R:LX/BFW;

.field public S:LX/BFZ;

.field public T:LX/89c;

.field public U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private V:Ljava/lang/String;

.field public W:I

.field public X:Z

.field public final Y:LX/Jvj;

.field private final Z:LX/Jvk;

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BFZ;",
            ">;"
        }
    .end annotation
.end field

.field private final aa:LX/Jvl;

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BFW;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BFo;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

.field public i:Landroid/view/View;

.field private j:Landroid/view/View;

.field public k:Landroid/view/View;

.field public l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public n:Landroid/view/ViewGroup;

.field public o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/9c7;

.field private s:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/0i4;

.field public u:LX/0kL;

.field public v:LX/BG7;

.field public w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

.field public x:LX/BGA;

.field private y:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

.field public z:LX/BFk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2751023
    const-class v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->e:Ljava/lang/String;

    .line 2751024
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->f:[Ljava/lang/String;

    .line 2751025
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v3

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2751026
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2751027
    new-instance v0, LX/Jvj;

    invoke-direct {v0, p0}, LX/Jvj;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Y:LX/Jvj;

    .line 2751028
    new-instance v0, LX/Jvk;

    invoke-direct {v0, p0}, LX/Jvk;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Z:LX/Jvk;

    .line 2751029
    new-instance v0, LX/Jvl;

    invoke-direct {v0, p0}, LX/Jvl;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->aa:LX/Jvl;

    return-void
.end method

.method public static A(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2751030
    iput-boolean v4, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->L:Z

    .line 2751031
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    .line 2751032
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2751033
    iget-boolean v5, v3, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->c:Z

    move v3, v5

    .line 2751034
    invoke-interface {v0, v1, v2, v4, v3}, LX/89e;->a(IIIZ)V

    .line 2751035
    invoke-direct {p0, v4}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a(I)V

    .line 2751036
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2751037
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->d(I)V

    .line 2751038
    :cond_0
    return-void
.end method

.method public static B(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 2

    .prologue
    .line 2751039
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->CREATIVECAM:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->n()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->a()LX/8AA;

    move-result-object v0

    .line 2751040
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsVideos()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2751041
    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    .line 2751042
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v1, :cond_1

    .line 2751043
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/0Px;)LX/8AA;

    .line 2751044
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    .line 2751045
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2751046
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2751047
    return-void

    .line 2751048
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsPhotos()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2751049
    invoke-virtual {v0}, LX/8AA;->k()LX/8AA;

    goto :goto_0
.end method

.method public static C$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 2

    .prologue
    .line 2751050
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2751051
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->X:Z

    .line 2751052
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2751053
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2751054
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;ZLcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;)LX/BFq;
    .locals 3

    .prologue
    .line 2751099
    new-instance v0, LX/Jvg;

    invoke-direct {v0, p0}, LX/Jvg;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    new-instance v1, LX/Jvh;

    invoke-direct {v1, p0}, LX/Jvh;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2751100
    new-instance p0, LX/BFq;

    invoke-direct {p0, v0, v1, p2, v2}, LX/BFq;-><init>(LX/Jvg;LX/Jvh;Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;Ljava/lang/Boolean;)V

    .line 2751101
    move-object v0, p0

    .line 2751102
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;
    .locals 2

    .prologue
    .line 2751055
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2751056
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2751057
    new-instance v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {v1}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;-><init>()V

    .line 2751058
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2751059
    return-object v1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 2751060
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2751061
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d0ba5

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2751062
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 2751063
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    new-instance v2, LX/89R;

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v0}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/89W;->CREATIVE_CAM_FRONT:LX/89W;

    :goto_0
    iget-object v3, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    iget-object v4, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v4}, LX/89e;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v5}, LX/89e;->b()LX/0Px;

    move-result-object v5

    invoke-direct {v2, v0, v3, v4, v5}, LX/89R;-><init>(LX/89W;LX/0Px;Ljava/lang/String;LX/0Px;)V

    invoke-interface {v1, p1, v2}, LX/89S;->a(Landroid/net/Uri;LX/89R;)V

    .line 2751064
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_1

    const-string v0, "picker"

    .line 2751065
    :goto_1
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/9c4;->CREATIVECAM_CAPTURE_PHOTO_CONFIRMED:LX/9c4;

    invoke-virtual {v4}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "composer"

    .line 2751066
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2751067
    move-object v3, v3

    .line 2751068
    sget-object v4, LX/9c5;->SOURCE:LX/9c5;

    invoke-virtual {v4}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {v4}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2751069
    invoke-static {v1, v3}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2751070
    return-void

    .line 2751071
    :cond_0
    sget-object v0, LX/89W;->CREATIVE_CAM_BACK:LX/89W;

    goto :goto_0

    .line 2751072
    :cond_1
    const-string v0, "camera"

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;Landroid/net/Uri;LX/5fk;I)V
    .locals 6
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2751073
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->L:Z

    .line 2751074
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->y:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v1}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e()Z

    move-result v1

    const/4 v4, 0x0

    .line 2751075
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->e:Landroid/content/Context;

    const/high16 v3, 0x435c0000    # 220.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 2751076
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v3

    new-instance v5, LX/1o9;

    invoke-direct {v5, v2, v2}, LX/1o9;-><init>(II)V

    .line 2751077
    iput-object v5, v3, LX/1bX;->c:LX/1o9;

    .line 2751078
    move-object v2, v3

    .line 2751079
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v2

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 2751080
    iget-object v3, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->f:LX/1Ad;

    invoke-virtual {v3, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    iget-object v3, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->h:LX/1Ai;

    invoke-virtual {v2, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    sget-object v3, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 2751081
    move-object v3, v2

    .line 2751082
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    invoke-virtual {v2}, LX/1af;->b()V

    .line 2751083
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2751084
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2751085
    if-eqz p2, :cond_0

    .line 2751086
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p2, LX/5fk;->a:Landroid/graphics/Rect;

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2751087
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p2, v4}, LX/5fk;->a(I)Landroid/graphics/Rect;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2751088
    iget-object v4, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->b:LX/BG9;

    .line 2751089
    iput-object v2, v4, LX/BG9;->i:Landroid/graphics/RectF;

    .line 2751090
    iput-object v3, v4, LX/BG9;->j:Landroid/graphics/RectF;

    .line 2751091
    iput p3, v4, LX/BG9;->k:I

    .line 2751092
    iput-boolean v1, v4, LX/BG9;->l:Z

    .line 2751093
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    iget-object v4, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->b:LX/BG9;

    invoke-virtual {v2, v4}, LX/1af;->a(LX/1Up;)V

    .line 2751094
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float v3, v4, v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2751095
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->d(I)V

    .line 2751096
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a(I)V

    .line 2751097
    return-void

    .line 2751098
    :cond_0
    iget-object v2, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    iget-boolean v3, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->g:Z

    if-nez v3, :cond_1

    const/4 v3, 0x0

    invoke-static {p1, v3}, LX/74d;->a(Landroid/net/Uri;Lcom/facebook/photos/base/media/PhotoItem;)F

    move-result v3

    sget-object v4, LX/89U;->PORTRAIT_4_3:LX/89U;

    invoke-virtual {v4}, LX/89U;->getValue()F

    move-result v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_2

    :cond_1
    sget-object v3, LX/1Up;->g:LX/1Up;

    :goto_1
    invoke-virtual {v2, v3}, LX/1af;->a(LX/1Up;)V

    goto :goto_0

    :cond_2
    sget-object v3, LX/1Up;->c:LX/1Up;

    goto :goto_1
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 2750999
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2751000
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d2f8c

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2751001
    return-void
.end method

.method public static l(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 5

    .prologue
    .line 2751002
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d0864

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2751003
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v1}, LX/89S;->e()LX/BFW;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v1}, LX/89S;->e()LX/BFW;

    .line 2751004
    goto/16 :goto_1

    .line 2751005
    :goto_0
    move-object v1, v1

    .line 2751006
    iput-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->S:LX/BFZ;

    .line 2751007
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->S:LX/BFZ;

    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Z:LX/Jvk;

    iget-object v3, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->aa:LX/Jvl;

    .line 2751008
    const v4, 0x7f0303ac

    invoke-virtual {v0, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2751009
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, v1, LX/BFZ;->a:Landroid/widget/ImageButton;

    .line 2751010
    new-instance v4, LX/BFX;

    invoke-direct {v4, v1, v2, v3}, LX/BFX;-><init>(LX/BFZ;LX/Jvk;LX/Jvl;)V

    iput-object v4, v1, LX/BFZ;->k:Landroid/view/View$OnClickListener;

    .line 2751011
    iget-object v4, v1, LX/BFZ;->a:Landroid/widget/ImageButton;

    iget-object p0, v1, LX/BFZ;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2751012
    iget-object v4, v1, LX/BFZ;->a:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, v1, LX/BFZ;->b:Landroid/content/res/Resources;

    .line 2751013
    iget-object v4, v1, LX/BFZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f0b0beb

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sput v4, LX/BFZ;->d:I

    .line 2751014
    iget-object v4, v1, LX/BFZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f0b0bea

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sput v4, LX/BFZ;->e:I

    .line 2751015
    iget-object v4, v1, LX/BFZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f0b0be9

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sput v4, LX/BFZ;->f:I

    .line 2751016
    iget-object v4, v1, LX/BFZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f0b0bf0

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sput v4, LX/BFZ;->g:I

    .line 2751017
    iget-object v4, v1, LX/BFZ;->a:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/LayerDrawable;

    iput-object v4, v1, LX/BFZ;->j:Landroid/graphics/drawable/LayerDrawable;

    .line 2751018
    iget-object v4, v1, LX/BFZ;->j:Landroid/graphics/drawable/LayerDrawable;

    const p0, 0x7f0d31dd

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v1, LX/BFZ;->h:Landroid/graphics/drawable/Drawable;

    .line 2751019
    iget-object v4, v1, LX/BFZ;->j:Landroid/graphics/drawable/LayerDrawable;

    const p0, 0x7f0d31de

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v1, LX/BFZ;->i:Landroid/graphics/drawable/Drawable;

    .line 2751020
    iget-object v4, v1, LX/BFZ;->i:Landroid/graphics/drawable/Drawable;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2751021
    return-void

    .line 2751022
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BFZ;

    goto/16 :goto_0
.end method

.method public static p(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 11

    .prologue
    .line 2750983
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d087b

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2750984
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->j:Landroid/view/View;

    .line 2750985
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d17cd

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2750986
    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->x:LX/BGA;

    new-instance v3, LX/Jve;

    invoke-direct {v3, p0}, LX/Jve;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    const/4 v4, 0x1

    .line 2750987
    new-instance v5, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    const-class v6, Landroid/content/Context;

    invoke-interface {v2, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v10

    check-cast v10, LX/1Ad;

    move-object v6, v3

    move-object v7, v0

    move v8, v4

    invoke-direct/range {v5 .. v10}, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;-><init>(LX/Jve;Lcom/facebook/drawee/fbpipeline/FbDraweeView;ZLandroid/content/Context;LX/1Ad;)V

    .line 2750988
    move-object v2, v5

    .line 2750989
    move-object v0, v2

    .line 2750990
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->y:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    .line 2750991
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d2685

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->U:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2750992
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d2f8d

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2750993
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->I:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2750994
    new-instance v1, LX/Jvq;

    invoke-direct {v1, p0}, LX/Jvq;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2750995
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const v1, 0x7f0d2f8e

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2750996
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->I:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2750997
    new-instance v1, LX/Jvr;

    invoke-direct {v1, p0}, LX/Jvr;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2750998
    return-void
.end method

.method public static r(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 2

    .prologue
    .line 2750981
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->S:LX/BFZ;

    sget-object v1, LX/89b;->IMAGE:LX/89b;

    invoke-virtual {v0, v1}, LX/BFZ;->a(LX/89b;)V

    .line 2750982
    return-void
.end method

.method public static s$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 2

    .prologue
    .line 2750979
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->S:LX/BFZ;

    sget-object v1, LX/89b;->VIDEO:LX/89b;

    invoke-virtual {v0, v1}, LX/BFZ;->a(LX/89b;)V

    .line 2750980
    return-void
.end method

.method public static t(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)LX/89f;
    .locals 3

    .prologue
    .line 2750969
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v0}, LX/89S;->d()LX/89f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2750970
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v0}, LX/89S;->d()LX/89f;

    move-result-object v0

    .line 2750971
    :goto_0
    return-object v0

    .line 2750972
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BFo;

    .line 2750973
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->V:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750974
    iget-object p0, v2, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->k:LX/89Z;

    move-object v2, p0

    .line 2750975
    iput-object v2, v0, LX/BFo;->c:LX/89Z;

    .line 2750976
    iget-object p0, v0, LX/BFo;->b:LX/9c7;

    .line 2750977
    iput-object v1, p0, LX/9c7;->b:Ljava/lang/String;

    .line 2750978
    goto :goto_0
.end method

.method public static z(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2750965
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2750966
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->s:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v1, :cond_0

    .line 2750967
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->s:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    const-class v2, LX/1kW;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/BMQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2750968
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2750927
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2750928
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v0}, LX/9c7;->b(LX/0QB;)LX/9c7;

    move-result-object v3

    check-cast v3, LX/9c7;

    const-class v4, LX/0i4;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/0i4;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    const-class v6, LX/BG7;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/BG7;

    const-class v7, LX/BGA;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BGA;

    const-class v8, LX/BFk;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/BFk;

    const-class v9, LX/BFr;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/BFr;

    const-class v10, LX/BGD;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/BGD;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v11

    check-cast v11, LX/23P;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v13

    check-cast v13, LX/8GN;

    iput-object v3, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iput-object v4, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->t:LX/0i4;

    iput-object v5, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->u:LX/0kL;

    iput-object v6, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->v:LX/BG7;

    iput-object v7, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->x:LX/BGA;

    iput-object v8, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->z:LX/BFk;

    iput-object v9, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->B:LX/BFr;

    iput-object v10, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->D:LX/BGD;

    iput-object v11, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->I:LX/23P;

    iput-object v12, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->J:Ljava/util/concurrent/Executor;

    iput-object v13, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->K:LX/8GN;

    const/16 v3, 0x2e1f

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2e20

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x2e1e

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2e21

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    iput-object v3, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->b:LX/0Or;

    iput-object v5, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->c:LX/0Or;

    iput-object v6, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->d:LX/0Or;

    .line 2750929
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2750930
    const-string v1, "extra_creativecam_launch_configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750931
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2750932
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750933
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->k:LX/89Z;

    move-object v0, v1

    .line 2750934
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    .line 2750935
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750936
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    move-object v0, v1

    .line 2750937
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    .line 2750938
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750939
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->h:LX/0Px;

    move-object v0, v1

    .line 2750940
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    .line 2750941
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2750942
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2750943
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->p:LX/0Px;

    .line 2750944
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2750945
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    .line 2750946
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2750947
    const-string v1, "extra_creativecam_composer_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->V:Ljava/lang/String;

    .line 2750948
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2750949
    const-string v1, "extra_creativecam_prompt_entrypoint_analytics"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->s:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 2750950
    if-eqz p1, :cond_1

    .line 2750951
    const-string v0, "mediaSelectedFromPicker"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    .line 2750952
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->V:Ljava/lang/String;

    .line 2750953
    iput-object v1, v0, LX/9c7;->b:Ljava/lang/String;

    .line 2750954
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    .line 2750955
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/9c4;->CREATIVECAM_ENTRY:LX/9c4;

    invoke-virtual {v3}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "composer"

    .line 2750956
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2750957
    move-object v2, v2

    .line 2750958
    invoke-static {v0, v2}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2750959
    return-void

    .line 2750960
    :cond_2
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2750961
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 2750962
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2750963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2750964
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->p:LX/0Px;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2750913
    iget-boolean v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->L:Z

    if-eqz v1, :cond_1

    .line 2750914
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    .line 2750915
    invoke-static {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->B(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750916
    :goto_0
    invoke-static {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->C$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750917
    const/4 v0, 0x1

    .line 2750918
    :goto_1
    return v0

    .line 2750919
    :cond_0
    invoke-static {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    goto :goto_0

    .line 2750920
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->z(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2750921
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    .line 2750922
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/9c4;->CREATIVECAM_EXIT_BACK:LX/9c4;

    invoke-virtual {p0}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object p0, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "composer"

    .line 2750923
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2750924
    move-object v3, v3

    .line 2750925
    invoke-static {v1, v3}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2750926
    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2750894
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2750895
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2750896
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2750897
    invoke-static {p0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750898
    :cond_0
    :goto_0
    return-void

    .line 2750899
    :cond_1
    if-eqz p3, :cond_0

    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2750900
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2750901
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2750902
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2750903
    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    .line 2750904
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsPhotos()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2750905
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    .line 2750906
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750907
    iget-boolean v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->d:Z

    move v0, v1

    .line 2750908
    if-eqz v0, :cond_2

    .line 2750909
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;Landroid/net/Uri;)V

    goto :goto_0

    .line 2750910
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;Landroid/net/Uri;LX/5fk;I)V

    goto :goto_0

    .line 2750911
    :cond_3
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    if-eqz v1, :cond_0

    .line 2750912
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    new-instance v2, LX/89R;

    sget-object v3, LX/89W;->CREATIVE_CAM_PICKER:LX/89W;

    iget-object v4, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    iget-object v5, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v5}, LX/89e;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v6}, LX/89e;->b()LX/0Px;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, LX/89R;-><init>(LX/89W;LX/0Px;Ljava/lang/String;LX/0Px;)V

    invoke-interface {v1, v0, v2}, LX/89S;->b(Landroid/net/Uri;LX/89R;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x50eef38e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2750884
    const v0, 0x7f0303ae

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    .line 2750885
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    iget-object v2, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 2750886
    iget-object v3, v2, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->l:LX/89U;

    move-object v2, v3

    .line 2750887
    invoke-virtual {v2}, LX/89U;->getValue()F

    move-result v2

    .line 2750888
    iput v2, v0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    .line 2750889
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v0}, LX/4gI;->supportsVideos()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->g:[Ljava/lang/String;

    move-object v2, v0

    .line 2750890
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2750891
    iget-object v3, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->t:LX/0i4;

    invoke-virtual {v3, v0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    new-instance v3, LX/Jvm;

    invoke-direct {v3, p0}, LX/Jvm;-><init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2750892
    iget-object v0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->h:Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;

    const/16 v2, 0x2b

    const v3, -0x4fb2678b

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    .line 2750893
    :cond_0
    sget-object v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->f:[Ljava/lang/String;

    move-object v2, v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x49458e5a    # 809189.6f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750877
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    if-eqz v1, :cond_0

    .line 2750878
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->a()V

    .line 2750879
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->S:LX/BFZ;

    .line 2750880
    iget-object v2, v1, LX/BFZ;->i:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2750881
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->E:LX/BGC;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/BGC;->a(Z)V

    .line 2750882
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2750883
    const/16 v1, 0x2b

    const v2, -0x540aa05e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x9130b89    # 1.7699905E-33f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750872
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2750873
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    if-eqz v1, :cond_0

    .line 2750874
    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 2750875
    iget-object v2, v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v2}, Lcom/facebook/optic/CameraPreviewView;->b()V

    .line 2750876
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x40709b95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2750869
    const-string v0, "mediaSelectedFromPicker"

    iget-object v1, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->M:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2750870
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2750871
    return-void
.end method
