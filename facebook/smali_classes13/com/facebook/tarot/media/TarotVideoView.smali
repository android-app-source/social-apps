.class public Lcom/facebook/tarot/media/TarotVideoView;
.super LX/2oW;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K8s;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/K8u;

.field private r:Z

.field private s:LX/Cso;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2776453
    const-class v0, Lcom/facebook/tarot/media/TarotVideoView;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/media/TarotVideoView;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2776454
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/media/TarotVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2776455
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2776456
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/media/TarotVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2776457
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2776458
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2776459
    sget-object v0, LX/K8u;->STORY_CARD:LX/K8u;

    iput-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->q:LX/K8u;

    .line 2776460
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->r:Z

    .line 2776461
    invoke-direct {p0}, Lcom/facebook/tarot/media/TarotVideoView;->c()V

    .line 2776462
    return-void
.end method

.method private static a(Lcom/facebook/tarot/media/TarotVideoView;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tarot/media/TarotVideoView;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/K8s;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2776463
    iput-object p1, p0, Lcom/facebook/tarot/media/TarotVideoView;->m:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/tarot/media/TarotVideoView;->n:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/tarot/media/TarotVideoView;->o:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/media/TarotVideoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/media/TarotVideoView;

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x35f4

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1358

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/tarot/media/TarotVideoView;->a(Lcom/facebook/tarot/media/TarotVideoView;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2776464
    const-class v0, Lcom/facebook/tarot/media/TarotVideoView;

    invoke-static {v0, p0}, Lcom/facebook/tarot/media/TarotVideoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2776465
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2776446
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->q:LX/K8u;

    sget-object v1, LX/K8u;->STORY_CARD:LX/K8u;

    if-ne v0, v1, :cond_1

    .line 2776447
    sget-object v0, LX/04D;->TAROT_VIDEO_CARD:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 2776448
    :cond_0
    :goto_0
    return-void

    .line 2776449
    :cond_1
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->q:LX/K8u;

    sget-object v1, LX/K8u;->DIGEST_COVER:LX/K8u;

    if-ne v0, v1, :cond_2

    .line 2776450
    sget-object v0, LX/04D;->TAROT_COVER_CARD:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    goto :goto_0

    .line 2776451
    :cond_2
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->q:LX/K8u;

    sget-object v1, LX/K8u;->END_CARD:LX/K8u;

    if-ne v0, v1, :cond_0

    .line 2776452
    sget-object v0, LX/04D;->TAROT_END_CARD:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2776428
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2776429
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->l()Z

    .line 2776430
    new-instance v1, LX/Csy;

    invoke-virtual {p0}, Lcom/facebook/tarot/media/TarotVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/tarot/media/TarotVideoView;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, p0, v3}, LX/Csy;-><init>(Landroid/content/Context;LX/2oW;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2776431
    new-instance v1, LX/Ctw;

    invoke-direct {v1, p1}, LX/Ctw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2776432
    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/tarot/media/TarotVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2776433
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/tarot/data/VideoData;IILX/K8u;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 2776378
    iput-object p4, p0, Lcom/facebook/tarot/media/TarotVideoView;->q:LX/K8u;

    .line 2776379
    invoke-direct {p0}, Lcom/facebook/tarot/media/TarotVideoView;->d()V

    .line 2776380
    new-instance v0, LX/Csx;

    iget-object v1, p1, Lcom/facebook/tarot/data/VideoData;->g:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p2, p3}, LX/Csx;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2776381
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 2776382
    const-string v2, "CoverImageParamsKey"

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2776383
    invoke-static {p1}, Lcom/facebook/tarot/data/VideoData;->b(Lcom/facebook/tarot/data/VideoData;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2776384
    if-nez v0, :cond_1

    .line 2776385
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2776386
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "Tarot"

    const-string v3, "Invalid video URL: %s"

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/facebook/tarot/media/TarotVideoView;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K8s;

    .line 2776387
    iget-object v6, v1, LX/K8s;->d:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2776388
    const-string v6, ""

    .line 2776389
    :goto_1
    move-object v1, v6

    .line 2776390
    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2776391
    :cond_0
    :goto_2
    return-void

    .line 2776392
    :cond_1
    new-instance v0, LX/2pZ;

    invoke-direct {v0}, LX/2pZ;-><init>()V

    iget-wide v2, p1, Lcom/facebook/tarot/data/VideoData;->b:J

    long-to-double v2, v2

    iget-wide v4, p1, Lcom/facebook/tarot/data/VideoData;->c:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    .line 2776393
    iput-wide v2, v0, LX/2pZ;->e:D

    .line 2776394
    move-object v2, v0

    .line 2776395
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1m0;

    const/4 v7, 0x1

    .line 2776396
    invoke-static {p1}, Lcom/facebook/tarot/data/VideoData;->b(Lcom/facebook/tarot/data/VideoData;)Ljava/lang/String;

    move-result-object v4

    .line 2776397
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p1, Lcom/facebook/tarot/data/VideoData;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v5, v7}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v3

    .line 2776398
    if-nez v3, :cond_2

    .line 2776399
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2776400
    :cond_2
    new-instance v4, LX/2oE;

    invoke-direct {v4}, LX/2oE;-><init>()V

    .line 2776401
    iput-object v3, v4, LX/2oE;->a:Landroid/net/Uri;

    .line 2776402
    move-object v3, v4

    .line 2776403
    sget-object v4, LX/097;->FROM_STREAM:LX/097;

    .line 2776404
    iput-object v4, v3, LX/2oE;->e:LX/097;

    .line 2776405
    move-object v3, v3

    .line 2776406
    iget-object v4, p1, Lcom/facebook/tarot/data/VideoData;->f:Ljava/lang/String;

    .line 2776407
    iput-object v4, v3, LX/2oE;->d:Ljava/lang/String;

    .line 2776408
    move-object v3, v3

    .line 2776409
    invoke-virtual {v3}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v3

    .line 2776410
    new-instance v4, LX/2oH;

    invoke-direct {v4}, LX/2oH;-><init>()V

    invoke-virtual {v4, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v3

    .line 2776411
    iput-boolean v7, v3, LX/2oH;->g:Z

    .line 2776412
    move-object v3, v3

    .line 2776413
    invoke-virtual {v3}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 2776414
    move-object v0, v3

    .line 2776415
    iput-object v0, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2776416
    move-object v0, v2

    .line 2776417
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 2776418
    invoke-virtual {p0, v6}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 2776419
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2776420
    :cond_4
    iget-object v6, v1, LX/K8s;->d:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2776421
    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_5

    iget-object p0, v1, LX/K8s;->c:Ljava/util/Map;

    invoke-interface {p0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    .line 2776422
    :cond_5
    const-string v6, ""

    goto/16 :goto_1

    .line 2776423
    :cond_6
    iget-object p0, v1, LX/K8s;->c:Ljava/util/Map;

    invoke-interface {p0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/K79;

    .line 2776424
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2776425
    if-eqz v6, :cond_7

    .line 2776426
    invoke-virtual {v6, p0}, LX/K79;->a(Landroid/os/Bundle;)V

    .line 2776427
    :cond_7
    invoke-virtual {p0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1
.end method

.method public final a(ZLX/04g;)V
    .locals 0

    .prologue
    .line 2776434
    invoke-super {p0, p1, p2}, LX/2oW;->a(ZLX/04g;)V

    .line 2776435
    iput-boolean p1, p0, Lcom/facebook/tarot/media/TarotVideoView;->r:Z

    .line 2776436
    return-void
.end method

.method public getAudioPolicy()LX/Cso;
    .locals 1

    .prologue
    .line 2776437
    iget-object v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->s:LX/Cso;

    return-object v0
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 2776438
    sget-object v0, LX/04D;->TAROT_VIDEO_CARD:LX/04D;

    return-object v0
.end method

.method public getDefaultPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 2776439
    sget-object v0, LX/04G;->TAROT:LX/04G;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2776440
    iget-boolean v0, p0, Lcom/facebook/tarot/media/TarotVideoView;->r:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2776441
    invoke-super {p0, p1, p2}, LX/2oW;->onMeasure(II)V

    .line 2776442
    return-void
.end method

.method public setAudioPolicy(LX/Cso;)V
    .locals 0

    .prologue
    .line 2776443
    iput-object p1, p0, Lcom/facebook/tarot/media/TarotVideoView;->s:LX/Cso;

    .line 2776444
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2776445
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
