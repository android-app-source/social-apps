.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1d312af5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:D

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774481
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774482
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2774483
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2774484
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 2774485
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774486
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2774487
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->l()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2774488
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 2774489
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2774490
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->p()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2774491
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2774492
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->r()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2774493
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2774494
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->t()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 2774495
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2774496
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->e:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2774497
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2774498
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2774499
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->h:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2774500
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2774501
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2774502
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2774503
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2774504
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2774505
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2774506
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2774507
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->p:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2774508
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774509
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2774510
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774511
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2774512
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    .line 2774513
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2774514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2774515
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    .line 2774516
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2774517
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2774518
    if-eqz v2, :cond_1

    .line 2774519
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2774520
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->i:Ljava/util/List;

    move-object v1, v0

    .line 2774521
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2774522
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    .line 2774523
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2774524
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2774525
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->j:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    .line 2774526
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2774527
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2774528
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2774529
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2774530
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2774531
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2774532
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    .line 2774533
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2774534
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2774535
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    .line 2774536
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774537
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774538
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2774539
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2774540
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->e:J

    .line 2774541
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->h:D

    .line 2774542
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->p:D

    .line 2774543
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2774544
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;-><init>()V

    .line 2774545
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2774546
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2774547
    const v0, -0x7fd8f7bf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2774478
    const v0, -0x11b13572    # -1.5999696E28f

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2774479
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2774480
    iget-wide v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->e:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774476
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    .line 2774477
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774474
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->g:Ljava/lang/String;

    .line 2774475
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()D
    .locals 2

    .prologue
    .line 2774472
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2774473
    iget-wide v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->h:D

    return-wide v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2774470
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->i:Ljava/util/List;

    .line 2774471
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774468
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->j:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->j:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    .line 2774469
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->j:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774466
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k:Ljava/lang/String;

    .line 2774467
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774464
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2774465
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774462
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->m:Ljava/lang/String;

    .line 2774463
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774460
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    .line 2774461
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774456
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o:Ljava/lang/String;

    .line 2774457
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final u()D
    .locals 2

    .prologue
    .line 2774458
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2774459
    iget-wide v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->p:D

    return-wide v0
.end method
