.class public final Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x710d5d7d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2776010
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2776009
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2776007
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2776008
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2775995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2775996
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2775997
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2775998
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2775999
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2776000
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2776001
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2776002
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2776003
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2776004
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2776005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2776006
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2775982
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2775983
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2775984
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    .line 2775985
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2775986
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    .line 2775987
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->g:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    .line 2775988
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2775989
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    .line 2775990
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2775991
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    .line 2775992
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->h:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    .line 2775993
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2775994
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2775981
    new-instance v0, LX/K8k;

    invoke-direct {v0, p1}, LX/K8k;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775980
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 2776011
    const-string v0, "tarot_publisher_info.is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2776012
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v0

    .line 2776013
    if-eqz v0, :cond_0

    .line 2776014
    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2776015
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2776016
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2776017
    :goto_0
    return-void

    .line 2776018
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 2775971
    const-string v0, "tarot_publisher_info.is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2775972
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v0

    .line 2775973
    if-eqz v0, :cond_0

    .line 2775974
    if-eqz p3, :cond_1

    .line 2775975
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    .line 2775976
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;->a(Z)V

    .line 2775977
    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->h:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    .line 2775978
    :cond_0
    :goto_0
    return-void

    .line 2775979
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;->a(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2775968
    new-instance v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;-><init>()V

    .line 2775969
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2775970
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2775967
    const v0, 0x12bf63db

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2775958
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775965
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->e:Ljava/lang/String;

    .line 2775966
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775963
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->f:Ljava/lang/String;

    .line 2775964
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775961
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->g:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->g:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    .line 2775962
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->g:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775959
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->h:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->h:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    .line 2775960
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->h:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    return-object v0
.end method
