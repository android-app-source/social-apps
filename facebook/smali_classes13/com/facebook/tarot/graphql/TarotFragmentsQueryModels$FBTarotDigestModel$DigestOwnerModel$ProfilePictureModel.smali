.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774212
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774211
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2774198
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2774199
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2774205
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774206
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2774207
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2774208
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2774209
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774210
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2774213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774215
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774203
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;->e:Ljava/lang/String;

    .line 2774204
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2774200
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;-><init>()V

    .line 2774201
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2774202
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2774197
    const v0, 0x32b6aa28

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2774196
    const v0, 0x437b93b

    return v0
.end method
