.class public final Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2ed4cbe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2776090
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2776089
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2776087
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2776088
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2776079
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2776080
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2776081
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2776082
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2776083
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2776084
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2776085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2776086
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2776091
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->e:Ljava/util/List;

    .line 2776092
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2776066
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2776067
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2776068
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2776069
    if-eqz v1, :cond_0

    .line 2776070
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;

    .line 2776071
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->e:Ljava/util/List;

    .line 2776072
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2776073
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2776074
    if-eqz v1, :cond_1

    .line 2776075
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;

    .line 2776076
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->f:Ljava/util/List;

    .line 2776077
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2776078
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2776063
    new-instance v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;-><init>()V

    .line 2776064
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2776065
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2776062
    const v0, -0x33d3aeff    # -4.5171716E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2776061
    const v0, 0x3b6569aa

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2776059
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->f:Ljava/util/List;

    .line 2776060
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
