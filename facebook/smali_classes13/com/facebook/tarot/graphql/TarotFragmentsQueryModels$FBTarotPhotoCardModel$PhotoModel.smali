.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x20cf9620
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774680
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774681
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2774684
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2774685
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774682
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->e:Ljava/lang/String;

    .line 2774683
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774668
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->g:Ljava/lang/String;

    .line 2774669
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2774670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774671
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2774672
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2774673
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2774674
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2774675
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2774676
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2774677
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2774678
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774679
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2774660
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774661
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2774662
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    .line 2774663
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2774664
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    .line 2774665
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    .line 2774666
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774667
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774659
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2774656
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;-><init>()V

    .line 2774657
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2774658
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2774655
    const v0, 0x25d579cf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2774654
    const v0, 0x4984e12

    return v0
.end method

.method public final j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774652
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    .line 2774653
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel$ImageModel;

    return-object v0
.end method
