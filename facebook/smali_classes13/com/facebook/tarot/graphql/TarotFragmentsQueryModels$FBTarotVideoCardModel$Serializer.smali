.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2774828
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel;

    new-instance v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2774829
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2774830
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2774831
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2774832
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2774833
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2774834
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2774835
    if-eqz v2, :cond_0

    .line 2774836
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774837
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2774838
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774839
    if-eqz v2, :cond_1

    .line 2774840
    const-string p0, "card_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774841
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774842
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774843
    if-eqz v2, :cond_2

    .line 2774844
    const-string p0, "card_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774845
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774846
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774847
    if-eqz v2, :cond_3

    .line 2774848
    const-string p0, "featured_article"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774849
    invoke-static {v1, v2, p1, p2}, LX/K8S;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2774850
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774851
    if-eqz v2, :cond_4

    .line 2774852
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774853
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2774854
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774855
    if-eqz v2, :cond_5

    .line 2774856
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774857
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774858
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774859
    if-eqz v2, :cond_6

    .line 2774860
    const-string p0, "video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774861
    invoke-static {v1, v2, p1, p2}, LX/K8b;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2774862
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2774863
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2774864
    check-cast p1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$Serializer;->a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel;LX/0nX;LX/0my;)V

    return-void
.end method
