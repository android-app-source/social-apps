.class public final Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5e963588
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2775817
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2775818
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2775819
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2775820
    return-void
.end method

.method private a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775821
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->e:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->e:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    .line 2775822
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->e:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2775823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2775824
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2775825
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2775826
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2775827
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2775828
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2775829
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2775830
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2775831
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    .line 2775832
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2775833
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;

    .line 2775834
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;->e:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    .line 2775835
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2775836
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2775837
    new-instance v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$FBTarotNotificationUnsubscribeMutationModel;-><init>()V

    .line 2775838
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2775839
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2775840
    const v0, 0x53329f46

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2775841
    const v0, 0x7493c511

    return v0
.end method
