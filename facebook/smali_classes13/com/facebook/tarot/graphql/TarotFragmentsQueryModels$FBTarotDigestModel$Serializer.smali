.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2774454
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    new-instance v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2774455
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2774453
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2774396
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2774397
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    .line 2774398
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2774399
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2774400
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 2774401
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774402
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2774403
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774404
    if-eqz v2, :cond_1

    .line 2774405
    const-string v3, "description_font"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774406
    invoke-static {v1, v2, p1}, LX/8a1;->a(LX/15i;ILX/0nX;)V

    .line 2774407
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774408
    if-eqz v2, :cond_2

    .line 2774409
    const-string v3, "description_font_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774410
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774411
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2774412
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_3

    .line 2774413
    const-string v4, "description_line_height_multiplier"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774414
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 2774415
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774416
    if-eqz v2, :cond_5

    .line 2774417
    const-string v3, "digest_cards"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774418
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2774419
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 2774420
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1, p2}, LX/K8T;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2774421
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2774422
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2774423
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774424
    if-eqz v2, :cond_6

    .line 2774425
    const-string v3, "digest_owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774426
    invoke-static {v1, v2, p1, p2}, LX/K8X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2774427
    :cond_6
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774428
    if-eqz v2, :cond_7

    .line 2774429
    const-string v3, "digest_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774430
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774431
    :cond_7
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774432
    if-eqz v2, :cond_8

    .line 2774433
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774434
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2774435
    :cond_8
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774436
    if-eqz v2, :cond_9

    .line 2774437
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774438
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774439
    :cond_9
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2774440
    if-eqz v2, :cond_a

    .line 2774441
    const-string v3, "title_font"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774442
    invoke-static {v1, v2, p1}, LX/8a1;->a(LX/15i;ILX/0nX;)V

    .line 2774443
    :cond_a
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2774444
    if-eqz v2, :cond_b

    .line 2774445
    const-string v3, "title_font_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774446
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774447
    :cond_b
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2774448
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_c

    .line 2774449
    const-string v4, "title_line_height_multiplier"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774450
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 2774451
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2774452
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2774395
    check-cast p1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$Serializer;->a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;LX/0nX;LX/0my;)V

    return-void
.end method
