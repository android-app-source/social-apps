.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2774004
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel;

    new-instance v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2774005
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2774003
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2773974
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2773975
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2773976
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2773977
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2773978
    if-eqz v2, :cond_0

    .line 2773979
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2773980
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2773981
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2773982
    if-eqz v2, :cond_1

    .line 2773983
    const-string p0, "card_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2773984
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2773985
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2773986
    if-eqz v2, :cond_2

    .line 2773987
    const-string p0, "card_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2773988
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2773989
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2773990
    if-eqz v2, :cond_3

    .line 2773991
    const-string p0, "featured_article"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2773992
    invoke-static {v1, v2, p1, p2}, LX/K8S;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2773993
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2773994
    if-eqz v2, :cond_4

    .line 2773995
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2773996
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2773997
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2773998
    if-eqz v2, :cond_5

    .line 2773999
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2774000
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2774001
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2774002
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2773973
    check-cast p1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$Serializer;->a(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel;LX/0nX;LX/0my;)V

    return-void
.end method
