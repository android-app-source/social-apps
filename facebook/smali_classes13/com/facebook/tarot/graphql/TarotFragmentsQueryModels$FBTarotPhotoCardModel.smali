.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x535b9e13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774783
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774782
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2774780
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2774781
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2774778
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2774779
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774775
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->f:Ljava/lang/String;

    .line 2774776
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774723
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->g:Ljava/lang/String;

    .line 2774724
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774773
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->h:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->h:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    .line 2774774
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->h:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774771
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2774772
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774769
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->j:Ljava/lang/String;

    .line 2774770
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774767
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->k:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->k:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    .line 2774768
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->k:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2774749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774750
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2774751
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2774752
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2774753
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->m()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2774754
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2774755
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2774756
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2774757
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2774758
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2774759
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2774760
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2774761
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2774762
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2774763
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2774764
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2774765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774766
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2774731
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774732
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->m()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2774733
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->m()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    .line 2774734
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->m()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2774735
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;

    .line 2774736
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->h:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    .line 2774737
    :cond_0
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2774738
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2774739
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2774740
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;

    .line 2774741
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2774742
    :cond_1
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2774743
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    .line 2774744
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2774745
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;

    .line 2774746
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->k:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$PhotoModel;

    .line 2774747
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774748
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774730
    invoke-direct {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2774727
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;-><init>()V

    .line 2774728
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2774729
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2774726
    const v0, 0x14f3b45c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2774725
    const v0, 0x6ed6eb58

    return v0
.end method
