.class public final Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2775902
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2775901
    const-class v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2775877
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2775878
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2775893
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2775894
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2775895
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2775896
    iget v1, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 2775897
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2775898
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2775899
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2775900
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2775890
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2775891
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2775892
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2775888
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->f:Ljava/lang/String;

    .line 2775889
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2775884
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2775885
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->e:I

    .line 2775886
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;->g:I

    .line 2775887
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2775881
    new-instance v0, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$PageLogoModel;-><init>()V

    .line 2775882
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2775883
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2775880
    const v0, 0x40d37611

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2775879
    const v0, 0x437b93b

    return v0
.end method
