.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4e34f770
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774382
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2774385
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2774383
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2774384
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2774368
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774369
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2774370
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2774371
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2774372
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2774373
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2774374
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2774375
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2774376
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2774377
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2774378
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2774379
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2774380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774381
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2774355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2774356
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2774357
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    .line 2774358
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2774359
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    .line 2774360
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->g:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    .line 2774361
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2774362
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    .line 2774363
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2774364
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    .line 2774365
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->i:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    .line 2774366
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2774367
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2774354
    new-instance v0, LX/K8P;

    invoke-direct {v0, p1}, LX/K8P;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774353
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 2774345
    const-string v0, "tarot_publisher_info.is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2774346
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    .line 2774347
    if-eqz v0, :cond_0

    .line 2774348
    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2774349
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2774350
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2774351
    :goto_0
    return-void

    .line 2774352
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 2774386
    const-string v0, "tarot_publisher_info.is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2774387
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    .line 2774388
    if-eqz v0, :cond_0

    .line 2774389
    if-eqz p3, :cond_1

    .line 2774390
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    .line 2774391
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;->a(Z)V

    .line 2774392
    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->i:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    .line 2774393
    :cond_0
    :goto_0
    return-void

    .line 2774394
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;->a(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2774342
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;-><init>()V

    .line 2774343
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2774344
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2774341
    const v0, -0x418cfa03

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2774340
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774338
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->e:Ljava/lang/String;

    .line 2774339
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774336
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->f:Ljava/lang/String;

    .line 2774337
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774330
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->g:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->g:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    .line 2774331
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->g:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774334
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2774335
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2774332
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->i:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->i:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    .line 2774333
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->i:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    return-object v0
.end method
