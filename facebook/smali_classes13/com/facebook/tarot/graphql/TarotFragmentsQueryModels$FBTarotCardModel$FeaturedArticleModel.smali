.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2897e9f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2773970
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2773971
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2773968
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2773969
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2773960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2773961
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2773962
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2773963
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2773964
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2773965
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2773966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2773967
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2773952
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2773953
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2773954
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    .line 2773955
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2773956
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    .line 2773957
    iput-object v0, v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    .line 2773958
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2773959
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2773972
    invoke-virtual {p0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2773949
    new-instance v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;

    invoke-direct {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;-><init>()V

    .line 2773950
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2773951
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2773948
    const v0, -0x4b33e5f2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2773947
    const v0, 0x5fcedbf5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2773943
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->e:Ljava/lang/String;

    .line 2773944
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2773945
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    iput-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    .line 2773946
    iget-object v0, p0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel;->f:Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotCardModel$FeaturedArticleModel$LatestVersionModel;

    return-object v0
.end method
