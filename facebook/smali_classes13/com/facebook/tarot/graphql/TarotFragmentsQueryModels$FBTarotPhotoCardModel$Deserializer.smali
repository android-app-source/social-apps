.class public final Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2774548
    const-class v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;

    new-instance v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2774549
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2774550
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2774551
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2774552
    const/4 v2, 0x0

    .line 2774553
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 2774554
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2774555
    :goto_0
    move v1, v2

    .line 2774556
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2774557
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2774558
    new-instance v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;

    invoke-direct {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotPhotoCardModel;-><init>()V

    .line 2774559
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2774560
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2774561
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2774562
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2774563
    :cond_0
    return-object v1

    .line 2774564
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2774565
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_a

    .line 2774566
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2774567
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2774568
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 2774569
    const-string p0, "__type__"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2774570
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 2774571
    :cond_4
    const-string p0, "card_description"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2774572
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2774573
    :cond_5
    const-string p0, "card_title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2774574
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2774575
    :cond_6
    const-string p0, "featured_article"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2774576
    invoke-static {p1, v0}, LX/K8S;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2774577
    :cond_7
    const-string p0, "feedback"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2774578
    invoke-static {p1, v0}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2774579
    :cond_8
    const-string p0, "id"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2774580
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2774581
    :cond_9
    const-string p0, "photo"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2774582
    invoke-static {p1, v0}, LX/K8a;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 2774583
    :cond_a
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2774584
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2774585
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2774586
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2774587
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2774588
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2774589
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2774590
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2774591
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
