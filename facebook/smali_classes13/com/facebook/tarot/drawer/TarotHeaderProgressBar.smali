.class public Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Z

.field private b:LX/K7W;

.field private c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2773255
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2773256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2773257
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773258
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2773259
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773260
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    .line 2773261
    invoke-direct {p0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a()V

    .line 2773262
    return-void
.end method

.method private static a(FFF)F
    .locals 2

    .prologue
    .line 2773263
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    .line 2773264
    mul-float/2addr v0, p0

    mul-float v1, p2, p1

    add-float/2addr v0, v1

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2773265
    new-instance v0, LX/K7W;

    invoke-direct {v0, p0}, LX/K7W;-><init>(Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    .line 2773266
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2773267
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFF)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2773268
    iget-boolean v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v0, :cond_0

    .line 2773269
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v1, v1, LX/K7W;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2773270
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v4, v0, LX/K7W;->h:F

    iget-object v5, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v3, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;)V

    .line 2773271
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v1, v1, LX/K7W;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2773272
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v4, v0, LX/K7W;->h:F

    iget-object v5, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p4

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;)V

    .line 2773273
    :goto_0
    return-void

    .line 2773274
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v1, v1, LX/K7W;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2773275
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v4, v0, LX/K7W;->h:F

    iget-object v5, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p4

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;)V

    .line 2773276
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v1, v1, LX/K7W;->b:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2773277
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v4, v0, LX/K7W;->h:F

    iget-object v5, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v3, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;)V
    .locals 8

    .prologue
    const/high16 v5, 0x40400000    # 3.0f

    .line 2773278
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    move-object v7, p5

    .line 2773279
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    .line 2773280
    :goto_0
    return-void

    .line 2773281
    :cond_0
    invoke-virtual/range {p0 .. p5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IF)V
    .locals 1

    .prologue
    .line 2773282
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iput p1, v0, LX/K7W;->e:I

    .line 2773283
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iput p2, v0, LX/K7W;->f:F

    .line 2773284
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->invalidate()V

    .line 2773285
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2773286
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    invoke-virtual {v0, p1, p2}, LX/K7W;->a(II)V

    .line 2773287
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 2773288
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2773289
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v0, v0, LX/K7W;->d:I

    if-nez v0, :cond_1

    .line 2773290
    :cond_0
    return-void

    .line 2773291
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getPaddingRight()I

    move-result v0

    int-to-float v0, v0

    .line 2773292
    :goto_0
    const/4 v1, 0x0

    move v6, v1

    move v7, v0

    :goto_1
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v0, v0, LX/K7W;->d:I

    if-ge v6, v0, :cond_0

    .line 2773293
    iget-boolean v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v0, :cond_4

    const/4 v0, -0x1

    :goto_2
    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v1, v1, LX/K7W;->g:F

    mul-float/2addr v0, v1

    add-float v2, v7, v0

    .line 2773294
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v0, v0, LX/K7W;->h:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 2773295
    iget-boolean v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v0, :cond_5

    neg-float v0, v1

    :goto_3
    add-float/2addr v0, v7

    .line 2773296
    iget-object v3, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    invoke-virtual {v3, v6}, LX/K7W;->a(I)I

    move-result v4

    .line 2773297
    iget-boolean v3, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v3, :cond_6

    :goto_4
    add-float v3, v2, v1

    .line 2773298
    cmpg-float v1, v0, v3

    if-gez v1, :cond_7

    move v1, v0

    .line 2773299
    :goto_5
    cmpg-float v2, v0, v3

    if-gez v2, :cond_8

    .line 2773300
    :goto_6
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v0, v0, LX/K7W;->e:I

    add-int/lit8 v0, v0, 0x1

    if-ne v6, v0, :cond_a

    .line 2773301
    iget-boolean v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v0, :cond_9

    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v2, v2, LX/K7W;->f:F

    sub-float/2addr v0, v2

    :goto_7
    invoke-static {v1, v3, v0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(FFF)F

    move-result v0

    .line 2773302
    invoke-direct {p0, p1, v1, v3, v0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(Landroid/graphics/Canvas;FFF)V

    .line 2773303
    :goto_8
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v0, v0, LX/K7W;->g:F

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/high16 v1, 0x40800000    # 4.0f

    add-float/2addr v0, v1

    .line 2773304
    iget-boolean v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a:Z

    if-eqz v1, :cond_2

    neg-float v0, v0

    :cond_2
    add-float v1, v7, v0

    .line 2773305
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_1

    .line 2773306
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    .line 2773307
    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 2773308
    goto :goto_3

    .line 2773309
    :cond_6
    neg-float v1, v1

    goto :goto_4

    :cond_7
    move v1, v3

    .line 2773310
    goto :goto_5

    :cond_8
    move v3, v0

    .line 2773311
    goto :goto_6

    .line 2773312
    :cond_9
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v0, v0, LX/K7W;->f:F

    goto :goto_7

    .line 2773313
    :cond_a
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2773314
    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v4, v0, LX/K7W;->h:F

    iget-object v5, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;)V

    goto :goto_8
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2773315
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2773316
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    iget v1, v1, LX/K7W;->h:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->setMeasuredDimension(II)V

    .line 2773317
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->b:LX/K7W;

    invoke-virtual {v0}, LX/K7W;->a()V

    .line 2773318
    return-void
.end method
