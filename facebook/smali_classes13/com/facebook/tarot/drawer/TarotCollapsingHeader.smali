.class public Lcom/facebook/tarot/drawer/TarotCollapsingHeader;
.super LX/K7V;
.source ""

# interfaces
.implements LX/IXs;
.implements LX/Chq;


# instance fields
.field public a:LX/K7m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K87;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/K6a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/animation/ValueAnimator;

.field private e:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/fbui/glyph/GlyphView;

.field private h:I

.field private i:I

.field private j:Z

.field private k:I

.field private l:LX/K7T;

.field private m:LX/K7U;

.field private final n:LX/K7Q;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2773193
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2773194
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2773195
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2773197
    invoke-direct {p0, p1, p2, p3}, LX/K7V;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773198
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    .line 2773199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->j:Z

    .line 2773200
    new-instance v0, LX/K7T;

    invoke-direct {v0, p0}, LX/K7T;-><init>(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->l:LX/K7T;

    .line 2773201
    new-instance v0, LX/K7U;

    invoke-direct {v0, p0}, LX/K7U;-><init>(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->m:LX/K7U;

    .line 2773202
    new-instance v0, LX/K7R;

    invoke-direct {v0, p0}, LX/K7R;-><init>(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->n:LX/K7Q;

    .line 2773203
    invoke-direct {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->f()V

    .line 2773204
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 2773205
    if-ne p1, p2, :cond_1

    .line 2773206
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2773207
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2773208
    :cond_0
    :goto_0
    return-void

    .line 2773209
    :cond_1
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    .line 2773210
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2773211
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2773212
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;LX/K7m;LX/0Ot;LX/K6a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tarot/drawer/TarotCollapsingHeader;",
            "LX/K7m;",
            "LX/0Ot",
            "<",
            "LX/K87;",
            ">;",
            "LX/K6a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2773213
    iput-object p1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a:LX/K7m;

    iput-object p2, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->c:LX/K6a;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    invoke-static {v1}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v0

    check-cast v0, LX/K7m;

    const/16 v2, 0x35ef

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/K6a;->a(LX/0QB;)LX/K6a;

    move-result-object v1

    check-cast v1, LX/K6a;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;LX/K7m;LX/0Ot;LX/K6a;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;LX/K80;)V
    .locals 4

    .prologue
    .line 2773170
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->f:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_0

    .line 2773171
    :goto_0
    return-void

    .line 2773172
    :cond_0
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->f:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p1, LX/K80;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2773173
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->e:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    iget v2, p1, LX/K80;->f:I

    iget v0, p1, LX/K80;->b:I

    iget v3, p1, LX/K80;->c:I

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_2
    add-int/2addr v0, v2

    iget v2, p1, LX/K80;->e:I

    invoke-virtual {v1, v0, v2}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(II)V

    goto :goto_0

    .line 2773174
    :cond_1
    iget-object v0, p1, LX/K80;->d:Ljava/lang/String;

    goto :goto_1

    .line 2773175
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private f()V
    .locals 4

    .prologue
    .line 2773217
    const-class v0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    invoke-static {v0, p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2773218
    const v0, 0x7f031484

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2773219
    const v0, 0x7f0d2eb0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->e:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    .line 2773220
    const v0, 0x7f0d2eb1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2773221
    const v0, 0x7f0d2eaf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2773222
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->l:LX/K7T;

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2773223
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->g:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->m:LX/K7U;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2773224
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b261c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2773225
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2773226
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1287

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->h:I

    .line 2773227
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->i:I

    .line 2773228
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2773229
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->d:Landroid/animation/ValueAnimator;

    new-instance v1, LX/K7S;

    invoke-direct {v1, p0}, LX/K7S;-><init>(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2773230
    return-void
.end method

.method public static f(Lcom/facebook/tarot/drawer/TarotCollapsingHeader;I)V
    .locals 1

    .prologue
    .line 2773231
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2773232
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2773233
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2773234
    invoke-virtual {p0, p1}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->e(I)V

    .line 2773235
    return-void
.end method


# virtual methods
.method public final a(IIZ)V
    .locals 2

    .prologue
    .line 2773214
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->e:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, p1

    invoke-virtual {v1, v0, p2}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(II)V

    .line 2773215
    return-void

    .line 2773216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/Chr;)V
    .locals 0

    .prologue
    .line 2773190
    return-void
.end method

.method public final b(IFI)V
    .locals 1

    .prologue
    .line 2773191
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->e:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->a(IF)V

    .line 2773192
    return-void
.end method

.method public final b(LX/Chr;)V
    .locals 2

    .prologue
    .line 2773165
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getCurrentHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->k:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a(II)V

    .line 2773166
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 2773167
    return-void
.end method

.method public getCollapsedHeight()I
    .locals 1

    .prologue
    .line 2773168
    iget v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->h:I

    return v0
.end method

.method public getCurrentBottom()I
    .locals 1

    .prologue
    .line 2773169
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getBottom()I

    move-result v0

    return v0
.end method

.method public getCurrentHeight()I
    .locals 1

    .prologue
    .line 2773176
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->getHeight()I

    move-result v0

    return v0
.end method

.method public getExpandedHeight()I
    .locals 1

    .prologue
    .line 2773177
    iget v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->i:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1e25c673

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2773178
    invoke-super {p0}, LX/K7V;->onAttachedToWindow()V

    .line 2773179
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->n:LX/K7Q;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2773180
    const/16 v1, 0x2d

    const v2, 0x654c8e8b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x77a73131

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2773181
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->n:LX/K7Q;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2773182
    invoke-super {p0}, LX/K7V;->onDetachedFromWindow()V

    .line 2773183
    const/16 v1, 0x2d

    const v2, 0x78ba145e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setEnableExpansion(Z)V
    .locals 0

    .prologue
    .line 2773184
    iput-boolean p1, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->j:Z

    .line 2773185
    return-void
.end method

.method public setPublisherName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2773186
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2773187
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->invalidate()V

    .line 2773188
    return-void
.end method

.method public setRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 2773189
    return-void
.end method
