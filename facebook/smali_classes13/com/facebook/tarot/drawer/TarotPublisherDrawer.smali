.class public Lcom/facebook/tarot/drawer/TarotPublisherDrawer;
.super Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/K7m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/K87;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0So;
    .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/K6a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Lcom/facebook/common/callercontext/CallerContext;

.field private final g:I

.field private final h:LX/K7h;

.field private final i:LX/K7k;

.field public j:I

.field private k:I

.field private l:Landroid/content/Context;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/HorizontalScrollView;

.field private o:Lcom/facebook/widget/CustomLinearLayout;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/K7e;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/K5t;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/K7f;

.field public t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private final v:LX/K7i;

.field private final w:LX/K7d;

.field private final x:LX/K7Q;

.field private final y:LX/K7Y;

.field private final z:LX/K7a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2773527
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2773528
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2773503
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773504
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2773505
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2773506
    const-class v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2773507
    const/16 v0, 0x320

    iput v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->g:I

    .line 2773508
    new-instance v0, LX/K7h;

    invoke-direct {v0, p0}, LX/K7h;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->h:LX/K7h;

    .line 2773509
    new-instance v0, LX/K7k;

    invoke-direct {v0, p0}, LX/K7k;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->i:LX/K7k;

    .line 2773510
    iput v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->k:I

    .line 2773511
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->p:Ljava/util/List;

    .line 2773512
    new-instance v0, LX/K7e;

    invoke-direct {v0, p0}, LX/K7e;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    .line 2773513
    new-instance v0, LX/K7f;

    invoke-direct {v0, p0}, LX/K7f;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->s:LX/K7f;

    .line 2773514
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->t:Ljava/util/Map;

    .line 2773515
    new-instance v0, LX/K7i;

    invoke-direct {v0, p0}, LX/K7i;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->v:LX/K7i;

    .line 2773516
    new-instance v0, LX/K7d;

    invoke-direct {v0, p0}, LX/K7d;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->w:LX/K7d;

    .line 2773517
    new-instance v0, LX/K7X;

    invoke-direct {v0, p0}, LX/K7X;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->x:LX/K7Q;

    .line 2773518
    new-instance v0, LX/K7Z;

    invoke-direct {v0, p0}, LX/K7Z;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->y:LX/K7Y;

    .line 2773519
    new-instance v0, LX/K7b;

    invoke-direct {v0, p0}, LX/K7b;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->z:LX/K7a;

    .line 2773520
    invoke-direct {p0, p1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(Landroid/content/Context;)V

    .line 2773521
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 4

    .prologue
    .line 2773529
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2773530
    const v1, 0x7f031488

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->o:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2773531
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2773532
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2773533
    :cond_0
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2773534
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->v:LX/K7i;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2773535
    :cond_1
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2773536
    const v0, 0x7f0d2eb5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->m:Landroid/view/View;

    .line 2773537
    const v0, 0x7f0d2eb6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->o:Lcom/facebook/widget/CustomLinearLayout;

    .line 2773538
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 2773539
    invoke-direct {p0, p1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->e(I)I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;I)V

    .line 2773540
    return-void
.end method

.method private a(LX/K5t;)V
    .locals 3

    .prologue
    .line 2773541
    iget-object v0, p1, LX/K5t;->b:LX/K5s;

    move-object v0, v0

    .line 2773542
    iget-object v0, v0, LX/K5s;->c:Ljava/lang/String;

    .line 2773543
    iget-object v1, p1, LX/K5t;->b:LX/K5s;

    move-object v1, v1

    .line 2773544
    iget-object v1, v1, LX/K5s;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    .line 2773545
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->t:Ljava/util/Map;

    .line 2773546
    iget-object v2, p1, LX/K5t;->b:LX/K5s;

    move-object v2, v2

    .line 2773547
    iget-object v2, v2, LX/K5s;->a:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2773548
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->p:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2773549
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->o:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 2773550
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2773562
    const-class v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-static {v0, p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2773563
    const v0, 0x7f031487

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2773564
    iput-object p1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->l:Landroid/content/Context;

    .line 2773565
    const v0, 0x7f0d2eb3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->n:Landroid/widget/HorizontalScrollView;

    .line 2773566
    invoke-direct {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a()V

    .line 2773567
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->n:Landroid/widget/HorizontalScrollView;

    new-instance v1, LX/K7c;

    invoke-direct {v1, p0}, LX/K7c;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2773568
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b260a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->j:I

    .line 2773569
    return-void
.end method

.method private static a(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/0hB;LX/K7m;LX/K87;LX/0So;LX/K6a;)V
    .locals 0

    .prologue
    .line 2773551
    iput-object p1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a:LX/0hB;

    iput-object p2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b:LX/K7m;

    iput-object p3, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->c:LX/K87;

    iput-object p4, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->d:LX/0So;

    iput-object p5, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->e:LX/K6a;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-static {v5}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {v5}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v2

    check-cast v2, LX/K7m;

    invoke-static {v5}, LX/K87;->a(LX/0QB;)LX/K87;

    move-result-object v3

    check-cast v3, LX/K87;

    invoke-static {v5}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v5}, LX/K6a;->a(LX/0QB;)LX/K6a;

    move-result-object v5

    check-cast v5, LX/K6a;

    invoke-static/range {v0 .. v5}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/0hB;LX/K7m;LX/K87;LX/0So;LX/K6a;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/K80;)V
    .locals 4

    .prologue
    .line 2773556
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    iget-boolean v0, v0, LX/K7e;->b:Z

    if-eqz v0, :cond_0

    .line 2773557
    invoke-static {p0, p1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/K80;)V

    .line 2773558
    :goto_0
    return-void

    .line 2773559
    :cond_0
    sget-object v0, LX/K72;->PUBLISHER_CHANGE:LX/K72;

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(LX/K72;)V

    .line 2773560
    new-instance v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer$5;

    invoke-direct {v0, p0, p1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer$5;-><init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/K80;)V

    const-wide/16 v2, 0x320

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2773561
    iget v0, p1, LX/K80;->b:I

    iput v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->k:I

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/K83;)V
    .locals 1

    .prologue
    .line 2773554
    iget-object v0, p1, LX/K83;->a:LX/K72;

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b(LX/K72;)V

    .line 2773555
    return-void
.end method

.method public static a$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/K84;)V
    .locals 1

    .prologue
    .line 2773552
    iget-object v0, p1, LX/K84;->a:LX/K72;

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(LX/K72;)V

    .line 2773553
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2773522
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2773523
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->n:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->removeAllViews()V

    .line 2773524
    const v1, 0x7f031489

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->n:Landroid/widget/HorizontalScrollView;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2773525
    invoke-direct {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a()V

    .line 2773526
    return-void
.end method

.method public static b$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;I)V
    .locals 2

    .prologue
    .line 2773434
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2773435
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2773436
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->m:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2773437
    return-void
.end method

.method public static b$redex0(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;LX/K80;)V
    .locals 3

    .prologue
    .line 2773429
    iget v0, p1, LX/K80;->a:I

    invoke-direct {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->e(I)I

    move-result v0

    .line 2773430
    iget v1, p1, LX/K80;->b:I

    invoke-direct {p0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->e(I)I

    move-result v1

    .line 2773431
    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->h:LX/K7h;

    invoke-virtual {v2, v0, v1}, LX/K7h;->a(II)V

    .line 2773432
    iget v0, p1, LX/K80;->b:I

    iput v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->k:I

    .line 2773433
    return-void
.end method

.method private e(I)I
    .locals 1

    .prologue
    .line 2773438
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->o:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2773439
    if-eqz v0, :cond_0

    .line 2773440
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 2773441
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;I)V
    .locals 1

    .prologue
    .line 2773442
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2773443
    if-eqz v0, :cond_0

    .line 2773444
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2773445
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2773446
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->g(I)V

    .line 2773447
    return-void
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 2773448
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    iget-boolean v0, v0, LX/K7e;->b:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->j:I

    neg-int v0, v0

    if-ne p1, v0, :cond_0

    .line 2773449
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->c:LX/K87;

    new-instance v1, LX/K7q;

    invoke-direct {v1}, LX/K7q;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2773450
    :cond_0
    return-void
.end method

.method public static getTopMargin(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)I
    .locals 1

    .prologue
    .line 2773451
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2773452
    if-eqz v0, :cond_0

    .line 2773453
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2773454
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/K72;)V
    .locals 2

    .prologue
    .line 2773455
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    iget-boolean v0, v0, LX/K7e;->b:Z

    if-eqz v0, :cond_0

    .line 2773456
    :goto_0
    return-void

    .line 2773457
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->i:LX/K7k;

    invoke-virtual {v0}, LX/K7k;->a()V

    .line 2773458
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    invoke-virtual {v0, p1}, LX/K7e;->a(LX/K72;)V

    .line 2773459
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->c:LX/K87;

    new-instance v1, LX/K82;

    invoke-direct {v1, p1}, LX/K82;-><init>(LX/K72;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2773460
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->s:LX/K7f;

    invoke-virtual {v0}, LX/K7f;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/K5t;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2773461
    iput-object p1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->r:Ljava/util/List;

    .line 2773462
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->r:Ljava/util/List;

    if-nez v1, :cond_0

    .line 2773463
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2773464
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->o:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->removeAllViews()V

    .line 2773465
    :goto_0
    return-void

    .line 2773466
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b260b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2773467
    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    div-int v1, v2, v1

    iput v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->u:I

    .line 2773468
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->u:I

    if-le v1, v2, :cond_2

    const/4 v1, 0x1

    .line 2773469
    :goto_1
    if-eqz v1, :cond_1

    .line 2773470
    invoke-direct {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b()V

    :cond_1
    move v1, v0

    .line 2773471
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2773472
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    invoke-direct {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(LX/K5t;)V

    .line 2773473
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v0

    .line 2773474
    goto :goto_1

    .line 2773475
    :cond_3
    invoke-virtual {p0, p2}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->setCurrentActiveIndex(I)V

    goto :goto_0
.end method

.method public final b(LX/K72;)V
    .locals 2

    .prologue
    .line 2773476
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    iget-boolean v0, v0, LX/K7e;->b:Z

    if-nez v0, :cond_0

    .line 2773477
    :goto_0
    return-void

    .line 2773478
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->i:LX/K7k;

    invoke-virtual {v0}, LX/K7k;->b()V

    .line 2773479
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->q:LX/K7e;

    invoke-virtual {v0, p1}, LX/K7e;->b(LX/K72;)V

    .line 2773480
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->s:LX/K7f;

    invoke-virtual {v0}, LX/K7f;->b()V

    .line 2773481
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->c:LX/K87;

    new-instance v1, LX/K81;

    invoke-direct {v1, p1}, LX/K81;-><init>(LX/K72;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1d212422

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2773482
    invoke-super {p0}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->onAttachedToWindow()V

    .line 2773483
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->x:LX/K7Q;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2773484
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->y:LX/K7Y;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2773485
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->c:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->z:LX/K7a;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2773486
    iget v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->j:I

    neg-int v1, v1

    invoke-static {p0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->f(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;I)V

    .line 2773487
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->w:LX/K7d;

    invoke-virtual {p0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2773488
    const/16 v1, 0x2d

    const v2, -0x30c9b5ba

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x28a3774d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2773489
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2773490
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->x:LX/K7Q;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2773491
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->b:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->y:LX/K7Y;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2773492
    iget-object v1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->c:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->z:LX/K7a;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2773493
    invoke-super {p0}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->onDetachedFromWindow()V

    .line 2773494
    const/16 v1, 0x2d

    const v2, 0x56b8b667

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 2773495
    invoke-super/range {p0 .. p5}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->onLayout(ZIIII)V

    .line 2773496
    iget-object v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-nez v0, :cond_0

    .line 2773497
    iget v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->k:I

    invoke-direct {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(I)V

    .line 2773498
    :cond_0
    return-void
.end method

.method public setCurrentActiveIndex(I)V
    .locals 1

    .prologue
    .line 2773499
    iput p1, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->k:I

    .line 2773500
    iget v0, p0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->k:I

    invoke-direct {p0, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(I)V

    .line 2773501
    invoke-virtual {p0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->requestLayout()V

    .line 2773502
    return-void
.end method
