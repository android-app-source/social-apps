.class public final Lcom/facebook/tarot/cards/TarotCardDeckFragment$RemoveEmbeddedFragmentRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/tarot/cards/TarotCardDeckFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardDeckFragment;)V
    .locals 1

    .prologue
    .line 2771061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2771062
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment$RemoveEmbeddedFragmentRunnable;->a:Ljava/lang/ref/WeakReference;

    .line 2771063
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .prologue
    .line 2771064
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment$RemoveEmbeddedFragmentRunnable;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2771065
    if-eqz v0, :cond_0

    .line 2771066
    invoke-static {v0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->v(Lcom/facebook/tarot/cards/TarotCardDeckFragment;)V

    .line 2771067
    :cond_0
    return-void
.end method
