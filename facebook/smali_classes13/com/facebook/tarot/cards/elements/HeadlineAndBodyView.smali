.class public Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K91;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2771651
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771652
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2771656
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771657
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2771653
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771654
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->b()V

    .line 2771655
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    const/16 v1, 0x35f8

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->a:LX/0Ot;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2771641
    const-class v0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2771642
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setOrientation(I)V

    .line 2771643
    const v0, 0x7f03147d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2771644
    const v0, 0x7f0d2ea0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2771645
    const v0, 0x7f0d2e96

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2771646
    const v0, 0x7f0d2ea1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2771647
    iget-object v1, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K91;

    .line 2771648
    iget-object v2, v0, LX/K91;->e:Ljava/util/Map;

    sget-object p0, LX/K8y;->COVER_CARD_READ_STORY_CTA:LX/K8y;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8z;

    invoke-virtual {v2}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2771649
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771650
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2771658
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setHeadlineText(Ljava/lang/String;)V

    .line 2771659
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionText(Ljava/lang/String;)V

    .line 2771660
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setTitleFont(Landroid/graphics/Typeface;)V

    .line 2771661
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionFont(Landroid/graphics/Typeface;)V

    .line 2771662
    return-void
.end method

.method public setDescriptionFont(Landroid/graphics/Typeface;)V
    .locals 2

    .prologue
    .line 2771638
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2771639
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2771640
    return-void
.end method

.method public setDescriptionLineHeightMultiplier(F)V
    .locals 2

    .prologue
    .line 2771636
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setLineSpacing(FF)V

    .line 2771637
    return-void
.end method

.method public setDescriptionText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2771634
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771635
    return-void
.end method

.method public setHeadlineLineHeightMultiplier(F)V
    .locals 2

    .prologue
    .line 2771628
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setLineSpacing(FF)V

    .line 2771629
    return-void
.end method

.method public setHeadlineText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2771632
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771633
    return-void
.end method

.method public setTitleFont(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771630
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2771631
    return-void
.end method
