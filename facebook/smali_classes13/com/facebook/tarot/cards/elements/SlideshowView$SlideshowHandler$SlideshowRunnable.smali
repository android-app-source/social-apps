.class public final Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/K6W;


# direct methods
.method public constructor <init>(LX/K6W;)V
    .locals 0

    .prologue
    .line 2771706
    iput-object p1, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/K6W;B)V
    .locals 0

    .prologue
    .line 2771707
    invoke-direct {p0, p1}, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;-><init>(LX/K6W;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const-wide/16 v6, 0xbb8

    .line 2771708
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    iget-object v0, v0, LX/K6W;->a:Lcom/facebook/tarot/cards/elements/SlideshowView;

    iget-object v0, v0, Lcom/facebook/tarot/cards/elements/SlideshowView;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2771709
    iget-object v2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    iget-wide v2, v2, LX/K6W;->g:J

    add-long/2addr v2, v6

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2771710
    iget-object v2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    iget-object v2, v2, LX/K6W;->h:Landroid/os/Handler;

    iget-object v3, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    iget-wide v4, v3, LX/K6W;->g:J

    add-long/2addr v4, v6

    sub-long v0, v4, v0

    const v3, -0x6c257528

    invoke-static {v2, p0, v0, v1, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2771711
    :goto_0
    return-void

    .line 2771712
    :cond_0
    iget-object v2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    invoke-static {v2}, LX/K6W;->j(LX/K6W;)LX/K6V;

    move-result-object v2

    .line 2771713
    if-eqz v2, :cond_1

    .line 2771714
    iget-object v3, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    .line 2771715
    invoke-static {v3, v2}, LX/K6W;->a$redex0(LX/K6W;LX/K6V;)V

    .line 2771716
    iget-object v2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    iget-object v2, v2, LX/K6W;->f:LX/K6V;

    invoke-virtual {v2}, LX/K6V;->d()V

    .line 2771717
    iget-object v2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    invoke-static {v2}, LX/K6W;->g(LX/K6W;)V

    .line 2771718
    iget-object v2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    .line 2771719
    iput-wide v0, v2, LX/K6W;->g:J

    .line 2771720
    :cond_1
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView$SlideshowHandler$SlideshowRunnable;->a:LX/K6W;

    iget-object v0, v0, LX/K6W;->h:Landroid/os/Handler;

    const v1, 0x6b42ef5f

    invoke-static {v0, p0, v6, v7, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
