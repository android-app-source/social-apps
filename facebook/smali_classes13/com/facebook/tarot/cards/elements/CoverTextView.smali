.class public Lcom/facebook/tarot/cards/elements/CoverTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:I


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/K91;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2771555
    const-class v0, Lcom/facebook/tarot/cards/elements/CoverTextView;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/elements/CoverTextView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2771594
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771595
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2771592
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771593
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2771589
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771590
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->b()V

    .line 2771591
    return-void
.end method

.method private static a(Lcom/facebook/tarot/cards/elements/CoverTextView;LX/0hB;LX/K91;)V
    .locals 0

    .prologue
    .line 2771588
    iput-object p1, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->a:LX/0hB;

    iput-object p2, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->b:LX/K91;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/tarot/cards/elements/CoverTextView;

    invoke-static {v1}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-static {v1}, LX/K91;->a(LX/0QB;)LX/K91;

    move-result-object v1

    check-cast v1, LX/K91;

    invoke-static {p0, v0, v1}, Lcom/facebook/tarot/cards/elements/CoverTextView;->a(Lcom/facebook/tarot/cards/elements/CoverTextView;LX/0hB;LX/K91;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2771596
    const-class v0, Lcom/facebook/tarot/cards/elements/CoverTextView;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2771597
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setOrientation(I)V

    .line 2771598
    const v0, 0x7f031477

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2771599
    const v0, 0x7f0d2e95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2771600
    const v0, 0x7f0d2e96

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2771601
    const v0, 0x7f0d2e97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2771602
    return-void
.end method

.method private getMaxLogoHeight()I
    .locals 2

    .prologue
    .line 2771583
    sget v0, Lcom/facebook/tarot/cards/elements/CoverTextView;->g:I

    if-eqz v0, :cond_0

    .line 2771584
    sget v0, Lcom/facebook/tarot/cards/elements/CoverTextView;->g:I

    .line 2771585
    :goto_0
    return v0

    .line 2771586
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2610

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2771587
    sput v0, Lcom/facebook/tarot/cards/elements/CoverTextView;->g:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2771578
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setDescriptionText(Ljava/lang/String;)V

    .line 2771579
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setDescriptionFont(Landroid/graphics/Typeface;)V

    .line 2771580
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setCreationTimeText(Ljava/lang/String;)V

    .line 2771581
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->setCreationTextFont(Landroid/graphics/Typeface;)V

    .line 2771582
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 4

    .prologue
    .line 2771566
    if-nez p1, :cond_0

    .line 2771567
    :goto_0
    return-void

    .line 2771568
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/tarot/cards/elements/CoverTextView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2771569
    const v0, 0x3f19999a    # 0.6f

    iget-object v1, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 2771570
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/CoverTextView;->getMaxLogoHeight()I

    move-result v1

    .line 2771571
    int-to-float v0, v0

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 2771572
    int-to-float v2, p3

    mul-float/2addr v2, v0

    int-to-float v3, v1

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 2771573
    int-to-float v0, v1

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 2771574
    :cond_1
    iget-object v1, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2771575
    int-to-float v2, p2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2771576
    int-to-float v2, p3

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2771577
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setCreationTextFont(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771564
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2771565
    return-void
.end method

.method public setCreationTimeText(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2771560
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2771561
    iget-object v1, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771562
    return-void

    .line 2771563
    :cond_0
    const-string v0, "%s  |  %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iget-object v3, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->b:LX/K91;

    invoke-virtual {v3}, LX/K91;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setDescriptionFont(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771558
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2771559
    return-void
.end method

.method public setDescriptionText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2771556
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/CoverTextView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771557
    return-void
.end method
