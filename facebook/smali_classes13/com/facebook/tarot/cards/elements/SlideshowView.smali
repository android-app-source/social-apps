.class public Lcom/facebook/tarot/cards/elements/SlideshowView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0So;
    .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/tarot/media/TarotVideoView;

.field public g:Lcom/facebook/tarot/media/TarotVideoView;

.field private h:LX/K6W;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tarot/data/VideoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2771824
    const-class v0, Lcom/facebook/tarot/cards/elements/SlideshowView;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/elements/SlideshowView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2771832
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771833
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2771830
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771831
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2771825
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771826
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->i:Ljava/util/List;

    .line 2771827
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->j:Ljava/util/List;

    .line 2771828
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->f()V

    .line 2771829
    return-void
.end method

.method private static a(Lcom/facebook/tarot/cards/elements/SlideshowView;LX/0So;LX/0hB;)V
    .locals 0

    .prologue
    .line 2771823
    iput-object p1, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->a:LX/0So;

    iput-object p2, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->b:LX/0hB;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-static {v1}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-static {v1}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0, v0, v1}, Lcom/facebook/tarot/cards/elements/SlideshowView;->a(Lcom/facebook/tarot/cards/elements/SlideshowView;LX/0So;LX/0hB;)V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2771816
    const-class v0, Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2771817
    const v0, 0x7f031480

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2771818
    const v0, 0x7f0d2ea3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2771819
    const v0, 0x7f0d2ea4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2771820
    const v0, 0x7f0d2ea5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/media/TarotVideoView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->f:Lcom/facebook/tarot/media/TarotVideoView;

    .line 2771821
    const v0, 0x7f0d2ea6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/media/TarotVideoView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->g:Lcom/facebook/tarot/media/TarotVideoView;

    .line 2771822
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2771810
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2771811
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2771812
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    if-eqz v0, :cond_0

    .line 2771813
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    invoke-virtual {v0}, LX/K6W;->b()V

    .line 2771814
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    .line 2771815
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2771808
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2771809
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2771795
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    if-nez v0, :cond_0

    .line 2771796
    new-instance v0, LX/K6W;

    invoke-direct {v0, p0}, LX/K6W;-><init>(Lcom/facebook/tarot/cards/elements/SlideshowView;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    .line 2771797
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    invoke-virtual {v0}, LX/K6W;->a()V

    .line 2771798
    return-void
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/tarot/data/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2771806
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2771807
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2771802
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    if-nez v0, :cond_0

    .line 2771803
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->b()V

    .line 2771804
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    invoke-virtual {v0}, LX/K6W;->c()V

    .line 2771805
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2771799
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    if-eqz v0, :cond_0

    .line 2771800
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/SlideshowView;->h:LX/K6W;

    invoke-virtual {v0}, LX/K6W;->d()V

    .line 2771801
    :cond_0
    return-void
.end method
