.class public Lcom/facebook/tarot/cards/elements/FooterFeedbackView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2771625
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2771626
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a()V

    .line 2771627
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2771622
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771623
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a()V

    .line 2771624
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2771603
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771604
    invoke-direct {p0}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a()V

    .line 2771605
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2771615
    const v0, 0x7f03147c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2771616
    const v0, 0x7f0d2e9f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    iput-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    .line 2771617
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setVisibility(I)V

    .line 2771618
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setClipTokens(Z)V

    .line 2771619
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-virtual {p0}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setCountsTextColor(I)V

    .line 2771620
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setButtonsColor(I)V

    .line 2771621
    return-void
.end method


# virtual methods
.method public final a(LX/ClW;LX/162;LX/CnN;)V
    .locals 4

    .prologue
    .line 2771606
    new-instance v0, LX/CnO;

    const-string v1, "tarot_story"

    const-string v2, "tarot_ufi"

    sget-object v3, LX/21D;->TAROT_STORY:LX/21D;

    invoke-direct {v0, v1, v2, v3}, LX/CnO;-><init>(Ljava/lang/String;Ljava/lang/String;LX/21D;)V

    .line 2771607
    iget-object v1, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    .line 2771608
    iput-object v0, v1, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    .line 2771609
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-virtual {v0, p1}, LX/CnR;->setAnnotation(LX/ClW;)V

    .line 2771610
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/CnR;->setShowTopDivider(Z)V

    .line 2771611
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-virtual {v0, p2}, LX/CnR;->setFeedbackLoggingParams(LX/162;)V

    .line 2771612
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-virtual {v0, p3}, LX/CnR;->setComposerLaunchParams(LX/CnN;)V

    .line 2771613
    iget-object v0, p0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->bringToFront()V

    .line 2771614
    return-void
.end method
