.class public Lcom/facebook/tarot/cards/TarotCardVideo;
.super Lcom/facebook/tarot/cards/BaseTarotCard;
.source ""

# interfaces
.implements LX/K5b;


# instance fields
.field public c:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/K8w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/K5e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/K6S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/tarot/media/TarotVideoView;

.field public h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

.field private i:Landroid/widget/ImageView;

.field private j:LX/K6O;

.field private k:LX/K6M;

.field private final l:LX/K6N;

.field private final m:LX/K6L;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2771473
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/TarotCardVideo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771474
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2771493
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/TarotCardVideo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771494
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2771488
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/tarot/cards/BaseTarotCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771489
    new-instance v0, LX/K6N;

    invoke-direct {v0, p0}, LX/K6N;-><init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->l:LX/K6N;

    .line 2771490
    new-instance v0, LX/K6L;

    invoke-direct {v0, p0}, LX/K6L;-><init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->m:LX/K6L;

    .line 2771491
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->g()V

    .line 2771492
    return-void
.end method

.method private static a(Lcom/facebook/tarot/cards/TarotCardVideo;LX/0hB;LX/K8w;LX/K5e;LX/K6S;)V
    .locals 0

    .prologue
    .line 2771487
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->c:LX/0hB;

    iput-object p2, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->d:LX/K8w;

    iput-object p3, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->e:LX/K5e;

    iput-object p4, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->f:LX/K6S;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/TarotCardVideo;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/tarot/cards/TarotCardVideo;

    invoke-static {v3}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-static {v3}, LX/K8w;->a(LX/0QB;)LX/K8w;

    move-result-object v1

    check-cast v1, LX/K8w;

    invoke-static {v3}, LX/K5e;->b(LX/0QB;)LX/K5e;

    move-result-object v2

    check-cast v2, LX/K5e;

    invoke-static {v3}, LX/K6S;->b(LX/0QB;)LX/K6S;

    move-result-object v3

    check-cast v3, LX/K6S;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/tarot/cards/TarotCardVideo;->a(Lcom/facebook/tarot/cards/TarotCardVideo;LX/0hB;LX/K8w;LX/K5e;LX/K6S;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/tarot/cards/TarotCardVideo;LX/04g;)V
    .locals 3

    .prologue
    .line 2771481
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->d:LX/K8w;

    .line 2771482
    iget-boolean v1, v0, LX/K8w;->b:Z

    move v1, v1

    .line 2771483
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->g:Lcom/facebook/tarot/media/TarotVideoView;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2771484
    invoke-direct {p0, v1}, Lcom/facebook/tarot/cards/TarotCardVideo;->setAudioIconEnabled(Z)V

    .line 2771485
    return-void

    .line 2771486
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/tarot/cards/TarotCardVideo;Z)V
    .locals 2

    .prologue
    .line 2771475
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2771476
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 2771477
    if-eqz p1, :cond_1

    .line 2771478
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 2771479
    :cond_0
    :goto_0
    return-void

    .line 2771480
    :cond_1
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 2771456
    const-class v0, Lcom/facebook/tarot/cards/TarotCardVideo;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2771457
    const v0, 0x7f031482

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2771458
    const v0, 0x7f0d2ea7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/media/TarotVideoView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->g:Lcom/facebook/tarot/media/TarotVideoView;

    .line 2771459
    const v0, 0x7f0d2ea2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    .line 2771460
    const v0, 0x7f0d2ea8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->i:Landroid/widget/ImageView;

    .line 2771461
    const v0, 0x7f0d2e94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    .line 2771462
    new-instance v0, LX/K6M;

    invoke-direct {v0, p0}, LX/K6M;-><init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->k:LX/K6M;

    .line 2771463
    new-instance v0, LX/K6O;

    invoke-direct {v0, p0}, LX/K6O;-><init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->j:LX/K6O;

    .line 2771464
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->m:LX/K6L;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771465
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b261c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2771466
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->i:Landroid/widget/ImageView;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2771467
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->e:LX/K5e;

    invoke-virtual {v0, p0}, LX/K5e;->a(Lcom/facebook/tarot/cards/BaseTarotCard;)V

    .line 2771468
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 2771469
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-static {p0, v0}, Lcom/facebook/tarot/cards/TarotCardVideo;->a$redex0(Lcom/facebook/tarot/cards/TarotCardVideo;LX/04g;)V

    .line 2771470
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->g:Lcom/facebook/tarot/media/TarotVideoView;

    invoke-virtual {v0}, LX/2oW;->jj_()V

    .line 2771471
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/tarot/cards/TarotCardVideo;->b(Lcom/facebook/tarot/cards/TarotCardVideo;Z)V

    .line 2771472
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2771495
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->g:Lcom/facebook/tarot/media/TarotVideoView;

    invoke-virtual {v0}, LX/2oW;->b()V

    .line 2771496
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/tarot/cards/TarotCardVideo;->b(Lcom/facebook/tarot/cards/TarotCardVideo;Z)V

    .line 2771497
    return-void
.end method

.method private setAudioIconEnabled(Z)V
    .locals 2

    .prologue
    .line 2771440
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->i:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const v0, 0x7f0200dd

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2771441
    return-void

    .line 2771442
    :cond_0
    const v0, 0x7f0200e5

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2771443
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->i()V

    .line 2771444
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2771445
    if-nez p1, :cond_0

    .line 2771446
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->h()V

    .line 2771447
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2771448
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->i()V

    .line 2771449
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2771450
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardVideo;->h()V

    .line 2771451
    return-void
.end method

.method public getBackgroundVideoComponent()LX/K6O;
    .locals 1

    .prologue
    .line 2771452
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->j:LX/K6O;

    return-object v0
.end method

.method public getDescriptionTextComponent()LX/K5l;
    .locals 1

    .prologue
    .line 2771453
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->k:LX/K6M;

    return-object v0
.end method

.method public getFontComponent()LX/K5m;
    .locals 1

    .prologue
    .line 2771454
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->l:LX/K6N;

    return-object v0
.end method

.method public getInstantArticleComponent()LX/K6S;
    .locals 1

    .prologue
    .line 2771455
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->f:LX/K6S;

    return-object v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x400acdff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771439
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardVideo;->e:LX/K5e;

    invoke-virtual {v1, p1}, LX/K5e;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x63ed250a

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
