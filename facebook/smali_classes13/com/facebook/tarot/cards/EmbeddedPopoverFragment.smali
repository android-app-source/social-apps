.class public Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""


# static fields
.field public static final m:I

.field public static final n:Ljava/lang/String;


# instance fields
.field public o:LX/K5U;

.field private p:LX/8qU;

.field public q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

.field public r:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2770622
    sget-object v0, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v0}, LX/31M;->flag()I

    move-result v0

    sput v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->m:I

    .line 2770623
    const-class v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2770620
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    .line 2770621
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2770617
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->b()LX/K5U;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->b()LX/K5U;

    move-result-object v0

    invoke-interface {v0}, LX/K5U;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2770618
    const/4 v0, 0x1

    .line 2770619
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()LX/K5U;
    .locals 2

    .prologue
    .line 2770616
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0807

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/K5U;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2770615
    const v0, 0x7f030471

    return v0
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 2770624
    iget-object v0, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->p:LX/8qU;

    if-nez v0, :cond_0

    .line 2770625
    new-instance v0, LX/K5h;

    invoke-direct {v0, p0}, LX/K5h;-><init>(Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->p:LX/8qU;

    .line 2770626
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->p:LX/8qU;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x442401bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2770607
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2770608
    iget-object v1, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->o:LX/K5U;

    if-eqz v1, :cond_0

    .line 2770609
    iget-object v1, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->o:LX/K5U;

    const/4 p1, 0x0

    .line 2770610
    if-eqz v1, :cond_0

    .line 2770611
    iput-object v1, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->o:LX/K5U;

    .line 2770612
    iget-object v2, p0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->o:LX/K5U;

    invoke-interface {v2, p0}, LX/K5U;->a(Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;)V

    .line 2770613
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v4, 0x7f0d0807

    check-cast v1, Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v4, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2770614
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x4be2c8bc    # 2.9725048E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2b77f5bb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2770604
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/8tn;

    .line 2770605
    invoke-virtual {v0}, LX/8tn;->d()V

    .line 2770606
    const/16 v2, 0x2b

    const v3, -0x7e6ac193

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 2770603
    sget v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->m:I

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2770601
    sget v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->m:I

    return v0
.end method

.method public final w()D
    .locals 2

    .prologue
    .line 2770602
    const-wide v0, 0x3feccccccccccccdL    # 0.9

    return-wide v0
.end method
