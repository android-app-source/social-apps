.class public Lcom/facebook/tarot/cards/TarotCardDeckFragment;
.super Lcom/facebook/richdocument/RichDocumentFragment;
.source ""

# interfaces
.implements LX/K5f;


# static fields
.field private static final t:I


# instance fields
.field public n:LX/CsO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/K5g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/DelayedListeningExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

.field public s:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

.field private final u:Ljava/lang/Runnable;

.field public v:Z

.field private w:Landroid/view/ViewGroup;

.field private x:LX/0YG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2771125
    const v0, 0x7f0d2e99

    sput v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2771126
    invoke-direct {p0}, Lcom/facebook/richdocument/RichDocumentFragment;-><init>()V

    .line 2771127
    new-instance v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment$RemoveEmbeddedFragmentRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment$RemoveEmbeddedFragmentRunnable;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->u:Ljava/lang/Runnable;

    .line 2771128
    return-void
.end method

.method public static a(LX/K7D;Landroid/os/Bundle;)Lcom/facebook/tarot/cards/TarotCardDeckFragment;
    .locals 4

    .prologue
    .line 2771129
    new-instance v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    invoke-direct {v0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;-><init>()V

    .line 2771130
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2771131
    const-string v1, "tarot_card_deck_id"

    iget-object v3, p0, LX/K7D;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771132
    const-string v1, "tarot_card_deck_article_id"

    iget-object v3, p0, LX/K7D;->c:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771133
    const-string v3, "tarot_card_deck_card_list"

    iget-object v1, p0, LX/K7D;->b:Ljava/util/List;

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2771134
    move-object v1, v2

    .line 2771135
    if-eqz p1, :cond_0

    .line 2771136
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2771137
    :cond_0
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2771138
    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 2771139
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->p:LX/0Tf;

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->u:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->x:LX/0YG;

    .line 2771140
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    invoke-static {p0}, LX/CsO;->a(LX/0QB;)LX/CsO;

    move-result-object v1

    check-cast v1, LX/CsO;

    invoke-static {p0}, LX/K5g;->a(LX/0QB;)LX/K5g;

    move-result-object v2

    check-cast v2, LX/K5g;

    invoke-static {p0}, LX/44j;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, LX/0Tf;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    iput-object v1, p1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->n:LX/CsO;

    iput-object v2, p1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->o:LX/K5g;

    iput-object v3, p1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->p:LX/0Tf;

    iput-object p0, p1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->q:LX/0So;

    return-void
.end method

.method private t()LX/K5U;
    .locals 2

    .prologue
    .line 2771155
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    sget v1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t:I

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    .line 2771156
    if-eqz v0, :cond_0

    .line 2771157
    invoke-virtual {v0}, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->b()LX/K5U;

    move-result-object v0

    .line 2771158
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()J
    .locals 4

    .prologue
    .line 2771141
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->x:LX/0YG;

    if-eqz v0, :cond_0

    .line 2771142
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->x:LX/0YG;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1}, LX/0YG;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 2771143
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->x:LX/0YG;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/0YG;->cancel(Z)Z

    .line 2771144
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static v(Lcom/facebook/tarot/cards/TarotCardDeckFragment;)V
    .locals 2

    .prologue
    .line 2771145
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    sget v1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t:I

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    .line 2771146
    if-eqz v0, :cond_0

    .line 2771147
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2771148
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2771149
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2771150
    :cond_0
    :goto_0
    return v0

    .line 2771151
    :cond_1
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t()LX/K5U;

    move-result-object v1

    .line 2771152
    if-eqz v1, :cond_0

    .line 2771153
    invoke-interface {v1}, LX/K5U;->S_()Z

    move-result v0

    goto :goto_0
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 2771154
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/ChL;
    .locals 1

    .prologue
    .line 2771123
    new-instance v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    invoke-direct {v0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    .line 2771124
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2771121
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2771122
    invoke-static {v0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/CqD;
    .locals 1

    .prologue
    .line 2771068
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->s:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    return-object v0
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 2771069
    invoke-super {p0}, Lcom/facebook/richdocument/RichDocumentFragment;->n()V

    .line 2771070
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    if-eqz v0, :cond_0

    .line 2771071
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    .line 2771072
    iget-object v1, v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    if-eqz v1, :cond_0

    .line 2771073
    iget-object v1, v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-static {v0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->D(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Z

    move-result v2

    invoke-interface {v1, v2}, LX/K5b;->a(Z)V

    .line 2771074
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2771075
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t()LX/K5U;

    move-result-object v0

    .line 2771076
    invoke-interface {v0}, LX/K5U;->ns_()V

    .line 2771077
    :cond_1
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->u()J

    .line 2771078
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->v:Z

    .line 2771079
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 2771080
    invoke-super {p0}, Lcom/facebook/richdocument/RichDocumentFragment;->o()V

    .line 2771081
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    if-eqz v0, :cond_0

    .line 2771082
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r:Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    .line 2771083
    iget-object v1, v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    if-eqz v1, :cond_0

    .line 2771084
    iget-object v1, v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v1}, LX/K5b;->a()V

    .line 2771085
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2771086
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t()LX/K5U;

    move-result-object v0

    .line 2771087
    invoke-interface {v0}, LX/K5U;->nt_()V

    .line 2771088
    const-wide/16 v0, 0x1388

    invoke-direct {p0, v0, v1}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->a(J)V

    .line 2771089
    :cond_1
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2771090
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/richdocument/RichDocumentFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2771091
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x537ecc05

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771092
    invoke-super {p0, p1}, Lcom/facebook/richdocument/RichDocumentFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2771093
    const-class v1, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    invoke-static {v1, p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2771094
    if-eqz p1, :cond_0

    .line 2771095
    const-string v1, "millis_before_embedded_fragment_removal"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2771096
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 2771097
    const-string v1, "last_state_save_time"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2771098
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->q:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 2771099
    cmp-long v1, v4, v2

    if-lez v1, :cond_1

    .line 2771100
    invoke-static {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->v(Lcom/facebook/tarot/cards/TarotCardDeckFragment;)V

    .line 2771101
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->o:LX/K5g;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    sget v3, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t:I

    .line 2771102
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2771103
    invoke-static {v4}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 2771104
    iput-object v2, v1, LX/K5g;->b:LX/0gc;

    .line 2771105
    iput v3, v1, LX/K5g;->c:I

    .line 2771106
    iput-object v4, v1, LX/K5g;->d:Ljava/lang/String;

    .line 2771107
    const v1, -0x11073a7e

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2771108
    :cond_1
    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->a(J)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2771109
    invoke-super {p0, p1}, Lcom/facebook/richdocument/RichDocumentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2771110
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->u()J

    move-result-wide v0

    .line 2771111
    const-string v2, "millis_before_embedded_fragment_removal"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2771112
    const-string v0, "last_state_save_time"

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->q:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2771113
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2771114
    invoke-super {p0, p1, p2}, Lcom/facebook/richdocument/RichDocumentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2771115
    sget v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->t:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->w:Landroid/view/ViewGroup;

    .line 2771116
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2771117
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->w:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setFragmentPager(LX/CqD;)V
    .locals 1

    .prologue
    .line 2771118
    instance-of v0, p1, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2771119
    :goto_0
    return-void

    .line 2771120
    :cond_0
    check-cast p1, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->s:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    goto :goto_0
.end method
