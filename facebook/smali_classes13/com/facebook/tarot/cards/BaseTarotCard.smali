.class public abstract Lcom/facebook/tarot/cards/BaseTarotCard;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/K5b;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

.field public d:Landroid/view/GestureDetector;

.field public e:LX/K5a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2770508
    const-class v0, Lcom/facebook/tarot/cards/BaseTarotCard;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/BaseTarotCard;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2770521
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/BaseTarotCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2770522
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2770519
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/BaseTarotCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2770510
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770511
    const/4 p3, 0x0

    .line 2770512
    const-class v0, Lcom/facebook/tarot/cards/BaseTarotCard;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/BaseTarotCard;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2770513
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/tarot/cards/BaseTarotCard;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, LX/K5Y;

    invoke-direct {p2, p0}, LX/K5Y;-><init>(Lcom/facebook/tarot/cards/BaseTarotCard;)V

    invoke-direct {v0, p1, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/BaseTarotCard;->d:Landroid/view/GestureDetector;

    .line 2770514
    iget-object v0, p0, Lcom/facebook/tarot/cards/BaseTarotCard;->d:Landroid/view/GestureDetector;

    invoke-virtual {v0, p3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 2770515
    invoke-virtual {p0, p3}, Lcom/facebook/tarot/cards/BaseTarotCard;->setClipChildren(Z)V

    .line 2770516
    invoke-virtual {p0, p3}, Lcom/facebook/tarot/cards/BaseTarotCard;->setClipToPadding(Z)V

    .line 2770517
    new-instance v0, LX/K5a;

    invoke-direct {v0, p0}, LX/K5a;-><init>(Lcom/facebook/tarot/cards/BaseTarotCard;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/BaseTarotCard;->e:LX/K5a;

    .line 2770518
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/tarot/cards/BaseTarotCard;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object p0

    check-cast p0, LX/Chv;

    iput-object p0, p1, Lcom/facebook/tarot/cards/BaseTarotCard;->a:LX/Chv;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 2770509
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 2770507
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 2770506
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 2770505
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 2770504
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 2770523
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2770500
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/BaseTarotCard;->getDescriptionTextComponent()LX/K5l;

    move-result-object v0

    .line 2770501
    if-eqz v0, :cond_0

    .line 2770502
    invoke-interface {v0}, LX/K5l;->a()V

    .line 2770503
    :cond_0
    return-void
.end method

.method public getBackgroundImageComponent()LX/K5k;
    .locals 1

    .prologue
    .line 2770499
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBackgroundVideoComponent()LX/K6O;
    .locals 1

    .prologue
    .line 2770498
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoverTextComponent()LX/K5p;
    .locals 1

    .prologue
    .line 2770497
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDescriptionTextComponent()LX/K5l;
    .locals 1

    .prologue
    .line 2770491
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFeedbackComponent()LX/K5Z;
    .locals 1

    .prologue
    .line 2770496
    iget-object v0, p0, Lcom/facebook/tarot/cards/BaseTarotCard;->e:LX/K5a;

    return-object v0
.end method

.method public getFontComponent()LX/K5m;
    .locals 1

    .prologue
    .line 2770495
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInstantArticleComponent()LX/K6S;
    .locals 1

    .prologue
    .line 2770494
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShareBarComponent()LX/K5n;
    .locals 1

    .prologue
    .line 2770493
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSlideshowComponent()LX/K5r;
    .locals 1

    .prologue
    .line 2770492
    const/4 v0, 0x0

    return-object v0
.end method
