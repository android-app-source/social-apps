.class public Lcom/facebook/tarot/cards/TarotCardImage;
.super Lcom/facebook/tarot/cards/BaseTarotCard;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/K5b;
.implements LX/K5k;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/K5e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/K6S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

.field private h:LX/K6H;

.field private final i:LX/K6I;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2771390
    const-class v0, Lcom/facebook/tarot/cards/TarotCardImage;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/TarotCardImage;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2771391
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/TarotCardImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771392
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2771394
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/TarotCardImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771395
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2771375
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/tarot/cards/BaseTarotCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771376
    new-instance v0, LX/K6I;

    invoke-direct {v0, p0}, LX/K6I;-><init>(Lcom/facebook/tarot/cards/TarotCardImage;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->i:LX/K6I;

    .line 2771377
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardImage;->g()V

    .line 2771378
    return-void
.end method

.method private static a(Lcom/facebook/tarot/cards/TarotCardImage;LX/K5e;LX/K6S;)V
    .locals 0

    .prologue
    .line 2771393
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardImage;->c:LX/K5e;

    iput-object p2, p0, Lcom/facebook/tarot/cards/TarotCardImage;->d:LX/K6S;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/TarotCardImage;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/tarot/cards/TarotCardImage;

    invoke-static {v1}, LX/K5e;->b(LX/0QB;)LX/K5e;

    move-result-object v0

    check-cast v0, LX/K5e;

    invoke-static {v1}, LX/K6S;->b(LX/0QB;)LX/K6S;

    move-result-object v1

    check-cast v1, LX/K6S;

    invoke-static {p0, v0, v1}, Lcom/facebook/tarot/cards/TarotCardImage;->a(Lcom/facebook/tarot/cards/TarotCardImage;LX/K5e;LX/K6S;)V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2771382
    const-class v0, Lcom/facebook/tarot/cards/TarotCardImage;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/TarotCardImage;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2771383
    const v0, 0x7f03147f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2771384
    const v0, 0x7f0d2e9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2771385
    const v0, 0x7f0d2ea2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    .line 2771386
    const v0, 0x7f0d2e94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    .line 2771387
    new-instance v0, LX/K6H;

    invoke-direct {v0, p0}, LX/K6H;-><init>(Lcom/facebook/tarot/cards/TarotCardImage;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->h:LX/K6H;

    .line 2771388
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->c:LX/K5e;

    invoke-virtual {v0, p0}, LX/K5e;->a(Lcom/facebook/tarot/cards/BaseTarotCard;)V

    .line 2771389
    return-void
.end method


# virtual methods
.method public getBackgroundImageComponent()LX/K5k;
    .locals 0

    .prologue
    .line 2771381
    return-object p0
.end method

.method public getDescriptionTextComponent()LX/K5l;
    .locals 1

    .prologue
    .line 2771380
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->h:LX/K6H;

    return-object v0
.end method

.method public getFontComponent()LX/K5m;
    .locals 1

    .prologue
    .line 2771379
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->i:LX/K6I;

    return-object v0
.end method

.method public getInstantArticleComponent()LX/K6S;
    .locals 1

    .prologue
    .line 2771374
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->d:LX/K6S;

    return-object v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x264049a1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771373
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardImage;->c:LX/K5e;

    invoke-virtual {v1, p1}, LX/K5e;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x4ce60755

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setBackgroundImage(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2771371
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardImage;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/tarot/cards/TarotCardImage;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2771372
    return-void
.end method
