.class public Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;
.super Lcom/facebook/instantarticles/InstantArticlesFragment;
.source ""

# interfaces
.implements LX/K5U;


# instance fields
.field public u:LX/K87;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

.field private w:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2771505
    invoke-direct {p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;-><init>()V

    .line 2771506
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    invoke-static {p0}, LX/K87;->a(LX/0QB;)LX/K87;

    move-result-object p0

    check-cast p0, LX/K87;

    iput-object p0, p1, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->u:LX/K87;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2771543
    invoke-super {p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->S_()Z

    .line 2771544
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->v:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    if-eqz v0, :cond_0

    .line 2771545
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->v:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/popover/PopoverFragment;->l()V

    .line 2771546
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;)V
    .locals 1

    .prologue
    .line 2771539
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->v:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    .line 2771540
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->v:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    .line 2771541
    iput-object p0, v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->q:Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    .line 2771542
    return-void
.end method

.method public final a(LX/31M;)Z
    .locals 3

    .prologue
    .line 2771533
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->S()Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2771534
    if-eqz v0, :cond_0

    .line 2771535
    sget-object v1, LX/K6R;->a:[I

    invoke-virtual {p1}, LX/31M;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2771536
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2771537
    :pswitch_0
    invoke-static {v0}, LX/K5j;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)Z

    move-result v0

    goto :goto_0

    .line 2771538
    :pswitch_1
    invoke-static {v0}, LX/K5j;->b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final k()LX/ChL;
    .locals 1

    .prologue
    .line 2771531
    new-instance v0, LX/K6P;

    invoke-direct {v0}, LX/K6P;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    .line 2771532
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    return-object v0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 2771528
    invoke-super {p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->n()V

    .line 2771529
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->u:LX/K87;

    new-instance v1, LX/K7x;

    invoke-direct {v1}, LX/K7x;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2771530
    return-void
.end method

.method public final ns_()V
    .locals 0

    .prologue
    .line 2771526
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->n()V

    .line 2771527
    return-void
.end method

.method public final nt_()V
    .locals 0

    .prologue
    .line 2771524
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->o()V

    .line 2771525
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 2771519
    invoke-super {p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->o()V

    .line 2771520
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->u:LX/K87;

    new-instance v1, LX/K7w;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->v:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    .line 2771521
    iget-object p0, v2, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->r:Ljava/lang/String;

    move-object v2, p0

    .line 2771522
    invoke-direct {v1, v2}, LX/K7w;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2771523
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3d745aa4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771515
    invoke-super {p0, p1}, Lcom/facebook/instantarticles/InstantArticlesFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2771516
    const-class v1, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    invoke-static {v1, p0}, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2771517
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->u:LX/K87;

    new-instance v2, LX/K7u;

    invoke-direct {v2}, LX/K7u;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2771518
    const/16 v1, 0x2b

    const v2, -0x6de9daef

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x91c14c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771511
    invoke-super {p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->onDestroyView()V

    .line 2771512
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->w:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771513
    iput-object v2, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->w:Landroid/view/View;

    .line 2771514
    const/16 v1, 0x2b

    const v2, -0x7823a54c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2771507
    invoke-super {p0, p1, p2}, Lcom/facebook/instantarticles/InstantArticlesFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2771508
    const v0, 0x7f0d2eb2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->w:Landroid/view/View;

    .line 2771509
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;->w:Landroid/view/View;

    new-instance v1, LX/K6Q;

    invoke-direct {v1, p0}, LX/K6Q;-><init>(Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771510
    return-void
.end method
