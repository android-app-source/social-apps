.class public Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;
.implements LX/ChL;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final A:LX/ChV;

.field public final B:LX/ChN;

.field public final C:LX/ChP;

.field public final D:LX/K5y;

.field public final E:LX/K60;

.field public final F:LX/K62;

.field public a:Landroid/view/View;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K7m;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K87;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K6J;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K6a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K8M;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K7P;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final m:LX/1B1;

.field private n:Landroid/content/Context;

.field private o:Landroid/view/View;

.field public p:Landroid/os/Bundle;

.field public q:I

.field public r:Z

.field public s:Z

.field public t:LX/ChK;

.field public u:LX/ChI;

.field private v:LX/FAd;

.field public w:LX/K5b;

.field public x:Lcom/facebook/widget/CustomLinearLayout;

.field private y:Lcom/facebook/richdocument/RichDocumentFragment;

.field public final z:LX/ChT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2770938
    const-class v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2770946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770947
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    .line 2770948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->r:Z

    .line 2770949
    new-instance v0, LX/K5u;

    invoke-direct {v0, p0}, LX/K5u;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->z:LX/ChT;

    .line 2770950
    new-instance v0, LX/K5v;

    invoke-direct {v0, p0}, LX/K5v;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->A:LX/ChV;

    .line 2770951
    new-instance v0, LX/K5w;

    invoke-direct {v0, p0}, LX/K5w;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->B:LX/ChN;

    .line 2770952
    new-instance v0, LX/K5x;

    invoke-direct {v0, p0}, LX/K5x;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->C:LX/ChP;

    .line 2770953
    new-instance v0, LX/K5z;

    invoke-direct {v0, p0}, LX/K5z;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->D:LX/K5y;

    .line 2770954
    new-instance v0, LX/K61;

    invoke-direct {v0, p0}, LX/K61;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->E:LX/K60;

    .line 2770955
    new-instance v0, LX/K63;

    invoke-direct {v0, p0}, LX/K63;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->F:LX/K62;

    .line 2770956
    return-void
.end method

.method public static B(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2770957
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->p:Landroid/os/Bundle;

    move-object v0, v0

    .line 2770958
    if-nez v0, :cond_0

    .line 2770959
    const/4 v0, 0x0

    .line 2770960
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "tarot_digest_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static C(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V
    .locals 9

    .prologue
    .line 2770961
    invoke-static {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->D(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2770962
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v0}, LX/K5b;->getInstantArticleComponent()LX/K6S;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2770963
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v0}, LX/K5b;->getInstantArticleComponent()LX/K6S;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2770964
    if-eqz v1, :cond_1

    iget-object v2, v0, LX/K6S;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2770965
    iget-object v2, v0, LX/K6S;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K5L;

    iget-object v3, v0, LX/K6S;->b:Ljava/lang/String;

    iget-object v4, v0, LX/K6S;->c:Ljava/lang/String;

    .line 2770966
    iget-object v5, v2, LX/K5L;->b:LX/K8s;

    .line 2770967
    iget-object v6, v5, LX/K8s;->d:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v5, v6

    .line 2770968
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2770969
    const-string v7, "extra_instant_articles_id"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770970
    const-string v7, "extra_instant_articles_referrer"

    const-string v8, "tarot_digest"

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770971
    const-string v7, "extra_instant_articles_can_log_open_link_on_settle"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2770972
    const-string v7, "extra_instant_articles_canonical_url"

    invoke-virtual {v6, v7, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770973
    if-eqz v5, :cond_0

    .line 2770974
    const-string v7, "tarot_session_id"

    invoke-virtual {v6, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770975
    :cond_0
    move-object v5, v6

    .line 2770976
    new-instance v6, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;

    invoke-direct {v6}, Lcom/facebook/tarot/cards/TarotEmbeddedInstantArticleFragment;-><init>()V

    .line 2770977
    invoke-virtual {v6, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2770978
    iget-object v5, v2, LX/K5L;->b:LX/K8s;

    const/4 p0, 0x0

    .line 2770979
    iget-object v7, v5, LX/K8s;->d:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2770980
    if-nez v7, :cond_2

    .line 2770981
    :goto_0
    iget-object v5, v2, LX/K5L;->c:LX/K5g;

    invoke-virtual {v5, v6}, LX/K5g;->a(LX/K5U;)V

    .line 2770982
    :cond_1
    return-void

    .line 2770983
    :cond_2
    iget-object v8, v5, LX/K8s;->c:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/K79;

    .line 2770984
    if-eqz v8, :cond_5

    iget-object v0, v8, LX/K79;->h:Ljava/lang/String;

    :goto_1
    if-eqz v8, :cond_6

    iget-object v8, v8, LX/K79;->i:Ljava/lang/String;

    .line 2770985
    :goto_2
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2770986
    const-string v1, "digest_id"

    if-nez v0, :cond_3

    const-string v0, ""

    :cond_3
    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770987
    const-string v1, "page_id"

    if-nez v8, :cond_4

    const-string v8, ""

    :cond_4
    invoke-interface {p0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770988
    const-string v1, "card_type"

    const-string v4, "instant_article"

    invoke-interface {p0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770989
    const-string v1, "attachment_id"

    invoke-interface {p0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770990
    const-string v1, "tarot_open_attachment"

    invoke-static {v5, v1, v7, p0}, LX/K8s;->a(LX/K8s;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2770991
    goto :goto_0

    :cond_5
    move-object v0, p0

    goto :goto_1

    :cond_6
    move-object v8, p0

    goto :goto_2
.end method

.method public static D(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Z
    .locals 1

    .prologue
    .line 2770992
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->y:Lcom/facebook/richdocument/RichDocumentFragment;

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->r()Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;I)Lcom/facebook/tarot/data/BaseTarotCardData;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2770993
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->p:Landroid/os/Bundle;

    move-object v1, v1

    .line 2770994
    if-nez v1, :cond_1

    .line 2770995
    :cond_0
    :goto_0
    return-object v0

    .line 2770996
    :cond_1
    const-string v2, "tarot_card_deck_card_list"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2770997
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 2770998
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/BaseTarotCardData;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;LX/K5m;)V
    .locals 10

    .prologue
    .line 2770999
    invoke-static {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->B(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Ljava/lang/String;

    move-result-object v1

    .line 2771000
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K8M;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2771001
    if-eqz v1, :cond_0

    iget-object v2, v0, LX/K8M;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2771002
    :cond_0
    :goto_0
    move-object v0, v5

    .line 2771003
    if-eqz v0, :cond_1

    .line 2771004
    iget-object v1, v0, LX/K8K;->a:Landroid/graphics/Typeface;

    invoke-interface {p1, v1}, LX/K5m;->a(Landroid/graphics/Typeface;)V

    .line 2771005
    iget-object v0, v0, LX/K8K;->b:Landroid/graphics/Typeface;

    invoke-interface {p1, v0}, LX/K5m;->b(Landroid/graphics/Typeface;)V

    .line 2771006
    :cond_1
    return-void

    .line 2771007
    :cond_2
    iget-object v2, v0, LX/K8M;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8L;

    iget-object v6, v2, LX/K8L;->a:LX/8GE;

    .line 2771008
    iget-object v2, v0, LX/K8M;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8L;

    iget-object v7, v2, LX/K8L;->b:LX/8GE;

    .line 2771009
    iget-object v2, v0, LX/K8M;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8L;

    iget-object v8, v2, LX/K8L;->c:Ljava/lang/String;

    .line 2771010
    iget-object v2, v0, LX/K8M;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8L;

    iget-object v9, v2, LX/K8L;->d:Ljava/lang/String;

    .line 2771011
    if-eqz v6, :cond_4

    if-eqz v8, :cond_4

    move v2, v4

    .line 2771012
    :goto_1
    if-eqz v7, :cond_3

    if-eqz v9, :cond_3

    move v3, v4

    .line 2771013
    :cond_3
    new-instance p0, LX/0Pz;

    invoke-direct {p0}, LX/0Pz;-><init>()V

    .line 2771014
    invoke-static {p0, v6}, LX/K8M;->a(LX/0Pz;LX/8GE;)V

    .line 2771015
    invoke-static {p0, v7}, LX/K8M;->a(LX/0Pz;LX/8GE;)V

    .line 2771016
    iget-object v6, v0, LX/K8M;->a:LX/8Yg;

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v6, v7, p0, v4}, LX/8Yg;->a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;

    move-result-object v7

    .line 2771017
    if-eqz v7, :cond_0

    .line 2771018
    new-instance v4, LX/K8K;

    if-eqz v2, :cond_5

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Typeface;

    move-object v6, v2

    :goto_2
    if-eqz v3, :cond_6

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Typeface;

    :goto_3
    invoke-direct {v4, v6, v2}, LX/K8K;-><init>(Landroid/graphics/Typeface;Landroid/graphics/Typeface;)V

    move-object v5, v4

    goto :goto_0

    :cond_4
    move v2, v3

    .line 2771019
    goto :goto_1

    :cond_5
    move-object v6, v5

    .line 2771020
    goto :goto_2

    :cond_6
    move-object v2, v5

    goto :goto_3
.end method

.method public static a(Ljava/lang/Class;LX/02k;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v1, p1

    check-cast v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    const/16 v2, 0x31e0

    invoke-static {v11, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x35ee

    invoke-static {v11, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x35ef

    invoke-static {v11, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x35e9

    invoke-static {v11, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x35eb

    invoke-static {v11, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x35f2

    invoke-static {v11, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2e4

    invoke-static {v11, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ba

    invoke-static {v11, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x35ed

    invoke-static {v11, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0x2524

    invoke-static {v11, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    iput-object v2, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    iput-object v3, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    iput-object v4, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->d:LX/0Ot;

    iput-object v5, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->e:LX/0Ot;

    iput-object v6, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->f:LX/0Ot;

    iput-object v7, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->g:LX/0Ot;

    iput-object v8, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->h:LX/0Ot;

    iput-object v9, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->i:LX/0Ot;

    iput-object v10, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->j:LX/0Ot;

    iput-object v11, v1, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->k:LX/0Ot;

    return-void
.end method

.method public static d(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2771060
    const-string v0, "tarot_card_deck_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static z(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Landroid/app/Activity;
    .locals 2

    .prologue
    .line 2771021
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2771022
    const/4 v3, 0x0

    .line 2771023
    const-class v0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 2771024
    const v0, 0x7f031478

    move v0, v0

    .line 2771025
    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2771026
    const v0, 0x7f0d2e98

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->x:Lcom/facebook/widget/CustomLinearLayout;

    .line 2771027
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->x:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {p0, v3}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->a(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;I)Lcom/facebook/tarot/data/BaseTarotCardData;

    move-result-object v3

    const/4 p2, 0x0

    .line 2771028
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p3

    .line 2771029
    instance-of p1, v3, Lcom/facebook/tarot/data/TarotCardImageData;

    if-eqz p1, :cond_0

    .line 2771030
    const p1, 0x7f03147e

    invoke-virtual {p3, p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    check-cast p3, LX/K5b;

    .line 2771031
    :goto_0
    move-object v0, p3

    .line 2771032
    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    .line 2771033
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v0}, LX/K5b;->f()V

    .line 2771034
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->x:Lcom/facebook/widget/CustomLinearLayout;

    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 2771035
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->z:LX/ChT;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2771036
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->A:LX/ChV;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2771037
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->B:LX/ChN;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2771038
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->C:LX/ChP;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2771039
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v2, v0}, LX/1B1;->a(LX/0b4;)V

    .line 2771040
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v2, LX/CiV;

    sget-object v3, LX/CiU;->ON_CREATE:LX/CiU;

    invoke-direct {v2, v3}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2771041
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->D:LX/K5y;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2771042
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->E:LX/K60;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2771043
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->F:LX/K62;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2771044
    move-object v0, v1

    .line 2771045
    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->o:Landroid/view/View;

    .line 2771046
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->o:Landroid/view/View;

    return-object v0

    .line 2771047
    :cond_0
    instance-of p1, v3, Lcom/facebook/tarot/data/TarotCardVideoData;

    if-eqz p1, :cond_1

    .line 2771048
    const p1, 0x7f031481

    invoke-virtual {p3, p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    check-cast p3, LX/K5b;

    goto/16 :goto_0

    .line 2771049
    :cond_1
    instance-of p1, v3, Lcom/facebook/tarot/data/TarotCardCoverData;

    if-eqz p1, :cond_2

    .line 2771050
    const p1, 0x7f031475

    invoke-virtual {p3, p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    check-cast p3, LX/K5b;

    goto/16 :goto_0

    .line 2771051
    :cond_2
    instance-of p1, v3, Lcom/facebook/tarot/data/TarotCardEndCardData;

    if-eqz p1, :cond_3

    .line 2771052
    const p1, 0x7f031479

    invoke-virtual {p3, p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    check-cast p3, LX/K5b;

    goto/16 :goto_0

    .line 2771053
    :cond_3
    const/4 p3, 0x0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2771054
    const-string v0, "tarot_story"

    return-object v0
.end method

.method public final a(LX/ChI;)V
    .locals 0

    .prologue
    .line 2771055
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->u:LX/ChI;

    .line 2771056
    return-void
.end method

.method public final a(LX/ChK;)V
    .locals 0

    .prologue
    .line 2771057
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->t:LX/ChK;

    .line 2771058
    return-void
.end method

.method public final a(LX/ChZ;)V
    .locals 0

    .prologue
    .line 2771059
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2770939
    invoke-static {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->z(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Landroid/app/Activity;

    move-result-object v0

    .line 2770940
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/richdocument/BaseRichDocumentActivity;

    if-nez v1, :cond_0

    .line 2770941
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->q:I

    .line 2770942
    iput-boolean v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->r:Z

    .line 2770943
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2770944
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2770945
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770809
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->a:Landroid/view/View;

    .line 2770810
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->p:Landroid/os/Bundle;

    move-object v0, v0

    .line 2770811
    if-nez v0, :cond_1

    .line 2770812
    :cond_0
    :goto_0
    return-void

    .line 2770813
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->a(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;I)Lcom/facebook/tarot/data/BaseTarotCardData;

    move-result-object v1

    .line 2770814
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    const/4 v5, 0x0

    .line 2770815
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->b()Lcom/facebook/tarot/data/DescriptionData;

    move-result-object v2

    .line 2770816
    invoke-interface {v0}, LX/K5b;->getDescriptionTextComponent()LX/K5l;

    move-result-object v3

    .line 2770817
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    .line 2770818
    iget-object v4, v2, Lcom/facebook/tarot/data/DescriptionData;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2770819
    invoke-interface {v3, v4}, LX/K5l;->a(Ljava/lang/String;)V

    .line 2770820
    iget-object v4, v2, Lcom/facebook/tarot/data/DescriptionData;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2770821
    invoke-interface {v3, v4}, LX/K5l;->b(Ljava/lang/String;)V

    .line 2770822
    iget v4, v2, Lcom/facebook/tarot/data/DescriptionData;->c:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    .line 2770823
    iget v4, v2, Lcom/facebook/tarot/data/DescriptionData;->c:F

    invoke-interface {v3, v4}, LX/K5l;->a(F)V

    .line 2770824
    :cond_2
    iget v4, v2, Lcom/facebook/tarot/data/DescriptionData;->d:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    .line 2770825
    iget v2, v2, Lcom/facebook/tarot/data/DescriptionData;->d:F

    invoke-interface {v3, v2}, LX/K5l;->b(F)V

    .line 2770826
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->d()Lcom/facebook/tarot/data/BackgroundImageData;

    move-result-object v2

    .line 2770827
    invoke-interface {v0}, LX/K5b;->getBackgroundImageComponent()LX/K5k;

    move-result-object v3

    .line 2770828
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 2770829
    iget-object v4, v2, Lcom/facebook/tarot/data/BackgroundImageData;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2770830
    if-eqz v4, :cond_4

    .line 2770831
    iget-object v4, v2, Lcom/facebook/tarot/data/BackgroundImageData;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2770832
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v3, v2}, LX/K5k;->setBackgroundImage(Landroid/net/Uri;)V

    .line 2770833
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->e()Lcom/facebook/tarot/data/VideoData;

    move-result-object v2

    .line 2770834
    invoke-interface {v0}, LX/K5b;->getBackgroundVideoComponent()LX/K6O;

    move-result-object v3

    .line 2770835
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    .line 2770836
    invoke-virtual {v3, v2}, LX/K6O;->a(Lcom/facebook/tarot/data/VideoData;)V

    .line 2770837
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->c()Lcom/facebook/tarot/data/FeedbackData;

    move-result-object v2

    .line 2770838
    invoke-interface {v0}, LX/K5b;->getFeedbackComponent()LX/K5Z;

    move-result-object v3

    .line 2770839
    if-eqz v2, :cond_6

    if-eqz v3, :cond_6

    .line 2770840
    iget-object v4, v2, Lcom/facebook/tarot/data/FeedbackData;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v4, v4

    .line 2770841
    if-eqz v4, :cond_6

    .line 2770842
    new-instance v4, LX/ClW;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2770843
    iget-object v6, v2, Lcom/facebook/tarot/data/FeedbackData;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v6, v6

    .line 2770844
    sget-object v7, LX/ClQ;->LEFT:LX/ClQ;

    invoke-direct {v4, v5, v6, v7}, LX/ClW;-><init>(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/ClQ;)V

    const/4 v6, 0x0

    .line 2770845
    iget-object v5, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->p:Landroid/os/Bundle;

    move-object v5, v5

    .line 2770846
    if-nez v5, :cond_c

    move-object v5, v6

    .line 2770847
    :goto_1
    move-object v5, v5

    .line 2770848
    invoke-static {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->z(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)Landroid/app/Activity;

    move-result-object v6

    .line 2770849
    iget-object v7, v2, Lcom/facebook/tarot/data/FeedbackData;->c:Ljava/lang/String;

    if-nez v7, :cond_e

    .line 2770850
    const/4 v7, 0x0

    .line 2770851
    :goto_2
    move-object v2, v7

    .line 2770852
    invoke-interface {v3, v4, v5, v2}, LX/K5Z;->a(LX/ClW;LX/162;LX/CnN;)V

    .line 2770853
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->g()Lcom/facebook/tarot/data/CoverTextData;

    move-result-object v3

    .line 2770854
    invoke-interface {v0}, LX/K5b;->getCoverTextComponent()LX/K5p;

    move-result-object v4

    .line 2770855
    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    .line 2770856
    invoke-virtual {v4}, LX/K5p;->a()V

    .line 2770857
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11S;

    sget-object v5, LX/1lB;->MONTH_DAY_LONG_STYLE:LX/1lB;

    .line 2770858
    iget-wide v10, v3, Lcom/facebook/tarot/data/CoverTextData;->d:J

    move-wide v6, v10

    .line 2770859
    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-interface {v2, v5, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/K5p;->b(Ljava/lang/String;)V

    .line 2770860
    iget-object v2, v3, Lcom/facebook/tarot/data/CoverTextData;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2770861
    invoke-virtual {v4, v2}, LX/K5p;->a(Ljava/lang/String;)V

    .line 2770862
    iget-object v2, v3, Lcom/facebook/tarot/data/CoverTextData;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2770863
    iget v5, v3, Lcom/facebook/tarot/data/CoverTextData;->b:I

    move v5, v5

    .line 2770864
    iget v6, v3, Lcom/facebook/tarot/data/CoverTextData;->c:I

    move v3, v6

    .line 2770865
    invoke-virtual {v4, v2, v5, v3}, LX/K5p;->a(Ljava/lang/String;II)V

    .line 2770866
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->f()Lcom/facebook/tarot/data/SlideshowData;

    move-result-object v2

    .line 2770867
    invoke-interface {v0}, LX/K5b;->getSlideshowComponent()LX/K5r;

    move-result-object v3

    .line 2770868
    if-eqz v3, :cond_8

    if-eqz v2, :cond_8

    .line 2770869
    invoke-virtual {v3}, LX/K5r;->a()V

    .line 2770870
    iget-object v4, v2, Lcom/facebook/tarot/data/SlideshowData;->a:Ljava/util/List;

    move-object v4, v4

    .line 2770871
    invoke-virtual {v3, v4}, LX/K5r;->a(Ljava/util/Collection;)V

    .line 2770872
    iget-object v4, v2, Lcom/facebook/tarot/data/SlideshowData;->b:Ljava/util/List;

    move-object v2, v4

    .line 2770873
    invoke-virtual {v3, v2}, LX/K5r;->b(Ljava/util/Collection;)V

    .line 2770874
    invoke-virtual {v3}, LX/K5r;->b()V

    .line 2770875
    :cond_8
    invoke-interface {v0}, LX/K5b;->getFontComponent()LX/K5m;

    move-result-object v2

    .line 2770876
    if-eqz v2, :cond_9

    .line 2770877
    invoke-static {p0, v2}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->a(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;LX/K5m;)V

    .line 2770878
    :cond_9
    invoke-interface {v0}, LX/K5b;->getShareBarComponent()LX/K5n;

    move-result-object v2

    .line 2770879
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->c()Lcom/facebook/tarot/data/FeedbackData;

    .line 2770880
    if-eqz v2, :cond_a

    if-eqz v2, :cond_a

    .line 2770881
    :cond_a
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->h()Ljava/lang/String;

    move-result-object v3

    .line 2770882
    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->i()Ljava/lang/String;

    move-result-object v2

    .line 2770883
    invoke-interface {v0}, LX/K5b;->getInstantArticleComponent()LX/K6S;

    move-result-object v4

    .line 2770884
    if-eqz v3, :cond_b

    if-eqz v4, :cond_b

    .line 2770885
    invoke-virtual {v4, v3, v2}, LX/K6S;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770886
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CH7;

    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, LX/CH2;->TAROT_CARD:LX/CH2;

    sget-object v6, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v3, v5, v6}, LX/CH7;->a(Landroid/content/Context;Ljava/lang/String;LX/CH2;Lcom/facebook/common/callercontext/CallerContext;)LX/CGw;

    .line 2770887
    :cond_b
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    instance-of v0, v0, LX/K5c;

    if-eqz v0, :cond_0

    .line 2770888
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    check-cast v0, LX/K5c;

    invoke-virtual {v1}, Lcom/facebook/tarot/data/BaseTarotCardData;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, LX/K5c;->a(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2770889
    :cond_c
    const-string v7, "tracking_codes"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2770890
    iget-object v5, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->i:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-static {v5, v7}, LX/K5I;->a(LX/0lC;Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 2770891
    if-nez v5, :cond_d

    move-object v5, v6

    .line 2770892
    goto/16 :goto_1

    .line 2770893
    :cond_d
    new-instance v6, LX/162;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/162;-><init>(LX/0mC;)V

    invoke-virtual {v6, v5}, LX/162;->a(LX/0lF;)LX/162;

    move-result-object v5

    goto/16 :goto_1

    :cond_e
    iget-object v7, v2, Lcom/facebook/tarot/data/FeedbackData;->c:Ljava/lang/String;

    iget v8, v2, Lcom/facebook/tarot/data/FeedbackData;->d:I

    iget v9, v2, Lcom/facebook/tarot/data/FeedbackData;->e:I

    iget-object v10, v2, Lcom/facebook/tarot/data/FeedbackData;->f:Ljava/lang/String;

    .line 2770894
    new-instance v2, LX/CnM;

    invoke-direct {v2, v7, v8}, LX/CnM;-><init>(Ljava/lang/String;I)V

    .line 2770895
    iput v9, v2, LX/CnM;->d:I

    .line 2770896
    if-eqz v6, :cond_f

    .line 2770897
    const-class v11, LX/0ew;

    invoke-static {v6, v11}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/0ew;

    .line 2770898
    if-eqz v11, :cond_f

    .line 2770899
    invoke-interface {v11}, LX/0ew;->iC_()LX/0gc;

    move-result-object v11

    invoke-virtual {v11, v10}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v11

    .line 2770900
    if-eqz v11, :cond_f

    .line 2770901
    iput-object v11, v2, LX/CnM;->c:Landroid/support/v4/app/Fragment;

    .line 2770902
    :cond_f
    invoke-virtual {v2}, LX/CnM;->a()LX/CnN;

    move-result-object v11

    move-object v7, v11

    .line 2770903
    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/richdocument/RichDocumentFragment;)V
    .locals 0

    .prologue
    .line 2770904
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->y:Lcom/facebook/richdocument/RichDocumentFragment;

    .line 2770905
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2770906
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->p:Landroid/os/Bundle;

    move-object v0, v0

    .line 2770907
    invoke-static {v0}, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2770908
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2770909
    const-string p0, "tarot_card_deck_id"

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770910
    move-object v0, v1

    .line 2770911
    return-object v0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2770912
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->n:Landroid/content/Context;

    .line 2770913
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2770914
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_SAVE_INSTANCE_STATE:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2770915
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2770916
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->p:Landroid/os/Bundle;

    .line 2770917
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2770918
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2770919
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->s:Z

    .line 2770920
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    if-eqz v0, :cond_0

    .line 2770921
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v0}, LX/K5b;->e()V

    .line 2770922
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2770806
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    if-eqz v0, :cond_0

    .line 2770807
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->w:LX/K5b;

    invoke-interface {v0}, LX/K5b;->d()V

    .line 2770808
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2770923
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_DESTROY:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2770924
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->m:LX/1B1;

    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v1, v0}, LX/1B1;->b(LX/0b4;)V

    .line 2770925
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7m;

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->D:LX/K5y;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2770926
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7m;

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->E:LX/K60;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2770927
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7m;

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->F:LX/K62;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2770928
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->v:LX/FAd;

    if-eqz v0, :cond_0

    .line 2770929
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->v:LX/FAd;

    invoke-virtual {v0}, LX/FAd;->a()V

    .line 2770930
    :cond_0
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2770931
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->n:Landroid/content/Context;

    return-object v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 2770932
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2770933
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_LOW_MEMORY:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2770934
    return-void
.end method

.method public final j()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 2770935
    new-instance v0, LX/K64;

    invoke-direct {v0, p0}, LX/K64;-><init>(Lcom/facebook/tarot/cards/TarotCardDeckDelegateImpl;)V

    return-object v0
.end method

.method public final k()LX/ChZ;
    .locals 1

    .prologue
    .line 2770936
    const/4 v0, 0x0

    return-object v0
.end method

.method public final mB_()V
    .locals 0

    .prologue
    .line 2770937
    return-void
.end method
