.class public Lcom/facebook/tarot/cards/TarotCardEndCard;
.super Lcom/facebook/tarot/cards/BaseTarotCard;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/K5c;
.implements LX/K5b;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/K87;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/K8f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2dj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/K91;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/K5g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/K7A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final m:LX/K6C;

.field private final n:LX/K6D;

.field private final o:LX/K6G;

.field private final p:LX/K6E;

.field private final q:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private s:Lcom/facebook/fbui/glyph/GlyphView;

.field private t:Lcom/facebook/fbui/glyph/GlyphView;

.field private u:Landroid/view/ViewGroup;

.field public v:LX/K5i;

.field public w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:LX/K6F;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2771349
    const-class v0, Lcom/facebook/tarot/cards/TarotCardEndCard;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/TarotCardEndCard;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2771347
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/TarotCardEndCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2771348
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2771345
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/TarotCardEndCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771346
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2771337
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/tarot/cards/BaseTarotCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2771338
    new-instance v0, LX/K6C;

    invoke-direct {v0, p0}, LX/K6C;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->m:LX/K6C;

    .line 2771339
    new-instance v0, LX/K6D;

    invoke-direct {v0, p0}, LX/K6D;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->n:LX/K6D;

    .line 2771340
    new-instance v0, LX/K6G;

    invoke-direct {v0, p0}, LX/K6G;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->o:LX/K6G;

    .line 2771341
    new-instance v0, LX/K6E;

    invoke-direct {v0, p0}, LX/K6E;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->p:LX/K6E;

    .line 2771342
    new-instance v0, LX/K65;

    invoke-direct {v0, p0}, LX/K65;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->q:LX/0TF;

    .line 2771343
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->h()V

    .line 2771344
    return-void
.end method

.method private static a(Lcom/facebook/tarot/cards/TarotCardEndCard;LX/K87;LX/1Ad;LX/K8f;LX/2dj;LX/K91;LX/K5g;LX/0iA;LX/K7A;LX/1My;)V
    .locals 0

    .prologue
    .line 2771336
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->c:LX/K87;

    iput-object p2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->d:LX/1Ad;

    iput-object p3, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->e:LX/K8f;

    iput-object p4, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->f:LX/2dj;

    iput-object p5, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iput-object p6, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->h:LX/K5g;

    iput-object p7, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->i:LX/0iA;

    iput-object p8, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->j:LX/K7A;

    iput-object p9, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->k:LX/1My;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardEndCard;

    invoke-static {v9}, LX/K87;->a(LX/0QB;)LX/K87;

    move-result-object v1

    check-cast v1, LX/K87;

    invoke-static {v9}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v9}, LX/K8f;->b(LX/0QB;)LX/K8f;

    move-result-object v3

    check-cast v3, LX/K8f;

    invoke-static {v9}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-static {v9}, LX/K91;->a(LX/0QB;)LX/K91;

    move-result-object v5

    check-cast v5, LX/K91;

    invoke-static {v9}, LX/K5g;->a(LX/0QB;)LX/K5g;

    move-result-object v6

    check-cast v6, LX/K5g;

    invoke-static {v9}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    invoke-static {v9}, LX/K7A;->a(LX/0QB;)LX/K7A;

    move-result-object v8

    check-cast v8, LX/K7A;

    invoke-static {v9}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v9

    check-cast v9, LX/1My;

    invoke-static/range {v0 .. v9}, Lcom/facebook/tarot/cards/TarotCardEndCard;->a(Lcom/facebook/tarot/cards/TarotCardEndCard;LX/K87;LX/1Ad;LX/K8f;LX/2dj;LX/K91;LX/K5g;LX/0iA;LX/K7A;LX/1My;)V

    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 2771323
    const-class v0, Lcom/facebook/tarot/cards/TarotCardEndCard;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2771324
    const v0, 0x7f03147a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2771325
    const v0, 0x7f0d2e9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2771326
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/K66;

    invoke-direct {v1, p0}, LX/K66;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771327
    const v0, 0x7f0d2e9c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->s:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2771328
    const v0, 0x7f0d2e9d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2771329
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b261c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2771330
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2771331
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->s:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2771332
    const v0, 0x7f0d2e9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->u:Landroid/view/ViewGroup;

    .line 2771333
    const v0, 0x7f0d2e94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    .line 2771334
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->s:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->o:LX/K6G;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771335
    return-void
.end method

.method private i()V
    .locals 7

    .prologue
    .line 2771312
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->j:LX/K7A;

    .line 2771313
    iget-object v1, v0, LX/K7A;->a:LX/K79;

    move-object v0, v1

    .line 2771314
    if-nez v0, :cond_1

    .line 2771315
    :cond_0
    :goto_0
    return-void

    .line 2771316
    :cond_1
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->j:LX/K7A;

    .line 2771317
    iget-object v1, v0, LX/K7A;->a:LX/K79;

    move-object v0, v1

    .line 2771318
    iget-object v1, v0, LX/K79;->d:LX/0Px;

    move-object v0, v1

    .line 2771319
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2771320
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    .line 2771321
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->k:LX/1My;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 2771322
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->k:LX/1My;

    iget-object v3, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->q:LX/0TF;

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2, v0}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2771309
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_0

    .line 2771310
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/K67;

    invoke-direct {v1, p0}, LX/K67;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771311
    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 2771306
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_0

    .line 2771307
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2771308
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/tarot/cards/TarotCardEndCard;)V
    .locals 2

    .prologue
    .line 2771350
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->h:LX/K5g;

    new-instance v1, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;

    invoke-direct {v1}, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;-><init>()V

    invoke-virtual {v0, v1}, LX/K5g;->a(LX/K5U;)V

    .line 2771351
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    .line 2771290
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->i:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAROT_END_CARD_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/K8x;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/K8x;

    .line 2771291
    if-eqz v0, :cond_1

    .line 2771292
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->t:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2771293
    if-eqz v1, :cond_0

    .line 2771294
    new-instance v2, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2771295
    const/16 v3, 0x2710

    .line 2771296
    iput v3, v2, LX/0hs;->t:I

    .line 2771297
    iget-object v3, v0, LX/K8x;->b:LX/K91;

    .line 2771298
    iget-object v4, v3, LX/K91;->e:Ljava/util/Map;

    sget-object v5, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_TITLE:LX/K8y;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/K8z;

    invoke-virtual {v4}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2771299
    invoke-virtual {v2, v3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2771300
    iget-object v3, v0, LX/K8x;->b:LX/K91;

    .line 2771301
    iget-object v4, v3, LX/K91;->e:Ljava/util/Map;

    sget-object v5, LX/K8y;->END_SCREEN_MANAGE_SUBSCRIPTIONS_BUTTON_BLUE_CLUE_DESCRIPTION:LX/K8y;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/K8z;

    invoke-virtual {v4}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2771302
    invoke-virtual {v2, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2771303
    invoke-virtual {v2, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2771304
    :cond_0
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->i:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/K8x;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2771305
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2771289
    return-void
.end method

.method public final a(LX/K6F;)V
    .locals 5

    .prologue
    .line 2771234
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->y:LX/K6F;

    if-eq v0, p1, :cond_2

    .line 2771235
    invoke-virtual {p1}, LX/K6F;->getLayoutResourceId()I

    move-result v0

    .line 2771236
    iget-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2771237
    invoke-virtual {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->u:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2771238
    const v0, 0x7f0d0d70

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/view/CheckableButton;

    .line 2771239
    if-eqz v0, :cond_0

    .line 2771240
    new-instance v2, LX/K69;

    invoke-direct {v2, p0}, LX/K69;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    .line 2771241
    iput-object v2, v0, Lcom/facebook/tarot/view/CheckableButton;->g:LX/K68;

    .line 2771242
    :cond_0
    const v0, 0x7f0d0d6f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/view/CheckableButton;

    .line 2771243
    if-eqz v0, :cond_1

    .line 2771244
    new-instance v2, LX/K6A;

    invoke-direct {v2, p0}, LX/K6A;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    .line 2771245
    iput-object v2, v0, Lcom/facebook/tarot/view/CheckableButton;->g:LX/K68;

    .line 2771246
    :cond_1
    const v0, 0x7f0d0d6d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2771247
    const v2, 0x7f0d0d6e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2771248
    sget-object v2, LX/K6B;->a:[I

    invoke-virtual {p1}, LX/K6F;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2771249
    :goto_0
    iput-object p1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->y:LX/K6F;

    .line 2771250
    :cond_2
    return-void

    .line 2771251
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    .line 2771252
    iget-object v3, v2, LX/K91;->e:Ljava/util/Map;

    sget-object v4, LX/K8y;->END_SCREEN_ALREADY_CONNECTED_TITLE:LX/K8y;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/K8z;

    invoke-virtual {v3}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2771253
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771254
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    .line 2771255
    iget-object v2, v0, LX/K91;->e:Ljava/util/Map;

    sget-object v3, LX/K8y;->END_SCREEN_ALREADY_SUBSCRIBED_DESCRIPTION:LX/K8y;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/K8z;

    invoke-virtual {v2}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2771256
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2771257
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    invoke-virtual {v2}, LX/K91;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771258
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/K91;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2771259
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v3, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/K91;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771260
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/K91;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2771261
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    invoke-virtual {v2}, LX/K91;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771262
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/K91;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2771263
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    invoke-virtual {v2}, LX/K91;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771264
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/K91;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2771265
    :pswitch_5
    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v3, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/K91;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2771266
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->g:LX/K91;

    iget-object v2, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/K91;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2771282
    if-nez p1, :cond_0

    .line 2771283
    :goto_0
    return-void

    .line 2771284
    :cond_0
    const-string v0, "tarot_card_publisher_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->w:Ljava/lang/String;

    .line 2771285
    const-string v0, "tarot_card_publisher"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->x:Ljava/lang/String;

    .line 2771286
    const-string v0, "tarot_subscription_status_at_launch"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/K73;

    .line 2771287
    new-instance v1, LX/K5i;

    invoke-direct {v1, p0, v0}, LX/K5i;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;LX/K73;)V

    iput-object v1, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->v:LX/K5i;

    .line 2771288
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->i()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2771280
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->m()V

    .line 2771281
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2771278
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->k:LX/1My;

    invoke-virtual {v0}, LX/1My;->d()V

    .line 2771279
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2771276
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->k:LX/1My;

    invoke-virtual {v0}, LX/1My;->e()V

    .line 2771277
    return-void
.end method

.method public getBackgroundImageComponent()LX/K5k;
    .locals 1

    .prologue
    .line 2771275
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->m:LX/K6C;

    return-object v0
.end method

.method public getFeedbackComponent()LX/K5Z;
    .locals 1

    .prologue
    .line 2771274
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->p:LX/K6E;

    return-object v0
.end method

.method public getFontComponent()LX/K5m;
    .locals 1

    .prologue
    .line 2771273
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardEndCard;->n:LX/K6D;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2400d6c2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771270
    invoke-super {p0}, Lcom/facebook/tarot/cards/BaseTarotCard;->onAttachedToWindow()V

    .line 2771271
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->j()V

    .line 2771272
    const/16 v1, 0x2d

    const v2, 0x45c79a10

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2af3f806

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2771267
    invoke-super {p0}, Lcom/facebook/tarot/cards/BaseTarotCard;->onDetachedFromWindow()V

    .line 2771268
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardEndCard;->k()V

    .line 2771269
    const/16 v1, 0x2d

    const v2, 0x2d881087

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
