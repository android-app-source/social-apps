.class public Lcom/facebook/tarot/cards/TarotCardCover;
.super Lcom/facebook/tarot/cards/BaseTarotCard;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/K5b;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/tarot/cards/elements/SlideshowView;

.field public f:Lcom/facebook/tarot/cards/elements/CoverTextView;

.field private final g:LX/K5p;

.field private final h:LX/K5r;

.field private final i:LX/K5q;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2770717
    const-class v0, Lcom/facebook/tarot/cards/TarotCardCover;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/cards/TarotCardCover;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2770715
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tarot/cards/TarotCardCover;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2770716
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2770713
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/tarot/cards/TarotCardCover;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770714
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2770693
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/tarot/cards/BaseTarotCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770694
    new-instance v0, LX/K5p;

    invoke-direct {v0, p0}, LX/K5p;-><init>(Lcom/facebook/tarot/cards/TarotCardCover;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->g:LX/K5p;

    .line 2770695
    new-instance v0, LX/K5r;

    invoke-direct {v0, p0}, LX/K5r;-><init>(Lcom/facebook/tarot/cards/TarotCardCover;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->h:LX/K5r;

    .line 2770696
    new-instance v0, LX/K5q;

    invoke-direct {v0, p0}, LX/K5q;-><init>(Lcom/facebook/tarot/cards/TarotCardCover;)V

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->i:LX/K5q;

    .line 2770697
    invoke-direct {p0}, Lcom/facebook/tarot/cards/TarotCardCover;->g()V

    .line 2770698
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/cards/TarotCardCover;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/cards/TarotCardCover;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v0

    check-cast v0, LX/Chv;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->c:LX/Chv;

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2770706
    const-class v0, Lcom/facebook/tarot/cards/TarotCardCover;

    invoke-static {v0, p0}, Lcom/facebook/tarot/cards/TarotCardCover;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2770707
    const v0, 0x7f031476

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2770708
    const v0, 0x7f0d2e91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/SlideshowView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    .line 2770709
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    new-instance v1, LX/K5o;

    invoke-direct {v1, p0}, LX/K5o;-><init>(Lcom/facebook/tarot/cards/TarotCardCover;)V

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/cards/elements/SlideshowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2770710
    const v0, 0x7f0d2e93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/CoverTextView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->f:Lcom/facebook/tarot/cards/elements/CoverTextView;

    .line 2770711
    const v0, 0x7f0d2e94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    iput-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    .line 2770712
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2770704
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->d()V

    .line 2770705
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2770702
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->e:Lcom/facebook/tarot/cards/elements/SlideshowView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/SlideshowView;->c()V

    .line 2770703
    return-void
.end method

.method public getCoverTextComponent()LX/K5p;
    .locals 1

    .prologue
    .line 2770701
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->g:LX/K5p;

    return-object v0
.end method

.method public getFontComponent()LX/K5m;
    .locals 1

    .prologue
    .line 2770700
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->i:LX/K5q;

    return-object v0
.end method

.method public getSlideshowComponent()LX/K5r;
    .locals 1

    .prologue
    .line 2770699
    iget-object v0, p0, Lcom/facebook/tarot/cards/TarotCardCover;->h:LX/K5r;

    return-object v0
.end method
