.class public Lcom/facebook/tarot/data/TarotCardVideoData;
.super Lcom/facebook/tarot/data/BaseTarotCardData;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/TarotCardVideoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/K6K;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/tarot/data/VideoData;

.field private final d:Lcom/facebook/tarot/data/DescriptionData;

.field private final e:Lcom/facebook/tarot/data/FeedbackData;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2773042
    new-instance v0, LX/K7I;

    invoke-direct {v0}, LX/K7I;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/TarotCardVideoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2773052
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2773053
    sget-object v0, LX/K6K;->VIDEO:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->a:LX/K6K;

    .line 2773054
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 2773055
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->b:Ljava/lang/String;

    .line 2773056
    const-class v0, Lcom/facebook/tarot/data/VideoData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/VideoData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->c:Lcom/facebook/tarot/data/VideoData;

    .line 2773057
    const-class v0, Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/DescriptionData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->d:Lcom/facebook/tarot/data/DescriptionData;

    .line 2773058
    const-class v0, Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/FeedbackData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2773059
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->f:Ljava/lang/String;

    .line 2773060
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->g:Ljava/lang/String;

    .line 2773061
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/tarot/data/VideoData;Lcom/facebook/tarot/data/DescriptionData;Lcom/facebook/tarot/data/FeedbackData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2773043
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2773044
    sget-object v0, LX/K6K;->VIDEO:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->a:LX/K6K;

    .line 2773045
    iput-object p1, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->b:Ljava/lang/String;

    .line 2773046
    iput-object p2, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->c:Lcom/facebook/tarot/data/VideoData;

    .line 2773047
    iput-object p3, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->d:Lcom/facebook/tarot/data/DescriptionData;

    .line 2773048
    iput-object p4, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2773049
    iput-object p5, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->f:Ljava/lang/String;

    .line 2773050
    iput-object p6, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->g:Ljava/lang/String;

    .line 2773051
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2773031
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2773032
    const-string v1, "tarot_card_type"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->a:LX/K6K;

    invoke-virtual {v2}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773033
    const-string v1, "tarot_card_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773034
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->c:Lcom/facebook/tarot/data/VideoData;

    .line 2773035
    const-string v2, "tarot_card_bgvideo_uri"

    iget-object v3, v1, Lcom/facebook/tarot/data/VideoData;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773036
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->d:Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/DescriptionData;->a(Landroid/os/Bundle;)V

    .line 2773037
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->e:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/FeedbackData;->a(Landroid/os/Bundle;)V

    .line 2773038
    const-string v1, "instant_article_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773039
    const-string v1, "instant_article_url"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773040
    return-object v0
.end method

.method public final b()Lcom/facebook/tarot/data/DescriptionData;
    .locals 1

    .prologue
    .line 2773041
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->d:Lcom/facebook/tarot/data/DescriptionData;

    return-object v0
.end method

.method public final c()Lcom/facebook/tarot/data/FeedbackData;
    .locals 1

    .prologue
    .line 2773030
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->e:Lcom/facebook/tarot/data/FeedbackData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2773029
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/tarot/data/VideoData;
    .locals 1

    .prologue
    .line 2773028
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->c:Lcom/facebook/tarot/data/VideoData;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2773027
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2773026
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2773018
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->a:LX/K6K;

    invoke-virtual {v0}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773019
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773020
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->c:Lcom/facebook/tarot/data/VideoData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2773021
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->d:Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2773022
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->e:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2773023
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773024
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardVideoData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773025
    return-void
.end method
