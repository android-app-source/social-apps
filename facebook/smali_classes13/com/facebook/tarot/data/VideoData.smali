.class public Lcom/facebook/tarot/data/VideoData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/VideoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2773091
    new-instance v0, LX/K7L;

    invoke-direct {v0}, LX/K7L;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/VideoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2773082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773083
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->a:Ljava/lang/String;

    .line 2773084
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tarot/data/VideoData;->b:J

    .line 2773085
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tarot/data/VideoData;->c:J

    .line 2773086
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->d:Ljava/lang/String;

    .line 2773087
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->e:Ljava/lang/String;

    .line 2773088
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->f:Ljava/lang/String;

    .line 2773089
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->g:Ljava/lang/String;

    .line 2773090
    return-void
.end method

.method public constructor <init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;)V
    .locals 2

    .prologue
    .line 2773092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773093
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->a:Ljava/lang/String;

    .line 2773094
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->D()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/facebook/tarot/data/VideoData;->b:J

    .line 2773095
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->m()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/facebook/tarot/data/VideoData;->c:J

    .line 2773096
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->d:Ljava/lang/String;

    .line 2773097
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->e:Ljava/lang/String;

    .line 2773098
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->f:Ljava/lang/String;

    .line 2773099
    invoke-virtual {p1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/VideoData;->g:Ljava/lang/String;

    .line 2773100
    return-void
.end method

.method public static b(Lcom/facebook/tarot/data/VideoData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2773081
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->d:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2773080
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2773072
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773073
    iget-wide v0, p0, Lcom/facebook/tarot/data/VideoData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2773074
    iget-wide v0, p0, Lcom/facebook/tarot/data/VideoData;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2773075
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773076
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773077
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773078
    iget-object v0, p0, Lcom/facebook/tarot/data/VideoData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2773079
    return-void
.end method
