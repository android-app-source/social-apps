.class public Lcom/facebook/tarot/data/TarotCardCoverData;
.super Lcom/facebook/tarot/data/BaseTarotCardData;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/TarotCardCoverData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/K6K;

.field private final c:Lcom/facebook/tarot/data/CoverTextData;

.field private final d:Lcom/facebook/tarot/data/FeedbackData;

.field private final e:Lcom/facebook/tarot/data/SlideshowData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772805
    new-instance v0, LX/K7C;

    invoke-direct {v0}, LX/K7C;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/TarotCardCoverData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2772806
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2772807
    sget-object v0, LX/K6K;->COVER:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->b:LX/K6K;

    .line 2772808
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 2772809
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->a:Ljava/lang/String;

    .line 2772810
    const-class v0, Lcom/facebook/tarot/data/CoverTextData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/CoverTextData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->c:Lcom/facebook/tarot/data/CoverTextData;

    .line 2772811
    const-class v0, Lcom/facebook/tarot/data/SlideshowData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/SlideshowData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    .line 2772812
    const-class v0, Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/FeedbackData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->d:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772813
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;Lcom/facebook/tarot/data/SlideshowData;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2772829
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2772830
    sget-object v0, LX/K6K;->COVER:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->b:LX/K6K;

    .line 2772831
    iput-object p1, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->a:Ljava/lang/String;

    .line 2772832
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2772833
    :cond_0
    const/4 v0, 0x0

    .line 2772834
    :goto_0
    move-object v4, v0

    .line 2772835
    new-instance v0, Lcom/facebook/tarot/data/CoverTextData;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel$LogoModel;->j()Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel$LogoModel;->k()I

    move-result v2

    :goto_2
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel$LogoModel;->a()I

    move-result v3

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->j()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/tarot/data/CoverTextData;-><init>(Ljava/lang/String;IIJLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->c:Lcom/facebook/tarot/data/CoverTextData;

    .line 2772836
    iput-object p3, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    .line 2772837
    new-instance v0, Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->r()Ljava/lang/String;

    move-result-object v2

    const v3, -0x11b13572    # -1.5999696E28f

    const/16 v4, 0x7d1

    const-string v5, "chromeless:content:fragment:tag"

    invoke-direct/range {v0 .. v5}, Lcom/facebook/tarot/data/FeedbackData;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->d:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772838
    return-void

    .line 2772839
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    :cond_4
    invoke-virtual {p2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;->j()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel$LogoModel;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 7

    .prologue
    .line 2772814
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2772815
    const-string v1, "tarot_card_type"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->b:LX/K6K;

    invoke-virtual {v2}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772816
    const-string v1, "tarot_card_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772817
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->c:Lcom/facebook/tarot/data/CoverTextData;

    .line 2772818
    const-string v3, "tarot_card_bgimg_uri"

    iget-object v4, v1, Lcom/facebook/tarot/data/CoverTextData;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772819
    const-string v3, "tarot_card_logo_width_dps"

    iget v4, v1, Lcom/facebook/tarot/data/CoverTextData;->b:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2772820
    const-string v3, "tarot_card_logo_height_dps"

    iget v4, v1, Lcom/facebook/tarot/data/CoverTextData;->c:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2772821
    const-string v3, "tarot_card_creation_time"

    iget-wide v5, v1, Lcom/facebook/tarot/data/CoverTextData;->d:J

    invoke-virtual {v0, v3, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2772822
    const-string v3, "tarot_card_publisher"

    iget-object v4, v1, Lcom/facebook/tarot/data/CoverTextData;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772823
    const-string v3, "tarot_card_cover_text"

    iget-object v4, v1, Lcom/facebook/tarot/data/CoverTextData;->f:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772824
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    .line 2772825
    const-string v3, "tarot_card_slideshow_images"

    iget-object v2, v1, Lcom/facebook/tarot/data/SlideshowData;->a:Ljava/util/List;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2772826
    const-string v3, "tarot_card_slideshow_videos"

    iget-object v2, v1, Lcom/facebook/tarot/data/SlideshowData;->b:Ljava/util/List;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2772827
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->d:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/FeedbackData;->a(Landroid/os/Bundle;)V

    .line 2772828
    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2772799
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    .line 2772800
    iget-object p0, v0, Lcom/facebook/tarot/data/SlideshowData;->a:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2772801
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tarot/data/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2772802
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    .line 2772803
    iget-object p0, v0, Lcom/facebook/tarot/data/SlideshowData;->b:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2772804
    return-void
.end method

.method public final c()Lcom/facebook/tarot/data/FeedbackData;
    .locals 1

    .prologue
    .line 2772798
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->d:Lcom/facebook/tarot/data/FeedbackData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772797
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Lcom/facebook/tarot/data/SlideshowData;
    .locals 1

    .prologue
    .line 2772796
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    return-object v0
.end method

.method public final g()Lcom/facebook/tarot/data/CoverTextData;
    .locals 1

    .prologue
    .line 2772795
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->c:Lcom/facebook/tarot/data/CoverTextData;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2772789
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->b:LX/K6K;

    invoke-virtual {v0}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772790
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772791
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->c:Lcom/facebook/tarot/data/CoverTextData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772792
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->e:Lcom/facebook/tarot/data/SlideshowData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772793
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardCoverData;->d:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772794
    return-void
.end method
