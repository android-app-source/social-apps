.class public Lcom/facebook/tarot/data/BackgroundImageData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/BackgroundImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772609
    new-instance v0, LX/K75;

    invoke-direct {v0}, LX/K75;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/BackgroundImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2772616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772617
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/BackgroundImageData;->a:Ljava/lang/String;

    .line 2772618
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2772613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772614
    iput-object p1, p0, Lcom/facebook/tarot/data/BackgroundImageData;->a:Ljava/lang/String;

    .line 2772615
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2772619
    const-string v0, "tarot_card_bgimg_uri"

    iget-object v1, p0, Lcom/facebook/tarot/data/BackgroundImageData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772620
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772612
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2772610
    iget-object v0, p0, Lcom/facebook/tarot/data/BackgroundImageData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772611
    return-void
.end method
