.class public Lcom/facebook/tarot/data/CoverTextData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/CoverTextData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772634
    new-instance v0, LX/K76;

    invoke-direct {v0}, LX/K76;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/CoverTextData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2772651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772652
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/CoverTextData;->a:Ljava/lang/String;

    .line 2772653
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/data/CoverTextData;->b:I

    .line 2772654
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/data/CoverTextData;->c:I

    .line 2772655
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tarot/data/CoverTextData;->d:J

    .line 2772656
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/CoverTextData;->e:Ljava/lang/String;

    .line 2772657
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/CoverTextData;->f:Ljava/lang/String;

    .line 2772658
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIJLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2772643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772644
    iput-object p1, p0, Lcom/facebook/tarot/data/CoverTextData;->a:Ljava/lang/String;

    .line 2772645
    iput p2, p0, Lcom/facebook/tarot/data/CoverTextData;->b:I

    .line 2772646
    iput p3, p0, Lcom/facebook/tarot/data/CoverTextData;->c:I

    .line 2772647
    iput-wide p4, p0, Lcom/facebook/tarot/data/CoverTextData;->d:J

    .line 2772648
    iput-object p6, p0, Lcom/facebook/tarot/data/CoverTextData;->e:Ljava/lang/String;

    .line 2772649
    iput-object p7, p0, Lcom/facebook/tarot/data/CoverTextData;->f:Ljava/lang/String;

    .line 2772650
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772642
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2772635
    iget-object v0, p0, Lcom/facebook/tarot/data/CoverTextData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772636
    iget v0, p0, Lcom/facebook/tarot/data/CoverTextData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2772637
    iget v0, p0, Lcom/facebook/tarot/data/CoverTextData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2772638
    iget-wide v0, p0, Lcom/facebook/tarot/data/CoverTextData;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2772639
    iget-object v0, p0, Lcom/facebook/tarot/data/CoverTextData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772640
    iget-object v0, p0, Lcom/facebook/tarot/data/CoverTextData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772641
    return-void
.end method
