.class public Lcom/facebook/tarot/data/TarotCardImageData;
.super Lcom/facebook/tarot/data/BaseTarotCardData;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/TarotCardImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/K6K;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/tarot/data/DescriptionData;

.field private final d:Lcom/facebook/tarot/data/FeedbackData;

.field private final e:Lcom/facebook/tarot/data/BackgroundImageData;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772952
    new-instance v0, LX/K7G;

    invoke-direct {v0}, LX/K7G;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/TarotCardImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2772953
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2772954
    sget-object v0, LX/K6K;->IMAGE:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->a:LX/K6K;

    .line 2772955
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 2772956
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->b:Ljava/lang/String;

    .line 2772957
    const-class v0, Lcom/facebook/tarot/data/BackgroundImageData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/BackgroundImageData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->e:Lcom/facebook/tarot/data/BackgroundImageData;

    .line 2772958
    const-class v0, Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/DescriptionData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->c:Lcom/facebook/tarot/data/DescriptionData;

    .line 2772959
    const-class v0, Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/FeedbackData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->d:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772960
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->f:Ljava/lang/String;

    .line 2772961
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->g:Ljava/lang/String;

    .line 2772962
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/tarot/data/BackgroundImageData;Lcom/facebook/tarot/data/DescriptionData;Lcom/facebook/tarot/data/FeedbackData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2772963
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2772964
    sget-object v0, LX/K6K;->IMAGE:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->a:LX/K6K;

    .line 2772965
    iput-object p1, p0, Lcom/facebook/tarot/data/TarotCardImageData;->b:Ljava/lang/String;

    .line 2772966
    iput-object p2, p0, Lcom/facebook/tarot/data/TarotCardImageData;->e:Lcom/facebook/tarot/data/BackgroundImageData;

    .line 2772967
    iput-object p3, p0, Lcom/facebook/tarot/data/TarotCardImageData;->c:Lcom/facebook/tarot/data/DescriptionData;

    .line 2772968
    iput-object p4, p0, Lcom/facebook/tarot/data/TarotCardImageData;->d:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772969
    iput-object p5, p0, Lcom/facebook/tarot/data/TarotCardImageData;->f:Ljava/lang/String;

    .line 2772970
    iput-object p6, p0, Lcom/facebook/tarot/data/TarotCardImageData;->g:Ljava/lang/String;

    .line 2772971
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2772972
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2772973
    const-string v1, "tarot_card_type"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardImageData;->a:LX/K6K;

    invoke-virtual {v2}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772974
    const-string v1, "tarot_card_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardImageData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772975
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardImageData;->e:Lcom/facebook/tarot/data/BackgroundImageData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/BackgroundImageData;->a(Landroid/os/Bundle;)V

    .line 2772976
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardImageData;->c:Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/DescriptionData;->a(Landroid/os/Bundle;)V

    .line 2772977
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardImageData;->d:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/FeedbackData;->a(Landroid/os/Bundle;)V

    .line 2772978
    const-string v1, "instant_article_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardImageData;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772979
    const-string v1, "instant_article_url"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardImageData;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772980
    return-object v0
.end method

.method public final b()Lcom/facebook/tarot/data/DescriptionData;
    .locals 1

    .prologue
    .line 2772981
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->c:Lcom/facebook/tarot/data/DescriptionData;

    return-object v0
.end method

.method public final c()Lcom/facebook/tarot/data/FeedbackData;
    .locals 1

    .prologue
    .line 2772982
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->d:Lcom/facebook/tarot/data/FeedbackData;

    return-object v0
.end method

.method public final d()Lcom/facebook/tarot/data/BackgroundImageData;
    .locals 1

    .prologue
    .line 2772983
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->e:Lcom/facebook/tarot/data/BackgroundImageData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772984
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2772985
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2772986
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2772987
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->a:LX/K6K;

    invoke-virtual {v0}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772988
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772989
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->e:Lcom/facebook/tarot/data/BackgroundImageData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772990
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->c:Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772991
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->d:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772992
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772993
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardImageData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772994
    return-void
.end method
