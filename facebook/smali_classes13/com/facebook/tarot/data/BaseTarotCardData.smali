.class public abstract Lcom/facebook/tarot/data/BaseTarotCardData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2772630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2772629
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Lcom/facebook/tarot/data/DescriptionData;
    .locals 1

    .prologue
    .line 2772628
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Lcom/facebook/tarot/data/FeedbackData;
    .locals 1

    .prologue
    .line 2772627
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Lcom/facebook/tarot/data/BackgroundImageData;
    .locals 1

    .prologue
    .line 2772626
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Lcom/facebook/tarot/data/VideoData;
    .locals 1

    .prologue
    .line 2772625
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Lcom/facebook/tarot/data/SlideshowData;
    .locals 1

    .prologue
    .line 2772621
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()Lcom/facebook/tarot/data/CoverTextData;
    .locals 1

    .prologue
    .line 2772624
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2772623
    const/4 v0, 0x0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2772622
    const/4 v0, 0x0

    return-object v0
.end method
