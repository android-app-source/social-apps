.class public Lcom/facebook/tarot/data/TarotCardEndCardData;
.super Lcom/facebook/tarot/data/BaseTarotCardData;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/TarotCardEndCardData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/K6K;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/tarot/data/BackgroundImageData;

.field private final d:Lcom/facebook/tarot/data/DescriptionData;

.field private final e:Lcom/facebook/tarot/data/FeedbackData;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:LX/K73;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772928
    new-instance v0, LX/K7E;

    invoke-direct {v0}, LX/K7E;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/TarotCardEndCardData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2772917
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2772918
    sget-object v0, LX/K6K;->END_CARD:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->a:LX/K6K;

    .line 2772919
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 2772920
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->b:Ljava/lang/String;

    .line 2772921
    const-class v0, Lcom/facebook/tarot/data/BackgroundImageData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/BackgroundImageData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->c:Lcom/facebook/tarot/data/BackgroundImageData;

    .line 2772922
    const-class v0, Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/DescriptionData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->d:Lcom/facebook/tarot/data/DescriptionData;

    .line 2772923
    const-class v0, Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/data/FeedbackData;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772924
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->f:Ljava/lang/String;

    .line 2772925
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->g:Ljava/lang/String;

    .line 2772926
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/K73;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->h:LX/K73;

    .line 2772927
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/tarot/data/BackgroundImageData;Lcom/facebook/tarot/data/DescriptionData;Lcom/facebook/tarot/data/FeedbackData;Ljava/lang/String;Ljava/lang/String;LX/K73;)V
    .locals 1

    .prologue
    .line 2772907
    invoke-direct {p0}, Lcom/facebook/tarot/data/BaseTarotCardData;-><init>()V

    .line 2772908
    sget-object v0, LX/K6K;->END_CARD:LX/K6K;

    iput-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->a:LX/K6K;

    .line 2772909
    iput-object p1, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->b:Ljava/lang/String;

    .line 2772910
    iput-object p2, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->c:Lcom/facebook/tarot/data/BackgroundImageData;

    .line 2772911
    iput-object p3, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->d:Lcom/facebook/tarot/data/DescriptionData;

    .line 2772912
    iput-object p4, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772913
    iput-object p5, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->f:Ljava/lang/String;

    .line 2772914
    iput-object p6, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->g:Ljava/lang/String;

    .line 2772915
    iput-object p7, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->h:LX/K73;

    .line 2772916
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2772884
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2772885
    const-string v1, "tarot_card_type"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->a:LX/K6K;

    invoke-virtual {v2}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772886
    const-string v1, "tarot_card_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772887
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->c:Lcom/facebook/tarot/data/BackgroundImageData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/BackgroundImageData;->a(Landroid/os/Bundle;)V

    .line 2772888
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->d:Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/DescriptionData;->a(Landroid/os/Bundle;)V

    .line 2772889
    iget-object v1, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->e:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {v1, v0}, Lcom/facebook/tarot/data/FeedbackData;->a(Landroid/os/Bundle;)V

    .line 2772890
    const-string v1, "tarot_card_publisher_id"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772891
    const-string v1, "tarot_card_publisher"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772892
    const-string v1, "tarot_subscription_status_at_launch"

    iget-object v2, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->h:LX/K73;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2772893
    return-object v0
.end method

.method public final b()Lcom/facebook/tarot/data/DescriptionData;
    .locals 1

    .prologue
    .line 2772906
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->d:Lcom/facebook/tarot/data/DescriptionData;

    return-object v0
.end method

.method public final c()Lcom/facebook/tarot/data/FeedbackData;
    .locals 1

    .prologue
    .line 2772905
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->e:Lcom/facebook/tarot/data/FeedbackData;

    return-object v0
.end method

.method public final d()Lcom/facebook/tarot/data/BackgroundImageData;
    .locals 1

    .prologue
    .line 2772904
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->c:Lcom/facebook/tarot/data/BackgroundImageData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772903
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2772894
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->a:LX/K6K;

    invoke-virtual {v0}, LX/K6K;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772895
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772896
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->c:Lcom/facebook/tarot/data/BackgroundImageData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772897
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->d:Lcom/facebook/tarot/data/DescriptionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772898
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->e:Lcom/facebook/tarot/data/FeedbackData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772899
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772900
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772901
    iget-object v0, p0, Lcom/facebook/tarot/data/TarotCardEndCardData;->h:LX/K73;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2772902
    return-void
.end method
