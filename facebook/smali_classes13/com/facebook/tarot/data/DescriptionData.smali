.class public Lcom/facebook/tarot/data/DescriptionData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/DescriptionData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:F

.field public final d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772662
    new-instance v0, LX/K77;

    invoke-direct {v0}, LX/K77;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/DescriptionData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2772663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772664
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/DescriptionData;->a:Ljava/lang/String;

    .line 2772665
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/DescriptionData;->b:Ljava/lang/String;

    .line 2772666
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/data/DescriptionData;->c:F

    .line 2772667
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/data/DescriptionData;->d:F

    .line 2772668
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FF)V
    .locals 0

    .prologue
    .line 2772669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772670
    iput-object p1, p0, Lcom/facebook/tarot/data/DescriptionData;->a:Ljava/lang/String;

    .line 2772671
    iput-object p2, p0, Lcom/facebook/tarot/data/DescriptionData;->b:Ljava/lang/String;

    .line 2772672
    iput p3, p0, Lcom/facebook/tarot/data/DescriptionData;->c:F

    .line 2772673
    iput p4, p0, Lcom/facebook/tarot/data/DescriptionData;->d:F

    .line 2772674
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2772675
    const-string v0, "tarot_card_headline_text"

    iget-object v1, p0, Lcom/facebook/tarot/data/DescriptionData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772676
    const-string v0, "tarot_card_desc_text"

    iget-object v1, p0, Lcom/facebook/tarot/data/DescriptionData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772677
    const-string v0, "taort_card_headline_line_height_multiplier"

    iget v1, p0, Lcom/facebook/tarot/data/DescriptionData;->c:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2772678
    const-string v0, "taort_card_desc_line_height_multiplier"

    iget v1, p0, Lcom/facebook/tarot/data/DescriptionData;->d:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2772679
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772680
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2772681
    iget-object v0, p0, Lcom/facebook/tarot/data/DescriptionData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772682
    iget-object v0, p0, Lcom/facebook/tarot/data/DescriptionData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772683
    iget v0, p0, Lcom/facebook/tarot/data/DescriptionData;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2772684
    iget v0, p0, Lcom/facebook/tarot/data/DescriptionData;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2772685
    return-void
.end method
