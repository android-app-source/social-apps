.class public Lcom/facebook/tarot/data/FeedbackData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tarot/data/FeedbackData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2772689
    new-instance v0, LX/K78;

    invoke-direct {v0}, LX/K78;-><init>()V

    sput-object v0, Lcom/facebook/tarot/data/FeedbackData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2772690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772691
    const-class v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 2772692
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v1

    .line 2772693
    iput-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2772694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->b:Ljava/lang/String;

    .line 2772695
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->c:Ljava/lang/String;

    .line 2772696
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/data/FeedbackData;->d:I

    .line 2772697
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/data/FeedbackData;->e:I

    .line 2772698
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->f:Ljava/lang/String;

    .line 2772699
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;)V
    .locals 7

    .prologue
    .line 2772700
    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tarot/data/FeedbackData;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 2772701
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2772702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772703
    iput-object p1, p0, Lcom/facebook/tarot/data/FeedbackData;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2772704
    iput-object p2, p0, Lcom/facebook/tarot/data/FeedbackData;->c:Ljava/lang/String;

    .line 2772705
    iput p3, p0, Lcom/facebook/tarot/data/FeedbackData;->d:I

    .line 2772706
    iput p4, p0, Lcom/facebook/tarot/data/FeedbackData;->e:I

    .line 2772707
    iput-object p5, p0, Lcom/facebook/tarot/data/FeedbackData;->f:Ljava/lang/String;

    .line 2772708
    iput-object p6, p0, Lcom/facebook/tarot/data/FeedbackData;->b:Ljava/lang/String;

    .line 2772709
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2772710
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    iget-object v1, p0, Lcom/facebook/tarot/data/FeedbackData;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2772711
    iput-object v1, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2772712
    move-object v0, v0

    .line 2772713
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    .line 2772714
    const-string v1, "tarot_card_feedback"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2772715
    const-string v0, "tarot_card_share_text"

    iget-object v1, p0, Lcom/facebook/tarot/data/FeedbackData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772716
    const-string v0, "tarot_card_sharing_object_id"

    iget-object v1, p0, Lcom/facebook/tarot/data/FeedbackData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772717
    const-string v0, "tarot_card_graphql_type"

    iget v1, p0, Lcom/facebook/tarot/data/FeedbackData;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2772718
    const-string v0, "tarot_card_request_code"

    iget v1, p0, Lcom/facebook/tarot/data/FeedbackData;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2772719
    const-string v0, "tarot_card_fragment_tag"

    iget-object v1, p0, Lcom/facebook/tarot/data/FeedbackData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772720
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2772721
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2772722
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    iget-object v1, p0, Lcom/facebook/tarot/data/FeedbackData;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2772723
    iput-object v1, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2772724
    move-object v0, v0

    .line 2772725
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    .line 2772726
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2772727
    iget-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772728
    iget-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772729
    iget v0, p0, Lcom/facebook/tarot/data/FeedbackData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2772730
    iget v0, p0, Lcom/facebook/tarot/data/FeedbackData;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2772731
    iget-object v0, p0, Lcom/facebook/tarot/data/FeedbackData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2772732
    return-void
.end method
