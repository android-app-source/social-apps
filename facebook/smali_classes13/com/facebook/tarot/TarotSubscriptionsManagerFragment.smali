.class public Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/K5U;


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public a:LX/K7m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/K8I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/K91;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/1B1;

.field private final f:LX/K5N;

.field private final g:LX/K5P;

.field public h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private j:Lcom/facebook/fbui/glyph/GlyphView;

.field public k:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2770411
    const-class v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2770432
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2770433
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->e:LX/1B1;

    .line 2770434
    new-instance v0, LX/K5O;

    invoke-direct {v0, p0}, LX/K5O;-><init>(Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->f:LX/K5N;

    .line 2770435
    new-instance v0, LX/K5Q;

    invoke-direct {v0, p0}, LX/K5Q;-><init>(Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->g:LX/K5P;

    .line 2770436
    return-void
.end method

.method public static d(Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;)V
    .locals 10

    .prologue
    .line 2770415
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2770416
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->b:LX/K8I;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->a:LX/K7m;

    .line 2770417
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 2770418
    :cond_0
    :goto_0
    return-void

    .line 2770419
    :cond_1
    new-instance v6, LX/K8i;

    invoke-direct {v6}, LX/K8i;-><init>()V

    move-object v6, v6

    .line 2770420
    const-string v7, "page_logo_size"

    .line 2770421
    const/16 v8, 0xc8

    move v8, v8

    .line 2770422
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2770423
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0x3c

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    const/4 v7, 0x1

    .line 2770424
    iput-boolean v7, v6, LX/0zO;->p:Z

    .line 2770425
    move-object v6, v6

    .line 2770426
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v6, v7}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v6

    sget-object v7, LX/0zS;->a:LX/0zS;

    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    .line 2770427
    move-object v3, v6

    .line 2770428
    iget-object v4, v0, LX/K8I;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2770429
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2770430
    new-instance v5, LX/K8H;

    invoke-direct {v5, v0, v4, v2}, LX/K8H;-><init>(LX/K8I;Landroid/os/Handler;LX/K7m;)V

    move-object v4, v5

    .line 2770431
    iget-object v5, v0, LX/K8I;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2770412
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->k:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    if-eqz v0, :cond_0

    .line 2770413
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->k:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/popover/PopoverFragment;->l()V

    .line 2770414
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2770408
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2770409
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;

    invoke-static {v0}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v2

    check-cast v2, LX/K7m;

    invoke-static {v0}, LX/K8I;->a(LX/0QB;)LX/K8I;

    move-result-object p1

    check-cast p1, LX/K8I;

    invoke-static {v0}, LX/K91;->a(LX/0QB;)LX/K91;

    move-result-object v0

    check-cast v0, LX/K91;

    iput-object v2, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->a:LX/K7m;

    iput-object p1, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->b:LX/K8I;

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->c:LX/K91;

    .line 2770410
    return-void
.end method

.method public final a(Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;)V
    .locals 0

    .prologue
    .line 2770437
    iput-object p1, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->k:Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;

    .line 2770438
    return-void
.end method

.method public final a(LX/31M;)Z
    .locals 2

    .prologue
    .line 2770403
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_0

    .line 2770404
    sget-object v0, LX/K5T;->a:[I

    invoke-virtual {p1}, LX/31M;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2770405
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2770406
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-static {v0}, LX/K5j;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)Z

    move-result v0

    goto :goto_0

    .line 2770407
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-static {v0}, LX/K5j;->b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final ns_()V
    .locals 0

    .prologue
    .line 2770402
    return-void
.end method

.method public final nt_()V
    .locals 0

    .prologue
    .line 2770387
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x35662e3a    # -5040355.0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2770401
    const v1, 0x7f03148a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x54e6fb02

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f94de16

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2770398
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2770399
    iget-object v1, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->e:LX/1B1;

    iget-object v2, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->a:LX/K7m;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2770400
    const/16 v1, 0x2b

    const v2, -0x5a95ede9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2770388
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2770389
    const v0, 0x7f0d2eb8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2770390
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/K95;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, LX/K95;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2770391
    const v0, 0x7f0d2eb7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->j:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2770392
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->j:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/K5R;

    invoke-direct {v1, p0}, LX/K5R;-><init>(Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2770393
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->e:LX/1B1;

    iget-object v1, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->f:LX/K5N;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2770394
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->e:LX/1B1;

    iget-object v1, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->g:LX/K5P;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2770395
    iget-object v0, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->e:LX/1B1;

    iget-object v1, p0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->a:LX/K7m;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2770396
    invoke-static {p0}, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->d(Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;)V

    .line 2770397
    return-void
.end method
