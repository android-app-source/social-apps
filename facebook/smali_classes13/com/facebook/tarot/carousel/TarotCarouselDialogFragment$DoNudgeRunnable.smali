.class public final Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/view/ViewPager;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0So;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0So;I)V
    .locals 1

    .prologue
    .line 2772074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772075
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->a:Ljava/lang/ref/WeakReference;

    .line 2772076
    iput-object p2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2772077
    iput-object p3, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->c:LX/0So;

    .line 2772078
    iput p4, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->d:I

    .line 2772079
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2772080
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2772081
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->c:LX/0So;

    if-eqz v1, :cond_0

    .line 2772082
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/K74;->a:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    sget-object v2, LX/K74;->b:LX/0Tn;

    iget v3, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;->d:I

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2772083
    new-instance v1, LX/IXn;

    invoke-direct {v1, v0}, LX/IXn;-><init>(Landroid/support/v4/view/ViewPager;)V

    .line 2772084
    invoke-virtual {v1}, LX/IXn;->a()V

    .line 2772085
    :cond_0
    return-void
.end method
