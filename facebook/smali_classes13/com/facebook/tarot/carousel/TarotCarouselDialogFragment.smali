.class public Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""


# instance fields
.field public A:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

.field public B:LX/Gmd;

.field public final C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/K5t;",
            ">;"
        }
    .end annotation
.end field

.field public D:Z

.field public E:Z

.field private F:Z

.field public G:LX/K79;

.field private H:LX/8qU;

.field private I:Landroid/content/Context;

.field public final J:LX/K6b;

.field public final K:LX/K6d;

.field public final L:LX/K6f;

.field public m:LX/8bM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/K7P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/K6a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/K8t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K87;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/K8w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/K8s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/K91;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K7A;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/K6i;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2772190
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    .line 2772191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->y:Ljava/util/List;

    .line 2772192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    .line 2772193
    iput-boolean v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->E:Z

    .line 2772194
    iput-boolean v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->F:Z

    .line 2772195
    new-instance v0, LX/K6c;

    invoke-direct {v0, p0}, LX/K6c;-><init>(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->J:LX/K6b;

    .line 2772196
    new-instance v0, LX/K6e;

    invoke-direct {v0, p0}, LX/K6e;-><init>(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->K:LX/K6d;

    .line 2772197
    new-instance v0, LX/K6g;

    invoke-direct {v0, p0}, LX/K6g;-><init>(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->L:LX/K6f;

    .line 2772198
    return-void
.end method

.method private static A(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2772199
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2772200
    :goto_0
    return-object v0

    .line 2772201
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2772202
    invoke-virtual {v0}, LX/K5t;->b()I

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 2772203
    goto :goto_0

    .line 2772204
    :cond_1
    invoke-virtual {v0}, LX/K5t;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/K5t;->a(I)LX/K7D;

    move-result-object v0

    .line 2772205
    const/4 p0, 0x0

    .line 2772206
    const/4 v1, 0x0

    .line 2772207
    iget-object v2, v0, LX/K7D;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2772208
    iget-object v1, v0, LX/K7D;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2772209
    :cond_2
    :goto_1
    move-object v0, v1

    .line 2772210
    goto :goto_0

    .line 2772211
    :cond_3
    iget-object v2, v0, LX/K7D;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2772212
    iget-object v1, v0, LX/K7D;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/data/VideoData;

    iget-object v1, v1, Lcom/facebook/tarot/data/VideoData;->g:Ljava/lang/String;

    goto :goto_1
.end method

.method private static B(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2772213
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2772214
    const-string v0, ""

    .line 2772215
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2772216
    iget-object v1, v0, LX/K5t;->b:LX/K5s;

    move-object v0, v1

    .line 2772217
    iget-object v0, v0, LX/K5s;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static C(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2772218
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/K74;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 2772219
    iget-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->F:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->E:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-lt v2, v0, :cond_1

    .line 2772220
    :cond_0
    :goto_0
    return-void

    .line 2772221
    :cond_1
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 2772222
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/K74;->a:LX/0Tn;

    invoke-interface {v0, v1, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 2772223
    cmp-long v3, v0, v6

    if-eqz v3, :cond_2

    sub-long v0, v4, v0

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 2772224
    :cond_2
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v3

    .line 2772225
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v4, v0

    .line 2772226
    new-instance v5, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;

    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v5, v3, v0, v1, v2}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment$DoNudgeRunnable;-><init>(Landroid/support/v4/view/ViewPager;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0So;I)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {v4, v5, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private a(LX/K6i;)V
    .locals 3

    .prologue
    .line 2772099
    if-nez p1, :cond_1

    .line 2772100
    :cond_0
    :goto_0
    return-void

    .line 2772101
    :cond_1
    iget v0, p1, LX/K6i;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2772102
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p1, LX/K6i;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v0, v1}, LX/CsS;->b(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    goto :goto_0

    .line 2772103
    :cond_2
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p1, LX/K6i;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    iget v2, p1, LX/K6i;->b:I

    invoke-virtual {v0, v1, v2}, LX/CsS;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V

    .line 2772104
    iget-boolean v0, p1, LX/K6i;->c:Z

    if-eqz v0, :cond_0

    .line 2772105
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget v1, p1, LX/K6i;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;Lcom/facebook/richdocument/view/carousel/PageableFragment;)V
    .locals 3

    .prologue
    .line 2772270
    if-nez p1, :cond_0

    .line 2772271
    :goto_0
    return-void

    .line 2772272
    :cond_0
    new-instance v0, LX/K6i;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, LX/K6i;-><init>(Lcom/facebook/richdocument/view/carousel/PageableFragment;IZ)V

    .line 2772273
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    if-nez v1, :cond_1

    .line 2772274
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2772275
    :cond_1
    invoke-direct {p0, v0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->a(LX/K6i;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v1, p1

    check-cast v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    invoke-static {v13}, LX/8bM;->a(LX/0QB;)LX/8bM;

    move-result-object v2

    check-cast v2, LX/8bM;

    invoke-static {v13}, LX/K7P;->a(LX/0QB;)LX/K7P;

    move-result-object v3

    check-cast v3, LX/K7P;

    invoke-static {v13}, LX/K6a;->a(LX/0QB;)LX/K6a;

    move-result-object v4

    check-cast v4, LX/K6a;

    invoke-static {v13}, LX/K8t;->a(LX/0QB;)LX/K8t;

    move-result-object v5

    check-cast v5, LX/K8t;

    invoke-static {v13}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v6

    check-cast v6, LX/Chv;

    const/16 v7, 0x35ef

    invoke-static {v13, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v13}, LX/K8w;->a(LX/0QB;)LX/K8w;

    move-result-object v8

    check-cast v8, LX/K8w;

    invoke-static {v13}, LX/K8s;->a(LX/0QB;)LX/K8s;

    move-result-object v9

    check-cast v9, LX/K8s;

    invoke-static {v13}, LX/K91;->a(LX/0QB;)LX/K91;

    move-result-object v10

    check-cast v10, LX/K91;

    const/16 v11, 0xf9a

    invoke-static {v13, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2db

    invoke-static {v13, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 p0, 0x35ec

    invoke-static {v13, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    iput-object v2, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->m:LX/8bM;

    iput-object v3, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->n:LX/K7P;

    iput-object v4, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    iput-object v5, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->p:LX/K8t;

    iput-object v6, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->q:LX/Chv;

    iput-object v7, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    iput-object v8, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->s:LX/K8w;

    iput-object v9, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iput-object v10, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->u:LX/K91;

    iput-object v11, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->v:LX/0Ot;

    iput-object v12, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->w:LX/0Ot;

    iput-object v13, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->x:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2772227
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1}, LX/CsS;->getActiveFragmentIndex()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2772228
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2772229
    const/4 v0, 0x1

    .line 2772230
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/K5t;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x7d4

    const v3, -0x11b13572    # -1.5999696E28f

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2772231
    new-instance v7, LX/K7D;

    const-string v0, "carddeck_endcard"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, LX/K7D;-><init>(Ljava/lang/String;)V

    .line 2772232
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083a94

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f083a95

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2772233
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 2772234
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LX/K5t;

    .line 2772235
    new-instance v0, Lcom/facebook/tarot/data/FeedbackData;

    iget-object v1, v2, LX/K5t;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, v2, LX/K5t;->c:Ljava/lang/String;

    const-string v5, "chromeless:content:fragment:tag"

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tarot/data/FeedbackData;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 2772236
    :goto_0
    new-instance v1, LX/K7F;

    const-string v2, "endcard"

    invoke-direct {v1, v2}, LX/K7F;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a9a

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2772237
    iput-object v2, v1, LX/K7F;->c:Ljava/lang/String;

    .line 2772238
    move-object v1, v1

    .line 2772239
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a9b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->u:LX/K91;

    .line 2772240
    iget-object v6, v5, LX/K91;->e:Ljava/util/Map;

    sget-object p1, LX/K8y;->PRODUCT_NAME:LX/K8y;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/K8z;

    invoke-virtual {v6}, LX/K8z;->a()Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2772241
    aput-object v5, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2772242
    iput-object v2, v1, LX/K7F;->d:Ljava/lang/String;

    .line 2772243
    move-object v1, v1

    .line 2772244
    invoke-static {p0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->A(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2772245
    iput-object v2, v1, LX/K7F;->b:Ljava/lang/String;

    .line 2772246
    move-object v1, v1

    .line 2772247
    iput-object v0, v1, LX/K7F;->e:Lcom/facebook/tarot/data/FeedbackData;

    .line 2772248
    move-object v1, v1

    .line 2772249
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2772250
    iget-object v2, v0, LX/K5t;->b:LX/K5s;

    move-object v0, v2

    .line 2772251
    iget-object v0, v0, LX/K5s;->a:Ljava/lang/String;

    .line 2772252
    iput-object v0, v1, LX/K7F;->f:Ljava/lang/String;

    .line 2772253
    move-object v1, v1

    .line 2772254
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2772255
    iget-object v2, v0, LX/K5t;->b:LX/K5s;

    move-object v0, v2

    .line 2772256
    iget-object v0, v0, LX/K5s;->b:Ljava/lang/String;

    .line 2772257
    iput-object v0, v1, LX/K7F;->g:Ljava/lang/String;

    .line 2772258
    move-object v0, v1

    .line 2772259
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->g:LX/K73;

    .line 2772260
    iput-object v1, v0, LX/K7F;->h:LX/K73;

    .line 2772261
    move-object v0, v0

    .line 2772262
    invoke-virtual {v0}, LX/K7F;->a()Lcom/facebook/tarot/data/TarotCardEndCardData;

    move-result-object v0

    .line 2772263
    invoke-virtual {v7, v0}, LX/K7D;->a(Lcom/facebook/tarot/data/BaseTarotCardData;)V

    .line 2772264
    invoke-static {v7, v10}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->a(LX/K7D;Landroid/os/Bundle;)Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->a(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    .line 2772265
    iput-boolean v9, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->F:Z

    .line 2772266
    invoke-static {p0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V

    .line 2772267
    return-void

    .line 2772268
    :cond_0
    new-instance v0, Lcom/facebook/tarot/data/FeedbackData;

    const-string v5, "chromeless:content:fragment:tag"

    move-object v1, v10

    move-object v2, v10

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tarot/data/FeedbackData;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final eK_()I
    .locals 1

    .prologue
    .line 2772269
    const v0, 0x7f0e0c3c

    return v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 2

    .prologue
    .line 2772186
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->I:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2772187
    new-instance v0, LX/ChJ;

    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/ChJ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->I:Landroid/content/Context;

    .line 2772188
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->I:Landroid/content/Context;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2772189
    const v0, 0x7f031486

    return v0
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 2772183
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->H:LX/8qU;

    if-nez v0, :cond_0

    .line 2772184
    new-instance v0, LX/K6h;

    invoke-direct {v0, p0}, LX/K6h;-><init>(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->H:LX/8qU;

    .line 2772185
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->H:LX/8qU;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2772175
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2772176
    packed-switch p1, :pswitch_data_0

    .line 2772177
    :goto_0
    return-void

    .line 2772178
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v2}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    sget-object v3, LX/K8u;->DIGEST_COVER:LX/K8u;

    invoke-virtual {v0, v1, v2, v3}, LX/K8s;->a(Ljava/lang/String;Ljava/util/Map;LX/K8u;)V

    goto :goto_0

    .line 2772179
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v2}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    sget-object v3, LX/K8u;->END_CARD:LX/K8u;

    invoke-virtual {v0, v1, v2, v3}, LX/K8s;->a(Ljava/lang/String;Ljava/util/Map;LX/K8u;)V

    goto :goto_0

    .line 2772180
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v2}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/K8s;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2772181
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v2}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/K8s;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2772182
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v2}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    sget-object v3, LX/K8u;->STORY_CARD:LX/K8u;

    invoke-virtual {v0, v1, v2, v3}, LX/K8s;->a(Ljava/lang/String;Ljava/util/Map;LX/K8u;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x48543d85

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2772168
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2772169
    const-class v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2772170
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v1}, LX/K6a;->a()V

    .line 2772171
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->p:LX/K8t;

    .line 2772172
    iget-object v2, v1, LX/K8t;->i:LX/0if;

    sget-object p1, LX/0ig;->aO:LX/0ih;

    invoke-virtual {v2, p1}, LX/0if;->a(LX/0ih;)V

    .line 2772173
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->D:Z

    .line 2772174
    const/16 v1, 0x2b

    const v2, 0x709938b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x5ffd2eb8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2772135
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    .line 2772136
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->m:LX/8bM;

    if-eqz v1, :cond_0

    .line 2772137
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->m:LX/8bM;

    invoke-virtual {v1, v0}, LX/8bM;->a(Landroid/app/Activity;)V

    .line 2772138
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 2772139
    const v0, 0x7f0d0807

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2772140
    const v1, 0x7f031483

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2772141
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    iget-object v4, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    .line 2772142
    iput-object v4, v1, LX/K6a;->h:LX/K79;

    .line 2772143
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->s:LX/K8w;

    .line 2772144
    iget-object v4, v1, LX/K8w;->a:LX/1CD;

    invoke-virtual {v4}, LX/1CD;->a()LX/45B;

    move-result-object v4

    sget-object v5, LX/45B;->CONNECTED:LX/45B;

    if-ne v4, v5, :cond_4

    const/4 v4, 0x1

    .line 2772145
    :goto_0
    iput-boolean v4, v1, LX/K8w;->b:Z

    .line 2772146
    const v1, 0x7f0d2ea9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iput-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    .line 2772147
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    new-instance v4, LX/K6j;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v5

    invoke-direct {v4, v5}, LX/K6j;-><init>(LX/0gc;)V

    invoke-virtual {v1, v4}, LX/CsS;->setPagerAdapter(LX/CsQ;)V

    .line 2772148
    new-instance v1, LX/Gmd;

    invoke-direct {v1}, LX/Gmd;-><init>()V

    iput-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B:LX/Gmd;

    .line 2772149
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B:LX/Gmd;

    iget-object v4, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1, v4}, LX/Gmd;->setFragmentPager(LX/CqD;)V

    .line 2772150
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v4, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B:LX/Gmd;

    invoke-virtual {v1, v4}, LX/CsS;->a(LX/0hc;)V

    .line 2772151
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B:LX/Gmd;

    iget-object v4, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->q:LX/Chv;

    invoke-virtual {v1, v4}, LX/Gmd;->a(LX/0b4;)V

    .line 2772152
    const v1, 0x7f0d2ead

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->A:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    .line 2772153
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2772154
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K6i;

    .line 2772155
    invoke-direct {p0, v0}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->a(LX/K6i;)V

    goto :goto_1

    .line 2772156
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2772157
    iget-object v4, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    invoke-virtual {v4, v0}, LX/K6a;->a(LX/K5t;)V

    .line 2772158
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2772159
    :cond_2
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2772160
    :cond_3
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->J:LX/K6b;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2772161
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->n:LX/K7P;

    .line 2772162
    iget-object v1, v0, LX/K7P;->a:LX/K87;

    iget-object v4, v0, LX/K7P;->b:LX/K6k;

    invoke-virtual {v1, v4}, LX/0b4;->a(LX/0b2;)Z

    .line 2772163
    iget-object v1, v0, LX/K7P;->a:LX/K87;

    iget-object v4, v0, LX/K7P;->c:LX/K7N;

    invoke-virtual {v1, v4}, LX/0b4;->a(LX/0b2;)Z

    .line 2772164
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->K:LX/K6d;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2772165
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->L:LX/K6f;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2772166
    const v0, -0x3970fb84

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 2772167
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 2772111
    iget-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->D:Z

    if-nez v0, :cond_0

    .line 2772112
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2772113
    return-void

    .line 2772114
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->s:LX/K8w;

    .line 2772115
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/K8w;->b:Z

    .line 2772116
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->p:LX/K8t;

    .line 2772117
    iget-object v1, v0, LX/K8t;->i:LX/0if;

    sget-object v2, LX/0ig;->aO:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2772118
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->J:LX/K6b;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2772119
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->K:LX/K6d;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2772120
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K87;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->L:LX/K6f;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2772121
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B:LX/Gmd;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->q:LX/Chv;

    invoke-virtual {v0, v1}, LX/Gmd;->b(LX/0b4;)V

    .line 2772122
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->B:LX/Gmd;

    invoke-virtual {v0, v1}, LX/CsS;->b(LX/0hc;)V

    .line 2772123
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->n:LX/K7P;

    .line 2772124
    iget-object v1, v0, LX/K7P;->a:LX/K87;

    iget-object v2, v0, LX/K7P;->b:LX/K6k;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772125
    iget-object v1, v0, LX/K7P;->a:LX/K87;

    iget-object v2, v0, LX/K7P;->c:LX/K7N;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772126
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    if-eqz v0, :cond_1

    .line 2772127
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v1, v1, LX/K79;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v2}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v2

    .line 2772128
    const-string v3, "tarot_close"

    invoke-static {v0, v3, v1, v2}, LX/K8s;->a(LX/K8s;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2772129
    iget-object v3, v0, LX/K8s;->c:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772130
    iget-object v3, v0, LX/K8s;->d:Ljava/util/Stack;

    invoke-virtual {v3, v1}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 2772131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    .line 2772132
    :cond_1
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K7A;

    .line 2772133
    const/4 v1, 0x0

    iput-object v1, v0, LX/K7A;->a:LX/K79;

    .line 2772134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->D:Z

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3df1cad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2772106
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v2}, LX/CsS;->getActiveFragmentIndex()I

    move-result v2

    invoke-virtual {v0, v2}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2772107
    if-eqz v0, :cond_0

    .line 2772108
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->o()V

    .line 2772109
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onPause()V

    .line 2772110
    const/16 v0, 0x2b

    const v2, 0x558b9d6

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x784e3927

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2772094
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onResume()V

    .line 2772095
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v2}, LX/CsS;->getActiveFragmentIndex()I

    move-result v2

    invoke-virtual {v0, v2}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2772096
    if-eqz v0, :cond_0

    .line 2772097
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->n()V

    .line 2772098
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x7cf19cd6

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 2772093
    sget v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->m:I

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2772092
    sget v0, Lcom/facebook/tarot/cards/EmbeddedPopoverFragment;->m:I

    return v0
.end method

.method public final w()D
    .locals 2

    .prologue
    .line 2772091
    const-wide v0, 0x3feccccccccccccdL    # 0.9

    return-wide v0
.end method
