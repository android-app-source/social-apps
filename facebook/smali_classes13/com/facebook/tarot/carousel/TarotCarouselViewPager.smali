.class public Lcom/facebook/tarot/carousel/TarotCarouselViewPager;
.super LX/Hi9;
.source ""

# interfaces
.implements LX/Che;


# instance fields
.field public a:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2772347
    invoke-direct {p0, p1}, LX/Hi9;-><init>(Landroid/content/Context;)V

    .line 2772348
    invoke-direct {p0}, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->g()V

    .line 2772349
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2772344
    invoke-direct {p0, p1, p2}, LX/Hi9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2772345
    invoke-direct {p0}, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->g()V

    .line 2772346
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v0

    check-cast v0, LX/Crz;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->a:LX/Crz;

    return-void
.end method

.method public static a(Landroid/view/View;III)Z
    .locals 1

    .prologue
    .line 2772350
    check-cast p0, Landroid/view/ViewGroup;

    if-lez p1, :cond_0

    sget-object v0, LX/31M;->RIGHT:LX/31M;

    :goto_0
    invoke-static {p0, v0, p2, p3}, LX/3BA;->a(Landroid/view/ViewGroup;LX/31M;II)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, LX/31M;->LEFT:LX/31M;

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 2772341
    const-class v0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;

    invoke-static {v0, p0}, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2772342
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->a:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->c:Z

    .line 2772343
    return-void
.end method


# virtual methods
.method public final a(LX/31M;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2772330
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2772331
    :cond_0
    :goto_0
    return v1

    .line 2772332
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->c:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2772333
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->c:Z

    if-eqz v2, :cond_3

    move v2, v1

    .line 2772334
    :goto_2
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    .line 2772335
    sget-object v5, LX/31M;->RIGHT:LX/31M;

    if-ne p1, v5, :cond_5

    .line 2772336
    iget-boolean v2, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->c:Z

    if-eqz v2, :cond_4

    if-ge v4, v0, :cond_0

    move v1, v3

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2772337
    goto :goto_1

    .line 2772338
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 2772339
    :cond_4
    if-le v4, v0, :cond_0

    move v1, v3

    goto :goto_0

    .line 2772340
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->c:Z

    if-eqz v0, :cond_6

    if-le v4, v2, :cond_0

    move v1, v3

    goto :goto_0

    :cond_6
    if-ge v4, v2, :cond_0

    move v1, v3

    goto :goto_0
.end method

.method public getFragmentPager()LX/CqD;
    .locals 1

    .prologue
    .line 2772329
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->b:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2772322
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->b:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2772323
    invoke-super {p0, p1}, LX/Hi9;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2772324
    :goto_0
    return v0

    .line 2772325
    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->b:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->b:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1}, LX/CsS;->getActiveFragmentIndex()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2772326
    if-eqz v0, :cond_1

    .line 2772327
    goto :goto_1

    .line 2772328
    :cond_1
    :goto_1
    invoke-super {p0, p1}, LX/Hi9;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setFragmentPager(LX/CqD;)V
    .locals 1

    .prologue
    .line 2772319
    instance-of v0, p1, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2772320
    :goto_0
    return-void

    .line 2772321
    :cond_0
    check-cast p1, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    iput-object p1, p0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->b:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    goto :goto_0
.end method
