.class public Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;
.super LX/CsS;
.source ""

# interfaces
.implements LX/CtT;
.implements LX/K5f;


# instance fields
.field private final A:LX/K6u;

.field private final B:LX/K6w;

.field private final C:LX/K6d;

.field public h:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/K7m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/K87;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/K6a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:I

.field private m:I

.field private n:I

.field private o:LX/K70;

.field private p:Z

.field private q:Z

.field private r:LX/K94;

.field private final s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/K6z;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

.field private u:LX/K71;

.field private final v:LX/K6k;

.field private final w:LX/K6m;

.field private final x:LX/K6o;

.field private final y:LX/K6q;

.field private final z:LX/K6s;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2772532
    invoke-direct {p0, p1}, LX/CsS;-><init>(Landroid/content/Context;)V

    .line 2772533
    sget-object v0, LX/K70;->WAITING_FOR_DOWN:LX/K70;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    .line 2772534
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    .line 2772535
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    .line 2772536
    new-instance v0, LX/K71;

    invoke-direct {v0, p0}, LX/K71;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->u:LX/K71;

    .line 2772537
    new-instance v0, LX/K6l;

    invoke-direct {v0, p0}, LX/K6l;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->v:LX/K6k;

    .line 2772538
    new-instance v0, LX/K6n;

    invoke-direct {v0, p0}, LX/K6n;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->w:LX/K6m;

    .line 2772539
    new-instance v0, LX/K6p;

    invoke-direct {v0, p0}, LX/K6p;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->x:LX/K6o;

    .line 2772540
    new-instance v0, LX/K6r;

    invoke-direct {v0, p0}, LX/K6r;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->y:LX/K6q;

    .line 2772541
    new-instance v0, LX/K6t;

    invoke-direct {v0, p0}, LX/K6t;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->z:LX/K6s;

    .line 2772542
    new-instance v0, LX/K6v;

    invoke-direct {v0, p0}, LX/K6v;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->A:LX/K6u;

    .line 2772543
    new-instance v0, LX/K6x;

    invoke-direct {v0, p0}, LX/K6x;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->B:LX/K6w;

    .line 2772544
    new-instance v0, LX/K6y;

    invoke-direct {v0, p0}, LX/K6y;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->C:LX/K6d;

    .line 2772545
    invoke-direct {p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a()V

    .line 2772546
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2772569
    invoke-direct {p0, p1, p2}, LX/CsS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2772570
    sget-object v0, LX/K70;->WAITING_FOR_DOWN:LX/K70;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    .line 2772571
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    .line 2772572
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    .line 2772573
    new-instance v0, LX/K71;

    invoke-direct {v0, p0}, LX/K71;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->u:LX/K71;

    .line 2772574
    new-instance v0, LX/K6l;

    invoke-direct {v0, p0}, LX/K6l;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->v:LX/K6k;

    .line 2772575
    new-instance v0, LX/K6n;

    invoke-direct {v0, p0}, LX/K6n;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->w:LX/K6m;

    .line 2772576
    new-instance v0, LX/K6p;

    invoke-direct {v0, p0}, LX/K6p;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->x:LX/K6o;

    .line 2772577
    new-instance v0, LX/K6r;

    invoke-direct {v0, p0}, LX/K6r;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->y:LX/K6q;

    .line 2772578
    new-instance v0, LX/K6t;

    invoke-direct {v0, p0}, LX/K6t;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->z:LX/K6s;

    .line 2772579
    new-instance v0, LX/K6v;

    invoke-direct {v0, p0}, LX/K6v;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->A:LX/K6u;

    .line 2772580
    new-instance v0, LX/K6x;

    invoke-direct {v0, p0}, LX/K6x;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->B:LX/K6w;

    .line 2772581
    new-instance v0, LX/K6y;

    invoke-direct {v0, p0}, LX/K6y;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->C:LX/K6d;

    .line 2772582
    invoke-direct {p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a()V

    .line 2772583
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2772554
    invoke-direct {p0, p1, p2, p3}, LX/CsS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2772555
    sget-object v0, LX/K70;->WAITING_FOR_DOWN:LX/K70;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    .line 2772556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    .line 2772557
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    .line 2772558
    new-instance v0, LX/K71;

    invoke-direct {v0, p0}, LX/K71;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->u:LX/K71;

    .line 2772559
    new-instance v0, LX/K6l;

    invoke-direct {v0, p0}, LX/K6l;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->v:LX/K6k;

    .line 2772560
    new-instance v0, LX/K6n;

    invoke-direct {v0, p0}, LX/K6n;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->w:LX/K6m;

    .line 2772561
    new-instance v0, LX/K6p;

    invoke-direct {v0, p0}, LX/K6p;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->x:LX/K6o;

    .line 2772562
    new-instance v0, LX/K6r;

    invoke-direct {v0, p0}, LX/K6r;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->y:LX/K6q;

    .line 2772563
    new-instance v0, LX/K6t;

    invoke-direct {v0, p0}, LX/K6t;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->z:LX/K6s;

    .line 2772564
    new-instance v0, LX/K6v;

    invoke-direct {v0, p0}, LX/K6v;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->A:LX/K6u;

    .line 2772565
    new-instance v0, LX/K6x;

    invoke-direct {v0, p0}, LX/K6x;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->B:LX/K6w;

    .line 2772566
    new-instance v0, LX/K6y;

    invoke-direct {v0, p0}, LX/K6y;-><init>(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->C:LX/K6d;

    .line 2772567
    invoke-direct {p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a()V

    .line 2772568
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2772548
    const-class v0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-static {v0, p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2772549
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->h:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->q:Z

    .line 2772550
    invoke-virtual {p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->l:I

    .line 2772551
    invoke-virtual {p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->n:I

    .line 2772552
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2772553
    return-void
.end method

.method private static a(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;LX/Crz;LX/K7m;LX/K87;LX/K6a;)V
    .locals 0

    .prologue
    .line 2772547
    iput-object p1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->h:LX/Crz;

    iput-object p2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iput-object p3, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iput-object p4, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->k:LX/K6a;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-static {v3}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v0

    check-cast v0, LX/Crz;

    invoke-static {v3}, LX/K7m;->a(LX/0QB;)LX/K7m;

    move-result-object v1

    check-cast v1, LX/K7m;

    invoke-static {v3}, LX/K87;->a(LX/0QB;)LX/K87;

    move-result-object v2

    check-cast v2, LX/K87;

    invoke-static {v3}, LX/K6a;->a(LX/0QB;)LX/K6a;

    move-result-object v3

    check-cast v3, LX/K6a;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;LX/Crz;LX/K7m;LX/K87;LX/K6a;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;LX/K7s;)V
    .locals 6

    .prologue
    .line 2772520
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->k:LX/K6a;

    iget-object v2, p1, LX/K7s;->a:Ljava/lang/String;

    const/4 v5, 0x0

    .line 2772521
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2772522
    :cond_0
    :goto_0
    move v1, v5

    .line 2772523
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2772524
    return-void

    :cond_1
    move v4, v5

    move p0, v5

    .line 2772525
    :goto_1
    iget-object v3, v1, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_0

    .line 2772526
    iget-object v3, v1, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/K5t;

    .line 2772527
    iget-object p1, v3, LX/K5t;->b:LX/K5s;

    move-object v3, p1

    .line 2772528
    iget-object v3, v3, LX/K5s;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v5, p0

    .line 2772529
    goto :goto_0

    .line 2772530
    :cond_2
    iget-object v3, v1, LX/K6a;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/K5t;

    invoke-virtual {v3}, LX/K5t;->b()I

    move-result v3

    add-int/2addr p0, v3

    .line 2772531
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1
.end method

.method public static b(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V
    .locals 1

    .prologue
    .line 2772518
    sget-object v0, LX/K6z;->DRAWER:LX/K6z;

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a(LX/K6z;)V

    .line 2772519
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2772517
    iget-boolean v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->q:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->n:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->n:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static c(Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;)V
    .locals 1

    .prologue
    .line 2772515
    sget-object v0, LX/K6z;->DRAWER:LX/K6z;

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->b(LX/K6z;)V

    .line 2772516
    return-void
.end method


# virtual methods
.method public final a(IIZ)V
    .locals 1

    .prologue
    .line 2772584
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->t:Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->a(IIZ)V

    .line 2772585
    return-void
.end method

.method public final a(LX/K6z;)V
    .locals 2

    .prologue
    .line 2772428
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2772429
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->r:LX/K94;

    const/4 p1, 0x1

    .line 2772430
    iget v1, v0, LX/K94;->c:I

    if-ne v1, p1, :cond_0

    .line 2772431
    :goto_0
    return-void

    .line 2772432
    :cond_0
    iget-object v1, v0, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2772433
    iget-object v1, v0, LX/K94;->b:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2772434
    iget-object v1, v0, LX/K94;->b:Landroid/view/View;

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {v1, p0}, Landroid/view/View;->setAlpha(F)V

    .line 2772435
    iput p1, v0, LX/K94;->c:I

    .line 2772436
    iget-object v1, v0, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_0
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 2772437
    invoke-virtual {p0}, LX/CsS;->getActiveFragmentIndex()I

    move-result v0

    invoke-virtual {p0, v0}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    .line 2772438
    if-nez v0, :cond_0

    .line 2772439
    const/4 v0, 0x1

    .line 2772440
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->a(FFLX/31M;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2772441
    invoke-virtual {p0}, LX/CsS;->getActiveFragmentIndex()I

    move-result v0

    .line 2772442
    invoke-direct {p0, p1}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 2772443
    iget-boolean v4, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    move v0, v2

    .line 2772444
    :goto_0
    return v0

    .line 2772445
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 2772446
    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v3

    .line 2772447
    goto :goto_0

    .line 2772448
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->m:I

    .line 2772449
    sget-object v0, LX/K70;->WAITING_FOR_CONSIDERABLE_MOVE:LX/K70;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    .line 2772450
    iput-boolean v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    move v0, v3

    .line 2772451
    goto :goto_0

    .line 2772452
    :pswitch_1
    iget-boolean v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    if-nez v0, :cond_1

    .line 2772453
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 2772454
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    .line 2772455
    iget v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->m:I

    sub-int v6, v4, v0

    .line 2772456
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    sget-object v1, LX/K70;->WAITING_FOR_CONSIDERABLE_MOVE:LX/K70;

    if-ne v0, v1, :cond_2

    .line 2772457
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->l:I

    if-le v0, v1, :cond_2

    .line 2772458
    sget-object v0, LX/K70;->SILENTLY_WATCHING_MOVE_EVENTS:LX/K70;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    .line 2772459
    :cond_2
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    sget-object v1, LX/K70;->SILENTLY_WATCHING_MOVE_EVENTS:LX/K70;

    if-ne v0, v1, :cond_1

    .line 2772460
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    check-cast v0, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;

    .line 2772461
    if-gez v6, :cond_4

    sget-object v1, LX/31M;->LEFT:LX/31M;

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->a(LX/31M;)Z

    move-result v1

    .line 2772462
    invoke-static {v0, v6, v4, v5}, Lcom/facebook/tarot/carousel/TarotCarouselViewPager;->a(Landroid/view/View;III)Z

    move-result v0

    .line 2772463
    if-nez v1, :cond_3

    if-eqz v0, :cond_5

    :cond_3
    move v0, v2

    goto :goto_0

    .line 2772464
    :cond_4
    sget-object v1, LX/31M;->RIGHT:LX/31M;

    goto :goto_2

    :cond_5
    move v0, v3

    .line 2772465
    goto :goto_0

    .line 2772466
    :pswitch_2
    sget-object v0, LX/K70;->WAITING_FOR_DOWN:LX/K70;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->o:LX/K70;

    .line 2772467
    iput-boolean v3, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->p:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(LX/K6z;)V
    .locals 2

    .prologue
    .line 2772468
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2772469
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2772470
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->r:LX/K94;

    const/4 p1, 0x0

    .line 2772471
    iget v1, v0, LX/K94;->c:I

    if-nez v1, :cond_1

    .line 2772472
    :cond_0
    :goto_0
    return-void

    .line 2772473
    :cond_1
    iget-object v1, v0, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2772474
    iget-object v1, v0, LX/K94;->b:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2772475
    iget-object v1, v0, LX/K94;->b:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setAlpha(F)V

    .line 2772476
    iput p1, v0, LX/K94;->c:I

    .line 2772477
    iget-object v1, v0, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public getHeaderResourceId()I
    .locals 1

    .prologue
    .line 2772478
    const v0, 0x7f0d2eac

    return v0
.end method

.method public getPageIndicatorResourceId()I
    .locals 1

    .prologue
    .line 2772479
    const/4 v0, -0x1

    return v0
.end method

.method public getViewPagerResourceId()I
    .locals 1

    .prologue
    .line 2772480
    const v0, 0x7f0d2eab

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x209ec3b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2772481
    invoke-super {p0}, LX/CsS;->onAttachedToWindow()V

    .line 2772482
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->u:LX/K71;

    invoke-virtual {p0, v1}, LX/CsS;->a(LX/0hc;)V

    .line 2772483
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->x:LX/K6o;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772484
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->y:LX/K6q;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772485
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->z:LX/K6s;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772486
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->A:LX/K6u;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772487
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->B:LX/K6w;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772488
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->C:LX/K6d;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772489
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->v:LX/K6k;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772490
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->w:LX/K6m;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2772491
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->r:LX/K94;

    .line 2772492
    iget-object v2, v1, LX/K94;->a:Landroid/animation/ValueAnimator;

    new-instance p0, LX/K92;

    invoke-direct {p0, v1}, LX/K92;-><init>(LX/K94;)V

    invoke-virtual {v2, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2772493
    iget-object v2, v1, LX/K94;->a:Landroid/animation/ValueAnimator;

    new-instance p0, LX/K93;

    invoke-direct {p0, v1}, LX/K93;-><init>(LX/K94;)V

    invoke-virtual {v2, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2772494
    const/16 v1, 0x2d

    const v2, 0x5e840e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x729044d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2772495
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->u:LX/K71;

    invoke-virtual {p0, v1}, LX/CsS;->b(LX/0hc;)V

    .line 2772496
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->x:LX/K6o;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772497
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->y:LX/K6q;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772498
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->i:LX/K7m;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->z:LX/K6s;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772499
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->A:LX/K6u;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772500
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->B:LX/K6w;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772501
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->C:LX/K6d;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772502
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->v:LX/K6k;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772503
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->j:LX/K87;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->w:LX/K6m;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2772504
    iget-object v1, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->r:LX/K94;

    .line 2772505
    iget-object v2, v1, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 2772506
    iget-object v2, v1, LX/K94;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 2772507
    invoke-super {p0}, LX/CsS;->onDetachedFromWindow()V

    .line 2772508
    const/16 v1, 0x2d

    const v2, -0x90932bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x121cff56

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2772509
    invoke-super {p0}, LX/CsS;->onFinishInflate()V

    .line 2772510
    invoke-virtual {p0}, LX/CsS;->getHeader()LX/CsR;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->t:Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    .line 2772511
    new-instance v0, LX/K94;

    iget-object v2, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->t:Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    invoke-direct {v0, v2}, LX/K94;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->r:LX/K94;

    .line 2772512
    const/16 v0, 0x2d

    const v2, 0x37d8ec3e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setPublisherName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2772513
    iget-object v0, p0, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->t:Lcom/facebook/tarot/drawer/TarotCollapsingHeader;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/drawer/TarotCollapsingHeader;->setPublisherName(Ljava/lang/String;)V

    .line 2772514
    return-void
.end method
