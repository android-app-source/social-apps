.class public Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/view/DraweeView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/facebook/widget/SwitchCompat;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2776851
    const-class v0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2776848
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2776849
    invoke-direct {p0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->b()V

    .line 2776850
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2776823
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2776824
    invoke-direct {p0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->b()V

    .line 2776825
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2776852
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2776853
    invoke-direct {p0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->b()V

    .line 2776854
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->a:LX/1Ad;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2776842
    const-class v0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    invoke-static {v0, p0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2776843
    const v0, 0x7f031412

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2776844
    const v0, 0x7f0d2e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->c:Lcom/facebook/drawee/view/DraweeView;

    .line 2776845
    const v0, 0x7f0d1802

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->d:Landroid/widget/TextView;

    .line 2776846
    const v0, 0x7f0d2e17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->e:Lcom/facebook/widget/SwitchCompat;

    .line 2776847
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2776837
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->c:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2776838
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->d:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2776839
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2776840
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->e:Lcom/facebook/widget/SwitchCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2776841
    return-void
.end method

.method public setLogoUri(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2776832
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2776833
    :goto_0
    return-void

    .line 2776834
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2776835
    iget-object v1, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->a:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2776836
    iget-object v1, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->c:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2776830
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2776831
    return-void
.end method

.method public setSwitchEnabled(Z)V
    .locals 1

    .prologue
    .line 2776828
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2776829
    return-void
.end method

.method public setSwitchStateChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 2776826
    iget-object v0, p0, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2776827
    return-void
.end method
