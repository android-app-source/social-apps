.class public Lcom/facebook/tarot/view/CheckableButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Landroid/widget/TextView;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field public g:LX/K68;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2776668
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2776669
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    .line 2776670
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/tarot/view/CheckableButton;->a(Landroid/util/AttributeSet;)V

    .line 2776671
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2776664
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2776665
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    .line 2776666
    invoke-direct {p0, p2}, Lcom/facebook/tarot/view/CheckableButton;->a(Landroid/util/AttributeSet;)V

    .line 2776667
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2776660
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2776661
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    .line 2776662
    invoke-direct {p0, p2}, Lcom/facebook/tarot/view/CheckableButton;->a(Landroid/util/AttributeSet;)V

    .line 2776663
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2776653
    iget-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/tarot/view/CheckableButton;->d:Ljava/lang/String;

    .line 2776654
    :goto_0
    if-eqz v0, :cond_0

    .line 2776655
    iget-object v1, p0, Lcom/facebook/tarot/view/CheckableButton;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2776656
    :cond_0
    iget-object v1, p0, Lcom/facebook/tarot/view/CheckableButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2776657
    return-void

    .line 2776658
    :cond_1
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckableButton;->e:Ljava/lang/String;

    goto :goto_0

    .line 2776659
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    .line 2776632
    const v0, 0x7f030283

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2776633
    const v0, 0x7f0d092a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckableButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2776634
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckableButton;->b:Landroid/widget/TextView;

    .line 2776635
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/view/CheckableButton;->setDescendantFocusability(I)V

    .line 2776636
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/tarot/view/CheckableButton;->setClickable(Z)V

    .line 2776637
    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckableButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->CheckableButton:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2776638
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    .line 2776639
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    .line 2776640
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 2776641
    const/16 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2776642
    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckableButton;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x2

    invoke-static {v3, v1, v4}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/tarot/view/CheckableButton;->d:Ljava/lang/String;

    .line 2776643
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2776644
    :cond_1
    const/16 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 2776645
    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckableButton;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x3

    invoke-static {v3, v1, v4}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/tarot/view/CheckableButton;->e:Ljava/lang/String;

    goto :goto_1

    .line 2776646
    :cond_2
    const/16 v4, 0x0

    if-ne v3, v4, :cond_3

    .line 2776647
    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckableButton;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x0

    invoke-static {v3, v1, v4}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/tarot/view/CheckableButton;->e:Ljava/lang/String;

    iput-object v3, p0, Lcom/facebook/tarot/view/CheckableButton;->d:Ljava/lang/String;

    goto :goto_1

    .line 2776648
    :cond_3
    const/16 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 2776649
    const/16 v3, 0x1

    iget-boolean v4, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/facebook/tarot/view/CheckableButton;->setChecked(Z)V

    goto :goto_1

    .line 2776650
    :cond_4
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2776651
    invoke-direct {p0}, Lcom/facebook/tarot/view/CheckableButton;->a()V

    .line 2776652
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2776631
    iget-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 2776629
    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckableButton;->toggle()V

    .line 2776630
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2776615
    iget-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    if-eq v0, p1, :cond_0

    .line 2776616
    iput-boolean p1, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    .line 2776617
    iget-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->f:Z

    if-eqz v0, :cond_1

    .line 2776618
    :cond_0
    :goto_0
    return-void

    .line 2776619
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->f:Z

    .line 2776620
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckableButton;->g:LX/K68;

    if-eqz v0, :cond_2

    .line 2776621
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckableButton;->g:LX/K68;

    invoke-interface {v0, p1}, LX/K68;->a(Z)V

    .line 2776622
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->f:Z

    .line 2776623
    invoke-direct {p0}, Lcom/facebook/tarot/view/CheckableButton;->a()V

    goto :goto_0
.end method

.method public setOnCheckChangedListener(LX/K68;)V
    .locals 0

    .prologue
    .line 2776627
    iput-object p1, p0, Lcom/facebook/tarot/view/CheckableButton;->g:LX/K68;

    .line 2776628
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2776624
    iget-boolean v0, p0, Lcom/facebook/tarot/view/CheckableButton;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/tarot/view/CheckableButton;->setChecked(Z)V

    .line 2776625
    return-void

    .line 2776626
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
