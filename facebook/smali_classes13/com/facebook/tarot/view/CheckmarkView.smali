.class public Lcom/facebook/tarot/view/CheckmarkView;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/RectF;

.field public b:LX/1FZ;

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Paint;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2776672
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2776673
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    .line 2776674
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/tarot/view/CheckmarkView;->a(Landroid/util/AttributeSet;I)V

    .line 2776675
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2776676
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2776677
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    .line 2776678
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/tarot/view/CheckmarkView;->a(Landroid/util/AttributeSet;I)V

    .line 2776679
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2776680
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2776681
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    .line 2776682
    invoke-direct {p0, p2, p3}, Lcom/facebook/tarot/view/CheckmarkView;->a(Landroid/util/AttributeSet;I)V

    .line 2776683
    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    .line 2776684
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->b:LX/1FZ;

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getMeasuredHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v2, v3}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->c:Landroid/graphics/Bitmap;

    .line 2776685
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->c:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2776686
    iget v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->e:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 2776687
    iget-object v2, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v1

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v1

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    sub-float/2addr v5, v1

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    sub-float v1, v6, v1

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2776688
    iget-object v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    .line 2776689
    iget-object v2, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    .line 2776690
    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    const-wide v6, 0x3fe6666666666666L    # 0.7

    float-to-double v8, v2

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-float v2, v6

    add-float/2addr v2, v5

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2776691
    new-instance v2, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    const-wide v6, 0x3fd999999999999aL    # 0.4

    float-to-double v8, v1

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-float v1, v6

    add-float/2addr v1, v4

    iget-object v4, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v2, v1, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2776692
    new-instance v1, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/facebook/tarot/view/CheckmarkView;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2776693
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 2776694
    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v5, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2776695
    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2776696
    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2776697
    iget-object v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2776698
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 2776699
    const-class v0, Lcom/facebook/tarot/view/CheckmarkView;

    invoke-static {v0, p0}, Lcom/facebook/tarot/view/CheckmarkView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2776700
    invoke-virtual {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2776701
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b261d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 2776702
    sget-object v2, LX/03r;->CheckmarkView:[I

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2776703
    const/16 v2, 0x0

    float-to-int v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->e:I

    .line 2776704
    const/16 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->f:I

    .line 2776705
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2776706
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->d:Landroid/graphics/Paint;

    .line 2776707
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2776708
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/tarot/view/CheckmarkView;->e:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2776709
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2776710
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tarot/view/CheckmarkView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tarot/view/CheckmarkView;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v0

    check-cast v0, LX/1FZ;

    iput-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->b:LX/1FZ;

    return-void
.end method

.method private getCheckmarkBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2776711
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 2776712
    :cond_1
    invoke-direct {p0}, Lcom/facebook/tarot/view/CheckmarkView;->a()V

    .line 2776713
    :cond_2
    iget-object v0, p0, Lcom/facebook/tarot/view/CheckmarkView;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2776714
    invoke-direct {p0}, Lcom/facebook/tarot/view/CheckmarkView;->getCheckmarkBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2776715
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2776716
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 2776717
    if-eqz p1, :cond_0

    .line 2776718
    invoke-direct {p0}, Lcom/facebook/tarot/view/CheckmarkView;->a()V

    .line 2776719
    :cond_0
    return-void
.end method
