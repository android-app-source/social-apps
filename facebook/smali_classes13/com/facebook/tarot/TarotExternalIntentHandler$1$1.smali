.class public final Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/K5J;


# direct methods
.method public constructor <init>(LX/K5J;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2770198
    iput-object p1, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->b:LX/K5J;

    iput-object p2, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2770199
    iget-object v0, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K5t;

    .line 2770200
    iget-object v2, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->b:LX/K5J;

    iget-object v2, v2, LX/K5J;->e:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    .line 2770201
    iget-object v3, v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2770202
    iget-object v3, v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    if-eqz v3, :cond_0

    .line 2770203
    iget-object v3, v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v3, v0}, LX/K6a;->a(LX/K5t;)V

    .line 2770204
    :cond_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2770205
    const-string v3, "tarot_digest_id"

    iget-object v5, v0, LX/K5t;->c:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770206
    iget-object v3, v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    if-eqz v3, :cond_1

    .line 2770207
    iget-object v3, v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    invoke-virtual {v3, v4}, LX/K79;->a(Landroid/os/Bundle;)V

    .line 2770208
    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0}, LX/K5t;->b()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 2770209
    invoke-virtual {v0, v3}, LX/K5t;->a(I)LX/K7D;

    move-result-object v5

    .line 2770210
    invoke-static {v5, v4}, Lcom/facebook/tarot/cards/TarotCardDeckFragment;->a(LX/K7D;Landroid/os/Bundle;)Lcom/facebook/tarot/cards/TarotCardDeckFragment;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->a(Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    .line 2770211
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2770212
    :cond_2
    goto :goto_0

    .line 2770213
    :cond_3
    iget-object v0, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->b:LX/K5J;

    iget-object v0, v0, LX/K5J;->e:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    iget-object v1, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->a(Ljava/util/List;)V

    .line 2770214
    iget-object v0, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->b:LX/K5J;

    iget-object v0, v0, LX/K5J;->e:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2770215
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2770216
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K5t;

    .line 2770217
    iget-object v4, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1}, LX/K5t;->b()I

    move-result v5

    iget-object v6, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v2, :cond_5

    :goto_2
    invoke-virtual {v4, v5, v3, v2}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->a(IIZ)V

    .line 2770218
    iget-object v2, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->z:Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;

    .line 2770219
    iget-object v4, v1, LX/K5t;->b:LX/K5s;

    move-object v1, v4

    .line 2770220
    iget-object v1, v1, LX/K5s;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/facebook/tarot/carousel/TarotPagerWithSharedHeaderAndPageIndicator;->setPublisherName(Ljava/lang/String;)V

    .line 2770221
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->A:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iget-object v2, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->C:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->a(Ljava/util/List;I)V

    .line 2770222
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v2, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    .line 2770223
    iget-object v3, v1, LX/K8s;->c:Ljava/util/Map;

    iget-object v4, v2, LX/K79;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770224
    iget-object v3, v1, LX/K8s;->d:Ljava/util/Stack;

    iget-object v4, v2, LX/K79;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2770225
    iget-object v1, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->t:LX/K8s;

    iget-object v2, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    iget-object v2, v2, LX/K79;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    invoke-virtual {v3}, LX/K6a;->c()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/K8s;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2770226
    :cond_4
    iget-object v0, p0, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;->b:LX/K5J;

    iget-object v0, v0, LX/K5J;->f:LX/0ew;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2770227
    return-void

    :cond_5
    move v2, v3

    .line 2770228
    goto :goto_2
.end method
