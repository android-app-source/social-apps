.class public final Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6afd443f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2777532
    const-class v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2777533
    const-class v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2777534
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2777535
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2777536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2777537
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->a()Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2777538
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2777539
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2777540
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2777541
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2777523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2777524
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->a()Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2777525
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->a()Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    .line 2777526
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->a()Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2777527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;

    .line 2777528
    iput-object v0, v1, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->e:Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    .line 2777529
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2777530
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2777531
    new-instance v0, LX/K9S;

    invoke-direct {v0, p1}, LX/K9S;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2777521
    iget-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->e:Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    iput-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->e:Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    .line 2777522
    iget-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;->e:Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2777513
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2777514
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2777520
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2777517
    new-instance v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;

    invoke-direct {v0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel;-><init>()V

    .line 2777518
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2777519
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2777516
    const v0, 0x173fe9b5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2777515
    const v0, 0x285feb

    return v0
.end method
