.class public final Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63b8ca3b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2777512
    const-class v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2777479
    const-class v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2777510
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2777511
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2777502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2777503
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2777504
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2777505
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2777506
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2777507
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2777508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2777509
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2777500
    iget-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->e:Ljava/util/List;

    .line 2777501
    iget-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2777487
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2777488
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2777489
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2777490
    if-eqz v1, :cond_2

    .line 2777491
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    .line 2777492
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2777493
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2777494
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2777495
    invoke-virtual {p0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2777496
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    .line 2777497
    iput-object v0, v1, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2777498
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2777499
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2777484
    new-instance v0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;-><init>()V

    .line 2777485
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2777486
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2777483
    const v0, -0x1a772f54

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2777482
    const v0, 0x6fe01397

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2777480
    iget-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2777481
    iget-object v0, p0, Lcom/facebook/timeline/videos/protocol/FetchTimelineVideosGraphQLModels$FetchTimelineVideosModel$UploadedVideosFeedUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method
