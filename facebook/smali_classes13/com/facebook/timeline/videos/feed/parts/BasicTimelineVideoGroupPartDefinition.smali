.class public Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static s:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

.field private final i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

.field private final k:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

.field private final m:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final n:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

.field private final p:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final q:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2777160
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2777161
    iput-object p14, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2777162
    iput-object p13, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2777163
    iput-object p12, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 2777164
    iput-object p8, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->d:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    .line 2777165
    iput-object p9, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->e:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    .line 2777166
    iput-object p11, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->f:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    .line 2777167
    iput-object p6, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2777168
    iput-object p7, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    .line 2777169
    iput-object p5, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2777170
    iput-object p4, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->j:Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    .line 2777171
    iput-object p3, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 2777172
    iput-object p2, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->l:Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    .line 2777173
    iput-object p1, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 2777174
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->n:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 2777175
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->o:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    .line 2777176
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->q:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    .line 2777177
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->p:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 2777178
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->r:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    .line 2777179
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;
    .locals 3

    .prologue
    .line 2777201
    const-class v1, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;

    monitor-enter v1

    .line 2777202
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2777203
    sput-object v2, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2777204
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2777205
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2777206
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2777207
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2777208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;
    .locals 20

    .prologue
    .line 2777209
    new-instance v0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    move-result-object v16

    check-cast v16, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    move-result-object v17

    check-cast v17, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v18

    check-cast v18, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    move-result-object v19

    check-cast v19, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    invoke-direct/range {v0 .. v19}, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;)V

    .line 2777210
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2777181
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2777182
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777183
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->r:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777184
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->l:Lcom/facebook/timeline/feed/parts/TimelineHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777185
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777186
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777187
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777188
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->j:Lcom/facebook/feed/rows/sections/TimelineStoryTextSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777189
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777190
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->d:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777191
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->e:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777192
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->f:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777193
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->o:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777194
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->q:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777195
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777196
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777197
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777198
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->n:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777199
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;->p:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2777200
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2777180
    const/4 v0, 0x1

    return v0
.end method
