.class public Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/facebook/timeline/videos/feed/TimelineVideosEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "Lcom/facebook/timeline/videos/feed/TimelineVideosEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/videos/feed/parts/BasicTimelineVideoGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2777211
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2777212
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/HideableUnit;

    .line 2777213
    move-object v2, p2

    .line 2777214
    invoke-virtual {v0, v1, v2}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, p1}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/G50;

    .line 2777215
    move-object v2, p4

    .line 2777216
    invoke-virtual {v0, v1, v2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/G54;

    .line 2777217
    move-object v2, p5

    .line 2777218
    invoke-virtual {v0, v1, v2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, v1, p3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;->a:LX/1T5;

    .line 2777219
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;
    .locals 9

    .prologue
    .line 2777220
    const-class v1, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;

    monitor-enter v1

    .line 2777221
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2777222
    sput-object v2, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2777223
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2777224
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2777225
    new-instance v3, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;

    const/16 v4, 0x3704

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x956

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x6fa

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1258

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1259

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2777226
    move-object v0, v3

    .line 2777227
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2777228
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2777229
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2777230
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2777231
    iget-object v0, p0, Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 2777232
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2777233
    const/4 v0, 0x1

    return v0
.end method
