.class public Lcom/facebook/timeline/videos/TimelineVideosTabFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->TIMELINE_VIDEOS_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/videos/feed/parts/TimelineVideoFeedRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/K9P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/G10;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/K9H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/BP0;

.field public j:LX/1Rq;

.field private k:LX/1Jg;

.field public l:LX/0g7;

.field public m:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2777047
    const-class v0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2777046
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v1, p1

    check-cast v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;

    invoke-static {v8}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v2

    check-cast v2, LX/1DS;

    const/16 v3, 0x3705

    invoke-static {v8, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class v4, LX/K9P;

    invoke-interface {v8, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/K9P;

    invoke-static {v8}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v8}, LX/G10;->a(LX/0QB;)LX/G10;

    move-result-object v6

    check-cast v6, LX/G10;

    invoke-static {v8}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v7

    check-cast v7, LX/1Db;

    new-instance v0, LX/K9H;

    invoke-static {v8}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v8}, LX/K9J;->a(LX/0QB;)LX/K9J;

    move-result-object p0

    check-cast p0, LX/K9J;

    invoke-static {v8}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-direct {v0, v9, p0, p1}, LX/K9H;-><init>(LX/0tX;LX/K9J;LX/1Ck;)V

    move-object v8, v0

    check-cast v8, LX/K9H;

    iput-object v2, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->a:LX/1DS;

    iput-object v3, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->b:LX/0Ot;

    iput-object v4, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->c:LX/K9P;

    iput-object v5, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->d:Ljava/lang/String;

    iput-object v6, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->e:LX/G10;

    iput-object v7, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->f:LX/1Db;

    iput-object v8, v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->g:LX/K9H;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2777027
    const-string v0, "timeline_videos_tab"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2777028
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2777029
    const-class v0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;

    invoke-static {v0, p0}, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2777030
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v0

    .line 2777031
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2777032
    const-string v0, "profile_name"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->m:Ljava/lang/String;

    .line 2777033
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2777034
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2777035
    new-instance v5, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 2777036
    const-string v7, "timeline_context_item_type"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v7

    .line 2777037
    const-string v8, "timeline_friend_request_ref"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    invoke-static {v6}, LX/5P2;->from(Ljava/io/Serializable;)LX/5P2;

    move-result-object v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, LX/BP0;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;LX/5P2;LX/0am;Z)LX/BP0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->i:LX/BP0;

    .line 2777038
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->g:LX/K9H;

    sget-object v1, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 2777039
    new-instance v9, LX/K9F;

    invoke-direct {v9}, LX/K9F;-><init>()V

    iput-object v9, v0, LX/K9H;->a:LX/K9F;

    .line 2777040
    iput-object v1, v0, LX/K9H;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2777041
    iput-object p0, v0, LX/K9H;->g:Lcom/facebook/timeline/videos/TimelineVideosTabFragment;

    .line 2777042
    iput-wide v2, v0, LX/K9H;->h:J

    .line 2777043
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->g:LX/K9H;

    .line 2777044
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/K9H;->a(LX/K9H;Z)V

    .line 2777045
    return-void
.end method

.method public final mJ_()V
    .locals 0

    .prologue
    .line 2777026
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v11, 0x2

    const/16 v0, 0x2a

    const v1, -0x43439c93

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2777000
    new-instance v9, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v9, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2777001
    const v0, 0x7f0314f3

    invoke-virtual {p1, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 2777002
    const v0, 0x7f0d2f62

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2777003
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2777004
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    .line 2777005
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0g7;->d(Z)V

    .line 2777006
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->k()V

    .line 2777007
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->b()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 2777008
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->f:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0g7;->a(LX/1St;)V

    .line 2777009
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    new-instance v1, LX/K9K;

    invoke-direct {v1, p0}, LX/K9K;-><init>(Lcom/facebook/timeline/videos/TimelineVideosTabFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->b(LX/0fu;)V

    .line 2777010
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    new-instance v1, LX/K9L;

    invoke-direct {v1, p0}, LX/K9L;-><init>(Lcom/facebook/timeline/videos/TimelineVideosTabFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->b(LX/0fx;)V

    .line 2777011
    new-instance v3, LX/K9N;

    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->i:LX/BP0;

    invoke-direct {v3, v0}, LX/K9N;-><init>(LX/5SB;)V

    .line 2777012
    new-instance v4, Lcom/facebook/timeline/videos/TimelineVideosTabFragment$1;

    invoke-direct {v4, p0}, Lcom/facebook/timeline/videos/TimelineVideosTabFragment$1;-><init>(Lcom/facebook/timeline/videos/TimelineVideosTabFragment;)V

    .line 2777013
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->e:LX/G10;

    invoke-virtual {v0}, LX/G10;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->k:LX/1Jg;

    .line 2777014
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->c:LX/K9P;

    const-string v1, "native_timeline"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v5, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-static {v5}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->k:LX/1Jg;

    iget-object v7, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->i:LX/BP0;

    invoke-virtual/range {v0 .. v7}, LX/K9P;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;LX/1Jg;LX/5SB;)LX/K9O;

    move-result-object v0

    .line 2777015
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->a:LX/1DS;

    iget-object v2, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->b:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->g:LX/K9H;

    .line 2777016
    iget-object v4, v3, LX/K9H;->a:LX/K9F;

    move-object v3, v4

    .line 2777017
    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2777018
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2777019
    move-object v0, v1

    .line 2777020
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-virtual {v0, v1}, LX/1Ql;->b(LX/0g8;)LX/1Ql;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ql;->d()LX/1Rq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->j:LX/1Rq;

    .line 2777021
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->j:LX/1Rq;

    invoke-virtual {v0, v1}, LX/0g7;->a(LX/1OO;)V

    .line 2777022
    iget-object v0, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->j:LX/1Rq;

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    if-nez v0, :cond_0

    .line 2777023
    const v0, 0x7f0d2f38

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2777024
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0g7;->f(Landroid/view/View;)V

    .line 2777025
    :cond_0
    const/16 v0, 0x2b

    const v1, -0x4d4fdcf4

    invoke-static {v11, v0, v1, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v9
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0xbd4f3aa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2776980
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    if-eqz v1, :cond_0

    .line 2776981
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-virtual {v1, v2}, LX/0g7;->a(Landroid/widget/ListAdapter;)V

    .line 2776982
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    invoke-virtual {v1}, LX/0g7;->x()V

    .line 2776983
    :cond_0
    iput-object v2, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->g:LX/K9H;

    .line 2776984
    iput-object v2, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->j:LX/1Rq;

    .line 2776985
    iput-object v2, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->l:LX/0g7;

    .line 2776986
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2776987
    const/16 v1, 0x2b

    const v2, 0x7a7e4a2d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f8c42d5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2776996
    iget-object v1, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->g:LX/K9H;

    .line 2776997
    iget-object v2, v1, LX/K9H;->d:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2776998
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 2776999
    const/16 v1, 0x2b

    const v2, -0x3f503f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x567cc17f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2776988
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2776989
    const/4 v8, 0x1

    .line 2776990
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2776991
    if-eqz v1, :cond_0

    .line 2776992
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081636

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/timeline/videos/TimelineVideosTabFragment;->m:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2776993
    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2776994
    invoke-interface {v1, v8}, LX/1ZF;->k_(Z)V

    .line 2776995
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2885a5fb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
