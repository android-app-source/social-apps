.class public final Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2716257
    const-class v0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;

    new-instance v1, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2716258
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2716256
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2716242
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2716243
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 2716244
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2716245
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2716246
    if-eqz v2, :cond_0

    .line 2716247
    const-string v3, "group_admined_page_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2716248
    invoke-static {v1, v2, p1, p2}, LX/Jb1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2716249
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2716250
    if-eqz v2, :cond_1

    .line 2716251
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2716252
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2716253
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2716254
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2716255
    check-cast p1, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Serializer;->a(Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;LX/0nX;LX/0my;)V

    return-void
.end method
