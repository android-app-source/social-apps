.class public final Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x23458ddd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2716302
    const-class v0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2716303
    const-class v0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2716300
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2716301
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 2716293
    iput-object p1, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2716294
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2716295
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2716296
    if-eqz v0, :cond_0

    .line 2716297
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2716298
    :cond_0
    return-void

    .line 2716299
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2716285
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2716286
    invoke-virtual {p0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->a()Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2716287
    invoke-virtual {p0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2716288
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2716289
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2716290
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2716291
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2716292
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2716277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2716278
    invoke-virtual {p0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->a()Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2716279
    invoke-virtual {p0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->a()Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    .line 2716280
    invoke-virtual {p0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->a()Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2716281
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;

    .line 2716282
    iput-object v0, v1, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->e:Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    .line 2716283
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2716284
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2716304
    new-instance v0, LX/Jax;

    invoke-direct {v0, p1}, LX/Jax;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2716275
    iget-object v0, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->e:Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->e:Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    .line 2716276
    iget-object v0, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->e:Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel$GroupAdminedPageMembersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2716269
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2716270
    invoke-virtual {p0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2716271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2716272
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2716273
    :goto_0
    return-void

    .line 2716274
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2716266
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2716267
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 2716268
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2716263
    new-instance v0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;

    invoke-direct {v0}, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;-><init>()V

    .line 2716264
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2716265
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2716262
    const v0, 0x3f47736d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2716259
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2716260
    iget-object v0, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2716261
    iget-object v0, p0, Lcom/facebook/groups/page_voice_switcher/data/GroupsFetchPageVoiceQueryModels$GroupsFetchPageVoicesModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method
