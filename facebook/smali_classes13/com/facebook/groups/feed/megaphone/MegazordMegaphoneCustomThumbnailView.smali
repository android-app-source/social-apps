.class public Lcom/facebook/groups/feed/megaphone/MegazordMegaphoneCustomThumbnailView;
.super LX/B9V;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2716030
    const-class v0, Lcom/facebook/groups/feed/megaphone/MegazordMegaphoneCustomThumbnailView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/megaphone/MegazordMegaphoneCustomThumbnailView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2716031
    invoke-direct {p0, p1}, LX/B9V;-><init>(Landroid/content/Context;)V

    .line 2716032
    const p1, 0x7f030ab7

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2716033
    const p1, 0x7f0d1b67

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/groups/feed/megaphone/MegazordMegaphoneCustomThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2716034
    return-void
.end method


# virtual methods
.method public setImageUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2716035
    iget-object v0, p0, Lcom/facebook/groups/feed/megaphone/MegazordMegaphoneCustomThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/groups/feed/megaphone/MegazordMegaphoneCustomThumbnailView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2716036
    return-void
.end method
