.class public Lcom/facebook/groups/fb4a/memberpicker/GroupsMemberPickerReactActivity;
.super Lcom/facebook/fbreact/fragment/ReactActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2716023
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/ReactActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final d(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2716024
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/memberpicker/GroupsMemberPickerReactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2716025
    const-string v1, "disableContactImporter"

    const-string v2, "disableContactImporter"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2716026
    const-string v1, "disableAnimation"

    const-string v2, "disableAnimation"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2716027
    const-string v1, "enableMessage"

    const-string v2, "enableMessage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2716028
    const-string v1, "group"

    const-string v2, "group"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2716029
    return-object p1
.end method
