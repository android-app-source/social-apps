.class public Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0ic;


# instance fields
.field public a:LX/Jae;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/ComponentName;
    .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DN3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/JaW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/support/v4/view/ViewPager;

.field private h:LX/Jad;

.field private i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2715749
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2715748
    const-string v0, "groups_hub"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2715745
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2715746
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;

    const-class v3, LX/Jae;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Jae;

    invoke-static {v0}, LX/2lC;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/DN3;->a(LX/0QB;)LX/DN3;

    move-result-object v6

    check-cast v6, LX/DN3;

    new-instance v1, LX/JaW;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    invoke-direct {v1, p1, p0}, LX/JaW;-><init>(LX/1Ck;LX/0tX;)V

    move-object p1, v1

    check-cast p1, LX/JaW;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    iput-object v3, v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->a:LX/Jae;

    iput-object v4, v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->b:Landroid/content/ComponentName;

    iput-object v5, v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->d:LX/DN3;

    iput-object p1, v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->e:LX/JaW;

    iput-object v0, v2, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->f:LX/0iA;

    .line 2715747
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2715737
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 2715738
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    .line 2715739
    iget-object v2, v1, LX/Jad;->b:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JaX;

    invoke-interface {v2}, LX/JaX;->b()Z

    move-result v2

    move v1, v2

    .line 2715740
    if-eqz v1, :cond_0

    .line 2715741
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    invoke-virtual {v1, v0}, LX/DK1;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2715742
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0ic;

    if-eqz v1, :cond_0

    check-cast v0, LX/0ic;

    invoke-interface {v0}, LX/0ic;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2715743
    const/4 v0, 0x1

    .line 2715744
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2715750
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2715751
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    if-eqz v0, :cond_1

    .line 2715752
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2715753
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    invoke-virtual {v1, v0}, LX/DK1;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2715754
    if-eqz v1, :cond_0

    .line 2715755
    invoke-virtual {v1, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2715756
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2715757
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6534dde6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2715736
    const v1, 0x7f030851

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1bdbdec7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6fd950db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2715720
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2715721
    const/4 v6, 0x0

    .line 2715722
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2715723
    if-nez v1, :cond_0

    .line 2715724
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x695f75ef

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2715725
    :cond_0
    iget-object v2, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->d:LX/DN3;

    invoke-virtual {v2}, LX/DN3;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2715726
    new-instance v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;-><init>(Landroid/content/Context;)V

    .line 2715727
    iget-object v4, v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, v4

    .line 2715728
    const v5, 0x7f083abb

    invoke-virtual {v4, v5}, Lcom/facebook/ui/search/SearchEditText;->setHint(I)V

    .line 2715729
    iget-object v4, v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, v4

    .line 2715730
    invoke-virtual {v4, v6}, Lcom/facebook/ui/search/SearchEditText;->setFocusable(Z)V

    .line 2715731
    new-instance v4, LX/Jai;

    invoke-direct {v4, p0}, LX/Jai;-><init>(Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;)V

    invoke-virtual {v2, v4}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Landroid/view/View$OnClickListener;)V

    .line 2715732
    invoke-interface {v1, v2}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2715733
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0

    .line 2715734
    :cond_1
    const v2, 0x7f083abc

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2715735
    invoke-interface {v1, v6}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2715681
    const v0, 0x7f0d15b3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    .line 2715682
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->a:LX/Jae;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    .line 2715683
    new-instance p2, LX/Jad;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/DN3;->a(LX/0QB;)LX/DN3;

    move-result-object p1

    check-cast p1, LX/DN3;

    invoke-direct {p2, v1, v2, v3, p1}, LX/Jad;-><init>(Landroid/content/Intent;LX/0gc;Landroid/content/res/Resources;LX/DN3;)V

    .line 2715684
    move-object v0, p2

    .line 2715685
    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    .line 2715686
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2715687
    const v0, 0x7f0d15b2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2715688
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2715689
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2715690
    if-eqz v0, :cond_0

    .line 2715691
    const-string v0, "groups_hub_discover"

    .line 2715692
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2715693
    const-string v2, "groups_hub_tab"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2715694
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    sget-object v2, LX/Jac;->DISCOVER:LX/Jac;

    invoke-virtual {v1, v2}, LX/Jad;->a(LX/Jac;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2715695
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->d:LX/DN3;

    invoke-virtual {v0}, LX/DN3;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2715696
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->e:LX/JaW;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2715697
    iget-object v3, v0, LX/JaW;->a:LX/1Ck;

    sget-object v4, LX/JaV;->ADMINED_GROUPS_QUERY:LX/JaV;

    invoke-virtual {v3, v4}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2715698
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->f:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUPS_DISCOVER_TAB_TOOL_TIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/3l2;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3l2;

    .line 2715699
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->d:LX/DN3;

    invoke-virtual {v1}, LX/DN3;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    sget-object v3, LX/Jac;->DISCOVER:LX/Jac;

    invoke-virtual {v2, v3}, LX/Jad;->a(LX/Jac;)I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 2715700
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    sget-object v3, LX/Jac;->DISCOVER:LX/Jac;

    invoke-virtual {v2, v3}, LX/Jad;->a(LX/Jac;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d(I)Landroid/view/View;

    move-result-object v1

    .line 2715701
    if-nez v1, :cond_6

    .line 2715702
    :goto_2
    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->f:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/3l2;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2715703
    :cond_2
    return-void

    .line 2715704
    :cond_3
    const-string v0, "gysc"

    .line 2715705
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2715706
    const-string v2, "extra_navigation_source"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2715707
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    sget-object v2, LX/Jac;->CREATE:LX/Jac;

    invoke-virtual {v1, v2}, LX/Jad;->a(LX/Jac;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 2715708
    :cond_4
    const-string v0, "groups_hub_inbox"

    .line 2715709
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2715710
    const-string v2, "groups_hub_tab"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2715711
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->g:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;->h:LX/Jad;

    sget-object v2, LX/Jac;->INBOX:LX/Jac;

    invoke-virtual {v1, v2}, LX/Jad;->a(LX/Jac;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_0

    .line 2715712
    :cond_5
    new-instance v3, LX/JaT;

    invoke-direct {v3, v0}, LX/JaT;-><init>(LX/JaW;)V

    .line 2715713
    new-instance v4, LX/JaU;

    invoke-direct {v4, v0, v1, v2}, LX/JaU;-><init>(LX/JaW;LX/Jad;Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;)V

    .line 2715714
    iget-object p1, v0, LX/JaW;->a:LX/1Ck;

    sget-object p2, LX/JaV;->ADMINED_GROUPS_QUERY:LX/JaV;

    invoke-virtual {p1, p2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto/16 :goto_1

    .line 2715715
    :cond_6
    new-instance v2, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2715716
    iget-object v3, v0, LX/3l2;->b:LX/0wM;

    const v4, 0x7f02080e

    const/4 p1, -0x1

    invoke-virtual {v3, v4, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2715717
    const v3, 0x7f083abd    # 1.8108E38f

    invoke-virtual {v2, v3}, LX/0hs;->a(I)V

    .line 2715718
    const v3, 0x7f083abe

    invoke-virtual {v2, v3}, LX/0hs;->b(I)V

    .line 2715719
    invoke-virtual {v2, v1}, LX/0ht;->f(Landroid/view/View;)V

    goto/16 :goto_2
.end method
