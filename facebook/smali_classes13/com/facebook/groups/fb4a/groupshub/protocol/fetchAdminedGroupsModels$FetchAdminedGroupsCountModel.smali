.class public final Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4e261547
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2715945
    const-class v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2715922
    const-class v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2715943
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2715944
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2715937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2715938
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2715939
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2715940
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2715941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2715942
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2715929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2715930
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2715931
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    .line 2715932
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2715933
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;

    .line 2715934
    iput-object v0, v1, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->e:Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    .line 2715935
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2715936
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAccountUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2715927
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->e:Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->e:Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    .line 2715928
    iget-object v0, p0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;->e:Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel$AccountUserModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2715924
    new-instance v0, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/groupshub/protocol/fetchAdminedGroupsModels$FetchAdminedGroupsCountModel;-><init>()V

    .line 2715925
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2715926
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2715923
    const v0, -0x5d587c89

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2715921
    const v0, -0x6747e1ce

    return v0
.end method
