.class public Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/F5h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/F5k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/widget/Button;

.field public f:Landroid/view/View$OnClickListener;

.field private g:Lcom/facebook/groups/gysc/GYSCRowView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2715458
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    invoke-static {p0}, LX/F5h;->b(LX/0QB;)LX/F5h;

    move-result-object v3

    check-cast v3, LX/F5h;

    invoke-static {p0}, LX/F5k;->b(LX/0QB;)LX/F5k;

    move-result-object p0

    check-cast p0, LX/F5k;

    iput-object v1, p1, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->a:Landroid/content/Context;

    iput-object v2, p1, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->b:LX/17W;

    iput-object v3, p1, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->c:LX/F5h;

    iput-object p0, p1, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->d:LX/F5k;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2715473
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2715474
    const-class v0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2715475
    iget-object v0, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->d:LX/F5k;

    .line 2715476
    iget-object v1, v0, LX/F5k;->c:LX/DKO;

    iget-object p1, v0, LX/F5k;->f:LX/DKP;

    invoke-virtual {v1, p1}, LX/0b4;->a(LX/0b2;)Z

    .line 2715477
    iget-object v0, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->c:LX/F5h;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->d:LX/F5k;

    invoke-virtual {v0, v1}, LX/F5h;->a(LX/F5k;)V

    .line 2715478
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x79b38387

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2715479
    const v1, 0x7f030603

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4fbd010c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd762ddf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2715469
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2715470
    iget-object v1, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->d:LX/F5k;

    .line 2715471
    iget-object v2, v1, LX/F5k;->c:LX/DKO;

    iget-object p0, v1, LX/F5k;->f:LX/DKP;

    invoke-virtual {v2, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 2715472
    const/16 v1, 0x2b

    const v2, 0x5ad51542

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2715459
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2715460
    const v0, 0x7f0d108a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->e:Landroid/widget/Button;

    .line 2715461
    const v0, 0x7f0d108c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/gysc/GYSCRowView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->g:Lcom/facebook/groups/gysc/GYSCRowView;

    .line 2715462
    iget-object v0, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->e:Landroid/widget/Button;

    .line 2715463
    iget-object v1, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->f:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_0

    .line 2715464
    new-instance v1, LX/JaQ;

    invoke-direct {v1, p0}, LX/JaQ;-><init>(Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;)V

    iput-object v1, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->f:Landroid/view/View$OnClickListener;

    .line 2715465
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->f:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 2715466
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2715467
    iget-object v0, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->g:Lcom/facebook/groups/gysc/GYSCRowView;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/createrpanel/FB4AGroupsCreateTabFragment;->d:LX/F5k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2715468
    return-void
.end method
