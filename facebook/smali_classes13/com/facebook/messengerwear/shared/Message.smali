.class public Lcom/facebook/messengerwear/shared/Message;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messengerwear/shared/Message;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:LX/Jsz;

.field public final g:LX/Jsy;

.field public final h:Lcom/facebook/messengerwear/shared/Message$Attachment;

.field public final i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746043
    new-instance v0, LX/Jsu;

    invoke-direct {v0}, LX/Jsu;-><init>()V

    sput-object v0, Lcom/facebook/messengerwear/shared/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;LX/Jsz;LX/Jsy;Lcom/facebook/messengerwear/shared/Message$Attachment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2746094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746095
    iput-wide p1, p0, Lcom/facebook/messengerwear/shared/Message;->a:J

    .line 2746096
    iput-object p3, p0, Lcom/facebook/messengerwear/shared/Message;->b:Ljava/lang/String;

    .line 2746097
    iput-object p4, p0, Lcom/facebook/messengerwear/shared/Message;->c:Ljava/lang/String;

    .line 2746098
    iput-boolean p5, p0, Lcom/facebook/messengerwear/shared/Message;->d:Z

    .line 2746099
    iput-object p6, p0, Lcom/facebook/messengerwear/shared/Message;->e:Ljava/lang/String;

    .line 2746100
    iput-object p7, p0, Lcom/facebook/messengerwear/shared/Message;->f:LX/Jsz;

    .line 2746101
    iput-object p8, p0, Lcom/facebook/messengerwear/shared/Message;->g:LX/Jsy;

    .line 2746102
    iput-object p9, p0, Lcom/facebook/messengerwear/shared/Message;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    .line 2746103
    iput-object p10, p0, Lcom/facebook/messengerwear/shared/Message;->i:Ljava/lang/String;

    .line 2746104
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2746082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746083
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/messengerwear/shared/Message;->a:J

    .line 2746084
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messengerwear/shared/Message;->b:Ljava/lang/String;

    .line 2746085
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messengerwear/shared/Message;->c:Ljava/lang/String;

    .line 2746086
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messengerwear/shared/Message;->d:Z

    .line 2746087
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->e:Ljava/lang/String;

    .line 2746088
    invoke-static {}, LX/Jsz;->values()[LX/Jsz;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->f:LX/Jsz;

    .line 2746089
    invoke-static {}, LX/Jsy;->values()[LX/Jsy;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->g:LX/Jsy;

    .line 2746090
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/shared/Message$Attachment;

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    .line 2746091
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->i:Ljava/lang/String;

    .line 2746092
    return-void

    .line 2746093
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2746105
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2746081
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2746055
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss:SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2746056
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2746057
    const-class v2, Lcom/facebook/messengerwear/shared/Message;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746058
    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746059
    const-string v2, "text=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746060
    iget-object v2, p0, Lcom/facebook/messengerwear/shared/Message;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746061
    const-string v2, "], stickerId=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746062
    iget-object v2, p0, Lcom/facebook/messengerwear/shared/Message;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746063
    const-string v2, "], senderName=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746064
    iget-object v2, p0, Lcom/facebook/messengerwear/shared/Message;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746065
    const-string v2, "], me="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746066
    iget-boolean v2, p0, Lcom/facebook/messengerwear/shared/Message;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2746067
    const-string v2, ", timestampMs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746068
    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/facebook/messengerwear/shared/Message;->a:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746069
    const-string v0, " ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746070
    iget-wide v2, p0, Lcom/facebook/messengerwear/shared/Message;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2746071
    const-string v0, "), messageType=["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746072
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->f:LX/Jsz;

    invoke-virtual {v0}, LX/Jsz;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746073
    const-string v0, "], messageGrouping=["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746074
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->g:LX/Jsy;

    invoke-virtual {v0}, LX/Jsy;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746075
    const-string v0, "], attachmment=["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746076
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2746077
    const-string v0, "], attributionText=["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746078
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746079
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2746080
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2746044
    iget-wide v2, p0, Lcom/facebook/messengerwear/shared/Message;->a:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 2746045
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2746046
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2746047
    iget-boolean v0, p0, Lcom/facebook/messengerwear/shared/Message;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2746048
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2746049
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->f:LX/Jsz;

    invoke-virtual {v0}, LX/Jsz;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2746050
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->g:LX/Jsy;

    invoke-virtual {v0}, LX/Jsy;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2746051
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2746052
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/Message;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2746053
    return-void

    :cond_0
    move v0, v1

    .line 2746054
    goto :goto_0
.end method
