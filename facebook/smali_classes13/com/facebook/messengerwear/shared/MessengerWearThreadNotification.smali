.class public Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/messengerwear/shared/Message;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messengerwear/shared/Message;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746285
    new-instance v0, LX/Jt3;

    invoke-direct {v0}, LX/Jt3;-><init>()V

    sput-object v0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2746257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->a:Ljava/lang/String;

    .line 2746259
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->b:I

    .line 2746260
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->c:Ljava/lang/String;

    .line 2746261
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/shared/Message;

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->d:Lcom/facebook/messengerwear/shared/Message;

    .line 2746262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2746263
    sget-object v1, Lcom/facebook/messengerwear/shared/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2746264
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->e:LX/0Px;

    .line 2746265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2746266
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2746267
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->f:LX/0Px;

    .line 2746268
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/messengerwear/shared/Message;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/facebook/messengerwear/shared/Message;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messengerwear/shared/Message;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2746277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746278
    iput-object p1, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->a:Ljava/lang/String;

    .line 2746279
    iput p2, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->b:I

    .line 2746280
    iput-object p3, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->c:Ljava/lang/String;

    .line 2746281
    iput-object p4, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->d:Lcom/facebook/messengerwear/shared/Message;

    .line 2746282
    iput-object p5, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->e:LX/0Px;

    .line 2746283
    iput-object p6, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->f:LX/0Px;

    .line 2746284
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2746276
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2746269
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2746270
    iget v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2746271
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2746272
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->d:Lcom/facebook/messengerwear/shared/Message;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2746273
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2746274
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2746275
    return-void
.end method
