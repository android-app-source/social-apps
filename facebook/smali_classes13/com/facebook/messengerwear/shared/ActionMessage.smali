.class public Lcom/facebook/messengerwear/shared/ActionMessage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messengerwear/shared/ActionMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/Jss;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2745978
    new-instance v0, LX/Jsr;

    invoke-direct {v0}, LX/Jsr;-><init>()V

    sput-object v0, Lcom/facebook/messengerwear/shared/ActionMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2745973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745974
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/ActionMessage;->a:Ljava/lang/String;

    .line 2745975
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/ActionMessage;->b:Ljava/lang/String;

    .line 2745976
    invoke-static {}, LX/Jss;->values()[LX/Jss;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/messengerwear/shared/ActionMessage;->c:LX/Jss;

    .line 2745977
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2745983
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2745979
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/ActionMessage;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2745980
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/ActionMessage;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2745981
    iget-object v0, p0, Lcom/facebook/messengerwear/shared/ActionMessage;->c:LX/Jss;

    invoke-virtual {v0}, LX/Jss;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2745982
    return-void
.end method
