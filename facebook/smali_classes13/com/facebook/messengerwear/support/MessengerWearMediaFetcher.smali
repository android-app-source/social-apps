.class public Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Class;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3dt;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/8jI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/1HI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/1FZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746623
    const-class v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    sput-object v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    .line 2746624
    const-class v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2746625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746626
    return-void
.end method

.method private a(LX/1GB;Ljava/lang/String;)LX/JtM;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2746627
    invoke-interface {p1}, LX/1GB;->c()I

    move-result v3

    .line 2746628
    invoke-interface {p1}, LX/1GB;->a()I

    move-result v4

    .line 2746629
    invoke-interface {p1}, LX/1GB;->b()I

    move-result v5

    .line 2746630
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->h:LX/1FZ;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v4, v5, v1}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v6

    .line 2746631
    invoke-virtual {v6}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2746632
    new-array v7, v3, [[B

    move v1, v2

    .line 2746633
    :goto_0
    if-ge v1, v3, :cond_0

    .line 2746634
    invoke-interface {p1, v1}, LX/1GB;->a(I)LX/40I;

    move-result-object v8

    invoke-interface {v8, v4, v5, v0}, LX/40I;->a(IILandroid/graphics/Bitmap;)V

    .line 2746635
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2746636
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->WEBP:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0, v9, v2, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2746637
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    aput-object v8, v7, v1

    .line 2746638
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2746639
    :cond_0
    invoke-virtual {v6}, LX/1FJ;->close()V

    .line 2746640
    new-instance v0, LX/JtM;

    invoke-interface {p1}, LX/1GB;->d()[I

    move-result-object v1

    invoke-interface {p1}, LX/1GB;->e()I

    move-result v2

    invoke-direct {v0, p2, v7, v1, v2}, LX/JtM;-><init>(Ljava/lang/String;[[B[II)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;[BLjava/lang/String;)LX/JtM;
    .locals 5
    .param p0    # Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2746641
    if-nez p1, :cond_0

    .line 2746642
    :goto_0
    return-object v0

    .line 2746643
    :cond_0
    sget-object v2, LX/1ld;->b:LX/1lW;

    .line 2746644
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2746645
    :try_start_1
    invoke-static {v3}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2746646
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 2746647
    :goto_1
    sget-object v1, LX/1ld;->c:LX/1lW;

    if-ne v0, v1, :cond_3

    .line 2746648
    invoke-static {p1}, Lcom/facebook/animated/gif/GifImage;->a([B)Lcom/facebook/animated/gif/GifImage;

    move-result-object v1

    .line 2746649
    invoke-virtual {v1}, Lcom/facebook/animated/gif/GifImage;->c()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    .line 2746650
    invoke-direct {p0, v1, p2}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a(LX/1GB;Ljava/lang/String;)LX/JtM;

    move-result-object v0

    .line 2746651
    :goto_2
    invoke-virtual {v1}, Lcom/facebook/animated/gif/GifImage;->h()V

    goto :goto_0

    .line 2746652
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2746653
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 2746654
    :goto_5
    sget-object v2, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v3, "Error while detecting image format."

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2746655
    :catch_2
    move-exception v3

    :try_start_6
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_1
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_4

    .line 2746656
    :cond_2
    new-instance v0, LX/JtM;

    invoke-direct {v0, p2, p1}, LX/JtM;-><init>(Ljava/lang/String;[B)V

    goto :goto_2

    .line 2746657
    :cond_3
    sget-object v1, LX/1ld;->i:LX/1lW;

    if-ne v0, v1, :cond_4

    .line 2746658
    invoke-static {p1}, Lcom/facebook/animated/webp/WebPImage;->a([B)Lcom/facebook/animated/webp/WebPImage;

    move-result-object v1

    .line 2746659
    invoke-direct {p0, v1, p2}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a(LX/1GB;Ljava/lang/String;)LX/JtM;

    move-result-object v0

    .line 2746660
    invoke-virtual {v1}, Lcom/facebook/animated/webp/WebPImage;->h()V

    goto :goto_0

    .line 2746661
    :cond_4
    new-instance v0, LX/JtM;

    invoke-direct {v0, p2, p1}, LX/JtM;-><init>(Ljava/lang/String;[B)V

    goto :goto_0

    .line 2746662
    :catch_3
    move-exception v1

    goto :goto_5

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<[B>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2746663
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2746664
    iget-object v2, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 2746665
    invoke-static {v2}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2746666
    invoke-virtual {p1}, LX/1bf;->n()Ljava/io/File;

    move-result-object v0

    .line 2746667
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2746668
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [B
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2746669
    :try_start_2
    invoke-static {v3, v0}, LX/0hW;->a(Ljava/io/InputStream;[B)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2746670
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 2746671
    :goto_0
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2746672
    :goto_1
    return-object v0

    .line 2746673
    :catch_0
    move-exception v0

    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2746674
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :goto_4
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 2746675
    :goto_5
    sget-object v2, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v3, "Exception loading media file from File System"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2746676
    :catch_2
    move-exception v3

    :try_start_7
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_4

    .line 2746677
    :cond_1
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->f:LX/1HI;

    sget-object v2, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p1, v2}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 2746678
    new-instance v2, LX/JtL;

    invoke-direct {v2, p0, v0}, LX/JtL;-><init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v3, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->e:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v2, v3}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2746679
    :catch_3
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catchall_2
    move-exception v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    goto :goto_3

    .line 2746680
    :catch_5
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2
.end method
