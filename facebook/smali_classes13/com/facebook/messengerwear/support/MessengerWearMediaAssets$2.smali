.class public final Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JtM;

.field public final synthetic b:LX/Jt0;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:LX/JtG;


# direct methods
.method public constructor <init>(LX/JtG;LX/JtM;LX/Jt0;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2746529
    iput-object p1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->d:LX/JtG;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iput-object p3, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->b:LX/Jt0;

    iput-object p4, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    const/4 v0, 0x0

    .line 2746482
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->d:LX/JtG;

    iget-object v1, v1, LX/JtG;->d:LX/JtZ;

    invoke-virtual {v1}, LX/JtZ;->a()LX/2wX;

    move-result-object v2

    .line 2746483
    :try_start_0
    invoke-virtual {v2}, LX/2wX;->f()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v1

    .line 2746484
    invoke-virtual {v1}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2746485
    sget-object v0, LX/JtG;->a:Ljava/lang/Class;

    const-string v1, "Connection to Google API failed"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2746486
    invoke-virtual {v2}, LX/2wX;->g()V

    .line 2746487
    :goto_0
    return-void

    .line 2746488
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iget-object v1, v1, LX/JtM;->a:Ljava/lang/String;

    invoke-static {v1}, LX/Jt2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2746489
    const/4 v1, 0x0

    .line 2746490
    sget-object v4, LX/JtF;->a:[I

    iget-object v5, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->b:LX/Jt0;

    invoke-virtual {v5}, LX/Jt0;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2746491
    :goto_1
    iget-object v4, v1, LX/KAx;->b:LX/KAr;

    move-object v4, v4

    .line 2746492
    const-string v5, "timestamp"

    iget-object v6, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->d:LX/JtG;

    iget-object v6, v6, LX/JtG;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, LX/KAr;->a(Ljava/lang/String;J)V

    .line 2746493
    const-string v5, "media_id"

    invoke-virtual {v4, v5, v3}, LX/KAr;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2746494
    iget-object v3, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iget-object v3, v3, LX/JtM;->b:[[B

    array-length v3, v3

    .line 2746495
    new-array v5, v3, [Lcom/google/android/gms/wearable/Asset;

    .line 2746496
    const/4 v6, 0x1

    if-le v3, v6, :cond_3

    .line 2746497
    const-string v6, "frame_count"

    invoke-virtual {v4, v6, v3}, LX/KAr;->a(Ljava/lang/String;I)V

    .line 2746498
    const-string v6, "loop_count"

    iget-object v7, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iget v7, v7, LX/JtM;->d:I

    invoke-virtual {v4, v6, v7}, LX/KAr;->a(Ljava/lang/String;I)V

    .line 2746499
    const-string v6, "frame_durations"

    iget-object v7, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iget-object v7, v7, LX/JtM;->c:[I

    .line 2746500
    array-length v10, v7

    .line 2746501
    new-array v11, v10, [J

    .line 2746502
    const/4 v9, 0x0

    :goto_2
    if-ge v9, v10, :cond_1

    .line 2746503
    aget v12, v7, v9

    int-to-long v13, v12

    aput-wide v13, v11, v9

    .line 2746504
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 2746505
    :cond_1
    move-object v7, v11

    .line 2746506
    invoke-virtual {v4, v6, v7}, LX/KAr;->a(Ljava/lang/String;[J)V

    .line 2746507
    new-array v6, v3, [Ljava/lang/String;

    .line 2746508
    :goto_3
    if-ge v0, v3, :cond_2

    .line 2746509
    iget-object v7, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iget-object v7, v7, LX/JtM;->b:[[B

    aget-object v7, v7, v0

    invoke-static {v7}, LX/Jt2;->a([B)[B

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/wearable/Asset;->a([B)Lcom/google/android/gms/wearable/Asset;

    move-result-object v7

    aput-object v7, v5, v0

    .line 2746510
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "frame_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    .line 2746511
    aget-object v7, v6, v0

    aget-object v8, v5, v0

    invoke-virtual {v4, v7, v8}, LX/KAr;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)V

    .line 2746512
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2746513
    :pswitch_0
    invoke-static {v3}, LX/KCf;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/KAx;->a(Ljava/lang/String;)LX/KAx;

    move-result-object v1

    goto :goto_1

    .line 2746514
    :pswitch_1
    invoke-static {v3}, LX/KCf;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/KAx;->a(Ljava/lang/String;)LX/KAx;

    move-result-object v1

    goto/16 :goto_1

    .line 2746515
    :cond_2
    const-string v0, "frame_ids"

    invoke-virtual {v4, v0, v6}, LX/KAr;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2746516
    :goto_4
    invoke-virtual {v1}, LX/KAx;->c()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v0

    .line 2746517
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataRequest;->g()Lcom/google/android/gms/wearable/PutDataRequest;

    .line 2746518
    sget-object v3, LX/KB1;->a:LX/KAl;

    invoke-interface {v3, v2, v0}, LX/KAl;->a(LX/2wX;Lcom/google/android/gms/wearable/PutDataRequest;)LX/2wg;

    move-result-object v0

    .line 2746519
    invoke-virtual {v0}, LX/2wg;->a()LX/2NW;

    move-result-object v0

    check-cast v0, LX/KAj;

    .line 2746520
    invoke-interface {v0}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    .line 2746521
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x337f7056

    invoke-static {v0, v5, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2746522
    invoke-virtual {v2}, LX/2wX;->g()V

    goto/16 :goto_0

    .line 2746523
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iget-object v3, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->a:LX/JtM;

    iget-object v3, v3, LX/JtM;->b:[[B

    const/4 v6, 0x0

    aget-object v3, v3, v6

    invoke-static {v3}, LX/Jt2;->a([B)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/wearable/Asset;->a([B)Lcom/google/android/gms/wearable/Asset;

    move-result-object v3

    aput-object v3, v5, v0

    .line 2746524
    const-string v0, "static_asset"

    const/4 v3, 0x0

    aget-object v3, v5, v3

    invoke-virtual {v4, v0, v3}, LX/KAr;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 2746525
    :catch_0
    move-exception v0

    .line 2746526
    :try_start_3
    sget-object v1, LX/JtG;->a:Ljava/lang/Class;

    const-string v3, "Exception occured while saving an asset"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746527
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2746528
    invoke-virtual {v2}, LX/2wX;->g()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/2wX;->g()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
