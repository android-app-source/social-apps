.class public final Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JtS;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/JtG;


# direct methods
.method public constructor <init>(LX/JtG;LX/JtS;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2746438
    iput-object p1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->c:LX/JtG;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->a:LX/JtS;

    iput-object p3, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 2746439
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->c:LX/JtG;

    iget-object v1, v1, LX/JtG;->d:LX/JtZ;

    invoke-virtual {v1}, LX/JtZ;->a()LX/2wX;

    move-result-object v3

    .line 2746440
    :try_start_0
    invoke-virtual {v3}, LX/2wX;->f()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v1

    .line 2746441
    invoke-virtual {v1}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2746442
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x77c43cf4

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2746443
    invoke-virtual {v3}, LX/2wX;->g()V

    .line 2746444
    :goto_0
    return-void

    .line 2746445
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->a:LX/JtS;

    iget-object v1, v1, LX/JtS;->b:Ljava/lang/String;

    invoke-static {v1}, LX/Jt2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2746446
    sget-object v4, LX/JtF;->a:[I

    iget-object v5, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->a:LX/JtS;

    iget-object v5, v5, LX/JtS;->c:LX/Jt0;

    invoke-virtual {v5}, LX/Jt0;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2746447
    :goto_1
    sget-object v1, LX/KB1;->a:LX/KAl;

    invoke-interface {v1, v3, v0}, LX/KAl;->a(LX/2wX;Landroid/net/Uri;)LX/2wg;

    move-result-object v0

    .line 2746448
    invoke-virtual {v0}, LX/2wg;->a()LX/2NW;

    move-result-object v0

    check-cast v0, LX/KAq;

    .line 2746449
    invoke-virtual {v0}, LX/KAq;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2746450
    invoke-virtual {v0}, LX/KAq;->a()Lcom/google/android/gms/common/api/Status;

    .line 2746451
    invoke-virtual {v0}, LX/KAq;->a()Lcom/google/android/gms/common/api/Status;

    .line 2746452
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x5e01e194

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2746453
    invoke-virtual {v3}, LX/2wX;->g()V

    goto :goto_0

    .line 2746454
    :pswitch_0
    :try_start_2
    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2746455
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "wear:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/KCf;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, v1

    .line 2746456
    goto :goto_1

    .line 2746457
    :pswitch_1
    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2746458
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "wear:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/KCf;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, v1

    .line 2746459
    goto :goto_1

    .line 2746460
    :cond_1
    invoke-virtual {v0}, LX/4sY;->b()I

    move-result v1

    if-nez v1, :cond_2

    .line 2746461
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x4c12dd0c

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2746462
    invoke-virtual {v3}, LX/2wX;->g()V

    goto/16 :goto_0

    .line 2746463
    :cond_2
    :try_start_3
    invoke-virtual {v0}, LX/4sY;->b()I

    .line 2746464
    invoke-virtual {v0}, LX/4sY;->b()I

    move-result v1

    if-nez v1, :cond_3

    .line 2746465
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v4, 0x0

    const v5, 0x5cc588be

    invoke-static {v1, v4, v5}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746466
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4sj;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/KAo;

    .line 2746467
    invoke-static {v1}, LX/KAs;->a(LX/KAo;)LX/KAs;

    move-result-object v1

    iget-object v4, v1, LX/KAs;->b:LX/KAr;

    move-object v4, v4

    .line 2746468
    const-string v1, "frame_count"

    const/4 v5, 0x0

    iget-object v7, v4, LX/KAr;->a:Ljava/util/HashMap;

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_6

    :goto_2
    move v5, v5

    move v5, v5

    .line 2746469
    if-nez v5, :cond_5

    .line 2746470
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/wearable/Asset;

    .line 2746471
    const/4 v2, 0x0

    const-string v5, "static_asset"

    invoke-virtual {v4, v5}, LX/KAr;->e(Ljava/lang/String;)Lcom/google/android/gms/wearable/Asset;

    move-result-object v4

    aput-object v4, v1, v2

    .line 2746472
    :cond_4
    iget-object v2, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v4, 0x3da3b10c

    invoke-static {v2, v1, v4}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746473
    invoke-virtual {v0}, LX/4sY;->nc_()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2746474
    invoke-virtual {v3}, LX/2wX;->g()V

    goto/16 :goto_0

    .line 2746475
    :cond_5
    :try_start_4
    new-array v1, v5, [Lcom/google/android/gms/wearable/Asset;

    .line 2746476
    :goto_3
    if-ge v2, v5, :cond_4

    .line 2746477
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "frame_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/KAr;->e(Ljava/lang/String;)Lcom/google/android/gms/wearable/Asset;

    move-result-object v6

    aput-object v6, v1, v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2746478
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2746479
    :catch_0
    move-exception v0

    .line 2746480
    :try_start_5
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2746481
    invoke-virtual {v3}, LX/2wX;->g()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/2wX;->g()V

    throw v0

    :cond_6
    :try_start_6
    move-object v6, v8

    check-cast v6, Ljava/lang/Integer;

    move-object v7, v6

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_6
    .catch Ljava/lang/ClassCastException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v5

    goto :goto_2

    :catch_1
    move-exception v7

    const-string v9, "Integer"

    invoke-static {v1, v8, v9, v7}, LX/KAr;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
