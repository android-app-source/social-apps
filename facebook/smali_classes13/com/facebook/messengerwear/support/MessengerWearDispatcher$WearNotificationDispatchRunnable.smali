.class public final Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JtE;

.field private final b:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

.field private final c:[B

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/JtT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[B)V
    .locals 1

    .prologue
    .line 2746356
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;-><init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[BLjava/util/List;)V

    .line 2746357
    return-void
.end method

.method public constructor <init>(LX/JtE;Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[BLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;",
            "[B",
            "Ljava/util/List",
            "<",
            "LX/JtT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2746358
    iput-object p1, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->a:LX/JtE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746359
    iput-object p2, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->b:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    .line 2746360
    iput-object p3, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->c:[B

    .line 2746361
    iput-object p4, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->d:Ljava/util/List;

    .line 2746362
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1e

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 2746363
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->a:LX/JtE;

    iget-object v0, v0, LX/JtE;->d:LX/2wX;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v12, v13, v1}, LX/2wX;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 2746364
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2746365
    sget-object v1, LX/JtE;->a:Ljava/lang/Class;

    const-string v3, "Unable to connection to Google Api: %d - %s"

    new-array v4, v11, [Ljava/lang/Object;

    iget v5, v0, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v5, v5

    .line 2746366
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v2, v0, Lcom/google/android/gms/common/ConnectionResult;->e:Ljava/lang/String;

    move-object v0, v2

    .line 2746367
    aput-object v0, v4, v10

    invoke-static {v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2746368
    :goto_0
    return-void

    .line 2746369
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->b:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    iget-object v0, v0, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->a:Ljava/lang/String;

    invoke-static {v0}, LX/Jt2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2746370
    invoke-static {v0}, LX/KCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/KAx;->a(Ljava/lang/String;)LX/KAx;

    move-result-object v5

    .line 2746371
    iget-object v0, v5, LX/KAx;->b:LX/KAr;

    move-object v6, v0

    .line 2746372
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 2746373
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->b:Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2746374
    const-string v1, "thread_data_obj"

    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    invoke-static {v0}, LX/Jt2;->a([B)[B

    move-result-object v0

    invoke-virtual {v6, v1, v0}, LX/KAr;->a(Ljava/lang/String;[B)V

    .line 2746375
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->c:[B

    if-eqz v0, :cond_1

    .line 2746376
    const-string v0, "thread_picture"

    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->c:[B

    invoke-static {v1}, LX/Jt2;->a([B)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wearable/Asset;->a([B)Lcom/google/android/gms/wearable/Asset;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/KAr;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2746377
    :cond_1
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->d:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 2746378
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JtT;

    .line 2746379
    iget-object v3, v0, LX/JtT;->b:[Lcom/google/android/gms/wearable/Asset;

    array-length v8, v3

    move v3, v2

    .line 2746380
    :goto_1
    if-ge v3, v8, :cond_2

    .line 2746381
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v4, "asset_"

    invoke-direct {v9, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v9, v0, LX/JtT;->b:[Lcom/google/android/gms/wearable/Asset;

    aget-object v9, v9, v3

    invoke-virtual {v6, v1, v9}, LX/KAr;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)V

    .line 2746382
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_1

    .line 2746383
    :catch_0
    move-exception v0

    .line 2746384
    sget-object v1, LX/JtE;->a:Ljava/lang/Class;

    const-string v2, "Unable to encrypt notification"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746385
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->a:LX/JtE;

    iget-object v0, v0, LX/JtE;->d:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    goto/16 :goto_0

    .line 2746386
    :cond_3
    const-string v0, "ts"

    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->a:LX/JtE;

    iget-object v1, v1, LX/JtE;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual {v6, v0, v8, v9}, LX/KAr;->a(Ljava/lang/String;J)V

    .line 2746387
    invoke-virtual {v5}, LX/KAx;->c()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v0

    .line 2746388
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataRequest;->g()Lcom/google/android/gms/wearable/PutDataRequest;

    .line 2746389
    sget-object v1, LX/KB1;->a:LX/KAl;

    iget-object v3, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->a:LX/JtE;

    iget-object v3, v3, LX/JtE;->d:LX/2wX;

    invoke-interface {v1, v3, v0}, LX/KAl;->a(LX/2wX;Lcom/google/android/gms/wearable/PutDataRequest;)LX/2wg;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v12, v13, v1}, LX/2wg;->a(JLjava/util/concurrent/TimeUnit;)LX/2NW;

    move-result-object v0

    check-cast v0, LX/KAj;

    .line 2746390
    invoke-interface {v0}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2746391
    sget-object v1, LX/JtE;->a:Ljava/lang/Class;

    const-string v3, "Failed to set DataItem: %d - %s"

    new-array v4, v11, [Ljava/lang/Object;

    invoke-interface {v0}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v5

    iget v6, v5, Lcom/google/android/gms/common/api/Status;->i:I

    move v5, v6

    .line 2746392
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-interface {v0}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/gms/common/api/Status;->j:Ljava/lang/String;

    move-object v0, v2

    .line 2746393
    aput-object v0, v4, v10

    invoke-static {v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2746394
    :cond_4
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDispatcher$WearNotificationDispatchRunnable;->a:LX/JtE;

    iget-object v0, v0, LX/JtE;->d:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    goto/16 :goto_0
.end method
