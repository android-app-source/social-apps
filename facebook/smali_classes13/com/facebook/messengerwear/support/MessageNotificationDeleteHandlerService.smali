.class public Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;
.super LX/0te;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public b:LX/JtZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746292
    const-class v0, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;

    sput-object v0, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2746301
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;LX/JtZ;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 2746300
    iput-object p1, p0, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;->b:LX/JtZ;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;->c:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;

    invoke-static {v1}, LX/JtZ;->a(LX/0QB;)LX/JtZ;

    move-result-object v0

    check-cast v0, LX/JtZ;

    invoke-static {v1}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, v1}, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;->a(Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;LX/JtZ;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2746299
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x52c4c744

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2746296
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2746297
    invoke-static {p0, p0}, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2746298
    const/16 v1, 0x25

    const v2, -0x3a422b9a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x3f8e9af9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2746293
    const-string v1, "thread_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2746294
    iget-object v2, p0, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService$1;

    invoke-direct {v3, p0, v1, p3}, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService$1;-><init>(Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;Ljava/lang/String;I)V

    const v1, -0x6b595561

    invoke-static {v2, v3, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2746295
    const/4 v1, 0x1

    const/16 v2, 0x25

    const v3, -0x1f1875d2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
