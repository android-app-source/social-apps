.class public Lcom/facebook/messengerwear/support/WearMessageActionListenerService;
.super LX/JtC;
.source ""

# interfaces
.implements LX/0Xn;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0jb;

.field public c:Lcom/facebook/messaging/send/client/SendMessageManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/It1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/It0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/2Mq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:LX/6eF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746980
    const-class v0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2746978
    invoke-direct {p0}, LX/JtC;-><init>()V

    .line 2746979
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->b:LX/0jb;

    return-void
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2746975
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Di5;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/Di5;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2746976
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messengerwear/support/WearMessageActionListenerService$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService$1;-><init>(Lcom/facebook/messengerwear/support/WearMessageActionListenerService;Lcom/facebook/messaging/model/messages/Message;)V

    const v2, -0x4f6393e0

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2746977
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 3

    .prologue
    .line 2746970
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->j:LX/6eF;

    invoke-virtual {v0, p1}, LX/6eF;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v0

    .line 2746971
    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2746972
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2746973
    iget-object v1, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2746974
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2746954
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2746955
    const-string v1, "com.facebook.messaging.mutators.ThreadNotificationsDialogActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 2746956
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2746957
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2746958
    new-instance v1, LX/3qF;

    const-string v2, "voice_reply"

    invoke-direct {v1, v2}, LX/3qF;-><init>(Ljava/lang/String;)V

    const-string v2, "Mute"

    .line 2746959
    iput-object v2, v1, LX/3qF;->b:Ljava/lang/CharSequence;

    .line 2746960
    move-object v1, v1

    .line 2746961
    iput-boolean v4, v1, LX/3qF;->d:Z

    .line 2746962
    move-object v1, v1

    .line 2746963
    invoke-virtual {v1}, LX/3qF;->a()LX/3qL;

    move-result-object v1

    .line 2746964
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2746965
    invoke-virtual {v1}, LX/3qL;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2746966
    const/4 v3, 0x1

    new-array v3, v3, [LX/3qL;

    aput-object v1, v3, v4

    .line 2746967
    sget-object v1, LX/3qL;->g:LX/3qG;

    invoke-interface {v1, v3, v0, v2}, LX/3qG;->a([LX/3qL;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 2746968
    iget-object v1, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2746969
    return-void
.end method

.method private static a(Lcom/facebook/messengerwear/support/WearMessageActionListenerService;Lcom/facebook/messaging/send/client/SendMessageManager;LX/It1;LX/It0;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/2Mq;Lcom/facebook/content/SecureContextHelper;LX/6eF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messengerwear/support/WearMessageActionListenerService;",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            "LX/It1;",
            "LX/It0;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2Mq;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/6eF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2746953
    iput-object p1, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->d:LX/It1;

    iput-object p3, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->e:LX/It0;

    iput-object p4, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->f:LX/0Or;

    iput-object p5, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->g:Ljava/util/concurrent/ExecutorService;

    iput-object p6, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->h:LX/2Mq;

    iput-object p7, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->i:Lcom/facebook/content/SecureContextHelper;

    iput-object p8, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->j:LX/6eF;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;

    invoke-static {v8}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v8}, LX/It1;->a(LX/0QB;)LX/It1;

    move-result-object v2

    check-cast v2, LX/It1;

    invoke-static {v8}, LX/It0;->a(LX/0QB;)LX/It0;

    move-result-object v3

    check-cast v3, LX/It0;

    const/16 v4, 0x2847

    invoke-static {v8, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v8}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v8}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v6

    check-cast v6, LX/2Mq;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/6eF;->a(LX/0QB;)LX/6eF;

    move-result-object v8

    check-cast v8, LX/6eF;

    invoke-static/range {v0 .. v8}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Lcom/facebook/messengerwear/support/WearMessageActionListenerService;Lcom/facebook/messaging/send/client/SendMessageManager;LX/It1;LX/It0;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/2Mq;Lcom/facebook/content/SecureContextHelper;LX/6eF;)V

    return-void
.end method


# virtual methods
.method public final a(LX/KAu;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2746909
    invoke-interface {p1}, LX/KAu;->a()Ljava/lang/String;

    move-result-object v0

    .line 2746910
    const-string v1, "/action"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2746911
    :goto_0
    return-void

    .line 2746912
    :cond_0
    invoke-static {p0, p0}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2746913
    :try_start_0
    invoke-interface {p1}, LX/KAu;->b()[B

    move-result-object v0

    .line 2746914
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {v1}, LX/Jt2;->b(Ljava/nio/ByteBuffer;)[B

    move-result-object v1

    move-object v0, v1

    .line 2746915
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2746916
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 2746917
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 2746918
    sget-object v0, Lcom/facebook/messengerwear/shared/ActionMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/shared/ActionMessage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2746919
    iget-object v1, v0, Lcom/facebook/messengerwear/shared/ActionMessage;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2746920
    if-nez v1, :cond_1

    .line 2746921
    sget-object v1, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a:Ljava/lang/String;

    const-string v2, "Error decoding Thread Key from %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/facebook/messengerwear/shared/ActionMessage;->b:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2746922
    :catch_0
    move-exception v0

    .line 2746923
    sget-object v1, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a:Ljava/lang/String;

    const-string v2, "Error parsing message"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2746924
    :cond_1
    sget-object v2, LX/JtY;->a:[I

    iget-object v3, v0, Lcom/facebook/messengerwear/shared/ActionMessage;->c:LX/Jss;

    invoke-virtual {v3}, LX/Jss;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2746925
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->d:LX/It1;

    iget-object v0, v0, Lcom/facebook/messengerwear/shared/ActionMessage;->a:Ljava/lang/String;

    .line 2746926
    invoke-static {v2, v1}, LX/It1;->a(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    move-result-object v3

    .line 2746927
    iput-object v0, v3, LX/6f7;->k:Ljava/lang/String;

    .line 2746928
    move-object v3, v3

    .line 2746929
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    move-object v0, v3

    .line 2746930
    invoke-direct {p0, v0}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    .line 2746931
    :pswitch_1
    new-instance v0, LX/Iu1;

    invoke-direct {v0}, LX/Iu1;-><init>()V

    const-string v2, "369239263222822"

    .line 2746932
    iput-object v2, v0, LX/Iu1;->c:Ljava/lang/String;

    .line 2746933
    move-object v0, v0

    .line 2746934
    iget-object v2, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->e:LX/It0;

    invoke-virtual {v2}, LX/It0;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 2746935
    iput-object v2, v0, LX/Iu1;->d:Ljava/lang/String;

    .line 2746936
    move-object v0, v0

    .line 2746937
    iget-object v2, v0, LX/Iu1;->d:Ljava/lang/String;

    const-string v3, "Attempting to create without offline threading id"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2746938
    new-instance v2, LX/Iu2;

    invoke-direct {v2, v0}, LX/Iu2;-><init>(LX/Iu1;)V

    move-object v0, v2

    .line 2746939
    iget-object v2, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->d:LX/It1;

    .line 2746940
    iget-object v3, v0, LX/Iu2;->d:Ljava/lang/String;

    invoke-static {v2, v1, v3}, LX/It1;->c(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;

    move-result-object v3

    iget-object v4, v0, LX/Iu2;->c:Ljava/lang/String;

    .line 2746941
    iput-object v4, v3, LX/6f7;->k:Ljava/lang/String;

    .line 2746942
    move-object v3, v3

    .line 2746943
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    move-object v0, v3

    .line 2746944
    invoke-direct {p0, v0}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto/16 :goto_0

    .line 2746945
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->d:LX/It1;

    iget-object v0, v0, Lcom/facebook/messengerwear/shared/ActionMessage;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/It1;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto/16 :goto_0

    .line 2746946
    :pswitch_3
    iget-object v0, v0, Lcom/facebook/messengerwear/shared/ActionMessage;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2746947
    :pswitch_4
    invoke-direct {p0, v1}, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2746952
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->b:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x646a4873

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2746950
    invoke-super {p0}, LX/JtC;->onCreate()V

    .line 2746951
    const/16 v1, 0x25

    const v2, -0x4fdeaa40

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2746948
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearMessageActionListenerService;->b:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2746949
    return-void
.end method
