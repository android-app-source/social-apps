.class public Lcom/facebook/messengerwear/support/MessengerWearDetectionService;
.super LX/JtC;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JtX;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/3Ri;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2746347
    invoke-direct {p0}, LX/JtC;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messengerwear/support/MessengerWearDetectionService;LX/0Or;LX/3Ri;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messengerwear/support/MessengerWearDetectionService;",
            "LX/0Or",
            "<",
            "LX/JtX;",
            ">;",
            "LX/3Ri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2746336
    iput-object p1, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->b:LX/3Ri;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;

    const/16 v1, 0x2a6b

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/3Ri;->b(LX/0QB;)LX/3Ri;

    move-result-object v0

    check-cast v0, LX/3Ri;

    invoke-static {p0, v1, v0}, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->a(Lcom/facebook/messengerwear/support/MessengerWearDetectionService;LX/0Or;LX/3Ri;)V

    return-void
.end method


# virtual methods
.method public final a(LX/KAh;)V
    .locals 2

    .prologue
    .line 2746337
    invoke-super {p0, p1}, LX/JtC;->a(LX/KAh;)V

    .line 2746338
    invoke-static {p0, p0}, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2746339
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->b:LX/3Ri;

    invoke-virtual {v0}, LX/3Ri;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2746340
    :goto_0
    return-void

    .line 2746341
    :cond_0
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->b:LX/3Ri;

    invoke-virtual {v0}, LX/3Ri;->a()Z

    move-result v0

    .line 2746342
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->b:LX/3Ri;

    invoke-virtual {v1, p1}, LX/3Ri;->a(LX/KAh;)V

    .line 2746343
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->b:LX/3Ri;

    invoke-virtual {v0}, LX/3Ri;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2746344
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearDetectionService;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JtX;

    .line 2746345
    invoke-virtual {v0}, LX/JtX;->a()V

    .line 2746346
    :cond_1
    invoke-interface {p1}, LX/KAh;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    goto :goto_0
.end method
