.class public final Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/facebook/messengerwear/support/WearNotificationListenerService;


# direct methods
.method public constructor <init>(Lcom/facebook/messengerwear/support/WearNotificationListenerService;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 2746981
    iput-object p1, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;->b:Lcom/facebook/messengerwear/support/WearNotificationListenerService;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2746982
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;->b:Lcom/facebook/messengerwear/support/WearNotificationListenerService;

    iget-object v0, v0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->e:LX/JtZ;

    invoke-virtual {v0}, LX/JtZ;->a()LX/2wX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;->a:Ljava/util/ArrayList;

    const-wide/16 v6, 0xbb8

    .line 2746983
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v2}, LX/2wX;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v2

    .line 2746984
    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2746985
    :goto_0
    return-void

    .line 2746986
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2746987
    sget-object v5, LX/KB1;->a:LX/KAl;

    invoke-interface {v5, v0, v2}, LX/KAl;->b(LX/2wX;Landroid/net/Uri;)LX/2wg;

    move-result-object v2

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v6, v7, v5}, LX/2wg;->a(JLjava/util/concurrent/TimeUnit;)LX/2NW;

    move-result-object v2

    check-cast v2, LX/KAk;

    .line 2746988
    invoke-interface {v2}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->e()Z

    .line 2746989
    goto :goto_1

    .line 2746990
    :cond_1
    invoke-virtual {v0}, LX/2wX;->g()V

    goto :goto_0
.end method
