.class public final Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2746574
    iput-object p1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2746575
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v0, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->d:LX/8jI;

    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8jI;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2746576
    new-instance v1, LX/JtK;

    invoke-direct {v1, p0}, LX/JtK;-><init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;)V

    iget-object v2, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v2, v2, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2746577
    :goto_0
    return-void

    .line 2746578
    :catch_0
    move-exception v0

    .line 2746579
    sget-object v1, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v2, "Fetch sticker failed..."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746580
    iget-object v1, p0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
