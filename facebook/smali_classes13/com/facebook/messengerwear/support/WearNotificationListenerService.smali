.class public Lcom/facebook/messengerwear/support/WearNotificationListenerService;
.super LX/JtC;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.io.ObjectInputStream.readObject"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bae;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/JtZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2747049
    const-class v0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;

    sput-object v0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2747048
    invoke-direct {p0}, LX/JtC;-><init>()V

    return-void
.end method

.method private static a([B)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2747045
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2747046
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2747047
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/KAo;)V
    .locals 7

    .prologue
    .line 2747037
    invoke-static {p1}, LX/KAs;->a(LX/KAo;)LX/KAs;

    move-result-object v0

    iget-object v1, v0, LX/KAs;->b:LX/KAr;

    move-object v0, v1

    .line 2747038
    const-string v1, "category"

    invoke-virtual {v0, v1}, LX/KAr;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2747039
    const-string v1, "message"

    invoke-virtual {v0, v1}, LX/KAr;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2747040
    const-string v1, "cause"

    const/4 v5, 0x0

    iget-object v4, v0, LX/KAr;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    move-object v4, v5

    :goto_0
    move-object v0, v4

    .line 2747041
    :try_start_0
    invoke-static {v0}, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 2747042
    :goto_1
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2747043
    return-void

    .line 2747044
    :catch_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    :cond_0
    :try_start_1
    check-cast v4, [B
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v6

    const-string p1, "byte[]"

    invoke-static {v1, v4, p1, v6}, LX/KAr;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    move-object v4, v5

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messengerwear/support/WearNotificationListenerService;LX/0Or;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/JtZ;LX/0Zb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messengerwear/support/WearNotificationListenerService;",
            "LX/0Or",
            "<",
            "LX/Bae;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/JtZ;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2747036
    iput-object p1, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->b:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->c:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->d:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->e:LX/JtZ;

    iput-object p5, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->f:LX/0Zb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;

    const/16 v1, 0x17f2

    invoke-static {v5, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x259

    invoke-static {v5, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v5}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v5}, LX/JtZ;->a(LX/0QB;)LX/JtZ;

    move-result-object v4

    check-cast v4, LX/JtZ;

    invoke-static {v5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static/range {v0 .. v5}, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a(Lcom/facebook/messengerwear/support/WearNotificationListenerService;LX/0Or;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/JtZ;LX/0Zb;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2747023
    const-string v0, "/threads/"

    invoke-static {p1, v0}, LX/KCf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2747024
    :try_start_0
    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 2747025
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 2747026
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    sget-object v4, LX/Jt2;->b:[B

    const-string p1, "AES"

    invoke-direct {v3, v4, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 2747027
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 2747028
    invoke-virtual {v2, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 2747029
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    move-object v0, v2

    .line 2747030
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/2b2;->k:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2747031
    const-string v2, "thread_key_string"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2747032
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bae;

    invoke-virtual {v0, v1, p0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2747033
    :goto_0
    return-void

    .line 2747034
    :catch_0
    move-exception v0

    .line 2747035
    sget-object v1, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a:Ljava/lang/Class;

    const-string v2, "Failed to decrypt threadKey"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b(LX/KAo;)V
    .locals 6

    .prologue
    .line 2747008
    invoke-static {p1}, LX/KAs;->a(LX/KAo;)LX/KAs;

    move-result-object v0

    iget-object v1, v0, LX/KAs;->b:LX/KAr;

    move-object v0, v1

    .line 2747009
    const-string v1, "event"

    invoke-virtual {v0, v1}, LX/KAr;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2747010
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2747011
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Analytic event expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2747012
    :cond_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2747013
    const-string v1, "params"

    iget-object v3, v0, LX/KAr;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    move v1, v3

    .line 2747014
    if-eqz v1, :cond_2

    .line 2747015
    const-string v1, "params"

    const/4 v4, 0x0

    iget-object v3, v0, LX/KAr;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    move-object v3, v4

    :goto_0
    move-object v0, v3

    .line 2747016
    invoke-virtual {v0}, LX/KAr;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 2747017
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2747018
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2747019
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2747020
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1

    .line 2747021
    :cond_2
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->f:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2747022
    return-void

    :cond_3
    :try_start_0
    check-cast v3, LX/KAr;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    const-string p1, "DataMap"

    invoke-static {v1, v3, p1, v5}, LX/KAr;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    move-object v3, v4

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/KAn;)V
    .locals 5

    .prologue
    .line 2746991
    invoke-super {p0, p1}, LX/JtC;->a(LX/KAn;)V

    .line 2746992
    invoke-static {p0, p0}, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2746993
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2746994
    invoke-virtual {p1}, LX/4sY;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/KAm;

    .line 2746995
    invoke-interface {v0}, LX/4sd;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/KAm;

    invoke-interface {v1}, LX/KAm;->b()LX/KAo;

    move-result-object v1

    .line 2746996
    invoke-interface {v1}, LX/KAo;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 2746997
    invoke-interface {v0}, LX/KAm;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2746998
    :pswitch_0
    const-string v0, "/reporting"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2746999
    invoke-direct {p0, v1}, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a(LX/KAo;)V

    .line 2747000
    invoke-interface {v1}, LX/KAo;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2747001
    :cond_1
    const-string v0, "/analytics"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2747002
    invoke-direct {p0, v1}, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->b(LX/KAo;)V

    .line 2747003
    invoke-interface {v1}, LX/KAo;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2747004
    :pswitch_1
    const-string v0, "/threads/"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2747005
    invoke-direct {p0, v4}, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2747006
    :cond_2
    iget-object v0, p0, Lcom/facebook/messengerwear/support/WearNotificationListenerService;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;

    invoke-direct {v1, p0, v2}, Lcom/facebook/messengerwear/support/WearNotificationListenerService$1;-><init>(Lcom/facebook/messengerwear/support/WearNotificationListenerService;Ljava/util/ArrayList;)V

    const v2, -0xefc5023

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2747007
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
