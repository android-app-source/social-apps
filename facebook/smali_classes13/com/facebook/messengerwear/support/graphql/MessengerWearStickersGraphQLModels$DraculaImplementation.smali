.class public final Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2747147
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2747148
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2747149
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2747150
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2747110
    if-nez p1, :cond_0

    move v0, v1

    .line 2747111
    :goto_0
    return v0

    .line 2747112
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2747113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2747114
    :pswitch_0
    const-class v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;

    .line 2747115
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2747116
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2747117
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2747118
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4b9596a0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2747138
    if-nez p0, :cond_0

    move v0, v1

    .line 2747139
    :goto_0
    return v0

    .line 2747140
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2747141
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2747142
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2747143
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2747144
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2747145
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2747146
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2747131
    const/4 v7, 0x0

    .line 2747132
    const/4 v1, 0x0

    .line 2747133
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2747134
    invoke-static {v2, v3, v0}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2747135
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2747136
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2747137
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2747130
    new-instance v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2747125
    if-eqz p0, :cond_0

    .line 2747126
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2747127
    if-eq v0, p0, :cond_0

    .line 2747128
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2747129
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2747120
    packed-switch p2, :pswitch_data_0

    .line 2747121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2747122
    :pswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$FetchRecentlyUsedStickersQueryModel$RecentlyUsedStickersModel$EdgesModel$NodeModel;

    .line 2747123
    invoke-static {v0, p3}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2747124
    return-void

    :pswitch_data_0
    .packed-switch 0x4b9596a0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2747119
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2747151
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2747152
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2747079
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2747080
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2747081
    :cond_0
    iput-object p1, p0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2747082
    iput p2, p0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->b:I

    .line 2747083
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2747084
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2747085
    new-instance v0, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2747086
    iget v0, p0, LX/1vt;->c:I

    .line 2747087
    move v0, v0

    .line 2747088
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2747089
    iget v0, p0, LX/1vt;->c:I

    .line 2747090
    move v0, v0

    .line 2747091
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2747092
    iget v0, p0, LX/1vt;->b:I

    .line 2747093
    move v0, v0

    .line 2747094
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747095
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2747096
    move-object v0, v0

    .line 2747097
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2747098
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2747099
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2747100
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2747101
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2747102
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2747103
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2747104
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2747105
    invoke-static {v3, v9, v2}, Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messengerwear/support/graphql/MessengerWearStickersGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2747106
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2747107
    iget v0, p0, LX/1vt;->c:I

    .line 2747108
    move v0, v0

    .line 2747109
    return v0
.end method
