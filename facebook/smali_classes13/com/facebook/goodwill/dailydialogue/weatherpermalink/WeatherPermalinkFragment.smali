.class public Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/CNT;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->DAILY_DIALOGUE_WEATHER_PERMALINK_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/CNt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CNc;

.field private f:Lcom/facebook/components/ComponentView;

.field private g:LX/1De;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2709769
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;

    const-class v1, LX/CNt;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/CNt;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->a:LX/CNt;

    iput-object v2, p1, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->b:LX/0tX;

    iput-object v3, p1, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p0, p1, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->d:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2709770
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2709771
    iget-object v1, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->f:Lcom/facebook/components/ComponentView;

    iget-object v2, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->g:LX/1De;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    iget-object v3, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->e:LX/CNc;

    iget-object v4, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->g:LX/1De;

    invoke-static {v0, v3, v4}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v0

    invoke-static {v2, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2709772
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2709773
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2709774
    const-class v0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;

    invoke-static {v0, p0}, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2709775
    sget-object v0, LX/3j6;->a:LX/0P1;

    sget-object v1, LX/3jH;->a:LX/0P1;

    sget-object v2, LX/3jL;->a:LX/0P1;

    invoke-static {v0, v1, v2}, LX/3jU;->a(LX/0P1;LX/0P1;LX/0P1;)V

    .line 2709776
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->a:LX/CNt;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CNt;->a(Landroid/content/Context;)LX/CNq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->e:LX/CNc;

    .line 2709777
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->e:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iput-object p0, v0, LX/CNS;->b:LX/CNT;

    .line 2709778
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2709779
    if-nez v0, :cond_0

    .line 2709780
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->b:LX/0tX;

    iget-object v1, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->c:Ljava/util/concurrent/ExecutorService;

    .line 2709781
    new-instance v3, LX/DId;

    invoke-direct {v3}, LX/DId;-><init>()V

    move-object v3, v3

    .line 2709782
    new-instance v4, LX/0zR;

    invoke-direct {v4}, LX/0zR;-><init>()V

    const-string v5, "701268200040015"

    invoke-virtual {v4, v5}, LX/0zR;->a(Ljava/lang/String;)LX/0zR;

    move-result-object v4

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v5}, LX/0wC;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0zR;->a(Ljava/lang/Double;)LX/0zR;

    move-result-object v4

    .line 2709783
    const-string v5, "nt_context"

    invoke-virtual {v3, v5, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2709784
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2709785
    invoke-virtual {v0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2709786
    new-instance v4, LX/JaL;

    invoke-direct {v4, p0}, LX/JaL;-><init>(Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;)V

    invoke-static {v3, v4, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2709787
    return-void

    .line 2709788
    :cond_0
    const v1, 0x7f08317e

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2709789
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f083164

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2709790
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 2709791
    move-object v1, v1

    .line 2709792
    iget-object v2, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->d:LX/0wM;

    const v3, 0x7f0208ac

    const/4 p1, -0x1

    invoke-virtual {v2, v3, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2709793
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2709794
    move-object v1, v1

    .line 2709795
    const/4 v2, -0x2

    .line 2709796
    iput v2, v1, LX/108;->h:I

    .line 2709797
    move-object v1, v1

    .line 2709798
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2709799
    new-instance v1, LX/JaK;

    invoke-direct {v1, p0}, LX/JaK;-><init>(Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2709800
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x676b0c65

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2709801
    const v0, 0x7f0303cd

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2709802
    const v0, 0x7f0d0be3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->f:Lcom/facebook/components/ComponentView;

    .line 2709803
    new-instance v0, LX/1De;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/weatherpermalink/WeatherPermalinkFragment;->g:LX/1De;

    .line 2709804
    const/16 v0, 0x2b

    const v3, 0x40aae598

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
