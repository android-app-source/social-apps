.class public Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

.field private final b:Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

.field private final c:Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

.field private final d:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

.field private final e:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2779398
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2779399
    iput-object p2, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->b:Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

    .line 2779400
    iput-object p3, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->c:Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

    .line 2779401
    iput-object p4, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    .line 2779402
    iput-object p1, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->a:Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

    .line 2779403
    iput-object p5, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->e:LX/0Uh;

    .line 2779404
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;
    .locals 9

    .prologue
    .line 2779405
    const-class v1, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;

    monitor-enter v1

    .line 2779406
    :try_start_0
    sget-object v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2779407
    sput-object v2, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2779408
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779409
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2779410
    new-instance v3, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

    invoke-static {v0}, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;-><init>(Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;LX/0Uh;)V

    .line 2779411
    move-object v0, v3

    .line 2779412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2779413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2779414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2779415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2779416
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 2779417
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2779418
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2779419
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->e:LX/0Uh;

    const/16 v1, 0x2dd

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2779420
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->b:Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2779421
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2779422
    :cond_1
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->a:Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2779423
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2779424
    :cond_2
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->e:LX/0Uh;

    const/16 v1, 0x2dc

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779425
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;->c:Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2779426
    const/4 v0, 0x1

    return v0
.end method
