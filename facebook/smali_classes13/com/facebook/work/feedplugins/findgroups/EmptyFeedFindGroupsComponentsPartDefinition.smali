.class public Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/KAE;

.field private final e:LX/KAO;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/KAE;LX/KAO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2779098
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2779099
    iput-object p2, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->d:LX/KAE;

    .line 2779100
    iput-object p3, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->e:LX/KAO;

    .line 2779101
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->f:Ljava/lang/String;

    .line 2779102
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2779124
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->d:LX/KAE;

    const/4 v1, 0x0

    .line 2779125
    new-instance v2, LX/KAD;

    invoke-direct {v2, v0}, LX/KAD;-><init>(LX/KAE;)V

    .line 2779126
    sget-object v3, LX/KAE;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/KAC;

    .line 2779127
    if-nez v3, :cond_0

    .line 2779128
    new-instance v3, LX/KAC;

    invoke-direct {v3}, LX/KAC;-><init>()V

    .line 2779129
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/KAC;->a$redex0(LX/KAC;LX/1De;IILX/KAD;)V

    .line 2779130
    move-object v2, v3

    .line 2779131
    move-object v1, v2

    .line 2779132
    move-object v0, v1

    .line 2779133
    iget-object v1, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->f:Ljava/lang/String;

    .line 2779134
    iget-object v2, v0, LX/KAC;->a:LX/KAD;

    iput-object v1, v2, LX/KAD;->b:Ljava/lang/String;

    .line 2779135
    iget-object v2, v0, LX/KAC;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2779136
    move-object v0, v0

    .line 2779137
    iget-object v1, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->e:LX/KAO;

    .line 2779138
    iget-object v2, v0, LX/KAC;->a:LX/KAD;

    iput-object v1, v2, LX/KAD;->a:LX/KAO;

    .line 2779139
    iget-object v2, v0, LX/KAC;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2779140
    move-object v0, v0

    .line 2779141
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;",
            ">;TE;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 2779121
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;

    move-result-object v0

    .line 2779122
    iget-object v1, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->e:LX/KAO;

    iget-object v2, p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->f:Ljava/lang/String;

    const-string v3, "empty_feed_find_groups_components"

    invoke-virtual {v1, v2, v3}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2779123
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;
    .locals 6

    .prologue
    .line 2779110
    const-class v1, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

    monitor-enter v1

    .line 2779111
    :try_start_0
    sget-object v0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2779112
    sput-object v2, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2779113
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779114
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2779115
    new-instance p0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/KAE;->a(LX/0QB;)LX/KAE;

    move-result-object v4

    check-cast v4, LX/KAE;

    invoke-static {v0}, LX/KAO;->b(LX/0QB;)LX/KAO;

    move-result-object v5

    check-cast v5, LX/KAO;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;-><init>(Landroid/content/Context;LX/KAE;LX/KAO;)V

    .line 2779116
    move-object v0, p0

    .line 2779117
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2779118
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2779119
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2779120
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2779109
    invoke-direct {p0, p1}, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2779108
    invoke-direct {p0, p1}, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;
    .locals 1

    .prologue
    .line 2779107
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2779106
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/work/feedplugins/findgroups/EmptyFeedFindGroupsComponentsPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2779105
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2779103
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2779104
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
