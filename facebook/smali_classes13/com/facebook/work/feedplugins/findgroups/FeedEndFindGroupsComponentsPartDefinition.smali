.class public Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/KAI;

.field private final e:LX/KAO;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/KAI;LX/KAO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2779262
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2779263
    iput-object p2, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->d:LX/KAI;

    .line 2779264
    iput-object p3, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->e:LX/KAO;

    .line 2779265
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->f:Ljava/lang/String;

    .line 2779266
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2779270
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->d:LX/KAI;

    const/4 v1, 0x0

    .line 2779271
    new-instance v2, LX/KAH;

    invoke-direct {v2, v0}, LX/KAH;-><init>(LX/KAI;)V

    .line 2779272
    sget-object v3, LX/KAI;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/KAG;

    .line 2779273
    if-nez v3, :cond_0

    .line 2779274
    new-instance v3, LX/KAG;

    invoke-direct {v3}, LX/KAG;-><init>()V

    .line 2779275
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/KAG;->a$redex0(LX/KAG;LX/1De;IILX/KAH;)V

    .line 2779276
    move-object v2, v3

    .line 2779277
    move-object v1, v2

    .line 2779278
    move-object v0, v1

    .line 2779279
    iget-object v1, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->f:Ljava/lang/String;

    .line 2779280
    iget-object v2, v0, LX/KAG;->a:LX/KAH;

    iput-object v1, v2, LX/KAH;->a:Ljava/lang/String;

    .line 2779281
    iget-object v2, v0, LX/KAG;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2779282
    move-object v0, v0

    .line 2779283
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;",
            ">;TE;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 2779267
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;

    move-result-object v0

    .line 2779268
    iget-object v1, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->e:LX/KAO;

    iget-object v2, p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->f:Ljava/lang/String;

    const-string v3, "feed_end_find_groups_components"

    invoke-virtual {v1, v2, v3}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2779269
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;
    .locals 6

    .prologue
    .line 2779251
    const-class v1, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

    monitor-enter v1

    .line 2779252
    :try_start_0
    sget-object v0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2779253
    sput-object v2, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2779254
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779255
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2779256
    new-instance p0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/KAI;->a(LX/0QB;)LX/KAI;

    move-result-object v4

    check-cast v4, LX/KAI;

    invoke-static {v0}, LX/KAO;->b(LX/0QB;)LX/KAO;

    move-result-object v5

    check-cast v5, LX/KAO;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;-><init>(Landroid/content/Context;LX/KAI;LX/KAO;)V

    .line 2779257
    move-object v0, p0

    .line 2779258
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2779259
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2779260
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2779261
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2779284
    invoke-direct {p0, p1}, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2779250
    invoke-direct {p0, p1}, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;
    .locals 1

    .prologue
    .line 2779249
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2779245
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/work/feedplugins/findgroups/FeedEndFindGroupsComponentsPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2779248
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2779246
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2779247
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
