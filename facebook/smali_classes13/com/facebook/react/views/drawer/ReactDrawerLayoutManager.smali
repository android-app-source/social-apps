.class public Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidDrawerLayout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/K0E;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2758178
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    .line 2758179
    return-void
.end method

.method private static a(LX/5rJ;LX/K0E;)V
    .locals 2

    .prologue
    .line 2758173
    new-instance v1, LX/K0F;

    const-class v0, LX/5rQ;

    invoke-virtual {p0, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2758174
    iget-object p0, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, p0

    .line 2758175
    invoke-direct {v1, p1, v0}, LX/K0F;-><init>(LX/3tW;LX/5s9;)V

    .line 2758176
    iput-object v1, p1, LX/3tW;->t:LX/3tR;

    .line 2758177
    return-void
.end method

.method public static a(LX/K0E;F)V
    .locals 5

    .prologue
    .line 2758167
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2758168
    :try_start_0
    const-class v0, LX/K0E;

    const-string v1, "setDrawerElevation"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2758169
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, LX/5r2;->a(F)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2758170
    :cond_0
    :goto_0
    return-void

    .line 2758171
    :catch_0
    move-exception v0

    .line 2758172
    const-string v1, "React"

    const-string v2, "setDrawerElevation is not available in this version of the support lib."

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(LX/K0E;I)V
    .locals 0

    .prologue
    .line 2758163
    packed-switch p1, :pswitch_data_0

    .line 2758164
    :goto_0
    return-void

    .line 2758165
    :pswitch_0
    invoke-virtual {p0}, LX/K0E;->d()V

    goto :goto_0

    .line 2758166
    :pswitch_1
    invoke-virtual {p0}, LX/K0E;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/K0E;Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 2758130
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 2758131
    new-instance v0, LX/5pA;

    const-string v1, "The Drawer cannot have more than two children"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2758132
    :cond_0
    if-eqz p3, :cond_1

    const/4 v0, 0x1

    if-eq p3, v0, :cond_1

    .line 2758133
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The only valid indices for drawer\'s child are 0 or 1. Got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " instead."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2758134
    :cond_1
    invoke-virtual {p1, p2, p3}, LX/K0E;->addView(Landroid/view/View;I)V

    .line 2758135
    invoke-virtual {p1}, LX/K0E;->f()V

    .line 2758136
    return-void
.end method

.method private static b(LX/5rJ;)LX/K0E;
    .locals 1

    .prologue
    .line 2758162
    new-instance v0, LX/K0E;

    invoke-direct {v0, p0}, LX/K0E;-><init>(LX/5pX;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2758161
    invoke-static {p1}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->b(LX/5rJ;)LX/K0E;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2758160
    check-cast p2, LX/K0E;

    invoke-static {p1, p2}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->a(LX/5rJ;LX/K0E;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2758159
    check-cast p1, LX/K0E;

    invoke-static {p1, p2}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->a(LX/K0E;I)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 2758158
    check-cast p1, LX/K0E;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->a(LX/K0E;Landroid/view/View;I)V

    return-void
.end method

.method public getDrawerWidth(LX/K0E;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "drawerWidth"
    .end annotation

    .prologue
    .line 2758154
    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    .line 2758155
    :goto_0
    invoke-virtual {p1, v0}, LX/K0E;->f(I)V

    .line 2758156
    return-void

    .line 2758157
    :cond_0
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2758153
    const-string v0, "AndroidDrawerLayout"

    return-object v0
.end method

.method public setDrawerLockMode(LX/K0E;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "drawerLockMode"
    .end annotation

    .prologue
    .line 2758145
    if-eqz p2, :cond_0

    const-string v0, "unlocked"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2758146
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/3tW;->setDrawerLockMode(I)V

    .line 2758147
    :goto_0
    return-void

    .line 2758148
    :cond_1
    const-string v0, "locked-closed"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2758149
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/3tW;->setDrawerLockMode(I)V

    goto :goto_0

    .line 2758150
    :cond_2
    const-string v0, "locked-open"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2758151
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/3tW;->setDrawerLockMode(I)V

    goto :goto_0

    .line 2758152
    :cond_3
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown drawerLockMode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDrawerPosition(LX/K0E;I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x800003
        name = "drawerPosition"
    .end annotation

    .prologue
    .line 2758142
    const v0, 0x800003

    if-eq v0, p2, :cond_0

    const v0, 0x800005

    if-ne v0, p2, :cond_1

    .line 2758143
    :cond_0
    invoke-virtual {p1, p2}, LX/K0E;->e(I)V

    return-void

    .line 2758144
    :cond_1
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown drawerPosition "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic setElevation(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 2758141
    check-cast p1, LX/K0E;

    invoke-static {p1, p2}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->a(LX/K0E;F)V

    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 2758140
    const/4 v0, 0x1

    return v0
.end method

.method public final v()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2758139
    const-string v0, "openDrawer"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "closeDrawer"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2758138
    const-string v0, "topDrawerSlide"

    const-string v1, "registrationName"

    const-string v2, "onDrawerSlide"

    invoke-static {v1, v2}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "topDrawerOpened"

    const-string v3, "registrationName"

    const-string v4, "onDrawerOpen"

    invoke-static {v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    const-string v4, "topDrawerClosed"

    const-string v5, "registrationName"

    const-string v6, "onDrawerClose"

    invoke-static {v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    const-string v6, "topDrawerStateChanged"

    const-string v7, "registrationName"

    const-string v8, "onDrawerStateChanged"

    invoke-static {v7, v8}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/util/Map;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2758137
    const-string v0, "DrawerPosition"

    const-string v1, "Left"

    const v2, 0x800003

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "Right"

    const v4, 0x800005

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
