.class public final Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager$$PropsSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ViewManagerSetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ViewManagerSetter",
        "<",
        "Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;",
        "LX/K0E;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2758060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;LX/K0E;Ljava/lang/String;LX/5rC;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 2758061
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2758062
    :goto_1
    return-void

    .line 2758063
    :sswitch_0
    const-string v2, "accessibilityComponentType"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v2, "accessibilityLabel"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "accessibilityLiveRegion"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "backgroundColor"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "drawerLockMode"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "drawerPosition"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "drawerWidth"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "elevation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "importantForAccessibility"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "opacity"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "renderToHardwareTextureAndroid"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v2, "rotation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v2, "scaleX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "scaleY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "testID"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "transform"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "translateX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "translateY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "zIndex"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    .line 2758064
    :pswitch_0
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2758065
    invoke-static {p1, v0}, LX/5qk;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 2758066
    goto/16 :goto_1

    .line 2758067
    :pswitch_1
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2758068
    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2758069
    goto/16 :goto_1

    .line 2758070
    :pswitch_2
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setAccessibilityLiveRegion(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2758071
    :pswitch_3
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setBackgroundColor(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 2758072
    :pswitch_4
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->setDrawerLockMode(LX/K0E;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2758073
    :pswitch_5
    const v0, 0x800003

    invoke-virtual {p3, p2, v0}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->setDrawerPosition(LX/K0E;I)V

    goto/16 :goto_1

    .line 2758074
    :pswitch_6
    const/high16 v0, 0x7fc00000    # NaNf

    invoke-virtual {p3, p2, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->getDrawerWidth(LX/K0E;F)V

    goto/16 :goto_1

    .line 2758075
    :pswitch_7
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-static {p1, v0}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;->a(LX/K0E;F)V

    goto/16 :goto_1

    .line 2758076
    :pswitch_8
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setImportantForAccessibility(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2758077
    :pswitch_9
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2758078
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2758079
    goto/16 :goto_1

    .line 2758080
    :pswitch_a
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setRenderToHardwareTexture(Landroid/view/View;Z)V

    goto/16 :goto_1

    .line 2758081
    :pswitch_b
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2758082
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 2758083
    goto/16 :goto_1

    .line 2758084
    :pswitch_c
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2758085
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2758086
    goto/16 :goto_1

    .line 2758087
    :pswitch_d
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2758088
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2758089
    goto/16 :goto_1

    .line 2758090
    :pswitch_e
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2758091
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2758092
    goto/16 :goto_1

    .line 2758093
    :pswitch_f
    invoke-virtual {p3, p2}, LX/5rC;->d(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTransform(Landroid/view/View;LX/5pC;)V

    goto/16 :goto_1

    .line 2758094
    :pswitch_10
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTranslateX(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2758095
    :pswitch_11
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTranslateY(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2758096
    :pswitch_12
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setZIndex(Landroid/view/View;F)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x66a2c736 -> :sswitch_10
        -0x66a2c735 -> :sswitch_11
        -0x658128d7 -> :sswitch_0
        -0x4b8807f5 -> :sswitch_9
        -0x3621dfb2 -> :sswitch_c
        -0x3621dfb1 -> :sswitch_d
        -0x34488ed3 -> :sswitch_e
        -0x2b988b88 -> :sswitch_12
        -0x4d24f13 -> :sswitch_a
        -0x266f082 -> :sswitch_b
        -0x42d1a3 -> :sswitch_7
        0x22936ee -> :sswitch_2
        0xffd3355 -> :sswitch_6
        0x297a753a -> :sswitch_5
        0x2c861b47 -> :sswitch_8
        0x3ebe6b6c -> :sswitch_f
        0x445b6e46 -> :sswitch_1
        0x4cb7f6d5 -> :sswitch_3
        0x6eb2c17f -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/react/uimanager/ViewManager;Landroid/view/View;Ljava/lang/String;LX/5rC;)V
    .locals 0

    .prologue
    .line 2758097
    check-cast p1, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;

    check-cast p2, LX/K0E;

    invoke-static {p1, p2, p3, p4}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager$$PropsSetter;->a(Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;LX/K0E;Ljava/lang/String;LX/5rC;)V

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2758098
    const-string v0, "accessibilityComponentType"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758099
    const-string v0, "accessibilityLabel"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758100
    const-string v0, "accessibilityLiveRegion"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758101
    const-string v0, "backgroundColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758102
    const-string v0, "drawerLockMode"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758103
    const-string v0, "drawerPosition"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758104
    const-string v0, "drawerWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758105
    const-string v0, "elevation"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758106
    const-string v0, "importantForAccessibility"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758107
    const-string v0, "opacity"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758108
    const-string v0, "renderToHardwareTextureAndroid"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758109
    const-string v0, "rotation"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758110
    const-string v0, "scaleX"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758111
    const-string v0, "scaleY"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758112
    const-string v0, "testID"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758113
    const-string v0, "transform"

    const-string v1, "Array"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758114
    const-string v0, "translateX"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758115
    const-string v0, "translateY"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758116
    const-string v0, "zIndex"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758117
    return-void
.end method
