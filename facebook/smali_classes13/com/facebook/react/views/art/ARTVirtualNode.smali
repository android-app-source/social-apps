.class public abstract Lcom/facebook/react/views/art/ARTVirtualNode;
.super Lcom/facebook/react/uimanager/ReactShadowNode;
.source ""


# static fields
.field private static final a:[F

.field private static final d:[F


# instance fields
.field public b:F

.field public final c:F

.field private e:Landroid/graphics/Matrix;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 2757563
    new-array v0, v1, [F

    sput-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    .line 2757564
    new-array v0, v1, [F

    sput-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2757557
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;-><init>()V

    .line 2757558
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->b:F

    .line 2757559
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    .line 2757560
    sget-object v0, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v0, v0

    .line 2757561
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    .line 2757562
    return-void
.end method

.method private H()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 2757544
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    sget-object v1, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    aget v1, v1, v2

    aput v1, v0, v2

    .line 2757545
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    sget-object v1, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    aget v1, v1, v5

    aput v1, v0, v3

    .line 2757546
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    sget-object v1, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    iget v2, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v1, v2

    aput v1, v0, v5

    .line 2757547
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    sget-object v1, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    aget v1, v1, v3

    aput v1, v0, v6

    .line 2757548
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    const/4 v1, 0x4

    sget-object v2, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    aget v2, v2, v6

    aput v2, v0, v1

    .line 2757549
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v2, v3

    aput v2, v0, v1

    .line 2757550
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    const/4 v1, 0x6

    aput v4, v0, v1

    .line 2757551
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    const/4 v1, 0x7

    aput v4, v0, v1

    .line 2757552
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    const/16 v1, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 2757553
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    if-nez v0, :cond_0

    .line 2757554
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    .line 2757555
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    sget-object v1, Lcom/facebook/react/views/art/ARTVirtualNode;->d:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 2757556
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2757540
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2757541
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 2757542
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 2757543
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2757527
    const/4 v0, 0x1

    return v0
.end method

.method public setOpacity(F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 1.0f
        name = "opacity"
    .end annotation

    .prologue
    .line 2757537
    iput p1, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->b:F

    .line 2757538
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757539
    return-void
.end method

.method public setTransform(LX/5pC;)V
    .locals 2
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "transform"
    .end annotation

    .prologue
    .line 2757528
    if-eqz p1, :cond_2

    .line 2757529
    sget-object v0, Lcom/facebook/react/views/art/ARTVirtualNode;->a:[F

    invoke-static {p1, v0}, LX/K0D;->a(LX/5pC;[F)I

    move-result v0

    .line 2757530
    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 2757531
    invoke-direct {p0}, Lcom/facebook/react/views/art/ARTVirtualNode;->H()V

    .line 2757532
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757533
    return-void

    .line 2757534
    :cond_1
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2757535
    new-instance v0, LX/5pA;

    const-string v1, "Transform matrices must be of size 6"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757536
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->e:Landroid/graphics/Matrix;

    goto :goto_0
.end method
