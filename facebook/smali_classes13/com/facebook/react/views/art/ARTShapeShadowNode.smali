.class public Lcom/facebook/react/views/art/ARTShapeShadowNode;
.super Lcom/facebook/react/views/art/ARTVirtualNode;
.source ""


# instance fields
.field public a:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:F

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2757729
    invoke-direct {p0}, Lcom/facebook/react/views/art/ARTVirtualNode;-><init>()V

    .line 2757730
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->g:F

    .line 2757731
    iput v1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->h:I

    .line 2757732
    iput v1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->i:I

    return-void
.end method

.method private a([F)Landroid/graphics/Path;
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x0

    .line 2757704
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 2757705
    invoke-virtual {v0, v12, v12}, Landroid/graphics/Path;->moveTo(FF)V

    move v1, v7

    .line 2757706
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    .line 2757707
    add-int/lit8 v2, v1, 0x1

    aget v1, p1, v1

    float-to-int v1, v1

    .line 2757708
    packed-switch v1, :pswitch_data_0

    .line 2757709
    new-instance v0, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized drawing instruction "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757710
    :pswitch_0
    add-int/lit8 v3, v2, 0x1

    aget v1, p1, v2

    iget v2, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v2, v1

    add-int/lit8 v1, v3, 0x1

    aget v3, p1, v3

    iget v4, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_0

    .line 2757711
    :pswitch_1
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    move v1, v2

    .line 2757712
    goto :goto_0

    .line 2757713
    :pswitch_2
    add-int/lit8 v3, v2, 0x1

    aget v1, p1, v2

    iget v2, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v2, v1

    add-int/lit8 v1, v3, 0x1

    aget v3, p1, v3

    iget v4, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 2757714
    :pswitch_3
    add-int/lit8 v3, v2, 0x1

    aget v1, p1, v2

    iget v2, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v1, v2

    add-int/lit8 v4, v3, 0x1

    aget v2, p1, v3

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v2, v3

    add-int/lit8 v5, v4, 0x1

    aget v3, p1, v4

    iget v4, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v3, v4

    add-int/lit8 v6, v5, 0x1

    aget v4, p1, v5

    iget v5, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v4, v5

    add-int/lit8 v9, v6, 0x1

    aget v5, p1, v6

    iget v6, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v5, v6

    add-int/lit8 v8, v9, 0x1

    aget v6, p1, v9

    iget v9, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v1, v8

    .line 2757715
    goto :goto_0

    .line 2757716
    :pswitch_4
    add-int/lit8 v1, v2, 0x1

    aget v2, p1, v2

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float v4, v2, v3

    .line 2757717
    add-int/lit8 v2, v1, 0x1

    aget v1, p1, v1

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float v5, v1, v3

    .line 2757718
    add-int/lit8 v1, v2, 0x1

    aget v2, p1, v2

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float v6, v2, v3

    .line 2757719
    add-int/lit8 v2, v1, 0x1

    aget v1, p1, v1

    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    double-to-float v8, v8

    .line 2757720
    add-int/lit8 v3, v2, 0x1

    aget v1, p1, v2

    float-to-double v10, v1

    invoke-static {v10, v11}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v10

    double-to-float v2, v10

    .line 2757721
    add-int/lit8 v1, v3, 0x1

    aget v3, p1, v3

    cmpl-float v3, v3, v12

    if-nez v3, :cond_1

    const/4 v3, 0x1

    .line 2757722
    :goto_1
    if-nez v3, :cond_0

    .line 2757723
    const/high16 v3, 0x43b40000    # 360.0f

    sub-float v2, v3, v2

    .line 2757724
    :cond_0
    sub-float v2, v8, v2

    .line 2757725
    new-instance v3, Landroid/graphics/RectF;

    sub-float v9, v4, v6

    sub-float v10, v5, v6

    add-float/2addr v4, v6

    add-float/2addr v5, v6

    invoke-direct {v3, v9, v10, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2757726
    invoke-virtual {v0, v3, v8, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    goto/16 :goto_0

    :cond_1
    move v3, v7

    .line 2757727
    goto :goto_1

    .line 2757728
    :cond_2
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V
    .locals 2

    .prologue
    .line 2757692
    iget v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->b:F

    mul-float/2addr v0, p3

    .line 2757693
    const v1, 0x3c23d70a    # 0.01f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    .line 2757694
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/art/ARTVirtualNode;->a(Landroid/graphics/Canvas;)V

    .line 2757695
    iget-object v1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    if-nez v1, :cond_0

    .line 2757696
    new-instance v0, LX/5pA;

    const-string v1, "Shapes should have a valid path (d) prop"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757697
    :cond_0
    invoke-virtual {p0, p2, v0}, Lcom/facebook/react/views/art/ARTShapeShadowNode;->b(Landroid/graphics/Paint;F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2757698
    iget-object v1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2757699
    :cond_1
    invoke-virtual {p0, p2, v0}, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a(Landroid/graphics/Paint;F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2757700
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2757701
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2757702
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2757703
    return-void
.end method

.method public final a(Landroid/graphics/Paint;F)Z
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    const/high16 v6, 0x437f0000    # 255.0f

    .line 2757671
    iget v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->g:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 2757672
    :goto_0
    return v0

    .line 2757673
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Paint;->reset()V

    .line 2757674
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 2757675
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2757676
    iget v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->h:I

    packed-switch v0, :pswitch_data_0

    .line 2757677
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "strokeCap "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unrecognized"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757678
    :pswitch_0
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 2757679
    :goto_1
    iget v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->i:I

    packed-switch v0, :pswitch_data_1

    .line 2757680
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "strokeJoin "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unrecognized"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757681
    :pswitch_1
    sget-object v0, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_1

    .line 2757682
    :pswitch_2
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_1

    .line 2757683
    :pswitch_3
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 2757684
    :goto_2
    iget v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->g:F

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v0, v3

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2757685
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    array-length v0, v0

    if-le v0, v4, :cond_3

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    aget v0, v0, v4

    mul-float/2addr v0, p2

    mul-float/2addr v0, v6

    :goto_3
    float-to-int v0, v0

    iget-object v3, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    aget v2, v3, v2

    mul-float/2addr v2, v6

    float-to-int v2, v2

    iget-object v3, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    aget v3, v3, v1

    mul-float/2addr v3, v6

    float-to-int v3, v3

    iget-object v4, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-float/2addr v4, v6

    float-to-int v4, v4

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2757686
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->f:[F

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->f:[F

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2757687
    new-instance v0, Landroid/graphics/DashPathEffect;

    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->f:[F

    invoke-direct {v0, v2, v7}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    :cond_2
    move v0, v1

    .line 2757688
    goto/16 :goto_0

    .line 2757689
    :pswitch_4
    sget-object v0, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_2

    .line 2757690
    :pswitch_5
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_2

    .line 2757691
    :cond_3
    mul-float v0, p2, v6

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Landroid/graphics/Paint;F)Z
    .locals 7

    .prologue
    const/4 v3, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/high16 v6, 0x437f0000    # 255.0f

    .line 2757662
    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    array-length v2, v2

    if-lez v2, :cond_0

    .line 2757663
    invoke-virtual {p1}, Landroid/graphics/Paint;->reset()V

    .line 2757664
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 2757665
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2757666
    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    aget v0, v2, v0

    float-to-int v0, v0

    .line 2757667
    packed-switch v0, :pswitch_data_0

    .line 2757668
    const-string v2, "React"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ART: Color type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " not supported!"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move v0, v1

    .line 2757669
    :cond_0
    return v0

    .line 2757670
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    array-length v0, v0

    if-le v0, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    aget v0, v0, v3

    mul-float/2addr v0, p2

    mul-float/2addr v0, v6

    :goto_1
    float-to-int v0, v0

    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    aget v2, v2, v1

    mul-float/2addr v2, v6

    float-to-int v2, v2

    iget-object v3, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v3, v6

    float-to-int v3, v3

    iget-object v4, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    const/4 v5, 0x3

    aget v4, v4, v5

    mul-float/2addr v4, v6

    float-to-int v4, v4

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0

    :cond_1
    mul-float v0, p2, v6

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setFill(LX/5pC;)V
    .locals 1
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fill"
    .end annotation

    .prologue
    .line 2757659
    invoke-static {p1}, LX/K0D;->a(LX/5pC;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->e:[F

    .line 2757660
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757661
    return-void
.end method

.method public setShapePath(LX/5pC;)V
    .locals 1
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "d"
    .end annotation

    .prologue
    .line 2757655
    invoke-static {p1}, LX/K0D;->a(LX/5pC;)[F

    move-result-object v0

    .line 2757656
    invoke-direct {p0, v0}, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a([F)Landroid/graphics/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    .line 2757657
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757658
    return-void
.end method

.method public setStroke(LX/5pC;)V
    .locals 1
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "stroke"
    .end annotation

    .prologue
    .line 2757640
    invoke-static {p1}, LX/K0D;->a(LX/5pC;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->d:[F

    .line 2757641
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757642
    return-void
.end method

.method public setStrokeCap(I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x1
        name = "strokeCap"
    .end annotation

    .prologue
    .line 2757652
    iput p1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->h:I

    .line 2757653
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757654
    return-void
.end method

.method public setStrokeDash(LX/5pC;)V
    .locals 1
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "strokeDash"
    .end annotation

    .prologue
    .line 2757649
    invoke-static {p1}, LX/K0D;->a(LX/5pC;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->f:[F

    .line 2757650
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757651
    return-void
.end method

.method public setStrokeJoin(I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x1
        name = "strokeJoin"
    .end annotation

    .prologue
    .line 2757646
    iput p1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->i:I

    .line 2757647
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757648
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 1.0f
        name = "strokeWidth"
    .end annotation

    .prologue
    .line 2757643
    iput p1, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->g:F

    .line 2757644
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757645
    return-void
.end method
