.class public Lcom/facebook/react/views/art/ARTTextShadowNode;
.super Lcom/facebook/react/views/art/ARTShapeShadowNode;
.source ""


# instance fields
.field public d:LX/5pG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2757967
    invoke-direct {p0}, Lcom/facebook/react/views/art/ARTShapeShadowNode;-><init>()V

    .line 2757968
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->e:I

    return-void
.end method

.method private a(Landroid/graphics/Paint;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2757969
    iget v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->e:I

    .line 2757970
    packed-switch v0, :pswitch_data_0

    .line 2757971
    :goto_0
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    if-eqz v0, :cond_2

    .line 2757972
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    const-string v3, "font"

    invoke-interface {v0, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2757973
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    const-string v3, "font"

    invoke-interface {v0, v3}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v4

    .line 2757974
    if-eqz v4, :cond_2

    .line 2757975
    const/high16 v0, 0x41400000    # 12.0f

    .line 2757976
    const-string v3, "fontSize"

    invoke-interface {v4, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2757977
    const-string v0, "fontSize"

    invoke-interface {v4, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v0, v6

    .line 2757978
    :cond_0
    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v0, v3

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2757979
    const-string v0, "fontWeight"

    invoke-interface {v4, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "bold"

    const-string v3, "fontWeight"

    invoke-interface {v4, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2757980
    :goto_1
    const-string v3, "fontStyle"

    invoke-interface {v4, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "italic"

    const-string v5, "fontStyle"

    invoke-interface {v4, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    .line 2757981
    :goto_2
    if-eqz v0, :cond_5

    if-eqz v3, :cond_5

    .line 2757982
    const/4 v1, 0x3

    .line 2757983
    :cond_1
    :goto_3
    const-string v0, "fontFamily"

    invoke-interface {v4, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2757984
    :cond_2
    return-void

    .line 2757985
    :pswitch_0
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0

    .line 2757986
    :pswitch_1
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0

    .line 2757987
    :pswitch_2
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 2757988
    goto :goto_1

    :cond_4
    move v3, v2

    .line 2757989
    goto :goto_2

    .line 2757990
    :cond_5
    if-nez v0, :cond_1

    .line 2757991
    if-eqz v3, :cond_6

    .line 2757992
    const/4 v1, 0x2

    goto :goto_3

    :cond_6
    move v1, v2

    .line 2757993
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2757994
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    if-nez v0, :cond_1

    .line 2757995
    :cond_0
    :goto_0
    return-void

    .line 2757996
    :cond_1
    iget v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->b:F

    mul-float v6, p3, v0

    .line 2757997
    const v0, 0x3c23d70a    # 0.01f

    cmpg-float v0, v6, v0

    if-lez v0, :cond_0

    .line 2757998
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    const-string v1, "lines"

    invoke-interface {v0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2757999
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    const-string v1, "lines"

    invoke-interface {v0, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    .line 2758000
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2758001
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/art/ARTVirtualNode;->a(Landroid/graphics/Canvas;)V

    .line 2758002
    invoke-interface {v1}, LX/5pC;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 2758003
    const/4 v0, 0x0

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 2758004
    invoke-interface {v1, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    .line 2758005
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2758006
    :cond_2
    const-string v0, "\n"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2758007
    invoke-virtual {p0, p2, v6}, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a(Landroid/graphics/Paint;F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2758008
    invoke-direct {p0, p2}, Lcom/facebook/react/views/art/ARTTextShadowNode;->a(Landroid/graphics/Paint;)V

    .line 2758009
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    if-nez v0, :cond_5

    .line 2758010
    invoke-virtual {p2}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p1, v1, v3, v0, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2758011
    :cond_3
    :goto_2
    invoke-virtual {p0, p2, v6}, Lcom/facebook/react/views/art/ARTShapeShadowNode;->b(Landroid/graphics/Paint;F)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2758012
    invoke-direct {p0, p2}, Lcom/facebook/react/views/art/ARTTextShadowNode;->a(Landroid/graphics/Paint;)V

    .line 2758013
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    if-nez v0, :cond_6

    .line 2758014
    invoke-virtual {p2}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p1, v1, v3, v0, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2758015
    :cond_4
    :goto_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2758016
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    goto :goto_0

    .line 2758017
    :cond_5
    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    move-object v0, p1

    move v4, v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 2758018
    :cond_6
    iget-object v2, p0, Lcom/facebook/react/views/art/ARTShapeShadowNode;->a:Landroid/graphics/Path;

    move-object v0, p1

    move v4, v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method public setAlignment(I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        name = "alignment"
    .end annotation

    .prologue
    .line 2758019
    iput p1, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->e:I

    .line 2758020
    return-void
.end method

.method public setFrame(LX/5pG;)V
    .locals 0
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "frame"
    .end annotation

    .prologue
    .line 2758021
    iput-object p1, p0, Lcom/facebook/react/views/art/ARTTextShadowNode;->d:LX/5pG;

    .line 2758022
    return-void
.end method
