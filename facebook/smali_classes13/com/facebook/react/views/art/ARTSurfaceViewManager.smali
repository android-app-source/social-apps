.class public Lcom/facebook/react/views/art/ARTSurfaceViewManager;
.super Lcom/facebook/react/uimanager/BaseViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "ARTSurfaceView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/BaseViewManager",
        "<",
        "LX/K0B;",
        "Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/csslayout/YogaMeasureFunction;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2757799
    new-instance v0, LX/K0C;

    invoke-direct {v0}, LX/K0C;-><init>()V

    sput-object v0, Lcom/facebook/react/views/art/ARTSurfaceViewManager;->a:Lcom/facebook/csslayout/YogaMeasureFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2757802
    invoke-direct {p0}, Lcom/facebook/react/uimanager/BaseViewManager;-><init>()V

    return-void
.end method

.method private static a(LX/K0B;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2757800
    check-cast p1, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;

    invoke-virtual {p0, p1}, LX/K0B;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2757801
    return-void
.end method

.method private static b(LX/5rJ;)LX/K0B;
    .locals 1

    .prologue
    .line 2757795
    new-instance v0, LX/K0B;

    invoke-direct {v0, p0}, LX/K0B;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static h()Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;
    .locals 2

    .prologue
    .line 2757796
    new-instance v0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;-><init>()V

    .line 2757797
    sget-object v1, Lcom/facebook/react/views/art/ARTSurfaceViewManager;->a:Lcom/facebook/csslayout/YogaMeasureFunction;

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2757798
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2757791
    invoke-static {p1}, Lcom/facebook/react/views/art/ARTSurfaceViewManager;->b(LX/5rJ;)LX/K0B;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2757790
    check-cast p1, LX/K0B;

    invoke-static {p1, p2}, Lcom/facebook/react/views/art/ARTSurfaceViewManager;->a(LX/K0B;Ljava/lang/Object;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757794
    const-string v0, "ARTSurfaceView"

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2757792
    const-class v0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2757793
    invoke-static {}, Lcom/facebook/react/views/art/ARTSurfaceViewManager;->h()Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;

    move-result-object v0

    return-object v0
.end method
