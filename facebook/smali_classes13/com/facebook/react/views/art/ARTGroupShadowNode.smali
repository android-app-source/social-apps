.class public Lcom/facebook/react/views/art/ARTGroupShadowNode;
.super Lcom/facebook/react/views/art/ARTVirtualNode;
.source ""


# instance fields
.field public a:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2757565
    invoke-direct {p0}, Lcom/facebook/react/views/art/ARTVirtualNode;-><init>()V

    return-void
.end method

.method private static a([F)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2757566
    array-length v0, p0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2757567
    new-instance v0, LX/5pA;

    const-string v1, "Clipping should be array of length 4 (e.g. [x, y, width, height])"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2757568
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    aget v1, p0, v3

    aget v2, p0, v5

    aget v3, p0, v3

    const/4 v4, 0x2

    aget v4, p0, v4

    add-float/2addr v3, v4

    aget v4, p0, v5

    const/4 v5, 0x3

    aget v5, p0, v5

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2757569
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V
    .locals 7

    .prologue
    .line 2757570
    iget v0, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->b:F

    mul-float v6, p3, v0

    .line 2757571
    const v0, 0x3c23d70a    # 0.01f

    cmpl-float v0, v6, v0

    if-lez v0, :cond_2

    .line 2757572
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/art/ARTVirtualNode;->a(Landroid/graphics/Canvas;)V

    .line 2757573
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 2757574
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v1, v0

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v2, v0

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v3, v0

    iget-object v0, p0, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/facebook/react/views/art/ARTVirtualNode;->c:F

    mul-float/2addr v4, v0

    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 2757575
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2757576
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/views/art/ARTVirtualNode;

    .line 2757577
    invoke-virtual {v0, p1, p2, v6}, Lcom/facebook/react/views/art/ARTVirtualNode;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V

    .line 2757578
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2757579
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2757580
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2757581
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2757582
    const/4 v0, 0x1

    return v0
.end method

.method public setClipping(LX/5pC;)V
    .locals 1
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "clipping"
    .end annotation

    .prologue
    .line 2757583
    invoke-static {p1}, LX/K0D;->a(LX/5pC;)[F

    move-result-object v0

    .line 2757584
    if-eqz v0, :cond_0

    .line 2757585
    invoke-static {v0}, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a([F)Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTGroupShadowNode;->a:Landroid/graphics/RectF;

    .line 2757586
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2757587
    :cond_0
    return-void
.end method
