.class public Lcom/facebook/react/views/art/ARTRenderableViewManager;
.super Lcom/facebook/react/uimanager/ViewManager;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewManager",
        "<",
        "Landroid/view/View;",
        "Lcom/facebook/react/uimanager/ReactShadowNode;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757610
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewManager;-><init>()V

    .line 2757611
    iput-object p1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    .line 2757612
    return-void
.end method

.method public static h()Lcom/facebook/react/views/art/ARTRenderableViewManager;
    .locals 2

    .prologue
    .line 2757613
    new-instance v0, Lcom/facebook/react/views/art/ARTRenderableViewManager;

    const-string v1, "ARTGroup"

    invoke-direct {v0, v1}, Lcom/facebook/react/views/art/ARTRenderableViewManager;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static t()Lcom/facebook/react/views/art/ARTRenderableViewManager;
    .locals 2

    .prologue
    .line 2757615
    new-instance v0, Lcom/facebook/react/views/art/ARTRenderableViewManager;

    const-string v1, "ARTShape"

    invoke-direct {v0, v1}, Lcom/facebook/react/views/art/ARTRenderableViewManager;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static u()Lcom/facebook/react/views/art/ARTRenderableViewManager;
    .locals 2

    .prologue
    .line 2757614
    new-instance v0, Lcom/facebook/react/views/art/ARTRenderableViewManager;

    const-string v1, "ARTText"

    invoke-direct {v0, v1}, Lcom/facebook/react/views/art/ARTRenderableViewManager;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/5rJ;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2757609
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ARTShape does not map into a native view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2757591
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ARTShape does not map into a native view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2757608
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/react/uimanager/ReactShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2757600
    const-string v0, "ARTGroup"

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2757601
    const-class v0, Lcom/facebook/react/views/art/ARTGroupShadowNode;

    .line 2757602
    :goto_0
    return-object v0

    .line 2757603
    :cond_0
    const-string v0, "ARTShape"

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2757604
    const-class v0, Lcom/facebook/react/views/art/ARTShapeShadowNode;

    goto :goto_0

    .line 2757605
    :cond_1
    const-string v0, "ARTText"

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2757606
    const-class v0, Lcom/facebook/react/views/art/ARTTextShadowNode;

    goto :goto_0

    .line 2757607
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 3

    .prologue
    .line 2757592
    const-string v0, "ARTGroup"

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2757593
    new-instance v0, Lcom/facebook/react/views/art/ARTGroupShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/art/ARTGroupShadowNode;-><init>()V

    .line 2757594
    :goto_0
    return-object v0

    .line 2757595
    :cond_0
    const-string v0, "ARTShape"

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2757596
    new-instance v0, Lcom/facebook/react/views/art/ARTShapeShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/art/ARTShapeShadowNode;-><init>()V

    goto :goto_0

    .line 2757597
    :cond_1
    const-string v0, "ARTText"

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2757598
    new-instance v0, Lcom/facebook/react/views/art/ARTTextShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/art/ARTTextShadowNode;-><init>()V

    goto :goto_0

    .line 2757599
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/react/views/art/ARTRenderableViewManager;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
