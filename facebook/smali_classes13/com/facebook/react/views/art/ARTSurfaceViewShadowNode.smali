.class public Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;
.super Lcom/facebook/react/uimanager/LayoutShadowNode;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private a:Landroid/view/Surface;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2757934
    invoke-direct {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;-><init>()V

    return-void
.end method

.method private H()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2757918
    iget-object v1, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2757919
    :cond_0
    invoke-direct {p0, p0}, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->e(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2757920
    :cond_1
    :goto_0
    return-void

    .line 2757921
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v2

    .line 2757922
    const/4 v1, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2757923
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move v1, v0

    .line 2757924
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2757925
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/views/art/ARTVirtualNode;

    .line 2757926
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3, v4}, Lcom/facebook/react/views/art/ARTVirtualNode;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V

    .line 2757927
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2757928
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2757929
    :cond_3
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 2757930
    iget-object v0, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    invoke-virtual {v0, v2}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2757931
    :catch_0
    move-exception v0

    .line 2757932
    :goto_2
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in Surface.unlockCanvasAndPost"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2757933
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private e(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 2

    .prologue
    .line 2757912
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2757913
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 2757914
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2757915
    invoke-direct {p0, v1}, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->e(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2757916
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2757917
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/5rl;)V
    .locals 1

    .prologue
    .line 2757897
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->a(LX/5rl;)V

    .line 2757898
    invoke-direct {p0}, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->H()V

    .line 2757899
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 2757900
    invoke-virtual {p1, v0, p0}, LX/5rl;->a(ILjava/lang/Object;)V

    .line 2757901
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2757911
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2757910
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 2757907
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    .line 2757908
    invoke-direct {p0}, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->H()V

    .line 2757909
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 2757904
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 2757905
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/views/art/ARTSurfaceViewShadowNode;->a:Landroid/view/Surface;

    .line 2757906
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 2757903
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 2757902
    return-void
.end method
