.class public final Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager$$PropsSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ViewManagerSetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ViewManagerSetter",
        "<",
        "Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;",
        "LX/K0h;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2759364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;LX/K0h;Ljava/lang/String;LX/5rC;)V
    .locals 6

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2759365
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2759366
    :goto_1
    return-void

    .line 2759367
    :sswitch_0
    const-string v2, "accessibilityComponentType"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v2, "accessibilityLabel"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "accessibilityLiveRegion"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "animating"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "backgroundColor"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "color"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "elevation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "importantForAccessibility"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "indeterminate"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "opacity"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "progress"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v2, "renderToHardwareTextureAndroid"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v2, "rotation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "scaleX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "scaleY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "styleAttr"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "testID"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "transform"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "translateX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "translateY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "zIndex"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_0

    .line 2759368
    :pswitch_0
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2759369
    invoke-static {p1, v0}, LX/5qk;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 2759370
    goto/16 :goto_1

    .line 2759371
    :pswitch_1
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2759372
    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2759373
    goto/16 :goto_1

    .line 2759374
    :pswitch_2
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setAccessibilityLiveRegion(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2759375
    :pswitch_3
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2759376
    iput-boolean v0, p1, LX/K0h;->c:Z

    .line 2759377
    goto/16 :goto_1

    .line 2759378
    :pswitch_4
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setBackgroundColor(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 2759379
    :pswitch_5
    invoke-virtual {p3, p2}, LX/5rC;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 2759380
    :goto_2
    iput-object v0, p1, LX/K0h;->a:Ljava/lang/Integer;

    .line 2759381
    goto/16 :goto_1

    :cond_1
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 2759382
    :pswitch_6
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setElevation(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2759383
    :pswitch_7
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setImportantForAccessibility(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2759384
    :pswitch_8
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2759385
    iput-boolean v0, p1, LX/K0h;->b:Z

    .line 2759386
    goto/16 :goto_1

    .line 2759387
    :pswitch_9
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2759388
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2759389
    goto/16 :goto_1

    .line 2759390
    :pswitch_a
    const-wide/16 v0, 0x0

    invoke-virtual {p3, p2, v0, v1}, LX/5rC;->a(Ljava/lang/String;D)D

    move-result-wide v0

    .line 2759391
    iput-wide v0, p1, LX/K0h;->d:D

    .line 2759392
    goto/16 :goto_1

    .line 2759393
    :pswitch_b
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setRenderToHardwareTexture(Landroid/view/View;Z)V

    goto/16 :goto_1

    .line 2759394
    :pswitch_c
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2759395
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 2759396
    goto/16 :goto_1

    .line 2759397
    :pswitch_d
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2759398
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2759399
    goto/16 :goto_1

    .line 2759400
    :pswitch_e
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2759401
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2759402
    goto/16 :goto_1

    .line 2759403
    :pswitch_f
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2759404
    invoke-virtual {p1, v0}, LX/K0h;->a(Ljava/lang/String;)V

    .line 2759405
    goto/16 :goto_1

    .line 2759406
    :pswitch_10
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2759407
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2759408
    goto/16 :goto_1

    .line 2759409
    :pswitch_11
    invoke-virtual {p3, p2}, LX/5rC;->d(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTransform(Landroid/view/View;LX/5pC;)V

    goto/16 :goto_1

    .line 2759410
    :pswitch_12
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTranslateX(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2759411
    :pswitch_13
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTranslateY(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2759412
    :pswitch_14
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setZIndex(Landroid/view/View;F)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x66a2c736 -> :sswitch_12
        -0x66a2c735 -> :sswitch_13
        -0x658128d7 -> :sswitch_0
        -0x4b8807f5 -> :sswitch_9
        -0x3bab3dd3 -> :sswitch_a
        -0x3621dfb2 -> :sswitch_d
        -0x3621dfb1 -> :sswitch_e
        -0x34488ed3 -> :sswitch_10
        -0x2b988b88 -> :sswitch_14
        -0x4d24f13 -> :sswitch_b
        -0x266f082 -> :sswitch_c
        -0x42d1a3 -> :sswitch_6
        0x22936ee -> :sswitch_2
        0x5a72f63 -> :sswitch_5
        0x25bcecbb -> :sswitch_8
        0x2c861b47 -> :sswitch_7
        0x3ebe6b6c -> :sswitch_11
        0x42ab1b5e -> :sswitch_3
        0x445b6e46 -> :sswitch_1
        0x4cb7f6d5 -> :sswitch_4
        0x6b922b42 -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/react/uimanager/ViewManager;Landroid/view/View;Ljava/lang/String;LX/5rC;)V
    .locals 0

    .prologue
    .line 2759413
    check-cast p1, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;

    check-cast p2, LX/K0h;

    invoke-static {p1, p2, p3, p4}, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager$$PropsSetter;->a(Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;LX/K0h;Ljava/lang/String;LX/5rC;)V

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2759414
    const-string v0, "accessibilityComponentType"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759415
    const-string v0, "accessibilityLabel"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759416
    const-string v0, "accessibilityLiveRegion"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759417
    const-string v0, "animating"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759418
    const-string v0, "backgroundColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759419
    const-string v0, "color"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759420
    const-string v0, "elevation"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759421
    const-string v0, "importantForAccessibility"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759422
    const-string v0, "indeterminate"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759423
    const-string v0, "opacity"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759424
    const-string v0, "progress"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759425
    const-string v0, "renderToHardwareTextureAndroid"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759426
    const-string v0, "rotation"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759427
    const-string v0, "scaleX"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759428
    const-string v0, "scaleY"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759429
    const-string v0, "styleAttr"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759430
    const-string v0, "testID"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759431
    const-string v0, "transform"

    const-string v1, "Array"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759432
    const-string v0, "translateX"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759433
    const-string v0, "translateY"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759434
    const-string v0, "zIndex"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759435
    return-void
.end method
