.class public Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;
.super Lcom/facebook/react/uimanager/BaseViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidProgressBar"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/BaseViewManager",
        "<",
        "LX/K0h;",
        "Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2759453
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2759454
    invoke-direct {p0}, Lcom/facebook/react/uimanager/BaseViewManager;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2759455
    if-nez p0, :cond_0

    .line 2759456
    new-instance v0, LX/5pA;

    const-string v1, "ProgressBar needs to have a style, null received"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2759457
    :cond_0
    const-string v0, "Horizontal"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2759458
    const v0, 0x1010078

    .line 2759459
    :goto_0
    return v0

    .line 2759460
    :cond_1
    const-string v0, "Small"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2759461
    const v0, 0x1010079

    goto :goto_0

    .line 2759462
    :cond_2
    const-string v0, "Large"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2759463
    const v0, 0x101007a

    goto :goto_0

    .line 2759464
    :cond_3
    const-string v0, "Inverse"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2759465
    const v0, 0x1010287

    goto :goto_0

    .line 2759466
    :cond_4
    const-string v0, "SmallInverse"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2759467
    const v0, 0x1010288

    goto :goto_0

    .line 2759468
    :cond_5
    const-string v0, "LargeInverse"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2759469
    const v0, 0x1010289

    goto :goto_0

    .line 2759470
    :cond_6
    const-string v0, "Normal"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2759471
    const v0, 0x1010077

    goto :goto_0

    .line 2759472
    :cond_7
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown ProgressBar style: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Landroid/widget/ProgressBar;
    .locals 3

    .prologue
    .line 2759473
    sget-object v1, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2759474
    :try_start_0
    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    monitor-exit v1

    return-object v0

    .line 2759475
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(LX/5rJ;)LX/K0h;
    .locals 1

    .prologue
    .line 2759476
    new-instance v0, LX/K0h;

    invoke-direct {v0, p0}, LX/K0h;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static h()Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;
    .locals 1

    .prologue
    .line 2759477
    new-instance v0, Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2759478
    invoke-static {p1}, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;->b(LX/5rJ;)LX/K0h;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2759449
    return-void
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2759450
    check-cast p1, LX/K0h;

    .line 2759451
    invoke-virtual {p1}, LX/K0h;->a()V

    .line 2759452
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2759448
    const-string v0, "AndroidProgressBar"

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2759447
    const-class v0, Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2759446
    invoke-static {}, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;->h()Lcom/facebook/react/views/progressbar/ProgressBarShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public setAnimating(LX/K0h;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "animating"
    .end annotation

    .prologue
    .line 2759444
    iput-boolean p2, p1, LX/K0h;->c:Z

    .line 2759445
    return-void
.end method

.method public setColor(LX/K0h;Ljava/lang/Integer;)V
    .locals 0
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "color"
    .end annotation

    .prologue
    .line 2759442
    iput-object p2, p1, LX/K0h;->a:Ljava/lang/Integer;

    .line 2759443
    return-void
.end method

.method public setIndeterminate(LX/K0h;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "indeterminate"
    .end annotation

    .prologue
    .line 2759440
    iput-boolean p2, p1, LX/K0h;->b:Z

    .line 2759441
    return-void
.end method

.method public setProgress(LX/K0h;D)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "progress"
    .end annotation

    .prologue
    .line 2759436
    iput-wide p2, p1, LX/K0h;->d:D

    .line 2759437
    return-void
.end method

.method public setStyle(LX/K0h;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "styleAttr"
    .end annotation

    .prologue
    .line 2759438
    invoke-virtual {p1, p2}, LX/K0h;->a(Ljava/lang/String;)V

    .line 2759439
    return-void
.end method
