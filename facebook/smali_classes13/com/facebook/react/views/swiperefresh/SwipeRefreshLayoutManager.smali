.class public Lcom/facebook/react/views/swiperefresh/SwipeRefreshLayoutManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidSwipeRefreshLayout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/K0p;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2759826
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private a(LX/5rJ;LX/K0p;)V
    .locals 1

    .prologue
    .line 2759824
    new-instance v0, LX/K0r;

    invoke-direct {v0, p0, p1, p2}, LX/K0r;-><init>(Lcom/facebook/react/views/swiperefresh/SwipeRefreshLayoutManager;LX/5rJ;LX/K0p;)V

    invoke-virtual {p2, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2759825
    return-void
.end method

.method private static b(LX/5rJ;)LX/K0p;
    .locals 1

    .prologue
    .line 2759823
    new-instance v0, LX/K0p;

    invoke-direct {v0, p0}, LX/K0p;-><init>(LX/5pX;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2759822
    invoke-static {p1}, Lcom/facebook/react/views/swiperefresh/SwipeRefreshLayoutManager;->b(LX/5rJ;)LX/K0p;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2759821
    check-cast p2, LX/K0p;

    invoke-direct {p0, p1, p2}, Lcom/facebook/react/views/swiperefresh/SwipeRefreshLayoutManager;->a(LX/5rJ;LX/K0p;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2759820
    const-string v0, "AndroidSwipeRefreshLayout"

    return-object v0
.end method

.method public setColors(LX/K0p;LX/5pC;)V
    .locals 3
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "ColorArray"
        name = "colors"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2759827
    if-eqz p2, :cond_1

    .line 2759828
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v1

    new-array v1, v1, [I

    .line 2759829
    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2759830
    invoke-interface {p2, v0}, LX/5pC;->getInt(I)I

    move-result v2

    aput v2, v1, v0

    .line 2759831
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2759832
    :cond_0
    invoke-virtual {p1, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    .line 2759833
    :goto_1
    return-void

    .line 2759834
    :cond_1
    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    goto :goto_1
.end method

.method public setEnabled(LX/K0p;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "enabled"
    .end annotation

    .prologue
    .line 2759818
    invoke-virtual {p1, p2}, LX/K0p;->setEnabled(Z)V

    .line 2759819
    return-void
.end method

.method public setProgressBackgroundColor(LX/K0p;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        customType = "Color"
        name = "progressBackgroundColor"
    .end annotation

    .prologue
    .line 2759816
    invoke-virtual {p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeColor(I)V

    .line 2759817
    return-void
.end method

.method public setProgressViewOffset(LX/K0p;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "progressViewOffset"
    .end annotation

    .prologue
    .line 2759814
    invoke-virtual {p1, p2}, LX/K0p;->setProgressViewOffset(F)V

    .line 2759815
    return-void
.end method

.method public setRefreshing(LX/K0p;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "refreshing"
    .end annotation

    .prologue
    .line 2759812
    invoke-virtual {p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2759813
    return-void
.end method

.method public setSize(LX/K0p;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x1
        name = "size"
    .end annotation

    .prologue
    .line 2759810
    invoke-virtual {p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setSize(I)V

    .line 2759811
    return-void
.end method

.method public final x()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2759808
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    const-string v1, "topRefresh"

    const-string v2, "registrationName"

    const-string v3, "onRefresh"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2759809
    const-string v0, "SIZE"

    const-string v1, "DEFAULT"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "LARGE"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
