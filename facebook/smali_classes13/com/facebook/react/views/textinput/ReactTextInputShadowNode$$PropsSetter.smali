.class public final Lcom/facebook/react/views/textinput/ReactTextInputShadowNode$$PropsSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ShadowNodeSetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ShadowNodeSetter",
        "<",
        "Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2761742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/high16 v5, 0x7fc00000    # NaNf

    .line 2761743
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2761744
    :goto_1
    return-void

    .line 2761745
    :sswitch_0
    const-string v4, "alignItems"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "alignSelf"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "aspectRatio"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_3
    const-string v4, "backgroundColor"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v4, "borderBottomWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v4, "borderLeftWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v4, "borderRightWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v4, "borderTopWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v4, "borderWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v4, "bottom"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v4, "color"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v4, "flex"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v4, "flexBasis"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v4, "flexDirection"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v4, "flexGrow"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v4, "flexShrink"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v4, "flexWrap"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v4, "fontFamily"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v4, "fontSize"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v4, "fontStyle"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v4, "fontWeight"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v4, "height"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v4, "justifyContent"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v4, "left"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v4, "lineHeight"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v4, "margin"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v4, "marginBottom"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v4, "marginHorizontal"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v4, "marginLeft"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v4, "marginRight"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v4, "marginTop"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v4, "marginVertical"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v4, "maxHeight"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v4, "maxWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v4, "minHeight"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v4, "minWidth"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v4, "mostRecentEventCount"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v4, "numberOfLines"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v4, "onLayout"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v4, "overflow"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v4, "padding"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v4, "paddingBottom"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v4, "paddingHorizontal"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string v4, "paddingLeft"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string v4, "paddingRight"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x2c

    goto/16 :goto_0

    :sswitch_2d
    const-string v4, "paddingTop"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x2d

    goto/16 :goto_0

    :sswitch_2e
    const-string v4, "paddingVertical"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const-string v4, "position"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const-string v4, "right"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x30

    goto/16 :goto_0

    :sswitch_31
    const-string v4, "text"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x31

    goto/16 :goto_0

    :sswitch_32
    const-string v4, "textAlign"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x32

    goto/16 :goto_0

    :sswitch_33
    const-string v4, "textDecorationLine"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x33

    goto/16 :goto_0

    :sswitch_34
    const-string v4, "textShadowColor"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x34

    goto/16 :goto_0

    :sswitch_35
    const-string v4, "textShadowOffset"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x35

    goto/16 :goto_0

    :sswitch_36
    const-string v4, "textShadowRadius"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x36

    goto/16 :goto_0

    :sswitch_37
    const-string v4, "top"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x37

    goto/16 :goto_0

    :sswitch_38
    const-string v4, "width"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x38

    goto/16 :goto_0

    .line 2761746
    :pswitch_0
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setAlignItems(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761747
    :pswitch_1
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setAlignSelf(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761748
    :pswitch_2
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2761749
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->g(F)V

    .line 2761750
    goto/16 :goto_1

    .line 2761751
    :pswitch_3
    invoke-virtual {p2, p1}, LX/5rC;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setBackgroundColor(Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_1
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 2761752
    :pswitch_4
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2761753
    :pswitch_5
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2761754
    :pswitch_6
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2761755
    :pswitch_7
    const/4 v0, 0x3

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2761756
    :pswitch_8
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2761757
    :pswitch_9
    const/4 v0, 0x3

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2761758
    :pswitch_a
    invoke-virtual {p2, p1}, LX/5rC;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setColor(Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_2
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3

    .line 2761759
    :pswitch_b
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlex(F)V

    goto/16 :goto_1

    .line 2761760
    :pswitch_c
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexBasis(F)V

    goto/16 :goto_1

    .line 2761761
    :pswitch_d
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setFlexDirection(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761762
    :pswitch_e
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexGrow(F)V

    goto/16 :goto_1

    .line 2761763
    :pswitch_f
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexShrink(F)V

    goto/16 :goto_1

    .line 2761764
    :pswitch_10
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setFlexWrap(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761765
    :pswitch_11
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setFontFamily(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761766
    :pswitch_12
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setFontSize(F)V

    goto/16 :goto_1

    .line 2761767
    :pswitch_13
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setFontStyle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761768
    :pswitch_14
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setFontWeight(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761769
    :pswitch_15
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setHeight(F)V

    goto/16 :goto_1

    .line 2761770
    :pswitch_16
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setJustifyContent(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761771
    :pswitch_17
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2761772
    :pswitch_18
    const/4 v0, -0x1

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setLineHeight(I)V

    goto/16 :goto_1

    .line 2761773
    :pswitch_19
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761774
    :pswitch_1a
    const/4 v0, 0x6

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761775
    :pswitch_1b
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761776
    :pswitch_1c
    const/4 v0, 0x3

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761777
    :pswitch_1d
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761778
    :pswitch_1e
    const/4 v0, 0x5

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761779
    :pswitch_1f
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2761780
    :pswitch_20
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMaxHeight(F)V

    goto/16 :goto_1

    .line 2761781
    :pswitch_21
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMaxWidth(F)V

    goto/16 :goto_1

    .line 2761782
    :pswitch_22
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMinHeight(F)V

    goto/16 :goto_1

    .line 2761783
    :pswitch_23
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMinWidth(F)V

    goto/16 :goto_1

    .line 2761784
    :pswitch_24
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    .line 2761785
    iput v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->g:I

    .line 2761786
    goto/16 :goto_1

    .line 2761787
    :pswitch_25
    const/4 v0, -0x1

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setNumberOfLines(I)V

    goto/16 :goto_1

    .line 2761788
    :pswitch_26
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2761789
    iput-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->e:Z

    .line 2761790
    goto/16 :goto_1

    .line 2761791
    :pswitch_27
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setOverflow(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761792
    :pswitch_28
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761793
    :pswitch_29
    const/4 v0, 0x6

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761794
    :pswitch_2a
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761795
    :pswitch_2b
    const/4 v0, 0x3

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761796
    :pswitch_2c
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761797
    :pswitch_2d
    const/4 v0, 0x5

    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761798
    :pswitch_2e
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2761799
    :pswitch_2f
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPosition(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761800
    :pswitch_30
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2761801
    :pswitch_31
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setText(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761802
    :pswitch_32
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setTextAlign(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761803
    :pswitch_33
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setTextDecorationLine(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2761804
    :pswitch_34
    const/high16 v0, 0x55000000

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setTextShadowColor(I)V

    goto/16 :goto_1

    .line 2761805
    :pswitch_35
    invoke-virtual {p2, p1}, LX/5rC;->e(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setTextShadowOffset(LX/5pG;)V

    goto/16 :goto_1

    .line 2761806
    :pswitch_36
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->setTextShadowRadius(F)V

    goto/16 :goto_1

    .line 2761807
    :pswitch_37
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2761808
    :pswitch_38
    invoke-virtual {p2, p1, v5}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setWidth(F)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x757f89aa -> :sswitch_6
        -0x719cd38e -> :sswitch_1b
        -0x6a52083b -> :sswitch_c
        -0x5c71855e -> :sswitch_13
        -0x597a2048 -> :sswitch_2b
        -0x56940a43 -> :sswitch_7
        -0x527265d5 -> :sswitch_9
        -0x5201456c -> :sswitch_23
        -0x4f447821 -> :sswitch_25
        -0x4cec9971 -> :sswitch_4
        -0x48ff636d -> :sswitch_11
        -0x48c76ed9 -> :sswitch_15
        -0x40737a52 -> :sswitch_19
        -0x3f826a28 -> :sswitch_32
        -0x3f600445 -> :sswitch_0
        -0x3e464339 -> :sswitch_1e
        -0x3a1ff07a -> :sswitch_d
        -0x36017855 -> :sswitch_20
        -0x300fc3ef -> :sswitch_28
        -0x2bc67c59 -> :sswitch_14
        -0x1ebe99c5 -> :sswitch_18
        -0x15737ceb -> :sswitch_2a
        -0x113c6e87 -> :sswitch_1a
        -0xd59d8cd -> :sswitch_5
        -0x7f661e7 -> :sswitch_22
        0x1c155 -> :sswitch_37
        0x2ffff9 -> :sswitch_b
        0x32a007 -> :sswitch_17
        0x36452d -> :sswitch_31
        0x55f4784 -> :sswitch_2d
        0x5a72f63 -> :sswitch_a
        0x677c21c -> :sswitch_30
        0x6be2dc6 -> :sswitch_38
        0xc0fb19c -> :sswitch_29
        0x15caa0f0 -> :sswitch_12
        0x17dd56c2 -> :sswitch_21
        0x1f91b402 -> :sswitch_27
        0x227eceb6 -> :sswitch_34
        0x2a8c788b -> :sswitch_2c
        0x2c2c84fa -> :sswitch_8
        0x2c929929 -> :sswitch_2f
        0x3a1ea90e -> :sswitch_1d
        0x3d759362 -> :sswitch_f
        0x41194293 -> :sswitch_2
        0x4153afa0 -> :sswitch_35
        0x43f1c4f3 -> :sswitch_24
        0x462ab79f -> :sswitch_36
        0x4cb7f6d5 -> :sswitch_3
        0x4ccfd1e9 -> :sswitch_26
        0x501666a7 -> :sswitch_2e
        0x5551c344 -> :sswitch_1f
        0x67ef5bac -> :sswitch_e
        0x67f69fe3 -> :sswitch_10
        0x6953cff1 -> :sswitch_1
        0x6ee75fc9 -> :sswitch_16
        0x757a12d5 -> :sswitch_1c
        0x79180351 -> :sswitch_33
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 0

    .prologue
    .line 2761809
    check-cast p1, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;

    invoke-static {p1, p2, p3}, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode$$PropsSetter;->a(Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;Ljava/lang/String;LX/5rC;)V

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2761810
    const-string v0, "alignItems"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761811
    const-string v0, "alignSelf"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761812
    const-string v0, "aspectRatio"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761813
    const-string v0, "backgroundColor"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761814
    const-string v0, "borderBottomWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761815
    const-string v0, "borderLeftWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761816
    const-string v0, "borderRightWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761817
    const-string v0, "borderTopWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761818
    const-string v0, "borderWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761819
    const-string v0, "bottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761820
    const-string v0, "color"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761821
    const-string v0, "flex"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761822
    const-string v0, "flexBasis"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761823
    const-string v0, "flexDirection"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761824
    const-string v0, "flexGrow"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761825
    const-string v0, "flexShrink"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761826
    const-string v0, "flexWrap"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761827
    const-string v0, "fontFamily"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761828
    const-string v0, "fontSize"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761829
    const-string v0, "fontStyle"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761830
    const-string v0, "fontWeight"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761831
    const-string v0, "height"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761832
    const-string v0, "justifyContent"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761833
    const-string v0, "left"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761834
    const-string v0, "lineHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761835
    const-string v0, "margin"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761836
    const-string v0, "marginBottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761837
    const-string v0, "marginHorizontal"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761838
    const-string v0, "marginLeft"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761839
    const-string v0, "marginRight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761840
    const-string v0, "marginTop"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761841
    const-string v0, "marginVertical"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761842
    const-string v0, "maxHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761843
    const-string v0, "maxWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761844
    const-string v0, "minHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761845
    const-string v0, "minWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761846
    const-string v0, "mostRecentEventCount"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761847
    const-string v0, "numberOfLines"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761848
    const-string v0, "onLayout"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761849
    const-string v0, "overflow"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761850
    const-string v0, "padding"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761851
    const-string v0, "paddingBottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761852
    const-string v0, "paddingHorizontal"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761853
    const-string v0, "paddingLeft"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761854
    const-string v0, "paddingRight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761855
    const-string v0, "paddingTop"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761856
    const-string v0, "paddingVertical"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761857
    const-string v0, "position"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761858
    const-string v0, "right"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761859
    const-string v0, "text"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761860
    const-string v0, "textAlign"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761861
    const-string v0, "textDecorationLine"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761862
    const-string v0, "textShadowColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761863
    const-string v0, "textShadowOffset"

    const-string v1, "Map"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761864
    const-string v0, "textShadowRadius"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761865
    const-string v0, "top"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761866
    const-string v0, "width"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761867
    return-void
.end method
