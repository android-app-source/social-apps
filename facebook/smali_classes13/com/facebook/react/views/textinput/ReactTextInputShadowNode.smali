.class public Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;
.super Lcom/facebook/react/views/text/ReactTextShadowNode;
.source ""

# interfaces
.implements Lcom/facebook/csslayout/YogaMeasureFunction;


# annotations
.annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private e:Landroid/widget/EditText;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2761880
    invoke-direct {p0}, Lcom/facebook/react/views/text/ReactTextShadowNode;-><init>()V

    .line 2761881
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->g:I

    .line 2761882
    invoke-virtual {p0, p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2761883
    return-void
.end method


# virtual methods
.method public final a(LX/5rJ;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, -0x2

    const/4 v2, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 2761898
    invoke-super {p0, p1}, Lcom/facebook/react/views/text/ReactTextShadowNode;->a(LX/5rJ;)V

    .line 2761899
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    .line 2761900
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2761901
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getPaddingStart()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2761902
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2761903
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getPaddingEnd()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v6, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2761904
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2761905
    new-array v0, v2, [F

    const/4 v1, 0x0

    invoke-virtual {p0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v0, v1

    invoke-virtual {p0, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    aput v1, v0, v3

    const/4 v1, 0x2

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v0, v1

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    aput v1, v0, v4

    iput-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->f:[F

    .line 2761906
    return-void
.end method

.method public final a(LX/5rl;)V
    .locals 9

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x4

    const/4 v7, 0x3

    const/4 v5, 0x1

    .line 2761884
    invoke-super {p0, p1}, Lcom/facebook/react/views/text/ReactTextShadowNode;->a(LX/5rl;)V

    .line 2761885
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->f:[F

    if-eqz v0, :cond_1

    .line 2761886
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->f:[F

    .line 2761887
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->F()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v1

    sget-object v2, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    if-ne v1, v2, :cond_0

    .line 2761888
    new-array v0, v4, [F

    const/4 v1, 0x0

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v0, v1

    invoke-virtual {p0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    aput v1, v0, v5

    const/4 v1, 0x2

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v0, v1

    invoke-virtual {p0, v7}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    aput v1, v0, v7

    .line 2761889
    :cond_0
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 2761890
    invoke-virtual {p1, v1, v0}, LX/5rl;->a(ILjava/lang/Object;)V

    .line 2761891
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->f:[F

    .line 2761892
    :cond_1
    iget v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 2761893
    invoke-static {p0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->a(Lcom/facebook/react/views/text/ReactTextShadowNode;)Landroid/text/Spannable;

    move-result-object v1

    .line 2761894
    new-instance v0, LX/K10;

    iget v2, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->g:I

    iget-boolean v3, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->d:Z

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v4

    invoke-virtual {p0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v5

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v6

    invoke-virtual {p0, v7}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v7

    iget v8, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    invoke-direct/range {v0 .. v8}, LX/K10;-><init>(Landroid/text/Spannable;IZFFFFI)V

    .line 2761895
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 2761896
    invoke-virtual {p1, v1, v0}, LX/5rl;->a(ILjava/lang/Object;)V

    .line 2761897
    :cond_2
    return-void
.end method

.method public final c(IF)V
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 2761907
    invoke-super {p0, p1, p2}, Lcom/facebook/react/views/text/ReactTextShadowNode;->c(IF)V

    .line 2761908
    new-array v0, v2, [F

    const/4 v1, 0x0

    invoke-virtual {p0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v0, v1

    invoke-virtual {p0, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    aput v1, v0, v3

    const/4 v1, 0x2

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v0, v1

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    aput v1, v0, v4

    iput-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->f:[F

    .line 2761909
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2761910
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 2761868
    return-void
.end method

.method public final measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v4, 0x1

    .line 2761869
    iget-object v0, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->e:Landroid/widget/EditText;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2761870
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    if-ne v1, v7, :cond_1

    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {v1}, LX/5r2;->b(F)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    :goto_0
    invoke-virtual {v0, v8, v1}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 2761871
    new-array v1, v5, [F

    invoke-virtual {p0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v1, v8

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v1, v4

    const/4 v2, 0x2

    const/4 v3, 0x5

    invoke-virtual {p0, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v3

    aput v3, v1, v2

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    aput v2, v1, v6

    iput-object v1, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->f:[F

    .line 2761872
    invoke-virtual {p0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    const/4 v3, 0x5

    invoke-virtual {p0, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 2761873
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    if-eq v1, v7, :cond_0

    .line 2761874
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLines(I)V

    .line 2761875
    :cond_0
    invoke-static {p2, p3}, LX/9nU;->a(FLcom/facebook/csslayout/YogaMeasureMode;)I

    move-result v1

    invoke-static {p4, p5}, LX/9nU;->a(FLcom/facebook/csslayout/YogaMeasureMode;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->measure(II)V

    .line 2761876
    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, LX/1Dr;->a(II)J

    move-result-wide v0

    return-wide v0

    .line 2761877
    :cond_1
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    int-to-float v1, v1

    goto :goto_0
.end method

.method public setMostRecentEventCount(I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "mostRecentEventCount"
    .end annotation

    .prologue
    .line 2761878
    iput p1, p0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;->g:I

    .line 2761879
    return-void
.end method
