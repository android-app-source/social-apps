.class public Lcom/facebook/react/views/image/ReactImageManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTImageView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/K0P;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/1Ae;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2758417
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2758418
    iput-object v0, p0, Lcom/facebook/react/views/image/ReactImageManager;->a:LX/1Ae;

    .line 2758419
    iput-object v0, p0, Lcom/facebook/react/views/image/ReactImageManager;->b:Ljava/lang/Object;

    .line 2758420
    return-void
.end method

.method public constructor <init>(LX/1Ae;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2758407
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2758408
    iput-object p1, p0, Lcom/facebook/react/views/image/ReactImageManager;->a:LX/1Ae;

    .line 2758409
    iput-object p2, p0, Lcom/facebook/react/views/image/ReactImageManager;->b:Ljava/lang/Object;

    .line 2758410
    return-void
.end method

.method private a(LX/K0P;)V
    .locals 0

    .prologue
    .line 2758411
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/SimpleViewManager;->b(Landroid/view/View;)V

    .line 2758412
    invoke-virtual {p1}, LX/K0P;->c()V

    .line 2758413
    return-void
.end method

.method private b(LX/5rJ;)LX/K0P;
    .locals 3

    .prologue
    .line 2758414
    new-instance v0, LX/K0P;

    invoke-virtual {p0}, Lcom/facebook/react/views/image/ReactImageManager;->t()LX/1Ae;

    move-result-object v1

    .line 2758415
    iget-object v2, p0, Lcom/facebook/react/views/image/ReactImageManager;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 2758416
    invoke-direct {v0, p1, v1, v2}, LX/K0P;-><init>(Landroid/content/Context;LX/1Ae;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2758372
    invoke-direct {p0, p1}, Lcom/facebook/react/views/image/ReactImageManager;->b(LX/5rJ;)LX/K0P;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2758421
    check-cast p1, LX/K0P;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/image/ReactImageManager;->a(LX/K0P;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2758422
    const-string v0, "RCTImageView"

    return-object v0
.end method

.method public setBorderColor(LX/K0P;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "borderColor"
    .end annotation

    .prologue
    .line 2758423
    if-nez p2, :cond_0

    .line 2758424
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/K0P;->setBorderColor(I)V

    .line 2758425
    :goto_0
    return-void

    .line 2758426
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/K0P;->setBorderColor(I)V

    goto :goto_0
.end method

.method public setBorderRadius(LX/K0P;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderRadius",
            "borderTopLeftRadius",
            "borderTopRightRadius",
            "borderBottomRightRadius",
            "borderBottomLeftRadius"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 2758427
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2758428
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 2758429
    :cond_0
    if-nez p2, :cond_1

    .line 2758430
    invoke-virtual {p1, p3}, LX/K0P;->setBorderRadius(F)V

    .line 2758431
    :goto_0
    return-void

    .line 2758432
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p1, p3, v0}, LX/K0P;->a(FI)V

    goto :goto_0
.end method

.method public setBorderWidth(LX/K0P;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderWidth"
    .end annotation

    .prologue
    .line 2758403
    invoke-virtual {p1, p2}, LX/K0P;->setBorderWidth(F)V

    .line 2758404
    return-void
.end method

.method public setFadeDuration(LX/K0P;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fadeDuration"
    .end annotation

    .prologue
    .line 2758405
    iput p2, p1, LX/K0P;->u:I

    .line 2758406
    return-void
.end method

.method public setLoadHandlersRegistered(LX/K0P;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "shouldNotifyLoadEvents"
    .end annotation

    .prologue
    .line 2758401
    invoke-virtual {p1, p2}, LX/K0P;->setShouldNotifyLoadEvents(Z)V

    .line 2758402
    return-void
.end method

.method public setLoadingIndicatorSource(LX/K0P;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "loadingIndicatorSrc"
    .end annotation

    .prologue
    .line 2758399
    invoke-virtual {p1, p2}, LX/K0P;->setLoadingIndicatorSource(Ljava/lang/String;)V

    .line 2758400
    return-void
.end method

.method public setOverlayColor(LX/K0P;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "overlayColor"
    .end annotation

    .prologue
    .line 2758395
    if-nez p2, :cond_0

    .line 2758396
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/K0P;->setOverlayColor(I)V

    .line 2758397
    :goto_0
    return-void

    .line 2758398
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/K0P;->setOverlayColor(I)V

    goto :goto_0
.end method

.method public setProgressiveRenderingEnabled(LX/K0P;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "progressiveRenderingEnabled"
    .end annotation

    .prologue
    .line 2758393
    iput-boolean p2, p1, LX/K0P;->v:Z

    .line 2758394
    return-void
.end method

.method public setResizeMethod(LX/K0P;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "resizeMethod"
    .end annotation

    .prologue
    .line 2758385
    if-eqz p2, :cond_0

    const-string v0, "auto"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2758386
    :cond_0
    sget-object v0, LX/K0L;->AUTO:LX/K0L;

    invoke-virtual {p1, v0}, LX/K0P;->setResizeMethod(LX/K0L;)V

    .line 2758387
    :goto_0
    return-void

    .line 2758388
    :cond_1
    const-string v0, "resize"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2758389
    sget-object v0, LX/K0L;->RESIZE:LX/K0L;

    invoke-virtual {p1, v0}, LX/K0P;->setResizeMethod(LX/K0L;)V

    goto :goto_0

    .line 2758390
    :cond_2
    const-string v0, "scale"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2758391
    sget-object v0, LX/K0L;->SCALE:LX/K0L;

    invoke-virtual {p1, v0}, LX/K0P;->setResizeMethod(LX/K0L;)V

    goto :goto_0

    .line 2758392
    :cond_3
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid resize method: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setResizeMode(LX/K0P;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "resizeMode"
    .end annotation

    .prologue
    .line 2758383
    invoke-static {p2}, LX/K0M;->a(Ljava/lang/String;)LX/1Up;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/K0P;->setScaleType(LX/1Up;)V

    .line 2758384
    return-void
.end method

.method public setSource(LX/K0P;LX/5pC;)V
    .locals 0
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "src"
    .end annotation

    .prologue
    .line 2758381
    invoke-virtual {p1, p2}, LX/K0P;->setSource(LX/5pC;)V

    .line 2758382
    return-void
.end method

.method public setTintColor(LX/K0P;Ljava/lang/Integer;)V
    .locals 2
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "tintColor"
    .end annotation

    .prologue
    .line 2758377
    if-nez p2, :cond_0

    .line 2758378
    invoke-virtual {p1}, LX/K0P;->clearColorFilter()V

    .line 2758379
    :goto_0
    return-void

    .line 2758380
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, LX/K0P;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method public final t()LX/1Ae;
    .locals 1

    .prologue
    .line 2758374
    iget-object v0, p0, Lcom/facebook/react/views/image/ReactImageManager;->a:LX/1Ae;

    if-nez v0, :cond_0

    .line 2758375
    invoke-static {}, LX/4AN;->a()LX/1bk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/image/ReactImageManager;->a:LX/1Ae;

    .line 2758376
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/views/image/ReactImageManager;->a:LX/1Ae;

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2758373
    const/4 v0, 0x4

    invoke-static {v0}, LX/K0K;->b(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "registrationName"

    const-string v2, "onLoadStart"

    invoke-static {v1, v2}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, LX/K0K;->b(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "registrationName"

    const-string v4, "onLoad"

    invoke-static {v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, LX/K0K;->b(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "registrationName"

    const-string v6, "onError"

    invoke-static {v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    const/4 v6, 0x3

    invoke-static {v6}, LX/K0K;->b(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "registrationName"

    const-string v8, "onLoadEnd"

    invoke-static {v7, v8}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
