.class public abstract Lcom/facebook/react/views/picker/ReactPickerManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/K0d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2759034
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2759035
    return-void
.end method

.method private static a(LX/5rJ;LX/K0d;)V
    .locals 2

    .prologue
    .line 2759063
    new-instance v1, LX/K0e;

    const-class v0, LX/5rQ;

    invoke-virtual {p0, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2759064
    iget-object p0, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, p0

    .line 2759065
    invoke-direct {v1, p1, v0}, LX/K0e;-><init>(LX/K0d;LX/5s9;)V

    invoke-virtual {p1, v1}, LX/K0d;->setOnSelectListener(LX/K0c;)V

    .line 2759066
    return-void
.end method

.method private a(LX/K0d;)V
    .locals 0

    .prologue
    .line 2759060
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/SimpleViewManager;->b(Landroid/view/View;)V

    .line 2759061
    invoke-virtual {p1}, LX/K0d;->a()V

    .line 2759062
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2759059
    check-cast p2, LX/K0d;

    invoke-static {p1, p2}, Lcom/facebook/react/views/picker/ReactPickerManager;->a(LX/5rJ;LX/K0d;)V

    return-void
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2759058
    check-cast p1, LX/K0d;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/picker/ReactPickerManager;->a(LX/K0d;)V

    return-void
.end method

.method public setColor(LX/K0d;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "color"
    .end annotation

    .prologue
    .line 2759053
    iput-object p2, p1, LX/K0d;->b:Ljava/lang/Integer;

    .line 2759054
    invoke-virtual {p1}, LX/K0d;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/K0f;

    .line 2759055
    if-eqz v0, :cond_0

    .line 2759056
    invoke-virtual {v0, p2}, LX/K0f;->a(Ljava/lang/Integer;)V

    .line 2759057
    :cond_0
    return-void
.end method

.method public setEnabled(LX/K0d;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "enabled"
    .end annotation

    .prologue
    .line 2759051
    invoke-virtual {p1, p2}, LX/K0d;->setEnabled(Z)V

    .line 2759052
    return-void
.end method

.method public setItems(LX/K0d;LX/5pC;)V
    .locals 3
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "items"
    .end annotation

    .prologue
    .line 2759040
    if-eqz p2, :cond_1

    .line 2759041
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    new-array v1, v0, [LX/5pG;

    .line 2759042
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2759043
    invoke-interface {p2, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2759044
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2759045
    :cond_0
    new-instance v0, LX/K0f;

    invoke-virtual {p1}, LX/K0d;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/K0f;-><init>(Landroid/content/Context;[LX/5pG;)V

    .line 2759046
    iget-object v1, p1, LX/K0d;->b:Ljava/lang/Integer;

    move-object v1, v1

    .line 2759047
    invoke-virtual {v0, v1}, LX/K0f;->a(Ljava/lang/Integer;)V

    .line 2759048
    invoke-virtual {p1, v0}, LX/K0d;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2759049
    :goto_1
    return-void

    .line 2759050
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/K0d;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_1
.end method

.method public setPrompt(LX/K0d;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "prompt"
    .end annotation

    .prologue
    .line 2759038
    invoke-virtual {p1, p2}, LX/K0d;->setPrompt(Ljava/lang/CharSequence;)V

    .line 2759039
    return-void
.end method

.method public setSelected(LX/K0d;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "selected"
    .end annotation

    .prologue
    .line 2759036
    invoke-virtual {p1, p2}, LX/K0d;->setStagedSelection(I)V

    .line 2759037
    return-void
.end method
