.class public final Lcom/facebook/react/views/picker/ReactPicker$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/K0d;


# direct methods
.method public constructor <init>(LX/K0d;)V
    .locals 0

    .prologue
    .line 2759143
    iput-object p1, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 2759144
    iget-object v0, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    iget-object v1, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-virtual {v1}, LX/K0d;->getWidth()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-virtual {v2}, LX/K0d;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/K0d;->measure(II)V

    .line 2759145
    iget-object v0, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    iget-object v1, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-virtual {v1}, LX/K0d;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-virtual {v2}, LX/K0d;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-virtual {v3}, LX/K0d;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/react/views/picker/ReactPicker$1;->a:LX/K0d;

    invoke-virtual {v4}, LX/K0d;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/K0d;->layout(IIII)V

    .line 2759146
    return-void
.end method
