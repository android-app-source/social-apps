.class public Lcom/facebook/react/views/text/ReactRawTextManager;
.super Lcom/facebook/react/views/text/ReactTextViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTRawText"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2760097
    invoke-direct {p0}, Lcom/facebook/react/views/text/ReactTextViewManager;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2760096
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/text/ReactTextViewManager;->b(LX/5rJ;)LX/K11;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/K11;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2760095
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2760098
    check-cast p1, LX/K11;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/react/views/text/ReactTextViewManager;->a(LX/K11;Ljava/lang/Object;)V

    return-void
.end method

.method public final b(LX/5rJ;)LX/K11;
    .locals 2

    .prologue
    .line 2760094
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RKRawText doesn\'t map into a native view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2760093
    const-string v0, "RCTRawText"

    return-object v0
.end method

.method public final h()Lcom/facebook/react/views/text/ReactTextShadowNode;
    .locals 1

    .prologue
    .line 2760092
    new-instance v0, Lcom/facebook/react/views/text/ReactVirtualTextShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/text/ReactVirtualTextShadowNode;-><init>()V

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2760091
    invoke-virtual {p0}, Lcom/facebook/react/views/text/ReactTextViewManager;->h()Lcom/facebook/react/views/text/ReactTextShadowNode;

    move-result-object v0

    return-object v0
.end method
