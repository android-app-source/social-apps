.class public Lcom/facebook/react/views/text/ReactTextViewManager;
.super Lcom/facebook/react/uimanager/BaseViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTText"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/BaseViewManager",
        "<",
        "LX/K11;",
        "Lcom/facebook/react/views/text/ReactTextShadowNode;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2760089
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/react/views/text/ReactTextViewManager;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x8
        0x0
        0x2
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2760088
    invoke-direct {p0}, Lcom/facebook/react/uimanager/BaseViewManager;-><init>()V

    return-void
.end method

.method private a(LX/K11;)V
    .locals 0

    .prologue
    .line 2760085
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/BaseViewManager;->b(Landroid/view/View;)V

    .line 2760086
    invoke-virtual {p1}, LX/K11;->a()V

    .line 2760087
    return-void
.end method


# virtual methods
.method public synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2760084
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/text/ReactTextViewManager;->b(LX/5rJ;)LX/K11;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/K11;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2760077
    check-cast p2, LX/K10;

    .line 2760078
    iget-boolean v0, p2, LX/K10;->c:Z

    move v0, v0

    .line 2760079
    if-eqz v0, :cond_0

    .line 2760080
    iget-object v0, p2, LX/K10;->a:Landroid/text/Spannable;

    move-object v0, v0

    .line 2760081
    invoke-static {v0, p1}, LX/K13;->a(Landroid/text/Spannable;Landroid/widget/TextView;)V

    .line 2760082
    :cond_0
    invoke-virtual {p1, p2}, LX/K11;->setText(LX/K10;)V

    .line 2760083
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2760076
    check-cast p1, LX/K11;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/react/views/text/ReactTextViewManager;->a(LX/K11;Ljava/lang/Object;)V

    return-void
.end method

.method public b(LX/5rJ;)LX/K11;
    .locals 1

    .prologue
    .line 2760075
    new-instance v0, LX/K11;

    invoke-direct {v0, p1}, LX/K11;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2760074
    check-cast p1, LX/K11;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/text/ReactTextViewManager;->a(LX/K11;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2760073
    const-string v0, "RCTText"

    return-object v0
.end method

.method public h()Lcom/facebook/react/views/text/ReactTextShadowNode;
    .locals 1

    .prologue
    .line 2760072
    new-instance v0, Lcom/facebook/react/views/text/ReactTextShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/text/ReactTextShadowNode;-><init>()V

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/react/views/text/ReactTextShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2760090
    const-class v0, Lcom/facebook/react/views/text/ReactTextShadowNode;

    return-object v0
.end method

.method public synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2760024
    invoke-virtual {p0}, Lcom/facebook/react/views/text/ReactTextViewManager;->h()Lcom/facebook/react/views/text/ReactTextShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public setBorderColor(LX/K11;ILjava/lang/Integer;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderColor",
            "borderLeftColor",
            "borderRightColor",
            "borderTopColor",
            "borderBottomColor"
        }
        customType = "Color"
    .end annotation

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 2760027
    if-nez p3, :cond_0

    move v1, v0

    .line 2760028
    :goto_0
    if-nez p3, :cond_1

    .line 2760029
    :goto_1
    sget-object v2, Lcom/facebook/react/views/text/ReactTextViewManager;->a:[I

    aget v2, v2, p2

    invoke-virtual {p1, v2, v1, v0}, LX/K11;->a(IFF)V

    .line 2760030
    return-void

    .line 2760031
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0xffffff

    and-int/2addr v1, v2

    int-to-float v1, v1

    goto :goto_0

    .line 2760032
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    ushr-int/lit8 v0, v0, 0x18

    int-to-float v0, v0

    goto :goto_1
.end method

.method public setBorderRadius(LX/K11;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderRadius",
            "borderTopLeftRadius",
            "borderTopRightRadius",
            "borderBottomRightRadius",
            "borderBottomLeftRadius"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 2760033
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2760034
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 2760035
    :cond_0
    if-nez p2, :cond_1

    .line 2760036
    invoke-virtual {p1, p3}, LX/K11;->setBorderRadius(F)V

    .line 2760037
    :goto_0
    return-void

    .line 2760038
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p1, p3, v0}, LX/K11;->a(FI)V

    goto :goto_0
.end method

.method public setBorderStyle(LX/K11;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderStyle"
    .end annotation

    .prologue
    .line 2760039
    invoke-virtual {p1, p2}, LX/K11;->setBorderStyle(Ljava/lang/String;)V

    .line 2760040
    return-void
.end method

.method public setBorderWidth(LX/K11;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderWidth",
            "borderLeftWidth",
            "borderRightWidth",
            "borderTopWidth",
            "borderBottomWidth"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 2760041
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2760042
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 2760043
    :cond_0
    sget-object v0, Lcom/facebook/react/views/text/ReactTextViewManager;->a:[I

    aget v0, v0, p2

    invoke-virtual {p1, v0, p3}, LX/K11;->a(IF)V

    .line 2760044
    return-void
.end method

.method public setEllipsizeMode(LX/K11;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "ellipsizeMode"
    .end annotation

    .prologue
    .line 2760045
    if-eqz p2, :cond_0

    const-string v0, "tail"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2760046
    :cond_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 2760047
    iput-object v0, p1, LX/K11;->i:Landroid/text/TextUtils$TruncateAt;

    .line 2760048
    :goto_0
    return-void

    .line 2760049
    :cond_1
    const-string v0, "head"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2760050
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    .line 2760051
    iput-object v0, p1, LX/K11;->i:Landroid/text/TextUtils$TruncateAt;

    .line 2760052
    goto :goto_0

    .line 2760053
    :cond_2
    const-string v0, "middle"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2760054
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    .line 2760055
    iput-object v0, p1, LX/K11;->i:Landroid/text/TextUtils$TruncateAt;

    .line 2760056
    goto :goto_0

    .line 2760057
    :cond_3
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid ellipsizeMode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setIncludeFontPadding(LX/K11;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "includeFontPadding"
    .end annotation

    .prologue
    .line 2760058
    invoke-virtual {p1, p2}, LX/K11;->setIncludeFontPadding(Z)V

    .line 2760059
    return-void
.end method

.method public setNumberOfLines(LX/K11;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x7fffffff
        name = "numberOfLines"
    .end annotation

    .prologue
    .line 2760025
    invoke-virtual {p1, p2}, LX/K11;->setNumberOfLines(I)V

    .line 2760026
    return-void
.end method

.method public setSelectable(LX/K11;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "selectable"
    .end annotation

    .prologue
    .line 2760060
    invoke-virtual {p1, p2}, LX/K11;->setTextIsSelectable(Z)V

    .line 2760061
    return-void
.end method

.method public setTextAlignVertical(LX/K11;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textAlignVertical"
    .end annotation

    .prologue
    .line 2760062
    if-eqz p2, :cond_0

    const-string v0, "auto"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2760063
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/K11;->setGravityVertical(I)V

    .line 2760064
    :goto_0
    return-void

    .line 2760065
    :cond_1
    const-string v0, "top"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2760066
    const/16 v0, 0x30

    invoke-virtual {p1, v0}, LX/K11;->setGravityVertical(I)V

    goto :goto_0

    .line 2760067
    :cond_2
    const-string v0, "bottom"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2760068
    const/16 v0, 0x50

    invoke-virtual {p1, v0}, LX/K11;->setGravityVertical(I)V

    goto :goto_0

    .line 2760069
    :cond_3
    const-string v0, "center"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2760070
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, LX/K11;->setGravityVertical(I)V

    goto :goto_0

    .line 2760071
    :cond_4
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid textAlignVertical: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method
