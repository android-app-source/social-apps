.class public Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;
.super Lcom/facebook/react/views/text/ReactTextInlineImageShadowNode;
.source ""


# instance fields
.field public a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/1Ae;

.field public final c:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:F

.field private e:F


# direct methods
.method public constructor <init>(LX/1Ae;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 2761042
    invoke-direct {p0}, Lcom/facebook/react/views/text/ReactTextInlineImageShadowNode;-><init>()V

    .line 2761043
    iput v0, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->d:F

    .line 2761044
    iput v0, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->e:F

    .line 2761045
    iput-object p1, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->b:LX/1Ae;

    .line 2761046
    iput-object p2, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->c:Ljava/lang/Object;

    .line 2761047
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2761072
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2761073
    :cond_0
    const/4 v0, 0x0

    .line 2761074
    :goto_0
    return-object v0

    .line 2761075
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2761076
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2761077
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "res"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final H()LX/K13;
    .locals 7

    .prologue
    .line 2761063
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v0

    invoke-virtual {v0}, LX/5rJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2761064
    iget v0, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->d:F

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 2761065
    iget v0, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->e:F

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 2761066
    new-instance v0, LX/K14;

    .line 2761067
    iget-object v4, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->a:Landroid/net/Uri;

    move-object v4, v4

    .line 2761068
    iget-object v5, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->b:LX/1Ae;

    move-object v5, v5

    .line 2761069
    iget-object v6, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->c:Ljava/lang/Object;

    move-object v6, v6

    .line 2761070
    invoke-direct/range {v0 .. v6}, LX/K14;-><init>(Landroid/content/res/Resources;IILandroid/net/Uri;LX/1Ae;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2761071
    const/4 v0, 0x1

    return v0
.end method

.method public final setHeight(F)V
    .locals 0

    .prologue
    .line 2761061
    iput p1, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->e:F

    .line 2761062
    return-void
.end method

.method public setSource(LX/5pC;)V
    .locals 4
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "src"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2761050
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5pC;->size()I

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    move-object v2, v0

    .line 2761051
    :goto_0
    if-eqz v2, :cond_1

    .line 2761052
    :try_start_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2761053
    :try_start_1
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    if-nez v3, :cond_4

    .line 2761054
    :goto_1
    if-nez v0, :cond_1

    .line 2761055
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2761056
    :cond_1
    iget-object v1, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->a:Landroid/net/Uri;

    if-eq v0, v1, :cond_2

    .line 2761057
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2761058
    :cond_2
    iput-object v0, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->a:Landroid/net/Uri;

    .line 2761059
    return-void

    .line 2761060
    :cond_3
    const/4 v1, 0x0

    invoke-interface {p1, v1}, LX/5pC;->a(I)LX/5pG;

    move-result-object v1

    const-string v2, "uri"

    invoke-interface {v1, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    :catch_0
    move-object v1, v0

    :cond_4
    :goto_2
    move-object v0, v1

    goto :goto_1

    :catch_1
    goto :goto_2
.end method

.method public final setWidth(F)V
    .locals 0

    .prologue
    .line 2761048
    iput p1, p0, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageShadowNode;->d:F

    .line 2761049
    return-void
.end method
