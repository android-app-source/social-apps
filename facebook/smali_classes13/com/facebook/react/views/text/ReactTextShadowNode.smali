.class public Lcom/facebook/react/views/text/ReactTextShadowNode;
.super Lcom/facebook/react/uimanager/LayoutShadowNode;
.source ""


# static fields
.field public static final e:Landroid/text/TextPaint;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Z

.field private final f:Lcom/facebook/csslayout/YogaMeasureFunction;

.field private g:F

.field private h:Z

.field private i:I

.field private j:Z

.field private k:I

.field private l:F

.field private m:F

.field private n:F

.field private o:I

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Landroid/text/Spannable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2760343
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 2760344
    sput-object v0, Lcom/facebook/react/views/text/ReactTextShadowNode;->e:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFlags(I)V

    .line 2760345
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x7fc00000    # NaNf

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2760346
    invoke-direct {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;-><init>()V

    .line 2760347
    new-instance v0, LX/K0y;

    invoke-direct {v0, p0}, LX/K0y;-><init>(Lcom/facebook/react/views/text/ReactTextShadowNode;)V

    iput-object v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->f:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 2760348
    iput v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->g:F

    .line 2760349
    iput-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->h:Z

    .line 2760350
    iput-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->j:Z

    .line 2760351
    iput v2, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    .line 2760352
    iput v2, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    .line 2760353
    iput v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    .line 2760354
    iput v3, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->l:F

    .line 2760355
    iput v3, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->m:F

    .line 2760356
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->n:F

    .line 2760357
    const/high16 v0, 0x55000000

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->o:I

    .line 2760358
    iput-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->p:Z

    .line 2760359
    iput-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->q:Z

    .line 2760360
    iput v2, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->r:I

    .line 2760361
    iput v2, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->s:I

    .line 2760362
    iput-object v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->t:Ljava/lang/String;

    .line 2760363
    iput-object v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->u:Ljava/lang/String;

    .line 2760364
    iput-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->d:Z

    .line 2760365
    iput v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    .line 2760366
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2760367
    iget-object v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->f:Lcom/facebook/csslayout/YogaMeasureFunction;

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2760368
    :cond_0
    return-void
.end method

.method private I()F
    .locals 2

    .prologue
    .line 2760369
    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->g:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->g:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2760370
    :goto_0
    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    :goto_1
    return v0

    .line 2760371
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2760372
    :cond_1
    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->g:F

    goto :goto_1
.end method

.method private J()I
    .locals 5

    .prologue
    const/4 v1, 0x5

    const/4 v0, 0x3

    .line 2760373
    iget v2, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    .line 2760374
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->F()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v3

    sget-object v4, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    if-ne v3, v4, :cond_1

    .line 2760375
    if-ne v2, v1, :cond_0

    .line 2760376
    :goto_0
    return v0

    .line 2760377
    :cond_0
    if-ne v2, v0, :cond_1

    move v0, v1

    .line 2760378
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static a(Lcom/facebook/react/views/text/ReactTextShadowNode;)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2760379
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2760380
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2760381
    invoke-static {p0, v3, v4}, Lcom/facebook/react/views/text/ReactTextShadowNode;->a(Lcom/facebook/react/views/text/ReactTextShadowNode;Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 2760382
    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2760383
    new-instance v0, Landroid/text/style/AbsoluteSizeSpan;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {v1}, LX/5r2;->b(F)F

    move-result v1

    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v1, v6

    invoke-direct {v0, v1}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v2, 0x11

    invoke-virtual {v3, v0, v5, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2760384
    :cond_0
    iput-boolean v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->d:Z

    .line 2760385
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    .line 2760386
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 2760387
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K0z;

    .line 2760388
    iget-object v1, v0, LX/K0z;->c:Ljava/lang/Object;

    instance-of v1, v1, LX/K13;

    if-eqz v1, :cond_2

    .line 2760389
    iget-object v1, v0, LX/K0z;->c:Ljava/lang/Object;

    check-cast v1, LX/K13;

    invoke-virtual {v1}, LX/K13;->f()I

    move-result v1

    .line 2760390
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->d:Z

    .line 2760391
    iget v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_1

    int-to-float v5, v1

    iget v6, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 2760392
    :cond_1
    int-to-float v1, v1

    iput v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->w:F

    .line 2760393
    :cond_2
    invoke-virtual {v0, v3}, LX/K0z;->a(Landroid/text/SpannableStringBuilder;)V

    .line 2760394
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 2760395
    :cond_3
    return-object v3
.end method

.method private static a(Lcom/facebook/react/views/text/ReactTextShadowNode;Landroid/text/SpannableStringBuilder;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/react/views/text/ReactTextShadowNode;",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/util/List",
            "<",
            "LX/K0z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 2760413
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 2760414
    iget-object v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2760415
    iget-object v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2760416
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    .line 2760417
    invoke-virtual {p0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 2760418
    instance-of v0, v1, Lcom/facebook/react/views/text/ReactTextShadowNode;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2760419
    check-cast v0, Lcom/facebook/react/views/text/ReactTextShadowNode;

    invoke-static {v0, p1, p2}, Lcom/facebook/react/views/text/ReactTextShadowNode;->a(Lcom/facebook/react/views/text/ReactTextShadowNode;Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 2760420
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2760421
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2760422
    :cond_1
    instance-of v0, v1, Lcom/facebook/react/views/text/ReactTextInlineImageShadowNode;

    if-eqz v0, :cond_2

    .line 2760423
    const-string v0, "I"

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2760424
    new-instance v5, LX/K0z;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    move-object v0, v1

    check-cast v0, Lcom/facebook/react/views/text/ReactTextInlineImageShadowNode;

    invoke-virtual {v0}, Lcom/facebook/react/views/text/ReactTextInlineImageShadowNode;->H()LX/K13;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2760425
    :cond_2
    new-instance v0, LX/5qo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected view type nested under text node: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2760426
    :cond_3
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2760427
    if-lt v0, v3, :cond_e

    .line 2760428
    iget-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->h:Z

    if-eqz v1, :cond_4

    .line 2760429
    new-instance v1, LX/K0z;

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->i:I

    invoke-direct {v2, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760430
    :cond_4
    iget-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->j:Z

    if-eqz v1, :cond_5

    .line 2760431
    new-instance v1, LX/K0z;

    new-instance v2, Landroid/text/style/BackgroundColorSpan;

    iget v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->k:I

    invoke-direct {v2, v4}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760432
    :cond_5
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    if-eq v1, v8, :cond_6

    .line 2760433
    new-instance v1, LX/K0z;

    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    iget v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    invoke-direct {v2, v4}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760434
    :cond_6
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->r:I

    if-ne v1, v8, :cond_7

    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->s:I

    if-ne v1, v8, :cond_7

    iget-object v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->t:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 2760435
    :cond_7
    new-instance v1, LX/K0z;

    new-instance v2, LX/K0t;

    iget v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->r:I

    iget v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->s:I

    iget-object v6, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->t:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v7

    invoke-virtual {v7}, LX/5rJ;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    invoke-direct {v2, v4, v5, v6, v7}, LX/K0t;-><init>(IILjava/lang/String;Landroid/content/res/AssetManager;)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760436
    :cond_8
    iget-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->p:Z

    if-eqz v1, :cond_9

    .line 2760437
    new-instance v1, LX/K0z;

    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760438
    :cond_9
    iget-boolean v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->q:Z

    if-eqz v1, :cond_a

    .line 2760439
    new-instance v1, LX/K0z;

    new-instance v2, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v2}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760440
    :cond_a
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->l:F

    cmpl-float v1, v1, v9

    if-nez v1, :cond_b

    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->m:F

    cmpl-float v1, v1, v9

    if-eqz v1, :cond_c

    .line 2760441
    :cond_b
    new-instance v1, LX/K0z;

    new-instance v2, LX/K12;

    iget v4, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->l:F

    iget v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->m:F

    iget v6, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->n:F

    iget v7, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->o:I

    invoke-direct {v2, v4, v5, v6, v7}, LX/K12;-><init>(FFFI)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760442
    :cond_c
    invoke-direct {p0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->I()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2760443
    new-instance v1, LX/K0z;

    new-instance v2, LX/K0s;

    invoke-direct {p0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->I()F

    move-result v4

    invoke-direct {v2, v4}, LX/K0s;-><init>(F)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760444
    :cond_d
    new-instance v1, LX/K0z;

    new-instance v2, LX/K0x;

    .line 2760445
    iget v4, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v4, v4

    .line 2760446
    invoke-direct {v2, v4}, LX/K0x;-><init>(I)V

    invoke-direct {v1, v3, v0, v2}, LX/K0z;-><init>(IILjava/lang/Object;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2760447
    :cond_e
    return-void
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2760396
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const-string v0, "00"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x31

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0x64

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public a(LX/5rl;)V
    .locals 9

    .prologue
    .line 2760397
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2760398
    :cond_0
    :goto_0
    return-void

    .line 2760399
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->a(LX/5rl;)V

    .line 2760400
    iget-object v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->v:Landroid/text/Spannable;

    if-eqz v0, :cond_0

    .line 2760401
    new-instance v0, LX/K10;

    iget-object v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->v:Landroid/text/Spannable;

    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->d:Z

    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v5

    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v6

    const/4 v7, 0x3

    invoke-virtual {p0, v7}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v7

    invoke-direct {p0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->J()I

    move-result v8

    invoke-direct/range {v0 .. v8}, LX/K10;-><init>(Landroid/text/Spannable;IZFFFFI)V

    .line 2760402
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 2760403
    invoke-virtual {p1, v1, v0}, LX/5rl;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2760404
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2760405
    invoke-super {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->f()V

    .line 2760406
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2760407
    invoke-super {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->h()V

    .line 2760408
    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 2760409
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2760410
    :goto_0
    return-void

    .line 2760411
    :cond_0
    invoke-static {p0}, Lcom/facebook/react/views/text/ReactTextShadowNode;->a(Lcom/facebook/react/views/text/ReactTextShadowNode;)Landroid/text/Spannable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->v:Landroid/text/Spannable;

    .line 2760412
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    goto :goto_0
.end method

.method public setBackgroundColor(Ljava/lang/Integer;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "backgroundColor"
    .end annotation

    .prologue
    .line 2760330
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2760331
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->j:Z

    .line 2760332
    iget-boolean v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->j:Z

    if-eqz v0, :cond_0

    .line 2760333
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->k:I

    .line 2760334
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760335
    :cond_1
    return-void

    .line 2760336
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setColor(Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "color"
    .end annotation

    .prologue
    .line 2760337
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->h:Z

    .line 2760338
    iget-boolean v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->h:Z

    if-eqz v0, :cond_0

    .line 2760339
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->i:I

    .line 2760340
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760341
    return-void

    .line 2760342
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFontFamily(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontFamily"
    .end annotation

    .prologue
    .line 2760283
    iput-object p1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->t:Ljava/lang/String;

    .line 2760284
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760285
    return-void
.end method

.method public setFontSize(F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = -1.0f
        name = "fontSize"
    .end annotation

    .prologue
    .line 2760252
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 2760253
    invoke-static {p1}, LX/5r2;->b(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float p1, v0

    .line 2760254
    :cond_0
    float-to-int v0, p1

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->b:I

    .line 2760255
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760256
    return-void
.end method

.method public setFontStyle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontStyle"
    .end annotation

    .prologue
    .line 2760257
    const/4 v0, -0x1

    .line 2760258
    const-string v1, "italic"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2760259
    const/4 v0, 0x2

    .line 2760260
    :cond_0
    :goto_0
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->r:I

    if-eq v0, v1, :cond_1

    .line 2760261
    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->r:I

    .line 2760262
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760263
    :cond_1
    return-void

    .line 2760264
    :cond_2
    const-string v1, "normal"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2760265
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFontWeight(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontWeight"
    .end annotation

    .prologue
    const/16 v3, 0x1f4

    const/4 v0, -0x1

    .line 2760266
    if-eqz p1, :cond_3

    invoke-static {p1}, Lcom/facebook/react/views/text/ReactTextShadowNode;->b(Ljava/lang/String;)I

    move-result v1

    .line 2760267
    :goto_0
    if-ge v1, v3, :cond_0

    const-string v2, "bold"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2760268
    :cond_0
    const/4 v0, 0x1

    .line 2760269
    :cond_1
    :goto_1
    iget v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->s:I

    if-eq v0, v1, :cond_2

    .line 2760270
    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->s:I

    .line 2760271
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760272
    :cond_2
    return-void

    :cond_3
    move v1, v0

    .line 2760273
    goto :goto_0

    .line 2760274
    :cond_4
    const-string v2, "normal"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    if-eq v1, v0, :cond_1

    if-ge v1, v3, :cond_1

    .line 2760275
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setLineHeight(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = -0x1
        name = "lineHeight"
    .end annotation

    .prologue
    .line 2760276
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/high16 v0, 0x7fc00000    # NaNf

    :goto_0
    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->g:F

    .line 2760277
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760278
    return-void

    .line 2760279
    :cond_0
    int-to-float v0, p1

    invoke-static {v0}, LX/5r2;->b(F)F

    move-result v0

    goto :goto_0
.end method

.method public setNumberOfLines(I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = -0x1
        name = "numberOfLines"
    .end annotation

    .prologue
    .line 2760280
    if-nez p1, :cond_0

    const/4 p1, -0x1

    :cond_0
    iput p1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->a:I

    .line 2760281
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760282
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "text"
    .end annotation

    .prologue
    .line 2760327
    iput-object p1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->u:Ljava/lang/String;

    .line 2760328
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760329
    return-void
.end method

.method public setTextAlign(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textAlign"
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 2760286
    if-eqz p1, :cond_0

    const-string v0, "auto"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2760287
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    .line 2760288
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760289
    return-void

    .line 2760290
    :cond_1
    const-string v0, "left"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2760291
    iput v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    goto :goto_0

    .line 2760292
    :cond_2
    const-string v0, "right"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2760293
    const/4 v0, 0x5

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    goto :goto_0

    .line 2760294
    :cond_3
    const-string v0, "center"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2760295
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    goto :goto_0

    .line 2760296
    :cond_4
    const-string v0, "justify"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2760297
    iput v1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->c:I

    goto :goto_0

    .line 2760298
    :cond_5
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid textAlign: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setTextDecorationLine(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textDecorationLine"
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2760299
    iput-boolean v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->p:Z

    .line 2760300
    iput-boolean v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->q:Z

    .line 2760301
    if-eqz p1, :cond_2

    .line 2760302
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2760303
    const-string v4, "underline"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2760304
    iput-boolean v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->p:Z

    .line 2760305
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2760306
    :cond_1
    const-string v4, "line-through"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2760307
    iput-boolean v5, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->q:Z

    goto :goto_1

    .line 2760308
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760309
    return-void
.end method

.method public setTextShadowColor(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x55000000
        customType = "Color"
        name = "textShadowColor"
    .end annotation

    .prologue
    .line 2760310
    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->o:I

    if-eq p1, v0, :cond_0

    .line 2760311
    iput p1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->o:I

    .line 2760312
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760313
    :cond_0
    return-void
.end method

.method public setTextShadowOffset(LX/5pG;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textShadowOffset"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2760314
    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->l:F

    .line 2760315
    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->m:F

    .line 2760316
    if-eqz p1, :cond_1

    .line 2760317
    const-string v0, "width"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "width"

    invoke-interface {p1, v0}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2760318
    const-string v0, "width"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->l:F

    .line 2760319
    :cond_0
    const-string v0, "height"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "height"

    invoke-interface {p1, v0}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2760320
    const-string v0, "height"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    iput v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->m:F

    .line 2760321
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760322
    return-void
.end method

.method public setTextShadowRadius(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x1
        name = "textShadowRadius"
    .end annotation

    .prologue
    .line 2760323
    iget v0, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->n:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 2760324
    iput p1, p0, Lcom/facebook/react/views/text/ReactTextShadowNode;->n:F

    .line 2760325
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2760326
    :cond_0
    return-void
.end method
