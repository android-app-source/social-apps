.class public Lcom/facebook/react/views/webview/ReactWebViewManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTWebView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "Landroid/webkit/WebView;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/K1T;

.field private b:Landroid/webkit/WebView$PictureListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2762258
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2762259
    new-instance v0, LX/K1U;

    invoke-direct {v0, p0}, LX/K1U;-><init>(Lcom/facebook/react/views/webview/ReactWebViewManager;)V

    iput-object v0, p0, Lcom/facebook/react/views/webview/ReactWebViewManager;->a:LX/K1T;

    .line 2762260
    return-void
.end method

.method private static a(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 2762319
    new-instance v0, LX/K1a;

    invoke-direct {v0}, LX/K1a;-><init>()V

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2762320
    return-void
.end method

.method private static a(Landroid/webkit/WebView;ILX/5pC;)V
    .locals 3
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2762321
    packed-switch p1, :pswitch_data_0

    .line 2762322
    :goto_0
    return-void

    .line 2762323
    :pswitch_0
    invoke-virtual {p0}, Landroid/webkit/WebView;->goBack()V

    goto :goto_0

    .line 2762324
    :pswitch_1
    invoke-virtual {p0}, Landroid/webkit/WebView;->goForward()V

    goto :goto_0

    .line 2762325
    :pswitch_2
    invoke-virtual {p0}, Landroid/webkit/WebView;->reload()V

    goto :goto_0

    .line 2762326
    :pswitch_3
    invoke-virtual {p0}, Landroid/webkit/WebView;->stopLoading()V

    goto :goto_0

    .line 2762327
    :pswitch_4
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2762328
    const-string v1, "data"

    const/4 v2, 0x0

    invoke-interface {p2, v2}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2762329
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:(document.dispatchEvent(new MessageEvent(\'message\', "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2762330
    :catch_0
    move-exception v0

    .line 2762331
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(LX/5rJ;)Landroid/webkit/WebView;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 2762332
    new-instance v0, LX/K1Z;

    invoke-direct {v0, p1}, LX/K1Z;-><init>(LX/5rJ;)V

    .line 2762333
    new-instance v1, LX/K1V;

    invoke-direct {v1, p0}, LX/K1V;-><init>(Lcom/facebook/react/views/webview/ReactWebViewManager;)V

    invoke-virtual {v0, v1}, LX/K1Z;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 2762334
    invoke-virtual {p1, v0}, LX/5pX;->a(LX/5pQ;)V

    .line 2762335
    invoke-virtual {v0}, LX/K1Z;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 2762336
    invoke-virtual {v0}, LX/K1Z;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 2762337
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/K1Z;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2762338
    sget-boolean v1, LX/0AN;->a:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 2762339
    invoke-static {v4}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    .line 2762340
    :cond_0
    return-object v0
.end method

.method private b(Landroid/webkit/WebView;)V
    .locals 2

    .prologue
    .line 2762341
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/SimpleViewManager;->a(Landroid/view/View;)V

    .line 2762342
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5rJ;

    move-object v1, p1

    check-cast v1, LX/K1Z;

    invoke-virtual {v0, v1}, LX/5pX;->b(LX/5pQ;)V

    .line 2762343
    check-cast p1, LX/K1Z;

    invoke-static {p1}, LX/K1Z;->f(LX/K1Z;)V

    .line 2762344
    return-void
.end method

.method public static b(Landroid/webkit/WebView;LX/5r0;)V
    .locals 2

    .prologue
    .line 2762345
    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2762346
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2762347
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2762348
    invoke-virtual {v0, p1}, LX/5s9;->a(LX/5r0;)V

    .line 2762349
    return-void
.end method

.method private t()Landroid/webkit/WebView$PictureListener;
    .locals 1

    .prologue
    .line 2762350
    iget-object v0, p0, Lcom/facebook/react/views/webview/ReactWebViewManager;->b:Landroid/webkit/WebView$PictureListener;

    if-nez v0, :cond_0

    .line 2762351
    new-instance v0, LX/K1W;

    invoke-direct {v0, p0}, LX/K1W;-><init>(Lcom/facebook/react/views/webview/ReactWebViewManager;)V

    iput-object v0, p0, Lcom/facebook/react/views/webview/ReactWebViewManager;->b:Landroid/webkit/WebView$PictureListener;

    .line 2762352
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/views/webview/ReactWebViewManager;->b:Landroid/webkit/WebView$PictureListener;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2762353
    invoke-direct {p0, p1}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(LX/5rJ;)Landroid/webkit/WebView;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2762354
    check-cast p2, Landroid/webkit/WebView;

    invoke-static {p2}, Lcom/facebook/react/views/webview/ReactWebViewManager;->a(Landroid/webkit/WebView;)V

    return-void
.end method

.method public final synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2762355
    check-cast p1, Landroid/webkit/WebView;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(Landroid/webkit/WebView;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2762318
    check-cast p1, Landroid/webkit/WebView;

    invoke-static {p1, p2, p3}, Lcom/facebook/react/views/webview/ReactWebViewManager;->a(Landroid/webkit/WebView;ILX/5pC;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2762356
    const-string v0, "RCTWebView"

    return-object v0
.end method

.method public setAllowUniversalAccessFromFileURLs(Landroid/webkit/WebView;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "allowUniversalAccessFromFileURLs"
    .end annotation

    .prologue
    .line 2762316
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 2762317
    return-void
.end method

.method public setDomStorageEnabled(Landroid/webkit/WebView;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "domStorageEnabled"
    .end annotation

    .prologue
    .line 2762314
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 2762315
    return-void
.end method

.method public setInjectedJavaScript(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "injectedJavaScript"
    .end annotation

    .prologue
    .line 2762311
    check-cast p1, LX/K1Z;

    .line 2762312
    iput-object p2, p1, LX/K1Z;->a:Ljava/lang/String;

    .line 2762313
    return-void
.end method

.method public setJavaScriptEnabled(Landroid/webkit/WebView;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "javaScriptEnabled"
    .end annotation

    .prologue
    .line 2762309
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 2762310
    return-void
.end method

.method public setMediaPlaybackRequiresUserAction(Landroid/webkit/WebView;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "mediaPlaybackRequiresUserAction"
    .end annotation

    .prologue
    .line 2762307
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 2762308
    return-void
.end method

.method public setMessagingEnabled(Landroid/webkit/WebView;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "messagingEnabled"
    .end annotation

    .prologue
    .line 2762305
    check-cast p1, LX/K1Z;

    invoke-virtual {p1, p2}, LX/K1Z;->setMessagingEnabled(Z)V

    .line 2762306
    return-void
.end method

.method public setOnContentSizeChange(Landroid/webkit/WebView;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "onContentSizeChange"
    .end annotation

    .prologue
    .line 2762301
    if-eqz p2, :cond_0

    .line 2762302
    invoke-direct {p0}, Lcom/facebook/react/views/webview/ReactWebViewManager;->t()Landroid/webkit/WebView$PictureListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    .line 2762303
    :goto_0
    return-void

    .line 2762304
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    goto :goto_0
.end method

.method public setScalesPageToFit(Landroid/webkit/WebView;Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "scalesPageToFit"
    .end annotation

    .prologue
    .line 2762298
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 2762299
    return-void

    .line 2762300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSource(Landroid/webkit/WebView;LX/5pG;)V
    .locals 7
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "source"
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2762265
    if-eqz p2, :cond_9

    .line 2762266
    const-string v0, "html"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2762267
    const-string v0, "html"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2762268
    const-string v0, "baseUrl"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2762269
    const-string v0, "baseUrl"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "text/html; charset=utf-8"

    const-string v4, "UTF-8"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2762270
    :cond_0
    :goto_0
    return-void

    .line 2762271
    :cond_1
    const-string v0, "text/html; charset=utf-8"

    const-string v1, "UTF-8"

    invoke-virtual {p1, v2, v0, v1}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2762272
    :cond_2
    const-string v0, "uri"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2762273
    const-string v0, "uri"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2762274
    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 2762275
    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2762276
    :cond_3
    const-string v0, "method"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2762277
    const-string v0, "method"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2762278
    const-string v2, "POST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2762279
    const-string v0, "body"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2762280
    const-string v0, "body"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2762281
    :try_start_0
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2762282
    :goto_1
    if-nez v0, :cond_4

    .line 2762283
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2762284
    :cond_4
    invoke-virtual {p1, v1, v0}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    goto :goto_0

    .line 2762285
    :catch_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_1

    .line 2762286
    :cond_5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2762287
    const-string v2, "headers"

    invoke-interface {p2, v2}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2762288
    const-string v2, "headers"

    invoke-interface {p2, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v2

    .line 2762289
    invoke-interface {v2}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v3

    .line 2762290
    :cond_6
    :goto_2
    invoke-interface {v3}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2762291
    invoke-interface {v3}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v4

    .line 2762292
    const-string v5, "user-agent"

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2762293
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 2762294
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-interface {v2, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    goto :goto_2

    .line 2762295
    :cond_7
    invoke-interface {v2, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2762296
    :cond_8
    invoke-virtual {p1, v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2762297
    :cond_9
    const-string v0, "about:blank"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    move-object v0, v5

    goto :goto_1
.end method

.method public setUserAgent(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "userAgent"
    .end annotation

    .prologue
    .line 2762262
    if-eqz p2, :cond_0

    .line 2762263
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 2762264
    :cond_0
    return-void
.end method

.method public final v()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2762261
    const-string v0, "goBack"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "goForward"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "reload"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "stopLoading"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "postMessage"

    const/4 v9, 0x5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
