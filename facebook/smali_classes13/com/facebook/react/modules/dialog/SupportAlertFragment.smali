.class public Lcom/facebook/react/modules/dialog/SupportAlertFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final j:LX/Jzo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2756838
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 2756839
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;->j:LX/Jzo;

    .line 2756840
    return-void
.end method

.method public constructor <init>(LX/Jzo;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # LX/Jzo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2756841
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 2756842
    iput-object p1, p0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;->j:LX/Jzo;

    .line 2756843
    invoke-virtual {p0, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2756844
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2756845
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2756846
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2756847
    invoke-static {v0, v1, p0}, LX/Jzn;->a(Landroid/content/Context;Landroid/os/Bundle;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 2756848
    iget-object v0, p0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;->j:LX/Jzo;

    if-eqz v0, :cond_0

    .line 2756849
    iget-object v0, p0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;->j:LX/Jzo;

    invoke-virtual {v0, p1, p2}, LX/Jzo;->onClick(Landroid/content/DialogInterface;I)V

    .line 2756850
    :cond_0
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2756851
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2756852
    iget-object v0, p0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;->j:LX/Jzo;

    if-eqz v0, :cond_0

    .line 2756853
    iget-object v0, p0, Lcom/facebook/react/modules/dialog/SupportAlertFragment;->j:LX/Jzo;

    invoke-virtual {v0, p1}, LX/Jzo;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2756854
    :cond_0
    return-void
.end method
