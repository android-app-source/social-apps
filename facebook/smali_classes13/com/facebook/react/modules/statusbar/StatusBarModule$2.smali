.class public final Lcom/facebook/react/modules/statusbar/StatusBarModule$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Z

.field public final synthetic c:LX/K08;


# direct methods
.method public constructor <init>(LX/K08;Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 2757316
    iput-object p1, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;->c:LX/K08;

    iput-object p2, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;->a:Landroid/app/Activity;

    iput-boolean p3, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 2757317
    iget-object v0, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 2757318
    iget-boolean v1, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$2;->b:Z

    if-eqz v1, :cond_0

    .line 2757319
    new-instance v1, LX/K07;

    invoke-direct {v1, p0}, LX/K07;-><init>(Lcom/facebook/react/modules/statusbar/StatusBarModule$2;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 2757320
    :goto_0
    invoke-static {v0}, LX/0vv;->z(Landroid/view/View;)V

    .line 2757321
    return-void

    .line 2757322
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    goto :goto_0
.end method
