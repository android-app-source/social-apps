.class public final Lcom/facebook/react/modules/statusbar/StatusBarModule$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/K08;


# direct methods
.method public constructor <init>(LX/K08;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2757330
    iput-object p1, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;->c:LX/K08;

    iput-object p2, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;->a:Landroid/app/Activity;

    iput-object p3, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 2757331
    iget-object v0, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 2757332
    iget-object v0, p0, Lcom/facebook/react/modules/statusbar/StatusBarModule$4;->b:Ljava/lang/String;

    const-string v2, "dark-content"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2000

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 2757333
    return-void

    .line 2757334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
