.class public final Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Jzy;


# direct methods
.method public constructor <init>(LX/Jzy;)V
    .locals 0

    .prologue
    .line 2757042
    iput-object p1, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2757043
    iget-object v1, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    monitor-enter v1

    .line 2757044
    :try_start_0
    iget-object v0, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    iget-boolean v0, v0, LX/Jzy;->i:Z

    if-nez v0, :cond_0

    .line 2757045
    iget-object v0, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    iget-object v0, v0, LX/Jzy;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget v4, LX/K00;->c:I

    const-string v5, "Location request timed out"

    invoke-static {v4, v5}, LX/K00;->a(ILjava/lang/String;)LX/5pH;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2757046
    iget-object v0, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    iget-object v0, v0, LX/Jzy;->c:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    iget-object v2, v2, LX/Jzy;->h:Landroid/location/LocationListener;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 2757047
    iget-object v0, p0, Lcom/facebook/react/modules/location/LocationModule$SingleUpdateRequest$1;->a:LX/Jzy;

    const/4 v2, 0x1

    .line 2757048
    iput-boolean v2, v0, LX/Jzy;->i:Z

    .line 2757049
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
