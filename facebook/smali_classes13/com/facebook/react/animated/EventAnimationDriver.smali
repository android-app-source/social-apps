.class public Lcom/facebook/react/animated/EventAnimationDriver;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/events/RCTEventEmitter;


# instance fields
.field private mEventPath:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mValueNode:LX/Jyx;


# direct methods
.method public constructor <init>(Ljava/util/List;LX/Jyx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/Jyx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2755703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2755704
    iput-object p1, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mEventPath:Ljava/util/List;

    .line 2755705
    iput-object p2, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mValueNode:LX/Jyx;

    .line 2755706
    return-void
.end method


# virtual methods
.method public receiveEvent(ILjava/lang/String;LX/5pH;)V
    .locals 4
    .param p3    # LX/5pH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2755695
    if-nez p3, :cond_0

    .line 2755696
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Native animated events must have event data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755697
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mEventPath:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    .line 2755698
    iget-object v0, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mEventPath:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p3, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object p3

    .line 2755699
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2755700
    :cond_1
    iget-object v1, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mValueNode:LX/Jyx;

    iget-object v0, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mEventPath:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/react/animated/EventAnimationDriver;->mEventPath:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p3, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, v1, LX/Jyx;->e:D

    .line 2755701
    return-void
.end method

.method public receiveTouches(Ljava/lang/String;LX/5pD;LX/5pD;)V
    .locals 2

    .prologue
    .line 2755702
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "receiveTouches is not support by native animated events"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
