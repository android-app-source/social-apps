.class public Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/22r;

.field private final b:Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

.field private final c:Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;


# direct methods
.method public constructor <init>(LX/22r;Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709704
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2709705
    iput-object p1, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->a:LX/22r;

    .line 2709706
    iput-object p2, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->b:Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

    .line 2709707
    iput-object p3, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->c:Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;

    .line 2709708
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;
    .locals 6

    .prologue
    .line 2709709
    const-class v1, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;

    monitor-enter v1

    .line 2709710
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709711
    sput-object v2, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709712
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709713
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709714
    new-instance p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;

    invoke-static {v0}, LX/22r;->b(LX/0QB;)LX/22r;

    move-result-object v3

    check-cast v3, LX/22r;

    invoke-static {v0}, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->a(LX/0QB;)Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->a(LX/0QB;)Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;-><init>(LX/22r;Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;)V

    .line 2709715
    move-object v0, p0

    .line 2709716
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709717
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709718
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709719
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2709720
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    .line 2709721
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const v3, 0x7f020a54

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f020a56

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f020a58

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 2709722
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2709723
    iget-object v4, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->b:Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

    new-instance v5, LX/JaI;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v5, v0, p2}, LX/JaI;-><init>(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, v4, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2709724
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2709725
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;->c:Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;

    invoke-virtual {p1, v0, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2709726
    return-object v6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2709727
    const/4 v0, 0x1

    move v0, v0

    .line 2709728
    return v0
.end method
