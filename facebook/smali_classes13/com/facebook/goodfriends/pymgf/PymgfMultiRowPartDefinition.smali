.class public Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0oJ;


# direct methods
.method public constructor <init>(LX/0oJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709734
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2709735
    iput-object p1, p0, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;->a:LX/0oJ;

    .line 2709736
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;
    .locals 4

    .prologue
    .line 2709737
    const-class v1, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;

    monitor-enter v1

    .line 2709738
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709739
    sput-object v2, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709740
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709741
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709742
    new-instance p0, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;

    invoke-static {v0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v3

    check-cast v3, LX/0oJ;

    invoke-direct {p0, v3}, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;-><init>(LX/0oJ;)V

    .line 2709743
    move-object v0, p0

    .line 2709744
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709745
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709746
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709747
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2709748
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2709749
    iget-object v0, p0, Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;->a:LX/0oJ;

    .line 2709750
    sget-short v1, LX/0ob;->H:S

    sget-short p0, LX/0ob;->v:S

    const/4 p1, 0x0

    invoke-static {v0, v1, p0, p1}, LX/0oJ;->a(LX/0oJ;SSZ)Z

    move-result v1

    move v0, v1

    .line 2709751
    return v0
.end method
