.class public Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0YZ;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public p:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9Tl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0Yb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2716859
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2716856
    iget-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->p:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "action_background_contactpoint_confirmed"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->r:LX/0Yb;

    .line 2716857
    iget-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2716858
    return-void
.end method

.method private static a(Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;LX/0Xl;LX/9Tl;)V
    .locals 0

    .prologue
    .line 2716855
    iput-object p1, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->p:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->q:LX/9Tl;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;

    invoke-static {v1}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-static {v1}, LX/9Tl;->b(LX/0QB;)LX/9Tl;

    move-result-object v1

    check-cast v1, LX/9Tl;

    invoke-static {p0, v0, v1}, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->a(Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;LX/0Xl;LX/9Tl;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2716851
    iget-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->r:LX/0Yb;

    if-eqz v0, :cond_0

    .line 2716852
    iget-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2716853
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->r:LX/0Yb;

    .line 2716854
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2716817
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2716818
    invoke-static {p0, p0}, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2716819
    const v0, 0x7f03009e

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->setContentView(I)V

    .line 2716820
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2716821
    const-string v0, "/phoneacquire?source=%s&state=%s"

    const-string v1, "phone_acquisition_embedded"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v3}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Ljava/lang/String;ZZ)Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    move-result-object v0

    .line 2716822
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2716823
    invoke-direct {p0}, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->a()V

    .line 2716824
    iget-object v0, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->q:LX/9Tl;

    .line 2716825
    iget-object v1, v0, LX/9Tl;->a:LX/0Zb;

    sget-object v2, LX/9Tm;->FACEWEB_ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

    invoke-virtual {v2}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2716826
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2716827
    const-string v2, "growth"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2716828
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2716829
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x23a2e2ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2716848
    invoke-direct {p0}, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->b()V

    .line 2716849
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2716850
    const/16 v1, 0x23

    const v2, 0x1e6e56ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x264c822f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2716830
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2716831
    if-eqz v0, :cond_0

    .line 2716832
    iget-object v2, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    move-object v2, v2

    .line 2716833
    if-nez v2, :cond_1

    .line 2716834
    :cond_0
    const/16 v0, 0x27

    const v2, 0x300e85f2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2716835
    :goto_0
    return-void

    .line 2716836
    :cond_1
    iget-object v2, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    move-object v0, v2

    .line 2716837
    const-string v2, "/phoneacquire?source=%s&state=%s"

    const-string v3, "phone_acquisition_embedded"

    const-string v4, "3"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/GxF;->a(Ljava/lang/String;Z)V

    .line 2716838
    const-string v0, "extra_background_confirmed_contactpoint"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    .line 2716839
    iget-object v2, p0, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->q:LX/9Tl;

    if-nez v0, :cond_3

    const-string v0, ""

    .line 2716840
    :goto_1
    iget-object v3, v2, LX/9Tl;->a:LX/0Zb;

    sget-object v4, LX/9Tm;->FACEWEB_ADD_CONTACTPOINT_CONFIRMED:LX/9Tm;

    invoke-virtual {v4}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x1

    invoke-interface {v3, v4, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2716841
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2716842
    const-string v4, "growth"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2716843
    const-string v4, "phone_number_added"

    invoke-virtual {v3, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2716844
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2716845
    :cond_2
    invoke-direct {p0}, Lcom/facebook/katana/activity/contactpoints/AddContactpointFacewebActivity;->b()V

    .line 2716846
    const v0, -0x6215514b

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 2716847
    :cond_3
    iget-object v0, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    goto :goto_1
.end method
