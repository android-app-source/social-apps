.class public final Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x644a9880
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2717122
    const-class v0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2717121
    const-class v0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2717119
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2717120
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2717116
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2717117
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2717118
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2717089
    iput-object p1, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2717090
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2717091
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2717092
    if-eqz v0, :cond_0

    .line 2717093
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2717094
    :cond_0
    return-void

    .line 2717095
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2717114
    iget-object v0, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2717115
    iget-object v0, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2717112
    iget-object v0, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->f:Ljava/lang/String;

    .line 2717113
    iget-object v0, p0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2717104
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2717105
    invoke-direct {p0}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2717106
    invoke-direct {p0}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2717107
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2717108
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2717109
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2717110
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2717111
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2717123
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2717124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2717125
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2717103
    new-instance v0, LX/Jbl;

    invoke-direct {v0, p1}, LX/Jbl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2717102
    invoke-direct {p0}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2717096
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717097
    invoke-direct {p0}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2717098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2717099
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2717100
    :goto_0
    return-void

    .line 2717101
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2717086
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717087
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2717088
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2717083
    new-instance v0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;-><init>()V

    .line 2717084
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2717085
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2717082
    const v0, -0x7457b49b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2717081
    const v0, 0x285feb

    return v0
.end method
