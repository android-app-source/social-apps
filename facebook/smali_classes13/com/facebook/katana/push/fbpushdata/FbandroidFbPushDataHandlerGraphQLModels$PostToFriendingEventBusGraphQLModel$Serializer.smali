.class public final Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2717064
    const-class v0, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;

    new-instance v1, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2717065
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2717066
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2717067
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2717068
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 2717069
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2717070
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 2717071
    if-eqz p0, :cond_0

    .line 2717072
    const-string p0, "friendship_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717073
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2717074
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2717075
    if-eqz p0, :cond_1

    .line 2717076
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2717077
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2717078
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2717079
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2717080
    check-cast p1, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel$Serializer;->a(Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
