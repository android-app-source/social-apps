.class public Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1Kf;

.field public final c:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

.field public final d:LX/8TY;

.field public final e:LX/1HI;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field private final g:LX/14x;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ARK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2716914
    const-class v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method private constructor <init>(LX/1Kf;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/8TY;LX/1HI;Lcom/facebook/content/SecureContextHelper;LX/14x;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Kf;",
            "Lcom/facebook/common/shortcuts/InstallShortcutHelper;",
            "LX/8TY;",
            "LX/1HI;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/14x;",
            "LX/0Ot",
            "<",
            "LX/ARK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2717010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717011
    iput-object p1, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->b:LX/1Kf;

    .line 2717012
    iput-object p2, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->c:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    .line 2717013
    iput-object p3, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->d:LX/8TY;

    .line 2717014
    iput-object p4, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->e:LX/1HI;

    .line 2717015
    iput-object p5, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2717016
    iput-object p6, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->g:LX/14x;

    .line 2717017
    iput-object p7, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->h:LX/0Ot;

    .line 2717018
    return-void
.end method

.method private a(Landroid/app/Activity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;I)LX/8Tp;
    .locals 2

    .prologue
    .line 2717007
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2717008
    iget-object v1, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->b:LX/1Kf;

    invoke-interface {v1, v0, p2, p3, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2717009
    sget-object v0, LX/8Tp;->ACTIVITY_STARTED:LX/8Tp;

    return-object v0
.end method

.method public static a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2717006
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/3RH;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "share_return_to_fb4a"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_game_share"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "game_share_data"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2716999
    if-nez p0, :cond_1

    .line 2717000
    const-string v0, "QuicksilverFB4ADataProvider"

    const-string v2, "No extraData provided."

    invoke-static {v0, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2717001
    :cond_0
    :goto_0
    return-object v0

    .line 2717002
    :cond_1
    const-string v0, "score_screenshot_handle"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2717003
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2717004
    const-string v0, "QuicksilverFB4ADataProvider"

    const-string v2, "No screenshot handle provided."

    invoke-static {v0, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2717005
    goto :goto_0
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 2716996
    iget-object v0, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->d:LX/8TY;

    .line 2716997
    iget-object v1, v0, LX/8TY;->a:LX/0Uh;

    const/16 v2, 0xe3

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2716998
    return v0
.end method

.method public static a(Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2717019
    iget-object v0, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->g:LX/14x;

    invoke-virtual {v0}, LX/14x;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->g:LX/14x;

    invoke-virtual {v0, p1}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
    .locals 8

    .prologue
    .line 2716994
    new-instance v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v2

    check-cast v2, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-static {p0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v3

    check-cast v3, LX/8TY;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v6

    check-cast v6, LX/14x;

    const/16 v7, 0x19d1

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;-><init>(LX/1Kf;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/8TY;LX/1HI;Lcom/facebook/content/SecureContextHelper;LX/14x;LX/0Ot;)V

    .line 2716995
    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/app/Activity;Ljava/util/Map;)LX/8Tp;
    .locals 6
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/8Tp;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2716933
    move-object v5, p2

    check-cast v5, Lcom/facebook/quicksilver/QuicksilverActivity;

    .line 2716934
    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->l()Ljava/lang/String;

    move-result-object v1

    .line 2716935
    const v0, 0x7f083abf

    if-ne p1, v0, :cond_0

    .line 2716936
    iget-object v0, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARK;

    new-instance v2, Lcom/facebook/quicksilver/common/sharing/GameEntityShareExtras;

    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->m()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/facebook/quicksilver/common/sharing/GameEntityShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/ARK;->a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2716937
    const/16 v1, 0x22b3

    invoke-direct {p0, p2, v0, v1}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Landroid/app/Activity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;I)LX/8Tp;

    move-result-object v0

    .line 2716938
    :goto_0
    return-object v0

    .line 2716939
    :cond_0
    const v0, 0x7f083ac1

    if-ne p1, v0, :cond_1

    .line 2716940
    const-string v0, "score"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2716941
    invoke-static {p3}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    .line 2716942
    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->m()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)LX/8Tp;

    move-result-object v0

    goto :goto_0

    .line 2716943
    :cond_1
    const v0, 0x7f083ac3

    if-ne p1, v0, :cond_3

    .line 2716944
    instance-of v0, p2, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2716945
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-direct {v1, p2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object v0, p2

    .line 2716946
    check-cast v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    .line 2716947
    const-string v2, "app_id"

    invoke-virtual {v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2716948
    const-string v2, "source"

    sget-object v3, LX/8TZ;->FB_HOMESCREEN_SHORTCUT:LX/8TZ;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2716949
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2716950
    invoke-virtual {v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->m()Ljava/lang/String;

    move-result-object v2

    .line 2716951
    invoke-virtual {v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->n()Ljava/lang/String;

    move-result-object v0

    .line 2716952
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020737

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2716953
    if-eqz v2, :cond_2

    if-nez v0, :cond_8

    .line 2716954
    :cond_2
    const-string v0, "QuicksilverFB4ADataProvider"

    const-string v1, "Either shortcut name is null or icon url"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2716955
    sget-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    .line 2716956
    :goto_1
    move-object v0, v0

    .line 2716957
    goto :goto_0

    .line 2716958
    :cond_3
    const v0, 0x7f083ac0

    if-ne p1, v0, :cond_4

    .line 2716959
    const-string v0, "90.0"

    invoke-static {p0, v0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2716960
    sget-object v0, LX/8Tp;->MESSENGER_UNSUPPORTED:LX/8Tp;

    .line 2716961
    :goto_2
    move-object v0, v0

    .line 2716962
    goto :goto_0

    .line 2716963
    :cond_4
    const v0, 0x7f083ac2

    if-ne p1, v0, :cond_7

    .line 2716964
    if-eqz p3, :cond_5

    move v0, v2

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2716965
    invoke-static {p3}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 2716966
    const-string v0, "score"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2716967
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    :goto_4
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2716968
    const-string v2, "90.0"

    invoke-static {p0, v2}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2716969
    sget-object v2, LX/8Tp;->MESSENGER_UNSUPPORTED:LX/8Tp;

    .line 2716970
    :goto_5
    move-object v0, v2

    .line 2716971
    goto/16 :goto_0

    :cond_5
    move v0, v3

    .line 2716972
    goto :goto_3

    :cond_6
    move v2, v3

    .line 2716973
    goto :goto_4

    .line 2716974
    :cond_7
    sget-object v0, LX/8Tp;->UNHANDLED_EVENT:LX/8Tp;

    goto/16 :goto_0

    .line 2716975
    :cond_8
    :try_start_0
    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2716976
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 2716977
    if-nez v0, :cond_9

    .line 2716978
    const-string v0, "QuicksilverFB4ADataProvider"

    const-string v1, "Could not generate ImageRequest from URI"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2716979
    sget-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    goto :goto_1

    .line 2716980
    :catch_0
    move-exception v0

    .line 2716981
    const-string v1, "QuicksilverFB4ADataProvider"

    const-string v2, "Icon URL encoding unsupported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2716982
    sget-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    goto :goto_1

    .line 2716983
    :cond_9
    iget-object v4, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->e:LX/1HI;

    sget-object v5, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v0, v5}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2716984
    new-instance v4, LX/Jbf;

    invoke-direct {v4, p0, v1, v2, v3}, LX/Jbf;-><init>(Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 2716985
    sget-object v1, LX/1fo;->a:LX/1fo;

    move-object v1, v1

    .line 2716986
    invoke-interface {v0, v4, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2716987
    sget-object v0, LX/8Tp;->SHORTCUT_CREATED:LX/8Tp;

    goto/16 :goto_1

    .line 2716988
    :cond_a
    new-instance v0, Lcom/facebook/quicksilver/common/sharing/GameEntityShareExtras;

    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->m()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/quicksilver/common/sharing/GameEntityShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2716989
    iget-object v1, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0x22b5

    invoke-interface {v1, v0, v2, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2716990
    sget-object v0, LX/8Tp;->ACTIVITY_STARTED:LX/8Tp;

    goto/16 :goto_2

    .line 2716991
    :cond_b
    new-instance v2, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;

    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->m()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2716992
    iget-object v3, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Landroid/content/Intent;

    move-result-object v2

    const/16 v4, 0x22b6

    invoke-interface {v3, v2, v4, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2716993
    sget-object v2, LX/8Tp;->ACTIVITY_STARTED:LX/8Tp;

    goto :goto_5
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)LX/8Tp;
    .locals 2

    .prologue
    .line 2716929
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2716930
    iget-object v0, p0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARK;

    new-instance v1, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/ARK;->a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2716931
    const/16 v1, 0x22b4

    invoke-direct {p0, p5, v0, v1}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Landroid/app/Activity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;I)LX/8Tp;

    move-result-object v0

    return-object v0

    .line 2716932
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8To;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2716922
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2716923
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083abf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2716924
    new-instance v2, LX/8To;

    const v3, 0x7f083abf

    const v4, 0x7f02080f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, LX/8To;-><init>(ILjava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2716925
    invoke-direct {p0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2716926
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083ac0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2716927
    new-instance v2, LX/8To;

    const v3, 0x7f083ac0

    const v4, 0x7f020740

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, LX/8To;-><init>(ILjava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2716928
    :cond_0
    return-object v0
.end method

.method public final c(Landroid/app/Activity;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8To;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2716915
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2716916
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083ac1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2716917
    new-instance v2, LX/8To;

    const v3, 0x7f083ac1

    const v4, 0x7f02080f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, LX/8To;-><init>(ILjava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2716918
    invoke-direct {p0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2716919
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083ac2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2716920
    new-instance v2, LX/8To;

    const v3, 0x7f083ac2

    const v4, 0x7f020740

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, LX/8To;-><init>(ILjava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2716921
    :cond_0
    return-object v0
.end method
