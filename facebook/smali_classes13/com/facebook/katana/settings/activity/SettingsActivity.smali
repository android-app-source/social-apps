.class public Lcom/facebook/katana/settings/activity/SettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/10X;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static a:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private A:LX/BSM;

.field private B:LX/0iY;

.field public C:LX/0iX;

.field public D:LX/0iZ;

.field private E:LX/0s5;

.field private F:LX/2PY;

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4oo;",
            ">;"
        }
    .end annotation
.end field

.field private H:Landroid/preference/PreferenceScreen;

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private c:LX/0ad;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:LX/03R;

.field private h:LX/03R;

.field private i:Z

.field public j:LX/0Zb;

.field private k:LX/2TI;

.field private l:LX/2TK;

.field private m:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private n:LX/F6t;

.field private o:LX/D3a;

.field private p:LX/FK8;

.field public q:Lcom/facebook/content/SecureContextHelper;

.field public r:LX/CIm;

.field private s:LX/1mD;

.field private t:LX/1Bv;

.field private u:LX/1sd;

.field private v:LX/0TD;

.field private w:Ljava/util/concurrent/ExecutorService;

.field private x:LX/A6h;

.field public y:LX/A6X;

.field public z:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2717565
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2717566
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 2717567
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->G:Ljava/util/List;

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2717568
    const-string v0, "MOBILE_RADIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717569
    const v0, 0x7f0811aa

    .line 2717570
    :goto_0
    return v0

    .line 2717571
    :cond_0
    const-string v0, "WIFI_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2717572
    const v0, 0x7f0811ab

    goto :goto_0

    .line 2717573
    :cond_1
    const v0, 0x7f0811ad

    goto :goto_0
.end method

.method private a(Landroid/preference/Preference;)Landroid/preference/Preference;
    .locals 3

    .prologue
    .line 2717518
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->l:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2717519
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2717520
    const-string v1, "tab_to_show"

    const-string v2, "sync"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2717521
    const-string v1, "nux_ref"

    const-string v2, "pref"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2717522
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->k:LX/2TI;

    invoke-virtual {v1}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v1

    .line 2717523
    invoke-static {v1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 2717524
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2717525
    :goto_0
    return-object p1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private a(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717574
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->p:LX/FK8;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717575
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->p:LX/FK8;

    new-instance v1, LX/Jbz;

    invoke-direct {v1, p0}, LX/Jbz;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, LX/FK8;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717576
    return-void
.end method

.method private a(Landroid/preference/PreferenceGroup;)V
    .locals 1

    .prologue
    .line 2717577
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->g(Landroid/preference/PreferenceGroup;)V

    .line 2717578
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->c(Landroid/preference/PreferenceGroup;)V

    .line 2717579
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->d(Landroid/preference/PreferenceGroup;)V

    .line 2717580
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->e(Landroid/preference/PreferenceGroup;)V

    .line 2717581
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->f(Landroid/preference/PreferenceGroup;)V

    .line 2717582
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    invoke-virtual {v0, p1}, LX/CIm;->a(Landroid/preference/PreferenceGroup;)V

    .line 2717583
    return-void
.end method

.method private a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2TI;LX/2TK;LX/0Zb;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/Boolean;LX/03R;LX/03R;LX/0s5;LX/F6t;LX/D3a;LX/FK8;Lcom/facebook/content/SecureContextHelper;LX/CIm;LX/1mD;LX/1Bv;LX/0ad;LX/1sd;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/A6h;LX/A6X;LX/03V;LX/03R;LX/03R;LX/03R;LX/2PY;LX/BSM;LX/0iY;LX/0iX;LX/0iZ;)V
    .locals 2
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/growth/annotations/IsPhoneNumberAcquisitionEnabled;
        .end annotation
    .end param
    .param p7    # LX/03R;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadBackgroundTaskEnabled;
        .end annotation
    .end param
    .param p8    # LX/03R;
        .annotation runtime Lcom/facebook/video/abtest/VideoInline;
        .end annotation
    .end param
    .param p19    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p20    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p24    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserTrustedTester;
        .end annotation
    .end param
    .param p25    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p26    # LX/03R;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsInAppBrowserEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717584
    iput-object p1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2717585
    iput-object p2, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->k:LX/2TI;

    .line 2717586
    iput-object p3, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->l:LX/2TK;

    .line 2717587
    iput-object p4, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->j:LX/0Zb;

    .line 2717588
    iput-object p5, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2717589
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->d:Z

    .line 2717590
    const/4 v1, 0x0

    invoke-virtual {p7, v1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->e:Z

    .line 2717591
    const/4 v1, 0x1

    invoke-virtual {p8, v1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->f:Z

    .line 2717592
    iput-object p9, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->E:LX/0s5;

    .line 2717593
    iput-object p10, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->n:LX/F6t;

    .line 2717594
    iput-object p11, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->o:LX/D3a;

    .line 2717595
    iput-object p12, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->p:LX/FK8;

    .line 2717596
    iput-object p13, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->q:Lcom/facebook/content/SecureContextHelper;

    .line 2717597
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    .line 2717598
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->s:LX/1mD;

    .line 2717599
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->t:LX/1Bv;

    .line 2717600
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->c:LX/0ad;

    .line 2717601
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->u:LX/1sd;

    .line 2717602
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->v:LX/0TD;

    .line 2717603
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->w:Ljava/util/concurrent/ExecutorService;

    .line 2717604
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->x:LX/A6h;

    .line 2717605
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->y:LX/A6X;

    .line 2717606
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->z:LX/03V;

    .line 2717607
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->g:LX/03R;

    .line 2717608
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->h:LX/03R;

    .line 2717609
    const/4 v1, 0x0

    move-object/from16 v0, p26

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->i:Z

    .line 2717610
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->F:LX/2PY;

    .line 2717611
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->A:LX/BSM;

    .line 2717612
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->B:LX/0iY;

    .line 2717613
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->C:LX/0iX;

    .line 2717614
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->D:LX/0iZ;

    .line 2717615
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 32

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v31

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/katana/settings/activity/SettingsActivity;

    invoke-static/range {v31 .. v31}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v31 .. v31}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v2

    check-cast v2, LX/2TI;

    invoke-static/range {v31 .. v31}, LX/2TK;->a(LX/0QB;)LX/2TK;

    move-result-object v3

    check-cast v3, LX/2TK;

    invoke-static/range {v31 .. v31}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static/range {v31 .. v31}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v31 .. v31}, LX/F6T;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-static/range {v31 .. v31}, LX/Jbd;->a(LX/0QB;)LX/03R;

    move-result-object v7

    check-cast v7, LX/03R;

    invoke-static/range {v31 .. v31}, LX/33t;->a(LX/0QB;)LX/03R;

    move-result-object v8

    check-cast v8, LX/03R;

    invoke-static/range {v31 .. v31}, LX/0s5;->a(LX/0QB;)LX/0s5;

    move-result-object v9

    check-cast v9, LX/0s5;

    invoke-static/range {v31 .. v31}, LX/F6t;->a(LX/0QB;)LX/F6t;

    move-result-object v10

    check-cast v10, LX/F6t;

    invoke-static/range {v31 .. v31}, LX/D3a;->a(LX/0QB;)LX/D3a;

    move-result-object v11

    check-cast v11, LX/D3a;

    invoke-static/range {v31 .. v31}, LX/FK8;->a(LX/0QB;)LX/FK8;

    move-result-object v12

    check-cast v12, LX/FK8;

    invoke-static/range {v31 .. v31}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v13

    check-cast v13, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v31 .. v31}, LX/CIm;->a(LX/0QB;)LX/CIm;

    move-result-object v14

    check-cast v14, LX/CIm;

    invoke-static/range {v31 .. v31}, LX/1mD;->a(LX/0QB;)LX/1mD;

    move-result-object v15

    check-cast v15, LX/1mD;

    invoke-static/range {v31 .. v31}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v16

    check-cast v16, LX/1Bv;

    invoke-static/range {v31 .. v31}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-static/range {v31 .. v31}, LX/1sb;->a(LX/0QB;)LX/1sd;

    move-result-object v18

    check-cast v18, LX/1sd;

    invoke-static/range {v31 .. v31}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v19

    check-cast v19, LX/0TD;

    invoke-static/range {v31 .. v31}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v20

    check-cast v20, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v31 .. v31}, LX/A6h;->a(LX/0QB;)LX/A6h;

    move-result-object v21

    check-cast v21, LX/A6h;

    invoke-static/range {v31 .. v31}, LX/A6X;->a(LX/0QB;)LX/A6X;

    move-result-object v22

    check-cast v22, LX/A6X;

    invoke-static/range {v31 .. v31}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v23

    check-cast v23, LX/03V;

    invoke-static/range {v31 .. v31}, LX/0xy;->a(LX/0QB;)LX/03R;

    move-result-object v24

    check-cast v24, LX/03R;

    invoke-static/range {v31 .. v31}, LX/0XD;->a(LX/0QB;)LX/03R;

    move-result-object v25

    check-cast v25, LX/03R;

    invoke-static/range {v31 .. v31}, LX/2Ha;->a(LX/0QB;)LX/03R;

    move-result-object v26

    check-cast v26, LX/03R;

    invoke-static/range {v31 .. v31}, LX/2PY;->a(LX/0QB;)LX/2PY;

    move-result-object v27

    check-cast v27, LX/2PY;

    invoke-static/range {v31 .. v31}, LX/BSM;->a(LX/0QB;)LX/BSM;

    move-result-object v28

    check-cast v28, LX/BSM;

    invoke-static/range {v31 .. v31}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v29

    check-cast v29, LX/0iY;

    invoke-static/range {v31 .. v31}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v30

    check-cast v30, LX/0iX;

    invoke-static/range {v31 .. v31}, LX/0iZ;->a(LX/0QB;)LX/0iZ;

    move-result-object v31

    check-cast v31, LX/0iZ;

    invoke-direct/range {v0 .. v31}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2TI;LX/2TK;LX/0Zb;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/Boolean;LX/03R;LX/03R;LX/0s5;LX/F6t;LX/D3a;LX/FK8;Lcom/facebook/content/SecureContextHelper;LX/CIm;LX/1mD;LX/1Bv;LX/0ad;LX/1sd;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/A6h;LX/A6X;LX/03V;LX/03R;LX/03R;LX/03R;LX/2PY;LX/BSM;LX/0iY;LX/0iX;LX/0iZ;)V

    return-void
.end method

.method private b(Landroid/preference/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 2717616
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    sget-object v1, LX/1rO;->b:LX/0Tn;

    const v2, 0x7f083aeb

    const/4 v3, 0x1

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    .line 2717617
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717618
    new-instance v1, LX/Jc0;

    invoke-direct {v1, p0}, LX/Jc0;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717619
    return-void
.end method

.method private b(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2717677
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717678
    const v1, 0x7f0309f6

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2717679
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2717680
    return-void
.end method

.method private c(Landroid/preference/PreferenceCategory;)V
    .locals 5

    .prologue
    .line 2717620
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2717621
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->c:LX/0ad;

    sget-char v2, LX/1rJ;->G:C

    const v3, 0x7f083aec

    invoke-virtual {p0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2717622
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->c:LX/0ad;

    sget-char v2, LX/1rJ;->F:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOn(Ljava/lang/CharSequence;)V

    .line 2717623
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->c:LX/0ad;

    sget-char v2, LX/1rJ;->E:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOff(Ljava/lang/CharSequence;)V

    .line 2717624
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->B:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2717625
    sget-object v1, LX/1rO;->c:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2717626
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717627
    new-instance v1, LX/Jc1;

    invoke-direct {v1, p0}, LX/Jc1;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717628
    return-void
.end method

.method private c(Landroid/preference/PreferenceGroup;)V
    .locals 4

    .prologue
    .line 2717629
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2717630
    const v1, 0x7f083ad7

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2717631
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2717632
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->n(Landroid/preference/PreferenceCategory;)V

    .line 2717633
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->B:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->c:LX/0ad;

    sget-short v2, LX/1rJ;->g:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2717634
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->c(Landroid/preference/PreferenceCategory;)V

    .line 2717635
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->b(Landroid/preference/PreferenceCategory;)V

    .line 2717636
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->d(Landroid/preference/PreferenceCategory;)V

    .line 2717637
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->f(Landroid/preference/PreferenceCategory;)V

    .line 2717638
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->g(Landroid/preference/PreferenceCategory;)V

    .line 2717639
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->h(Landroid/preference/PreferenceCategory;)V

    .line 2717640
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->l(Landroid/preference/PreferenceCategory;)V

    .line 2717641
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->m(Landroid/preference/PreferenceCategory;)V

    .line 2717642
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->i(Landroid/preference/PreferenceCategory;)V

    .line 2717643
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->k(Landroid/preference/PreferenceCategory;)V

    .line 2717644
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->j(Landroid/preference/PreferenceCategory;)V

    .line 2717645
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    .line 2717646
    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2717647
    if-nez p1, :cond_1

    .line 2717648
    :cond_0
    return-void

    .line 2717649
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4oo;

    .line 2717650
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    goto :goto_0
.end method

.method private d(Landroid/preference/PreferenceCategory;)V
    .locals 1

    .prologue
    .line 2717651
    iget-boolean v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->e:Z

    if-eqz v0, :cond_0

    .line 2717652
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->n:LX/F6t;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717653
    :cond_0
    return-void
.end method

.method private d(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2717654
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->l:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OFF"

    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->k:LX/2TI;

    invoke-virtual {v1}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2717655
    :cond_0
    :goto_0
    return-void

    .line 2717656
    :cond_1
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2717657
    const v1, 0x7f083ad8

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2717658
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2717659
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->e(Landroid/preference/PreferenceCategory;)V

    .line 2717660
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    goto :goto_0
.end method

.method private e(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717661
    invoke-virtual {p0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Landroid/preference/Preference;)Landroid/preference/Preference;

    move-result-object v0

    .line 2717662
    if-eqz v0, :cond_0

    .line 2717663
    const-string v1, "vault_sync_mode"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 2717664
    const v1, 0x7f083aea

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 2717665
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717666
    :cond_0
    return-void
.end method

.method private e(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2717667
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2717668
    const v1, 0x7f083440

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2717669
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2717670
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Landroid/preference/PreferenceCategory;)V

    .line 2717671
    return-void
.end method

.method private f(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717672
    iget-boolean v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->i:Z

    if-eqz v0, :cond_0

    .line 2717673
    new-instance v0, LX/D34;

    const v1, 0x7f083ae7

    invoke-direct {v0, p0, v1}, LX/D34;-><init>(Landroid/content/Context;I)V

    .line 2717674
    new-instance v1, LX/Jc2;

    invoke-direct {v1, p0}, LX/Jc2;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, LX/D34;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717675
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717676
    :cond_0
    return-void
.end method

.method private f(Landroid/preference/PreferenceGroup;)V
    .locals 4

    .prologue
    .line 2717540
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    .line 2717541
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2717542
    const v1, 0x7f083add

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2717543
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2717544
    iget-boolean v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->d:Z

    if-eqz v1, :cond_0

    .line 2717545
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2717546
    const-string v2, "launch_point"

    const-string v3, "settings_phone_acquisition"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2717547
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717548
    const v3, 0x7f0833cf

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(I)V

    .line 2717549
    const v3, 0x7f0833d0

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 2717550
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2717551
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717552
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->o(Landroid/preference/PreferenceCategory;)V

    .line 2717553
    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 2717554
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 2717555
    :cond_1
    return-void
.end method

.method private g(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717556
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2717557
    const v1, 0x7f083ac7

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(I)V

    .line 2717558
    const v1, 0x7f083ac9

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOff(I)V

    .line 2717559
    const v1, 0x7f083aca

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOn(I)V

    .line 2717560
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2717561
    sget-object v1, LX/1Ip;->k:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2717562
    new-instance v1, LX/Jc3;

    invoke-direct {v1, p0}, LX/Jc3;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717563
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717564
    return-void
.end method

.method private g(Landroid/preference/PreferenceGroup;)V
    .locals 3

    .prologue
    .line 2717409
    sget-boolean v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->a:Z

    if-nez v0, :cond_0

    .line 2717410
    new-instance v0, LX/1pd;

    invoke-direct {v0, p0}, LX/1pd;-><init>(Landroid/content/Context;)V

    .line 2717411
    iget-object v1, v0, LX/1pd;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2717412
    move-object v0, v1

    .line 2717413
    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2717414
    sget-object v1, LX/0dU;->r:LX/0Tn;

    const-string v2, "facebook.com"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2717415
    :goto_0
    const-string v1, "facebook.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "beta.facebook.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "latest.facebook.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2717416
    if-nez v0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->g:LX/03R;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->h:LX/03R;

    if-ne v0, v1, :cond_1

    .line 2717417
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->a:Z

    .line 2717418
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717419
    const-string v1, "Intern settings"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2717420
    const-string v1, "Internal settings for debugging"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2717421
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2717422
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2717423
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2717424
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    .line 2717425
    :cond_1
    return-void

    .line 2717426
    :cond_2
    const-string v0, "facebook.com"

    goto :goto_0

    .line 2717427
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private h(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717428
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2717429
    const v1, 0x7f083ac8

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(I)V

    .line 2717430
    const v1, 0x7f083acb

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOff(I)V

    .line 2717431
    const v1, 0x7f083acc

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOn(I)V

    .line 2717432
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2717433
    sget-object v1, LX/1Ip;->l:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2717434
    new-instance v1, LX/Jc4;

    invoke-direct {v1, p0}, LX/Jc4;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717435
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717436
    return-void
.end method

.method private i(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717437
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->x:LX/A6h;

    invoke-virtual {v0}, LX/A6h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717438
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2717439
    const v1, 0x7f083acd

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(I)V

    .line 2717440
    const v1, 0x7f083acf

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOff(I)V

    .line 2717441
    const v1, 0x7f083ace

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOn(I)V

    .line 2717442
    sget-object v1, LX/A6h;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2717443
    new-instance v1, LX/Jc5;

    invoke-direct {v1, p0, v0}, LX/Jc5;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;LX/4ok;)V

    invoke-virtual {v0, v1}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2717444
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717445
    :cond_0
    return-void
.end method

.method private j(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717446
    iget-boolean v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->i:Z

    if-eqz v0, :cond_0

    .line 2717447
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->o:LX/D3a;

    const v1, 0x7f083ae8

    invoke-virtual {v0, v1}, LX/D3a;->setTitle(I)V

    .line 2717448
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->o:LX/D3a;

    const v1, 0x7f0e02d2

    .line 2717449
    iput v1, v0, LX/D3a;->d:I

    .line 2717450
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->o:LX/D3a;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717451
    :cond_0
    return-void
.end method

.method private k(Landroid/preference/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 2717452
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->u:LX/1sd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1sd;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2717453
    :goto_0
    return-void

    .line 2717454
    :cond_0
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717455
    const v1, 0x7f083ae9

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 2717456
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 2717457
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2717458
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2717459
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717460
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->v:LX/0TD;

    new-instance v2, LX/Jc6;

    invoke-direct {v2, p0}, LX/Jc6;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2717461
    new-instance v2, LX/Jc7;

    invoke-direct {v2, p0, v0, p1}, LX/Jc7;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;Landroid/preference/Preference;Landroid/preference/PreferenceCategory;)V

    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->w:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private l(Landroid/preference/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 2717462
    iget-boolean v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->f:Z

    if-eqz v0, :cond_0

    .line 2717463
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717464
    const-string v1, "video_autoplay"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 2717465
    const v1, 0x7f083ae5

    invoke-virtual {p0, v1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2717466
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->s:LX/1mD;

    iget-object v2, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->t:LX/1Bv;

    invoke-virtual {v2}, LX/1Bv;->b()LX/1mC;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1mD;->a(LX/1mC;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2717467
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2717468
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2717469
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717470
    new-instance v1, LX/Jbw;

    invoke-direct {v1, p0}, LX/Jbw;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2717471
    :cond_0
    return-void
.end method

.method private m(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717472
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->A:LX/BSM;

    invoke-virtual {v0}, LX/BSM;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717473
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717474
    const v1, 0x7f083ae6

    invoke-virtual {p0, v1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2717475
    invoke-static {p0}, LX/BSM;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2717476
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717477
    :cond_0
    return-void
.end method

.method private n(Landroid/preference/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 2717478
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->F:LX/2PY;

    .line 2717479
    iget-object v1, v0, LX/2PY;->a:LX/0ad;

    sget-short v2, LX/15r;->s:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 2717480
    if-eqz v0, :cond_0

    .line 2717481
    :goto_0
    return-void

    .line 2717482
    :cond_0
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2717483
    const v1, 0x7f083443

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 2717484
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717485
    new-instance v1, LX/Jbx;

    invoke-direct {v1, p0}, LX/Jbx;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method private o(Landroid/preference/PreferenceCategory;)V
    .locals 2

    .prologue
    .line 2717486
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->E:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->E:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717487
    new-instance v0, LX/GmP;

    invoke-direct {v0, p0}, LX/GmP;-><init>(Landroid/content/Context;)V

    .line 2717488
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2717489
    new-instance v1, LX/Jby;

    invoke-direct {v1, p0}, LX/Jby;-><init>(Lcom/facebook/katana/settings/activity/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2717490
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2717491
    const-string v0, "app_settings"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2717492
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2717493
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2717494
    invoke-static {p0, p0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2717495
    invoke-virtual {p0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->H:Landroid/preference/PreferenceScreen;

    .line 2717496
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2717497
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->H:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2717498
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->H:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Landroid/preference/PreferenceGroup;)V

    .line 2717499
    invoke-direct {p0, p1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->d(Landroid/os/Bundle;)V

    .line 2717500
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 2717501
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->finish()V

    .line 2717502
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    if-eqz v0, :cond_0

    .line 2717503
    invoke-static {p0}, LX/CIm;->b(Landroid/app/Activity;)V

    .line 2717504
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1e0106cc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2717505
    invoke-virtual {p0}, Lcom/facebook/katana/settings/activity/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 2717506
    if-eqz v1, :cond_0

    .line 2717507
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 2717508
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onDestroy()V

    .line 2717509
    const/16 v1, 0x23

    const v2, -0x68f68707

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x22

    const v1, -0x36d521f4    # -699872.75f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2717510
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 2717511
    const-string v1, "video_autoplay"

    invoke-virtual {p0, v1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 2717512
    if-eqz v1, :cond_0

    .line 2717513
    iget-object v2, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->s:LX/1mD;

    iget-object v3, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->s:LX/1mD;

    iget-object v4, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2717514
    iget-object v6, v3, LX/1mD;->f:LX/1mC;

    invoke-virtual {v3, v6, v4}, LX/1mD;->a(LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1mC;

    move-result-object v6

    move-object v3, v6

    .line 2717515
    invoke-virtual {v2, v3}, LX/1mD;->a(LX/1mC;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2717516
    :cond_0
    const-string v1, "vault_sync_mode"

    invoke-virtual {p0, v1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/katana/settings/activity/SettingsActivity;->a(Landroid/preference/Preference;)Landroid/preference/Preference;

    .line 2717517
    const/16 v1, 0x23

    const v2, -0x3afa7d3b

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2717526
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2717527
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4oo;

    .line 2717528
    invoke-interface {v0}, LX/4oo;->a()Landroid/os/Bundle;

    move-result-object v2

    .line 2717529
    if-eqz v2, :cond_0

    .line 2717530
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2717531
    :cond_1
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x33130616

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2717532
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStart()V

    .line 2717533
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    invoke-virtual {v1, p0}, LX/CIm;->b(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2717534
    iget-object v1, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    const v2, 0x7f083ad0

    invoke-virtual {v1, v2}, LX/CIm;->a(I)V

    .line 2717535
    const/16 v1, 0x23

    const v2, 0x5aeb3723

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2717536
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 2717537
    iget-object v0, p0, Lcom/facebook/katana/settings/activity/SettingsActivity;->r:LX/CIm;

    if-eqz v0, :cond_0

    .line 2717538
    invoke-static {p0}, LX/CIm;->a(Landroid/app/Activity;)V

    .line 2717539
    :cond_0
    return-void
.end method
