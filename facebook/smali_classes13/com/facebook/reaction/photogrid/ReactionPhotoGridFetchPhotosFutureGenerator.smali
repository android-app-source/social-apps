.class public Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;
.super LX/Dcc;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1V6;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0tX;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2762449
    const-class v0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1V6;LX/0tX;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762443
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2762444
    iput-object p2, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->b:LX/1V6;

    .line 2762445
    iput-object p4, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->c:Ljava/util/concurrent/ExecutorService;

    .line 2762446
    iput-object p3, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->d:LX/0tX;

    .line 2762447
    iput-object p1, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->e:Ljava/lang/String;

    .line 2762448
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2762438
    iget-object v0, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->b:LX/1V6;

    new-instance v1, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    iget-object v2, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1V6;->a(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;)LX/9h1;

    move-result-object v0

    .line 2762439
    const/16 v1, 0xa

    invoke-virtual {v0, v1, p2}, LX/9gr;->a(ILjava/lang/String;)LX/0gW;

    move-result-object v0

    .line 2762440
    iget-object v1, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    if-eqz p5, :cond_0

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2762441
    new-instance v1, LX/K1f;

    invoke-direct {v1}, LX/K1f;-><init>()V

    iget-object v2, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2762442
    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
