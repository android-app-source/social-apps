.class public Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;
.super Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.source ""


# instance fields
.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/K1g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2762452
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2762453
    invoke-super {p0, p1}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/os/Bundle;)V

    .line 2762454
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2762455
    const-string v1, "reaction_unit_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;->p:Ljava/lang/String;

    .line 2762456
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;

    const/16 p1, 0x2ead

    invoke-static {v2, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const-class v0, LX/K1g;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/K1g;

    iput-object p1, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;->n:LX/0Ot;

    iput-object v2, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;->o:LX/K1g;

    .line 2762457
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 7
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2762458
    iget-object v0, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2762459
    iget-object v3, v2, LX/Dvb;->i:LX/Dvc;

    move-object v2, v3

    .line 2762460
    invoke-virtual {v2}, LX/Dvc;->d()LX/0Px;

    move-result-object v4

    sget-object v5, LX/74S;->OTHER:LX/74S;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, LX/Dxd;->a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    .line 2762461
    return-void
.end method

.method public final c()LX/Dcc;
    .locals 5

    .prologue
    .line 2762462
    iget-object v0, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;->o:LX/K1g;

    iget-object v1, p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFragment;->p:Ljava/lang/String;

    .line 2762463
    new-instance p0, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;

    const-class v2, LX/1V6;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1V6;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/facebook/reaction/photogrid/ReactionPhotoGridFetchPhotosFutureGenerator;-><init>(Ljava/lang/String;LX/1V6;LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 2762464
    move-object v0, p0

    .line 2762465
    return-object v0
.end method
