.class public Lcom/facebook/places/checkin/activity/SelectAtTagActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field public q:LX/9j7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/9j5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/places/checkin/PlacePickerFragment;

.field private t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2751195
    const-class v0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2751194
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/checkin/activity/SelectAtTagActivity;LX/9j7;LX/9j5;)V
    .locals 0

    .prologue
    .line 2751193
    iput-object p1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    iput-object p2, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->r:LX/9j5;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;

    invoke-static {v1}, LX/9j7;->a(LX/0QB;)LX/9j7;

    move-result-object v0

    check-cast v0, LX/9j7;

    invoke-static {v1}, LX/9j5;->a(LX/0QB;)LX/9j5;

    move-result-object v1

    check-cast v1, LX/9j5;

    invoke-static {p0, v0, v1}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->a(Lcom/facebook/places/checkin/activity/SelectAtTagActivity;LX/9j7;LX/9j5;)V

    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2751174
    invoke-static {p0, p0}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2751175
    invoke-static {p0}, LX/9jy;->a(Ljava/lang/Object;)V

    .line 2751176
    invoke-virtual {p0}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "place_picker_configuration"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    iput-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751177
    iget-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2751178
    iget-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751179
    iget-object v3, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v3

    .line 2751180
    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2751181
    iget-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751182
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2751183
    const v5, 0x150017

    .line 2751184
    iget-object v2, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x150016

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2751185
    iget-object v2, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2751186
    invoke-static {v0, v5, v1}, LX/9j7;->a(LX/9j7;ILX/9jG;)V

    .line 2751187
    const v0, 0x7f0312ec

    invoke-virtual {p0, v0}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->setContentView(I)V

    .line 2751188
    invoke-direct {p0, p1}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->e(Landroid/os/Bundle;)V

    .line 2751189
    invoke-virtual {p0}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2751190
    return-void

    :cond_0
    move v0, v2

    .line 2751191
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2751192
    goto :goto_1
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2751161
    if-eqz p1, :cond_0

    .line 2751162
    iget-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->r:LX/9j5;

    invoke-virtual {v0, p1}, LX/9j5;->a(Landroid/os/Bundle;)V

    .line 2751163
    :goto_0
    return-void

    .line 2751164
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2751165
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->r:LX/9j5;

    iget-object v2, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751166
    iget-object p1, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v2, p1

    .line 2751167
    iput-object v2, v1, LX/9j5;->f:LX/9jG;

    .line 2751168
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->r:LX/9j5;

    iget-object v2, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->t:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751169
    iget-object p1, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v2, p1

    .line 2751170
    iput-object v2, v1, LX/9j5;->d:Ljava/lang/String;

    .line 2751171
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->r:LX/9j5;

    .line 2751172
    iput-object v0, v1, LX/9j5;->e:Ljava/lang/String;

    .line 2751173
    invoke-virtual {p0}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "place_picker_session_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private f(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2751156
    if-nez p1, :cond_0

    .line 2751157
    new-instance v0, Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->s:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2751158
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d2c11

    iget-object v2, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->s:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2751159
    :goto_0
    return-void

    .line 2751160
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2c11

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/PlacePickerFragment;

    iput-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->s:Lcom/facebook/places/checkin/PlacePickerFragment;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2751155
    const-string v0, "tag_places_view"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2751103
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2751104
    invoke-direct {p0, p1}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->d(Landroid/os/Bundle;)V

    .line 2751105
    invoke-direct {p0, p1}, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->f(Landroid/os/Bundle;)V

    .line 2751106
    iget-object v2, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    sget-object v3, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->p:Ljava/lang/String;

    .line 2751107
    iget-object p0, v2, LX/9j7;->b:LX/0id;

    invoke-virtual {p0, v3, v0, v1}, LX/0id;->b(Ljava/lang/String;J)V

    .line 2751108
    return-void
.end method

.method public final onBackPressed()V
    .locals 7

    .prologue
    .line 2751124
    iget-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->s:Lcom/facebook/places/checkin/PlacePickerFragment;

    if-eqz v0, :cond_0

    .line 2751125
    iget-object v0, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->s:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v2, 0x1

    .line 2751126
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    if-eqz v1, :cond_2

    .line 2751127
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;)V

    move v1, v2

    .line 2751128
    :goto_0
    move v0, v1

    .line 2751129
    if-nez v0, :cond_1

    .line 2751130
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2751131
    :cond_1
    return-void

    .line 2751132
    :cond_2
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2751133
    iget-object v3, v1, LX/9j5;->a:LX/0Zb;

    const-string v4, "place_picker_cancelled"

    invoke-static {v1, v4}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "results_seen"

    iget-object v6, v1, LX/9j5;->l:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v1, v4}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2751134
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751135
    iget-boolean v3, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    move v1, v3

    .line 2751136
    if-eqz v1, :cond_3

    .line 2751137
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751138
    iget-object v3, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v3

    .line 2751139
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2751140
    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->f:LX/0gd;

    iget-object v4, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751141
    iget-object v5, v4, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v4, v5

    .line 2751142
    invoke-virtual {v3, v4, v1}, LX/0gd;->b(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 2751143
    :cond_3
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->B:LX/0if;

    sget-object v3, LX/0ig;->ay:LX/0ih;

    const-string v4, "back_pressed"

    invoke-virtual {v1, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2751144
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-static {v0, v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->c(Lcom/facebook/places/checkin/PlacePickerFragment;Landroid/view/View;)V

    .line 2751145
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    .line 2751146
    iget-object v3, v1, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2751147
    iget-boolean v4, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    move v3, v4

    .line 2751148
    if-eqz v3, :cond_5

    iget-object v3, v1, LX/Dyt;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v3, :cond_5

    .line 2751149
    iget-object v3, v1, LX/Dyt;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v1, v3}, LX/Dyt;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2751150
    const/4 v3, 0x1

    .line 2751151
    :goto_1
    move v1, v3

    .line 2751152
    if-eqz v1, :cond_4

    move v1, v2

    .line 2751153
    goto :goto_0

    .line 2751154
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xdf6da88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2751121
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2751122
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    invoke-virtual {v1}, LX/9j7;->a()V

    .line 2751123
    const/16 v1, 0x23

    const v2, 0x271568f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2c18c5b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2751115
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    sget-object v2, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->p:Ljava/lang/String;

    .line 2751116
    iget-object v4, v1, LX/9j7;->b:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->e(Ljava/lang/String;)V

    .line 2751117
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2751118
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    sget-object v2, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->p:Ljava/lang/String;

    .line 2751119
    iget-object v4, v1, LX/9j7;->b:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->f(Ljava/lang/String;)V

    .line 2751120
    const/16 v1, 0x23

    const v2, 0x45100973

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x13d4d453

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2751109
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    sget-object v2, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->p:Ljava/lang/String;

    .line 2751110
    iget-object v4, v1, LX/9j7;->b:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->c(Ljava/lang/String;)V

    .line 2751111
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2751112
    iget-object v1, p0, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->q:LX/9j7;

    sget-object v2, Lcom/facebook/places/checkin/activity/SelectAtTagActivity;->p:Ljava/lang/String;

    .line 2751113
    iget-object v4, v1, LX/9j7;->b:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->d(Ljava/lang/String;)V

    .line 2751114
    const/16 v1, 0x23

    const v2, 0x630c4249

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
