.class public Lcom/facebook/storyline/ui/StorylineAudioIndicator;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Paint;

.field private c:Ljava/util/Random;

.field private d:[D

.field private e:[D

.field private f:[D

.field private g:[D

.field private h:I

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2770079
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2770080
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->c()V

    .line 2770081
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2770082
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2770083
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->c()V

    .line 2770084
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770085
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770086
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->c()V

    .line 2770087
    return-void
.end method

.method private static a(DDII)D
    .locals 4

    .prologue
    .line 2770099
    sub-double v0, p2, p0

    int-to-double v2, p4

    mul-double/2addr v0, v2

    int-to-double v2, p5

    div-double/2addr v0, v2

    add-double/2addr v0, p0

    return-wide v0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v2, 0x40800000    # 4.0f

    .line 2770088
    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->getWidth()I

    move-result v0

    .line 2770089
    mul-int/lit8 v1, v0, 0x3

    int-to-float v1, v1

    div-float v4, v1, v2

    .line 2770090
    int-to-float v1, v0

    div-float/2addr v1, v2

    .line 2770091
    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    iget v2, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->i:I

    mul-int/lit8 v2, v2, 0x6

    int-to-float v2, v2

    sub-float/2addr v0, v2

    const/high16 v2, 0x40e00000    # 7.0f

    div-float v7, v0, v2

    .line 2770092
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->d()V

    .line 2770093
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    const/4 v0, 0x7

    if-ge v6, v0, :cond_0

    .line 2770094
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->f:[D

    aget-wide v2, v0, v6

    double-to-float v0, v2

    .line 2770095
    sub-float v2, v4, v0

    add-float v3, v1, v7

    iget-object v5, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2770096
    iget v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->i:I

    int-to-float v0, v0

    add-float/2addr v0, v7

    add-float/2addr v1, v0

    .line 2770097
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2770098
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/Random;[D)V
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v0, 0x0

    .line 2770068
    invoke-virtual {p0}, Ljava/util/Random;->nextFloat()F

    move-result v3

    .line 2770069
    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    .line 2770070
    :goto_0
    if-ge v1, v8, :cond_0

    .line 2770071
    invoke-virtual {p0}, Ljava/util/Random;->nextFloat()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    aput-wide v4, p1, v1

    .line 2770072
    float-to-double v4, v2

    aget-wide v6, p1, v1

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 2770073
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2770074
    :cond_0
    const/high16 v1, 0x40e00000    # 7.0f

    div-float v1, v2, v1

    .line 2770075
    :goto_1
    if-ge v0, v8, :cond_1

    .line 2770076
    aget-wide v4, p1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    float-to-double v6, v2

    mul-double/2addr v4, v6

    mul-float v2, v1, v3

    float-to-double v6, v2

    add-double/2addr v4, v6

    aput-wide v4, p1, v0

    .line 2770077
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2770078
    :cond_1
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 2770054
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->setWillNotDraw(Z)V

    .line 2770055
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a:Landroid/graphics/Paint;

    .line 2770056
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2770057
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a09bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2770058
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->b:Landroid/graphics/Paint;

    .line 2770059
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2770060
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a09be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2770061
    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b26f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->i:I

    .line 2770062
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->c:Ljava/util/Random;

    .line 2770063
    new-array v0, v3, [D

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->d:[D

    .line 2770064
    new-array v0, v3, [D

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->e:[D

    .line 2770065
    new-array v0, v3, [D

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->f:[D

    .line 2770066
    new-array v0, v3, [D

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->g:[D

    .line 2770067
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v0, 0x0

    .line 2770028
    iget v1, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->h:I

    rem-int/lit8 v1, v1, 0x5

    if-nez v1, :cond_0

    .line 2770029
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->d:[D

    iget-object v2, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->e:[D

    invoke-static {v1, v0, v2, v0, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2770030
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->e()V

    :cond_0
    move v6, v0

    .line 2770031
    :goto_0
    if-ge v6, v8, :cond_1

    .line 2770032
    iget-object v7, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->f:[D

    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->e:[D

    aget-wide v0, v0, v6

    iget-object v2, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->d:[D

    aget-wide v2, v2, v6

    iget v4, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->h:I

    rem-int/lit8 v4, v4, 0x5

    const/4 v5, 0x5

    invoke-static/range {v0 .. v5}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a(DDII)D

    move-result-wide v0

    aput-wide v0, v7, v6

    .line 2770033
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2770034
    :cond_1
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 2770049
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->c:Ljava/util/Random;

    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->g:[D

    invoke-static {v0, v1}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a(Ljava/util/Random;[D)V

    .line 2770050
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 2770051
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->d:[D

    iget-object v2, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->g:[D

    aget-wide v2, v2, v0

    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->getWidth()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 2770052
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2770053
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2770045
    iget-boolean v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->j:Z

    if-eqz v0, :cond_0

    .line 2770046
    :goto_0
    return-void

    .line 2770047
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->j:Z

    .line 2770048
    const-wide/16 v0, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->postInvalidateDelayed(J)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2770043
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->j:Z

    .line 2770044
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2770035
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2770036
    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 2770037
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v0, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2770038
    invoke-direct {p0, p1}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a(Landroid/graphics/Canvas;)V

    .line 2770039
    iget v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->h:I

    .line 2770040
    iget-boolean v0, p0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->j:Z

    if-eqz v0, :cond_0

    .line 2770041
    const-wide/16 v0, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->postInvalidateDelayed(J)V

    .line 2770042
    :cond_0
    return-void
.end method
