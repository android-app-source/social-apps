.class public Lcom/facebook/storyline/ui/StorylineSeekBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/SeekBar;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field public d:LX/K40;

.field private e:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2770145
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2770146
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a()V

    .line 2770147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770148
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2770149
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a()V

    .line 2770150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770151
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770152
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a()V

    .line 2770153
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2770154
    const v0, 0x7f0313fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2770155
    invoke-virtual {p0, v2}, Lcom/facebook/storyline/ui/StorylineSeekBar;->setOrientation(I)V

    .line 2770156
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->setGravity(I)V

    .line 2770157
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->a:Landroid/widget/SeekBar;

    .line 2770158
    const v0, 0x7f0d1329

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->b:Landroid/widget/TextView;

    .line 2770159
    const v0, 0x7f0d132b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->c:Landroid/widget/TextView;

    .line 2770160
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->a:Landroid/widget/SeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 2770161
    new-instance v0, LX/K5G;

    invoke-direct {v0, p0}, LX/K5G;-><init>(Lcom/facebook/storyline/ui/StorylineSeekBar;)V

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->e:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 2770162
    return-void
.end method

.method public static a$redex0(Lcom/facebook/storyline/ui/StorylineSeekBar;F)V
    .locals 4

    .prologue
    .line 2770163
    iget v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 2770164
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->b:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2770165
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->c:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->f:I

    sub-int v0, v3, v0

    invoke-static {v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2770166
    return-void
.end method

.method private static c(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2770167
    rem-int/lit8 v0, p0, 0x3c

    int-to-long v0, v0

    .line 2770168
    div-int/lit8 v2, p0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    int-to-long v2, v2

    .line 2770169
    const-string v4, "%2d:%02d"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v4, v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setDuration(I)V
    .locals 2

    .prologue
    const/high16 v1, 0x447a0000    # 1000.0f

    .line 2770170
    int-to-float v0, p1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->f:I

    .line 2770171
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->a:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-static {p0, v0}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a$redex0(Lcom/facebook/storyline/ui/StorylineSeekBar;F)V

    .line 2770172
    return-void
.end method

.method public setListener(LX/K40;)V
    .locals 2
    .param p1    # LX/K40;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770173
    iput-object p1, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->d:LX/K40;

    .line 2770174
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->a:Landroid/widget/SeekBar;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2770175
    return-void

    .line 2770176
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->e:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    goto :goto_0
.end method

.method public setProgress(F)V
    .locals 2

    .prologue
    .line 2770177
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylineSeekBar;->a:Landroid/widget/SeekBar;

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2770178
    invoke-static {p0, p1}, Lcom/facebook/storyline/ui/StorylineSeekBar;->a$redex0(Lcom/facebook/storyline/ui/StorylineSeekBar;F)V

    .line 2770179
    return-void
.end method
