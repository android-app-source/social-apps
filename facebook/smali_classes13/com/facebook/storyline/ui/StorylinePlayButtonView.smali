.class public Lcom/facebook/storyline/ui/StorylinePlayButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2770103
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2770104
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a()V

    .line 2770105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770106
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2770107
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a()V

    .line 2770108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2770109
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2770110
    invoke-direct {p0}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a()V

    .line 2770111
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2770112
    const v0, 0x7f0313fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2770113
    const v0, 0x7f0d1333

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a:Landroid/view/View;

    .line 2770114
    new-instance v0, LX/K5F;

    invoke-direct {v0, p0}, LX/K5F;-><init>(Lcom/facebook/storyline/ui/StorylinePlayButtonView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2770115
    return-void
.end method

.method public static b(Lcom/facebook/storyline/ui/StorylinePlayButtonView;)V
    .locals 2

    .prologue
    .line 2770116
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2770117
    return-void

    .line 2770118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2770119
    iget-object v0, p0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2770120
    return-void
.end method

.method public setButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2770121
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2770122
    return-void

    .line 2770123
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setIsPlaying(Z)V
    .locals 3

    .prologue
    .line 2770124
    iget-object v1, p0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f021a52

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2770125
    return-void

    .line 2770126
    :cond_0
    const v0, 0x7f021a53

    goto :goto_0
.end method
