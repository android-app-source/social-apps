.class public final Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x59c84a5a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767252
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767251
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2767249
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2767250
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2767232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767233
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2767234
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2767235
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2767236
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2767237
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2767238
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2767239
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2767240
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2767241
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2767242
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2767243
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2767244
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2767245
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2767246
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2767247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767248
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2767219
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767220
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2767221
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    .line 2767222
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2767223
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2767224
    iput-object v0, v1, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    .line 2767225
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2767226
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2767227
    if-eqz v2, :cond_1

    .line 2767228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2767229
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 2767230
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767231
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767207
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2767216
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2767217
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->i:Z

    .line 2767218
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2767213
    new-instance v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    invoke-direct {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;-><init>()V

    .line 2767214
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2767215
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2767253
    const v0, -0x1a515ddb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2767212
    const v0, 0x61a31093

    return v0
.end method

.method public final j()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767210
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    .line 2767211
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2767208
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/util/List;

    .line 2767209
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767205
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->g:Ljava/lang/String;

    .line 2767206
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767203
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->h:Ljava/lang/String;

    .line 2767204
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2767201
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2767202
    iget-boolean v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->i:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767199
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j:Ljava/lang/String;

    .line 2767200
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767197
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k:Ljava/lang/String;

    .line 2767198
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k:Ljava/lang/String;

    return-object v0
.end method
