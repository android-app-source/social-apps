.class public final Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767109
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767108
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2767106
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2767107
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2767104
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2767105
    iget v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2767096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767097
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2767098
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2767099
    iget v1, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 2767100
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2767101
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2767102
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767103
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2767093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767095
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2767089
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2767090
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->e:I

    .line 2767091
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->g:I

    .line 2767092
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2767080
    new-instance v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;

    invoke-direct {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;-><init>()V

    .line 2767081
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2767082
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2767088
    const v0, 0x414b273

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2767087
    const v0, 0x437b93b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767085
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->f:Ljava/lang/String;

    .line 2767086
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2767083
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2767084
    iget v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$ButtonIconModel;->g:I

    return v0
.end method
