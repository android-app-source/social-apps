.class public final Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2767286
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;

    new-instance v1, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2767287
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2767288
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2767289
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2767290
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2767291
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2767292
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2767293
    if-eqz v2, :cond_0

    .line 2767294
    const-string p0, "movie_factory_config"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2767295
    invoke-static {v1, v2, p1, p2}, LX/K4D;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2767296
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2767297
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2767298
    check-cast p1, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Serializer;->a(Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
