.class public final Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x56718833
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767174
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767175
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2767172
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2767173
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2767157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767158
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2767159
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2767160
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2767161
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2767162
    const/4 v4, 0x7

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2767163
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2767164
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2767165
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->g:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 2767166
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2767167
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->i:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 2767168
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->j:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 2767169
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2767170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767171
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2767154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767156
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767153
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2767148
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2767149
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->g:I

    .line 2767150
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->i:I

    .line 2767151
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->j:I

    .line 2767152
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2767145
    new-instance v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;

    invoke-direct {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;-><init>()V

    .line 2767146
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2767147
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2767176
    const v0, -0x738be575

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2767144
    const v0, -0x55be1f07

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767142
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->e:Ljava/lang/String;

    .line 2767143
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767140
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->f:Ljava/lang/String;

    .line 2767141
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 2767138
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2767139
    iget v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->g:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767136
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->h:Ljava/lang/String;

    .line 2767137
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 2767134
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2767135
    iget v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->i:I

    return v0
.end method

.method public final o()I
    .locals 2

    .prologue
    .line 2767130
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2767131
    iget v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->j:I

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767132
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->k:Ljava/lang/String;

    .line 2767133
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->k:Ljava/lang/String;

    return-object v0
.end method
