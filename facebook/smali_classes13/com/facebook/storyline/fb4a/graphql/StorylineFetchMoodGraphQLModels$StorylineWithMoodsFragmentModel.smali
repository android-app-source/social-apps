.class public final Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12200e96
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767317
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2767312
    const-class v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2767315
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2767316
    return-void
.end method

.method private a()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767313
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    .line 2767314
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2767318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767319
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->a()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2767320
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2767321
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2767322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767323
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2767302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2767303
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->a()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2767304
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->a()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    .line 2767305
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->a()Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2767306
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;

    .line 2767307
    iput-object v0, v1, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;->e:Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel$MovieFactoryConfigModel;

    .line 2767308
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2767309
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2767299
    new-instance v0, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/storyline/fb4a/graphql/StorylineFetchMoodGraphQLModels$StorylineWithMoodsFragmentModel;-><init>()V

    .line 2767300
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2767301
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2767310
    const v0, 0x37174a67

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2767311
    const v0, -0x3ff252d0

    return v0
.end method
