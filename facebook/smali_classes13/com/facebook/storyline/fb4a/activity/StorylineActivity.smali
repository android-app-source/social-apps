.class public Lcom/facebook/storyline/fb4a/activity/StorylineActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Ljava/lang/String;

.field private static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/BOf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private D:LX/1kZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private E:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private G:Lcom/facebook/react/ReactRootView;

.field private H:Lcom/facebook/storyline/ui/SquareTextureView;

.field private I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public J:Lcom/facebook/drawee/view/DraweeView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/drawee/view/DraweeView",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field public K:Lcom/facebook/storyline/ui/StorylineSeekBar;

.field public L:Landroid/widget/ProgressBar;

.field public M:Landroid/widget/ProgressBar;

.field public N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

.field public O:Lcom/facebook/storyline/ui/StorylineAudioIndicator;

.field public P:LX/4BY;

.field private Q:Lcom/facebook/storyline/fb4a/ui/TabSectionView;

.field private R:Landroid/view/View;

.field private S:Landroid/view/View;

.field private T:Landroid/view/View;

.field public U:Lcom/facebook/storyline/fb4a/ui/TitleEditView;

.field public V:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private W:Lcom/facebook/fbui/glyph/GlyphView;

.field public X:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Mood;",
            ">;"
        }
    .end annotation
.end field

.field public Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylinePhoto;",
            ">;"
        }
    .end annotation
.end field

.field public Z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private aa:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylineUser;",
            ">;"
        }
    .end annotation
.end field

.field public ab:Ljava/lang/String;

.field public ac:Z

.field private ad:LX/K5D;

.field private ae:LX/K3z;

.field public af:LX/K4N;

.field public ag:Lcom/facebook/storyline/model/StorylinePhoto;

.field public ah:Lcom/facebook/storyline/model/Mood;

.field private ai:LX/4nG;

.field private aj:LX/4nF;

.field private ak:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private al:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/33u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/K4o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/K4H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/D1k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2766673
    const-class v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->p:Ljava/lang/String;

    .line 2766674
    const-class v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2766675
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2766676
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2766677
    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    .line 2766678
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2766679
    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    .line 2766680
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2766681
    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->aa:LX/0Px;

    .line 2766682
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2766683
    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    .line 2766684
    return-void
.end method

.method private A()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2766685
    new-instance v0, LX/4BY;

    invoke-direct {v0, p0}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    .line 2766686
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    const v1, 0x7f083c3b

    invoke-virtual {v0, v1}, LX/4BY;->setTitle(I)V

    .line 2766687
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v0, v5}, LX/4BY;->setCancelable(Z)V

    .line 2766688
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v0, v4}, LX/4BY;->setCanceledOnTouchOutside(Z)V

    .line 2766689
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    const/4 v1, -0x2

    const v2, 0x7f083c33

    invoke-virtual {p0, v2}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/K3j;

    invoke-direct {v3, p0}, LX/K3j;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2766690
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    new-instance v1, LX/K3k;

    invoke-direct {v1, p0}, LX/K3k;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, LX/4BY;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2766691
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    new-instance v1, LX/K3l;

    invoke-direct {v1, p0}, LX/K3l;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, LX/4BY;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2766692
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v0, v4}, LX/4BY;->a(Z)V

    .line 2766693
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    .line 2766694
    iput v5, v0, LX/4BY;->d:I

    .line 2766695
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LX/4BY;->c(I)V

    .line 2766696
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4BY;->a(Ljava/lang/String;)V

    .line 2766697
    return-void
.end method

.method public static B(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 12

    .prologue
    .line 2766698
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    iget-object v2, v2, Lcom/facebook/storyline/model/Mood;->name:Ljava/lang/String;

    .line 2766699
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "photos_count"

    invoke-virtual {v3, v4, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v3

    const-string v4, "mood_name"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    .line 2766700
    iget-object v4, v0, LX/D1k;->a:LX/0if;

    sget-object v5, LX/0ig;->V:LX/0ih;

    const-string v6, "export_video_started"

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2766701
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->v:LX/1Er;

    const-string v1, "storyline-video-"

    const-string v2, ".mp4"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 2766702
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const/16 v9, 0x280

    .line 2766703
    sget-object v4, LX/K4i;->a:[I

    iget-object v5, v1, LX/K4o;->p:LX/K4l;

    invoke-virtual {v5}, LX/K4l;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2766704
    :goto_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    if-nez v0, :cond_0

    .line 2766705
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->A()V

    .line 2766706
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 2766707
    return-void

    .line 2766708
    :pswitch_0
    invoke-virtual {v1}, LX/K4o;->d()V

    .line 2766709
    :pswitch_1
    sget-object v4, LX/K4l;->EXPORTING:LX/K4l;

    invoke-static {v1, v4}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    .line 2766710
    new-instance v4, LX/K3c;

    invoke-direct {v4}, LX/K3c;-><init>()V

    iput-object v4, v1, LX/K4o;->q:LX/K3c;

    .line 2766711
    new-instance v4, LX/K3g;

    iget-object v5, v1, LX/K4o;->c:Ljava/util/List;

    iget-object v6, v1, LX/K4o;->j:LX/K4q;

    iget-object v6, v6, LX/K4q;->e:Lcom/facebook/storyline/model/Cutdown;

    iget-object v6, v6, Lcom/facebook/storyline/model/Cutdown;->c:Ljava/lang/String;

    iget v7, v1, LX/K4o;->w:I

    invoke-static {v7, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget v7, v1, LX/K4o;->x:I

    invoke-static {v7, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    iget v10, v1, LX/K4o;->z:I

    iget-object v7, v1, LX/K4o;->j:LX/K4q;

    iget v11, v7, LX/K4q;->f:F

    move-object v7, v0

    invoke-direct/range {v4 .. v11}, LX/K3g;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IIIF)V

    .line 2766712
    iget-object v5, v1, LX/K4o;->f:LX/K4g;

    iget-object v6, v1, LX/K4o;->f:LX/K4g;

    sget-object v7, LX/K4d;->EXPORT_VIDEO:LX/K4d;

    iget-object v8, v1, LX/K4o;->q:LX/K3c;

    invoke-static {v4, v8}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, LX/K4g;->a(LX/K4d;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/K4g;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static C(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 1

    .prologue
    .line 2766713
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0}, LX/K4o;->e()V

    .line 2766714
    return-void
.end method

.method private static a(Lcom/facebook/ipc/model/FacebookProfile;)Lcom/facebook/storyline/model/StorylineUser;
    .locals 5

    .prologue
    .line 2766715
    new-instance v0, Lcom/facebook/storyline/model/StorylineUser;

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    const-string v3, ""

    iget-object v4, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/storyline/model/StorylineUser;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/user/model/User;)Lcom/facebook/storyline/model/StorylineUser;
    .locals 5

    .prologue
    .line 2766716
    new-instance v0, Lcom/facebook/storyline/model/StorylineUser;

    .line 2766717
    iget-object v1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2766718
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/storyline/model/StorylineUser;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylinePhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2766719
    iput-object p1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    .line 2766720
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 2766721
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/StorylinePhoto;

    invoke-direct {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Lcom/facebook/storyline/model/StorylinePhoto;)V

    .line 2766722
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->T:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2766723
    :goto_0
    return-void

    .line 2766724
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->T:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 4

    .prologue
    .line 2766725
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_storyline_entry_point"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2766726
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2766727
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    iget-object v3, v3, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;->setMoodId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v2

    .line 2766728
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialStorylineData(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-static {v2}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->c(Ljava/util/List;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2766729
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z:LX/1Kf;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-interface {v1, v2, v0, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2766730
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->finish()V

    .line 2766731
    return-void
.end method

.method private static a(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/33u;LX/K4o;LX/1Ad;Lcom/facebook/content/SecureContextHelper;LX/1Er;LX/K4H;Ljava/util/concurrent/Executor;LX/D1k;LX/1Kf;LX/1Ck;LX/0TD;LX/BOf;LX/1kZ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/storyline/fb4a/activity/StorylineActivity;",
            "LX/33u;",
            "LX/K4o;",
            "LX/1Ad;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Er;",
            "LX/K4H;",
            "Ljava/util/concurrent/Executor;",
            "LX/D1k;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0TD;",
            "LX/BOf;",
            "LX/1kZ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2766732
    iput-object p1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->r:LX/33u;

    iput-object p2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    iput-object p3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->t:LX/1Ad;

    iput-object p4, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->u:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->v:LX/1Er;

    iput-object p6, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->w:LX/K4H;

    iput-object p7, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->x:Ljava/util/concurrent/Executor;

    iput-object p8, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    iput-object p9, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z:LX/1Kf;

    iput-object p10, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->A:LX/1Ck;

    iput-object p11, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->B:LX/0TD;

    iput-object p12, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->C:LX/BOf;

    iput-object p13, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->D:LX/1kZ;

    iput-object p14, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->E:LX/0Or;

    return-void
.end method

.method private a(Lcom/facebook/storyline/model/StorylinePhoto;)V
    .locals 3
    .param p1    # Lcom/facebook/storyline/model/StorylinePhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2766733
    if-nez p1, :cond_0

    .line 2766734
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->J:Lcom/facebook/drawee/view/DraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2766735
    :goto_0
    return-void

    .line 2766736
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ad:LX/K5D;

    .line 2766737
    iget v1, v0, LX/K5D;->g:I

    move v0, v1

    .line 2766738
    iget-object v1, p1, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v2, LX/1o9;

    invoke-direct {v2, v0, v0}, LX/1o9;-><init>(II)V

    .line 2766739
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 2766740
    move-object v0, v1

    .line 2766741
    invoke-static {}, LX/1bd;->a()LX/1bd;

    move-result-object v1

    .line 2766742
    iput-object v1, v0, LX/1bX;->d:LX/1bd;

    .line 2766743
    move-object v0, v0

    .line 2766744
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ad:LX/K5D;

    .line 2766745
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 2766746
    move-object v0, v0

    .line 2766747
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2766748
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->J:Lcom/facebook/drawee/view/DraweeView;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->t:LX/1Ad;

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->q:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->J:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-static {v14}, LX/33u;->a(LX/0QB;)LX/33u;

    move-result-object v1

    check-cast v1, LX/33u;

    invoke-static {v14}, LX/K4o;->b(LX/0QB;)LX/K4o;

    move-result-object v2

    check-cast v2, LX/K4o;

    invoke-static {v14}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v14}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v14}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v5

    check-cast v5, LX/1Er;

    invoke-static {v14}, LX/K4H;->b(LX/0QB;)LX/K4H;

    move-result-object v6

    check-cast v6, LX/K4H;

    invoke-static {v14}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v14}, LX/D1k;->b(LX/0QB;)LX/D1k;

    move-result-object v8

    check-cast v8, LX/D1k;

    invoke-static {v14}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v9

    check-cast v9, LX/1Kf;

    invoke-static {v14}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v14}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, LX/0TD;

    invoke-static {v14}, LX/BOf;->b(LX/0QB;)LX/BOf;

    move-result-object v12

    check-cast v12, LX/BOf;

    invoke-static {v14}, LX/1kZ;->a(LX/0QB;)LX/1kZ;

    move-result-object v13

    check-cast v13, LX/1kZ;

    const/16 v15, 0x12cb

    invoke-static {v14, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {v0 .. v14}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/33u;LX/K4o;LX/1Ad;Lcom/facebook/content/SecureContextHelper;LX/1Er;LX/K4H;Ljava/util/concurrent/Executor;LX/D1k;LX/1Kf;LX/1Ck;LX/0TD;LX/BOf;LX/1kZ;LX/0Or;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2766749
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    .line 2766750
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2766751
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2766752
    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Lcom/facebook/ipc/model/FacebookProfile;)Lcom/facebook/storyline/model/StorylineUser;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2766753
    :cond_0
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2766754
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2766755
    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Lcom/facebook/user/model/User;)Lcom/facebook/storyline/model/StorylineUser;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2766756
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->aa:LX/0Px;

    .line 2766757
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766758
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2766759
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->W:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, -0xa76f01

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2766760
    :goto_1
    return-void

    .line 2766761
    :cond_2
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->W:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/K3g;)V
    .locals 5

    .prologue
    .line 2766838
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/K3g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2766839
    new-instance v1, LX/4gP;

    invoke-direct {v1}, LX/4gP;-><init>()V

    invoke-virtual {v1, v0}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/media/data/MimeType;->b:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0, v1}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v0

    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    iget v1, p1, LX/K3g;->d:I

    .line 2766840
    iput v1, v0, LX/4gP;->f:I

    .line 2766841
    move-object v0, v0

    .line 2766842
    iget v1, p1, LX/K3g;->e:I

    .line 2766843
    iput v1, v0, LX/4gP;->g:I

    .line 2766844
    move-object v0, v0

    .line 2766845
    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2766846
    new-instance v1, LX/4gN;

    invoke-direct {v1}, LX/4gN;-><init>()V

    invoke-virtual {v1, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    .line 2766847
    new-instance v1, LX/74m;

    invoke-direct {v1}, LX/74m;-><init>()V

    .line 2766848
    iput-object v0, v1, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2766849
    move-object v0, v1

    .line 2766850
    iget v1, p1, LX/K3g;->f:I

    int-to-long v2, v1

    .line 2766851
    iput-wide v2, v0, LX/74m;->d:J

    .line 2766852
    move-object v0, v0

    .line 2766853
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    .line 2766854
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2766855
    invoke-direct {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Lcom/facebook/ipc/media/MediaItem;)V

    .line 2766856
    :goto_0
    return-void

    .line 2766857
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2766858
    const-string v2, "extra_storyline_exported_media_item"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2766859
    const-string v0, "extra_storyline_data"

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    iget-object v3, v3, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;->setMoodId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2766860
    const-string v0, "extra_storyline_tagged_friends"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2766861
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->setResult(ILandroid/content/Intent;)V

    .line 2766862
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->finish()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/K4K;)V
    .locals 2

    .prologue
    .line 2766835
    sget-object v0, LX/K3m;->b:[I

    invoke-virtual {p1}, LX/K4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2766836
    :goto_0
    return-void

    .line 2766837
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->u(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/K4N;)V
    .locals 3

    .prologue
    .line 2766821
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    if-ne v0, p1, :cond_0

    .line 2766822
    :goto_0
    return-void

    .line 2766823
    :cond_0
    iput-object p1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    .line 2766824
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Q:Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    invoke-virtual {v0, p1}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a(LX/K4N;)V

    .line 2766825
    sget-object v0, LX/K3m;->a:[I

    invoke-virtual {p1}, LX/K4N;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2766826
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766827
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string p1, "photo_tab_selected"

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766828
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->v(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0

    .line 2766829
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766830
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string p1, "music_tab_selected"

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766831
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->w()V

    goto :goto_0

    .line 2766832
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766833
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string p1, "title_tab_selected"

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766834
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2766813
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766814
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string v3, "friends_picker_launched"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766815
    invoke-static {}, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->newBuilder()LX/A5S;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->b()LX/0Px;

    move-result-object v1

    .line 2766816
    iput-object v1, v0, LX/A5S;->j:LX/0Px;

    .line 2766817
    move-object v0, v0

    .line 2766818
    invoke-virtual {v0}, LX/A5S;->a()Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 2766819
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2766820
    return-void
.end method

.method public static a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Lcom/facebook/storyline/model/Mood;)V
    .locals 1

    .prologue
    .line 2766808
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2766809
    :goto_0
    return-void

    .line 2766810
    :cond_0
    iput-object p1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    .line 2766811
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2766812
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0
.end method

.method private b()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2766800
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2766801
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2766802
    :goto_0
    return-object v0

    .line 2766803
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2766804
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2766805
    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2766806
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2766807
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/StorylinePhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2766778
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2766779
    const/4 v0, 0x1

    .line 2766780
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v1, v0

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    move-object v6, v0

    .line 2766781
    check-cast v6, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2766782
    invoke-virtual {v6}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 2766783
    iget v0, v3, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v0, v0

    .line 2766784
    const/16 v2, 0x5a

    if-eq v0, v2, :cond_0

    .line 2766785
    iget v0, v3, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v0, v0

    .line 2766786
    const/16 v2, 0x10e

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2766787
    :goto_1
    if-eqz v0, :cond_2

    .line 2766788
    iget v2, v3, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v2, v2

    .line 2766789
    :goto_2
    if-eqz v0, :cond_3

    .line 2766790
    iget v0, v3, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v3, v0

    .line 2766791
    :goto_3
    new-instance v0, Lcom/facebook/storyline/model/StorylinePhoto;

    add-int/lit8 v10, v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-double v6, v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/storyline/model/StorylinePhoto;-><init>(Ljava/lang/String;IILandroid/net/Uri;Landroid/net/Uri;DLX/0Px;Landroid/graphics/RectF;)V

    .line 2766792
    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v1, v10

    .line 2766793
    goto :goto_0

    .line 2766794
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2766795
    :cond_2
    iget v2, v3, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v2, v2

    .line 2766796
    goto :goto_2

    .line 2766797
    :cond_3
    iget v0, v3, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v3, v0

    .line 2766798
    goto :goto_3

    .line 2766799
    :cond_4
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Lcom/facebook/storyline/model/StorylinePhoto;)V
    .locals 7
    .param p0    # Lcom/facebook/storyline/fb4a/activity/StorylineActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2766769
    iput-object p1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ag:Lcom/facebook/storyline/model/StorylinePhoto;

    .line 2766770
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2766771
    if-eqz p1, :cond_0

    .line 2766772
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766773
    sget-object v1, LX/K4i;->a:[I

    iget-object v2, v0, LX/K4o;->p:LX/K4l;

    invoke-virtual {v2}, LX/K4l;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2766774
    :cond_0
    :goto_0
    return-void

    .line 2766775
    :pswitch_0
    invoke-virtual {v0}, LX/K4o;->d()V

    .line 2766776
    :pswitch_1
    iget-object v1, v0, LX/K4o;->m:LX/0P1;

    iget-object v2, p1, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    .line 2766777
    const-wide v3, 0x408f400000000000L    # 1000.0

    mul-double/2addr v1, v3

    const-wide/16 v3, 0x2

    iget-wide v5, v0, LX/K4o;->A:J

    mul-long/2addr v3, v5

    long-to-double v3, v3

    add-double/2addr v1, v3

    iget v3, v0, LX/K4o;->z:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, LX/K4o;->a(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static c(Ljava/util/List;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2766663
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2766664
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2766665
    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 2766666
    iput-object v4, v3, LX/5Rc;->b:Ljava/lang/String;

    .line 2766667
    move-object v3, v3

    .line 2766668
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 2766669
    iput-object v0, v3, LX/5Rc;->c:Ljava/lang/String;

    .line 2766670
    move-object v0, v3

    .line 2766671
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2766672
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2766391
    if-eqz p1, :cond_1

    .line 2766392
    :goto_0
    if-nez p1, :cond_2

    .line 2766393
    :cond_0
    :goto_1
    return-void

    .line 2766394
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 2766395
    :cond_2
    const-string v0, "extra_storyline_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ab:Ljava/lang/String;

    .line 2766396
    const-string v0, "extra_storyline_selected_mood"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/Mood;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    .line 2766397
    const-string v0, "extra_storyline_selected_profiles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2766398
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2766399
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    .line 2766400
    :cond_3
    const-string v0, "extra_storyline_moods"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2766401
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2766402
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    .line 2766403
    :cond_4
    const-string v0, "extra_storyline_media_items"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2766404
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2766405
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    goto :goto_1
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2766762
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->D:LX/1kZ;

    .line 2766763
    iget-object v1, v0, LX/1kZ;->n:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 2766764
    iget-object v1, v0, LX/1kZ;->a:LX/0ad;

    sget-short v2, LX/1vU;->l:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1kZ;->n:Ljava/lang/Boolean;

    .line 2766765
    :cond_0
    iget-object v1, v0, LX/1kZ;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 2766766
    if-nez v0, :cond_2

    .line 2766767
    :cond_1
    :goto_0
    return-void

    .line 2766768
    :cond_2
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->A:LX/1Ck;

    sget-object v1, LX/K43;->CHECK_RECENT_MEDIA:LX/K43;

    new-instance v2, LX/K3t;

    invoke-direct {v2, p0}, LX/K3t;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    new-instance v3, LX/K3u;

    invoke-direct {v3, p0}, LX/K3u;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static m(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)Z
    .locals 1

    .prologue
    .line 2766474
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2766475
    const/4 v0, 0x0

    .line 2766476
    :goto_0
    return v0

    .line 2766477
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(LX/0Px;)V

    .line 2766478
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static n$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 7

    .prologue
    .line 2766461
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2766462
    :goto_0
    return-void

    .line 2766463
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766464
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string v3, "fetch_moods_started"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766465
    new-instance v0, LX/K3v;

    invoke-direct {v0, p0}, LX/K3v;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766466
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    sget-object v2, LX/K4N;->MUSIC:LX/K4N;

    if-ne v1, v2, :cond_1

    .line 2766467
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2766468
    :cond_1
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->w:LX/K4H;

    .line 2766469
    new-instance v3, LX/K45;

    invoke-direct {v3}, LX/K45;-><init>()V

    move-object v3, v3

    .line 2766470
    const-string v4, "appID"

    const-string v5, "350685531728"

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2766471
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/32 v5, 0x15180

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2766472
    iget-object v4, v1, LX/K4H;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v1, v3

    .line 2766473
    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->x:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static o$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 2

    .prologue
    .line 2766457
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    sget-object v1, LX/K4N;->MUSIC:LX/K4N;

    if-ne v0, v1, :cond_0

    .line 2766458
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2766459
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/Mood;

    invoke-static {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Lcom/facebook/storyline/model/Mood;)V

    .line 2766460
    return-void
.end method

.method public static p$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 2

    .prologue
    .line 2766449
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    sget-object v1, LX/K4N;->MUSIC:LX/K4N;

    if-eq v0, v1, :cond_1

    .line 2766450
    :cond_0
    :goto_0
    return-void

    .line 2766451
    :cond_1
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2766452
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2766453
    :goto_1
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->e()V

    .line 2766454
    if-eqz v0, :cond_0

    .line 2766455
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->q()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2766456
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private q()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2766448
    new-instance v0, LX/K3w;

    invoke-direct {v0, p0}, LX/K3w;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    return-object v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 2766439
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f083c39

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2766440
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/K3h;

    invoke-direct {v1, p0}, LX/K3h;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2766441
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f083c3a

    invoke-virtual {p0, v1}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2766442
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2766443
    move-object v0, v0

    .line 2766444
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ak:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2766445
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/K3i;

    invoke-direct {v1, p0}, LX/K3i;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2766446
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766447
    return-void
.end method

.method public static s(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 3

    .prologue
    .line 2766432
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766433
    iget-object v1, v0, LX/K4o;->p:LX/K4l;

    move-object v0, v1

    .line 2766434
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ak:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    sget-object v2, LX/K4l;->READY_TO_PLAY:LX/K4l;

    if-eq v0, v2, :cond_0

    sget-object v2, LX/K4l;->PLAYING:LX/K4l;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2766435
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2766436
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ak:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2766437
    return-void

    .line 2766438
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 6

    .prologue
    .line 2766425
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->D:LX/1kZ;

    .line 2766426
    iget-object v1, v0, LX/1kZ;->f:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 2766427
    iget-object v1, v0, LX/1kZ;->a:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/1vU;->c:S

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1kZ;->f:Ljava/lang/Boolean;

    .line 2766428
    :cond_0
    iget-object v1, v0, LX/1kZ;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 2766429
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2766430
    :cond_1
    :goto_0
    return-void

    .line 2766431
    :cond_2
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->u(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0
.end method

.method public static u(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 7

    .prologue
    .line 2766406
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v1}, LX/18h;->c(Ljava/util/Collection;)I

    move-result v1

    .line 2766407
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "photo_picker_media_items_count"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v2

    .line 2766408
    iget-object v3, v0, LX/D1k;->a:LX/0if;

    sget-object v4, LX/0ig;->V:LX/0ih;

    const-string v5, "photo_picker_launched"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2766409
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->A:LX/1Ck;

    sget-object v1, LX/K43;->CHECK_RECENT_MEDIA:LX/K43;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2766410
    const/4 v0, 0x0

    .line 2766411
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->D:LX/1kZ;

    invoke-virtual {v1}, LX/1kZ;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2766412
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->D:LX/1kZ;

    invoke-virtual {v0}, LX/1kZ;->l()Ljava/lang/String;

    move-result-object v0

    .line 2766413
    :cond_0
    :goto_0
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->STORYLINE:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->o()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->d()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->m()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->a()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    const/4 v2, 0x3

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, LX/8AA;->b(II)LX/8AA;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/0Px;)LX/8AA;

    move-result-object v1

    .line 2766414
    iput-object v0, v1, LX/8AA;->i:Ljava/lang/String;

    .line 2766415
    move-object v0, v1

    .line 2766416
    invoke-static {p0, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2766417
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2766418
    return-void

    .line 2766419
    :cond_1
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->D:LX/1kZ;

    .line 2766420
    iget-object v2, v1, LX/1kZ;->g:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 2766421
    iget-object v2, v1, LX/1kZ;->a:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/1vU;->o:S

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/1kZ;->g:Ljava/lang/Boolean;

    .line 2766422
    :cond_2
    iget-object v2, v1, LX/1kZ;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 2766423
    if-eqz v1, :cond_0

    .line 2766424
    const v0, 0x7f083c3e

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static v(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2766378
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2766379
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->U:Lcom/facebook/storyline/fb4a/ui/TitleEditView;

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->setVisibility(I)V

    .line 2766380
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2766381
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2766382
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2766383
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->R:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2766384
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2766385
    :goto_0
    return-void

    .line 2766386
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2766387
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2766388
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2766389
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2766390
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2766479
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2766480
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->n$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766481
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2766482
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->U:Lcom/facebook/storyline/fb4a/ui/TitleEditView;

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->setVisibility(I)V

    .line 2766483
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2766484
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2766485
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->x()I

    move-result v1

    .line 2766486
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2766487
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766488
    return-void
.end method

.method private x()I
    .locals 2

    .prologue
    .line 2766489
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    if-nez v0, :cond_0

    .line 2766490
    const/4 v0, 0x0

    .line 2766491
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2766492
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2766493
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2766494
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2766495
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2766496
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->U:Lcom/facebook/storyline/fb4a/ui/TitleEditView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->setVisibility(I)V

    .line 2766497
    return-void
.end method

.method public static z(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 12

    .prologue
    .line 2766498
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-static {v0}, LX/18h;->c(Ljava/util/Collection;)I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 2766499
    :cond_0
    :goto_0
    return-void

    .line 2766500
    :cond_1
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    .line 2766501
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    iget-object v2, v0, Lcom/facebook/storyline/model/Mood;->cutdowns:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/storyline/model/Cutdown;

    .line 2766502
    iget v4, v5, Lcom/facebook/storyline/model/Cutdown;->e:I

    if-gt v4, v1, :cond_3

    iget v4, v5, Lcom/facebook/storyline/model/Cutdown;->f:I

    if-lt v4, v1, :cond_3

    .line 2766503
    new-instance v0, LX/K4q;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->aa:LX/0Px;

    iget-object v3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ab:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    const/high16 v6, 0x41f00000    # 30.0f

    sget-object v7, LX/K4p;->FB4A:LX/K4p;

    invoke-direct/range {v0 .. v7}, LX/K4q;-><init>(LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/storyline/model/Mood;Lcom/facebook/storyline/model/Cutdown;FLX/K4p;)V

    .line 2766504
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766505
    iget-object v8, v1, LX/K4o;->j:LX/K4q;

    invoke-static {v8, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2766506
    :cond_2
    :goto_2
    goto :goto_0

    .line 2766507
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2766508
    :cond_4
    iget-object v8, v1, LX/K4o;->p:LX/K4l;

    sget-object v9, LX/K4l;->READY_TO_PLAY:LX/K4l;

    if-eq v8, v9, :cond_5

    iget-object v8, v1, LX/K4o;->p:LX/K4l;

    sget-object v9, LX/K4l;->PLAYING:LX/K4l;

    if-eq v8, v9, :cond_5

    .line 2766509
    :goto_3
    iput-object v0, v1, LX/K4o;->j:LX/K4q;

    .line 2766510
    const-wide v8, 0x408f400000000000L    # 1000.0

    iget v10, v0, LX/K4q;->f:F

    float-to-double v10, v10

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    iput-wide v8, v1, LX/K4o;->A:J

    .line 2766511
    iget-object v8, v1, LX/K4o;->p:LX/K4l;

    sget-object v9, LX/K4l;->UNINITIALIZED:LX/K4l;

    if-eq v8, v9, :cond_2

    .line 2766512
    invoke-static {v1}, LX/K4o;->k(LX/K4o;)V

    goto :goto_2

    .line 2766513
    :cond_5
    invoke-virtual {v1}, LX/K4o;->d()V

    .line 2766514
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, LX/K4o;->a(F)V

    goto :goto_3
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 2766515
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2766516
    invoke-static {p0, p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2766517
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_storyline_entry_point"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2766518
    iget-object v2, v0, LX/D1k;->a:LX/0if;

    sget-object v3, LX/0ig;->V:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->a(LX/0ih;)V

    .line 2766519
    iget-object v2, v0, LX/D1k;->a:LX/0if;

    sget-object v3, LX/0ig;->V:LX/0ih;

    invoke-virtual {v2, v3, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2766520
    invoke-direct {p0, p1}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->d(Landroid/os/Bundle;)V

    .line 2766521
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->l()V

    .line 2766522
    const v0, 0x7f0313fb

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->setContentView(I)V

    .line 2766523
    const v0, 0x7f0d2def

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/ReactRootView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->G:Lcom/facebook/react/ReactRootView;

    .line 2766524
    const v0, 0x7f0d2df0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/ui/SquareTextureView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->H:Lcom/facebook/storyline/ui/SquareTextureView;

    .line 2766525
    const v0, 0x7f0d1adb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->J:Lcom/facebook/drawee/view/DraweeView;

    .line 2766526
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2766527
    const v0, 0x7f0d2df3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/ui/StorylineSeekBar;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->K:Lcom/facebook/storyline/ui/StorylineSeekBar;

    .line 2766528
    const v0, 0x7f0d0bec

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->L:Landroid/widget/ProgressBar;

    .line 2766529
    const v0, 0x7f0d2df7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    .line 2766530
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2766531
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    const v2, -0x777778

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2766532
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2766533
    const v0, 0x7f0d2df5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->T:Landroid/view/View;

    .line 2766534
    const v0, 0x7f0d2df6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Q:Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    .line 2766535
    const v0, 0x7f0d2dfb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/fb4a/ui/TitleEditView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->U:Lcom/facebook/storyline/fb4a/ui/TitleEditView;

    .line 2766536
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->U:Lcom/facebook/storyline/fb4a/ui/TitleEditView;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a(Ljava/lang/String;)V

    .line 2766537
    new-instance v0, LX/K3n;

    invoke-direct {v0, p0}, LX/K3n;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766538
    const v1, 0x7f0d2df9

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->R:Landroid/view/View;

    .line 2766539
    const v1, 0x7f0d2dfa

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->S:Landroid/view/View;

    .line 2766540
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766541
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->S:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766542
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->T:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766543
    const v0, 0x7f0d2df8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->V:LX/4ob;

    .line 2766544
    const v0, 0x7f0d2df1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    .line 2766545
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    new-instance v1, LX/K3o;

    invoke-direct {v1, p0}, LX/K3o;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->setButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766546
    const v0, 0x7f0d2df2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/ui/StorylineAudioIndicator;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->O:Lcom/facebook/storyline/ui/StorylineAudioIndicator;

    .line 2766547
    const v0, 0x7f0d2df4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->W:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2766548
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->W:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/K3p;

    invoke-direct {v1, p0}, LX/K3p;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766549
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-direct {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Ljava/util/List;)V

    .line 2766550
    const v0, 0x7f0d0541

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2766551
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->r()V

    .line 2766552
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    new-instance v1, LX/K42;

    invoke-direct {v1, p0}, LX/K42;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766553
    iput-object v1, v0, LX/K4o;->i:LX/K42;

    .line 2766554
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->H:Lcom/facebook/storyline/ui/SquareTextureView;

    new-instance v1, LX/K44;

    invoke-direct {v1, p0}, LX/K44;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/ui/SquareTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2766555
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-direct {v1, v5, v5}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2766556
    new-instance v0, LX/K3z;

    invoke-direct {v0, p0}, LX/K3z;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    .line 2766557
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ae:LX/K3z;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2766558
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->I:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/K5E;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b26f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b26f7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/K5E;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2766559
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->K:Lcom/facebook/storyline/ui/StorylineSeekBar;

    new-instance v1, LX/K41;

    invoke-direct {v1, p0}, LX/K41;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/ui/StorylineSeekBar;->setListener(LX/K40;)V

    .line 2766560
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->J:Lcom/facebook/drawee/view/DraweeView;

    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f0a0120

    invoke-virtual {v1, v2}, LX/1Uo;->b(I)LX/1Uo;

    move-result-object v1

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    .line 2766561
    iput v5, v1, LX/1Uo;->d:I

    .line 2766562
    move-object v1, v1

    .line 2766563
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2766564
    new-instance v0, LX/K5D;

    const-string v1, "StorylineBlur"

    invoke-direct {v0, p0, v1}, LX/K5D;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ad:LX/K5D;

    .line 2766565
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->G:Lcom/facebook/react/ReactRootView;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->r:LX/33u;

    invoke-virtual {v1}, LX/33u;->c()LX/33y;

    move-result-object v1

    const-string v2, "StorylineNarrativeEngineApp"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/react/ReactRootView;->a(LX/33y;Ljava/lang/String;)V

    .line 2766566
    new-instance v0, LX/4nG;

    const v1, 0x1020002

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, v5}, LX/4nG;-><init>(Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ai:LX/4nG;

    .line 2766567
    new-instance v0, LX/K3q;

    invoke-direct {v0, p0}, LX/K3q;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->aj:LX/4nF;

    .line 2766568
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Q:Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    new-instance v1, LX/K3r;

    invoke-direct {v1, p0}, LX/K3r;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->setTabClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766569
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->m(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)Z

    .line 2766570
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->n$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766571
    sget-object v0, LX/K4N;->PHOTO:LX/K4N;

    invoke-static {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/K4N;)V

    .line 2766572
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->t()V

    .line 2766573
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 2766574
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2766575
    packed-switch p1, :pswitch_data_0

    .line 2766576
    :goto_0
    return-void

    .line 2766577
    :pswitch_0
    if-eq p2, v0, :cond_0

    .line 2766578
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766579
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object p0, LX/0ig;->V:LX/0ih;

    const-string p1, "photo_picker_canceled"

    invoke-virtual {v1, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766580
    goto :goto_0

    .line 2766581
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2766582
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2766583
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    .line 2766584
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(LX/0Px;)V

    .line 2766585
    :cond_1
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v1}, LX/18h;->c(Ljava/util/Collection;)I

    move-result v1

    .line 2766586
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "photo_picker_media_items_count"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v2

    .line 2766587
    iget-object v3, v0, LX/D1k;->a:LX/0if;

    sget-object p1, LX/0ig;->V:LX/0ih;

    const-string p2, "photo_picker_succeeded"

    const/4 p3, 0x0

    invoke-virtual {v3, p1, p2, p3, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2766588
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->v(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    goto :goto_0

    .line 2766589
    :pswitch_1
    if-eq p2, v0, :cond_2

    .line 2766590
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766591
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string v3, "friends_picker_canceled"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766592
    goto :goto_0

    .line 2766593
    :cond_2
    if-eqz p3, :cond_3

    const-string v0, "full_profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2766594
    const-string v0, "full_profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2766595
    invoke-direct {p0, v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a(Ljava/util/List;)V

    .line 2766596
    :cond_3
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-static {v1}, LX/18h;->c(Ljava/util/Collection;)I

    move-result v1

    .line 2766597
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "friends_picker_friends_count"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v2

    .line 2766598
    iget-object v3, v0, LX/D1k;->a:LX/0if;

    sget-object p0, LX/0ig;->V:LX/0ih;

    const-string p1, "friends_picker_succeeded"

    const/4 p2, 0x0

    invoke-virtual {v3, p0, p1, p2, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2766599
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x22

    const v1, 0x913631a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2766600
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->A:LX/1Ck;

    sget-object v2, LX/K43;->CHECK_RECENT_MEDIA:LX/K43;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2766601
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->H:Lcom/facebook/storyline/ui/SquareTextureView;

    invoke-virtual {v1, v3}, Lcom/facebook/storyline/ui/SquareTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2766602
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->S:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766603
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    invoke-virtual {v1, v3}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->setButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766604
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Q:Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    invoke-virtual {v1, v3}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->setTabClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766605
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->G:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v1}, Lcom/facebook/react/ReactRootView;->a()V

    .line 2766606
    iput-object v3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->G:Lcom/facebook/react/ReactRootView;

    .line 2766607
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766608
    iput-object v3, v1, LX/K4o;->i:LX/K42;

    .line 2766609
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    const/4 v6, 0x0

    .line 2766610
    iget-object v2, v1, LX/K4o;->p:LX/K4l;

    sget-object v5, LX/K4l;->DESTROYED:LX/K4l;

    if-ne v2, v5, :cond_0

    .line 2766611
    :goto_0
    iput-object v3, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    .line 2766612
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766613
    iget-object v2, v1, LX/D1k;->a:LX/0if;

    sget-object v3, LX/0ig;->V:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->c(LX/0ih;)V

    .line 2766614
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2766615
    const/16 v1, 0x23

    const v2, 0x2491e8de

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2766616
    :cond_0
    invoke-virtual {v1}, LX/K4o;->f()V

    .line 2766617
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/K4o;->u:Z

    .line 2766618
    iget-object v2, v1, LX/K4o;->b:LX/K4W;

    const/4 v8, 0x0

    .line 2766619
    iget-object v5, v2, LX/K4W;->g:LX/33y;

    iget-object v7, v2, LX/K4W;->j:LX/9mm;

    invoke-virtual {v5, v7}, LX/33y;->b(LX/9mm;)V

    .line 2766620
    iput-object v8, v2, LX/K4W;->j:LX/9mm;

    .line 2766621
    iput-object v8, v2, LX/K4W;->f:Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    .line 2766622
    iput-object v8, v2, LX/K4W;->h:LX/K4k;

    .line 2766623
    iget-object v5, v2, LX/K4W;->c:LX/K4Z;

    iget-object v7, v2, LX/K4W;->i:LX/K4V;

    .line 2766624
    iget-object v9, v5, LX/K4Z;->a:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2766625
    iput-object v8, v2, LX/K4W;->i:LX/K4V;

    .line 2766626
    iget-object v5, v2, LX/K4W;->g:LX/33y;

    invoke-virtual {v5}, LX/33y;->j()V

    .line 2766627
    iput-object v8, v2, LX/K4W;->g:LX/33y;

    .line 2766628
    iget-object v2, v1, LX/K4o;->f:LX/K4g;

    sget-object v5, LX/K4d;->QUIT_THREAD:LX/K4d;

    invoke-virtual {v2, v5}, LX/K4g;->a(LX/K4d;)Z

    .line 2766629
    iput-object v6, v1, LX/K4o;->f:LX/K4g;

    .line 2766630
    iput-object v6, v1, LX/K4o;->g:Landroid/os/Handler;

    .line 2766631
    sget-object v2, LX/K4l;->DESTROYED:LX/K4l;

    invoke-static {v1, v2}, LX/K4o;->a$redex0(LX/K4o;LX/K4l;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1e4037e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2766632
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2766633
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ac:Z

    .line 2766634
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v1}, LX/K4o;->d()V

    .line 2766635
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ai:LX/4nG;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->aj:LX/4nF;

    invoke-virtual {v1, v2}, LX/4nG;->b(LX/4nF;)V

    .line 2766636
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2766637
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->dismiss()V

    .line 2766638
    :cond_0
    const/16 v1, 0x23

    const v2, 0x1f6ebfbe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x24edc0b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2766639
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2766640
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ac:Z

    .line 2766641
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ai:LX/4nG;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->aj:LX/4nF;

    invoke-virtual {v1, v2}, LX/4nG;->a(LX/4nF;)V

    .line 2766642
    invoke-static {p0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->z(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766643
    const/16 v1, 0x23

    const v2, 0x2ad8df4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2766644
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2766645
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ab:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2766646
    const-string v0, "extra_storyline_title"

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2766647
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    if-eqz v0, :cond_1

    .line 2766648
    const-string v0, "extra_storyline_selected_mood"

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2766649
    :cond_1
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2766650
    const-string v0, "extra_storyline_moods"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2766651
    :cond_2
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2766652
    const-string v0, "extra_storyline_media_items"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Z:LX/0Px;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2766653
    :cond_3
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2766654
    const-string v0, "extra_storyline_selected_profiles"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->al:LX/0Px;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2766655
    :cond_4
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xcb43b60

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2766656
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2766657
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->H:Lcom/facebook/storyline/ui/SquareTextureView;

    invoke-virtual {v1}, Lcom/facebook/storyline/ui/SquareTextureView;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2766658
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->H:Lcom/facebook/storyline/ui/SquareTextureView;

    invoke-virtual {v2}, Lcom/facebook/storyline/ui/SquareTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/K4o;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2766659
    :cond_0
    const/16 v1, 0x23

    const v2, -0x446e0f0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x66ed67c9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2766660
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v1}, LX/K4o;->f()V

    .line 2766661
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2766662
    const/16 v1, 0x23

    const v2, -0x1b7f4701

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
