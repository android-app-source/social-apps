.class public Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Lcom/facebook/drawee/view/DraweeView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/drawee/view/DraweeView",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/view/View;

.field public final e:Lcom/facebook/fbui/glyph/GlyphView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;

.field public final i:Landroid/view/View;

.field private final j:Landroid/graphics/drawable/Drawable;

.field private final k:LX/1Ad;

.field private final l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2767836
    const-class v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Ad;)V
    .locals 3

    .prologue
    .line 2767837
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2767838
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2767839
    iput-object p2, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->k:LX/1Ad;

    .line 2767840
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setOrientation(I)V

    .line 2767841
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setGravity(I)V

    .line 2767842
    const v0, 0x7f0313fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2767843
    const v0, 0x7f0d2dfc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->b:Landroid/view/View;

    .line 2767844
    const v0, 0x7f0d1adb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->c:Lcom/facebook/drawee/view/DraweeView;

    .line 2767845
    const v0, 0x7f0d2e00    # 1.8766E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->d:Landroid/view/View;

    .line 2767846
    const v0, 0x7f0d0c97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2767847
    const v0, 0x7f0d2e01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->f:Landroid/widget/TextView;

    .line 2767848
    const v0, 0x7f0d2dfe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->g:Landroid/widget/TextView;

    .line 2767849
    const v0, 0x7f0d2dff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->h:Landroid/view/View;

    .line 2767850
    const v0, 0x7f0d2dfd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->i:Landroid/view/View;

    .line 2767851
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b26f9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->l:I

    .line 2767852
    new-instance v0, LX/1Uo;

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2767853
    const v2, 0x7f0a09bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->j:Landroid/graphics/drawable/Drawable;

    .line 2767854
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->c:Lcom/facebook/drawee/view/DraweeView;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->j:Landroid/graphics/drawable/Drawable;

    .line 2767855
    iput-object v2, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2767856
    move-object v0, v0

    .line 2767857
    const/4 v2, 0x0

    .line 2767858
    iput v2, v0, LX/1Uo;->d:I

    .line 2767859
    move-object v0, v0

    .line 2767860
    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2767861
    return-void
.end method

.method public static a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 2767862
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    iget v2, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->l:I

    iget v3, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->l:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 2767863
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 2767864
    move-object v0, v0

    .line 2767865
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2767866
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->c:Lcom/facebook/drawee/view/DraweeView;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->k:LX/1Ad;

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->c:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2767867
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->c:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2767868
    return-void
.end method

.method public static a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2767869
    if-eqz p1, :cond_0

    .line 2767870
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2767871
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2767872
    :goto_0
    return-void

    .line 2767873
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2767874
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public setSelected(Z)V
    .locals 2

    .prologue
    .line 2767875
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setSelected(Z)V

    .line 2767876
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->h:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2767877
    return-void

    .line 2767878
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
