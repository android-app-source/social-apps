.class public Lcom/facebook/storyline/fb4a/ui/TitleEditView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/ui/search/SearchEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2767967
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2767968
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a()V

    .line 2767969
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2767964
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2767965
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a()V

    .line 2767966
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2767954
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2767955
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a()V

    .line 2767956
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2767960
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->setOrientation(I)V

    .line 2767961
    const v0, 0x7f0313ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2767962
    const v0, 0x7f0d2e02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a:Lcom/facebook/ui/search/SearchEditText;

    .line 2767963
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2767958
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2767959
    return-void
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2767957
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TitleEditView;->a:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
