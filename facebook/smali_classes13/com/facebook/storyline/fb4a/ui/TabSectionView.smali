.class public Lcom/facebook/storyline/fb4a/ui/TabSectionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/K4N;",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/K4N;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0wd;

.field private g:F

.field private h:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2767892
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2767893
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a()V

    .line 2767894
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2767895
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2767896
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a()V

    .line 2767897
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2767898
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2767899
    invoke-direct {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a()V

    .line 2767900
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2767901
    const-class v1, Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    invoke-static {v1, p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2767902
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    .line 2767903
    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->g:F

    .line 2767904
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, LX/K4N;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->e:Ljava/util/Map;

    .line 2767905
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->e:Ljava/util/Map;

    sget-object v2, LX/K4N;->PHOTO:LX/K4N;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2767906
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->e:Ljava/util/Map;

    sget-object v2, LX/K4N;->MUSIC:LX/K4N;

    iget v3, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->g:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2767907
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->e:Ljava/util/Map;

    sget-object v2, LX/K4N;->TITLE:LX/K4N;

    iget v3, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->g:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2767908
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->c:Landroid/graphics/Paint;

    .line 2767909
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a008a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2767910
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b26f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->h:F

    .line 2767911
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->b:Landroid/graphics/Paint;

    .line 2767912
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a009f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2767913
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, LX/K4N;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->d:Ljava/util/Map;

    .line 2767914
    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->setOrientation(I)V

    .line 2767915
    invoke-static {}, LX/K4N;->values()[LX/K4N;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2767916
    invoke-direct {p0, v3}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->b(LX/K4N;)V

    .line 2767917
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2767918
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a:LX/0wW;

    return-void
.end method

.method private b(LX/K4N;)V
    .locals 4

    .prologue
    .line 2767919
    new-instance v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2767920
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTag(Ljava/lang/Object;)V

    .line 2767921
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setGravity(I)V

    .line 2767922
    sget-object v1, LX/K4M;->a:[I

    invoke-virtual {p1}, LX/K4N;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2767923
    :goto_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2767924
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2767925
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2767926
    invoke-virtual {p0, v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->addView(Landroid/view/View;)V

    .line 2767927
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2767928
    return-void

    .line 2767929
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083c30

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2767930
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083c31

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2767931
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083c32

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/K4N;)V
    .locals 6

    .prologue
    .line 2767932
    invoke-static {}, LX/K4N;->values()[LX/K4N;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v3, v2

    .line 2767933
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getContext()Landroid/content/Context;

    move-result-object v5

    if-ne v1, p1, :cond_0

    const v1, 0x7f0e0c6c

    :goto_1
    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2767934
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2767935
    :cond_0
    const v1, 0x7f0e0c6d

    goto :goto_1

    .line 2767936
    :cond_1
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->f:LX/0wd;

    if-nez v0, :cond_2

    .line 2767937
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide v2, 0x4051800000000000L    # 70.0

    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 2767938
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2767939
    move-object v1, v0

    .line 2767940
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    new-instance v1, LX/K4L;

    invoke-direct {v1, p0}, LX/K4L;-><init>(Lcom/facebook/storyline/fb4a/ui/TabSectionView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->f:LX/0wd;

    .line 2767941
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->invalidate()V

    .line 2767942
    :goto_2
    return-void

    .line 2767943
    :cond_2
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->f:LX/0wd;

    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_2
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2767944
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2767945
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2767946
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v7, v0, v4

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getWidth()I

    move-result v0

    int-to-float v8, v0

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getHeight()I

    move-result v0

    int-to-float v9, v0

    iget-object v10, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->b:Landroid/graphics/Paint;

    move-object v5, p1

    move v6, v1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2767947
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->f:LX/0wd;

    if-eqz v0, :cond_0

    .line 2767948
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->f:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v1, v0

    .line 2767949
    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->h:F

    sub-float/2addr v0, v2

    sub-float v2, v0, v4

    iget v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->g:F

    add-float v3, v1, v0

    invoke-virtual {p0}, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2767950
    :cond_0
    return-void
.end method

.method public setTabClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2767951
    iget-object v0, p0, Lcom/facebook/storyline/fb4a/ui/TabSectionView;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2767952
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2767953
    :cond_0
    return-void
.end method
