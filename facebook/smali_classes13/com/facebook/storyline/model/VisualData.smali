.class public Lcom/facebook/storyline/model/VisualData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/VisualDataDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final additionalTransitions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "additionalTransitions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/VisualData$Effect;",
            ">;"
        }
    .end annotation
.end field

.field public final beginningEffect:Lcom/facebook/storyline/model/VisualData$Effect;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "beginningEffect"
    .end annotation
.end field

.field public final endingEffect:Lcom/facebook/storyline/model/VisualData$Effect;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "endingEffect"
    .end annotation
.end field

.field public final itemEffect:Lcom/facebook/storyline/model/VisualData$Effect;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "itemEffect"
    .end annotation
.end field

.field public final primaryTransitions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primaryTransitions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/VisualData$Effect;",
            ">;"
        }
    .end annotation
.end field

.field public final secondaryTransitions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondaryTransitions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/VisualData$Effect;",
            ">;"
        }
    .end annotation
.end field

.field public final tertiaryTransitions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tertiaryTransitions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/VisualData$Effect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768297
    const-class v0, Lcom/facebook/storyline/model/VisualDataDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2768298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768299
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->beginningEffect:Lcom/facebook/storyline/model/VisualData$Effect;

    .line 2768300
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->itemEffect:Lcom/facebook/storyline/model/VisualData$Effect;

    .line 2768301
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->endingEffect:Lcom/facebook/storyline/model/VisualData$Effect;

    .line 2768302
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->primaryTransitions:LX/0Px;

    .line 2768303
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->secondaryTransitions:LX/0Px;

    .line 2768304
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->tertiaryTransitions:LX/0Px;

    .line 2768305
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData;->additionalTransitions:LX/0Px;

    .line 2768306
    return-void
.end method
