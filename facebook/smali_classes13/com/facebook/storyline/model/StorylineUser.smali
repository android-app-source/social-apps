.class public final Lcom/facebook/storyline/model/StorylineUser;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/storyline/model/StorylineUser;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768282
    new-instance v0, LX/K4T;

    invoke-direct {v0}, LX/K4T;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/StorylineUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2768270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768271
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    .line 2768272
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    .line 2768273
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    .line 2768274
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    .line 2768275
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2768276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768277
    iput-object p1, p0, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    .line 2768278
    iput-object p2, p0, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    .line 2768279
    iput-object p3, p0, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    .line 2768280
    iput-object p4, p0, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    .line 2768281
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2768269
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2768263
    if-ne p0, p1, :cond_1

    .line 2768264
    :cond_0
    :goto_0
    return v0

    .line 2768265
    :cond_1
    instance-of v2, p1, Lcom/facebook/storyline/model/StorylineUser;

    if-nez v2, :cond_2

    move v0, v1

    .line 2768266
    goto :goto_0

    .line 2768267
    :cond_2
    check-cast p1, Lcom/facebook/storyline/model/StorylineUser;

    .line 2768268
    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2768262
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2768257
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768258
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768259
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768260
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylineUser;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2768261
    return-void
.end method
