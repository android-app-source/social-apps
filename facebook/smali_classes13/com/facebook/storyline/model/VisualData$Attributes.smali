.class public final Lcom/facebook/storyline/model/VisualData$Attributes;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/VisualData_AttributesDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final alignNextEffectToCurrentHalf:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "alignNextEffectToCurrentHalf"
    .end annotation
.end field

.field public final alignPreviousEffectToCurrentEnd:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "alignPreviousEffectToCurrentEnd"
    .end annotation
.end field

.field public final alignPreviousEffectToCurrentHalf:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "alignPreviousEffectToCurrentHalf"
    .end annotation
.end field

.field public final overrideNext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "overrideNext"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768283
    const-class v0, Lcom/facebook/storyline/model/VisualData_AttributesDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2768284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/storyline/model/VisualData$Attributes;->overrideNext:Ljava/lang/String;

    .line 2768286
    iput-boolean v1, p0, Lcom/facebook/storyline/model/VisualData$Attributes;->alignPreviousEffectToCurrentHalf:Z

    .line 2768287
    iput-boolean v1, p0, Lcom/facebook/storyline/model/VisualData$Attributes;->alignPreviousEffectToCurrentEnd:Z

    .line 2768288
    iput-boolean v1, p0, Lcom/facebook/storyline/model/VisualData$Attributes;->alignNextEffectToCurrentHalf:Z

    .line 2768289
    return-void
.end method
