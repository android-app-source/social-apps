.class public Lcom/facebook/storyline/model/VisualMood;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/VisualMoodDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final visualData:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "visual_data"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768382
    const-class v0, Lcom/facebook/storyline/model/VisualMoodDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2768383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768384
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualMood;->id:Ljava/lang/String;

    .line 2768385
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualMood;->name:Ljava/lang/String;

    .line 2768386
    iput-object v0, p0, Lcom/facebook/storyline/model/VisualMood;->visualData:Ljava/lang/String;

    .line 2768387
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2768388
    if-ne p0, p1, :cond_0

    .line 2768389
    const/4 v0, 0x1

    .line 2768390
    :goto_0
    return v0

    .line 2768391
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/facebook/storyline/model/Mood;

    if-nez v0, :cond_2

    .line 2768392
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2768393
    :cond_2
    check-cast p1, Lcom/facebook/storyline/model/Mood;

    .line 2768394
    iget-object v0, p0, Lcom/facebook/storyline/model/VisualMood;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2768395
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/storyline/model/VisualMood;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2768396
    const-string v0, "id: %s, name: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/storyline/model/VisualMood;->id:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/storyline/model/VisualMood;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
