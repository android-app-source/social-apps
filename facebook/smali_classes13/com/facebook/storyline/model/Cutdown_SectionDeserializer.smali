.class public Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2768065
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    new-instance v1, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;

    invoke-direct {v1}, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2768066
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2768067
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 2768068
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 2768069
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 3

    .prologue
    .line 2768070
    const-class v1, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;

    monitor-enter v1

    .line 2768071
    :try_start_0
    sget-object v0, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 2768072
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2768073
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2768074
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    .line 2768075
    :goto_1
    return-object v0

    .line 2768076
    :cond_2
    sget-object v0, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 2768077
    if-eqz v0, :cond_0

    .line 2768078
    monitor-exit v1

    goto :goto_1

    .line 2768079
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2768080
    :sswitch_0
    :try_start_3
    const-string v2, "subdivisions"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "startTime"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "preferredSubdivision"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "imageEffect"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "fillFrom"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "rank"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "isOptional"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x6

    goto :goto_0

    .line 2768081
    :pswitch_0
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "subdivisions"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const-class v2, Ljava/lang/Integer;

    invoke-static {v0, v2}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2768082
    :goto_2
    :try_start_4
    sget-object v2, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2768083
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 2768084
    :pswitch_1
    :try_start_5
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "startTime"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 2768085
    :pswitch_2
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "preferredSubdivision"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 2768086
    :pswitch_3
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "imageEffect"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 2768087
    :pswitch_4
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "fillFrom"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 2768088
    :pswitch_5
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "rank"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 2768089
    :pswitch_6
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    const-string v2, "isOptional"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 2768090
    :catch_0
    move-exception v0

    .line 2768091
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7eea75b1 -> :sswitch_1
        -0x58a6d0da -> :sswitch_0
        -0x33fe0d56 -> :sswitch_6
        -0x2b7af833 -> :sswitch_4
        0x354c2c -> :sswitch_5
        0x13ac1fac -> :sswitch_2
        0x68fa9ecc -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
