.class public final Lcom/facebook/storyline/model/Cutdown$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/storyline/model/Cutdown;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2767973
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    return-void
.end method

.method private static a(LX/15w;)Lcom/facebook/storyline/model/Cutdown;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 2767974
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v9

    move v6, v8

    move v5, v8

    move-object v4, v7

    move-object v3, v7

    move v2, v8

    move-object v1, v7

    .line 2767975
    :cond_0
    :goto_0
    invoke-static {p0}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v0

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v10, :cond_2

    .line 2767976
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v10, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v10, :cond_0

    .line 2767977
    invoke-static {p0}, LX/1Xh;->a(LX/15w;)LX/15z;

    .line 2767978
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    const/4 v0, -0x1

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2767979
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 2767980
    :sswitch_0
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move v0, v8

    goto :goto_1

    :sswitch_1
    const-string v11, "duration"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v11, "audio_streaming_url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v11, "audio_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v11, "min_photos"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v11, "max_photos"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v11, "timing_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v0, 0x6

    goto :goto_1

    .line 2767981
    :pswitch_0
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2767982
    :pswitch_1
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    goto :goto_0

    .line 2767983
    :pswitch_2
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2767984
    :pswitch_3
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2767985
    :pswitch_4
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto/16 :goto_0

    .line 2767986
    :pswitch_5
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_0

    .line 2767987
    :pswitch_6
    iget-object v0, v9, LX/0lC;->_typeFactory:LX/0li;

    move-object v0, v0

    .line 2767988
    const-class v7, LX/0Px;

    const-class v10, Lcom/facebook/storyline/model/Cutdown$Section;

    invoke-virtual {v0, v7, v10}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;

    move-result-object v0

    .line 2767989
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7, v0}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    move-object v7, v0

    .line 2767990
    goto/16 :goto_0

    .line 2767991
    :cond_2
    new-instance v0, Lcom/facebook/storyline/model/Cutdown;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/storyline/model/Cutdown;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IILX/0Px;)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x76bbb26c -> :sswitch_1
        -0x402ef497 -> :sswitch_2
        -0x29a6a981 -> :sswitch_6
        -0x236b80f2 -> :sswitch_4
        0xd1b -> :sswitch_0
        0x3a2b3e24 -> :sswitch_3
        0x493cc7bc -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2767992
    invoke-static {p1}, Lcom/facebook/storyline/model/Cutdown$Deserializer;->a(LX/15w;)Lcom/facebook/storyline/model/Cutdown;

    move-result-object v0

    return-object v0
.end method
