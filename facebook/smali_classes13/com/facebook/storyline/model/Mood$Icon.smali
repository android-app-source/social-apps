.class public final Lcom/facebook/storyline/model/Mood$Icon;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/Mood_IconDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/storyline/model/Mood$Icon;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final height:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height"
    .end annotation
.end field

.field public final uri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation
.end field

.field public final width:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768098
    const-class v0, Lcom/facebook/storyline/model/Mood_IconDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768099
    new-instance v0, LX/K4R;

    invoke-direct {v0}, LX/K4R;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/Mood$Icon;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2768100
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/facebook/storyline/model/Mood$Icon;-><init>(Ljava/lang/String;II)V

    .line 2768101
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2768102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood$Icon;->uri:Ljava/lang/String;

    .line 2768104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Mood$Icon;->width:I

    .line 2768105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Mood$Icon;->height:I

    .line 2768106
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2768107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768108
    iput-object p1, p0, Lcom/facebook/storyline/model/Mood$Icon;->uri:Ljava/lang/String;

    .line 2768109
    iput p2, p0, Lcom/facebook/storyline/model/Mood$Icon;->width:I

    .line 2768110
    iput p3, p0, Lcom/facebook/storyline/model/Mood$Icon;->height:I

    .line 2768111
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2768112
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2768113
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood$Icon;->uri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768114
    iget v0, p0, Lcom/facebook/storyline/model/Mood$Icon;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768115
    iget v0, p0, Lcom/facebook/storyline/model/Mood$Icon;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768116
    return-void
.end method
