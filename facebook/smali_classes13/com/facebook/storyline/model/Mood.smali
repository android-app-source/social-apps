.class public Lcom/facebook/storyline/model/Mood;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/MoodDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/storyline/model/Mood;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/storyline/model/VisualData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field private c:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final cutdowns:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cutdowns"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Cutdown;",
            ">;"
        }
    .end annotation
.end field

.field public final defaultVisualMoodId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "default_visual_mood_id"
    .end annotation
.end field

.field public final icon:Lcom/facebook/storyline/model/Mood$Icon;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "button_icon"
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final isNew:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_new"
    .end annotation
.end field

.field public final moodAssetsString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mood_assets"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768159
    const-class v0, Lcom/facebook/storyline/model/MoodDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768158
    new-instance v0, LX/K4Q;

    invoke-direct {v0}, LX/K4Q;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/Mood;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2768156
    const/4 v3, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/storyline/model/Mood;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/storyline/model/Mood$Icon;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V

    .line 2768157
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2768147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    .line 2768149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood;->name:Ljava/lang/String;

    .line 2768150
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/storyline/model/Mood;->isNew:Z

    .line 2768151
    const-class v0, Lcom/facebook/storyline/model/Mood;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyline/model/Mood$Icon;

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood;->icon:Lcom/facebook/storyline/model/Mood$Icon;

    .line 2768152
    const-class v0, Lcom/facebook/storyline/model/Cutdown;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood;->cutdowns:LX/0Px;

    .line 2768153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood;->defaultVisualMoodId:Ljava/lang/String;

    .line 2768154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Mood;->moodAssetsString:Ljava/lang/String;

    .line 2768155
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/storyline/model/Mood$Icon;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/storyline/model/Mood$Icon;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/storyline/model/Mood$Icon;",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Cutdown;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2768138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768139
    iput-object p1, p0, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    .line 2768140
    iput-object p2, p0, Lcom/facebook/storyline/model/Mood;->name:Ljava/lang/String;

    .line 2768141
    iput-boolean p3, p0, Lcom/facebook/storyline/model/Mood;->isNew:Z

    .line 2768142
    iput-object p4, p0, Lcom/facebook/storyline/model/Mood;->icon:Lcom/facebook/storyline/model/Mood$Icon;

    .line 2768143
    iput-object p5, p0, Lcom/facebook/storyline/model/Mood;->cutdowns:LX/0Px;

    .line 2768144
    iput-object p6, p0, Lcom/facebook/storyline/model/Mood;->defaultVisualMoodId:Ljava/lang/String;

    .line 2768145
    iput-object p7, p0, Lcom/facebook/storyline/model/Mood;->moodAssetsString:Ljava/lang/String;

    .line 2768146
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/storyline/model/VisualData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2768117
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->a:Lcom/facebook/storyline/model/VisualData;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2768160
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/0P1;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2768137
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->c:LX/0P1;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2768136
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2768129
    if-ne p0, p1, :cond_0

    .line 2768130
    const/4 v0, 0x1

    .line 2768131
    :goto_0
    return v0

    .line 2768132
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/facebook/storyline/model/Mood;

    if-nez v0, :cond_2

    .line 2768133
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2768134
    :cond_2
    check-cast p1, Lcom/facebook/storyline/model/Mood;

    .line 2768135
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2768128
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2768127
    const-string v1, "id: %s, name: %s, %d cutdowns"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/storyline/model/Mood;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/facebook/storyline/model/Mood;->cutdowns:LX/0Px;

    if-nez v4, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->cutdowns:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2768118
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768119
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768120
    iget-boolean v0, p0, Lcom/facebook/storyline/model/Mood;->isNew:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2768121
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->icon:Lcom/facebook/storyline/model/Mood$Icon;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2768122
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->cutdowns:LX/0Px;

    .line 2768123
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2768124
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->defaultVisualMoodId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768125
    iget-object v0, p0, Lcom/facebook/storyline/model/Mood;->moodAssetsString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768126
    return-void
.end method
