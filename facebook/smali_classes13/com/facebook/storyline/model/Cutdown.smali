.class public Lcom/facebook/storyline/model/Cutdown;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/Cutdown$Deserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/storyline/model/Cutdown;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public final f:I

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Cutdown$Section;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768027
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Deserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768064
    new-instance v0, LX/K4O;

    invoke-direct {v0}, LX/K4O;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/Cutdown;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2768055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768056
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    .line 2768057
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown;->b:I

    .line 2768058
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->c:Ljava/lang/String;

    .line 2768059
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->d:Ljava/lang/String;

    .line 2768060
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown;->e:I

    .line 2768061
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown;->f:I

    .line 2768062
    const-class v0, Lcom/facebook/storyline/model/Cutdown$Section;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->g:LX/0Px;

    .line 2768063
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IILX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "LX/0Px",
            "<",
            "Lcom/facebook/storyline/model/Cutdown$Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2768046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768047
    iput-object p1, p0, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    .line 2768048
    iput p2, p0, Lcom/facebook/storyline/model/Cutdown;->b:I

    .line 2768049
    iput-object p3, p0, Lcom/facebook/storyline/model/Cutdown;->c:Ljava/lang/String;

    .line 2768050
    iput-object p4, p0, Lcom/facebook/storyline/model/Cutdown;->d:Ljava/lang/String;

    .line 2768051
    iput p5, p0, Lcom/facebook/storyline/model/Cutdown;->e:I

    .line 2768052
    iput p6, p0, Lcom/facebook/storyline/model/Cutdown;->f:I

    .line 2768053
    iput-object p7, p0, Lcom/facebook/storyline/model/Cutdown;->g:LX/0Px;

    .line 2768054
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2768045
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2768038
    if-ne p0, p1, :cond_0

    .line 2768039
    const/4 v0, 0x1

    .line 2768040
    :goto_0
    return v0

    .line 2768041
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/facebook/storyline/model/Cutdown;

    if-nez v0, :cond_2

    .line 2768042
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2768043
    :cond_2
    check-cast p1, Lcom/facebook/storyline/model/Cutdown;

    .line 2768044
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2768037
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2768028
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768029
    iget v0, p0, Lcom/facebook/storyline/model/Cutdown;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768030
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768031
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768032
    iget v0, p0, Lcom/facebook/storyline/model/Cutdown;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768033
    iget v0, p0, Lcom/facebook/storyline/model/Cutdown;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768034
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown;->g:LX/0Px;

    .line 2768035
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2768036
    return-void
.end method
