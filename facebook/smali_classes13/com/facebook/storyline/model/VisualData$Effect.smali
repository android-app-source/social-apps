.class public final Lcom/facebook/storyline/model/VisualData$Effect;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/VisualData_EffectDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final attributes:Lcom/facebook/storyline/model/VisualData$Attributes;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attributes"
    .end annotation
.end field

.field public final duration:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "duration"
    .end annotation
.end field

.field public final durationInCutDown:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "durationInCutDown"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768290
    const-class v0, Lcom/facebook/storyline/model/VisualData_EffectDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2768291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768292
    iput-object v1, p0, Lcom/facebook/storyline/model/VisualData$Effect;->name:Ljava/lang/String;

    .line 2768293
    iput v0, p0, Lcom/facebook/storyline/model/VisualData$Effect;->duration:I

    .line 2768294
    iput v0, p0, Lcom/facebook/storyline/model/VisualData$Effect;->durationInCutDown:I

    .line 2768295
    iput-object v1, p0, Lcom/facebook/storyline/model/VisualData$Effect;->attributes:Lcom/facebook/storyline/model/VisualData$Attributes;

    .line 2768296
    return-void
.end method
