.class public final Lcom/facebook/storyline/model/StorylinePhoto;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/storyline/model/StorylinePhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:Landroid/net/Uri;

.field public final e:Landroid/net/Uri;

.field public final f:D

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768214
    new-instance v0, LX/K4S;

    invoke-direct {v0}, LX/K4S;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/StorylinePhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2768215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    .line 2768217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    .line 2768218
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    .line 2768219
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    .line 2768220
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    .line 2768221
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    .line 2768222
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 2768223
    sget-object v1, Landroid/graphics/RectF;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2768224
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    .line 2768225
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    .line 2768226
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILandroid/net/Uri;Landroid/net/Uri;DLX/0Px;Landroid/graphics/RectF;)V
    .locals 0
    .param p8    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "D",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Landroid/graphics/RectF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2768227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768228
    iput-object p1, p0, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    .line 2768229
    iput p2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    .line 2768230
    iput p3, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    .line 2768231
    iput-object p4, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    .line 2768232
    iput-object p5, p0, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    .line 2768233
    iput-wide p6, p0, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    .line 2768234
    iput-object p8, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    .line 2768235
    iput-object p9, p0, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    .line 2768236
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2768237
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2768238
    if-ne p0, p1, :cond_1

    .line 2768239
    :cond_0
    :goto_0
    return v0

    .line 2768240
    :cond_1
    instance-of v2, p1, Lcom/facebook/storyline/model/StorylinePhoto;

    if-nez v2, :cond_2

    move v0, v1

    .line 2768241
    goto :goto_0

    .line 2768242
    :cond_2
    check-cast p1, Lcom/facebook/storyline/model/StorylinePhoto;

    .line 2768243
    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iget-wide v4, p1, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    iget-object v3, p1, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2768244
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2768245
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768246
    iget v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768247
    iget v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768248
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2768249
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2768250
    iget-wide v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->f:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2768251
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->g:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2768252
    iget-object v0, p0, Lcom/facebook/storyline/model/StorylinePhoto;->h:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2768253
    return-void
.end method
