.class public final Lcom/facebook/storyline/model/Cutdown$Section;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/storyline/model/Cutdown$Section;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final fillFrom:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fillFrom"
    .end annotation
.end field

.field public final imageEffect:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "imageEffect"
    .end annotation
.end field

.field public final isOptional:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isOptional"
    .end annotation
.end field

.field public final preferredSubdivision:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "preferredSubdivision"
    .end annotation
.end field

.field public final rank:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rank"
    .end annotation
.end field

.field public final startTime:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "startTime"
    .end annotation
.end field

.field public final subdivisions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subdivisions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2768026
    const-class v0, Lcom/facebook/storyline/model/Cutdown_SectionDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768005
    new-instance v0, LX/K4P;

    invoke-direct {v0}, LX/K4P;-><init>()V

    sput-object v0, Lcom/facebook/storyline/model/Cutdown$Section;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2768006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768007
    iput-object v1, p0, Lcom/facebook/storyline/model/Cutdown$Section;->subdivisions:LX/0Px;

    .line 2768008
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->startTime:F

    .line 2768009
    iput v2, p0, Lcom/facebook/storyline/model/Cutdown$Section;->preferredSubdivision:I

    .line 2768010
    iput-object v1, p0, Lcom/facebook/storyline/model/Cutdown$Section;->imageEffect:Ljava/lang/String;

    .line 2768011
    iput-object v1, p0, Lcom/facebook/storyline/model/Cutdown$Section;->fillFrom:Ljava/lang/String;

    .line 2768012
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->rank:I

    .line 2768013
    iput-boolean v2, p0, Lcom/facebook/storyline/model/Cutdown$Section;->isOptional:Z

    .line 2768014
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2768015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768016
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2768017
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->subdivisions:LX/0Px;

    .line 2768018
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->startTime:F

    .line 2768019
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->preferredSubdivision:I

    .line 2768020
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->imageEffect:Ljava/lang/String;

    .line 2768021
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->fillFrom:Ljava/lang/String;

    .line 2768022
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->rank:I

    .line 2768023
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->isOptional:Z

    .line 2768024
    return-void

    .line 2768025
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2767996
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2767997
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->subdivisions:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2767998
    iget v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->startTime:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2767999
    iget v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->preferredSubdivision:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768000
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->imageEffect:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768001
    iget-object v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->fillFrom:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2768002
    iget v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->rank:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2768003
    iget-boolean v0, p0, Lcom/facebook/storyline/model/Cutdown$Section;->isOptional:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2768004
    return-void
.end method
