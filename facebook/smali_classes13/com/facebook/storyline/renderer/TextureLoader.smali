.class public Lcom/facebook/storyline/renderer/TextureLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:LX/K4g;

.field private final d:LX/K4n;

.field public final e:LX/1HI;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/K4t;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/K59;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "[F>;>;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/Executor;

.field public final i:I

.field public j:Ljava/lang/String;

.field public k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2769588
    const-class v0, Lcom/facebook/storyline/renderer/TextureLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyline/renderer/TextureLoader;->a:Ljava/lang/String;

    .line 2769589
    const-class v0, Lcom/facebook/storyline/renderer/TextureLoader;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyline/renderer/TextureLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/K4g;LX/K4n;LX/1HI;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/K4g;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/K4n;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2769590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769591
    iput-object p1, p0, Lcom/facebook/storyline/renderer/TextureLoader;->c:LX/K4g;

    .line 2769592
    iput-object p2, p0, Lcom/facebook/storyline/renderer/TextureLoader;->d:LX/K4n;

    .line 2769593
    iput-object p3, p0, Lcom/facebook/storyline/renderer/TextureLoader;->e:LX/1HI;

    .line 2769594
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    .line 2769595
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->g:Ljava/util/Map;

    .line 2769596
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->i:I

    .line 2769597
    new-instance v0, LX/K4r;

    invoke-direct {v0, p0}, LX/K4r;-><init>(Lcom/facebook/storyline/renderer/TextureLoader;)V

    iput-object v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->h:Ljava/util/concurrent/Executor;

    .line 2769598
    return-void
.end method

.method public static c(Lcom/facebook/storyline/renderer/TextureLoader;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2769599
    iget-object v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K4t;

    .line 2769600
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2769601
    invoke-static {v0}, LX/K4t;->a(LX/K4t;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2769602
    iget-object v0, v0, LX/K4t;->a:LX/K5A;

    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 2769603
    new-array v1, p1, [I

    iget v2, v0, LX/K5A;->b:I

    aput v2, v1, p0

    .line 2769604
    invoke-static {p1, v1, p0}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 2769605
    :goto_1
    return-void

    .line 2769606
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2769607
    :cond_1
    iget-object v0, v0, LX/K4t;->b:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 2769608
    iget v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    goto :goto_1
.end method

.method public static c$redex0(Lcom/facebook/storyline/renderer/TextureLoader;)V
    .locals 4

    .prologue
    .line 2769609
    iget v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    if-nez v0, :cond_0

    .line 2769610
    iget-object v0, p0, Lcom/facebook/storyline/renderer/TextureLoader;->d:LX/K4n;

    iget-object v1, p0, Lcom/facebook/storyline/renderer/TextureLoader;->j:Ljava/lang/String;

    .line 2769611
    iget-object v2, v0, LX/K4n;->a:LX/K4o;

    iget-object v2, v2, LX/K4o;->g:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/storyline/renderer/StorylineController$TextureLoaderListener$1;

    invoke-direct {v3, v0, v1}, Lcom/facebook/storyline/renderer/StorylineController$TextureLoaderListener$1;-><init>(LX/K4n;Ljava/lang/String;)V

    const p0, 0x196971e4

    invoke-static {v2, v3, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2769612
    :cond_0
    return-void
.end method
