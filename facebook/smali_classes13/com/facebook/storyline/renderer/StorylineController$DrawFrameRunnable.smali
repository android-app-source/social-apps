.class public final Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/K4o;


# direct methods
.method public constructor <init>(LX/K4o;)V
    .locals 0

    .prologue
    .line 2769255
    iput-object p1, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2769244
    iget-object v0, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->p:LX/K4l;

    sget-object v1, LX/K4l;->PLAYING:LX/K4l;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2769245
    iget-object v0, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v0, v0, LX/K4o;->r:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-wide v2, v2, LX/K4o;->B:J

    sub-long/2addr v0, v2

    .line 2769246
    iget-object v2, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v3, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget v3, v3, LX/K4o;->C:I

    int-to-long v4, v3

    iget-object v3, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-wide v6, v3, LX/K4o;->A:J

    div-long v6, v0, v6

    add-long/2addr v4, v6

    long-to-int v3, v4

    iget-object v4, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v4, v4, LX/K4o;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2769247
    iput v3, v2, LX/K4o;->y:I

    .line 2769248
    iget-object v2, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    invoke-static {v2}, LX/K4o;->p$redex0(LX/K4o;)V

    .line 2769249
    iget-object v2, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget v2, v2, LX/K4o;->y:I

    iget-object v3, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v3, v3, LX/K4o;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    .line 2769250
    iget-object v0, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    invoke-virtual {v0}, LX/K4o;->d()V

    .line 2769251
    :goto_1
    return-void

    .line 2769252
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2769253
    :cond_1
    iget-object v2, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget v2, v2, LX/K4o;->y:I

    iget-object v3, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget v3, v3, LX/K4o;->C:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    iget-object v4, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-wide v4, v4, LX/K4o;->A:J

    mul-long/2addr v2, v4

    sub-long v0, v2, v0

    .line 2769254
    iget-object v2, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v2, v2, LX/K4o;->g:Landroid/os/Handler;

    iget-object v3, p0, Lcom/facebook/storyline/renderer/StorylineController$DrawFrameRunnable;->a:LX/K4o;

    iget-object v3, v3, LX/K4o;->d:Ljava/lang/Runnable;

    const v4, 0x32506fa2

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method
