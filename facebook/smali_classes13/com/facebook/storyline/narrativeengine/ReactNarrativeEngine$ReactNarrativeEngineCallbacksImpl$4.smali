.class public final Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Z

.field public final synthetic d:Ljava/util/List;

.field public final synthetic e:LX/K4V;


# direct methods
.method public constructor <init>(LX/K4V;Ljava/lang/String;ZZLjava/util/List;)V
    .locals 0

    .prologue
    .line 2768453
    iput-object p1, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    iput-object p2, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->b:Z

    iput-boolean p4, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->c:Z

    iput-object p5, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->d:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2768454
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    iget-object v0, v0, LX/K4V;->a:LX/K4W;

    iget-object v0, v0, LX/K4W;->h:LX/K4k;

    if-nez v0, :cond_1

    .line 2768455
    :cond_0
    :goto_0
    return-void

    .line 2768456
    :cond_1
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    iget-object v1, v1, LX/K4V;->a:LX/K4W;

    iget-object v1, v1, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2768457
    iget-boolean v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->b:Z

    if-eqz v0, :cond_2

    .line 2768458
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    iget-object v0, v0, LX/K4V;->a:LX/K4W;

    iget-object v0, v0, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 2768459
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2768460
    iput-wide v2, v0, LX/K4V;->b:J

    .line 2768461
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->c:Z

    if-eqz v0, :cond_3

    .line 2768462
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    iget-object v0, v0, LX/K4V;->a:LX/K4W;

    iget-object v0, v0, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 2768463
    :cond_3
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->e:LX/K4V;

    iget-object v0, v0, LX/K4V;->a:LX/K4W;

    iget-object v0, v0, LX/K4W;->h:LX/K4k;

    iget-object v1, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->d:Ljava/util/List;

    iget-boolean v2, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->b:Z

    iget-boolean v3, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;->c:Z

    .line 2768464
    if-eqz v2, :cond_4

    .line 2768465
    iget-object v4, v0, LX/K4k;->a:LX/K4o;

    iget-object v4, v4, LX/K4o;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2768466
    :cond_4
    iget-object v4, v0, LX/K4k;->a:LX/K4o;

    iget-object v4, v4, LX/K4o;->c:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2768467
    if-eqz v3, :cond_5

    .line 2768468
    iget-object v4, v0, LX/K4k;->a:LX/K4o;

    const/4 p0, 0x1

    .line 2768469
    iput-boolean p0, v4, LX/K4o;->v:Z

    .line 2768470
    iget-object v4, v0, LX/K4k;->a:LX/K4o;

    invoke-static {v4}, LX/K4o;->t(LX/K4o;)V

    .line 2768471
    :cond_5
    goto :goto_0
.end method
