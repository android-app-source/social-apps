.class public final Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/5pG;

.field public final synthetic b:LX/K4V;


# direct methods
.method public constructor <init>(LX/K4V;LX/5pG;)V
    .locals 0

    .prologue
    .line 2768430
    iput-object p1, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;->b:LX/K4V;

    iput-object p2, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;->a:LX/5pG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2768431
    iget-object v0, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;->b:LX/K4V;

    iget-object v0, v0, LX/K4V;->a:LX/K4W;

    iget-object v0, v0, LX/K4W;->h:LX/K4k;

    if-nez v0, :cond_0

    .line 2768432
    :goto_0
    return-void

    .line 2768433
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2768434
    iget-object v1, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;->a:LX/5pG;

    invoke-interface {v1}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2768435
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v2

    .line 2768436
    iget-object v3, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;->a:LX/5pG;

    invoke-interface {v3, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2768437
    :cond_1
    iget-object v1, p0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;->b:LX/K4V;

    iget-object v1, v1, LX/K4V;->a:LX/K4W;

    iget-object v1, v1, LX/K4W;->h:LX/K4k;

    .line 2768438
    iget-object v2, v1, LX/K4k;->a:LX/K4o;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    .line 2768439
    iput-object v3, v2, LX/K4o;->l:LX/0P1;

    .line 2768440
    goto :goto_0
.end method
