.class public Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/JuD;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/notes/view/block/FromBlockView;"
    }
.end annotation


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/widget/TextView;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2748638
    const-class v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2748639
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2748640
    const-class v0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 2748641
    iget-object v0, p0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->a:LX/Ck0;

    invoke-virtual {v0, p1}, LX/Ck0;->a(Landroid/view/View;)V

    .line 2748642
    const v0, 0x7f0d1dd0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->c:Landroid/widget/TextView;

    .line 2748643
    const v0, 0x7f0d1dcf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2748644
    iget-object v0, p0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->a:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v2, 0x7f0d011e

    const v3, 0x7f0d011e

    invoke-virtual {v0, v1, v2, v3}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 2748645
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, Lcom/facebook/notes/view/block/impl/FromBlockViewImpl;->a:LX/Ck0;

    return-void
.end method
