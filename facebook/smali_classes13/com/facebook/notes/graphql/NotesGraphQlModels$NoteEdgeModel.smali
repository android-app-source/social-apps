.class public final Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x701c63f0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:J

.field private s:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2747961
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2747960
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2747962
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2747963
    return-void
.end method

.method private n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747964
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747965
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    return-object v0
.end method

.method private o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747966
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747967
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    return-object v0
.end method

.method private p()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747968
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->g:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->g:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    .line 2747969
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->g:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    return-object v0
.end method

.method private q()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747970
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->h:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->h:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    .line 2747971
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->h:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747984
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747985
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    return-object v0
.end method

.method private s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747972
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747973
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    return-object v0
.end method

.method private t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747974
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    .line 2747975
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    return-object v0
.end method

.method private u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747976
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747977
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    return-object v0
.end method

.method private v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747978
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747979
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747980
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2747981
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747982
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2747983
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747926
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    .line 2747927
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 2747928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2747929
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2747930
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2747931
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2747932
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2747933
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2747934
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2747935
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2747936
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2747937
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2747938
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->w()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2747939
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->x()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2747940
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->y()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2747941
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2747942
    const/16 v13, 0xf

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 2747943
    const/4 v13, 0x0

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 2747944
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2747945
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2747946
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2747947
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2747948
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2747949
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2747950
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2747951
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2747952
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2747953
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2747954
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2747955
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2747956
    const/16 v1, 0xd

    iget-wide v2, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->r:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2747957
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2747958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2747959
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2747851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2747852
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2747853
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747854
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2747855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747856
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747857
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2747858
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747859
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2747860
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747861
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747862
    :cond_1
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2747863
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    .line 2747864
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2747865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747866
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->g:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    .line 2747867
    :cond_2
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2747868
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    .line 2747869
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2747870
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747871
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->h:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    .line 2747872
    :cond_3
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2747873
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747874
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2747875
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747876
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747877
    :cond_4
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2747878
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747879
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2747880
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747881
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747882
    :cond_5
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2747883
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    .line 2747884
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2747885
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747886
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    .line 2747887
    :cond_6
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2747888
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747889
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 2747890
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747891
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747892
    :cond_7
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2747893
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747894
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 2747895
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747896
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2747897
    :cond_8
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->w()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2747898
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->w()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2747899
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->w()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 2747900
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2747901
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2747902
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2747903
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747905
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2747906
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2747907
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->r:J

    .line 2747908
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s:J

    .line 2747909
    return-void
.end method

.method public final synthetic b()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747910
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2747911
    new-instance v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-direct {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;-><init>()V

    .line 2747912
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2747913
    return-object v0
.end method

.method public final synthetic c()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747914
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747915
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->p()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentAuthorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2747916
    const v0, -0x2f48df35

    return v0
.end method

.method public final synthetic e()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747917
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel$DocumentBodyElementsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2747918
    const v0, 0x3fc1086d

    return v0
.end method

.method public final synthetic j()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747919
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747920
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747921
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q:Ljava/lang/String;

    .line 2747922
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final m()J
    .locals 2

    .prologue
    .line 2747923
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2747924
    iget-wide v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s:J

    return-wide v0
.end method

.method public final synthetic nn_()LX/8Yp;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747904
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic no_()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747925
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object v0

    return-object v0
.end method
