.class public final Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2995ea3b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2748045
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2748007
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2748043
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2748044
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFocus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2748041
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2748042
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748039
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2748040
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2748031
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2748032
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x2cfb119c

    invoke-static {v1, v0, v2}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2748033
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2748034
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2748035
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2748036
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2748037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2748038
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2748016
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2748017
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2748018
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2cfb119c

    invoke-static {v2, v0, v3}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748019
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2748020
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    .line 2748021
    iput v3, v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->e:I

    move-object v1, v0

    .line 2748022
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2748023
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2748024
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2748025
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    .line 2748026
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2748027
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2748028
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2748029
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2748030
    goto :goto_0
.end method

.method public final synthetic a()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748015
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2748012
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2748013
    const/4 v0, 0x0

    const v1, -0x2cfb119c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;->e:I

    .line 2748014
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2748009
    new-instance v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;-><init>()V

    .line 2748010
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2748011
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2748008
    const v0, -0x56957311

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2748006
    const v0, 0x1da3a91b

    return v0
.end method
