.class public final Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2747671
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2747672
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2747726
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2747727
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 2747711
    if-nez p1, :cond_0

    .line 2747712
    :goto_0
    return v1

    .line 2747713
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2747714
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2747715
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2747716
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2747717
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2747718
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2747719
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2747720
    :sswitch_1
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2747721
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2747722
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 2747723
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2747724
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2747725
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2cfb119c -> :sswitch_1
        0x4a786e44 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2747710
    new-instance v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2747707
    sparse-switch p0, :sswitch_data_0

    .line 2747708
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2747709
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x2cfb119c -> :sswitch_0
        0x4a786e44 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2747706
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2747704
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->b(I)V

    .line 2747705
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2747699
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2747700
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2747701
    :cond_0
    iput-object p1, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a:LX/15i;

    .line 2747702
    iput p2, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->b:I

    .line 2747703
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2747698
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2747697
    new-instance v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2747694
    iget v0, p0, LX/1vt;->c:I

    .line 2747695
    move v0, v0

    .line 2747696
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2747691
    iget v0, p0, LX/1vt;->c:I

    .line 2747692
    move v0, v0

    .line 2747693
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2747688
    iget v0, p0, LX/1vt;->b:I

    .line 2747689
    move v0, v0

    .line 2747690
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2747685
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2747686
    move-object v0, v0

    .line 2747687
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2747676
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2747677
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2747678
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2747679
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2747680
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2747681
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2747682
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2747683
    invoke-static {v3, v9, v2}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2747684
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2747673
    iget v0, p0, LX/1vt;->c:I

    .line 2747674
    move v0, v0

    .line 2747675
    return v0
.end method
