.class public final Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x495743ed
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2748221
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2748220
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2748218
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2748219
    return-void
.end method

.method private l()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748216
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->e:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->e:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    .line 2748217
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->e:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    return-object v0
.end method

.method private m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748214
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    .line 2748215
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2748196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2748197
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->l()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2748198
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->b()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2748199
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2748200
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2748201
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x4a786e44    # 4070289.0f

    invoke-static {v5, v4, v6}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2748202
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2748203
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->np_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2748204
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2748205
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2748206
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2748207
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2748208
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2748209
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2748210
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2748211
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2748212
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2748213
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2748166
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2748167
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->l()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2748168
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->l()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    .line 2748169
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->l()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2748170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    .line 2748171
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->e:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    .line 2748172
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->b()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2748173
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->b()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2748174
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->b()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2748175
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    .line 2748176
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->f:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2748177
    :cond_1
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2748178
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    .line 2748179
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2748180
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    .line 2748181
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    .line 2748182
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2748183
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4a786e44    # 4070289.0f

    invoke-static {v2, v0, v3}, Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notes/graphql/NotesGraphQlModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748184
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2748185
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    .line 2748186
    iput v3, v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->i:I

    move-object v1, v0

    .line 2748187
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2748188
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2748189
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2748190
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    .line 2748191
    iput-object v0, v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2748192
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2748193
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    .line 2748194
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move-object p0, v1

    .line 2748195
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748165
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2748162
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2748163
    const/4 v0, 0x4

    const v1, 0x4a786e44    # 4070289.0f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->i:I

    .line 2748164
    return-void
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748222
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->f:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->f:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2748223
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->f:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2748159
    new-instance v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    invoke-direct {v0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;-><init>()V

    .line 2748160
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2748161
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748158
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748156
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->h:Ljava/lang/String;

    .line 2748157
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2748155
    const v0, 0x6ee7e77f

    return v0
.end method

.method public final synthetic e()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748154
    invoke-virtual {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2748153
    const v0, 0x252412

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacyScope"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748151
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2748152
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748149
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    .line 2748150
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->j:Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteEdgeModel;

    return-object v0
.end method

.method public final np_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748146
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k:Ljava/lang/String;

    .line 2748147
    iget-object v0, p0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic nq_()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2748148
    invoke-direct {p0}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;->l()Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method
