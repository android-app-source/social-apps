.class public final Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2748143
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    new-instance v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2748144
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2748145
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2748105
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2748106
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2748107
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2748108
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2748109
    if-eqz v2, :cond_0

    .line 2748110
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748111
    invoke-static {v1, v2, p1, p2}, LX/Ju3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748112
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2748113
    if-eqz v2, :cond_1

    .line 2748114
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748115
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748116
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2748117
    if-eqz v2, :cond_2

    .line 2748118
    const-string v3, "from"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748119
    invoke-static {v1, v2, p1, p2}, LX/8ZR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748120
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2748121
    if-eqz v2, :cond_3

    .line 2748122
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748123
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2748124
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2748125
    if-eqz v2, :cond_5

    .line 2748126
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748127
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2748128
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2748129
    if-eqz v3, :cond_4

    .line 2748130
    const-string p0, "label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748131
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2748132
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2748133
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2748134
    if-eqz v2, :cond_6

    .line 2748135
    const-string v3, "published_document"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748136
    invoke-static {v1, v2, p1, p2}, LX/Ju1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748137
    :cond_6
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2748138
    if-eqz v2, :cond_7

    .line 2748139
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748140
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2748141
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2748142
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2748104
    check-cast p1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Serializer;->a(Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;LX/0nX;LX/0my;)V

    return-void
.end method
