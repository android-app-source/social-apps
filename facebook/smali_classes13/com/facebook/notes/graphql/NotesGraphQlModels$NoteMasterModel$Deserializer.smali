.class public final Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2748046
    const-class v0, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    new-instance v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2748047
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2748048
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2748049
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2748050
    const/4 v2, 0x0

    .line 2748051
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 2748052
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2748053
    :goto_0
    move v1, v2

    .line 2748054
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2748055
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2748056
    new-instance v1, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;

    invoke-direct {v1}, Lcom/facebook/notes/graphql/NotesGraphQlModels$NoteMasterModel;-><init>()V

    .line 2748057
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2748058
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2748059
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2748060
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2748061
    :cond_0
    return-object v1

    .line 2748062
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2748063
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_9

    .line 2748064
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2748065
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2748066
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, p0, :cond_2

    if-eqz v9, :cond_2

    .line 2748067
    const-string v10, "cover_photo"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2748068
    invoke-static {p1, v0}, LX/Ju3;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2748069
    :cond_3
    const-string v10, "feedback"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2748070
    invoke-static {p1, v0}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2748071
    :cond_4
    const-string v10, "from"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 2748072
    invoke-static {p1, v0}, LX/8ZR;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2748073
    :cond_5
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2748074
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2748075
    :cond_6
    const-string v10, "privacy_scope"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2748076
    const/4 v9, 0x0

    .line 2748077
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v10, :cond_e

    .line 2748078
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2748079
    :goto_2
    move v4, v9

    .line 2748080
    goto :goto_1

    .line 2748081
    :cond_7
    const-string v10, "published_document"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2748082
    invoke-static {p1, v0}, LX/Ju1;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2748083
    :cond_8
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2748084
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 2748085
    :cond_9
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2748086
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2748087
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2748088
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2748089
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2748090
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2748091
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2748092
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748093
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1

    .line 2748094
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2748095
    :cond_c
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_d

    .line 2748096
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2748097
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2748098
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v10, :cond_c

    .line 2748099
    const-string p0, "label"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2748100
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 2748101
    :cond_d
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2748102
    invoke-virtual {v0, v9, v4}, LX/186;->b(II)V

    .line 2748103
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto/16 :goto_2

    :cond_e
    move v4, v9

    goto :goto_3
.end method
