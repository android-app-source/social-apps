.class public Lcom/facebook/profile/insight/view/ProfileInsightTitleView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2755498
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2755499
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2755500
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755501
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2755502
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755503
    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f031063

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2755504
    const v0, 0x7f0d2747

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2755505
    return-void
.end method


# virtual methods
.method public setExitClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2755506
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2755507
    return-void
.end method
