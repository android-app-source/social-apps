.class public Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2755295
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2755296
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2755293
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755294
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2755273
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755274
    const-class v0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    invoke-static {v0, p0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2755275
    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03105d

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2755276
    const v0, 0x7f0d2737

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2755277
    const v0, 0x7f0d2735

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2755278
    const v0, 0x7f0d2736

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2755279
    const v0, 0x7f0d2738

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2755280
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b26e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2755281
    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2755282
    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b26e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b26e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v4, v0, v4, v1}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->setPadding(IIII)V

    .line 2755283
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2755297
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2755298
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 2755299
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "MMM dd"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2755300
    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 2755301
    const-string v4, "LAST_60_DAYS"

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2755302
    const-wide v4, 0x134fd9000L

    sub-long/2addr v0, v4

    .line 2755303
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2755304
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2755305
    :cond_0
    const-string v4, "LAST_30_DAYS"

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2755306
    const-wide v4, 0x9a7ec800L

    sub-long/2addr v0, v4

    goto :goto_0

    .line 2755307
    :cond_1
    const-wide/32 v4, 0x240c8400

    sub-long/2addr v0, v4

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a:LX/0SG;

    return-void
.end method


# virtual methods
.method public final a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2755284
    if-eqz p4, :cond_0

    .line 2755285
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p4}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2755286
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2755287
    :goto_0
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2755288
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2755289
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2755290
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p5}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2755291
    return-void

    .line 2755292
    :cond_0
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->b:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
