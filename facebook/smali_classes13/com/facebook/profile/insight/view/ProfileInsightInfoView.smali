.class public Lcom/facebook/profile/insight/view/ProfileInsightInfoView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private b:Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

.field private c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2755414
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2755415
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2755430
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755431
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2755423
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2755424
    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f031062

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2755425
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->setOrientation(I)V

    .line 2755426
    const v0, 0x7f0d2746

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2755427
    const v0, 0x7f0d2744

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->b:Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    .line 2755428
    const v0, 0x7f0d2745

    invoke-virtual {p0, v0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2755429
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    .line 2755416
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->b:Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    const v1, 0x7f0208ed

    const v2, 0x7f083c25

    const v3, 0x7f02081b

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2755417
    iget-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083c26

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2755418
    new-instance v0, LX/Jyl;

    invoke-direct {v0, p1, p2}, LX/Jyl;-><init>(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;Landroid/view/View$OnClickListener;)V

    .line 2755419
    new-instance v1, LX/1P0;

    invoke-virtual {p0}, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2755420
    iget-object v2, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2755421
    iget-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightInfoView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2755422
    return-void
.end method
