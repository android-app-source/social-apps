.class public Lcom/facebook/profile/insight/view/ProfileInsightFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Jyp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:I

.field public e:Ljava/lang/String;

.field public f:Landroid/support/v4/view/ViewPager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Landroid/view/View;

.field public h:Lcom/facebook/profile/insight/view/ProfileInsightTitleView;

.field public i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public j:LX/JyX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2755375
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2755376
    const/4 v0, 0x6

    iput v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->d:I

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2755377
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2755378
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    new-instance v3, LX/Jyp;

    invoke-static {p1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-direct {v3, v2}, LX/Jyp;-><init>(LX/0Sh;)V

    move-object v2, v3

    check-cast v2, LX/Jyp;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const/16 v0, 0x15e7

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->a:LX/Jyp;

    iput-object v3, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->b:LX/0Zb;

    iput-object p1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->c:LX/0Ot;

    .line 2755379
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x4b5d91af

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2755362
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->e:Ljava/lang/String;

    .line 2755363
    const v0, 0x7f031060

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2755364
    const v0, 0x7f0d2742

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2755365
    const v1, 0x7f0d273f

    invoke-static {v3, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->g:Landroid/view/View;

    .line 2755366
    const v1, 0x7f0d2740

    invoke-static {v3, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;

    iput-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->h:Lcom/facebook/profile/insight/view/ProfileInsightTitleView;

    .line 2755367
    const v1, 0x7f0d2741

    invoke-static {v3, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2755368
    iget-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2755369
    const v1, 0x7f03105f

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2755370
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2755371
    const v0, 0x7f0d273d

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 2755372
    const v4, 0x7f0d273e

    invoke-static {v1, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->f:Landroid/support/v4/view/ViewPager;

    .line 2755373
    new-instance v1, LX/JyX;

    iget-object v4, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->g:Landroid/view/View;

    invoke-direct {v1, v4, v0, p0}, LX/JyX;-><init>(Landroid/view/View;Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;Lcom/facebook/profile/insight/view/ProfileInsightFragment;)V

    iput-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->j:LX/JyX;

    .line 2755374
    const/16 v0, 0x2b

    const v1, -0x16c9cfcb

    invoke-static {v5, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7ccc0b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2755359
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2755360
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->e:Ljava/lang/String;

    .line 2755361
    const/16 v1, 0x2b

    const v2, -0x2f8c7dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
