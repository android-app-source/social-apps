.class public final Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7cb20af9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2755148
    const-class v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2755147
    const-class v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2755145
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2755146
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2755142
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2755143
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2755144
    :cond_0
    iget-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2755140
    iget-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->f:Ljava/lang/String;

    .line 2755141
    iget-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2755130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2755131
    invoke-direct {p0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2755132
    invoke-direct {p0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2755133
    invoke-virtual {p0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 2755134
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2755135
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2755136
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2755137
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2755138
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2755139
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2755122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2755123
    invoke-virtual {p0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2755124
    invoke-virtual {p0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2755125
    if-eqz v1, :cond_0

    .line 2755126
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    .line 2755127
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->g:Ljava/util/List;

    .line 2755128
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2755129
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2755149
    new-instance v0, LX/Jya;

    invoke-direct {v0, p1}, LX/Jya;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2755121
    invoke-direct {p0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2755119
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2755120
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2755118
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2755115
    new-instance v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    invoke-direct {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;-><init>()V

    .line 2755116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2755117
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2755111
    const v0, 0x16bac547

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2755114
    const v0, 0x252222

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2755112
    iget-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->g:Ljava/util/List;

    .line 2755113
    iget-object v0, p0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
