.class public Lcom/facebook/profile/insight/ProfileInsightActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JyO;


# static fields
.field private static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public p:LX/JyR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

.field private s:LX/JyQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2754778
    const-class v0, Lcom/facebook/profile/insight/ProfileInsightActivity;

    const-string v1, "video_insight"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profile/insight/ProfileInsightActivity;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2754777
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2754717
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/profile/insight/ProfileInsightActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2754718
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2754719
    return-object v0
.end method

.method private a()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2754776
    new-instance v0, LX/JyN;

    invoke-direct {v0, p0}, LX/JyN;-><init>(Lcom/facebook/profile/insight/ProfileInsightActivity;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/profile/insight/ProfileInsightActivity;

    const-class v1, LX/JyR;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/JyR;

    iput-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->p:LX/JyR;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;)V
    .locals 2

    .prologue
    .line 2754760
    iget-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->r:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    invoke-direct {p0}, Lcom/facebook/profile/insight/ProfileInsightActivity;->a()Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 2754761
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object p0

    if-nez p0, :cond_1

    .line 2754762
    :cond_0
    :goto_0
    return-void

    .line 2754763
    :cond_1
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->a:LX/Jyp;

    .line 2754764
    iput-object p1, p0, LX/Jyp;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    .line 2754765
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2754766
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->h:Lcom/facebook/profile/insight/view/ProfileInsightTitleView;

    invoke-virtual {p0, v1}, Lcom/facebook/profile/insight/view/ProfileInsightTitleView;->setExitClickListener(Landroid/view/View$OnClickListener;)V

    .line 2754767
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->f:Landroid/support/v4/view/ViewPager;

    if-nez p0, :cond_2

    .line 2754768
    :goto_1
    goto :goto_0

    .line 2754769
    :cond_2
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->f:Landroid/support/v4/view/ViewPager;

    new-instance p1, Lcom/facebook/profile/insight/view/ProfileInsightFragment$1;

    invoke-direct {p1, v0}, Lcom/facebook/profile/insight/view/ProfileInsightFragment$1;-><init>(Lcom/facebook/profile/insight/view/ProfileInsightFragment;)V

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->post(Ljava/lang/Runnable;)Z

    .line 2754770
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->f:Landroid/support/v4/view/ViewPager;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2754771
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->f:Landroid/support/v4/view/ViewPager;

    iget-object p1, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->a:LX/Jyp;

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2754772
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object p1, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2754773
    iget-object p0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance p1, LX/Jyi;

    invoke-direct {p1, v0}, LX/Jyi;-><init>(Lcom/facebook/profile/insight/view/ProfileInsightFragment;)V

    .line 2754774
    iput-object p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2754775
    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2754730
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2754731
    invoke-static {p0, p0}, Lcom/facebook/profile/insight/ProfileInsightActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2754732
    iget-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->p:LX/JyR;

    sget-object v1, Lcom/facebook/profile/insight/ProfileInsightActivity;->q:Lcom/facebook/common/callercontext/CallerContext;

    .line 2754733
    new-instance v4, LX/JyQ;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    move-object v8, v1

    move-object v9, p0

    invoke-direct/range {v4 .. v9}, LX/JyQ;-><init>(LX/0tX;LX/1Ck;LX/0Or;Lcom/facebook/common/callercontext/CallerContext;LX/JyO;)V

    .line 2754734
    move-object v0, v4

    .line 2754735
    iput-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->s:LX/JyQ;

    .line 2754736
    iget-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->s:LX/JyQ;

    .line 2754737
    iget-object v4, v0, LX/JyQ;->b:LX/1Ck;

    const-string v5, "fetch_profile_insight"

    .line 2754738
    new-instance v8, LX/JyY;

    invoke-direct {v8}, LX/JyY;-><init>()V

    move-object v8, v8

    .line 2754739
    const-string v9, "id"

    iget-object v10, v0, LX/JyQ;->e:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2754740
    const-string v9, "durations"

    const-string v10, "LAST_60_DAYS"

    const-string v11, "LAST_30_DAYS"

    const-string v12, "LAST_7_DAYS"

    invoke-static {v10, v11, v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2754741
    iget-object v9, v0, LX/JyQ;->a:LX/0tX;

    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    iget-object v10, v0, LX/JyQ;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2754742
    iput-object v10, v8, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2754743
    move-object v8, v8

    .line 2754744
    sget-object v10, LX/0zS;->c:LX/0zS;

    invoke-virtual {v8, v10}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    .line 2754745
    move-object v6, v8

    .line 2754746
    new-instance v7, LX/JyP;

    invoke-direct {v7, v0}, LX/JyP;-><init>(LX/JyQ;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2754747
    invoke-virtual {p0}, Lcom/facebook/profile/insight/ProfileInsightActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profile_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2754748
    invoke-static {p0}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v1

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 2754749
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2754750
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2754751
    const-string v3, "profile_id"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2754752
    new-instance v3, Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    invoke-direct {v3}, Lcom/facebook/profile/insight/view/ProfileInsightFragment;-><init>()V

    .line 2754753
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2754754
    move-object v0, v3

    .line 2754755
    iput-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->r:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    .line 2754756
    invoke-static {p0}, LX/18w;->b(Landroid/content/Context;)I

    move-result v0

    iget-object v2, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->r:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    const-string v3, "ProfileInsightFragment"

    invoke-virtual {v1, v0, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2754757
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 2754758
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2754759
    return-void
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 2754720
    iget-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->r:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    if-eqz v0, :cond_1

    .line 2754721
    iget-object v0, p0, Lcom/facebook/profile/insight/ProfileInsightActivity;->r:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    const/4 v1, 0x1

    .line 2754722
    iget-object v2, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->j:LX/JyX;

    const/4 v0, 0x3

    .line 2754723
    iget-object v3, v2, LX/JyX;->e:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, v0}, LX/3CW;->c(II)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, LX/JyX;->e:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 p0, 0x1

    invoke-static {v3, p0}, LX/3CW;->c(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2754724
    :cond_0
    :goto_0
    return-void

    .line 2754725
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/profile/insight/ProfileInsightActivity;->finish()V

    goto :goto_0

    .line 2754726
    :cond_2
    invoke-static {v2}, LX/JyX;->f(LX/JyX;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2754727
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, LX/JyX;->e:Ljava/lang/Integer;

    .line 2754728
    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/JyX;->a$redex0(LX/JyX;F)V

    .line 2754729
    iget-object v3, v2, LX/JyX;->c:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    sget-object p0, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(LX/31M;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
