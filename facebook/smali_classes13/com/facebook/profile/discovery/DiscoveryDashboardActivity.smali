.class public Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private p:LX/Jy4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/Jxr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/Jy3;

.field private t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2753936
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()LX/0h5;
    .locals 3

    .prologue
    .line 2753959
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2753960
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2753961
    new-instance v1, LX/Jxz;

    invoke-direct {v1, p0}, LX/Jxz;-><init>(Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2753962
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const/4 v2, 0x2

    .line 2753963
    iput v2, v1, LX/108;->a:I

    .line 2753964
    move-object v1, v1

    .line 2753965
    const v2, 0x7f02098f

    .line 2753966
    iput v2, v1, LX/108;->i:I

    .line 2753967
    move-object v1, v1

    .line 2753968
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2753969
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2753970
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->s:LX/Jy3;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2753971
    return-object v0
.end method

.method private static a(Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;LX/Jy4;LX/Jxr;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0

    .prologue
    .line 2753958
    iput-object p1, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->p:LX/Jy4;

    iput-object p2, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->q:LX/Jxr;

    iput-object p3, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;

    const-class v0, LX/Jy4;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Jy4;

    const-class v1, LX/Jxr;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Jxr;

    invoke-static {v2}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v2

    check-cast v2, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->a(Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;LX/Jy4;LX/Jxr;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2753944
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2753945
    invoke-static {p0, p0}, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2753946
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xb30001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2753947
    new-instance v0, LX/1De;

    invoke-direct {v0, p0}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2753948
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/Jxx;->a(Landroid/content/Intent;Ljava/lang/Integer;)Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    move-result-object v1

    .line 2753949
    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->q:LX/Jxr;

    invoke-virtual {v2, v1}, LX/Jxr;->a(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)LX/Jxq;

    move-result-object v2

    .line 2753950
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, LX/Jxx;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2753951
    invoke-virtual {v2}, LX/Jxq;->a()V

    .line 2753952
    :cond_0
    iget-object v3, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->p:LX/Jy4;

    invoke-virtual {v3, v0, v1, v2}, LX/Jy4;->a(LX/1De;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;LX/Jxq;)LX/Jy3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->s:LX/Jy3;

    .line 2753953
    const v0, 0x7f030431

    invoke-virtual {p0, v0}, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->setContentView(I)V

    .line 2753954
    invoke-direct {p0}, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->a()LX/0h5;

    .line 2753955
    const v0, 0x7f0d0cca

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2753956
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->s:LX/Jy3;

    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1, v4}, LX/Jy3;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V

    .line 2753957
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2753941
    if-ne p1, v2, :cond_0

    .line 2753942
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->s:LX/Jy3;

    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1, v2}, LX/Jy3;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V

    .line 2753943
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x44fc4e70

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2753937
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2753938
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->s:LX/Jy3;

    if-eqz v1, :cond_0

    .line 2753939
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryDashboardActivity;->s:LX/Jy3;

    invoke-virtual {v1}, LX/Jy3;->a()V

    .line 2753940
    :cond_0
    const/16 v1, 0x23

    const v2, -0x31d800f3    # -7.0462752E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
