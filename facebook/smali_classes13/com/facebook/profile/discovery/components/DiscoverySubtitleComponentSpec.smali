.class public Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/graphics/PorterDuffColorFilter;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2754606
    const-class v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Or;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2754607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754608
    iput-object p1, p0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->b:Landroid/content/res/Resources;

    .line 2754609
    iput-object p2, p0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->c:LX/0Or;

    .line 2754610
    const v0, 0x7f0a00a4

    .line 2754611
    iget-object v1, p0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->b:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2754612
    new-instance p1, Landroid/graphics/PorterDuffColorFilter;

    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p1, v1, p2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    move-object v0, p1

    .line 2754613
    iput-object v0, p0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->d:Landroid/graphics/PorterDuffColorFilter;

    .line 2754614
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;
    .locals 5

    .prologue
    .line 2754615
    const-class v1, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;

    monitor-enter v1

    .line 2754616
    :try_start_0
    sget-object v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2754617
    sput-object v2, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2754618
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754619
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2754620
    new-instance v4, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;-><init>(Landroid/content/res/Resources;LX/0Or;)V

    .line 2754621
    move-object v0, v4

    .line 2754622
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2754623
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2754624
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2754625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
