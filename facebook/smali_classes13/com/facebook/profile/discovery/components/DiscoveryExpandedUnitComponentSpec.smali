.class public Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/JyJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2754349
    const-class v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Or;LX/JyJ;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/JyJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2754350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754351
    iput-object p1, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    .line 2754352
    iput-object p2, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->c:LX/0Or;

    .line 2754353
    iput-object p3, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->d:LX/JyJ;

    .line 2754354
    return-void
.end method

.method public static a(Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;LX/1Ad;)LX/1Dh;
    .locals 6

    .prologue
    .line 2754355
    invoke-static {p2}, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754356
    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2754357
    :goto_0
    iget-object v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    const v2, 0x7f0b26d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v1

    .line 2754358
    iget-object v2, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    const v3, 0x7f0a0097

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b26d2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, LX/4Ab;->a(IF)LX/4Ab;

    .line 2754359
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f02150b

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 2754360
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    invoke-virtual {p3, v0}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    sget-object v4, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b26d4

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b26d4

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    .line 2754361
    iget-object v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-lez v1, :cond_5

    .line 2754362
    iget-object v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2754363
    div-int/lit8 v3, v1, 0x4

    .line 2754364
    invoke-interface {v2, v1}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Dh;->I(I)LX/1Dh;

    .line 2754365
    :goto_1
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    move-object v0, v0

    .line 2754366
    :goto_2
    return-object v0

    .line 2754367
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    .line 2754368
    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2754369
    :goto_3
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    const/4 v0, 0x4

    if-ge v2, v0, :cond_3

    .line 2754370
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    .line 2754371
    :goto_5
    const/high16 p2, 0x3f800000    # 1.0f

    .line 2754372
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2754373
    :goto_6
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    invoke-virtual {p3, v4}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    sget-object p0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1up;->c(F)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object v4

    move-object v0, v4

    .line 2754374
    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2754375
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2754376
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2754377
    move-object v1, v0

    goto :goto_3

    .line 2754378
    :cond_2
    const/4 v0, 0x0

    goto :goto_5

    .line 2754379
    :cond_3
    move-object v0, v3

    .line 2754380
    goto/16 :goto_2

    .line 2754381
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2754382
    :cond_5
    const/4 v1, 0x7

    const v3, 0x7f0b26d1

    invoke-interface {v0, v1, v3}, LX/1Di;->c(II)LX/1Di;

    goto/16 :goto_1

    .line 2754383
    :cond_6
    const/4 v4, 0x0

    goto :goto_6
.end method

.method public static a(LX/0QB;)Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;
    .locals 6

    .prologue
    .line 2754384
    const-class v1, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;

    monitor-enter v1

    .line 2754385
    :try_start_0
    sget-object v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2754386
    sput-object v2, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2754387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2754389
    new-instance v5, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/JyJ;->a(LX/0QB;)LX/JyJ;

    move-result-object v4

    check-cast v4, LX/JyJ;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;-><init>(Landroid/content/res/Resources;LX/0Or;LX/JyJ;)V

    .line 2754390
    move-object v0, v5

    .line 2754391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2754392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/profile/discovery/components/DiscoveryExpandedUnitComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2754393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2754394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;)Z
    .locals 1

    .prologue
    .line 2754395
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
