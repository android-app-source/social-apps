.class public Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1vg;

.field public final e:LX/JyJ;

.field private final f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2754521
    const-class v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Or;LX/1vg;LX/JyJ;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/1vg;",
            "LX/JyJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2754511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754512
    iput-object p1, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->b:Landroid/content/res/Resources;

    .line 2754513
    iput-object p2, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->c:LX/0Or;

    .line 2754514
    iput-object p3, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->d:LX/1vg;

    .line 2754515
    iput-object p4, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->e:LX/JyJ;

    .line 2754516
    iget-object v0, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->b:Landroid/content/res/Resources;

    const p1, 0x7f0b26d7

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2754517
    iget-object p1, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->b:Landroid/content/res/Resources;

    const p2, 0x7f0b26d8

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 2754518
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    move v0, v0

    .line 2754519
    iput v0, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->f:I

    .line 2754520
    return-void
.end method

.method private static a(LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;ZLX/1Ad;)LX/1Dh;
    .locals 4
    .param p1    # Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2754507
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    .line 2754508
    if-eqz p3, :cond_0

    .line 2754509
    const/4 v1, 0x3

    const v2, 0x7f0b26d8

    invoke-interface {v0, v1, v2}, LX/1Dh;->q(II)LX/1Dh;

    .line 2754510
    :cond_0
    invoke-static {p0, p1, p4}, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a(LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;LX/1Ad;)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v3, 0x7f0b26d8

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p0, p2, p4}, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a(LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;LX/1Ad;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;LX/1Ad;)LX/1Di;
    .locals 3
    .param p1    # Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2754484
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel$BucketItemModel$PersonModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2754485
    :goto_0
    invoke-static {p0}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v1

    invoke-virtual {p2, v0}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    sget-object v2, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b26d7

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b26d7

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    return-object v0

    .line 2754486
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;LX/1Ad;)LX/1Di;
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x2

    .line 2754498
    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v9, :cond_2

    .line 2754499
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    iget v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->f:I

    invoke-interface {v0, v1}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v0

    iget v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->f:I

    invoke-interface {v0, v1}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b26d9    # 1.849644E38f

    invoke-interface {v0, v1, v2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v7

    .line 2754500
    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v8

    .line 2754501
    invoke-virtual {v8, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    move-object v1, p1

    move-object v5, p3

    invoke-static/range {v1 .. v5}, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a(LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;ZLX/1Ad;)LX/1Dh;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2754502
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v11, :cond_0

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    move-object v2, v0

    :goto_0
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    invoke-virtual {v8, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;

    move-object v3, v0

    :goto_1
    move-object v1, p1

    move v4, v10

    move-object v5, p3

    invoke-static/range {v1 .. v5}, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a(LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketContentFieldsModel;ZLX/1Ad;)LX/1Dh;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-object v0, v7

    .line 2754503
    :goto_2
    return-object v0

    :cond_0
    move-object v2, v6

    .line 2754504
    goto :goto_0

    :cond_1
    move-object v3, v6

    goto :goto_1

    .line 2754505
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2754506
    :cond_3
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v0

    invoke-virtual {p3, v6}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    iget v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->f:I

    invoke-interface {v0, v1}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    iget v1, p0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->f:I

    invoke-interface {v0, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b26d9    # 1.849644E38f

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;
    .locals 7

    .prologue
    .line 2754487
    const-class v1, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;

    monitor-enter v1

    .line 2754488
    :try_start_0
    sget-object v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2754489
    sput-object v2, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2754490
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754491
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2754492
    new-instance v6, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/JyJ;->a(LX/0QB;)LX/JyJ;

    move-result-object v5

    check-cast v5, LX/JyJ;

    invoke-direct {v6, v3, p0, v4, v5}, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;-><init>(Landroid/content/res/Resources;LX/0Or;LX/1vg;LX/JyJ;)V

    .line 2754493
    move-object v0, v6

    .line 2754494
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2754495
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2754496
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2754497
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
