.class public Lcom/facebook/profile/discovery/DiscoveryCardsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private p:LX/EnP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/EnS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/Emx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/Jxw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2753467
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;
    .locals 14

    .prologue
    .line 2753432
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "discovery_bucket"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2753433
    if-eqz v0, :cond_0

    .line 2753434
    :goto_0
    return-object v0

    .line 2753435
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "bucketid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2753436
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2753437
    new-instance v1, LX/FTj;

    invoke-direct {v1}, LX/FTj;-><init>()V

    .line 2753438
    iput-object v0, v1, LX/FTj;->b:Ljava/lang/String;

    .line 2753439
    move-object v0, v1

    .line 2753440
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v13, 0x0

    .line 2753441
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2753442
    iget-object v3, v0, LX/FTj;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$BucketContentsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2753443
    iget-object v5, v0, LX/FTj;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2753444
    iget-object v7, v0, LX/FTj;->c:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$FbglyphImageModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2753445
    iget-object v8, v0, LX/FTj;->d:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel$ImageModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2753446
    iget-object v9, v0, LX/FTj;->e:Ljava/lang/String;

    invoke-virtual {v2, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2753447
    iget-object v10, v0, LX/FTj;->f:Ljava/lang/String;

    invoke-virtual {v2, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2753448
    iget-object v11, v0, LX/FTj;->h:Ljava/lang/String;

    invoke-virtual {v2, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2753449
    const/16 v12, 0x8

    invoke-virtual {v2, v12}, LX/186;->c(I)V

    .line 2753450
    invoke-virtual {v2, v13, v3}, LX/186;->b(II)V

    .line 2753451
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 2753452
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 2753453
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 2753454
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 2753455
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 2753456
    const/4 v3, 0x6

    iget v5, v0, LX/FTj;->g:I

    invoke-virtual {v2, v3, v5, v13}, LX/186;->a(III)V

    .line 2753457
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 2753458
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2753459
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2753460
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2753461
    invoke-virtual {v3, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2753462
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2753463
    new-instance v3, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;-><init>(LX/15i;)V

    .line 2753464
    move-object v0, v3

    .line 2753465
    goto/16 :goto_0

    .line 2753466
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "UTF-8 not supported"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static a(Lcom/facebook/profile/discovery/DiscoveryCardsActivity;LX/EnP;LX/EnS;LX/Emx;LX/Jxw;)V
    .locals 0

    .prologue
    .line 2753431
    iput-object p1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->p:LX/EnP;

    iput-object p2, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->q:LX/EnS;

    iput-object p3, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->r:LX/Emx;

    iput-object p4, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->s:LX/Jxw;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;

    invoke-static {v3}, LX/EnP;->a(LX/0QB;)LX/EnP;

    move-result-object v0

    check-cast v0, LX/EnP;

    invoke-static {v3}, LX/EnS;->a(LX/0QB;)LX/EnS;

    move-result-object v1

    check-cast v1, LX/EnS;

    invoke-static {v3}, LX/Emx;->a(LX/0QB;)LX/Emx;

    move-result-object v2

    check-cast v2, LX/Emx;

    invoke-static {v3}, LX/Jxw;->a(LX/0QB;)LX/Jxw;

    move-result-object v3

    check-cast v3, LX/Jxw;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->a(Lcom/facebook/profile/discovery/DiscoveryCardsActivity;LX/EnP;LX/EnS;LX/Emx;LX/Jxw;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 2753468
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2753469
    invoke-static {p0, p0}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2753470
    const v0, 0x7f0400db

    const v1, 0x7f040031

    invoke-virtual {p0, v0, v1}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->overridePendingTransition(II)V

    .line 2753471
    invoke-static {p0}, LX/EnP;->a(Landroid/content/Context;)Lcom/facebook/entitycards/intent/EntityCardsFragment;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2753472
    invoke-direct {p0}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    move-result-object v9

    .line 2753473
    new-instance v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;

    const-string v1, "people_discovery"

    .line 2753474
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2753475
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lb_()Ljava/lang/String;

    move-result-object v6

    move-object v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/entitycards/intent/EntityCardsParameters;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2753476
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    .line 2753477
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    const-string v3, "bucketid"

    invoke-virtual {v9}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2753478
    iget-object v3, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    const-string v4, "bucket_has_title"

    invoke-virtual {v9}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lb_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v7

    :goto_0
    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2753479
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2753480
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3}, LX/Jxx;->a(Landroid/content/Intent;Ljava/lang/Integer;)Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    move-result-object v3

    .line 2753481
    iget-object v4, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    const-string v5, "discovery_curation_logging_data"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2753482
    iget-object v4, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->s:LX/Jxw;

    iget-object v5, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    invoke-virtual {v4, v5}, LX/Jxw;->c(Landroid/os/Bundle;)LX/Jxq;

    move-result-object v4

    .line 2753483
    invoke-static {v1}, LX/Jxx;->a(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2753484
    invoke-virtual {v4}, LX/Jxq;->a()V

    .line 2753485
    :cond_0
    const-string v5, "discovery_section_type"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "candidate_position"

    const/4 v7, -0x1

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v7, "candidate_is_last"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2753486
    iget-object v7, v4, LX/Jxq;->b:LX/0Zb;

    const-string v8, "profile_discovery_event"

    const/4 p1, 0x0

    invoke-interface {v7, v8, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 2753487
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2753488
    const-string v8, "bucket_session_impression"

    invoke-static {v4, v7, v8}, LX/Jxq;->d(LX/Jxq;LX/0oG;Ljava/lang/String;)V

    .line 2753489
    invoke-static {v7, v9, v5, v6, v1}, LX/Jxq;->a(LX/0oG;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IZ)V

    .line 2753490
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 2753491
    :cond_1
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->r:LX/Emx;

    .line 2753492
    iget-object v4, v3, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2753493
    const-string v4, "people_discovery"

    invoke-virtual {v1, v3, v4, v2, v2}, LX/Emx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2753494
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->r:LX/Emx;

    sget-object v2, LX/Emw;->LAUNCH_ENTITY_CARD:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->a(LX/Emw;)V

    .line 2753495
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->r:LX/Emx;

    sget-object v2, LX/Emw;->ACTIVITY_CREATE:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->a(LX/Emw;)V

    .line 2753496
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2753497
    const-string v2, "entity_cards_fragment_parameters"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2753498
    const-string v0, "entity_cards_config_extras"

    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2753499
    invoke-static {p0, v1}, LX/EnP;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2753500
    :cond_2
    return-void

    :cond_3
    move v1, v8

    .line 2753501
    goto/16 :goto_0
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2753428
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2753429
    const v0, 0x7f0400b7

    const v1, 0x7f0400ee

    invoke-virtual {p0, v0, v1}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->overridePendingTransition(II)V

    .line 2753430
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    .line 2753361
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->q:LX/EnS;

    if-nez v0, :cond_1

    .line 2753362
    :cond_0
    :goto_0
    return-void

    .line 2753363
    :cond_1
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->s:LX/Jxw;

    .line 2753364
    iget-object p0, v0, LX/Jxw;->j:LX/Jxn;

    move-object v0, p0

    .line 2753365
    if-eqz v0, :cond_0

    .line 2753366
    const/4 v1, -0x1

    if-eq p2, v1, :cond_2

    .line 2753367
    :goto_1
    goto :goto_0

    .line 2753368
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 2753369
    :pswitch_0
    const-string v1, "curation_card_model_extra"

    invoke-static {p3, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2753370
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2753371
    iget-object v2, v0, LX/Jxn;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Jxm;

    invoke-direct {v3, v0, v1}, LX/Jxm;-><init>(LX/Jxn;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)V

    iget-object v1, v0, LX/Jxn;->d:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2753372
    :pswitch_1
    const-string v1, "bio_curation_card_model_extra"

    invoke-static {p3, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2753373
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2753374
    const-string v2, "saved_bio_result"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2753375
    invoke-static {v1}, LX/FT2;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)LX/FT2;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v4

    .line 2753376
    new-instance v5, LX/FT3;

    invoke-direct {v5}, LX/FT3;-><init>()V

    .line 2753377
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->x()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->a:Ljava/lang/String;

    .line 2753378
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->l()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->b:Z

    .line 2753379
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->m()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->c:Z

    .line 2753380
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->d()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->d:Z

    .line 2753381
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->e()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->e:Z

    .line 2753382
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->F()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->f:Z

    .line 2753383
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->n()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->g:Z

    .line 2753384
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->h:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    .line 2753385
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->H()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->i:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2753386
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2753387
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->k:Ljava/lang/String;

    .line 2753388
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->I()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->l:Z

    .line 2753389
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->z()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->m:Z

    .line 2753390
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->A()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->n:Z

    .line 2753391
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->ce_()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->o:Ljava/lang/String;

    .line 2753392
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->J()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->p:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2753393
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2753394
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->r:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2753395
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->M()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->s:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    .line 2753396
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->N()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->t:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    .line 2753397
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->D()Z

    move-result v6

    iput-boolean v6, v5, LX/FT3;->u:Z

    .line 2753398
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->O()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    .line 2753399
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2753400
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->P()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2753401
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->y:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2753402
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->Q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    .line 2753403
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->A:Ljava/lang/String;

    .line 2753404
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v6

    iput-object v6, v5, LX/FT3;->B:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2753405
    move-object v4, v5

    .line 2753406
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->L()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v5

    .line 2753407
    new-instance v6, LX/FT5;

    invoke-direct {v6}, LX/FT5;-><init>()V

    .line 2753408
    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object p0

    iput-object p0, v6, LX/FT5;->a:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2753409
    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object p0

    iput-object p0, v6, LX/FT5;->b:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    .line 2753410
    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object p0

    iput-object p0, v6, LX/FT5;->c:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2753411
    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object p0

    iput-object p0, v6, LX/FT5;->d:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2753412
    move-object v5, v6

    .line 2753413
    new-instance v6, LX/4ae;

    invoke-direct {v6}, LX/4ae;-><init>()V

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 v2, 0x0

    .line 2753414
    :cond_3
    iput-object v2, v6, LX/4ae;->a:Ljava/lang/String;

    .line 2753415
    move-object v2, v6

    .line 2753416
    invoke-virtual {v2}, LX/4ae;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    .line 2753417
    iput-object v2, v5, LX/FT5;->a:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2753418
    move-object v2, v5

    .line 2753419
    invoke-virtual {v2}, LX/FT5;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v2

    .line 2753420
    iput-object v2, v4, LX/FT3;->r:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    .line 2753421
    move-object v2, v4

    .line 2753422
    invoke-virtual {v2}, LX/FT3;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v2

    .line 2753423
    iput-object v2, v3, LX/FT2;->d:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2753424
    move-object v2, v3

    .line 2753425
    invoke-virtual {v2}, LX/FT2;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v2

    .line 2753426
    iget-object v3, v0, LX/Jxn;->e:LX/Eny;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, LX/Eny;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2753427
    iget-object v1, v0, LX/Jxn;->c:LX/1mR;

    const-string v2, "com.facebook.entitycardsplugins.discoverycuration.fetchers.discoveryCurationCardsCacheTag"

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2753355
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->p:LX/EnP;

    if-eqz v0, :cond_0

    .line 2753356
    invoke-static {p0}, LX/EnP;->a(Landroid/content/Context;)Lcom/facebook/entitycards/intent/EntityCardsFragment;

    move-result-object v0

    .line 2753357
    if-eqz v0, :cond_0

    .line 2753358
    const/4 v1, 0x1

    sget-object v2, LX/Emm;->BACK_BUTTON_TAP:LX/Emm;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a(ZLX/Emm;)V

    .line 2753359
    :goto_0
    return-void

    .line 2753360
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->finish()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x267e57b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2753352
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->s:LX/Jxw;

    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCardsActivity;->t:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, LX/Jxw;->a(Landroid/os/Bundle;)LX/Emj;

    move-result-object v1

    invoke-interface {v1}, LX/Emj;->b()V

    .line 2753353
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2753354
    const/16 v1, 0x23

    const v2, 0x7acc6799

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
