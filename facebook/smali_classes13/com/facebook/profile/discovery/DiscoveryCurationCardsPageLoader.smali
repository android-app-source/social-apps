.class public Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Enh;


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0se;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BS3;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0rq;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7H7;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/Jxq;

.field public final i:Ljava/lang/String;

.field private final j:Z

.field public final k:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0ut;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2753832
    const-class v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Jxq;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # LX/Jxq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2753824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753825
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2753826
    iput-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->g:LX/0Ot;

    .line 2753827
    iput-object p1, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->h:LX/Jxq;

    .line 2753828
    iput-object p2, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->i:Ljava/lang/String;

    .line 2753829
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->j:Z

    .line 2753830
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->k:LX/0UE;

    .line 2753831
    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2753780
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1My;LX/0TF;Ljava/lang/String;LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/0TF",
            "<*>;",
            "Ljava/lang/String;",
            "LX/Enq;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 2753798
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    invoke-interface {v0}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2753799
    :goto_0
    iget-object v0, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7H7;

    .line 2753800
    iget-object v1, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rq;

    .line 2753801
    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    .line 2753802
    iget-object v4, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->j:Z

    if-nez v4, :cond_1

    move v4, v5

    .line 2753803
    :goto_1
    new-instance v6, LX/FSr;

    invoke-direct {v6}, LX/FSr;-><init>()V

    move-object v6, v6

    .line 2753804
    const-string v7, "bucket_id"

    iget-object v8, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->i:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "fetch_title"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v6, "bucket_cursor"

    invoke-virtual {v4, v6, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "bucket_page_size"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v6, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v6, "cover_image_portrait_size"

    invoke-virtual {v0}, LX/7H7;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v4, "media_type"

    invoke-virtual {v1}, LX/0rq;->a()LX/0wF;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v4, "profile_pic_media_type"

    invoke-virtual {v1}, LX/0rq;->b()LX/0wF;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v4, "profile_image_size"

    const v6, 0x7f0b253a

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "tags_page_size"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FSr;

    .line 2753805
    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BS3;

    .line 2753806
    invoke-virtual {v2, v0}, LX/BS3;->a(LX/0gW;)V

    .line 2753807
    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0se;

    .line 2753808
    invoke-virtual {v1}, LX/0rq;->c()LX/0wF;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2753809
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2753810
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2753811
    move-object v0, v0

    .line 2753812
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v6, 0x15180

    invoke-virtual {v0, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2753813
    iput-boolean v5, v0, LX/0zO;->p:Z

    .line 2753814
    move-object v0, v0

    .line 2753815
    const-string v1, "com.facebook.entitycardsplugins.discoverycuration.fetchers.discoveryCurationCardsCacheTag"

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 2753816
    iput-object v1, v0, LX/0zO;->d:Ljava/util/Set;

    .line 2753817
    move-object v0, v0

    .line 2753818
    invoke-virtual {p1, v0, p2, p3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2753819
    new-instance v1, LX/Jxu;

    invoke-direct {v1, p0, v3}, LX/Jxu;-><init>(Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;Ljava/lang/String;)V

    .line 2753820
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2753821
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2753822
    :cond_0
    const/4 v0, 0x0

    move-object v3, v0

    goto/16 :goto_0

    .line 2753823
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/Eny;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;",
            "Lcom/facebook/entitycards/model/EntityCardMutationService;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2753786
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2753787
    instance-of v2, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;

    if-nez v2, :cond_1

    .line 2753788
    const-string v2, "discovery_curation_page_loader"

    const-string v3, "Expected a DiscoveryCurationBucketQueryModel, got %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2753789
    :cond_0
    :goto_0
    return-void

    .line 2753790
    :cond_1
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;

    .line 2753791
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;

    move-result-object v0

    .line 2753792
    if-nez v0, :cond_2

    .line 2753793
    const-string v0, "discovery_curation_page_loader"

    const-string v1, "DiscoveryCurationBucketQueryModel had null bucket contents"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2753794
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;

    .line 2753795
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2753796
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel$BucketContentsModel$NodesModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    invoke-virtual {p2, v4, v0}, LX/Eny;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2753797
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(LX/Enq;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2753781
    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    if-nez v2, :cond_2

    .line 2753782
    sget-object v2, LX/Enq;->RIGHT:LX/Enq;

    if-ne p1, v2, :cond_1

    .line 2753783
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2753784
    goto :goto_0

    .line 2753785
    :cond_2
    iget-object v2, p0, Lcom/facebook/profile/discovery/DiscoveryCurationCardsPageLoader;->l:LX/0ut;

    invoke-interface {v2}, LX/0ut;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, LX/Enq;->RIGHT:LX/Enq;

    if-eq p1, v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
