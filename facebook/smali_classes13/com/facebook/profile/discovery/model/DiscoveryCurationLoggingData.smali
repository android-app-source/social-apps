.class public Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/JyM;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2754710
    new-instance v0, LX/JyK;

    invoke-direct {v0}, LX/JyK;-><init>()V

    sput-object v0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2754711
    new-instance v0, LX/JyM;

    invoke-direct {v0}, LX/JyM;-><init>()V

    sput-object v0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->a:LX/JyM;

    return-void
.end method

.method public constructor <init>(LX/JyL;)V
    .locals 1

    .prologue
    .line 2754701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754702
    iget-object v0, p1, LX/JyL;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    .line 2754703
    iget-object v0, p1, LX/JyL;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    .line 2754704
    iget-object v0, p1, LX/JyL;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    .line 2754705
    iget-object v0, p1, LX/JyL;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    .line 2754706
    iget-object v0, p1, LX/JyL;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    .line 2754707
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2754708
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2754709
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2754686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754687
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 2754688
    iput-object v1, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    .line 2754689
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 2754690
    iput-object v1, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    .line 2754691
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    .line 2754692
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 2754693
    iput-object v1, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    .line 2754694
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 2754695
    iput-object v1, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    .line 2754696
    :goto_3
    return-void

    .line 2754697
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    goto :goto_0

    .line 2754698
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    goto :goto_1

    .line 2754699
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    goto :goto_2

    .line 2754700
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2754712
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2754671
    if-ne p0, p1, :cond_1

    .line 2754672
    :cond_0
    :goto_0
    return v0

    .line 2754673
    :cond_1
    instance-of v2, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    if-nez v2, :cond_2

    move v0, v1

    .line 2754674
    goto :goto_0

    .line 2754675
    :cond_2
    check-cast p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2754676
    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2754677
    goto :goto_0

    .line 2754678
    :cond_3
    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2754679
    goto :goto_0

    .line 2754680
    :cond_4
    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2754681
    goto :goto_0

    .line 2754682
    :cond_5
    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2754683
    goto :goto_0

    .line 2754684
    :cond_6
    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2754685
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2754670
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2754652
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2754653
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754654
    :goto_0
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2754655
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754656
    :goto_1
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2754657
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2754658
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754659
    :goto_2
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2754660
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754661
    :goto_3
    return-void

    .line 2754662
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754663
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 2754664
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754665
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 2754666
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754667
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 2754668
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2754669
    iget-object v0, p0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3
.end method
