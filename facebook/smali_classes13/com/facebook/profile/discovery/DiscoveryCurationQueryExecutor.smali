.class public Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;


# instance fields
.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2753930
    const-class v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2753931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753932
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;
    .locals 5

    .prologue
    .line 2753915
    sget-object v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->d:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    if-nez v0, :cond_1

    .line 2753916
    const-class v1, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    monitor-enter v1

    .line 2753917
    :try_start_0
    sget-object v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->d:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2753918
    if-eqz v2, :cond_0

    .line 2753919
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2753920
    new-instance v4, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    invoke-direct {v4}, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;-><init>()V

    .line 2753921
    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    .line 2753922
    iput-object p0, v4, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->b:LX/0Or;

    iput-object v3, v4, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->c:LX/0tX;

    .line 2753923
    move-object v0, v4

    .line 2753924
    sput-object v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->d:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2753925
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2753926
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2753927
    :cond_1
    sget-object v0, Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;->d:Lcom/facebook/profile/discovery/DiscoveryCurationQueryExecutor;

    return-object v0

    .line 2753928
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2753929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
