.class public Lcom/facebook/orca/notify/LoggedOutNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/orca/notify/LoggedOutNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2749067
    new-instance v0, LX/Jue;

    invoke-direct {v0}, LX/Jue;-><init>()V

    sput-object v0, Lcom/facebook/orca/notify/LoggedOutNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2749075
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2749076
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->a:Ljava/lang/String;

    .line 2749077
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->b:Ljava/lang/String;

    .line 2749078
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->c:Ljava/lang/String;

    .line 2749079
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->d:Z

    .line 2749080
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2749081
    sget-object v0, LX/3RF;->USER_LOGGED_OUT:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2749082
    iput-object p1, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->a:Ljava/lang/String;

    .line 2749083
    iput-object p2, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->b:Ljava/lang/String;

    .line 2749084
    iput-object p3, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->c:Ljava/lang/String;

    .line 2749085
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2749074
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2749068
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2749069
    iget-object v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2749070
    iget-object v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2749071
    iget-object v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2749072
    iget-boolean v0, p0, Lcom/facebook/orca/notify/LoggedOutNotification;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2749073
    return-void
.end method
