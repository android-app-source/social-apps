.class public Lcom/facebook/orca/notify/MessagesNotificationService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:LX/0Tn;

.field private static final b:LX/0Tn;


# instance fields
.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/28x;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2749358
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "debug_messenger_notificaiton_service_last_intent_action"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/orca/notify/MessagesNotificationService;->a:LX/0Tn;

    .line 2749359
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "debug_messenger_notificaiton_service_last_intent_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/orca/notify/MessagesNotificationService;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2749356
    const-string v0, "MessagesNotificationService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2749357
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2749103
    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/orca/notify/MessagesNotificationService;->b:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    sub-long/2addr v2, v0

    .line 2749104
    const-string v1, "Service was started with a null intent. Most Recent Intent\'s Action: %s,Time since last intent %s"

    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/orca/notify/MessagesNotificationService;->a:LX/0Tn;

    const-string v5, "<intent not found>"

    invoke-interface {v0, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2749105
    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "MessagesNotificationServiceError"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749106
    return-void
.end method

.method private a(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/28x;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2749351
    iput-object p1, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->c:LX/0Ot;

    .line 2749352
    iput-object p2, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->d:LX/0Ot;

    .line 2749353
    iput-object p3, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->e:LX/0Ot;

    .line 2749354
    iput-object p4, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->f:LX/0Ot;

    .line 2749355
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/orca/notify/MessagesNotificationService;

    const/16 v1, 0xeb5

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x259

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xf9a

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2db

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/facebook/orca/notify/MessagesNotificationService;->a(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/16 v0, 0x24

    const v1, 0x39f7b73f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2749110
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2749111
    if-nez p1, :cond_0

    .line 2749112
    invoke-direct {p0}, Lcom/facebook/orca/notify/MessagesNotificationService;->a()V

    .line 2749113
    const/16 v0, 0x25

    const v1, -0x47618b78

    invoke-static {v2, v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2749114
    :goto_0
    return-void

    .line 2749115
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 2749116
    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, Lcom/facebook/orca/notify/MessagesNotificationService;->a:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, Lcom/facebook/orca/notify/MessagesNotificationService;->b:LX/0Tn;

    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    invoke-interface {v2, v3, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2749117
    iget-object v0, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/28x;

    .line 2749118
    sget-object v2, LX/2b2;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2749119
    const/4 v3, 0x0

    .line 2749120
    :try_start_0
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/NewMessageNotification;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2749121
    :goto_1
    if-eqz v1, :cond_1

    .line 2749122
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749123
    invoke-static {v0, v1}, LX/28x;->c(LX/28x;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 2749124
    iget-object v2, v0, LX/28x;->z:LX/2BG;

    invoke-virtual {v2}, LX/2BG;->a()V

    .line 2749125
    :cond_1
    :goto_2
    const v0, 0x62224ffd

    invoke-static {v0, v4}, LX/02F;->d(II)V

    goto :goto_0

    .line 2749126
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 2749127
    iget-object v1, p0, Lcom/facebook/orca/notify/MessagesNotificationService;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v5, "invalid_notification_parcelable"

    invoke-virtual {v1, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_1

    .line 2749128
    :cond_2
    sget-object v2, LX/2b2;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2749129
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    .line 2749130
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749131
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749132
    goto :goto_2

    .line 2749133
    :cond_3
    sget-object v2, LX/2b2;->c:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2749134
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/FriendInstallNotification;

    .line 2749135
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749136
    iget-object v2, v1, Lcom/facebook/messaging/notify/FriendInstallNotification;->e:Lcom/facebook/push/PushProperty;

    iget-object v2, v2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    move-object v2, v2

    .line 2749137
    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2749138
    iget-object v2, v1, Lcom/facebook/messaging/notify/FriendInstallNotification;->e:Lcom/facebook/push/PushProperty;

    iget-object v2, v2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    move-object v6, v2

    .line 2749139
    const-string v7, "10003"

    .line 2749140
    iget-object v2, v0, LX/28x;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-nez v2, :cond_30

    .line 2749141
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    const-string v3, "logged_out_user"

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2749142
    :goto_3
    goto :goto_2

    .line 2749143
    :cond_4
    sget-object v2, LX/2b2;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2749144
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/PaymentNotification;

    .line 2749145
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749146
    iget-object v2, v1, Lcom/facebook/messaging/notify/PaymentNotification;->h:Lcom/facebook/push/PushProperty;

    iget-object v2, v2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2749147
    iget-object v2, v1, Lcom/facebook/messaging/notify/PaymentNotification;->h:Lcom/facebook/push/PushProperty;

    iget-object v6, v2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    .line 2749148
    const-string v7, "10014"

    .line 2749149
    iget-object v2, v0, LX/28x;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-nez v2, :cond_34

    .line 2749150
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    const-string v3, "logged_out_user"

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2749151
    :goto_4
    goto/16 :goto_2

    .line 2749152
    :cond_5
    sget-object v2, LX/2b2;->e:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2749153
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/MissedCallNotification;

    .line 2749154
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749155
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749156
    goto/16 :goto_2

    .line 2749157
    :cond_6
    sget-object v2, LX/2b2;->f:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2749158
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/IncomingCallNotification;

    .line 2749159
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749160
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749161
    goto/16 :goto_2

    .line 2749162
    :cond_7
    sget-object v2, LX/2b2;->g:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2749163
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/CalleeReadyNotification;

    .line 2749164
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749165
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749166
    goto/16 :goto_2

    .line 2749167
    :cond_8
    sget-object v2, LX/2b2;->h:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2749168
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/ReadThreadNotification;

    .line 2749169
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749170
    iget-object v2, v1, Lcom/facebook/messaging/notify/ReadThreadNotification;->a:LX/0P1;

    move-object v2, v2

    .line 2749171
    invoke-virtual {v2}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2749172
    iget-object v5, v0, LX/28x;->f:LX/0c4;

    invoke-static {v2, v5}, LX/6bW;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0c4;)V

    goto :goto_5

    .line 2749173
    :cond_9
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749174
    goto/16 :goto_2

    .line 2749175
    :cond_a
    sget-object v2, LX/2b2;->i:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2749176
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/NewBuildNotification;

    .line 2749177
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749178
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749179
    goto/16 :goto_2

    .line 2749180
    :cond_b
    sget-object v2, LX/2b2;->j:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2749181
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;

    .line 2749182
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749183
    invoke-virtual {v1}, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->d()LX/3B4;

    move-result-object v2

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2749184
    invoke-virtual {v1}, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->e()Ljava/lang/String;

    move-result-object v6

    .line 2749185
    const-string v7, "10004"

    .line 2749186
    invoke-static {v0}, LX/28x;->n(LX/28x;)Z

    move-result v2

    if-nez v2, :cond_37

    .line 2749187
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    const-string v3, "notifications_disabled"

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2749188
    :goto_6
    goto/16 :goto_2

    .line 2749189
    :cond_c
    const-string v2, "ACTION_MQTT_NO_AUTH"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2749190
    iget-object v1, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 2749191
    const/4 v2, 0x0

    .line 2749192
    iget-object v1, v0, LX/28x;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/28x;->a:LX/0Tn;

    invoke-interface {v1, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_3c

    iget-object v1, v0, LX/28x;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->l()Z

    move-result v1

    if-nez v1, :cond_3c

    const/4 v1, 0x1

    :goto_7
    move v1, v1

    .line 2749193
    if-nez v1, :cond_39

    .line 2749194
    :goto_8
    goto/16 :goto_2

    .line 2749195
    :cond_d
    sget-object v2, LX/2b2;->k:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2749196
    const-string v1, "thread_key_string"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2749197
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2749198
    if-eqz v1, :cond_1

    .line 2749199
    const-string v2, "notification"

    invoke-virtual {v0, v1, v2}, LX/28x;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2749200
    :cond_e
    sget-object v2, LX/2b2;->l:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2749201
    const-string v1, "thread_key_string"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2749202
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2749203
    if-eqz v1, :cond_1

    .line 2749204
    iget-object v3, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3R9;

    .line 2749205
    invoke-virtual {v3, v1}, LX/3R9;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_9

    .line 2749206
    :cond_f
    goto/16 :goto_2

    .line 2749207
    :cond_10
    sget-object v2, LX/2b2;->p:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2749208
    const-string v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2749209
    iget-object v2, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3R9;

    .line 2749210
    invoke-virtual {v2, v1}, LX/3R9;->b(Ljava/lang/String;)V

    goto :goto_a

    .line 2749211
    :cond_11
    goto/16 :goto_2

    .line 2749212
    :cond_12
    sget-object v2, LX/2b2;->m:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 2749213
    iget-object v1, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3R9;

    .line 2749214
    sget-object v3, LX/3RF;->NEW_BUILD:LX/3RF;

    invoke-virtual {v1, v3}, LX/3R9;->a(LX/3RF;)V

    goto :goto_b

    .line 2749215
    :cond_13
    goto/16 :goto_2

    .line 2749216
    :cond_14
    sget-object v2, LX/2b2;->q:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 2749217
    const-string v1, "multiple_accounts_user_ids"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2749218
    iget-object v2, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3R9;

    .line 2749219
    invoke-virtual {v2, v1}, LX/3R9;->a(Ljava/util/List;)V

    goto :goto_c

    .line 2749220
    :cond_15
    goto/16 :goto_2

    .line 2749221
    :cond_16
    sget-object v2, LX/2b2;->o:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2749222
    const-string v1, "notification"

    invoke-virtual {v0, v1}, LX/28x;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2749223
    :cond_17
    sget-object v2, LX/2b2;->s:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 2749224
    const-string v1, "folder_counts"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2749225
    iput-object v1, v0, LX/28x;->E:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2749226
    iget-object v2, v0, LX/28x;->e:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/orca/notify/MessagesNotificationManager$3;

    invoke-direct {v3, v0}, Lcom/facebook/orca/notify/MessagesNotificationManager$3;-><init>(LX/28x;)V

    const v5, 0x791844

    invoke-static {v2, v3, v5}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2749227
    goto/16 :goto_2

    .line 2749228
    :cond_18
    sget-object v2, LX/2b2;->t:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 2749229
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/PromotionNotification;

    .line 2749230
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749231
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749232
    goto/16 :goto_2

    .line 2749233
    :cond_19
    sget-object v2, LX/2b2;->x:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 2749234
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/MentionNotification;

    .line 2749235
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749236
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749237
    goto/16 :goto_2

    .line 2749238
    :cond_1a
    sget-object v2, LX/2b2;->u:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2749239
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/StaleNotification;

    .line 2749240
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749241
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749242
    goto/16 :goto_2

    .line 2749243
    :cond_1b
    sget-object v2, LX/2b2;->v:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 2749244
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/MessageRequestNotification;

    .line 2749245
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749246
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749247
    goto/16 :goto_2

    .line 2749248
    :cond_1c
    sget-object v2, LX/2b2;->w:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2749249
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/TincanMessageRequestNotification;

    .line 2749250
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749251
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749252
    goto/16 :goto_2

    .line 2749253
    :cond_1d
    sget-object v2, LX/2b2;->n:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2749254
    iget-object v1, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3R9;

    .line 2749255
    sget-object v3, LX/3RF;->TINCAN_MESSAGE_REQUEST:LX/3RF;

    invoke-virtual {v1, v3}, LX/3R9;->a(LX/3RF;)V

    goto :goto_d

    .line 2749256
    :cond_1e
    goto/16 :goto_2

    .line 2749257
    :cond_1f
    sget-object v2, LX/2b2;->y:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 2749258
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    .line 2749259
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749260
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749261
    goto/16 :goto_2

    .line 2749262
    :cond_20
    sget-object v2, LX/2b2;->z:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 2749263
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    .line 2749264
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749265
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749266
    goto/16 :goto_2

    .line 2749267
    :cond_21
    sget-object v2, LX/2b2;->A:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 2749268
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;

    .line 2749269
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749270
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749271
    goto/16 :goto_2

    .line 2749272
    :cond_22
    sget-object v2, LX/2b2;->B:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 2749273
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/JoinRequestNotification;

    .line 2749274
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749275
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749276
    goto/16 :goto_2

    .line 2749277
    :cond_23
    sget-object v2, LX/2b2;->C:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 2749278
    const-string v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "user_display_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2749279
    iget-object v3, v0, LX/28x;->x:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2749280
    iget-object v5, v0, LX/28x;->j:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_24

    invoke-static {v1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 2749281
    :cond_24
    :goto_e
    goto/16 :goto_2

    .line 2749282
    :cond_25
    sget-object v2, LX/2b2;->D:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 2749283
    iget-object v1, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3R9;

    .line 2749284
    invoke-virtual {v1}, LX/3R9;->a()V

    goto :goto_f

    .line 2749285
    :cond_26
    goto/16 :goto_2

    .line 2749286
    :cond_27
    sget-object v2, LX/2b2;->E:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 2749287
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/EventReminderNotification;

    .line 2749288
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749289
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749290
    goto/16 :goto_2

    .line 2749291
    :cond_28
    sget-object v2, LX/2b2;->F:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 2749292
    iget-object v1, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 2749293
    new-instance v1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;

    iget-object v2, v0, LX/28x;->c:Landroid/content/Context;

    const v3, 0x7f080011

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/28x;->c:Landroid/content/Context;

    const v5, 0x7f083b2e

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, v0, LX/28x;->c:Landroid/content/Context;

    const v6, 0x7f083b2f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v5}, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749294
    goto/16 :goto_2

    .line 2749295
    :cond_29
    sget-object v2, LX/2b2;->r:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 2749296
    iget-object v1, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3R9;

    .line 2749297
    invoke-virtual {v1}, LX/3R9;->b()V

    goto :goto_10

    .line 2749298
    :cond_2a
    goto/16 :goto_2

    .line 2749299
    :cond_2b
    sget-object v2, LX/2b2;->G:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 2749300
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    .line 2749301
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749302
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749303
    goto/16 :goto_2

    .line 2749304
    :cond_2c
    sget-object v2, LX/2b2;->H:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 2749305
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    .line 2749306
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749307
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749308
    goto/16 :goto_2

    .line 2749309
    :cond_2d
    sget-object v2, LX/2b2;->I:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 2749310
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    .line 2749311
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749312
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749313
    goto/16 :goto_2

    .line 2749314
    :cond_2e
    sget-object v2, LX/2b2;->J:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 2749315
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    .line 2749316
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749317
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749318
    goto/16 :goto_2

    .line 2749319
    :cond_2f
    sget-object v2, LX/2b2;->K:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2749320
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/notify/MessageReactionNotification;

    .line 2749321
    iget-object v2, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2749322
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749323
    goto/16 :goto_2

    .line 2749324
    :cond_30
    iget-object v2, v0, LX/28x;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2gZ;

    .line 2749325
    iget-object v3, v1, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2749326
    const/4 p0, 0x1

    invoke-virtual {v2, v3, p0}, LX/2gZ;->a(Ljava/lang/String;Z)V

    .line 2749327
    iget-object v2, v0, LX/28x;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0db;->ak:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v2, v3, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    move v2, v2

    .line 2749328
    if-eqz v2, :cond_31

    invoke-static {v0}, LX/28x;->n(LX/28x;)Z

    move-result v2

    if-nez v2, :cond_32

    .line 2749329
    :cond_31
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    const-string v3, "notifications_disabled"

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2749330
    :cond_32
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749331
    iget-boolean v2, v1, Lcom/facebook/messaging/notify/FriendInstallNotification;->f:Z

    move v2, v2

    .line 2749332
    if-eqz v2, :cond_33

    const-string v2, "user_alerted_"

    move-object v3, v2

    .line 2749333
    :goto_11
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2749334
    :cond_33
    const-string v2, "user_not_alerted_"

    move-object v3, v2

    goto :goto_11

    .line 2749335
    :cond_34
    invoke-static {v0}, LX/28x;->n(LX/28x;)Z

    move-result v2

    if-nez v2, :cond_35

    .line 2749336
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    const-string v3, "notifications_disabled"

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2749337
    :cond_35
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749338
    iget-boolean v2, v1, Lcom/facebook/messaging/notify/PaymentNotification;->k:Z

    move v2, v2

    .line 2749339
    if-eqz v2, :cond_36

    const-string v2, "user_alerted_"

    move-object v3, v2

    .line 2749340
    :goto_12
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2749341
    :cond_36
    const-string v2, "user_not_alerted_"

    move-object v3, v2

    goto :goto_12

    .line 2749342
    :cond_37
    invoke-static {v0, v1}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 2749343
    iget-boolean v2, v1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->d:Z

    move v2, v2

    .line 2749344
    if-eqz v2, :cond_38

    const-string v2, "user_alerted_"

    move-object v3, v2

    .line 2749345
    :goto_13
    iget-object v2, v0, LX/28x;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2bC;

    invoke-virtual {v2, v5, v6, v7, v3}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2749346
    :cond_38
    const-string v2, "user_not_alerted_"

    move-object v3, v2

    goto :goto_13

    .line 2749347
    :cond_39
    iget-object v1, v0, LX/28x;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/28x;->a:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2749348
    new-instance v2, Lcom/facebook/orca/notify/LoggedOutNotification;

    iget-object v1, v0, LX/28x;->c:Landroid/content/Context;

    const v3, 0x7f0801ff

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, v0, LX/28x;->c:Landroid/content/Context;

    iget-boolean v1, v0, LX/28x;->i:Z

    if-eqz v1, :cond_3a

    const v1, 0x7f080203

    :goto_14
    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/28x;->c:Landroid/content/Context;

    iget-boolean v1, v0, LX/28x;->i:Z

    if-eqz v1, :cond_3b

    const v1, 0x7f080202

    :goto_15
    invoke-virtual {v6, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v5, v1}, Lcom/facebook/orca/notify/LoggedOutNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    goto/16 :goto_8

    :cond_3a
    const v1, 0x7f080201

    goto :goto_14

    :cond_3b
    const v1, 0x7f080200

    goto :goto_15

    :cond_3c
    move v1, v2

    goto/16 :goto_7

    .line 2749349
    :cond_3d
    iget-object v3, v0, LX/28x;->l:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->b()V

    .line 2749350
    new-instance v3, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;

    iget-object v5, v0, LX/28x;->c:Landroid/content/Context;

    const v6, 0x7f080011

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/28x;->c:Landroid/content/Context;

    const v7, 0x7f080557

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v2, p0, p1

    invoke-virtual {v6, v7, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, LX/28x;->c:Landroid/content/Context;

    const p0, 0x7f080558

    invoke-virtual {v7, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v6, v7}, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v3}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    goto/16 :goto_e
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x26c4eaaf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2749107
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2749108
    invoke-static {p0, p0}, Lcom/facebook/orca/notify/MessagesNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2749109
    const/16 v1, 0x25

    const v2, 0x28d11398

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
