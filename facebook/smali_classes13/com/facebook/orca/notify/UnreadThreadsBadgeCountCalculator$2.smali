.class public final Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2Q0;


# direct methods
.method public constructor <init>(LX/2Q0;)V
    .locals 0

    .prologue
    .line 2749509
    iput-object p1, p0, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2749510
    iget-object v0, p0, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    .line 2749511
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v3

    sget-object v4, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 2749512
    iput-object v4, v3, LX/6iI;->a:LX/0rS;

    .line 2749513
    move-object v3, v3

    .line 2749514
    iput-object v1, v3, LX/6iI;->b:LX/6ek;

    .line 2749515
    move-object v3, v3

    .line 2749516
    const/16 v4, 0x14

    .line 2749517
    iput v4, v3, LX/6iI;->f:I

    .line 2749518
    move-object v3, v3

    .line 2749519
    invoke-virtual {v3}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v3

    .line 2749520
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2749521
    const-string v4, "fetchThreadListParams"

    invoke-virtual {v5, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2749522
    iget-object v3, v0, LX/2Q0;->e:LX/0aG;

    const-string v4, "fetch_thread_list"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v7, LX/2Q0;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, 0x595b0704

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    move-object v0, v3

    .line 2749523
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/Juq;

    invoke-direct {v1, p0}, LX/Juq;-><init>(Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;)V

    iget-object v2, p0, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;->a:LX/2Q0;

    iget-object v2, v2, LX/2Q0;->c:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2749524
    return-void
.end method
