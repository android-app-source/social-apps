.class public Lcom/facebook/orca/notify/SwitchToFbAccountNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/orca/notify/SwitchToFbAccountNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2749406
    new-instance v0, LX/Juk;

    invoke-direct {v0}, LX/Juk;-><init>()V

    sput-object v0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2749407
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2749408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->a:Ljava/lang/String;

    .line 2749409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->b:Ljava/lang/String;

    .line 2749410
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->c:Ljava/lang/String;

    .line 2749411
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->d:Z

    .line 2749412
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2749413
    sget-object v0, LX/3RF;->SWITCH_TO_FB_ACCOUNT:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2749414
    iput-object p1, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->a:Ljava/lang/String;

    .line 2749415
    iput-object p2, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->b:Ljava/lang/String;

    .line 2749416
    iput-object p3, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->c:Ljava/lang/String;

    .line 2749417
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2749418
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2749419
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2749420
    iget-object v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2749421
    iget-object v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2749422
    iget-object v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2749423
    iget-boolean v0, p0, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2749424
    return-void
.end method
