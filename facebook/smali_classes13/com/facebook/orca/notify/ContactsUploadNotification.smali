.class public Lcom/facebook/orca/notify/ContactsUploadNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/orca/notify/ContactsUploadNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2748685
    new-instance v0, LX/JuN;

    invoke-direct {v0}, LX/JuN;-><init>()V

    sput-object v0, Lcom/facebook/orca/notify/ContactsUploadNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2748686
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2748687
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->a:Ljava/lang/String;

    .line 2748688
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->b:Ljava/lang/String;

    .line 2748689
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->c:Ljava/lang/String;

    .line 2748690
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->d:Z

    .line 2748691
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2748692
    sget-object v0, LX/3RF;->CONTACTS_UPLOAD:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2748693
    iput-object p1, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->a:Ljava/lang/String;

    .line 2748694
    iput-object p2, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->b:Ljava/lang/String;

    .line 2748695
    iput-object p3, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->c:Ljava/lang/String;

    .line 2748696
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2748697
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2748698
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2748699
    iget-object v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2748700
    iget-object v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2748701
    iget-object v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2748702
    iget-boolean v0, p0, Lcom/facebook/orca/notify/ContactsUploadNotification;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2748703
    return-void
.end method
