.class public Lcom/facebook/messengercar/CarNotificationService;
.super LX/1ZN;
.source ""


# instance fields
.field private a:LX/0Uq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/Di5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/messaging/cache/ReadThreadManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/It1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/messaging/send/client/SendMessageManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2745934
    const-string v0, "CarNotificationService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2745935
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 2745936
    iget-object v0, p0, Lcom/facebook/messengercar/CarNotificationService;->c:Lcom/facebook/messaging/cache/ReadThreadManager;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/cache/ReadThreadManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2745937
    iget-object v0, p0, Lcom/facebook/messengercar/CarNotificationService;->b:LX/Di5;

    invoke-virtual {v0, p1}, LX/Di5;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2745938
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2745939
    iget-object v0, p0, Lcom/facebook/messengercar/CarNotificationService;->d:LX/It1;

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/It1;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2745940
    iget-object v1, p0, Lcom/facebook/messengercar/CarNotificationService;->f:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/messengercar/CarNotificationService$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/messengercar/CarNotificationService$1;-><init>(Lcom/facebook/messengercar/CarNotificationService;Lcom/facebook/messaging/model/messages/Message;)V

    const v0, 0x402d6040    # 2.7089996f

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2745941
    return-void
.end method

.method private static a(Lcom/facebook/messengercar/CarNotificationService;LX/0Uq;LX/Di5;Lcom/facebook/messaging/cache/ReadThreadManager;LX/It1;Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 2745942
    iput-object p1, p0, Lcom/facebook/messengercar/CarNotificationService;->a:LX/0Uq;

    iput-object p2, p0, Lcom/facebook/messengercar/CarNotificationService;->b:LX/Di5;

    iput-object p3, p0, Lcom/facebook/messengercar/CarNotificationService;->c:Lcom/facebook/messaging/cache/ReadThreadManager;

    iput-object p4, p0, Lcom/facebook/messengercar/CarNotificationService;->d:LX/It1;

    iput-object p5, p0, Lcom/facebook/messengercar/CarNotificationService;->e:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p6, p0, Lcom/facebook/messengercar/CarNotificationService;->f:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/messengercar/CarNotificationService;

    invoke-static {v6}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v1

    check-cast v1, LX/0Uq;

    invoke-static {v6}, LX/Di5;->a(LX/0QB;)LX/Di5;

    move-result-object v2

    check-cast v2, LX/Di5;

    invoke-static {v6}, Lcom/facebook/messaging/cache/ReadThreadManager;->a(LX/0QB;)Lcom/facebook/messaging/cache/ReadThreadManager;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/cache/ReadThreadManager;

    invoke-static {v6}, LX/It1;->a(LX/0QB;)LX/It1;

    move-result-object v4

    check-cast v4, LX/It1;

    invoke-static {v6}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static/range {v0 .. v6}, Lcom/facebook/messengercar/CarNotificationService;->a(Lcom/facebook/messengercar/CarNotificationService;LX/0Uq;LX/Di5;Lcom/facebook/messaging/cache/ReadThreadManager;LX/It1;Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method private static b(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2745943
    const/4 v0, 0x0

    .line 2745944
    invoke-static {p0}, LX/3qL;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    .line 2745945
    if-eqz v1, :cond_0

    .line 2745946
    const-string v0, "voice_reply"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2745947
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x743ef1f0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2745948
    iget-object v0, p0, Lcom/facebook/messengercar/CarNotificationService;->a:LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 2745949
    const-string v0, "thread_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2745950
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const/4 v1, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2745951
    :goto_1
    const v0, 0x55bdc799

    invoke-static {v0, v2}, LX/02F;->d(II)V

    return-void

    .line 2745952
    :sswitch_0
    const-string v4, "read_thread"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "reply"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 2745953
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/facebook/messengercar/CarNotificationService;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_1

    .line 2745954
    :pswitch_1
    invoke-static {p1}, Lcom/facebook/messengercar/CarNotificationService;->b(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/messengercar/CarNotificationService;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x21fa232d -> :sswitch_0
        0x67612ea -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x19b02e9b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2745955
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2745956
    invoke-static {p0, p0}, Lcom/facebook/messengercar/CarNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2745957
    const/16 v1, 0x25

    const v2, -0x15e1e21c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
