.class public Lcom/facebook/messaging/blocking/BlockingUtils;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:LX/0aG;

.field public b:LX/0TD;

.field private c:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/blocking/annotations/IsBlockeeExperienceEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/1CX;

.field public e:Landroid/content/Context;

.field private f:LX/0tX;

.field private g:Ljava/util/concurrent/ExecutorService;

.field public h:LX/CKT;

.field public final i:Z

.field public final j:LX/DbA;

.field public k:LX/FM9;

.field public l:Ljava/util/concurrent/ExecutorService;

.field public m:LX/2Ow;


# direct methods
.method public constructor <init>(LX/0aG;LX/0TD;LX/0Or;LX/1CX;Landroid/content/Context;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/CKT;Ljava/lang/Boolean;LX/DbA;LX/FM9;Ljava/util/concurrent/ExecutorService;LX/2Ow;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/blocking/annotations/IsBlockeeExperienceEnabled;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p9    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1CX;",
            "Landroid/content/Context;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/CKT;",
            "Ljava/lang/Boolean;",
            "LX/DbA;",
            "LX/FM9;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2Ow;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2719685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2719686
    iput-object p1, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->a:LX/0aG;

    .line 2719687
    iput-object p2, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->b:LX/0TD;

    .line 2719688
    iput-object p3, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->c:LX/0Or;

    .line 2719689
    iput-object p4, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->d:LX/1CX;

    .line 2719690
    iput-object p5, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->e:Landroid/content/Context;

    .line 2719691
    iput-object p6, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->f:LX/0tX;

    .line 2719692
    iput-object p7, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->g:Ljava/util/concurrent/ExecutorService;

    .line 2719693
    iput-object p8, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->h:LX/CKT;

    .line 2719694
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->i:Z

    .line 2719695
    iput-object p10, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->j:LX/DbA;

    .line 2719696
    iput-object p11, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->k:LX/FM9;

    .line 2719697
    iput-object p12, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->l:Ljava/util/concurrent/ExecutorService;

    .line 2719698
    iput-object p13, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->m:LX/2Ow;

    .line 2719699
    return-void
.end method

.method public static a(Lcom/facebook/messaging/blocking/BlockingUtils;Ljava/lang/String;Z)LX/1MF;
    .locals 6

    .prologue
    .line 2719670
    new-instance v0, LX/8wm;

    invoke-direct {v0}, LX/8wm;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_MESSAGES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 2719671
    iput-object v1, v0, LX/8wm;->a:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 2719672
    move-object v0, v0

    .line 2719673
    sget-object v1, LX/0wD;->MESSENGER:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    .line 2719674
    iput-object v1, v0, LX/8wm;->c:Ljava/lang/String;

    .line 2719675
    move-object v0, v0

    .line 2719676
    iput-object p1, v0, LX/8wm;->b:Ljava/lang/String;

    .line 2719677
    move-object v0, v0

    .line 2719678
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2719679
    iput-object v1, v0, LX/8wm;->d:Ljava/lang/Boolean;

    .line 2719680
    move-object v0, v0

    .line 2719681
    new-instance v1, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;

    invoke-direct {v1, v0}, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;-><init>(LX/8wm;)V

    move-object v0, v1

    .line 2719682
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2719683
    const-string v1, "negativeFeedbackActionOnReportableEntityParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2719684
    iget-object v0, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->a:LX/0aG;

    const-string v1, "negative_feedback_action_on_reportable_entity"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, LX/Jdl;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x3490f666

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/user/model/User;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2719667
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719668
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    .line 2719669
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/blocking/BlockingUtils;LX/4H5;LX/Jdf;)V
    .locals 3

    .prologue
    .line 2719700
    new-instance v0, LX/JeO;

    invoke-direct {v0}, LX/JeO;-><init>()V

    move-object v0, v0

    .line 2719701
    const-string v1, "input"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2719702
    iget-object v1, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->f:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/JdX;

    invoke-direct {v1, p0, p2}, LX/JdX;-><init>(Lcom/facebook/messaging/blocking/BlockingUtils;LX/Jdf;)V

    iget-object v2, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2719703
    return-void
.end method

.method private a(Ljava/lang/String;ZLX/2h0;)V
    .locals 2
    .param p3    # LX/2h0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2719663
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/messaging/blocking/BlockingUtils;Ljava/lang/String;Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2719664
    if-eqz p3, :cond_0

    .line 2719665
    iget-object v1, p0, Lcom/facebook/messaging/blocking/BlockingUtils;->b:LX/0TD;

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2719666
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/blocking/BlockingUtils;
    .locals 14

    .prologue
    .line 2719661
    new-instance v0, Lcom/facebook/messaging/blocking/BlockingUtils;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, LX/0TD;

    const/16 v3, 0x14fd

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v4

    check-cast v4, LX/1CX;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/CKT;->b(LX/0QB;)LX/CKT;

    move-result-object v8

    check-cast v8, LX/CKT;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static {p0}, LX/DbA;->a(LX/0QB;)LX/DbA;

    move-result-object v10

    check-cast v10, LX/DbA;

    invoke-static {p0}, LX/FM9;->a(LX/0QB;)LX/FM9;

    move-result-object v11

    check-cast v11, LX/FM9;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v13

    check-cast v13, LX/2Ow;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/messaging/blocking/BlockingUtils;-><init>(LX/0aG;LX/0TD;LX/0Or;LX/1CX;Landroid/content/Context;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/CKT;Ljava/lang/Boolean;LX/DbA;LX/FM9;Ljava/util/concurrent/ExecutorService;LX/2Ow;)V

    .line 2719662
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/2h0;)V
    .locals 1
    .param p2    # LX/2h0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2719659
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Ljava/lang/String;ZLX/2h0;)V

    .line 2719660
    return-void
.end method

.method public final b(Ljava/lang/String;LX/2h0;)V
    .locals 1
    .param p2    # LX/2h0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2719657
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Ljava/lang/String;ZLX/2h0;)V

    .line 2719658
    return-void
.end method
