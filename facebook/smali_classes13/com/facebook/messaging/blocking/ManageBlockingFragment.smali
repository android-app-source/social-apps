.class public Lcom/facebook/messaging/blocking/ManageBlockingFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/Jda;


# instance fields
.field public m:LX/Jdl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/26j;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/facebook/user/model/User;

.field private p:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/messaging/blocking/ManageBlockingParam;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/Jrq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2719708
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2719709
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2719710
    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->n:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/messaging/blocking/ManageBlockingFragment;

    invoke-static {v2}, LX/Jdl;->b(LX/0QB;)LX/Jdl;

    move-result-object v1

    check-cast v1, LX/Jdl;

    const/16 p0, 0xdcc

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->m:LX/Jdl;

    iput-object v2, p1, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->n:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2719711
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2719712
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 2719713
    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x8f15ae

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2719714
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2719715
    const-class v0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2719716
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2719717
    if-eqz p1, :cond_1

    .line 2719718
    const-string v0, "arg_blockee"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->o:Lcom/facebook/user/model/User;

    .line 2719719
    const-string v0, "arg_thread_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2719720
    const-string v0, "arg_param"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/blocking/ManageBlockingParam;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->q:Lcom/facebook/messaging/blocking/ManageBlockingParam;

    .line 2719721
    :cond_0
    :goto_0
    const v0, 0x3a611ca

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2719722
    :cond_1
    if-eqz v2, :cond_0

    .line 2719723
    const-string v0, "arg_blockee"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->o:Lcom/facebook/user/model/User;

    .line 2719724
    const-string v0, "arg_thread_key"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2719725
    const-string v0, "arg_param"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/blocking/ManageBlockingParam;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->q:Lcom/facebook/messaging/blocking/ManageBlockingParam;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, -0x1b52dcfc

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2719726
    const v0, 0x7f030a6c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2719727
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2719728
    iget-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->m:LX/Jdl;

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->o:Lcom/facebook/user/model/User;

    iget-object v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->q:Lcom/facebook/messaging/blocking/ManageBlockingParam;

    .line 2719729
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v5, v5

    .line 2719730
    iput-object v1, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719731
    iput-object v2, v0, LX/Jdl;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2719732
    iput-object v5, v0, LX/Jdl;->e:LX/0gc;

    .line 2719733
    const/4 p1, 0x0

    .line 2719734
    instance-of v8, v3, Lcom/facebook/messaging/blocking/AdManageBlockingParam;

    if-eqz v8, :cond_1

    move-object v8, v3

    check-cast v8, Lcom/facebook/messaging/blocking/AdManageBlockingParam;

    iget-object v8, v8, Lcom/facebook/messaging/blocking/AdManageBlockingParam;->a:Ljava/lang/String;

    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x1

    move p0, v8

    .line 2719735
    :goto_0
    const v8, 0x7f0d1a8a

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 2719736
    if-nez p0, :cond_2

    .line 2719737
    const/16 p0, 0x8

    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2719738
    :goto_1
    const v8, 0x7f0d0705

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 2719739
    sget-object p0, LX/Jdk;->MESSAGES:LX/Jdk;

    invoke-static {v8, p0}, LX/Jdl;->a(Landroid/view/View;LX/Jdk;)Lcom/facebook/widget/BetterSwitch;

    move-result-object p1

    .line 2719740
    const p0, 0x7f0d1a8c

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {p0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object p0

    iput-object p0, v0, LX/Jdl;->f:LX/4ob;

    .line 2719741
    const p0, 0x7f0d1a8d

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {p0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object p0

    iput-object p0, v0, LX/Jdl;->g:LX/4ob;

    .line 2719742
    const p0, 0x7f0d1a8e

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {p0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object p0

    iput-object p0, v0, LX/Jdl;->h:LX/4ob;

    .line 2719743
    sget-object p0, LX/Jdk;->MESSAGES:LX/Jdk;

    invoke-static {v0, v8, p1, p0}, LX/Jdl;->a(LX/Jdl;Landroid/view/View;Lcom/facebook/widget/BetterSwitch;LX/Jdk;)V

    .line 2719744
    iget-object v8, v0, LX/Jdl;->f:LX/4ob;

    sget-object p0, LX/Jdk;->PROMOTION_MESSAGES:LX/Jdk;

    invoke-static {v0, v8, p0}, LX/Jdl;->a(LX/Jdl;LX/4ob;LX/Jdk;)V

    .line 2719745
    iget-object v8, v0, LX/Jdl;->g:LX/4ob;

    sget-object p0, LX/Jdk;->SUBSCRIPTION_MESSAGES:LX/Jdk;

    invoke-static {v0, v8, p0}, LX/Jdl;->a(LX/Jdl;LX/4ob;LX/Jdk;)V

    .line 2719746
    invoke-static {v0, p1}, LX/Jdl;->a$redex0(LX/Jdl;Lcom/facebook/widget/BetterSwitch;)V

    .line 2719747
    const v8, 0x7f0d0707

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 2719748
    invoke-static {v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2719749
    const/4 p1, 0x1

    .line 2719750
    iget-boolean p0, v1, Lcom/facebook/user/model/User;->F:Z

    move p0, p0

    .line 2719751
    if-nez p0, :cond_6

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->P()Z

    move-result p0

    if-nez p0, :cond_6

    iget-object p0, v0, LX/Jdl;->n:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/03R;

    invoke-virtual {p0, p1}, LX/03R;->asBoolean(Z)Z

    move-result p0

    if-nez p0, :cond_6

    move p0, p1

    :goto_2
    move p0, p0

    .line 2719752
    if-nez p0, :cond_4

    .line 2719753
    const/16 p0, 0x8

    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2719754
    :goto_3
    iget-object v8, v0, LX/Jdl;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2719755
    iget-object v8, v0, LX/Jdl;->h:LX/4ob;

    invoke-virtual {v8}, LX/4ob;->e()V

    .line 2719756
    iget-object v8, v0, LX/Jdl;->h:LX/4ob;

    invoke-virtual {v8}, LX/4ob;->a()Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    new-instance p0, LX/Jdc;

    invoke-direct {p0, v0}, LX/Jdc;-><init>(LX/Jdl;)V

    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2719757
    :cond_0
    const/16 v0, 0x2b

    const v1, -0xd1dfb78

    invoke-static {v7, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v4

    :cond_1
    move p0, p1

    .line 2719758
    goto/16 :goto_0

    .line 2719759
    :cond_2
    check-cast v3, Lcom/facebook/messaging/blocking/AdManageBlockingParam;

    .line 2719760
    invoke-virtual {v8, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2719761
    new-instance p0, LX/Jdb;

    invoke-direct {p0, v0, v8, v3}, LX/Jdb;-><init>(LX/Jdl;Landroid/widget/LinearLayout;Lcom/facebook/messaging/blocking/AdManageBlockingParam;)V

    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2719762
    const v8, 0x7f0d1a8b

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iget-boolean p0, v3, Lcom/facebook/messaging/blocking/AdManageBlockingParam;->b:Z

    if-eqz p0, :cond_3

    :goto_4
    invoke-virtual {v8, p1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_3
    const/4 p1, 0x4

    goto :goto_4

    .line 2719763
    :cond_4
    iget-boolean p0, v1, Lcom/facebook/user/model/User;->I:Z

    move p0, p0

    .line 2719764
    if-eqz p0, :cond_5

    .line 2719765
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2719766
    const p0, 0x7f0d0708

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/text/BetterTextView;

    .line 2719767
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f082b27    # 1.8099907E38f

    new-array p3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p3, v3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2719768
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2719769
    const p0, 0x7f0d0709

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/text/BetterTextView;

    .line 2719770
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f082b28    # 1.8099909E38f

    new-array p3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p3, v3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2719771
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2719772
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    .line 2719773
    new-instance p1, LX/Jdi;

    invoke-direct {p1, v0, p0}, LX/Jdi;-><init>(LX/Jdl;Landroid/content/Context;)V

    move-object p0, p1

    .line 2719774
    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 2719775
    :cond_5
    const p0, 0x7f0d0708

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/text/BetterTextView;

    .line 2719776
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f082b25    # 1.8099903E38f

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2719777
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2719778
    const p0, 0x7f0d0709

    invoke-virtual {v4, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/text/BetterTextView;

    .line 2719779
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f082b26    # 1.8099905E38f

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v1}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p3, v2

    invoke-virtual {p1, p2, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2719780
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2719781
    new-instance p0, LX/Jdh;

    invoke-direct {p0, v0}, LX/Jdh;-><init>(LX/Jdl;)V

    move-object p0, p0

    .line 2719782
    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    :cond_6
    const/4 p0, 0x0

    goto/16 :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2719783
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2719784
    const-string v0, "arg_blockee"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->o:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2719785
    const-string v0, "arg_thread_key"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2719786
    iget-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->q:Lcom/facebook/messaging/blocking/ManageBlockingParam;

    if-eqz v0, :cond_0

    .line 2719787
    const-string v0, "arg_param"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->q:Lcom/facebook/messaging/blocking/ManageBlockingParam;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2719788
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x7b45ca94

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719789
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 2719790
    iget-boolean v1, p0, Landroid/support/v4/app/DialogFragment;->d:Z

    move v1, v1

    .line 2719791
    if-nez v1, :cond_0

    .line 2719792
    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->r:LX/Jrq;

    if-eqz v1, :cond_0

    .line 2719793
    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    .line 2719794
    :cond_0
    const v1, 0x364daa67

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void
.end method
