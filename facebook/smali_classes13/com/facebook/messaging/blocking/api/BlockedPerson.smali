.class public Lcom/facebook/messaging/blocking/api/BlockedPerson;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/blocking/api/BlockedPerson;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBlockedDate:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "block_time"
    .end annotation
.end field

.field private final mBlockedType:LX/JeK;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "block_type"
    .end annotation
.end field

.field private final mFbid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fbid"
    .end annotation
.end field

.field private final mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2720572
    const-class v0, Lcom/facebook/messaging/blocking/api/BlockedPersonDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2720565
    new-instance v0, LX/JeJ;

    invoke-direct {v0}, LX/JeJ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2720558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720559
    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mFbid:Ljava/lang/String;

    .line 2720560
    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mName:Ljava/lang/String;

    .line 2720561
    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedType:LX/JeK;

    .line 2720562
    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedDate:Ljava/lang/String;

    .line 2720563
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2720566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720567
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mFbid:Ljava/lang/String;

    .line 2720568
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mName:Ljava/lang/String;

    .line 2720569
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/JeK;->valueOf(Ljava/lang/String;)LX/JeK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedType:LX/JeK;

    .line 2720570
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedDate:Ljava/lang/String;

    .line 2720571
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2720564
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2720557
    const-string v0, "fbid %s, name %s, block_type %s, block_date %s"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mFbid:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedType:LX/JeK;

    iget-object v4, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedDate:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2720552
    iget-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mFbid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2720553
    iget-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2720554
    iget-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedType:LX/JeK;

    invoke-virtual {v0}, LX/JeK;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2720555
    iget-object v0, p0, Lcom/facebook/messaging/blocking/api/BlockedPerson;->mBlockedDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2720556
    return-void
.end method
