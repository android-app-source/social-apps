.class public Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/JeL;


# direct methods
.method public constructor <init>(LX/0Or;LX/JeL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/JeL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720631
    iput-object p1, p0, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->a:LX/0Or;

    .line 2720632
    iput-object p2, p0, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->b:LX/JeL;

    .line 2720633
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;
    .locals 6

    .prologue
    .line 2720634
    const-class v1, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;

    monitor-enter v1

    .line 2720635
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2720636
    sput-object v2, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2720637
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2720638
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2720639
    new-instance v4, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 2720640
    new-instance p0, LX/JeL;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/JeL;-><init>(LX/0lC;)V

    .line 2720641
    move-object v3, p0

    .line 2720642
    check-cast v3, LX/JeL;

    invoke-direct {v4, v5, v3}, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;-><init>(LX/0Or;LX/JeL;)V

    .line 2720643
    move-object v0, v4

    .line 2720644
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2720645
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2720646
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2720647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2720648
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2720649
    const-string v1, "GetBlockedPeople"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 2720650
    iget-object v0, p0, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->b:LX/JeL;

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2720651
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
