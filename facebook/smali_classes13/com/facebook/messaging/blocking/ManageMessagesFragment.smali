.class public Lcom/facebook/messaging/blocking/ManageMessagesFragment;
.super Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;
.source ""

# interfaces
.implements LX/Jda;


# instance fields
.field public m:LX/JeI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/user/model/User;

.field private o:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:LX/JeH;

.field private r:LX/Jrq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2720483
    invoke-direct {p0}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/messaging/blocking/ManageMessagesFragment;

    const-class p0, LX/JeI;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/JeI;

    iput-object v1, p1, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->m:LX/JeI;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1929d69b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2720467
    invoke-super {p0, p1}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2720468
    const-class v0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2720469
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2720470
    iget-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->n:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_0

    .line 2720471
    const/16 v0, 0x2b

    const v2, -0x5cb01c1c

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2720472
    :goto_0
    return-void

    .line 2720473
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2720474
    if-eqz p1, :cond_2

    .line 2720475
    const-string v0, "arg_blockee"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->n:Lcom/facebook/user/model/User;

    .line 2720476
    const-string v0, "arg_topics_only"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->p:Z

    .line 2720477
    const-string v0, "arg_thread_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->o:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2720478
    :cond_1
    :goto_1
    const v0, -0x6fd98933

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0

    .line 2720479
    :cond_2
    if-eqz v2, :cond_1

    .line 2720480
    const-string v0, "arg_blockee"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->n:Lcom/facebook/user/model/User;

    .line 2720481
    const-string v0, "arg_topics_only"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->p:Z

    .line 2720482
    const-string v0, "arg_thread_key"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->o:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x499ec4fa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2720466
    const v1, 0x7f030a6f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6748306b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x51ae54f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2720461
    invoke-super {p0}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onDestroyView()V

    .line 2720462
    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->q:LX/JeH;

    .line 2720463
    iget-object v2, v1, LX/JeH;->a:LX/Jfw;

    .line 2720464
    iget-object v1, v2, LX/Jfw;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2720465
    const/16 v1, 0x2b

    const v2, 0x328b722e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2720456
    invoke-super {p0, p1}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2720457
    const-string v0, "arg_blockee"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->n:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2720458
    const-string v0, "arg_topics_only"

    iget-boolean v1, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2720459
    const-string v0, "arg_thread_key"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->o:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2720460
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x1585ada2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2720451
    invoke-super {p0}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onStart()V

    .line 2720452
    iget-boolean v1, p0, Landroid/support/v4/app/DialogFragment;->d:Z

    move v1, v1

    .line 2720453
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->r:LX/Jrq;

    if-eqz v1, :cond_0

    .line 2720454
    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->n:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->P()Z

    .line 2720455
    :cond_0
    const v1, -0x3feb6c8a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2720444
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2720445
    const v0, 0x7f0d1a92

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 2720446
    const v0, 0x7f0d1a91

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 2720447
    iget-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->m:LX/JeI;

    iget-object v3, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->n:Lcom/facebook/user/model/User;

    iget-object v4, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->o:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2720448
    iget-boolean v5, p0, Landroid/support/v4/app/DialogFragment;->d:Z

    move v5, v5

    .line 2720449
    new-instance v6, LX/JeD;

    invoke-direct {v6, p0}, LX/JeD;-><init>(Lcom/facebook/messaging/blocking/ManageMessagesFragment;)V

    iget-boolean v7, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->p:Z

    invoke-virtual/range {v0 .. v7}, LX/JeI;->a(Landroid/support/v7/widget/RecyclerView;Landroid/widget/ProgressBar;Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLX/JeD;Z)LX/JeH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageMessagesFragment;->q:LX/JeH;

    .line 2720450
    return-void
.end method
