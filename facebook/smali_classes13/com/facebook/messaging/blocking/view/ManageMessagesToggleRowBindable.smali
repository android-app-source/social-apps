.class public Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jek;


# instance fields
.field private final a:LX/Jdw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jdw",
            "<",
            "Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/JgE;


# direct methods
.method public constructor <init>(LX/JgE;LX/Jdw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JgE;",
            "LX/Jdw",
            "<",
            "Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2720980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720981
    iput-object p1, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    .line 2720982
    iput-object p2, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a:LX/Jdw;

    .line 2720983
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2720977
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    .line 2720978
    iget-object p0, v0, LX/JgE;->a:Landroid/view/ViewGroup;

    move-object v0, p0

    .line 2720979
    return-object v0
.end method

.method public final a(LX/JeZ;)V
    .locals 1

    .prologue
    .line 2720955
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a:LX/Jdw;

    invoke-interface {v0, p1, p0}, LX/Jdw;->a(LX/JeZ;LX/Jek;)V

    .line 2720956
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2720970
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    .line 2720971
    if-nez p1, :cond_0

    .line 2720972
    iget-object v1, v0, LX/JgE;->e:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->d()V

    .line 2720973
    :goto_0
    return-void

    .line 2720974
    :cond_0
    iget-object v1, v0, LX/JgE;->e:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v2, LX/JgE;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2720975
    iget-object v1, v0, LX/JgE;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/JgE;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0e0a6c

    invoke-virtual {v1, v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2720976
    iget-object v1, v0, LX/JgE;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/JgE;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0e0a6e

    invoke-virtual {v1, v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2720968
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    invoke-virtual {v0, p1}, LX/JgE;->a(Ljava/lang/String;)V

    .line 2720969
    return-void
.end method

.method public final a(ZLjava/lang/String;LX/JeV;LX/Jdt;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/JeV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/Jdt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2720963
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/JgE;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2720964
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    invoke-virtual {v0, p1}, LX/JgE;->a(Z)V

    .line 2720965
    new-instance v0, LX/Jen;

    invoke-direct {v0, p0, p3, p2, p4}, LX/Jen;-><init>(Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;LX/JeV;Ljava/lang/String;LX/Jdt;)V

    .line 2720966
    iget-object v1, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    invoke-virtual {v1, v0}, LX/JgE;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2720967
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2720957
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b:LX/JgE;

    .line 2720958
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2720959
    iget-object v1, v0, LX/JgE;->c:Lcom/facebook/widget/text/BetterTextView;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2720960
    :goto_0
    return-void

    .line 2720961
    :cond_0
    iget-object v1, v0, LX/JgE;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2720962
    iget-object v1, v0, LX/JgE;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
