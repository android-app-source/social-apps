.class public Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jek;


# instance fields
.field private final a:LX/Jdw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jdw",
            "<",
            "Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/JgA;


# direct methods
.method public constructor <init>(LX/JgA;LX/Jdw;)V
    .locals 0

    .prologue
    .line 2720923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720924
    iput-object p1, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720925
    iput-object p2, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a:LX/Jdw;

    .line 2720926
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2720927
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720928
    iget-object p0, v0, LX/JgA;->a:Landroid/view/ViewGroup;

    move-object v0, p0

    .line 2720929
    return-object v0
.end method

.method public final a(LX/JeZ;)V
    .locals 1

    .prologue
    .line 2720930
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a:LX/Jdw;

    invoke-interface {v0, p1, p0}, LX/Jdw;->a(LX/JeZ;LX/Jek;)V

    .line 2720931
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2720916
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720917
    if-nez p1, :cond_0

    .line 2720918
    iget-object v1, v0, LX/JgA;->d:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->d()V

    .line 2720919
    :goto_0
    return-void

    .line 2720920
    :cond_0
    iget-object v1, v0, LX/JgA;->d:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v2, LX/JgE;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2720921
    iget-object v1, v0, LX/JgA;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/JgA;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0e0a6c

    invoke-virtual {v1, v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2720922
    iget-object v1, v0, LX/JgA;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/JgA;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0e0a6e

    invoke-virtual {v1, v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2720904
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720905
    iget-object p0, v0, LX/JgA;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2720906
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2720913
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720914
    iget-object p0, v0, LX/JgA;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2720915
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2720907
    iget-object v0, p0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720908
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2720909
    iget-object v1, v0, LX/JgA;->c:Lcom/facebook/widget/text/BetterTextView;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2720910
    :goto_0
    return-void

    .line 2720911
    :cond_0
    iget-object v1, v0, LX/JgA;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2720912
    iget-object v1, v0, LX/JgA;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
