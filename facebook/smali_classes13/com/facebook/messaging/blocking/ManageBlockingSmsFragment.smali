.class public Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field public o:J

.field public p:LX/FMK;

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/blocking/BlockingUtils;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2720062
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2720063
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2720064
    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->q:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;

    const/16 p0, 0x266f

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->q:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2720084
    const v0, 0x7f08090c

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->m:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2720085
    const v1, 0x7f08090d

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->n:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->n:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2720086
    new-instance v2, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08090e

    new-instance v2, LX/Jdm;

    invoke-direct {v2, p0}, LX/Jdm;-><init>(Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08090f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7cf3f0e8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2720070
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2720071
    const-class v0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2720072
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2720073
    if-eqz p1, :cond_1

    .line 2720074
    const-string v0, "arg_address"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->m:Ljava/lang/String;

    .line 2720075
    const-string v0, "arg_contact_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->n:Ljava/lang/String;

    .line 2720076
    const-string v0, "arg_threadId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->o:J

    .line 2720077
    const-string v0, "arg_caller_context"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FMK;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->p:LX/FMK;

    .line 2720078
    :cond_0
    :goto_0
    const v0, -0x5eb5c9aa

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2720079
    :cond_1
    if-eqz v0, :cond_0

    .line 2720080
    const-string v2, "arg_address"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->m:Ljava/lang/String;

    .line 2720081
    const-string v2, "arg_contact_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->n:Ljava/lang/String;

    .line 2720082
    const-string v2, "arg_threadId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->o:J

    .line 2720083
    const-string v2, "arg_caller_context"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FMK;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->p:LX/FMK;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2720065
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2720066
    const-string v0, "arg_address"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2720067
    const-string v0, "arg_contact_name"

    iget-object v1, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2720068
    const-string v0, "arg_threadId"

    iget-wide v2, p0, Lcom/facebook/messaging/blocking/ManageBlockingSmsFragment;->o:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2720069
    return-void
.end method
