.class public Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/messaging/blocking/BlockingUtils;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/user/model/User;

.field public final p:LX/2h0;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2719607
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2719608
    new-instance v0, LX/JdR;

    invoke-direct {v0, p0}, LX/JdR;-><init>(Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->p:LX/2h0;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    invoke-static {p0}, Lcom/facebook/messaging/blocking/BlockingUtils;->b(LX/0QB;)Lcom/facebook/messaging/blocking/BlockingUtils;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/blocking/BlockingUtils;

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object p0

    check-cast p0, LX/1CX;

    iput-object v1, p1, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->m:Lcom/facebook/messaging/blocking/BlockingUtils;

    iput-object p0, p1, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->n:LX/1CX;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 2719609
    const-class v0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2719610
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2719611
    if-eqz v0, :cond_0

    .line 2719612
    const-string v1, "blockee"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    .line 2719613
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2719614
    new-instance v1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2719615
    iget-object v0, p0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2719616
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082b37    # 1.809994E38f

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    invoke-static {v4}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2719617
    :goto_0
    const v2, 0x7f082b2f    # 1.8099923E38f

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v2, 0x7f082b34    # 1.8099933E38f

    new-instance v3, LX/JdT;

    invoke-direct {v3, p0}, LX/JdT;-><init>(Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f082b33    # 1.8099931E38f

    new-instance v3, LX/JdS;

    invoke-direct {v3, p0}, LX/JdS;-><init>(Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0ju;->a(Z)LX/0ju;

    .line 2719618
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0

    .line 2719619
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082b30    # 1.8099925E38f

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/messaging/blocking/AskToUnblockDialogFragment;->o:Lcom/facebook/user/model/User;

    invoke-static {v4}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
