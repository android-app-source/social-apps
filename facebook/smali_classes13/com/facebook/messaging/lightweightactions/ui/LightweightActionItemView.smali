.class public Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2733935
    const-class v0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;

    const-string v1, "lightweight_actions_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2733936
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2733937
    const-class v0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733938
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->setGravity(I)V

    .line 2733939
    const v0, 0x7f030d14

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2733940
    const v0, 0x7f02125e

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->setBackgroundResource(I)V

    .line 2733941
    const v0, 0x7f0d20a2

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2733942
    const v0, 0x7f0d20a1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2733943
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object p0, p1, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemView;->a:LX/1Ad;

    return-void
.end method
