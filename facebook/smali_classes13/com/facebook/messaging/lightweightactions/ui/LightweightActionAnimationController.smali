.class public Lcom/facebook/messaging/lightweightactions/ui/LightweightActionAnimationController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2733877
    const-class v0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionAnimationController;

    const-string v1, "lightweight_actions_in_thread"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionAnimationController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2733878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2733879
    iput-object p1, p0, Lcom/facebook/messaging/lightweightactions/ui/LightweightActionAnimationController;->b:LX/1Ad;

    .line 2733880
    return-void
.end method
