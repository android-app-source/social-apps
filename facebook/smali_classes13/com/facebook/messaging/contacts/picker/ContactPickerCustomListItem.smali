.class public Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/fbui/widget/text/BadgeTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2724483
    const/4 v0, 0x0

    const v1, 0x7f0104c4

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2724484
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->a()V

    .line 2724485
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2724486
    const v0, 0x7f0104c4

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2724487
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->a()V

    .line 2724488
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2724489
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2724490
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->a()V

    .line 2724491
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2724492
    const v0, 0x7f030cd7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2724493
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setOrientation(I)V

    .line 2724494
    const v0, 0x7f0d1ffa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2724495
    const v0, 0x7f0d1ffb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2724496
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2724497
    return-void
.end method


# virtual methods
.method public setBadgeText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2724498
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2724499
    return-void
.end method

.method public setIcon(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2724500
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2724501
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2724502
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2724503
    return-void
.end method

.method public setText(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2724504
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2724505
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(I)V

    .line 2724506
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2724507
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2724508
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2724509
    :goto_0
    return-void

    .line 2724510
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2724511
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->b:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
