.class public final Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2724920
    new-instance v0, LX/Jhn;

    invoke-direct {v0}, LX/Jhn;-><init>()V

    invoke-static {v0}, LX/24y;->a(LX/24x;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2724921
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 2724922
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->a:LX/03R;

    .line 2724923
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2724924
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2724925
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2724926
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2724927
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListItem$SavedState;->a:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2724928
    return-void
.end method
