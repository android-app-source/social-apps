.class public Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;
.super LX/3Ml;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field public final d:LX/11H;

.field private final e:LX/3Kv;

.field public final f:LX/3fa;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/11H;LX/3fa;LX/3Kv;LX/0Zr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2725633
    invoke-direct {p0, p5}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2725634
    iput-object p1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->c:Landroid/content/res/Resources;

    .line 2725635
    iput-object p2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->d:LX/11H;

    .line 2725636
    iput-object p4, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->e:LX/3Kv;

    .line 2725637
    iput-object p3, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->f:LX/3fa;

    .line 2725638
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 8

    .prologue
    .line 2725639
    new-instance v0, LX/39y;

    invoke-direct {v0}, LX/39y;-><init>()V

    .line 2725640
    const-string v1, "ContactPickerFriendFilter.Filtering"

    const v2, 0x292d927

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2725641
    :try_start_0
    invoke-static {p1}, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2725642
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v1

    iput-object v1, v0, LX/39y;->a:Ljava/lang/Object;

    .line 2725643
    const/4 v1, -0x1

    iput v1, v0, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2725644
    const v1, 0x10ac472b

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2725645
    const-string v1, "orca:ContactPickerPaymentEligibleContactsFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    .line 2725646
    :goto_0
    return-object v0

    .line 2725647
    :cond_0
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2725648
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2725649
    :try_start_2
    new-instance v3, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;

    const/16 v4, 0x64

    invoke-direct {v3, v2, v4}, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;-><init>(Ljava/lang/String;I)V

    .line 2725650
    iget-object v4, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->d:LX/11H;

    iget-object v5, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->f:LX/3fa;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v4, v5, v3, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/server/FetchContactsResult;

    .line 2725651
    iget-object v4, v3, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v5, v4

    .line 2725652
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/graphql/Contact;

    .line 2725653
    invoke-static {v3}, LX/6Ok;->a(Lcom/facebook/contacts/graphql/Contact;)Lcom/facebook/user/model/User;

    move-result-object v3

    .line 2725654
    iget-object v7, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v7, v3}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v3

    .line 2725655
    if-eqz v3, :cond_1

    .line 2725656
    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2725657
    :cond_1
    :try_start_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2725658
    :catch_0
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2725659
    iget-object v2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerPaymentEligibleContactsFilter;->c:Landroid/content/res/Resources;

    const v3, 0x7f0802fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2725660
    new-instance v3, LX/DAX;

    invoke-direct {v3, v2}, LX/DAX;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    .line 2725661
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2725662
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v1

    .line 2725663
    iput-object v1, v0, LX/39y;->a:Ljava/lang/Object;

    .line 2725664
    iget v2, v1, LX/3Og;->d:I

    move v1, v2

    .line 2725665
    iput v1, v0, LX/39y;->b:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2725666
    const v1, 0x18f5058c

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2725667
    const-string v1, "orca:ContactPickerPaymentEligibleContactsFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 2725668
    :catch_1
    move-exception v1

    .line 2725669
    :try_start_4
    const-string v3, "Exception during filtering of payment eligible contacts for query: %s"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    aput-object v2, v4, v5

    invoke-static {v3, v4}, LX/0PR;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2725670
    const-string v2, "orca:ContactPickerPaymentEligibleContactsFilter"

    const-string v3, "Exception during filtering"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2725671
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v1

    iput-object v1, v0, LX/39y;->a:Ljava/lang/Object;

    .line 2725672
    const/4 v1, 0x0

    iput v1, v0, LX/39y;->b:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2725673
    const v1, -0x4726ec9f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2725674
    const-string v1, "orca:ContactPickerPaymentEligibleContactsFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2725675
    :cond_4
    :try_start_5
    const-string v2, "null"
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 2725676
    :catchall_0
    move-exception v0

    const v1, 0xe2aab03

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2725677
    const-string v1, "orca:ContactPickerPaymentEligibleContactsFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    throw v0
.end method
