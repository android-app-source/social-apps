.class public Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Landroid/widget/Button;

.field public final d:Landroid/widget/ImageView;

.field private final e:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725985
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725986
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2725983
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725984
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2725973
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725974
    const v0, 0x7f030cf2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2725975
    const v0, 0x7f0d204e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2725976
    const v0, 0x7f0d204f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2725977
    const v0, 0x7f0d2051

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->c:Landroid/widget/Button;

    .line 2725978
    const v0, 0x7f0d204d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->d:Landroid/widget/ImageView;

    .line 2725979
    const v0, 0x7f0d2050

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->e:LX/4ob;

    .line 2725980
    new-instance v0, LX/JiT;

    invoke-direct {v0, p0}, LX/JiT;-><init>(Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;)V

    .line 2725981
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->h:LX/JiT;

    .line 2725982
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2725967
    if-eqz p1, :cond_0

    .line 2725968
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2725969
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->e:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2725970
    :goto_0
    return-void

    .line 2725971
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2725972
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->e:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto :goto_0
.end method

.method public getTextView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 2725966
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->b:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public setNegativeButtonContentDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2725964
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2725965
    return-void
.end method

.method public setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2725962
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725963
    return-void
.end method

.method public setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2725952
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725953
    return-void
.end method

.method public setPositiveButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2725960
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2725961
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2725958
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725959
    return-void
.end method

.method public setTextContentDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2725956
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2725957
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2725954
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725955
    return-void
.end method
