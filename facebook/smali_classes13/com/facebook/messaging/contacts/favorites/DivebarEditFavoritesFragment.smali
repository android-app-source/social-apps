.class public Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;
.super Lcom/facebook/ui/drawers/DrawerContentFragment;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/3Md;

.field private c:LX/JhJ;

.field public d:LX/0Zb;

.field public e:LX/1CX;

.field public f:LX/4At;

.field public g:LX/JhA;

.field public h:Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

.field public i:Landroid/content/Context;

.field private j:Z

.field public k:Z

.field public l:Z

.field public m:Landroid/view/MenuItem;

.field public n:LX/2EJ;

.field public o:LX/01T;

.field public p:LX/Jh6;

.field private q:LX/JhK;

.field public r:LX/Jgu;

.field public s:LX/Jgt;

.field private t:LX/61y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2724174
    const-class v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    sput-object v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2724172
    invoke-direct {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;-><init>()V

    .line 2724173
    new-instance v0, LX/Jh1;

    invoke-direct {v0, p0}, LX/Jh1;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->t:LX/61y;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;Lcom/facebook/user/model/User;)V
    .locals 2

    .prologue
    .line 2724167
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2724168
    iget-object v1, v0, LX/Jgt;->h:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2724169
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->u(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724170
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->v(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724171
    return-void
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 2724162
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2724163
    instance-of v1, v0, LX/67U;

    if-eqz v1, :cond_1

    .line 2724164
    check-cast v0, LX/67U;

    invoke-interface {v0}, LX/67U;->b()LX/3u1;

    move-result-object v0

    .line 2724165
    :goto_0
    move-object v0, v0

    .line 2724166
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V
    .locals 3

    .prologue
    .line 2723959
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    invoke-interface {v0}, LX/JhA;->a()V

    .line 2723960
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->m:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2723961
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->m:Landroid/view/MenuItem;

    invoke-static {v0}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    .line 2723962
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2723963
    const-string v1, ""

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 2723964
    :cond_0
    return-void
.end method

.method private t()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2724155
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    invoke-virtual {v0}, LX/Jgt;->b()LX/0Px;

    move-result-object v2

    .line 2724156
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2724157
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2724158
    iget-object p0, v0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    move-object v0, p0

    .line 2724159
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2724160
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2724161
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static u(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V
    .locals 1

    .prologue
    .line 2724153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    .line 2724154
    return-void
.end method

.method public static v(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2724102
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    invoke-virtual {v0}, LX/Jgt;->b()LX/0Px;

    move-result-object v3

    .line 2724103
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2724104
    iget-object v2, v0, LX/Jgt;->i:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Jgt;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2724105
    :cond_0
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2724106
    :goto_0
    move-object v4, v2

    .line 2724107
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2724108
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2724109
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    :goto_1
    if-ge v2, v7, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2724110
    iget-object v8, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v8

    .line 2724111
    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2724112
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2724113
    :cond_1
    if-eqz v4, :cond_3

    .line 2724114
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2724115
    iget-object v7, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v7

    .line 2724116
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2724117
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2724118
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2724119
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2724120
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2724121
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2724122
    if-eqz v3, :cond_4

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2724123
    :cond_4
    sget-object v2, LX/Ji5;->e:LX/3OQ;

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2724124
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2724125
    :goto_3
    move-object v2, v2

    .line 2724126
    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2724127
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2724128
    if-eqz v0, :cond_7

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2724129
    sget-object v2, LX/Ji5;->d:LX/3OQ;

    move-object v2, v2

    .line 2724130
    if-eqz v2, :cond_5

    .line 2724131
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2724132
    :cond_5
    goto :goto_4

    .line 2724133
    :goto_4
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v5, :cond_7

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2724134
    iget-object v6, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->a:LX/3Md;

    invoke-interface {v6, v2}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v2

    .line 2724135
    if-eqz v2, :cond_6

    .line 2724136
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2724137
    :cond_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 2724138
    :cond_7
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2724139
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2724140
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;

    move-result-object v0

    invoke-virtual {v0}, LX/3LG;->d()LX/3Mi;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->t()LX/0Px;

    move-result-object v2

    invoke-interface {v0, v2}, LX/3Mi;->a(LX/0Px;)V

    .line 2724141
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2724142
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    invoke-interface {v1, v0}, LX/JhA;->a(LX/0Px;)V

    .line 2724143
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;

    move-result-object v0

    const v1, 0x5d831968

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2724144
    return-void

    :cond_8
    iget-object v2, v0, LX/Jgt;->i:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto/16 :goto_0

    .line 2724145
    :cond_9
    goto :goto_6

    .line 2724146
    :goto_6
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_7
    if-ge v4, v6, :cond_a

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2724147
    new-instance v7, LX/JhD;

    invoke-direct {v7, v2}, LX/JhD;-><init>(Lcom/facebook/user/model/User;)V

    .line 2724148
    new-instance v2, LX/Jh4;

    invoke-direct {v2, p0}, LX/Jh4;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724149
    iput-object v2, v7, LX/JhD;->b:LX/Jh4;

    .line 2724150
    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2724151
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_7

    .line 2724152
    :cond_a
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public static x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;
    .locals 6

    .prologue
    .line 2724095
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->c:LX/JhJ;

    if-nez v0, :cond_0

    .line 2724096
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->q:LX/JhK;

    .line 2724097
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->p:LX/Jh6;

    move-object v1, v1

    .line 2724098
    new-instance v4, LX/JhJ;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0x271a

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    invoke-direct {v4, v1, v2, v5, v3}, LX/JhJ;-><init>(LX/Jh6;Landroid/content/Context;LX/0Or;Landroid/view/LayoutInflater;)V

    .line 2724099
    move-object v0, v4

    .line 2724100
    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->c:LX/JhJ;

    .line 2724101
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->c:LX/JhJ;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2724081
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->a(Landroid/os/Bundle;)V

    .line 2724082
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0104b1

    const v2, 0x7f0e0552

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->i:Landroid/content/Context;

    .line 2724083
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->i:Landroid/content/Context;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2724084
    new-instance v2, LX/Jh6;

    const-class v0, Landroid/content/Context;

    invoke-interface {v1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v2, v0}, LX/Jh6;-><init>(Landroid/content/Context;)V

    .line 2724085
    move-object v0, v2

    .line 2724086
    check-cast v0, LX/Jh6;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->p:LX/Jh6;

    .line 2724087
    const-class v0, LX/JhK;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/JhK;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->q:LX/JhK;

    .line 2724088
    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->d:LX/0Zb;

    .line 2724089
    invoke-static {v1}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->o:LX/01T;

    .line 2724090
    invoke-static {v1}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v0

    check-cast v0, LX/1CX;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->e:LX/1CX;

    .line 2724091
    const-class v0, LX/Jgu;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Jgu;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->r:LX/Jgu;

    .line 2724092
    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->l:Z

    .line 2724093
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->p()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2724094
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 2724065
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724066
    iget-boolean v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    if-eqz v0, :cond_1

    .line 2724067
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->d:LX/0Zb;

    const-string v1, "update_favorites"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2724068
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2724069
    const-string v1, "favorite_count"

    iget-object v2, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    invoke-virtual {v2}, LX/Jgt;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2724070
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2724071
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2724072
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2724073
    const-string v2, "favorites"

    new-instance v3, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;

    iget-object v4, v0, LX/Jgt;->h:Ljava/util/List;

    invoke-direct {v3, v4}, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2724074
    iget-object v2, v0, LX/Jgt;->e:LX/0aG;

    const-string v3, "update_favorite_contacts"

    const v4, -0x10024aee

    invoke-static {v2, v3, v1, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2724075
    new-instance v2, LX/Jgs;

    invoke-direct {v2, v0}, LX/Jgs;-><init>(LX/Jgt;)V

    .line 2724076
    iget-object v3, v0, LX/Jgt;->f:LX/0Sh;

    invoke-virtual {v3, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2724077
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802e5

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->f:LX/4At;

    .line 2724078
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->f:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2724079
    :goto_0
    return-void

    .line 2724080
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->l()V

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2724055
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2724056
    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v1

    if-lez v1, :cond_2

    .line 2724057
    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2724058
    invoke-virtual {v0}, LX/0gc;->d()V

    .line 2724059
    :cond_0
    :goto_0
    return-void

    .line 2724060
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->j:Z

    if-nez v0, :cond_0

    .line 2724061
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->j:Z

    goto :goto_0

    .line 2724062
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2724063
    if-eqz v0, :cond_0

    .line 2724064
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x9e81314

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2724019
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2724020
    if-eqz p1, :cond_0

    .line 2724021
    const-string v1, "dirty"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    .line 2724022
    :cond_0
    iget-object v4, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    if-nez v4, :cond_1

    .line 2724023
    iget-object v4, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->r:LX/Jgu;

    .line 2724024
    new-instance v5, LX/Jgt;

    invoke-static {v4}, LX/3Ku;->a(LX/0QB;)LX/3Ku;

    move-result-object v7

    check-cast v7, LX/3Ku;

    invoke-static {v4}, LX/3Kk;->a(LX/0QB;)LX/3Kk;

    move-result-object v8

    check-cast v8, LX/3Kk;

    invoke-static {v4}, LX/3Kl;->a(LX/0QB;)LX/3Kl;

    move-result-object v9

    check-cast v9, LX/3Kl;

    invoke-static {v4}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v10

    check-cast v10, LX/0aG;

    invoke-static {v4}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    move-object v6, p0

    invoke-direct/range {v5 .. v11}, LX/Jgt;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;LX/3Ku;LX/3Kk;LX/3Kl;LX/0aG;LX/0Sh;)V

    .line 2724025
    move-object v4, v5

    .line 2724026
    iput-object v4, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2724027
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    move-object v1, v4

    .line 2724028
    iput-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2724029
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2724030
    iget-object v2, v1, LX/Jgt;->g:LX/3Lb;

    if-nez v2, :cond_2

    .line 2724031
    invoke-static {v1}, LX/Jgt;->e(LX/Jgt;)LX/3Lb;

    move-result-object v2

    iput-object v2, v1, LX/Jgt;->g:LX/3Lb;

    .line 2724032
    :cond_2
    iget-object v2, v1, LX/Jgt;->g:LX/3Lb;

    new-instance v4, LX/Jgr;

    invoke-direct {v4, v1}, LX/Jgr;-><init>(LX/Jgt;)V

    .line 2724033
    iput-object v4, v2, LX/3Lb;->B:LX/3Mb;

    .line 2724034
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/Jgt;->j:Z

    .line 2724035
    iget-object v2, v1, LX/Jgt;->b:LX/3Ku;

    invoke-virtual {v2}, LX/3Ku;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2724036
    const/4 v5, 0x0

    .line 2724037
    if-eqz p1, :cond_6

    .line 2724038
    const-string v4, "dirty"

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 2724039
    if-eqz v4, :cond_6

    const-string v4, "favorites"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2724040
    iput-boolean v5, v1, LX/Jgt;->j:Z

    .line 2724041
    const-string v4, "favorites"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;

    .line 2724042
    iget-object v5, v4, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;->a:LX/0Px;

    move-object v4, v5

    .line 2724043
    :goto_0
    move-object v4, v4

    .line 2724044
    if-eqz v4, :cond_4

    .line 2724045
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v1, LX/Jgt;->h:Ljava/util/List;

    .line 2724046
    :goto_1
    iget-object v2, v1, LX/Jgt;->c:LX/3Kk;

    invoke-virtual {v2}, LX/3Kk;->c()LX/0Px;

    move-result-object v2

    .line 2724047
    if-eqz v2, :cond_3

    .line 2724048
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, v1, LX/Jgt;->i:Ljava/util/List;

    .line 2724049
    :cond_3
    iget-object v2, v1, LX/Jgt;->g:LX/3Lb;

    invoke-virtual {v2}, LX/3Lb;->a()V

    .line 2724050
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->v(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724051
    const/16 v1, 0x2b

    const v2, -0x16f815e0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2724052
    :cond_4
    if-eqz v2, :cond_5

    .line 2724053
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, v1, LX/Jgt;->h:Ljava/util/List;

    goto :goto_1

    .line 2724054
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, LX/Jgt;->h:Ljava/util/List;

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4

    .prologue
    .line 2724001
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->m:Landroid/view/MenuItem;

    .line 2724002
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2724003
    const v0, 0x7f11001f

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2724004
    const v0, 0x7f0d323a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2724005
    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->m:Landroid/view/MenuItem;

    .line 2724006
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2724007
    const v2, 0x7f0e0574

    const v3, 0x7f021001

    const v0, 0x7f0a0190

    invoke-static {v1, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, LX/Hp3;->a(Landroid/content/Context;III)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2724008
    iget-object v2, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->m:Landroid/view/MenuItem;

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2724009
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->m:Landroid/view/MenuItem;

    invoke-static {v1}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/SearchView;

    .line 2724010
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-boolean v2, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->l:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0802dd

    :goto_0
    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2724011
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 2724012
    new-instance v2, LX/Jgz;

    invoke-direct {v2, p0}, LX/Jgz;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724013
    iput-object v2, v1, Landroid/support/v7/widget/SearchView;->mOnCloseListener:LX/3xJ;

    .line 2724014
    new-instance v2, LX/Jh0;

    invoke-direct {v2, p0}, LX/Jh0;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724015
    iput-object v2, v1, Landroid/support/v7/widget/SearchView;->mOnQueryChangeListener:LX/3xK;

    .line 2724016
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2724017
    return-void

    .line 2724018
    :cond_1
    const v2, 0x7f0802dc

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2449c14c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2723982
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;

    move-result-object v0

    .line 2723983
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    if-nez v1, :cond_0

    .line 2723984
    new-instance v1, LX/JhO;

    iget-object v3, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->i:Landroid/content/Context;

    invoke-direct {v1, v3, v0}, LX/JhO;-><init>(Landroid/content/Context;LX/3LG;)V

    iput-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    .line 2723985
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    move-object v0, v1

    .line 2723986
    iput-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    .line 2723987
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    new-instance v1, LX/Jgv;

    invoke-direct {v1, p0}, LX/Jgv;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    invoke-interface {v0, v1}, LX/JhA;->setOnButtonClickedListener(LX/Jgv;)V

    .line 2723988
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    const v1, 0x7f0802db

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/JhA;->setSearchHint(Ljava/lang/String;)V

    .line 2723989
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->i:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030d05

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2723990
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    invoke-interface {v1}, LX/JhA;->getThisView()Landroid/view/View;

    move-result-object v3

    .line 2723991
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2723992
    if-eqz v1, :cond_1

    .line 2723993
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2723994
    :cond_1
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2723995
    new-instance v1, LX/Jgy;

    invoke-direct {v1, p0}, LX/Jgy;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    iput-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->a:LX/3Md;

    .line 2723996
    invoke-static {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->x(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)LX/JhJ;

    move-result-object v1

    invoke-virtual {v1}, LX/3LG;->d()LX/3Mi;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->a:LX/3Md;

    invoke-interface {v1, v3}, LX/3Mi;->a(LX/3Md;)V

    .line 2723997
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->g:LX/JhA;

    invoke-interface {v1}, LX/JhA;->getDraggableList()Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->h:Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

    .line 2723998
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->h:Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;

    iget-object v3, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->t:LX/61y;

    .line 2723999
    iput-object v3, v1, LX/620;->m:LX/61y;

    .line 2724000
    const/16 v1, 0x2b

    const v3, -0x3f2d54e5

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1a8c0607

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2723977
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onPause()V

    .line 2723978
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->n:LX/2EJ;

    if-eqz v1, :cond_0

    .line 2723979
    iget-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->n:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->dismiss()V

    .line 2723980
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->n:LX/2EJ;

    .line 2723981
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x695a1450

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5ffa2e2a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2723972
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onResume()V

    .line 2723973
    iget-boolean v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->j:Z

    if-eqz v1, :cond_0

    .line 2723974
    invoke-virtual {p0}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->l()V

    .line 2723975
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->j:Z

    .line 2723976
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x53d5fb3e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2723965
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2723966
    const-string v0, "dirty"

    iget-boolean v1, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2723967
    iget-boolean v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->k:Z

    if-eqz v0, :cond_0

    .line 2723968
    iget-object v0, p0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    .line 2723969
    iget-object v1, v0, LX/Jgt;->h:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 2723970
    const-string v1, "favorites"

    new-instance v2, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;

    iget-object p0, v0, LX/Jgt;->h:Ljava/util/List;

    invoke-direct {v2, p0}, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2723971
    :cond_0
    return-void
.end method
