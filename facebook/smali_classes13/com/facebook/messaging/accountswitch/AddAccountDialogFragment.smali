.class public Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;
.super Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
.source ""


# instance fields
.field private u:Landroid/widget/TextView;

.field public v:Landroid/widget/EditText;

.field private w:Landroid/widget/EditText;

.field public x:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718587
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;-><init>()V

    return-void
.end method

.method public static t(Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;)V
    .locals 1

    .prologue
    .line 2718641
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->c(Z)V

    .line 2718642
    return-void

    .line 2718643
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718640
    const-string v0, "mswitch_accounts_add"

    return-object v0
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 2718637
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2718638
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2718639
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 4

    .prologue
    .line 2718623
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2718624
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_1

    .line 2718625
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2718626
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2718627
    if-eqz v0, :cond_1

    .line 2718628
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v1, 0x196

    if-ne v0, v1, :cond_1

    .line 2718629
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    move-object v0, v0

    .line 2718630
    if-eqz v0, :cond_0

    .line 2718631
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v2, "_op_redirect"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->a()Ljava/lang/String;

    move-result-object v3

    const/4 p1, 0x0

    invoke-virtual {v1, v2, v3, p1}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718632
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.messaging.accountswitch.TWO_FAC_AUTH_REQUIRED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718633
    const-string v2, "user_id"

    sget-object v3, LX/1IA;->WHITESPACE:LX/1IA;

    iget-object p1, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718634
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Landroid/content/Intent;)V

    .line 2718635
    :cond_0
    const/4 v0, 0x1

    .line 2718636
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 2718622
    const v0, 0x7f031455

    return v0
.end method

.method public final l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2718608
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->n()V

    .line 2718609
    const v0, 0x7f0d2e60

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->v:Landroid/widget/EditText;

    .line 2718610
    const v0, 0x7f0d29ab

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->w:Landroid/widget/EditText;

    .line 2718611
    const v0, 0x7f0d2e61

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->x:Landroid/widget/CheckBox;

    .line 2718612
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->p:LX/0Uh;

    const/16 v1, 0xfc

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2718613
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->x:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2718614
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->x:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2718615
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->w:Landroid/widget/EditText;

    new-instance v1, LX/Jcr;

    invoke-direct {v1, p0}, LX/Jcr;-><init>(Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2718616
    new-instance v0, LX/Jcs;

    invoke-direct {v0, p0}, LX/Jcs;-><init>(Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;)V

    .line 2718617
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2718618
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2718619
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->t(Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;)V

    .line 2718620
    return-void

    .line 2718621
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->x:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final m()V
    .locals 5

    .prologue
    .line 2718596
    sget-object v0, LX/1IA;->WHITESPACE:LX/1IA;

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2718597
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 2718598
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2718599
    :goto_0
    return-void

    .line 2718600
    :cond_0
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->x:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 2718601
    :goto_1
    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/2Vv;->i:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2718602
    new-instance v2, Lcom/facebook/auth/credentials/PasswordCredentials;

    sget-object v3, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v2, v0, v1, v3}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    .line 2718603
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2718604
    const-string v4, "passwordCredentials"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2718605
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Landroid/os/Bundle;)V

    .line 2718606
    const-string v2, "auth_switch_accounts"

    invoke-virtual {p0, v2, v3}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2718607
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public n()V
    .locals 2

    .prologue
    .line 2718588
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2718589
    const v1, 0x7f083b33

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(I)V

    .line 2718590
    const v1, 0x7f083b35

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;)V

    .line 2718591
    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Ljava/lang/String;)V

    .line 2718592
    const v0, 0x7f0d0cd7

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->u:Landroid/widget/TextView;

    .line 2718593
    const v0, 0x7f083b34

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2718594
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718595
    return-void
.end method
