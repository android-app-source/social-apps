.class public Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final x:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0Yb;

.field public B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:LX/3Kt;

.field private final E:LX/JdA;

.field private F:Landroid/support/v7/widget/RecyclerView;

.field public a:LX/67a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Jcq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Os;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2UW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/4Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2UX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2Vu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/2Oq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/3Fd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2uq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/2Or;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/FMo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/30m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2719276
    const-class v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->x:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2719306
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2719307
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->z:Z

    .line 2719308
    new-instance v0, LX/JdA;

    invoke-direct {v0, p0}, LX/JdA;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->E:LX/JdA;

    return-void
.end method

.method private static a(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;LX/67a;LX/Jcq;LX/0Or;LX/2Os;LX/2UW;LX/4Ad;LX/GvB;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2UX;LX/2Vu;LX/0aG;Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;LX/03V;LX/0ad;LX/2Oq;LX/3Fd;LX/2uq;LX/2Or;LX/0Sh;LX/0Xl;LX/FMo;LX/30m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;",
            "LX/67a;",
            "LX/Jcq;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2Os;",
            "LX/2UW;",
            "LX/4Ad;",
            "Lcom/facebook/auth/login/ipc/RedirectableLaunchAuthActivityUtil;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/2UX;",
            "LX/2Vu;",
            "LX/0aG;",
            "Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/2Oq;",
            "LX/3Fd;",
            "LX/2uq;",
            "LX/2Or;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Xl;",
            "LX/FMo;",
            "LX/30m;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2719305
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a:LX/67a;

    iput-object p2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b:LX/Jcq;

    iput-object p3, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    iput-object p5, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->e:LX/2UW;

    iput-object p6, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->f:LX/4Ad;

    iput-object p7, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->g:LX/GvB;

    iput-object p8, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p9, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->i:LX/0SG;

    iput-object p10, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->j:LX/2UX;

    iput-object p11, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->k:LX/2Vu;

    iput-object p12, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->l:LX/0aG;

    iput-object p13, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->m:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    iput-object p14, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->n:LX/03V;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->o:LX/0ad;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->p:LX/2Oq;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->q:LX/3Fd;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->r:LX/2uq;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->s:LX/2Or;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->t:LX/0Sh;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->u:LX/0Xl;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->v:LX/FMo;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->w:LX/30m;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 25

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v24

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-static/range {v24 .. v24}, LX/67a;->a(LX/0QB;)LX/67a;

    move-result-object v2

    check-cast v2, LX/67a;

    invoke-static/range {v24 .. v24}, LX/Jcq;->a(LX/0QB;)LX/Jcq;

    move-result-object v3

    check-cast v3, LX/Jcq;

    const/16 v4, 0x12cb

    move-object/from16 v0, v24

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {v24 .. v24}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v5

    check-cast v5, LX/2Os;

    invoke-static/range {v24 .. v24}, LX/2UW;->a(LX/0QB;)LX/2UW;

    move-result-object v6

    check-cast v6, LX/2UW;

    invoke-static/range {v24 .. v24}, LX/41d;->a(LX/0QB;)LX/4Ad;

    move-result-object v7

    check-cast v7, LX/4Ad;

    invoke-static/range {v24 .. v24}, LX/GvB;->a(LX/0QB;)LX/GvB;

    move-result-object v8

    check-cast v8, LX/GvB;

    invoke-static/range {v24 .. v24}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v24 .. v24}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static/range {v24 .. v24}, LX/2UX;->a(LX/0QB;)LX/2UX;

    move-result-object v11

    check-cast v11, LX/2UX;

    invoke-static/range {v24 .. v24}, LX/2Vu;->a(LX/0QB;)LX/2Vu;

    move-result-object v12

    check-cast v12, LX/2Vu;

    invoke-static/range {v24 .. v24}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v13

    check-cast v13, LX/0aG;

    invoke-static/range {v24 .. v24}, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->a(LX/0QB;)Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    move-result-object v14

    check-cast v14, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    invoke-static/range {v24 .. v24}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v15

    check-cast v15, LX/03V;

    invoke-static/range {v24 .. v24}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {v24 .. v24}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v17

    check-cast v17, LX/2Oq;

    invoke-static/range {v24 .. v24}, LX/3Fd;->a(LX/0QB;)LX/3Fd;

    move-result-object v18

    check-cast v18, LX/3Fd;

    invoke-static/range {v24 .. v24}, LX/2uq;->a(LX/0QB;)LX/2uq;

    move-result-object v19

    check-cast v19, LX/2uq;

    invoke-static/range {v24 .. v24}, LX/2Or;->a(LX/0QB;)LX/2Or;

    move-result-object v20

    check-cast v20, LX/2Or;

    invoke-static/range {v24 .. v24}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v21

    check-cast v21, LX/0Sh;

    invoke-static/range {v24 .. v24}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v22

    check-cast v22, LX/0Xl;

    invoke-static/range {v24 .. v24}, LX/FMo;->a(LX/0QB;)LX/FMo;

    move-result-object v23

    check-cast v23, LX/FMo;

    invoke-static/range {v24 .. v24}, LX/30m;->a(LX/0QB;)LX/30m;

    move-result-object v24

    check-cast v24, LX/30m;

    invoke-static/range {v1 .. v24}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;LX/67a;LX/Jcq;LX/0Or;LX/2Os;LX/2UW;LX/4Ad;LX/GvB;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2UX;LX/2Vu;LX/0aG;Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;LX/03V;LX/0ad;LX/2Oq;LX/3Fd;LX/2uq;LX/2Or;LX/0Sh;LX/0Xl;LX/FMo;LX/30m;)V

    return-void
.end method

.method public static b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2719294
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719295
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    if-eqz v0, :cond_0

    .line 2719296
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2719297
    iput-object v1, v0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    .line 2719298
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2719299
    iput-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2719300
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2719301
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2719302
    iput-object p0, v0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    .line 2719303
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v2, "dialog"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2719304
    return-void
.end method

.method public static d(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V
    .locals 1

    .prologue
    .line 2719290
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v0}, LX/2Os;->c()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    .line 2719291
    if-eqz v0, :cond_0

    .line 2719292
    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->C:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2719293
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V
    .locals 3

    .prologue
    .line 2719286
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->f:LX/4Ad;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Ad;->a(Landroid/content/Context;)LX/3dh;

    move-result-object v0

    .line 2719287
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    iget-object v2, v0, LX/3dh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v1}, LX/2Os;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2719288
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-static {v0}, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->a(LX/3dh;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2Os;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    .line 2719289
    :cond_0
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 2719277
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v0}, LX/2Os;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2719278
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v0}, LX/2Os;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2719279
    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2719280
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->m:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    .line 2719281
    iget-object v2, v0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 2719282
    new-instance p0, Landroid/content/Intent;

    sget-object v3, LX/2b2;->q:Ljava/lang/String;

    invoke-direct {p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2719283
    const-string v3, "multiple_accounts_user_ids"

    invoke-virtual {p0, v3, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2719284
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, p0, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2719285
    return-void
.end method

.method public static n(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V
    .locals 1

    .prologue
    .line 2719073
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v0}, LX/2Os;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719074
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->s(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719075
    :goto_0
    return-void

    .line 2719076
    :cond_0
    new-instance v0, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;-><init>()V

    .line 2719077
    move-object v0, v0

    .line 2719078
    invoke-static {p0, v0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_0
.end method

.method public static r(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2719272
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2719273
    if-eqz v0, :cond_0

    .line 2719274
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2719275
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V
    .locals 3

    .prologue
    .line 2719269
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->j:LX/2UX;

    const-string v1, "mswitchaccounts_max_reached_show"

    invoke-virtual {v0, v1}, LX/2UX;->a(Ljava/lang/String;)V

    .line 2719270
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f083b41

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f083b42

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08001a

    new-instance v2, LX/JdB;

    invoke-direct {v2, p0}, LX/JdB;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2719271
    return-void
.end method

.method public static t(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V
    .locals 6

    .prologue
    .line 2719245
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->t:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2719246
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->p:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2719247
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->c:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719248
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Kr;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2719249
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2719250
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->p:LX/2Oq;

    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2719251
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2719252
    invoke-static {v1}, LX/2Oq;->h(LX/2Oq;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2719253
    iget-object v2, v1, LX/2Oq;->i:LX/3Fd;

    .line 2719254
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2719255
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->p:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->c()LX/3Kt;

    move-result-object v0

    .line 2719256
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->D:LX/3Kt;

    if-eq v1, v0, :cond_2

    .line 2719257
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->v:LX/FMo;

    invoke-virtual {v1}, LX/FMo;->a()V

    .line 2719258
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->w:LX/30m;

    sget-object v2, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->x:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->D:LX/3Kt;

    invoke-virtual {v1, v2, v3, v0}, LX/30m;->a(Ljava/lang/Object;LX/3Kt;LX/3Kt;)V

    .line 2719259
    :cond_2
    return-void

    .line 2719260
    :cond_3
    const/4 v3, 0x0

    .line 2719261
    iget-object v4, v2, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/3Kr;->ab:LX/0Tn;

    const-string v1, ""

    invoke-interface {v4, v5, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2719262
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2719263
    :goto_1
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2719264
    iget-object v3, v2, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/3Kr;->ab:LX/0Tn;

    invoke-interface {v3, v4, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    goto :goto_0

    .line 2719265
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "(\\s|^)"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v1, "(\\s|$)"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2719266
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2719267
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2719268
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v0, v3

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2719309
    const-string v0, "mswitch_accounts_list"

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 2719193
    const-string v0, "com.facebook.messaging.accountswitch.SWITH_OPERATION_COMPLETE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2719194
    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b:LX/Jcq;

    .line 2719195
    const/4 v5, 0x1

    iput-boolean v5, v4, LX/Jcq;->o:Z

    .line 2719196
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "account_switch_result"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;

    .line 2719197
    iget-object v5, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->a:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    iget-object v6, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v5

    .line 2719198
    :goto_0
    if-nez v5, :cond_0

    .line 2719199
    iget-object v6, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->C:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->C:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v6, v6, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iget-object v7, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2719200
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->C:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2719201
    :cond_0
    :goto_1
    if-eqz v5, :cond_2

    .line 2719202
    new-instance v6, LX/2ud;

    invoke-direct {v6}, LX/2ud;-><init>()V

    invoke-virtual {v6, v5}, LX/2ud;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)LX/2ud;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    .line 2719203
    iput-wide v6, v5, LX/2ud;->c:J

    .line 2719204
    move-object v5, v5

    .line 2719205
    iget-object v6, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->b:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 2719206
    iget-object v6, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->b:Ljava/lang/String;

    .line 2719207
    iput-object v6, v5, LX/2ud;->d:Ljava/lang/String;

    .line 2719208
    :cond_1
    iget-object v6, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v5}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/2Os;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    .line 2719209
    :cond_2
    iget-object v4, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->c:Lcom/facebook/auth/component/AuthenticationResult;

    invoke-interface {v4}, Lcom/facebook/auth/component/AuthenticationResult;->a()Ljava/lang/String;

    move-result-object v4

    .line 2719210
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->e:LX/2UW;

    .line 2719211
    invoke-static {v4}, LX/2Vv;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v6

    .line 2719212
    iget-object v7, v5, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    invoke-interface {v7, v6}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 2719213
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v5, v4}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v5

    .line 2719214
    if-nez v5, :cond_3

    .line 2719215
    new-instance v5, LX/2ud;

    invoke-direct {v5}, LX/2ud;-><init>()V

    .line 2719216
    iput-object v4, v5, LX/2ud;->a:Ljava/lang/String;

    .line 2719217
    move-object v4, v5

    .line 2719218
    invoke-virtual {v4}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v4

    .line 2719219
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v5, v4}, LX/2Os;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    .line 2719220
    :cond_3
    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/2Vv;->h:LX/0Tn;

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2719221
    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->g:LX/GvB;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/GvB;->a(Landroid/app/Activity;)V

    .line 2719222
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2719223
    :cond_4
    :goto_2
    return-void

    .line 2719224
    :cond_5
    const-string v0, "com.facebook.messaging.accountswitch.TWO_FAC_AUTH_REQUIRED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2719225
    const-string v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2719226
    new-instance v1, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;-><init>()V

    .line 2719227
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2719228
    const-string v3, "user_id"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2719229
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2719230
    move-object v0, v1

    .line 2719231
    invoke-static {p0, v0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_2

    .line 2719232
    :cond_6
    const-string v0, "com.facebook.messaging.accountswitch.PASWORD_REENTRY_REQUIRED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2719233
    const-string v0, "name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2719234
    const-string v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2719235
    const-string v2, "enable_dbl"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2719236
    new-instance v3, LX/2ud;

    invoke-direct {v3}, LX/2ud;-><init>()V

    .line 2719237
    iput-object v0, v3, LX/2ud;->b:Ljava/lang/String;

    .line 2719238
    move-object v0, v3

    .line 2719239
    iput-object v1, v0, LX/2ud;->a:Ljava/lang/String;

    .line 2719240
    move-object v0, v0

    .line 2719241
    invoke-virtual {v0}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    .line 2719242
    invoke-static {v0, v2}, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;Z)Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_2

    .line 2719243
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 2719244
    :cond_8
    iget-object v6, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->n:LX/03V;

    const-string v7, "Unable to retrieve outgoing user"

    iget-object v8, v4, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2719167
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2719168
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2719169
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b:LX/Jcq;

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->E:LX/JdA;

    .line 2719170
    iput-object v2, v0, LX/Jcq;->l:LX/JdA;

    .line 2719171
    iput-object v1, v0, LX/Jcq;->m:LX/2Os;

    .line 2719172
    iget-object v5, v0, LX/Jcq;->m:LX/2Os;

    .line 2719173
    iput-object v0, v5, LX/2Os;->e:LX/Jcq;

    .line 2719174
    const/4 v5, 0x0

    iput-boolean v5, v0, LX/Jcq;->o:Z

    .line 2719175
    invoke-static {v0}, LX/Jcq;->f(LX/Jcq;)V

    .line 2719176
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719177
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->e(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719178
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b:LX/Jcq;

    .line 2719179
    iget-object v1, v0, LX/Jcq;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move v0, v1

    .line 2719180
    if-gt v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->o:LX/0ad;

    sget-short v1, LX/Jd1;->b:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719181
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->n(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719182
    :cond_0
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2719183
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a:LX/67a;

    new-instance v1, LX/67b;

    invoke-direct {v1, p0}, LX/67b;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 2719184
    iput-object v1, v0, LX/67a;->b:LX/67V;

    .line 2719185
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a:LX/67a;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2719186
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a:LX/67a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/67a;->a(I)Z

    .line 2719187
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Vv;->d:LX/0Tn;

    invoke-interface {v0, v1, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2719188
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->e:LX/2UW;

    invoke-virtual {v0}, LX/2UW;->b()V

    .line 2719189
    if-eqz p1, :cond_1

    .line 2719190
    const-string v0, "unseen_fetched"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->y:Z

    .line 2719191
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->p:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->c()LX/3Kt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->D:LX/3Kt;

    .line 2719192
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2719161
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 2719162
    instance-of v0, p1, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    if-eqz v0, :cond_0

    .line 2719163
    check-cast p1, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2719164
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2719165
    iput-object p0, v0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    .line 2719166
    :cond_0
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2719145
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2719146
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2719147
    const v2, 0x7f110014

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2719148
    const v2, 0x7f0d3220

    const v3, 0x7f020f65

    .line 2719149
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 2719150
    if-nez v4, :cond_2

    .line 2719151
    :goto_0
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->o:LX/0ad;

    sget-short v3, LX/Jd1;->a:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2719152
    const v3, 0x7f0d3220

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 2719153
    if-eqz v3, :cond_1

    .line 2719154
    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2719155
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a:LX/67a;

    invoke-virtual {v0}, LX/67a;->f()LX/3u1;

    move-result-object v0

    .line 2719156
    const v2, 0x7f083b30

    invoke-virtual {v0, v2}, LX/3u1;->b(I)V

    .line 2719157
    invoke-virtual {v0, v1}, LX/3u1;->a(Z)V

    .line 2719158
    return-void

    .line 2719159
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 2719160
    const v6, 0x7f0e0574

    const p2, 0x7f0a0190

    invoke-static {v5, p2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p2

    invoke-static {v5, v6, v3, p2}, LX/Hp3;->a(Landroid/content/Context;III)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xd92d69d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719144
    const v1, 0x7f030b06

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x67c6332e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x153dd153

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719140
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2719141
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->A:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2719142
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->A:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2719143
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x11e2a185

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2719136
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3220

    if-ne v0, v1, :cond_0

    .line 2719137
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->n(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719138
    const/4 v0, 0x1

    .line 2719139
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x1258fd34

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719092
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2719093
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->e(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719094
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2719095
    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2719096
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2719097
    if-eqz v2, :cond_4

    const-string v3, "trigger_sso_on_resume"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2719098
    if-eqz v2, :cond_2

    .line 2719099
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->f:LX/4Ad;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ad;->a(Landroid/content/Context;)LX/3dh;

    move-result-object v2

    .line 2719100
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2719101
    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    iget-object v3, v2, LX/3dh;->a:Ljava/lang/String;

    .line 2719102
    iget-object v4, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v4

    .line 2719103
    invoke-static {v3, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2719104
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v1}, LX/2Os;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2719105
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    iget-object v3, v2, LX/3dh;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2719106
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->s(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719107
    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->k()V

    .line 2719108
    iget-boolean v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->y:Z

    if-eqz v1, :cond_7

    .line 2719109
    :goto_3
    iget-boolean v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->z:Z

    if-eqz v1, :cond_1

    .line 2719110
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->t(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719111
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->z:Z

    .line 2719112
    :cond_1
    const v1, 0x55b86883

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2719113
    :cond_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2719114
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v2, v1}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v2

    .line 2719115
    if-eqz v2, :cond_9

    iget-object v3, v2, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 2719116
    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->k:LX/2Vu;

    invoke-virtual {v3, v1}, LX/2Vu;->a(Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;

    move-result-object v3

    .line 2719117
    if-eqz v3, :cond_8

    .line 2719118
    iget-object v2, v2, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->a(Ljava/lang/String;Lcom/facebook/dbllite/data/DblLiteCredentials;)Lcom/facebook/messaging/accountswitch/DblDialogFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    .line 2719119
    :goto_4
    goto :goto_2

    :cond_3
    const-string v2, "trigger_switch_user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 2719120
    :cond_5
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->j:LX/2UX;

    const-string v3, "mswitchaccounts_sso_diode"

    invoke-virtual {v1, v3}, LX/2UX;->a(Ljava/lang/String;)V

    .line 2719121
    invoke-static {v2}, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->a(LX/3dh;)Lcom/facebook/messaging/accountswitch/SsoDialogFragment;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    .line 2719122
    :cond_6
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2719123
    if-eqz v1, :cond_0

    .line 2719124
    const-string v2, "trigger_sso_on_resume"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 2719125
    :cond_7
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->m:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    invoke-virtual {v1}, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->a()LX/1ML;

    .line 2719126
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->m:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    const/4 v2, 0x0

    .line 2719127
    iput-boolean v2, v1, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->f:Z

    .line 2719128
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->y:Z

    goto :goto_3

    .line 2719129
    :cond_8
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;Z)Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_4

    .line 2719130
    :cond_9
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->d:LX/2Os;

    invoke-virtual {v2}, LX/2Os;->b()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2719131
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->s(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    .line 2719132
    :goto_5
    goto :goto_4

    .line 2719133
    :cond_a
    new-instance v2, Lcom/facebook/messaging/accountswitch/AddDiodeAccountDialogFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/accountswitch/AddDiodeAccountDialogFragment;-><init>()V

    .line 2719134
    move-object v2, v2

    .line 2719135
    invoke-static {p0, v2}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    goto :goto_5
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2719089
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2719090
    const-string v0, "unseen_fetched"

    iget-boolean v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2719091
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4c01cb33

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719085
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2719086
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->m:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    const/4 v2, 0x1

    .line 2719087
    iput-boolean v2, v1, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->f:Z

    .line 2719088
    const/16 v1, 0x2b

    const v2, -0x315c75ba

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2719079
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2719080
    const v0, 0x7f0d1bd6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->F:Landroid/support/v7/widget/RecyclerView;

    .line 2719081
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->F:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->b:LX/Jcq;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2719082
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->F:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2719083
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->F:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/Jd0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Jd0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2719084
    return-void
.end method
