.class public Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;
.super Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
.source ""


# instance fields
.field public u:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

.field private v:Z

.field private w:Landroid/widget/EditText;

.field public x:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2719402
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;Z)Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;
    .locals 3

    .prologue
    .line 2719396
    new-instance v0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;-><init>()V

    .line 2719397
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2719398
    const-string v2, "account_info"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2719399
    const-string v2, "default_dbl_enabled"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2719400
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2719401
    return-object v0
.end method

.method public static n(Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;)V
    .locals 1

    .prologue
    .line 2719393
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->c(Z)V

    .line 2719394
    return-void

    .line 2719395
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2719392
    const-string v0, "mswitch_accounts_saved"

    return-object v0
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 2719403
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2719404
    const-string v0, "account_info"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "default_dbl_enabled"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2719405
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There should be info on the account and default dbl enabled!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2719406
    :cond_1
    const-string v0, "account_info"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->u:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2719407
    const-string v0, "default_dbl_enabled"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->v:Z

    .line 2719408
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2719409
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2719410
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 4

    .prologue
    .line 2719378
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2719379
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_1

    .line 2719380
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2719381
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2719382
    if-eqz v0, :cond_1

    .line 2719383
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v1, 0x196

    if-ne v0, v1, :cond_1

    .line 2719384
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    move-object v0, v0

    .line 2719385
    if-eqz v0, :cond_0

    .line 2719386
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v2, "_op_redirect"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->a()Ljava/lang/String;

    move-result-object v3

    const/4 p1, 0x0

    invoke-virtual {v1, v2, v3, p1}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2719387
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.messaging.accountswitch.TWO_FAC_AUTH_REQUIRED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2719388
    const-string v2, "user_id"

    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->u:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v3, v3, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2719389
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Landroid/content/Intent;)V

    .line 2719390
    :cond_0
    const/4 v0, 0x1

    .line 2719391
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2719377
    const v0, 0x7f031458

    return v0
.end method

.method public final l()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2719359
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2719360
    const v0, 0x7f083b32

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->u:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v5, v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/CharSequence;)V

    .line 2719361
    const v0, 0x7f0d0cd7

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2719362
    const v4, 0x7f083b36

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2719363
    const v0, 0x7f08001a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;)V

    .line 2719364
    const v0, 0x7f080017

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Ljava/lang/String;)V

    .line 2719365
    const v0, 0x7f0d29ab

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->w:Landroid/widget/EditText;

    .line 2719366
    const v0, 0x7f0d2e61

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->x:Landroid/widget/CheckBox;

    .line 2719367
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->p:LX/0Uh;

    const/16 v3, 0xfc

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2719368
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->x:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2719369
    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->x:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->v:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2719370
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->w:Landroid/widget/EditText;

    new-instance v1, LX/JdI;

    invoke-direct {v1, p0}, LX/JdI;-><init>(Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2719371
    new-instance v0, LX/JdJ;

    invoke-direct {v0, p0}, LX/JdJ;-><init>(Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;)V

    .line 2719372
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2719373
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->n(Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;)V

    .line 2719374
    return-void

    :cond_0
    move v0, v2

    .line 2719375
    goto :goto_0

    .line 2719376
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->x:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_1
.end method

.method public final m()V
    .locals 5

    .prologue
    .line 2719347
    sget-object v0, LX/1IA;->WHITESPACE:LX/1IA;

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2719348
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->u:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    .line 2719349
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2719350
    :goto_0
    return-void

    .line 2719351
    :cond_0
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchSavedAccountDialogFragment;->x:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 2719352
    :goto_1
    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/2Vv;->i:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2719353
    new-instance v2, Lcom/facebook/auth/credentials/PasswordCredentials;

    sget-object v3, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v2, v1, v0, v3}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    .line 2719354
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2719355
    const-string v4, "passwordCredentials"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2719356
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Landroid/os/Bundle;)V

    .line 2719357
    const-string v2, "auth_switch_accounts"

    invoke-virtual {p0, v2, v3}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2719358
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
