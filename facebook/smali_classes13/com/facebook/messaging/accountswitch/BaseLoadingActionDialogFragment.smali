.class public abstract Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field private A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

.field public m:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/2UX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2Os;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/annotations/IsUnseenCountFetchingForAccountSwitchingEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field public x:Landroid/view/View;

.field public y:Landroid/view/View;

.field public z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718527
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2718528
    return-void
.end method

.method private a(Lcom/facebook/fbservice/ops/BlueServiceFragment;)V
    .locals 2

    .prologue
    .line 2718529
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2718530
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v1, LX/Jcx;

    invoke-direct {v1, p0}, LX/Jcx;-><init>(Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    .line 2718531
    iput-object v1, v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 2718532
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2718533
    new-instance v0, LX/Jcu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/Jcu;-><init>(Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;Landroid/content/Context;I)V

    .line 2718534
    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 2718535
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    if-nez v1, :cond_0

    .line 2718536
    const-string v1, "loading_operation"

    .line 2718537
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v2

    move-object v1, v2

    .line 2718538
    invoke-direct {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Lcom/facebook/fbservice/ops/BlueServiceFragment;)V

    .line 2718539
    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Landroid/app/Dialog;)V

    .line 2718540
    return-object v0
.end method

.method public abstract a(Landroid/app/Dialog;)V
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2718541
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718542
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2718543
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2718544
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2718545
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    .line 2718546
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2718547
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2718548
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->start(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2718549
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v1, "_op_start"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718550
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->s()V

    .line 2718551
    return-void
.end method

.method public abstract a(Lcom/facebook/fbservice/service/ServiceException;)Z
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2718552
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2718553
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2718554
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2718555
    :goto_0
    if-eqz v0, :cond_2

    .line 2718556
    :cond_0
    :goto_1
    return-void

    .line 2718557
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2718558
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2718559
    if-eqz v0, :cond_0

    .line 2718560
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->q:LX/2Os;

    invoke-virtual {v1, v0}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    .line 2718561
    if-eqz v0, :cond_0

    .line 2718562
    iget-object v0, v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2718563
    const-string v0, "alternative_token_app_id"

    const-string v1, "1517268191927890"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2718564
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2718565
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2718566
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2718567
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2718568
    if-eqz v0, :cond_0

    .line 2718569
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 2718570
    packed-switch v0, :pswitch_data_0

    .line 2718571
    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 2718572
    if-nez v0, :cond_1

    .line 2718573
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2718574
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_2

    .line 2718575
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2718576
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v0, v1

    .line 2718577
    :goto_1
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v2, "_op_failure"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718578
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->n:LX/1CX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/Context;)LX/4mn;

    move-result-object v1

    .line 2718579
    iput-object p1, v1, LX/4mn;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 2718580
    move-object v1, v1

    .line 2718581
    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 2718582
    return-void

    .line 2718583
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v2, "_op_usererror"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718584
    const/4 v0, 0x1

    goto :goto_0

    .line 2718585
    :cond_2
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2718586
    invoke-virtual {v0}, LX/1nY;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x190
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2718523
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->w:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2718524
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2718525
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2718526
    return-void
.end method

.method public abstract k()I
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2718479
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 2718480
    instance-of v0, p1, Lcom/facebook/fbservice/ops/BlueServiceFragment;

    if-eqz v0, :cond_0

    .line 2718481
    check-cast p1, Lcom/facebook/fbservice/ops/BlueServiceFragment;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Lcom/facebook/fbservice/ops/BlueServiceFragment;)V

    .line 2718482
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x268cd928

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2718483
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2718484
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    invoke-static {v1}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v1}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v6

    check-cast v6, LX/1CX;

    invoke-static {v1}, LX/2UX;->b(LX/0QB;)LX/2UX;

    move-result-object v7

    check-cast v7, LX/2UX;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v1}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v9

    check-cast v9, LX/2Os;

    const/16 v10, 0x15e8

    invoke-static {v1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 p1, 0x14f3

    invoke-static {v1, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    iput-object v6, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->n:LX/1CX;

    iput-object v7, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    iput-object v8, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->p:LX/0Uh;

    iput-object v9, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->q:LX/2Os;

    iput-object v10, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r:LX/0Or;

    iput-object p1, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->s:LX/0Or;

    iput-object v1, v4, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2718485
    const v1, 0x7f0e0588

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2718486
    const/16 v1, 0x2b

    const v2, 0x41330b14

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x8065da4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2718487
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->k()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1cbf1a08

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 2718488
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2718489
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    if-eqz v0, :cond_1

    .line 2718490
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    const/4 p1, 0x0

    .line 2718491
    iget-object v1, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    if-ne v1, p0, :cond_0

    .line 2718492
    iput-object p1, v0, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->B:Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;

    .line 2718493
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    .line 2718494
    :cond_1
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x76f6b357

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2718495
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2718496
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->s()V

    .line 2718497
    const/16 v1, 0x2b

    const v2, -0x29bbc49e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2718498
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2718499
    const v0, 0x7f0d0cd6

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->u:Landroid/widget/TextView;

    .line 2718500
    const v0, 0x7f0d2e63

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->v:Landroid/widget/Button;

    .line 2718501
    const v0, 0x7f0d2e62

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->w:Landroid/widget/Button;

    .line 2718502
    const v0, 0x7f0d2e5f

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->x:Landroid/view/View;

    .line 2718503
    const v0, 0x7f0d2e64

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->y:Landroid/view/View;

    .line 2718504
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->s()V

    .line 2718505
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->l()V

    .line 2718506
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->v:Landroid/widget/Button;

    new-instance v1, LX/Jcv;

    invoke-direct {v1, p0}, LX/Jcv;-><init>(Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2718507
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->w:Landroid/widget/Button;

    new-instance v1, LX/Jcw;

    invoke-direct {v1, p0}, LX/Jcw;-><init>(Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2718508
    return-void
.end method

.method public final q()V
    .locals 4

    .prologue
    .line 2718509
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v1, "_flow_cancel"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718510
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2718511
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2718512
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->A:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    invoke-virtual {v0}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 2718513
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2718514
    const/4 v2, 0x0

    .line 2718515
    invoke-virtual {p0, v2}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 2718516
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->x:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2718517
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2718518
    :goto_0
    return-void

    .line 2718519
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 2718520
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2718521
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->y:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2718522
    goto :goto_0
.end method
