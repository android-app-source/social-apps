.class public Lcom/facebook/messaging/accountswitch/AddDiodeAccountDialogFragment;
.super Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;
.source ""


# instance fields
.field private u:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718657
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/AddAccountDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718658
    const-string v0, "mswitch_accounts_add_diode"

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2718656
    const v0, 0x7f031456

    return v0
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 2718648
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2718649
    const v1, 0x7f083b3a

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(I)V

    .line 2718650
    const v1, 0x7f083b3c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;)V

    .line 2718651
    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Ljava/lang/String;)V

    .line 2718652
    const v0, 0x7f0d0cd7

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/AddDiodeAccountDialogFragment;->u:Landroid/widget/TextView;

    .line 2718653
    const v0, 0x7f083b3b

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x7f080011

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2718654
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/AddDiodeAccountDialogFragment;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718655
    return-void
.end method
