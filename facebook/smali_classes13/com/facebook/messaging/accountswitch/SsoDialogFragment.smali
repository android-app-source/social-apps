.class public Lcom/facebook/messaging/accountswitch/SsoDialogFragment;
.super Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
.source ""


# instance fields
.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718876
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;-><init>()V

    return-void
.end method

.method public static a(LX/3dh;)Lcom/facebook/messaging/accountswitch/SsoDialogFragment;
    .locals 4

    .prologue
    .line 2718877
    new-instance v0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;-><init>()V

    .line 2718878
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2718879
    const-string v2, "sso_uid"

    iget-object v3, p0, LX/3dh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718880
    const-string v2, "sso_user_name"

    iget-object v3, p0, LX/3dh;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718881
    const-string v2, "sso_token"

    iget-object v3, p0, LX/3dh;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718882
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2718883
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718884
    const-string v0, "mswitch_accounts_sso"

    return-object v0
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 2718885
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2718886
    const-string v1, "sso_user_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "sso_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "sso_uid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2718887
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There should be info on the account!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2718888
    :cond_1
    const-string v1, "sso_uid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->u:Ljava/lang/String;

    .line 2718889
    const-string v1, "sso_user_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->v:Ljava/lang/String;

    .line 2718890
    const-string v1, "sso_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->w:Ljava/lang/String;

    .line 2718891
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 4

    .prologue
    .line 2718892
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2718893
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_1

    .line 2718894
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2718895
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2718896
    if-eqz v0, :cond_1

    .line 2718897
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 2718898
    const/16 v1, 0xbe

    if-ne v0, v1, :cond_1

    .line 2718899
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    move-object v0, v0

    .line 2718900
    if-eqz v0, :cond_0

    .line 2718901
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v2, "_op_redirect"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->a()Ljava/lang/String;

    move-result-object v3

    const/4 p1, 0x0

    invoke-virtual {v1, v2, v3, p1}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718902
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.messaging.accountswitch.PASWORD_REENTRY_REQUIRED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718903
    const-string v2, "name"

    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->v:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718904
    const-string v2, "user_id"

    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->u:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718905
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Landroid/content/Intent;)V

    .line 2718906
    :cond_0
    const/4 v0, 0x1

    .line 2718907
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2718908
    const v0, 0x7f031459

    return v0
.end method

.method public final l()V
    .locals 5

    .prologue
    .line 2718909
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2718910
    const v0, 0x7f083b3d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->v:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/CharSequence;)V

    .line 2718911
    const v0, 0x7f0d0cd7

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2718912
    const v2, 0x7f083b3e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718913
    const v0, 0x7f08001a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;)V

    .line 2718914
    const v0, 0x7f080017

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Ljava/lang/String;)V

    .line 2718915
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 2718916
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2718917
    :goto_0
    return-void

    .line 2718918
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2718919
    const-string v1, "accessToken"

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SsoDialogFragment;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718920
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Landroid/os/Bundle;)V

    .line 2718921
    const-string v1, "auth_switch_accounts_sso"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
