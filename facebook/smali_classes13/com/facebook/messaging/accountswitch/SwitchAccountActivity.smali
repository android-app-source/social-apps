.class public Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0l6;


# instance fields
.field private p:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/2UX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718963
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;LX/0Xl;LX/2UX;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 2718922
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->q:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->r:LX/2UX;

    iput-object p3, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;

    invoke-static {v2}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-static {v2}, LX/2UX;->b(LX/0QB;)LX/2UX;

    move-result-object v1

    check-cast v1, LX/2UX;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->a(Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;LX/0Xl;LX/2UX;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718962
    const-string v0, "mswitch_accounts"

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2718958
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2718959
    instance-of v0, p1, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    if-eqz v0, :cond_0

    .line 2718960
    check-cast p1, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->p:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    .line 2718961
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2718923
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2718924
    invoke-static {p0, p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2718925
    if-nez p1, :cond_1

    .line 2718926
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2718927
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_account_switch_redirect_source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2718928
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2718929
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2Vv;->j:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2718930
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->r:LX/2UX;

    const-string v2, "mswitchaccounts_account_switch_entered"

    .line 2718931
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2718932
    if-eqz v0, :cond_0

    .line 2718933
    invoke-virtual {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718934
    :cond_0
    invoke-static {v1, v3}, LX/2UX;->a(LX/2UX;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2718935
    :cond_1
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/2b2;->D:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718936
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->q:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2718937
    const v0, 0x7f030b05

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->setContentView(I)V

    .line 2718938
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->p:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    if-eqz v0, :cond_2

    .line 2718939
    :goto_0
    return-void

    .line 2718940
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2718941
    const-string v1, "com.facebook.messaging.accountswitch.TRIGGER_DIODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2718942
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mtouch_diode_user_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2718943
    new-instance v1, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;-><init>()V

    .line 2718944
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2718945
    const-string v3, "trigger_sso_on_resume"

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2718946
    const-string v3, "trigger_switch_user_id"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718947
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2718948
    move-object v0, v1

    .line 2718949
    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->p:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    .line 2718950
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1bd5

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->p:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0

    .line 2718951
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.messaging.accountswitch.TRIGGER_SSO"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 2718952
    new-instance v1, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;-><init>()V

    .line 2718953
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2718954
    const-string v3, "trigger_sso_on_resume"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2718955
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2718956
    move-object v0, v1

    .line 2718957
    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/SwitchAccountActivity;->p:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    goto :goto_1
.end method
