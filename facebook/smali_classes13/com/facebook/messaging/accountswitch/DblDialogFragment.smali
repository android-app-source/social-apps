.class public Lcom/facebook/messaging/accountswitch/DblDialogFragment;
.super Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
.source ""


# instance fields
.field public u:Ljava/lang/String;

.field public v:Lcom/facebook/dbllite/data/DblLiteCredentials;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718692
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/dbllite/data/DblLiteCredentials;)Lcom/facebook/messaging/accountswitch/DblDialogFragment;
    .locals 3

    .prologue
    .line 2718693
    new-instance v0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/accountswitch/DblDialogFragment;-><init>()V

    .line 2718694
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2718695
    const-string v2, "dbl_user_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718696
    const-string v2, "dbl_lite_cred"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2718697
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2718698
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718699
    const-string v0, "mswitch_accounts_dbl"

    return-object v0
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 2718700
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2718701
    const-string v1, "dbl_user_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "dbl_lite_cred"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2718702
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There should be info on the account!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2718703
    :cond_1
    const-string v1, "dbl_user_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->u:Ljava/lang/String;

    .line 2718704
    const-string v1, "dbl_lite_cred"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/dbllite/data/DblLiteCredentials;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->v:Lcom/facebook/dbllite/data/DblLiteCredentials;

    .line 2718705
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2718706
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2718707
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v2, :cond_4

    .line 2718708
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2718709
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2718710
    if-eqz v0, :cond_4

    .line 2718711
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    .line 2718712
    const/16 v3, 0x17d5

    if-eq v2, v3, :cond_0

    const/16 v3, 0x17d4

    if-eq v2, v3, :cond_0

    const/16 v3, 0x191

    if-ne v2, v3, :cond_2

    .line 2718713
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    move-object v0, v0

    .line 2718714
    if-eqz v0, :cond_1

    .line 2718715
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v3, "_op_redirect"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->a()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x0

    invoke-virtual {v2, v3, v4, p1}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718716
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.messaging.accountswitch.PASWORD_REENTRY_REQUIRED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718717
    const-string v3, "name"

    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718718
    const-string v3, "user_id"

    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->v:Lcom/facebook/dbllite/data/DblLiteCredentials;

    iget-object v4, v4, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718719
    const-string v3, "enable_dbl"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2718720
    invoke-virtual {v0, v2}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Landroid/content/Intent;)V

    .line 2718721
    :cond_1
    move v0, v1

    .line 2718722
    :goto_0
    return v0

    .line 2718723
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v2, 0x196

    if-ne v0, v2, :cond_4

    .line 2718724
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->z:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    move-object v0, v0

    .line 2718725
    if-eqz v0, :cond_3

    .line 2718726
    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->o:LX/2UX;

    const-string v3, "_op_redirect"

    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->a()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x0

    invoke-virtual {v2, v3, v4, p1}, LX/2UX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2718727
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.messaging.accountswitch.TWO_FAC_AUTH_REQUIRED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718728
    const-string v3, "user_id"

    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->v:Lcom/facebook/dbllite/data/DblLiteCredentials;

    iget-object v4, v4, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718729
    invoke-virtual {v0, v2}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->a(Landroid/content/Intent;)V

    .line 2718730
    :cond_3
    move v0, v1

    .line 2718731
    goto :goto_0

    .line 2718732
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2718733
    const v0, 0x7f031459

    return v0
.end method

.method public final l()V
    .locals 5

    .prologue
    .line 2718734
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2718735
    const v0, 0x7f083b3d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->u:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/CharSequence;)V

    .line 2718736
    const v0, 0x7f0d0cd7

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2718737
    const v2, 0x7f083b3e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2718738
    const v0, 0x7f08001a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;)V

    .line 2718739
    const v0, 0x7f080017

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Ljava/lang/String;)V

    .line 2718740
    return-void
.end method

.method public final m()V
    .locals 5

    .prologue
    .line 2718741
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2718742
    :goto_0
    return-void

    .line 2718743
    :cond_0
    new-instance v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->v:Lcom/facebook/dbllite/data/DblLiteCredentials;

    iget-object v1, v1, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/DblDialogFragment;->v:Lcom/facebook/dbllite/data/DblLiteCredentials;

    iget-object v2, v2, Lcom/facebook/dbllite/data/DblLiteCredentials;->nonce:Ljava/lang/String;

    const-string v3, ""

    sget-object v4, LX/41B;->DEVICE_BASED_LOGIN_TYPE:LX/41B;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41B;)V

    .line 2718744
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2718745
    const-string v2, "dblCredentials"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2718746
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Landroid/os/Bundle;)V

    .line 2718747
    const-string v0, "auth_switch_accounts_dbl"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
