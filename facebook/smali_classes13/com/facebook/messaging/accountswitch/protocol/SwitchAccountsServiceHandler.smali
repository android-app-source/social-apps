.class public Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/11H;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2Os;

.field private final d:LX/2UW;

.field private final e:LX/JdN;


# direct methods
.method public constructor <init>(LX/11H;LX/0Or;LX/2Os;LX/2UW;LX/JdN;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/2Os;",
            "LX/2UW;",
            "LX/JdN;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2719496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2719497
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->a:LX/11H;

    .line 2719498
    iput-object p2, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->b:LX/0Or;

    .line 2719499
    iput-object p3, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->c:LX/2Os;

    .line 2719500
    iput-object p4, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->d:LX/2UW;

    .line 2719501
    iput-object p5, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->e:LX/JdN;

    .line 2719502
    return-void
.end method

.method private a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 15

    .prologue
    .line 2719503
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2719504
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2719505
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v7, v7

    .line 2719506
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->c:LX/2Os;

    invoke-virtual {v5}, LX/2Os;->a()Ljava/util/List;

    move-result-object v5

    .line 2719507
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2719508
    iget-object v9, v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-static {v9, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 2719509
    new-instance v9, LX/JdL;

    iget-object v10, v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iget-object v11, v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    iget-wide v13, v5, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    invoke-direct {v9, v10, v11, v13, v14}, LX/JdL;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2719510
    :cond_1
    move-object v0, v6

    .line 2719511
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2719512
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2719513
    :goto_1
    return-object v0

    .line 2719514
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->e:LX/JdN;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "SwitchAccountsServiceHandler"

    invoke-static {v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2719515
    invoke-static {p0, v0}, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->a(Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;
    .locals 11

    .prologue
    .line 2719516
    const-class v1, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;

    monitor-enter v1

    .line 2719517
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2719518
    sput-object v2, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2719519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2719520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2719521
    new-instance v3, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    const/16 v5, 0x19e

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v6

    check-cast v6, LX/2Os;

    invoke-static {v0}, LX/2UW;->a(LX/0QB;)LX/2UW;

    move-result-object v7

    check-cast v7, LX/2UW;

    .line 2719522
    new-instance v9, LX/JdN;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v8

    check-cast v8, LX/0dC;

    const/16 v10, 0x19e

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct {v9, v8, v10}, LX/JdN;-><init>(LX/0dC;LX/0Or;)V

    .line 2719523
    move-object v8, v9

    .line 2719524
    check-cast v8, LX/JdN;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;-><init>(LX/11H;LX/0Or;LX/2Os;LX/2UW;LX/JdN;)V

    .line 2719525
    move-object v0, v3

    .line 2719526
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2719527
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2719528
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2719529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/JdM;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2719530
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2719531
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 2719532
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JdM;

    .line 2719533
    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->c:LX/2Os;

    iget-object v5, v0, LX/JdM;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v4

    .line 2719534
    if-eqz v4, :cond_0

    .line 2719535
    iget-boolean v5, v0, LX/JdM;->b:Z

    if-nez v5, :cond_1

    .line 2719536
    new-instance v0, LX/2ud;

    invoke-direct {v0}, LX/2ud;-><init>()V

    invoke-virtual {v0, v4}, LX/2ud;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)LX/2ud;

    move-result-object v0

    const/4 v4, 0x0

    .line 2719537
    iput-object v4, v0, LX/2ud;->d:Ljava/lang/String;

    .line 2719538
    move-object v0, v0

    .line 2719539
    invoke-virtual {v0}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    .line 2719540
    iget-object v4, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->c:LX/2Os;

    invoke-virtual {v4, v0}, LX/2Os;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    goto :goto_0

    .line 2719541
    :cond_1
    iget-object v5, v0, LX/JdM;->a:Ljava/lang/String;

    iget v6, v0, LX/JdM;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2719542
    iget-wide v6, v4, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    iget-wide v8, v0, LX/JdM;->e:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    iget-wide v6, v0, LX/JdM;->e:J

    iget-wide v8, v4, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 2719543
    new-instance v5, LX/2ud;

    invoke-direct {v5}, LX/2ud;-><init>()V

    invoke-virtual {v5, v4}, LX/2ud;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)LX/2ud;

    move-result-object v4

    iget-wide v6, v0, LX/JdM;->e:J

    .line 2719544
    iput-wide v6, v4, LX/2ud;->e:J

    .line 2719545
    move-object v4, v4

    .line 2719546
    invoke-virtual {v4}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v4

    .line 2719547
    iget-object v5, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->c:LX/2Os;

    invoke-virtual {v5, v4}, LX/2Os;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    .line 2719548
    :cond_2
    iget-object v4, v0, LX/JdM;->d:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 2719549
    new-instance v4, Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;

    iget-object v5, v0, LX/JdM;->a:Ljava/lang/String;

    iget-object v6, v0, LX/JdM;->d:Ljava/lang/String;

    iget v0, v0, LX/JdM;->f:I

    invoke-direct {v4, v5, v6, v0}, Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2719550
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->d:LX/2UW;

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 2719551
    iget-object v3, v0, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 2719552
    sget-object v3, LX/2Vv;->g:LX/0Tn;

    invoke-interface {v5, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    .line 2719553
    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v3

    invoke-virtual {v3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2719554
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, LX/2Vv;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    .line 2719555
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v5, v4, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    goto :goto_1

    .line 2719556
    :cond_4
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2719557
    return-object v1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2719558
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2719559
    const-string v1, "update_unseen_counts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2719560
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2719561
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
