.class public Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;
.super Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;
.source ""


# instance fields
.field public u:Ljava/lang/String;

.field private v:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2718875
    invoke-direct {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;)V
    .locals 1

    .prologue
    .line 2718844
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->c(Z)V

    .line 2718845
    return-void

    .line 2718846
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718874
    const-string v0, "mswitch_accounts_2fac"

    return-object v0
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 2718867
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2718868
    const-string v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2718869
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There should be info on the account!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2718870
    :cond_0
    const-string v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->u:Ljava/lang/String;

    .line 2718871
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2718872
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2718873
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 1

    .prologue
    .line 2718866
    const/4 v0, 0x0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2718865
    const v0, 0x7f031457

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2718855
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2718856
    const v1, 0x7f083b3f

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(I)V

    .line 2718857
    const v1, 0x7f08001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;)V

    .line 2718858
    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Ljava/lang/String;)V

    .line 2718859
    const v0, 0x7f0d0b0a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->v:Landroid/widget/EditText;

    .line 2718860
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->v:Landroid/widget/EditText;

    new-instance v1, LX/Jd7;

    invoke-direct {v1, p0}, LX/Jd7;-><init>(Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2718861
    new-instance v0, LX/Jd8;

    invoke-direct {v0, p0}, LX/Jd8;-><init>(Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;)V

    .line 2718862
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2718863
    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->b(Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;)V

    .line 2718864
    return-void
.end method

.method public final m()V
    .locals 5

    .prologue
    .line 2718847
    sget-object v0, LX/1IA;->WHITESPACE:LX/1IA;

    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2718848
    invoke-virtual {p0}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2718849
    :goto_0
    return-void

    .line 2718850
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2718851
    new-instance v2, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v3, p0, Lcom/facebook/messaging/accountswitch/LoginApprovalDialogFragment;->u:Ljava/lang/String;

    sget-object v4, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v2, v3, v0, v4}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    .line 2718852
    const-string v3, "passwordCredentials"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2718853
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->b(Landroid/os/Bundle;)V

    .line 2718854
    const-string v2, "auth_switch_accounts"

    invoke-virtual {p0, v2, v1}, Lcom/facebook/messaging/accountswitch/BaseLoadingActionDialogFragment;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
