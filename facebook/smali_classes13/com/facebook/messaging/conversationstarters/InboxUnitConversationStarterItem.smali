.class public Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/messaging/model/threads/ThreadSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/Ddy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2727175
    new-instance v0, LX/Jiy;

    invoke-direct {v0}, LX/Jiy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2727176
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2727177
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 2727178
    const-class v1, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 2727179
    const-string v1, "fields"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ddy;

    iput-object v0, p0, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;->b:LX/Ddy;

    .line 2727180
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2727181
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/Ddy;)V
    .locals 1
    .param p3    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727182
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2727183
    invoke-interface {p4}, LX/Ddx;->b()Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$ItemUserModel;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2727184
    iput-object p3, p0, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2727185
    iput-object p4, p0, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;->b:LX/Ddy;

    .line 2727186
    return-void

    .line 2727187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2727188
    sget-object v0, LX/DfY;->V2_CONVERSATION_STARTER:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 2727189
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2727190
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2727191
    const-string v1, "fields"

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2727192
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2727193
    iget-object v0, p0, Lcom/facebook/messaging/conversationstarters/InboxUnitConversationStarterItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2727194
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2727195
    sget-object v0, LX/Dfa;->V2_CONVERSATION_STARTER:LX/Dfa;

    return-object v0
.end method
