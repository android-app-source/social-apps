.class public Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;
.super Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;
.source ""


# instance fields
.field public a:LX/Jgk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Ddc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/preference/PreferenceScreen;

.field public f:LX/3Af;

.field public g:LX/3Af;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2723730
    invoke-direct {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;Landroid/preference/Preference;)LX/IZR;
    .locals 5

    .prologue
    .line 2723720
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;->j()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2723721
    new-instance v1, LX/IZQ;

    invoke-direct {v1, v0}, LX/IZQ;-><init>(Ljava/lang/String;)V

    .line 2723722
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;->j()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel$PageModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2723723
    iput-object v2, v1, LX/IZQ;->a:Ljava/lang/String;

    .line 2723724
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020f7a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2723725
    iput-object v2, v1, LX/IZQ;->b:Landroid/graphics/drawable/Drawable;

    .line 2723726
    new-instance v2, LX/Jgi;

    invoke-direct {v2, p0, p1, v0, p2}, LX/Jgi;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;Ljava/lang/String;Landroid/preference/Preference;)V

    .line 2723727
    iput-object v2, v1, LX/IZQ;->f:Landroid/view/View$OnClickListener;

    .line 2723728
    new-instance v0, LX/IZR;

    invoke-direct {v0, v1}, LX/IZR;-><init>(LX/IZQ;)V

    move-object v0, v0

    .line 2723729
    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2723719
    invoke-static {p1, p2}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2723718
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/preference/Preference;)V
    .locals 7

    .prologue
    .line 2723711
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->f:LX/3Af;

    .line 2723712
    new-instance v1, LX/7TY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2723713
    invoke-static {}, LX/Jgj;->values()[LX/Jgj;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2723714
    iget v5, v4, LX/Jgj;->optionStringId:I

    invoke-virtual {v1, v5}, LX/34c;->e(I)LX/3Ai;

    move-result-object v5

    new-instance v6, LX/Jgf;

    invoke-direct {v6, p0, v4, p1}, LX/Jgf;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;LX/Jgj;Landroid/preference/Preference;)V

    invoke-virtual {v5, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2723715
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2723716
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->f:LX/3Af;

    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2723717
    return-void
.end method

.method private a(Landroid/preference/Preference;Landroid/preference/PreferenceCategory;Landroid/preference/Preference;)V
    .locals 7

    .prologue
    .line 2723701
    new-instance v0, LX/Jgh;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Jgh;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;Landroid/preference/Preference;Landroid/preference/PreferenceCategory;Landroid/preference/Preference;)V

    .line 2723702
    iget-object v1, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->c:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    .line 2723703
    new-instance v3, LX/IbI;

    invoke-direct {v3}, LX/IbI;-><init>()V

    move-object v3, v3

    .line 2723704
    goto :goto_0

    .line 2723705
    :goto_0
    const-string v4, "profile_image_width"

    iget-object v5, v1, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->h:Landroid/content/res/Resources;

    const v6, 0x7f0b12f8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2723706
    const-string v4, "profile_image_height"

    iget-object v5, v1, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->h:Landroid/content/res/Resources;

    const v6, 0x7f0b12f8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2723707
    iget-object v4, v1, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->g:LX/1Ck;

    sget-object v5, LX/IcH;->GET_RIDE_PROVIDER:LX/IcH;

    iget-object v6, v1, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->e:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2723708
    new-instance v6, LX/IcG;

    invoke-direct {v6, v1, v0}, LX/IcG;-><init>(Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;LX/Jgh;)V

    move-object v6, v6

    .line 2723709
    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2723710
    return-void
.end method

.method private static a(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;LX/Jgk;LX/0W3;Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;LX/Ddc;)V
    .locals 0

    .prologue
    .line 2723731
    iput-object p1, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    iput-object p2, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->b:LX/0W3;

    iput-object p3, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->c:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    iput-object p4, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->d:LX/Ddc;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    invoke-static {v3}, LX/Jgk;->b(LX/0QB;)LX/Jgk;

    move-result-object v0

    check-cast v0, LX/Jgk;

    invoke-static {v3}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {v3}, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->b(LX/0QB;)Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    invoke-static {v3}, LX/Ddc;->b(LX/0QB;)LX/Ddc;

    move-result-object v3

    check-cast v3, LX/Ddc;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;LX/Jgk;LX/0W3;Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;LX/Ddc;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;LX/0Px;Landroid/preference/Preference;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;",
            ">;",
            "Landroid/preference/Preference;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2723692
    new-instance v2, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;-><init>(Landroid/content/Context;)V

    .line 2723693
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->g:LX/3Af;

    .line 2723694
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2723695
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2723696
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;

    invoke-direct {p0, v0, p2}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel$RideProvidersModel;Landroid/preference/Preference;)LX/IZR;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2723697
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2723698
    :cond_0
    iput-object v3, v2, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    .line 2723699
    iget-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->g:LX/3Af;

    invoke-virtual {v0, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2723700
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 2723653
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2723654
    new-instance v1, LX/4ok;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2723655
    const v2, 0x7f030d5c

    invoke-virtual {v1, v2}, LX/4ok;->setLayoutResource(I)V

    .line 2723656
    sget-object v2, LX/Iid;->b:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2723657
    const v2, 0x7f080572

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(I)V

    .line 2723658
    const v2, 0x7f080573

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(I)V

    .line 2723659
    iget-object v2, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    .line 2723660
    iget-object v3, v2, LX/Jgk;->a:LX/3QX;

    sget-object v4, LX/6hP;->OMNI_M_SUGGESTION_ENABLED_PREF:LX/6hP;

    invoke-virtual {v3, v4}, LX/3QX;->a(LX/6hP;)LX/0am;

    move-result-object v3

    .line 2723661
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2723662
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2723663
    :goto_0
    move v2, v3

    .line 2723664
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2723665
    new-instance v2, LX/Jgc;

    invoke-direct {v2, p0}, LX/Jgc;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;)V

    invoke-virtual {v1, v2}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2723666
    iget-object v2, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2723667
    iget-object v2, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->b:LX/0W3;

    sget-wide v4, LX/0X5;->jX:J

    invoke-interface {v2, v4, v5}, LX/0W4;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2723668
    new-instance v2, Landroid/preference/Preference;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2723669
    const v3, 0x7f030d5c

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2723670
    const v3, 0x7f080574

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(I)V

    .line 2723671
    iget-object v3, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a:LX/Jgk;

    invoke-virtual {v3}, LX/Jgk;->d()LX/Jgj;

    move-result-object v3

    iget v3, v3, LX/Jgj;->optionStringId:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080578

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2723672
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2723673
    invoke-direct {p0, v2}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Landroid/preference/Preference;)V

    .line 2723674
    new-instance v0, LX/Jgd;

    invoke-direct {v0, p0}, LX/Jgd;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;)V

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2723675
    iget-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2723676
    invoke-virtual {v1}, LX/4ok;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    .line 2723677
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->b:LX/0W3;

    sget-wide v2, LX/0X5;->kj:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2723678
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2723679
    const v2, 0x7f080579

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2723680
    iget-object v2, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2723681
    new-instance v2, Landroid/preference/Preference;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2723682
    const v3, 0x7f030d5c

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2723683
    const v3, 0x7f08057a

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(I)V

    .line 2723684
    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Landroid/preference/Preference;Landroid/preference/PreferenceCategory;Landroid/preference/Preference;)V

    .line 2723685
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->b:LX/0W3;

    sget-wide v2, LX/0X5;->kk:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    if-lez v0, :cond_2

    .line 2723686
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2723687
    const v1, 0x7f030d5c

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2723688
    const v1, 0x7f080580

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 2723689
    new-instance v1, LX/Jge;

    invoke-direct {v1, p0}, LX/Jge;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2723690
    iget-object v1, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2723691
    :cond_2
    return-void

    :cond_3
    const/4 v3, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2723646
    invoke-super {p0, p1}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a(Landroid/os/Bundle;)V

    .line 2723647
    const-class v0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2723648
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    move-object v0, v0

    .line 2723649
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->e:Landroid/preference/PreferenceScreen;

    .line 2723650
    iget-object v0, p0, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a(Landroid/preference/PreferenceScreen;)V

    .line 2723651
    invoke-direct {p0}, Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;->c()V

    .line 2723652
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x64228af5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2723638
    const v1, 0x7f030d23

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x471d71ef

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2723639
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2723640
    const v0, 0x7f0d20bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a0225

    .line 2723641
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p1

    invoke-static {v0, p1}, LX/4oZ;->a(Landroid/view/View;I)V

    .line 2723642
    const v0, 0x7f0d20bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 2723643
    const v1, 0x7f080571

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 2723644
    new-instance v1, LX/Jgb;

    invoke-direct {v1, p0}, LX/Jgb;-><init>(Lcom/facebook/messaging/composer/mbar/prefs/OmniMPreferencesFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2723645
    return-void
.end method
