.class public final Lcom/facebook/messaging/sync/service/MessagesSyncServiceHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JrW;


# direct methods
.method public constructor <init>(LX/JrW;)V
    .locals 0

    .prologue
    .line 2743501
    iput-object p1, p0, Lcom/facebook/messaging/sync/service/MessagesSyncServiceHandler$1;->a:LX/JrW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 2743502
    iget-object v0, p0, Lcom/facebook/messaging/sync/service/MessagesSyncServiceHandler$1;->a:LX/JrW;

    iget-object v0, v0, LX/JrW;->k:LX/Jrg;

    const/4 v4, 0x0

    .line 2743503
    :try_start_0
    new-instance v1, LX/Jrh;

    invoke-direct {v1}, LX/Jrh;-><init>()V

    move-object v1, v1

    .line 2743504
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2743505
    iget-object v1, v0, LX/Jrg;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    const v2, -0x71b3529b

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2743506
    iget-object v2, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v2

    .line 2743507
    check-cast v1, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel;

    .line 2743508
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2743509
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 2743510
    iget-object v2, v0, LX/Jrg;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6cy;

    sget-object v3, LX/6cx;->l:LX/2bA;

    const-wide/16 v7, -0x1

    invoke-virtual {v2, v3, v7, v8}, LX/48u;->a(LX/0To;J)J

    move-result-wide v7

    .line 2743511
    cmp-long v2, v5, v7

    if-eqz v2, :cond_1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2743512
    :cond_0
    :goto_0
    return-void

    .line 2743513
    :cond_1
    :try_start_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2743514
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_1
    if-ge v5, v8, :cond_5

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;

    .line 2743515
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2743516
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v3, v4

    :goto_2
    if-ge v3, v10, :cond_4

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel$MessagesNodesModel;

    .line 2743517
    invoke-virtual {v2}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel$MessagesNodesModel;->a()Ljava/lang/String;

    move-result-object v11

    .line 2743518
    iget-object v2, v0, LX/Jrg;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2N4;

    invoke-virtual {v2, v11}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2743519
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2743520
    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v12, "OTHER_USER-"

    invoke-direct {v2, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2743521
    :goto_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, ":"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2743522
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2743523
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v12, "GROUP-"

    invoke-direct {v2, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->j()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2743524
    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_1

    .line 2743525
    :cond_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2743526
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "android_messenger_sync_missing_msgs"

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2743527
    const-string v1, "db_missing_messages"

    invoke-virtual {v2, v1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2743528
    iget-object v1, v0, LX/Jrg;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 2743529
    :catch_0
    move-exception v1

    .line 2743530
    const-string v2, "ThreadStateDebugUtil"

    const-string v3, "Failed to compare server data to local db."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method
