.class public final Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/JqR;


# direct methods
.method public constructor <init>(LX/JqR;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2738884
    iput-object p1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;->b:LX/JqR;

    iput-object p2, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2738885
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;->b:LX/JqR;

    iget-object v0, v0, LX/JqR;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2738886
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a:LX/6Po;

    sget-object v3, LX/FDy;->a:LX/6Pr;

    const-string v4, "fetchThreadList - Message Requests"

    invoke-virtual {v2, v3, v4}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2738887
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v2

    sget-object v3, LX/6ek;->PENDING:LX/6ek;

    .line 2738888
    iput-object v3, v2, LX/6iI;->b:LX/6ek;

    .line 2738889
    move-object v2, v2

    .line 2738890
    invoke-virtual {v2}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v3

    .line 2738891
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v3

    .line 2738892
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    invoke-virtual {v2, v3}, LX/FDs;->b(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2738893
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2738894
    :goto_0
    return-void

    .line 2738895
    :catch_0
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$2;->b:LX/JqR;

    iget-object v0, v0, LX/JqR;->j:LX/03V;

    const-string v1, "messages_sync_message_requests_fetch_error"

    const-string v2, "Message requests fetch failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
