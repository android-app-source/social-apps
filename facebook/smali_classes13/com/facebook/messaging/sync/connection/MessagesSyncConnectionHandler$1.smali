.class public final Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/JqR;


# direct methods
.method public constructor <init>(LX/JqR;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2738872
    iput-object p1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;->b:LX/JqR;

    iput-object p2, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2738873
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;->b:LX/JqR;

    iget-object v0, v0, LX/JqR;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2738874
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v2

    sget-object v3, LX/6ek;->MONTAGE:LX/6ek;

    .line 2738875
    iput-object v3, v2, LX/6iI;->b:LX/6ek;

    .line 2738876
    move-object v2, v2

    .line 2738877
    invoke-virtual {v2}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v3

    .line 2738878
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v3

    .line 2738879
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    invoke-virtual {v2, v3}, LX/FDs;->b(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2738880
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2738881
    iget-object v2, v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Ow;

    invoke-virtual {v2}, LX/2Ow;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2738882
    :goto_0
    return-void

    .line 2738883
    :catch_0
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncConnectionHandler$1;->b:LX/JqR;

    iget-object v0, v0, LX/JqR;->j:LX/03V;

    const-string v1, "messages_sync_montage_fetch_error"

    const-string v2, "Montage threads fetch failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
