.class public Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final q:Ljava/lang/Object;


# instance fields
.field public a:LX/6Po;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FDs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ow;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Jqb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/CLl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iuh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6fD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739218
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->q:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739220
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739221
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->c:LX/0Ot;

    .line 2739222
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739223
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->d:LX/0Ot;

    .line 2739224
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739225
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->e:LX/0Ot;

    .line 2739226
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739227
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->i:LX/0Ot;

    .line 2739228
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739229
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    .line 2739230
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739231
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->k:LX/0Ot;

    .line 2739232
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739233
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->l:LX/0Ot;

    .line 2739234
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739235
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->m:LX/0Ot;

    .line 2739236
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739237
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    .line 2739238
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739239
    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->o:LX/0Ot;

    .line 2739240
    return-void
.end method

.method private static a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;Lcom/facebook/messaging/service/model/FetchThreadResult;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 14
    .param p0    # Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2739241
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2739242
    :cond_0
    :goto_0
    return-object p1

    .line 2739243
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2739244
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2739245
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v7, v1

    .line 2739246
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    move v2, v4

    :goto_1
    if-ge v5, v8, :cond_6

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2739247
    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->p:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v9}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2739248
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v2

    .line 2739249
    :goto_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v2, v0

    goto :goto_1

    .line 2739250
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Og;

    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v10, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, LX/2Og;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2739251
    if-nez v1, :cond_3

    .line 2739252
    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2N4;

    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v9}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2739253
    :cond_3
    if-eqz v1, :cond_4

    iget-wide v10, v1, Lcom/facebook/messaging/model/messages/Message;->d:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gtz v9, :cond_5

    .line 2739254
    :cond_4
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v2

    .line 2739255
    goto :goto_2

    .line 2739256
    :cond_5
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    iget-wide v10, v1, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-virtual {v0, v10, v11}, LX/6f7;->b(J)LX/6f7;

    move-result-object v0

    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2739257
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v3

    goto :goto_2

    .line 2739258
    :cond_6
    if-eqz v2, :cond_0

    .line 2739259
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2739260
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2739261
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v1, v2

    .line 2739262
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ne v1, v2, :cond_7

    :goto_3
    invoke-static {v3}, LX/0Tp;->a(Z)V

    .line 2739263
    new-instance v1, Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2739264
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v3

    .line 2739265
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2739266
    iget-boolean v4, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v3, v4

    .line 2739267
    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 2739268
    invoke-static {p1}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;

    move-result-object v0

    .line 2739269
    iput-object v1, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2739270
    move-object v0, v0

    .line 2739271
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object p1

    goto/16 :goto_0

    :cond_7
    move v3, v4

    .line 2739272
    goto :goto_3
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    .locals 7

    .prologue
    .line 2739273
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2739274
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2739275
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2739276
    if-nez v1, :cond_0

    .line 2739277
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739278
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2739279
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2739280
    sget-object v1, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->q:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2739281
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2739282
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2739283
    :cond_1
    if-nez v1, :cond_4

    .line 2739284
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2739285
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2739286
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b(LX/0QB;)Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2739287
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2739288
    if-nez v1, :cond_2

    .line 2739289
    sget-object v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->q:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2739290
    :goto_1
    if-eqz v0, :cond_3

    .line 2739291
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739292
    :goto_3
    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2739293
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2739294
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2739295
    :catchall_1
    move-exception v0

    .line 2739296
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739297
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2739298
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2739299
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->q:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;LX/6Po;LX/FDs;LX/0Ot;LX/0Ot;LX/0Ot;LX/Jqb;LX/0SG;LX/CLl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;",
            "LX/6Po;",
            "LX/FDs;",
            "LX/0Ot",
            "<",
            "LX/Jqf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ow;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Di5;",
            ">;",
            "LX/Jqb;",
            "LX/0SG;",
            "Lcom/facebook/messaging/deliveryreceipt/SendDeliveryReceiptManager;",
            "LX/0Ot",
            "<",
            "LX/Iuh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Og;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6fD;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2739300
    iput-object p1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a:LX/6Po;

    iput-object p2, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    iput-object p3, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->f:LX/Jqb;

    iput-object p7, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->g:LX/0SG;

    iput-object p8, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->h:LX/CLl;

    iput-object p9, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->i:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->l:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->p:LX/0Or;

    return-void
.end method

.method public static a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 12

    .prologue
    .line 2739301
    invoke-static {p0, p2}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;Lcom/facebook/messaging/service/model/FetchThreadResult;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2739302
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v1, :cond_1

    .line 2739303
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    const/4 v11, 0x0

    .line 2739304
    iget-object v4, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2N4;

    const-wide/16 v6, -0x1

    const v8, 0x7fffffff

    const/4 v5, 0x2

    new-array v9, v5, [LX/2uW;

    sget-object v5, LX/2uW;->PENDING_SEND:LX/2uW;

    aput-object v5, v9, v11

    const/4 v5, 0x1

    sget-object v10, LX/2uW;->FAILED_SEND:LX/2uW;

    aput-object v10, v9, v5

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI[LX/2uW;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->reverse()LX/0Px;

    move-result-object v4

    .line 2739305
    new-instance v5, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-direct {v5, p1, v4, v11}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 2739306
    iget-object v4, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->o:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6fD;

    invoke-virtual {v4, v1, v5}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v4

    move-object v1, v4

    .line 2739307
    invoke-static {v0}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;

    move-result-object v0

    .line 2739308
    iput-object v1, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2739309
    move-object v0, v0

    .line 2739310
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    move-object v1, v0

    .line 2739311
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    invoke-virtual {v0, p1}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2739312
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    .line 2739313
    iget-object v3, v0, LX/2Oe;->a:LX/2OQ;

    .line 2739314
    invoke-static {v3, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2739315
    if-nez p1, :cond_2

    .line 2739316
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2739317
    :goto_1
    invoke-virtual {v3, v2, v0}, LX/2OQ;->a(LX/6ek;LX/0Px;)V

    .line 2739318
    if-nez v1, :cond_0

    .line 2739319
    :goto_2
    return-void

    .line 2739320
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    sget-object v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    invoke-virtual {v0, v2, v1}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2739321
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    const/16 v2, 0x14

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, LX/2Oe;->a(ILcom/facebook/messaging/service/model/FetchThreadResult;Z)V

    .line 2739322
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->h:LX/CLl;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "FETCH_THREAD"

    invoke-virtual {v0, v1, v2}, LX/CLl;->a(LX/0Px;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    move-object v1, v0

    goto :goto_0

    .line 2739323
    :cond_2
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;Lcom/facebook/messaging/service/model/FetchMessageResult;J)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2739324
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 2739325
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v0

    .line 2739326
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2739327
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->b:LX/FDs;

    invoke-virtual {v0, v1, p2, p3}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v1

    .line 2739328
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqf;

    invoke-virtual {v0, v1, v4, p2, p3}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;LX/6k4;J)V

    .line 2739329
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;
    .locals 19

    .prologue
    .line 2739330
    new-instance v2, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    invoke-direct {v2}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;-><init>()V

    .line 2739331
    invoke-static/range {p0 .. p0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v3

    check-cast v3, LX/6Po;

    invoke-static/range {p0 .. p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v4

    check-cast v4, LX/FDs;

    const/16 v5, 0x29c9

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xce9

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2847

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v8

    check-cast v8, LX/Jqb;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/CLl;->a(LX/0QB;)LX/CLl;

    move-result-object v10

    check-cast v10, LX/CLl;

    const/16 v11, 0x2a60

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xce5

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xce8

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0xd18

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xac0

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x2a01

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x281d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x15e8

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {v2 .. v18}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;LX/6Po;LX/FDs;LX/0Ot;LX/0Ot;LX/0Ot;LX/Jqb;LX/0SG;LX/CLl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 2739332
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/util/Set;Ljava/util/Set;J)LX/JqV;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMessageParams;",
            ">;J)",
            "LX/JqV;"
        }
    .end annotation

    .prologue
    .line 2739333
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "fetchThreadsAndMessages"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 2739334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2739335
    const-string v1, "fetch (sync); "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2739336
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2739337
    const-string v1, "threads "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2739338
    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2739339
    const-string v1, "messages "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2739340
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2739341
    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a:LX/6Po;

    sget-object v2, LX/FDy;->a:LX/6Pr;

    invoke-virtual {v1, v2, v0}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2739342
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v4

    .line 2739343
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 2739344
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2739345
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    const/16 v1, 0x14

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v3, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;ILcom/facebook/common/callercontext/CallerContext;Z)LX/0P1;

    move-result-object v6

    .line 2739346
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739347
    invoke-virtual {v6, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2739348
    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2739349
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-nez v2, :cond_3

    .line 2739350
    :cond_2
    invoke-virtual {v4, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2739351
    :cond_3
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v5, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2739352
    iget-object v2, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Iuh;

    invoke-virtual {v2, v1}, LX/Iuh;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2739353
    iget-object v1, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->f:LX/Jqb;

    .line 2739354
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2739355
    goto :goto_0

    .line 2739356
    :cond_4
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2739357
    iget-object v0, p0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v0, p2, v3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)LX/0P1;

    move-result-object v1

    .line 2739358
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2739359
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageResult;

    .line 2739360
    if-eqz v0, :cond_5

    .line 2739361
    invoke-static {p0, v0, p3, p4}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;Lcom/facebook/messaging/service/model/FetchMessageResult;J)V

    goto :goto_1

    .line 2739362
    :cond_6
    new-instance v0, LX/JqV;

    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/JqV;-><init>(LX/0P1;LX/0Rf;)V

    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2739363
    return-object v0

    .line 2739364
    :catch_0
    move-exception v0

    .line 2739365
    const-string v1, "MessagesSyncThreadsFetcher"

    const-string v2, "Failed graphql query fetch: "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2739366
    throw v0
.end method
