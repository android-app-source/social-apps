.class public final Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2739467
    const-class v0, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel;

    new-instance v1, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2739468
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2739469
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2739426
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2739427
    const/4 v2, 0x0

    .line 2739428
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2739429
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2739430
    :goto_0
    move v1, v2

    .line 2739431
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2739432
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2739433
    new-instance v1, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/sync/connection/graphql/InitialThreadListDataQueryModels$InitialThreadListDataQueryModel;-><init>()V

    .line 2739434
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2739435
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2739436
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2739437
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2739438
    :cond_0
    return-object v1

    .line 2739439
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2739440
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2739441
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2739442
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2739443
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2739444
    const-string v4, "message_threads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2739445
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2739446
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_a

    .line 2739447
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2739448
    :goto_2
    move v1, v3

    .line 2739449
    goto :goto_1

    .line 2739450
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2739451
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2739452
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2739453
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_8

    .line 2739454
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2739455
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2739456
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v7, :cond_5

    .line 2739457
    const-string p0, "count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2739458
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v4

    goto :goto_3

    .line 2739459
    :cond_6
    const-string p0, "sync_sequence_id"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2739460
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 2739461
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2739462
    :cond_8
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2739463
    if-eqz v1, :cond_9

    .line 2739464
    invoke-virtual {v0, v3, v6, v3}, LX/186;->a(III)V

    .line 2739465
    :cond_9
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 2739466
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v1, v3

    move v5, v3

    move v6, v3

    goto :goto_3
.end method
