.class public Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;


# instance fields
.field public final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2740071
    new-instance v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    .line 2740072
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 2740073
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2740074
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 2740075
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;-><init>(LX/0P1;LX/0Px;LX/0Rf;)V

    sput-object v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->a:Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    .line 2740076
    new-instance v0, LX/Jqg;

    invoke-direct {v0}, LX/Jqg;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0P1;LX/0Px;LX/0Rf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2740067
    iput-object p1, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->b:LX/0P1;

    .line 2740068
    iput-object p2, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    .line 2740069
    iput-object p3, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->d:LX/0Rf;

    .line 2740070
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2740077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2740078
    const-class v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->b:LX/0P1;

    .line 2740079
    const-class v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    .line 2740080
    const-class v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->d:LX/0Rf;

    .line 2740081
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1

    .prologue
    .line 2740065
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2740064
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2740063
    const-class v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "threadSummariesByThreadKey"

    iget-object v2, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->b:LX/0P1;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "threadsFetchedFromServer"

    iget-object v2, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2740059
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->b:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 2740060
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2740061
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2740062
    return-void
.end method
