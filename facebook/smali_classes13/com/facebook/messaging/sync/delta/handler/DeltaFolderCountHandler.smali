.class public Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;
.super LX/Jqi;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740784
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2740785
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2740786
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740787
    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->a:LX/0Ot;

    .line 2740788
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740789
    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->b:LX/0Ot;

    .line 2740790
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740791
    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->c:LX/0Ot;

    .line 2740792
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740793
    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->d:LX/0Ot;

    .line 2740794
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;
    .locals 10

    .prologue
    .line 2740795
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740796
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740797
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740798
    if-nez v1, :cond_0

    .line 2740799
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740800
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740801
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740802
    sget-object v1, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740803
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740804
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740805
    :cond_1
    if-nez v1, :cond_4

    .line 2740806
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740807
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740808
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2740809
    new-instance v1, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;

    invoke-direct {v1}, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;-><init>()V

    .line 2740810
    const/16 v7, 0x542

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2744

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0x1ce

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2740811
    iput-object v7, v1, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->a:LX/0Ot;

    iput-object v8, v1, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->b:LX/0Ot;

    iput-object v9, v1, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->c:LX/0Ot;

    iput-object p0, v1, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->d:LX/0Ot;

    .line 2740812
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2740813
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740814
    if-nez v1, :cond_2

    .line 2740815
    sget-object v0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740816
    :goto_1
    if-eqz v0, :cond_3

    .line 2740817
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740818
    :goto_3
    check-cast v0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740819
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740820
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740821
    :catchall_1
    move-exception v0

    .line 2740822
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740823
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740824
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740825
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/6jn;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2740826
    iget-object v2, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 2740827
    iget-object v2, p0, LX/6jn;->counts:Ljava/util/Map;

    if-eqz v2, :cond_1

    .line 2740828
    iget-object v2, p0, LX/6jn;->counts:Ljava/util/Map;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/6jn;->counts:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 2740829
    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740830
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740831
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740832
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->x()LX/6jn;

    move-result-object v0

    .line 2740833
    invoke-static {v0}, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->a(LX/6jn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2740834
    sget-object v0, LX/6ek;->CONVERSATION_REQUEST_FOLDERS:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ek;

    .line 2740835
    iget-object v1, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6cy;

    invoke-static {v0}, LX/6cx;->c(LX/6ek;)LX/2bA;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, LX/48u;->b(LX/0To;Z)V

    goto :goto_0

    .line 2740836
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740837
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->x()LX/6jn;

    move-result-object v2

    .line 2740838
    invoke-static {v2}, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->a(LX/6jn;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2740839
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const-string v1, "com.messaging.sync.delta.handler.DeltaFolderCountHandler.pending_folder_count_change"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 2740840
    sget-object v0, LX/6ek;->CONVERSATION_REQUEST_FOLDERS:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ek;

    .line 2740841
    iget-object v1, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2740842
    iget-object p1, v1, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {p1, v0}, LX/2OQ;->c(LX/6ek;)V

    .line 2740843
    goto :goto_0

    .line 2740844
    :cond_0
    iget-object v4, v2, LX/6jn;->counts:Ljava/util/Map;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6l4;

    iget-object v4, v4, LX/6l4;->count:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2740845
    iget-object v4, v2, LX/6jn;->counts:Ljava/util/Map;

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6l4;

    iget-object v4, v4, LX/6l4;->count:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2740846
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2740847
    const-string v7, "updateFolderCountsParams"

    new-instance v8, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;

    sget-object v9, LX/6ek;->PENDING:LX/6ek;

    invoke-direct {v8, v9, v5, v4}, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;-><init>(LX/6ek;II)V

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2740848
    iget-object v4, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0aG;

    const-string v5, "update_folder_counts"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v8, Lcom/facebook/messaging/sync/delta/handler/DeltaFolderCountHandler;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x29f4678b

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 2740849
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740850
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740851
    return-object v0
.end method
