.class public final Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7GJ;

.field public final synthetic b:LX/Jqz;


# direct methods
.method public constructor <init>(LX/Jqz;LX/7GJ;)V
    .locals 0

    .prologue
    .line 2741157
    iput-object p1, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;->b:LX/Jqz;

    iput-object p2, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;->a:LX/7GJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 2741158
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;->b:LX/Jqz;

    iget-object v1, p0, Lcom/facebook/messaging/sync/delta/handler/DeltaLiveLocationDataHandler$1;->a:LX/7GJ;

    .line 2741159
    iget-object v2, v1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v2, LX/6kU;

    invoke-virtual {v2}, LX/6kU;->d()LX/6jt;

    move-result-object v2

    .line 2741160
    iget-object v3, v0, LX/Jqz;->m:LX/Jrc;

    iget-object v4, v2, LX/6jt;->threadKey:LX/6l9;

    invoke-virtual {v3, v4}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    .line 2741161
    iget-object v2, v2, LX/6jt;->messageLiveLocations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6kl;

    .line 2741162
    iget-object v5, v2, LX/6kl;->senderId:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v5

    .line 2741163
    iget-object v6, v0, LX/Jqz;->i:LX/Ifw;

    invoke-virtual {v6, v5}, LX/Ifv;->a(Lcom/facebook/user/model/UserKey;)LX/Ig7;

    move-result-object v6

    .line 2741164
    iget-object v7, v0, LX/Jqz;->i:LX/Ifw;

    invoke-virtual {v7, v5, v3}, LX/Ifv;->a(Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Ig6;

    move-result-object v7

    .line 2741165
    iget-object v8, v0, LX/Jqz;->e:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v8, v5}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2741166
    iget-object v5, v2, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    invoke-virtual {v7, v5}, LX/Ig6;->a(Ljava/lang/String;)V

    .line 2741167
    iget-object v5, v2, LX/6kl;->id:Ljava/lang/Long;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/Ig6;->b(Ljava/lang/String;)V

    .line 2741168
    iget-object v5, v2, LX/6kl;->expirationTime:Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 2741169
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, v2, LX/6kl;->expirationTime:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v5, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, LX/Ig6;->a(J)V

    .line 2741170
    :cond_0
    iget-object v5, v2, LX/6kl;->coordinate:LX/6km;

    if-eqz v5, :cond_1

    .line 2741171
    new-instance v5, Landroid/location/Location;

    const-string v7, ""

    invoke-direct {v5, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2741172
    iget-object v7, v2, LX/6kl;->coordinate:LX/6km;

    iget-object v7, v7, LX/6km;->latitude:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, LX/FFr;->b(J)D

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Landroid/location/Location;->setLatitude(D)V

    .line 2741173
    iget-object v7, v2, LX/6kl;->coordinate:LX/6km;

    iget-object v7, v7, LX/6km;->longitude:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, LX/FFr;->b(J)D

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Landroid/location/Location;->setLongitude(D)V

    .line 2741174
    invoke-virtual {v6, v5}, LX/Ig7;->a(Landroid/location/Location;)V

    .line 2741175
    iget-object v5, v2, LX/6kl;->locationTitle:Ljava/lang/String;

    invoke-virtual {v6, v5}, LX/Ig7;->a(Ljava/lang/String;)V

    .line 2741176
    :cond_1
    :goto_1
    goto :goto_0

    .line 2741177
    :cond_2
    return-void

    .line 2741178
    :cond_3
    iget-object v5, v2, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    iget-object v6, v0, LX/Jqz;->i:LX/Ifw;

    .line 2741179
    iget-object v8, v6, LX/Ifv;->l:Ljava/lang/String;

    move-object v6, v8

    .line 2741180
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2741181
    iget-object v5, v0, LX/Jqz;->i:LX/Ifw;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/Ifv;->a(Ljava/lang/String;)V

    .line 2741182
    iget-object v5, v0, LX/Jqz;->j:LX/Ig9;

    iget-object v6, v2, LX/6kl;->id:Ljava/lang/Long;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2741183
    new-instance v12, LX/4H4;

    invoke-direct {v12}, LX/4H4;-><init>()V

    iget-object v11, v5, LX/Ig9;->c:LX/0Or;

    invoke-interface {v11}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v11}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v11

    .line 2741184
    const-string v13, "actor_id"

    invoke-virtual {v12, v13, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741185
    move-object v11, v12

    .line 2741186
    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v13, v5, LX/Ig9;->b:LX/0SG;

    invoke-interface {v13}, LX/0SG;->a()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v13

    long-to-int v12, v13

    add-int/lit8 v12, v12, -0x1e

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 2741187
    const-string v13, "expiration_time"

    invoke-virtual {v11, v13, v12}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2741188
    move-object v11, v11

    .line 2741189
    const-string v12, "message_live_location_id"

    invoke-virtual {v11, v12, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741190
    move-object v11, v11

    .line 2741191
    new-instance v12, LX/IgA;

    invoke-direct {v12}, LX/IgA;-><init>()V

    move-object v12, v12

    .line 2741192
    const-string v13, "input"

    invoke-virtual {v12, v13, v11}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2741193
    invoke-static {v12}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v11

    .line 2741194
    iget-object v12, v5, LX/Ig9;->f:LX/0tX;

    invoke-virtual {v12, v11}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2741195
    goto :goto_1

    .line 2741196
    :cond_4
    iget-object v5, v2, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    .line 2741197
    iget-object v6, v7, LX/Ig6;->h:Ljava/lang/String;

    move-object v6, v6

    .line 2741198
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2741199
    iget-object v5, v2, LX/6kl;->id:Ljava/lang/Long;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/Ig6;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
