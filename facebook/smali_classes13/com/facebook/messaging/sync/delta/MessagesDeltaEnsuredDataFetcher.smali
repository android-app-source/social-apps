.class public Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GH;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7GH",
        "<",
        "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field public final a:LX/FDI;

.field public final b:LX/2N4;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/Jqc;

.field private final e:LX/7G0;

.field private final f:LX/FO4;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JqS;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739604
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDI;LX/2N4;LX/0Ot;LX/Jqc;LX/7G0;LX/FO4;LX/0Ot;)V
    .locals 1
    .param p1    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDI;",
            "LX/2N4;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;",
            ">;",
            "LX/Jqc;",
            "LX/7G0;",
            "LX/FO4;",
            "LX/0Ot",
            "<",
            "LX/JqS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739606
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739607
    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->h:LX/0Ot;

    .line 2739608
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2739609
    iput-object v0, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->i:LX/0Ot;

    .line 2739610
    iput-object p1, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->a:LX/FDI;

    .line 2739611
    iput-object p2, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->b:LX/2N4;

    .line 2739612
    iput-object p3, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->c:LX/0Ot;

    .line 2739613
    iput-object p4, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->d:LX/Jqc;

    .line 2739614
    iput-object p5, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->e:LX/7G0;

    .line 2739615
    iput-object p6, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->f:LX/FO4;

    .line 2739616
    iput-object p7, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->g:LX/0Ot;

    .line 2739617
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;
    .locals 15

    .prologue
    .line 2739618
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2739619
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2739620
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2739621
    if-nez v1, :cond_0

    .line 2739622
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739623
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2739624
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2739625
    sget-object v1, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2739626
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2739627
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2739628
    :cond_1
    if-nez v1, :cond_4

    .line 2739629
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2739630
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2739631
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2739632
    new-instance v7, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;

    invoke-static {v0}, LX/FDK;->a(LX/0QB;)LX/FDI;

    move-result-object v8

    check-cast v8, LX/FDI;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v9

    check-cast v9, LX/2N4;

    const/16 v10, 0x29c3

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/Jqc;->a(LX/0QB;)LX/Jqc;

    move-result-object v11

    check-cast v11, LX/Jqc;

    invoke-static {v0}, LX/7G0;->b(LX/0QB;)LX/7G0;

    move-result-object v12

    check-cast v12, LX/7G0;

    invoke-static {v0}, LX/FO4;->a(LX/0QB;)LX/FO4;

    move-result-object v13

    check-cast v13, LX/FO4;

    const/16 v14, 0x29c1

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v7 .. v14}, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;-><init>(LX/FDI;LX/2N4;LX/0Ot;LX/Jqc;LX/7G0;LX/FO4;LX/0Ot;)V

    .line 2739633
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xac0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 2739634
    iput-object v8, v7, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->h:LX/0Ot;

    iput-object v9, v7, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->i:LX/0Ot;

    .line 2739635
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2739636
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2739637
    if-nez v1, :cond_2

    .line 2739638
    sget-object v0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2739639
    :goto_1
    if-eqz v0, :cond_3

    .line 2739640
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739641
    :goto_3
    check-cast v0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2739642
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2739643
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2739644
    :catchall_1
    move-exception v0

    .line 2739645
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739646
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2739647
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2739648
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/0Rf;Ljava/util/Map;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2739649
    invoke-virtual {p1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739650
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2739651
    const/16 p1, 0x14

    const/4 v3, 0x0

    .line 2739652
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v4

    .line 2739653
    iget-object v2, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->a:LX/FDI;

    invoke-virtual {v2, v4}, LX/FDI;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 2739654
    if-eqz v2, :cond_3

    .line 2739655
    :goto_1
    move-object v2, v2

    .line 2739656
    if-eqz v2, :cond_1

    .line 2739657
    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2739658
    :cond_1
    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2739659
    :cond_2
    return-void

    .line 2739660
    :cond_3
    iget-object v2, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->b:LX/2N4;

    invoke-virtual {v2, v4, p1}, LX/2N4;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v4

    .line 2739661
    iget-object v2, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v2, v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v2, :cond_5

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    iget-object v5, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v5, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-static {v2, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2739662
    iget-object v2, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v5, 0x1cb

    invoke-virtual {v2, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    move v3, v2

    .line 2739663
    :cond_4
    iget-object v2, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, p1, v4, v3}, LX/2Oe;->a(ILcom/facebook/messaging/service/model/FetchThreadResult;Z)V

    .line 2739664
    iget-object v2, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    goto :goto_1

    .line 2739665
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2739666
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 2739667
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v5

    .line 2739668
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v3

    .line 2739669
    const/4 v0, 0x0

    .line 2739670
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GJ;

    .line 2739671
    iget-object v7, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->d:LX/Jqc;

    iget-object v1, v0, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v7, v1}, LX/Jqc;->a(LX/6kW;)LX/Jqi;

    move-result-object v7

    .line 2739672
    iget-object v1, v0, LX/7GJ;->a:Ljava/lang/Object;

    invoke-virtual {v7, v1}, LX/Jqi;->d(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2739673
    const/4 v1, 0x1

    .line 2739674
    :goto_1
    iget-object v2, v0, LX/7GJ;->a:Ljava/lang/Object;

    invoke-virtual {v7, v2}, LX/Jqi;->a(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 2739675
    iget-object v8, v0, LX/7GJ;->a:Ljava/lang/Object;

    invoke-virtual {v7, v8}, LX/Jqi;->e(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2739676
    iget-object v0, v0, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    .line 2739677
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v10

    .line 2739678
    invoke-virtual {v7, v0}, LX/Jqi;->c(Ljava/lang/Object;)LX/0P1;

    move-result-object v8

    invoke-virtual {v8}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v8

    invoke-virtual {v8}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 2739679
    new-instance v12, Lcom/facebook/messaging/service/model/FetchMessageParams;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v12, v9, v8}, Lcom/facebook/messaging/service/model/FetchMessageParams;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v10, v12}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2739680
    :cond_0
    move-object v0, v10

    .line 2739681
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 2739682
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2739683
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-direct {p0, v0, v4, v5}, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->a(LX/0Rf;Ljava/util/Map;Ljava/util/Set;)V

    goto :goto_3

    .line 2739684
    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    move v2, v1

    .line 2739685
    goto/16 :goto_0

    .line 2739686
    :cond_2
    invoke-direct {p0, v2, v4, v5}, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->a(LX/0Rf;Ljava/util/Map;Ljava/util/Set;)V

    move v2, v1

    .line 2739687
    goto/16 :goto_0

    .line 2739688
    :cond_3
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2739689
    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2739690
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2739691
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5, v0}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2739692
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 2739693
    :cond_5
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v1, v0

    .line 2739694
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2739695
    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 2739696
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GJ;

    iget-wide v6, v0, LX/7GJ;->b:J

    .line 2739697
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;

    invoke-virtual {v0, v5, v3, v6, v7}, Lcom/facebook/messaging/sync/connection/MessagesSyncThreadsFetcher;->a(Ljava/util/Set;Ljava/util/Set;J)LX/JqV;

    move-result-object v0

    .line 2739698
    iget-object v1, v0, LX/JqV;->a:LX/0P1;

    invoke-interface {v4, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2739699
    iget-object v1, v0, LX/JqV;->a:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v1

    .line 2739700
    iget-object v3, v0, LX/JqV;->b:LX/0Rf;

    .line 2739701
    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2739702
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->e:LX/7G0;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->size()I

    move-result v7

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    invoke-virtual {v3}, LX/0Rf;->size()I

    move-result v9

    .line 2739703
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v11, "sync_thread_prefetch"

    invoke-direct {v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2739704
    const-string v11, "num_deltas_in_batch"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2739705
    const-string v11, "num_threads_tried_to_fetch"

    invoke-virtual {v10, v11, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2739706
    const-string v11, "num_threads_fetched"

    invoke-virtual {v10, v11, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2739707
    const-string v11, "num_non_existent_threads"

    invoke-virtual {v10, v11, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2739708
    iget-object v11, v0, LX/7G0;->a:LX/7G1;

    sget-object v12, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v11, v10, v12}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 2739709
    :cond_7
    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2739710
    iget-object v6, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->f:LX/FO4;

    invoke-virtual {v6, v0}, LX/FO4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_5

    :cond_8
    move-object v3, v1

    move-object v1, v0

    .line 2739711
    :cond_9
    if-eqz v2, :cond_a

    .line 2739712
    iget-object v0, p0, Lcom/facebook/messaging/sync/delta/MessagesDeltaEnsuredDataFetcher;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqS;

    .line 2739713
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2739714
    iget-object v5, v0, LX/JqS;->a:LX/JqN;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2739715
    iget-object v5, v0, LX/JqS;->b:LX/28W;

    const-string v6, "syncRefetchLoggedInUser"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v2, v8}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 2739716
    :cond_a
    new-instance v0, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    invoke-static {v4}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    invoke-direct {v0, v2, v1, v3}, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;-><init>(LX/0P1;LX/0Px;LX/0Rf;)V

    return-object v0

    :cond_b
    move v1, v2

    goto/16 :goto_1
.end method
