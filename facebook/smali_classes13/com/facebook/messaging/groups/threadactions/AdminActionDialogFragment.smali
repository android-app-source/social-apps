.class public Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;
.super Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;
.source ""


# instance fields
.field public m:LX/1CX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

.field private p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private q:Lcom/facebook/user/model/UserKey;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2730046
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;)V
    .locals 6

    .prologue
    .line 2730047
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    if-eqz v0, :cond_0

    .line 2730048
    :goto_0
    return-void

    .line 2730049
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2730050
    iget-object v2, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->r:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2730051
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to process Operation Type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for AdminDialogFragment."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2730052
    :sswitch_0
    const-string v3, "add_admins_to_group"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "remove_admins_from_group"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "remove_member"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    .line 2730053
    :pswitch_0
    new-instance v0, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;

    iget-object v2, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->q:Lcom/facebook/user/model/UserKey;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v0, v2, v3}, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;-><init>(LX/0Px;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2730054
    sget-object v2, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2730055
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2730056
    const-string v2, "addAdminsOperation"

    invoke-static {v0, v2}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2730057
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v2, LX/JkY;

    invoke-direct {v2, p0}, LX/JkY;-><init>(Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;)V

    .line 2730058
    iput-object v2, v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 2730059
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v2, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->s:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->setOperationProgressIndicator(LX/4At;)V

    .line 2730060
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    iget-object v2, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->start(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2730061
    :pswitch_1
    new-instance v0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;

    iget-object v2, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->q:Lcom/facebook/user/model/UserKey;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v0, v2, v3}, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;-><init>(LX/0Px;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2730062
    sget-object v2, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2730063
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2730064
    const-string v2, "removeAdminsOperation"

    invoke-static {v0, v2}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    goto :goto_2

    .line 2730065
    :pswitch_2
    new-instance v2, Lcom/facebook/user/model/UserFbidIdentifier;

    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->q:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    .line 2730066
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iget-object v3, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->q:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v3}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2730067
    new-instance v3, Lcom/facebook/messaging/service/model/RemoveMemberParams;

    const/4 v4, 0x1

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lcom/facebook/messaging/service/model/RemoveMemberParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/util/List;)V

    move-object v0, v3

    .line 2730068
    :goto_3
    const-string v2, "removeMemberParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2730069
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2730070
    const-string v2, "removeMemberOperation"

    invoke-static {v0, v2}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->o:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    goto :goto_2

    .line 2730071
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2730072
    new-instance v3, Lcom/facebook/messaging/service/model/RemoveMemberParams;

    const/4 v4, 0x0

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lcom/facebook/messaging/service/model/RemoveMemberParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/util/List;)V

    move-object v0, v3

    .line 2730073
    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x453bb2b5 -> :sswitch_2
        0x50f53af8 -> :sswitch_0
        0x7091f6ca -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/31Y;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2730074
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v0

    .line 2730075
    const-string v0, "thread_key"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2730076
    const-string v0, "user_key"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->q:Lcom/facebook/user/model/UserKey;

    .line 2730077
    const-string v0, "title_text"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2730078
    const-string v0, "body_text"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2730079
    const-string v0, "confirm_button_text"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2730080
    const-string v0, "operation_type"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->r:Ljava/lang/String;

    .line 2730081
    const-string v0, "loading_text"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->s:Ljava/lang/String;

    .line 2730082
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2730083
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->q:Lcom/facebook/user/model/UserKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2730084
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2730085
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2730086
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2730087
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2730088
    iget-object v0, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->s:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2730089
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2730090
    invoke-virtual {v0, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/JkW;

    invoke-direct {v2, p0}, LX/JkW;-><init>(Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;)V

    invoke-virtual {v1, v6, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, LX/JkV;

    invoke-direct {v3, p0}, LX/JkV;-><init>(Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2730091
    return-object v0

    :cond_0
    move v0, v2

    .line 2730092
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2730093
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2730094
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2730095
    goto :goto_3

    :cond_4
    move v1, v2

    .line 2730096
    goto :goto_4
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x72db9f3d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2730097
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2730098
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;

    invoke-static {p1}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v4

    check-cast v4, LX/1CX;

    const/16 v1, 0x12cd

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v4, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->m:LX/1CX;

    iput-object p1, p0, Lcom/facebook/messaging/groups/threadactions/AdminActionDialogFragment;->n:LX/0Or;

    .line 2730099
    const/16 v1, 0x2b

    const v2, -0x14c0a640

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
