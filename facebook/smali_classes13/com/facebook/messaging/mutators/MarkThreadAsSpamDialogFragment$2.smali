.class public final Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;)V
    .locals 0

    .prologue
    .line 2735814
    iput-object p1, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment$2;->a:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12

    .prologue
    .line 2735815
    iget-object v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment$2;->a:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;

    .line 2735816
    iget-object v1, v0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DdP;

    iget-object v2, v0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    iget-object v2, v2, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->e:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2735817
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2735818
    new-instance v4, LX/6ie;

    invoke-direct {v4}, LX/6ie;-><init>()V

    sget-object v5, LX/6iW;->SPAM:LX/6iW;

    .line 2735819
    iput-object v5, v4, LX/6ie;->a:LX/6iW;

    .line 2735820
    move-object v5, v4

    .line 2735821
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2735822
    new-instance v7, LX/6ia;

    invoke-direct {v7}, LX/6ia;-><init>()V

    iget-object v8, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735823
    iput-object v8, v7, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735824
    move-object v7, v7

    .line 2735825
    const/4 v8, 0x1

    .line 2735826
    iput-boolean v8, v7, LX/6ia;->b:Z

    .line 2735827
    move-object v7, v7

    .line 2735828
    iget-wide v8, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    .line 2735829
    iput-wide v8, v7, LX/6ia;->c:J

    .line 2735830
    move-object v7, v7

    .line 2735831
    iget-wide v8, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    .line 2735832
    iput-wide v8, v7, LX/6ia;->d:J

    .line 2735833
    move-object v4, v7

    .line 2735834
    invoke-virtual {v4}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    goto :goto_0

    .line 2735835
    :cond_0
    invoke-virtual {v5}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v10

    .line 2735836
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2735837
    const-string v4, "markThreadsParams"

    invoke-virtual {v6, v4, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2735838
    iget-object v4, v1, LX/DdP;->b:LX/0aG;

    const-string v5, "mark_threads"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x25b63b44

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2735839
    new-instance v5, LX/DdO;

    invoke-direct {v5, v1, v10}, LX/DdO;-><init>(LX/DdP;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2735840
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2735841
    iget-object v6, v1, LX/DdP;->c:Ljava/util/Set;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v6, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2735842
    :cond_1
    return-void
.end method
