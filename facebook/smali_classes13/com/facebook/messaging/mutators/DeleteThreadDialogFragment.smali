.class public Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# static fields
.field private static final s:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public m:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/annotations/IsGlobalMessageDeleteEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/1MF;

.field public w:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/JoV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2735713
    const-class v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    sput-object v0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->s:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2735763
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2735764
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735765
    iput-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->t:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2735745
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2735746
    :goto_0
    return-void

    .line 2735747
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2735748
    new-instance v1, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    invoke-direct {v1, v2}, Lcom/facebook/messaging/service/model/DeleteThreadsParams;-><init>(Ljava/util/List;)V

    .line 2735749
    const-string v2, "deleteThreadsParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2735750
    iget-object v1, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->n:LX/0aG;

    const-string v2, "delete_threads"

    const v3, -0x1cca5ffd

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->v:LX/1MF;

    .line 2735751
    iget-object v1, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->v:LX/1MF;

    new-instance v2, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0802be

    :goto_1
    invoke-direct {v2, v3, v0}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-interface {v1, v2}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    .line 2735752
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->v:LX/1MF;

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2735753
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->m:LX/0Zb;

    const-string v1, "delete_thread"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2735754
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2735755
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2735756
    instance-of v0, v0, LX/0f2;

    if-eqz v0, :cond_1

    .line 2735757
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2735758
    check-cast v0, LX/0f2;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2735759
    :cond_1
    const-string v0, "thread_key"

    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2735760
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2735761
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/JoT;

    invoke-direct {v1, p0}, LX/JoT;-><init>(Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;)V

    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->q:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2735762
    :cond_3
    const v0, 0x7f0802ba

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1d8408f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2735714
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2735715
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735716
    const-string v2, "thread_keys"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    .line 2735717
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v4, p0

    check-cast v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {p1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v6

    check-cast v6, LX/0aG;

    const-class v7, Landroid/content/Context;

    invoke-interface {p1, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {p1}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v8

    check-cast v8, LX/1CX;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    const/16 v10, 0x14e1

    invoke-static {p1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v0, 0x2923

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v5, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->m:LX/0Zb;

    iput-object v6, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->n:LX/0aG;

    iput-object v7, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->o:Landroid/content/Context;

    iput-object v8, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->p:LX/1CX;

    iput-object v9, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->q:Ljava/util/concurrent/Executor;

    iput-object v10, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->r:LX/0Or;

    iput-object p1, v4, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->t:LX/0Ot;

    .line 2735718
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735719
    const-string v2, "dialog_title"

    .line 2735720
    iget-object v4, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->r:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f0802bb

    :goto_0
    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2735721
    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2735722
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2735723
    const-string v4, "dialog_message"

    .line 2735724
    iget-object v5, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->r:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    const v5, 0x7f0802bc

    :goto_1
    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2735725
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2735726
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2735727
    const-string v5, "confirm_text"

    const/4 v7, 0x0

    .line 2735728
    const/4 v8, 0x1

    .line 2735729
    iget-object v6, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    move v9, v7

    :goto_2
    if-ge v9, v10, :cond_0

    iget-object v6, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->u:LX/0Px;

    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735730
    invoke-static {v6}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v6

    if-nez v6, :cond_5

    move v6, v7

    .line 2735731
    :goto_3
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v6

    goto :goto_2

    .line 2735732
    :cond_0
    if-eqz v8, :cond_3

    .line 2735733
    const v6, 0x7f0802b9

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2735734
    :goto_4
    move-object v6, v6

    .line 2735735
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2735736
    new-instance v5, LX/6dy;

    invoke-direct {v5, v0, v4}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735737
    iput-object v2, v5, LX/6dy;->d:Ljava/lang/String;

    .line 2735738
    move-object v0, v5

    .line 2735739
    const v2, 0x7f080017

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2735740
    iput-object v2, v0, LX/6dy;->e:Ljava/lang/String;

    .line 2735741
    move-object v0, v0

    .line 2735742
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    move-object v0, v0

    .line 2735743
    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2735744
    const/16 v0, 0x2b

    const v2, -0x1bb05891

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const v4, 0x7f0802b6

    goto :goto_0

    :cond_2
    const v5, 0x7f0802b7

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/facebook/messaging/mutators/DeleteThreadDialogFragment;->r:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_4

    const v6, 0x7f0802bd

    :goto_5
    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_4
    const v6, 0x7f0802b8

    goto :goto_5

    :cond_5
    move v6, v8

    goto :goto_3
.end method
