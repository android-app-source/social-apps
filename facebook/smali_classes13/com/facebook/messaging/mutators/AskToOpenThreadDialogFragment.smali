.class public Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/JdU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jkb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/JkQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public q:LX/JoR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2735594
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2735595
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 2735596
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735597
    if-eqz v0, :cond_0

    .line 2735598
    const-string v1, "thread_summary"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->p:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2735599
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->p:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2735600
    iget-object v0, p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->p:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-boolean v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    if-nez v0, :cond_1

    .line 2735601
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2735602
    :cond_1
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2735603
    const v1, 0x7f082b2b    # 1.8099915E38f

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f082b2c    # 1.8099917E38f

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f082b33    # 1.8099931E38f

    new-instance v3, LX/JoQ;

    invoke-direct {v3, p0}, LX/JoQ;-><init>(Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f082b2d

    new-instance v3, LX/JoP;

    invoke-direct {v3, p0}, LX/JoP;-><init>(Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f082b2e    # 1.809992E38f

    new-instance v3, LX/JoO;

    invoke-direct {v3, p0}, LX/JoO;-><init>(Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    .line 2735604
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x571d5c6d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2735605
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2735606
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    check-cast p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    invoke-static {v5}, LX/JdU;->b(LX/0QB;)LX/JdU;

    move-result-object v4

    check-cast v4, LX/JdU;

    const/16 v6, 0x2799

    invoke-static {v5, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    new-instance v2, LX/JkQ;

    invoke-static {v5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    invoke-static {v5}, LX/3QW;->a(LX/0QB;)LX/3QW;

    move-result-object v1

    check-cast v1, LX/3QW;

    invoke-direct {v2, p1, v1}, LX/JkQ;-><init>(LX/0Zb;LX/3QW;)V

    move-object v5, v2

    check-cast v5, LX/JkQ;

    iput-object v4, p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->m:LX/JdU;

    iput-object v6, p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->n:LX/0Ot;

    iput-object v5, p0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->o:LX/JkQ;

    .line 2735607
    const/16 v1, 0x2b

    const v2, -0x6eb678f3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
