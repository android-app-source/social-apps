.class public Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# instance fields
.field public m:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/annotations/IsGlobalMessageDeleteEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/2Mk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

.field public q:LX/2EJ;

.field public r:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private t:Landroid/content/DialogInterface$OnShowListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Landroid/content/DialogInterface$OnDismissListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2735688
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2735689
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;

    invoke-static {v2}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v1

    check-cast v1, LX/1CX;

    const/16 p0, 0x14e1

    invoke-static {v2, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v2}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v2

    check-cast v2, LX/2Mk;

    iput-object v1, p1, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->m:LX/1CX;

    iput-object p0, p1, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->n:LX/0Or;

    iput-object v2, p1, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->o:LX/2Mk;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2735626
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2735627
    iget-object v1, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->t:Landroid/content/DialogInterface$OnShowListener;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 2735628
    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 2735679
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2735680
    iget-object v1, v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 2735681
    iget-object v0, v1, LX/4Al;->operationState:LX/4Ak;

    move-object v1, v0

    .line 2735682
    move-object v0, v1

    .line 2735683
    sget-object v1, LX/4Ak;->INIT:LX/4Ak;

    if-eq v0, v1, :cond_0

    .line 2735684
    :goto_0
    return-void

    .line 2735685
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2735686
    const-string v1, "deleteMessagesParams"

    new-instance v2, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    iget-object v3, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->r:LX/0Rf;

    sget-object v4, LX/6hi;->MUST_UPDATE_SERVER:LX/6hi;

    iget-object v5, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/messaging/service/model/DeleteMessagesParams;-><init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2735687
    iget-object v1, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    const-string v2, "delete_messages"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->start(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2735672
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    if-eqz v0, :cond_0

    .line 2735673
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->setOperationProgressIndicator(LX/4At;)V

    .line 2735674
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->q:LX/2EJ;

    if-eqz v0, :cond_1

    .line 2735675
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->q:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2735676
    iput-object v1, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->q:LX/2EJ;

    .line 2735677
    :cond_1
    invoke-super {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->d()V

    .line 2735678
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x267876b4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2735633
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2735634
    const-class v0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2735635
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735636
    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2735637
    new-instance v2, LX/0cA;

    invoke-direct {v2}, LX/0cA;-><init>()V

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v2

    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->r:LX/0Rf;

    .line 2735638
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735639
    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->r:LX/0Rf;

    invoke-virtual {v2}, LX/0Rf;->size()I

    move-result v2

    .line 2735640
    iget-object v3, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->o:LX/2Mk;

    invoke-virtual {v3, v0}, LX/2Mk;->s(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    .line 2735641
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2735642
    iget-object v3, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->n:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2735643
    if-eqz v0, :cond_1

    .line 2735644
    new-instance v3, LX/6dy;

    const v6, 0x7f0f0011

    invoke-virtual {v5, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    const p1, 0x7f0f0014

    invoke-virtual {v5, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, v6, p1}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v6, 0x7f0f0013

    invoke-virtual {v5, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    .line 2735645
    iput-object v5, v3, LX/6dy;->d:Ljava/lang/String;

    .line 2735646
    move-object v3, v3

    .line 2735647
    :goto_0
    sget-object v5, LX/6dz;->DELETE:LX/6dz;

    .line 2735648
    iput-object v5, v3, LX/6dy;->g:LX/6dz;

    .line 2735649
    invoke-virtual {v3}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v3

    move-object v2, v3

    .line 2735650
    iput-object v2, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2735651
    const/4 v5, 0x1

    .line 2735652
    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    if-nez v2, :cond_0

    .line 2735653
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 2735654
    invoke-virtual {v2}, LX/0gc;->c()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2735655
    :cond_0
    :goto_1
    const/16 v0, 0x2b

    const v2, -0x6b5671a7

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2735656
    :cond_1
    new-instance v3, LX/6dy;

    const v6, 0x7f0f0016

    invoke-virtual {v5, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    const p1, 0x7f0f0018

    invoke-virtual {v5, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, v6, p1}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v6, 0x7f0f0017

    invoke-virtual {v5, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    .line 2735657
    iput-object v5, v3, LX/6dy;->d:Ljava/lang/String;

    .line 2735658
    move-object v3, v3

    .line 2735659
    goto :goto_0

    .line 2735660
    :cond_2
    new-instance v3, LX/6dy;

    const v6, 0x7f0f0011

    invoke-virtual {v5, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    const p1, 0x7f0f0014

    invoke-virtual {v5, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, v6, p1}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v6, 0x7f0f0012

    invoke-virtual {v5, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    .line 2735661
    iput-object v5, v3, LX/6dy;->d:Ljava/lang/String;

    .line 2735662
    move-object v3, v3

    .line 2735663
    goto :goto_0

    .line 2735664
    :cond_3
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 2735665
    const-string v3, "deleteMessagesOperation"

    invoke-static {v2, v3}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2735666
    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v3, LX/JoS;

    invoke-direct {v3, p0}, LX/JoS;-><init>(Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;)V

    .line 2735667
    iput-object v3, v2, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 2735668
    if-nez v0, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->n:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2735669
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0015

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    .line 2735670
    :goto_2
    iget-object v3, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->p:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v5, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->setOperationProgressIndicator(LX/4At;)V

    goto/16 :goto_1

    .line 2735671
    :cond_5
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0019

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2735629
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2735630
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->u:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 2735631
    iget-object v0, p0, Lcom/facebook/messaging/mutators/DeleteMessagesDialogFragment;->u:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2735632
    :cond_0
    return-void
.end method
