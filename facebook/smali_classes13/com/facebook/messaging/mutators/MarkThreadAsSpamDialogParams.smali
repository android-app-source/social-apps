.class public Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Lcom/facebook/messaging/model/threads/ThreadSummary;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2735856
    new-instance v0, LX/Job;

    invoke-direct {v0}, LX/Job;-><init>()V

    sput-object v0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2735864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2735865
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->a:I

    .line 2735866
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->b:I

    .line 2735867
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->c:I

    .line 2735868
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->d:I

    .line 2735869
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->e:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2735870
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2735863
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2735857
    iget v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2735858
    iget v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2735859
    iget v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2735860
    iget v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2735861
    iget-object v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->e:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2735862
    return-void
.end method
