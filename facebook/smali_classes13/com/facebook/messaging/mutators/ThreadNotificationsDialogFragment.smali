.class public Lcom/facebook/messaging/mutators/ThreadNotificationsDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/FJW;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2735910
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2735911
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735912
    if-eqz v0, :cond_0

    .line 2735913
    const-string v1, "thread_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 2735914
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogFragment;->m:LX/FJW;

    invoke-interface {v0}, LX/FJW;->b()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x32d77373

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2735915
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2735916
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogFragment;

    invoke-static {v1}, LX/FJX;->a(LX/0QB;)LX/FJW;

    move-result-object v1

    check-cast v1, LX/FJW;

    iput-object v1, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogFragment;->m:LX/FJW;

    .line 2735917
    const/16 v1, 0x2b

    const v2, 0x48a16417

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
