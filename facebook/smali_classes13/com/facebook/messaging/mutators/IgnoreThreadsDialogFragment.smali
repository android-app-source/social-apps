.class public Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public m:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/JoZ;

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2735801
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2735802
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 2735803
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2735804
    const v1, 0x7f083bd9

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f083bda

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f082b33    # 1.8099931E38f

    new-instance v3, LX/JoW;

    invoke-direct {v3, p0}, LX/JoW;-><init>(Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080520

    new-instance v3, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment$1;-><init>(Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2735805
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e6e271b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2735806
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2735807
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735808
    const-string v2, "thread_keys"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->q:LX/0Px;

    .line 2735809
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object p1

    check-cast p1, LX/1CX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v4, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->m:LX/0aG;

    iput-object p1, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->n:LX/1CX;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->o:Ljava/util/concurrent/Executor;

    .line 2735810
    const/16 v0, 0x2b

    const v2, -0xb04a936

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
