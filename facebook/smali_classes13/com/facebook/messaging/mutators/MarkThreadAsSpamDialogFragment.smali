.class public Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DdP;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2735843
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2735844
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735845
    iput-object v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->m:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2735846
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2735847
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2735848
    const-string v1, "arg_dialog_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    .line 2735849
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    iget v1, v1, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->a:I

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    iget v1, v1, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->b:I

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    iget v1, v1, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->c:I

    new-instance v2, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment$2;-><init>(Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->n:Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;

    iget v1, v1, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogParams;->d:I

    new-instance v2, LX/Joa;

    invoke-direct {v2, p0}, LX/Joa;-><init>(Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6350955e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2735850
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2735851
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;

    const/16 v1, 0x26ee

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/messaging/mutators/MarkThreadAsSpamDialogFragment;->m:LX/0Ot;

    .line 2735852
    const/16 v1, 0x2b

    const v2, 0x486491c3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
