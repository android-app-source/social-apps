.class public Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/KA0;

.field public q:LX/Di5;

.field public r:LX/FJW;

.field private s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private t:LX/2EJ;

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2735908
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2735909
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->u:Z

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;

    new-instance v0, LX/KA0;

    invoke-direct {v0}, LX/KA0;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/KA0;

    invoke-static {v2}, LX/Di5;->a(LX/0QB;)LX/Di5;

    move-result-object v1

    check-cast v1, LX/Di5;

    invoke-static {v2}, LX/FJX;->a(LX/0QB;)LX/FJW;

    move-result-object v2

    check-cast v2, LX/FJW;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->p:LX/KA0;

    iput-object v1, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->q:LX/Di5;

    iput-object v2, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->r:LX/FJW;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2735895
    iget-boolean v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->u:Z

    if-nez v0, :cond_0

    .line 2735896
    iput-boolean v7, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->u:Z

    .line 2735897
    :goto_0
    return-void

    .line 2735898
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->r:LX/FJW;

    invoke-interface {v0}, LX/FJW;->c()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    .line 2735899
    sget-object v1, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    if-eq v0, v1, :cond_1

    .line 2735900
    sget-object v1, Lcom/facebook/messaging/model/threads/NotificationSetting;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    if-ne v0, v1, :cond_2

    .line 2735901
    const v0, 0x7f08042b

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2735902
    :goto_1
    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2735903
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->q:LX/Di5;

    iget-object v1, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/Di5;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2735904
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->finish()V

    goto :goto_0

    .line 2735905
    :cond_2
    new-instance v1, Ljava/util/Date;

    iget-wide v2, v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 2735906
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2735907
    const v1, 0x7f08042d

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2735874
    const-string v0, "thread_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735875
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->s:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2735876
    const-string v0, "voice_reply"

    .line 2735877
    invoke-static {p1}, LX/3qL;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    .line 2735878
    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 2735879
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->r:LX/FJW;

    invoke-interface {v0}, LX/FJW;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2735880
    invoke-static {p0}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->a$redex0(Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;)V

    .line 2735881
    :goto_1
    return-void

    .line 2735882
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->r:LX/FJW;

    invoke-interface {v0}, LX/FJW;->b()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->t:LX/2EJ;

    .line 2735883
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->t:LX/2EJ;

    new-instance v1, LX/Joc;

    invoke-direct {v1, p0}, LX/Joc;-><init>(Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;)V

    invoke-virtual {v0, v1}, LX/2EJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2735884
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->t:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2735889
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2735890
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->t:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2735891
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->u:Z

    .line 2735892
    iget-object v0, p0, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->t:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->cancel()V

    .line 2735893
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->c(Landroid/content/Intent;)V

    .line 2735894
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2735885
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2735886
    invoke-static {p0, p0}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2735887
    invoke-virtual {p0}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;->c(Landroid/content/Intent;)V

    .line 2735888
    return-void
.end method
