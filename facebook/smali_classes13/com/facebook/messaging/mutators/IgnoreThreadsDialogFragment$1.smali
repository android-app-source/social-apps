.class public final Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;)V
    .locals 0

    .prologue
    .line 2735766
    iput-object p1, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment$1;->a:Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    .line 2735767
    iget-object v0, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment$1;->a:Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->p:LX/JoZ;

    if-eqz v0, :cond_0

    .line 2735768
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment$1;->a:Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;

    .line 2735769
    iget-object v1, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->q:LX/0Px;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->q:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2735770
    :cond_1
    :goto_0
    return-void

    .line 2735771
    :cond_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2735772
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2735773
    iget-object v1, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->q:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    iget-object v1, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->q:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735774
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2735775
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2735776
    :cond_3
    new-instance v1, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;-><init>(LX/0Px;)V

    .line 2735777
    sget-object v2, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2735778
    iget-object v1, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->m:LX/0aG;

    const-string v2, "message_ignore_requests"

    sget-object v4, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    const v6, -0x2935b616

    invoke-static/range {v1 .. v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v1

    new-instance v2, LX/4At;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f083be0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2735779
    new-instance v2, LX/JoX;

    invoke-direct {v2, v0}, LX/JoX;-><init>(Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;)V

    .line 2735780
    iget-object v3, v0, Lcom/facebook/messaging/mutators/IgnoreThreadsDialogFragment;->o:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
