.class public final Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1c9879f9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2728992
    const-class v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2728973
    const-class v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2728990
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2728991
    return-void
.end method

.method private j()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2728988
    iget-object v0, p0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->e:Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->e:Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    .line 2728989
    iget-object v0, p0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->e:Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2728982
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2728983
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->j()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2728984
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2728985
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2728986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2728987
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2728974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2728975
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->j()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2728976
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->j()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    .line 2728977
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->j()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2728978
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;

    .line 2728979
    iput-object v0, v1, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->e:Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    .line 2728980
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2728981
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2728993
    new-instance v0, LX/Jk6;

    invoke-direct {v0, p1}, LX/Jk6;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2728964
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->j()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2728965
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2728966
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2728967
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2728968
    new-instance v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;-><init>()V

    .line 2728969
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2728970
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2728971
    const v0, 0x1f3b0a8d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2728972
    const v0, -0x64104400

    return v0
.end method
