.class public final Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2729736
    const-class v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;

    new-instance v1, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2729737
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2729738
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2729739
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2729740
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2729741
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2729742
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2729743
    if-eqz v2, :cond_2

    .line 2729744
    const-string p0, "location_sharing"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2729745
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2729746
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2729747
    if-eqz p0, :cond_0

    .line 2729748
    const-string v0, "is_sharing_enabled"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2729749
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2729750
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2729751
    if-eqz p0, :cond_1

    .line 2729752
    const-string v0, "show_nux"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2729753
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2729754
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2729755
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2729756
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2729757
    check-cast p1, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Serializer;->a(Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
