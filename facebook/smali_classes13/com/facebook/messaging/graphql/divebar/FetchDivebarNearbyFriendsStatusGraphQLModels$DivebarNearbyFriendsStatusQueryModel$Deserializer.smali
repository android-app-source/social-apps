.class public final Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2729691
    const-class v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;

    new-instance v1, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2729692
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2729693
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2729694
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2729695
    const/4 v2, 0x0

    .line 2729696
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2729697
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2729698
    :goto_0
    move v1, v2

    .line 2729699
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2729700
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2729701
    new-instance v1, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;-><init>()V

    .line 2729702
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2729703
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2729704
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2729705
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2729706
    :cond_0
    return-object v1

    .line 2729707
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2729708
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2729709
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2729710
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2729711
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2729712
    const-string v4, "location_sharing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2729713
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2729714
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_b

    .line 2729715
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2729716
    :goto_2
    move v1, v3

    .line 2729717
    goto :goto_1

    .line 2729718
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2729719
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2729720
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2729721
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_8

    .line 2729722
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2729723
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2729724
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v8, :cond_5

    .line 2729725
    const-string p0, "is_sharing_enabled"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2729726
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v7, v5

    move v5, v4

    goto :goto_3

    .line 2729727
    :cond_6
    const-string p0, "show_nux"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2729728
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v4

    goto :goto_3

    .line 2729729
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2729730
    :cond_8
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2729731
    if-eqz v5, :cond_9

    .line 2729732
    invoke-virtual {v0, v3, v7}, LX/186;->a(IZ)V

    .line 2729733
    :cond_9
    if-eqz v1, :cond_a

    .line 2729734
    invoke-virtual {v0, v4, v6}, LX/186;->a(IZ)V

    .line 2729735
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_b
    move v1, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto :goto_3
.end method
