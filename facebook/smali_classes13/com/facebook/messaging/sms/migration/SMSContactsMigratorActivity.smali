.class public Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final s:[Ljava/lang/String;


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

.field private u:LX/Jpv;

.field private v:Z

.field public w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2738287
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_CONTACTS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->s:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738286
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2738282
    iget-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->v:Z

    if-nez v0, :cond_0

    .line 2738283
    invoke-static {p0, v1, v1}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->a$redex0(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2738284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->v:Z

    .line 2738285
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v1

    check-cast v1, LX/1Ml;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->q:LX/1Ml;

    iput-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->r:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 5
    .param p0    # Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2738288
    if-nez p1, :cond_0

    .line 2738289
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->u:LX/Jpv;

    invoke-virtual {v0}, LX/Jpv;->getFirstStep()Ljava/lang/Class;

    move-result-object v0

    .line 2738290
    :goto_0
    if-nez v0, :cond_1

    .line 2738291
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2738292
    sget-object v1, LX/3RH;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2738293
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2738294
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2738295
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->finish()V

    .line 2738296
    :goto_1
    return-void

    .line 2738297
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->u:LX/Jpv;

    invoke-virtual {v0, p1}, LX/Jpv;->getNextStep(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 2738298
    :cond_1
    new-instance v1, LX/42p;

    invoke-direct {v1, v0}, LX/42p;-><init>(Ljava/lang/Class;)V

    .line 2738299
    iget-object v0, v1, LX/42p;->a:Landroid/content/Intent;

    const-string v2, "com.facebook.fragment.BUNDLE_EXTRAS"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2738300
    if-eqz p1, :cond_2

    .line 2738301
    const v0, 0x7f04003c

    const v2, 0x7f040042

    const v3, 0x7f04003c

    const v4, 0x7f040042

    invoke-virtual {v1, v0, v2, v3, v4}, LX/42p;->a(IIII)LX/42p;

    .line 2738302
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->t:Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

    .line 2738303
    iget-object v2, v1, LX/42p;->a:Landroid/content/Intent;

    move-object v1, v2

    .line 2738304
    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2738274
    invoke-static {p0, p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2738275
    const v0, 0x7f030372

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->setContentView(I)V

    .line 2738276
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0b33

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->t:Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

    .line 2738277
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->t:Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

    new-instance v1, LX/Jpt;

    invoke-direct {v1, p0}, LX/Jpt;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;)V

    .line 2738278
    iput-object v1, v0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->d:LX/42q;

    .line 2738279
    if-eqz p1, :cond_0

    .line 2738280
    const-string v0, "controller_initialized"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->v:Z

    .line 2738281
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2738258
    const/16 v0, 0x539

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2738259
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2738260
    iput-boolean p1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->w:Z

    .line 2738261
    const-string v0, "extra_permission_results"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 2738262
    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2738263
    if-nez v0, :cond_1

    .line 2738264
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->finish()V

    .line 2738265
    :goto_0
    return-void

    .line 2738266
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2738267
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_2

    .line 2738268
    invoke-static {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->a(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;)V

    goto :goto_0

    .line 2738269
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p2, :cond_3

    .line 2738270
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Cuy;->c:LX/0Tn;

    invoke-interface {v1, v2, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2738271
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 2738272
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Cuy;->c:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2738273
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->finish()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2e6281c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738254
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2738255
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->t:Lcom/facebook/base/fragment/DefaultNavigableFragmentController;

    const/4 v2, 0x0

    .line 2738256
    iput-object v2, v1, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->d:LX/42q;

    .line 2738257
    const/16 v1, 0x23

    const v2, 0x42a994b9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2738224
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2738225
    const-string v0, "controller_initialized"

    iget-boolean v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2738226
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x23b80e9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738227
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2738228
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "migration_flow"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2738229
    invoke-static {v1}, LX/Jpv;->fromString(Ljava/lang/String;)LX/Jpv;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->u:LX/Jpv;

    .line 2738230
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->u:LX/Jpv;

    if-nez v1, :cond_0

    .line 2738231
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->finish()V

    .line 2738232
    const/16 v1, 0x23

    const v2, -0x6991fb55

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2738233
    :goto_0
    return-void

    .line 2738234
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->q:LX/1Ml;

    sget-object v2, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->s:[Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2738235
    invoke-static {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->a(Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;)V

    .line 2738236
    :goto_1
    const v1, -0x7b77835d

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0

    .line 2738237
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->w:Z

    if-eqz v1, :cond_2

    .line 2738238
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->w:Z

    .line 2738239
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->finish()V

    goto :goto_1

    .line 2738240
    :cond_2
    new-instance v1, LX/2rN;

    invoke-direct {v1}, LX/2rN;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082eb0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2738241
    iput-object v2, v1, LX/2rN;->b:Ljava/lang/String;

    .line 2738242
    move-object v1, v1

    .line 2738243
    sget-object v2, LX/0jt;->ALWAYS_SHOW:LX/0jt;

    .line 2738244
    iput-object v2, v1, LX/2rN;->c:LX/0jt;

    .line 2738245
    move-object v1, v1

    .line 2738246
    const/4 v2, 0x0

    .line 2738247
    iput-boolean v2, v1, LX/2rN;->d:Z

    .line 2738248
    move-object v1, v1

    .line 2738249
    invoke-virtual {v1}, LX/2rN;->e()Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    move-result-object v1

    .line 2738250
    sget-object v2, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->s:[Ljava/lang/String;

    invoke-static {p0, v2, v1}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->a(Landroid/content/Context;[Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;)Landroid/content/Intent;

    move-result-object v1

    .line 2738251
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->r:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x539

    invoke-interface {v2, v1, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2738252
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;->w:Z

    .line 2738253
    goto :goto_1
.end method
