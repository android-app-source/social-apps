.class public Lcom/facebook/messaging/sms/migration/SMSContactPickerWithUploadDialogFragment;
.super Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738205
    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 6

    .prologue
    .line 2738206
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->h:LX/Jpc;

    .line 2738207
    new-instance v1, LX/Jpr;

    invoke-direct {v1, p0}, LX/Jpr;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerWithUploadDialogFragment;)V

    move-object v1, v1

    .line 2738208
    new-instance v2, LX/Jps;

    invoke-direct {v2, p0}, LX/Jps;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerWithUploadDialogFragment;)V

    move-object v2, v2

    .line 2738209
    iget-object v3, v0, LX/Jpc;->e:LX/JqE;

    iget-object v4, v0, LX/Jpc;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082eaa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "{learn_more_link}"

    .line 2738210
    const/4 p0, 0x0

    invoke-virtual {v3, v4, v5, p0}, LX/JqE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object p0

    move-object v3, p0

    .line 2738211
    iget-object v4, v0, LX/Jpc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/JqB;->a:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v4, v5, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2738212
    new-instance v4, LX/0ju;

    iget-object v5, v0, LX/Jpc;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v5, 0x7f082ea9

    invoke-virtual {v4, v5}, LX/0ju;->a(I)LX/0ju;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v3

    const v4, 0x7f082eac

    invoke-virtual {v3, v4, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v4, 0x7f082ead

    invoke-virtual {v3, v4, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    move-result-object v3

    .line 2738213
    const v4, 0x7f0d0578

    invoke-virtual {v3, v4}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2738214
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2738215
    iget-object v4, v0, LX/Jpc;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0832

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 2738216
    return-void
.end method
