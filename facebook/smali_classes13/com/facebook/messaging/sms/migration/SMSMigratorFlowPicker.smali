.class public Lcom/facebook/messaging/sms/migration/SMSMigratorFlowPicker;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738430
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 2738431
    invoke-static {}, LX/Jpv;->values()[LX/Jpv;

    move-result-object v2

    .line 2738432
    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jpv;

    .line 2738433
    array-length v1, v0

    new-array v3, v1, [Ljava/lang/String;

    .line 2738434
    const/4 v1, 0x0

    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_0

    .line 2738435
    aget-object v4, v2, v1

    invoke-virtual {v4}, LX/Jpv;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 2738436
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2738437
    :cond_0
    const-string v1, "nux_upload_with_contact_matching_flow"

    .line 2738438
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1}, LX/Jpv;->fromString(Ljava/lang/String;)LX/Jpv;

    move-result-object v1

    aput-object v1, v0, v2

    .line 2738439
    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    const-string v2, "Experimental (start flow defined by android_messenger_sms_migration_flow)"

    aput-object v2, v3, v1

    .line 2738440
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const-string v2, "Pick a flow"

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/Jq1;

    invoke-direct {v2, p0, v0}, LX/Jq1;-><init>(Lcom/facebook/messaging/sms/migration/SMSMigratorFlowPicker;[LX/Jpv;)V

    invoke-virtual {v1, v3, v2}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x732258e5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738427
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2738428
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/sms/migration/SMSMigratorFlowPicker;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSMigratorFlowPicker;->m:Lcom/facebook/content/SecureContextHelper;

    .line 2738429
    const/16 v1, 0x2b

    const v2, -0x50e93ea9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
