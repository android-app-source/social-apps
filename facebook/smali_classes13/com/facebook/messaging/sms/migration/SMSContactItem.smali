.class public Lcom/facebook/messaging/sms/migration/SMSContactItem;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3Lx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:LX/8ui;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/CheckBox;

.field private i:LX/8ug;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2737792
    const-class v0, Lcom/facebook/messaging/sms/migration/SMSContactItem;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2737793
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2737794
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2737795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2737796
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2737797
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2737798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2737799
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2737800
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2737801
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2737802
    invoke-static {p1}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->c(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a:LX/0wM;

    const v1, 0x7f020fec

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 2737803
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    if-nez v0, :cond_0

    .line 2737804
    new-instance v0, LX/8ui;

    invoke-direct {v0}, LX/8ui;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    .line 2737805
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1f58

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/8ui;->a(F)V

    .line 2737806
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v3, LX/0xr;->LIGHT:LX/0xr;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8ui;->a(Landroid/graphics/Typeface;)V

    .line 2737807
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 2737808
    iget-object v2, v0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2737809
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/8ui;->b(I)V

    .line 2737810
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2737787
    const-class v0, Lcom/facebook/messaging/sms/migration/SMSContactItem;

    invoke-static {v0, p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2737788
    new-instance v0, LX/8ug;

    invoke-direct {v0, p1, p2, p3}, LX/8ug;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->i:LX/8ug;

    .line 2737789
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->i:LX/8ug;

    sget-object v1, LX/8ue;->MESSENGER:LX/8ue;

    invoke-virtual {v0, v1}, LX/8ug;->a(LX/8ue;)V

    .line 2737790
    return-void
.end method

.method private static a(Lcom/facebook/messaging/sms/migration/SMSContactItem;LX/0wM;LX/3Lx;)V
    .locals 0

    .prologue
    .line 2737791
    iput-object p1, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->b:LX/3Lx;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object v1

    check-cast v1, LX/3Lx;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Lcom/facebook/messaging/sms/migration/SMSContactItem;LX/0wM;LX/3Lx;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2737786
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private c(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2737781
    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a()V

    .line 2737782
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(I)I

    move-result v1

    .line 2737783
    const/4 p1, 0x0

    iput-object p1, v0, LX/8ui;->e:Ljava/lang/String;

    .line 2737784
    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, LX/8ui;->c(LX/8ui;Ljava/lang/String;)V

    .line 2737785
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->e:LX/8ui;

    return-object v0
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/16 v0, 0x2c

    const v1, 0x14a10611

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2737736
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onFinishInflate()V

    .line 2737737
    const v0, 0x7f0d2f8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2737738
    const v0, 0x7f0d2f90

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->f:Landroid/widget/TextView;

    .line 2737739
    const v0, 0x7f0d2f91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->g:Landroid/widget/TextView;

    .line 2737740
    const v0, 0x7f0d2f92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->h:Landroid/widget/CheckBox;

    .line 2737741
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v3

    .line 2737742
    iput-object v3, v2, LX/1Uo;->u:LX/4Ab;

    .line 2737743
    move-object v2, v2

    .line 2737744
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0835

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2737745
    iput-object v3, v2, LX/1Uo;->r:Landroid/graphics/drawable/Drawable;

    .line 2737746
    move-object v2, v2

    .line 2737747
    sget-object v3, LX/1Up;->f:LX/1Up;

    invoke-virtual {v2, v3}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v2

    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2737748
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2737749
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2737750
    const/16 v0, 0x2d

    const v2, 0x50ff70c2

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2737780
    const/4 v0, 0x1

    return v0
.end method

.method public setContactRow(Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 2737760
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2737761
    iget-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v4, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v1, v4}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 2737762
    iget-object v0, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->b:LX/3Lx;

    iget-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2737763
    :goto_0
    const/4 v1, 0x0

    .line 2737764
    iget-boolean v4, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->f:Z

    move v4, v4

    .line 2737765
    if-nez v4, :cond_2

    .line 2737766
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f082eb2

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2737767
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->f:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2737768
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2737769
    iget-object v4, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->g:Landroid/widget/TextView;

    if-nez v1, :cond_3

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2737770
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->h:Landroid/widget/CheckBox;

    invoke-virtual {p1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2737771
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->h:Landroid/widget/CheckBox;

    .line 2737772
    iget-boolean v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->f:Z

    move v1, v1

    .line 2737773
    if-eqz v1, :cond_4

    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2737774
    return-void

    .line 2737775
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    goto :goto_0

    .line 2737776
    :cond_2
    iget-object v4, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2737777
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->b:LX/3Lx;

    iget-object v4, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    move v0, v3

    .line 2737778
    goto :goto_2

    :cond_4
    move v3, v2

    .line 2737779
    goto :goto_3
.end method

.method public setContactRow(Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;)V
    .locals 3

    .prologue
    .line 2737751
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2737752
    iget-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/sms/migration/SMSContactItem;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v1, v2}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 2737753
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p1, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2737754
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->i:LX/8ug;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2737755
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2737756
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->g:Landroid/widget/TextView;

    const v1, 0x7f082eb1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2737757
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2737758
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactItem;->h:Landroid/widget/CheckBox;

    invoke-virtual {p1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2737759
    return-void
.end method
