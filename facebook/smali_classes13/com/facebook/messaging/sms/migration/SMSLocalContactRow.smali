.class public Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;
.super LX/3OP;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field private e:Z

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2738339
    new-instance v0, LX/Jpw;

    invoke-direct {v0}, LX/Jpw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Jpx;)V
    .locals 1

    .prologue
    .line 2738340
    invoke-direct {p0}, LX/3OP;-><init>()V

    .line 2738341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->f:Z

    .line 2738342
    iget-object v0, p1, LX/Jpx;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2738343
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    .line 2738344
    iget-object v0, p1, LX/Jpx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2738345
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    .line 2738346
    iget v0, p1, LX/Jpx;->c:I

    move v0, v0

    .line 2738347
    iput v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    .line 2738348
    iget v0, p1, LX/Jpx;->d:I

    move v0, v0

    .line 2738349
    iput v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    .line 2738350
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2738351
    invoke-direct {p0}, LX/3OP;-><init>()V

    .line 2738352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->f:Z

    .line 2738353
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    .line 2738354
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    .line 2738355
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    .line 2738356
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    .line 2738357
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->e:Z

    .line 2738358
    return-void
.end method

.method public static d()LX/Jpx;
    .locals 1

    .prologue
    .line 2738359
    new-instance v0, LX/Jpx;

    invoke-direct {v0}, LX/Jpx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 2738360
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/3OQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2738361
    iput-boolean p1, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->e:Z

    .line 2738362
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2738363
    iget-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->e:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2738364
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2738365
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 2738366
    :cond_0
    :goto_0
    return v0

    .line 2738367
    :cond_1
    check-cast p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    .line 2738368
    iget-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    iget v2, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    iget v2, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2738369
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2738370
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2738371
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    add-int/2addr v0, v1

    .line 2738372
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    add-int/2addr v0, v1

    .line 2738373
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2738374
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2738375
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2738376
    iget v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2738377
    iget v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2738378
    iget-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2738379
    return-void
.end method
