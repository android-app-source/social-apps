.class public Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/JqC;

.field public final b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final c:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field private final d:LX/JqF;

.field public final e:LX/JqH;

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;

.field public g:LX/Jpa;

.field public h:LX/JpY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JqC;Ljava/util/concurrent/Executor;LX/0TD;LX/JqF;LX/JqH;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2737720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737721
    sget-object v0, LX/Jpa;->NONE:LX/Jpa;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    .line 2737722
    iput-object p1, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a:LX/JqC;

    .line 2737723
    iput-object p2, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b:Ljava/util/concurrent/Executor;

    .line 2737724
    iput-object p3, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->c:LX/0TD;

    .line 2737725
    iput-object p4, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->d:LX/JqF;

    .line 2737726
    iput-object p5, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->e:LX/JqH;

    .line 2737727
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LX/Jpa;
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2737715
    if-nez p0, :cond_1

    .line 2737716
    sget-object v0, LX/Jpa;->NONE:LX/Jpa;

    .line 2737717
    :cond_0
    :goto_0
    return-object v0

    .line 2737718
    :cond_1
    const-string v0, "operation_type_to_restart"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Jpa;

    .line 2737719
    if-nez v0, :cond_0

    sget-object v0, LX/Jpa;->NONE:LX/Jpa;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/DAC;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2737714
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->c:LX/0TD;

    new-instance v1, LX/JpW;

    invoke-direct {v1, p0, p1}, LX/JpW;-><init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;I)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DAC;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Jpb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2737707
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->d:LX/JqF;

    .line 2737708
    iget-object v3, v0, LX/JqF;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    .line 2737709
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2737710
    const-string v4, "matchTopSmsContactsParams"

    new-instance v5, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;

    invoke-direct {v5, p1}, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;-><init>(LX/0Px;)V

    invoke-virtual {v6, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2737711
    iget-object v4, v3, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->b:LX/0aG;

    const-string v5, "match_top_sms_contacts"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v8, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0x21ed2683

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    move-object v3, v4

    .line 2737712
    move-object v0, v3

    .line 2737713
    new-instance v1, LX/JpX;

    invoke-direct {v1, p0}, LX/JpX;-><init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V

    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;
    .locals 10

    .prologue
    .line 2737699
    new-instance v0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    .line 2737700
    new-instance v3, LX/JqC;

    const/16 v1, 0xd06

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/2Ot;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    invoke-direct {v3, v4, v1, v2}, LX/JqC;-><init>(LX/0Ot;LX/0ad;Lcom/facebook/user/model/UserKey;)V

    .line 2737701
    move-object v1, v3

    .line 2737702
    check-cast v1, LX/JqC;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {p0}, LX/JqF;->b(LX/0QB;)LX/JqF;

    move-result-object v4

    check-cast v4, LX/JqF;

    .line 2737703
    new-instance v8, LX/JqH;

    const/16 v5, 0x2e3

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v5

    check-cast v5, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v7

    check-cast v7, LX/1Ml;

    invoke-direct {v8, v9, v5, v6, v7}, LX/JqH;-><init>(LX/0Ot;Landroid/content/ContentResolver;LX/0ad;LX/1Ml;)V

    .line 2737704
    move-object v5, v8

    .line 2737705
    check-cast v5, LX/JqH;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;-><init>(LX/JqC;Ljava/util/concurrent/Executor;LX/0TD;LX/JqF;LX/JqH;)V

    .line 2737706
    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V
    .locals 1

    .prologue
    .line 2737687
    sget-object v0, LX/Jpa;->NONE:LX/Jpa;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    .line 2737688
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2737689
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    if-eqz v0, :cond_0

    .line 2737690
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    invoke-interface {v0}, LX/JpY;->b()V

    .line 2737691
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2737694
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2737695
    sget-object v0, LX/Jpa;->NONE:LX/Jpa;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    .line 2737696
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2737697
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2737698
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2737692
    const-string v0, "operation_type_to_restart"

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2737693
    return-void
.end method
