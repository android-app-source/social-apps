.class public Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""


# instance fields
.field public b:LX/67a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Jpf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Jpi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Jpd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/JqF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Jpc;

.field public i:LX/3Ne;

.field public j:Landroid/widget/TextView;

.field public k:I

.field public l:I

.field public m:LX/Jpe;

.field public n:LX/Jph;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738194
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    .line 2738195
    return-void
.end method

.method public static a(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;Landroid/os/Bundle;Ljava/lang/String;)LX/0Px;
    .locals 5
    .param p0    # Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2738180
    const/4 v0, 0x0

    .line 2738181
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2738182
    if-eqz p1, :cond_0

    .line 2738183
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2738184
    :cond_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 2738185
    const-string v2, "com.facebook.fragment.BUNDLE_EXTRAS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2738186
    if-eqz v1, :cond_2

    .line 2738187
    invoke-virtual {v1, p2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    .line 2738188
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2738189
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2738190
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 2738191
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2738192
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2738193
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2738156
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738157
    iget v1, v0, LX/Jpe;->e:I

    move v0, v1

    .line 2738158
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738159
    iget-object v2, v1, LX/Jpe;->d:LX/0Px;

    move-object v1, v2

    .line 2738160
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2738161
    if-nez v0, :cond_2

    .line 2738162
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    iget v3, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->l:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2738163
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 2738164
    :goto_0
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    invoke-virtual {v3, v0, v1}, LX/Jph;->b(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738165
    const v0, 0x7f0d2f95

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2738166
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    const/4 v5, 0x0

    .line 2738167
    iget-object v2, v1, LX/Jph;->a:LX/Jpg;

    sget-object v3, LX/Jpg;->LOCAL:LX/Jpg;

    if-ne v2, v3, :cond_0

    .line 2738168
    :cond_0
    move v1, v5

    .line 2738169
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 2738170
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2738171
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    .line 2738172
    iget-object v2, v1, LX/Jph;->b:Landroid/content/res/Resources;

    const v3, 0x7f082eb3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2738173
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738174
    const-string v1, "#0084FF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    move v1, v1

    .line 2738175
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2738176
    new-instance v1, LX/Jpl;

    invoke-direct {v1, p0}, LX/Jpl;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2738177
    :cond_1
    return-void

    .line 2738178
    :cond_2
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    iget v3, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->k:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2738179
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setClickable(Z)V

    goto :goto_0
.end method

.method public static n(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 10

    .prologue
    .line 2738019
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738020
    iget-object v1, v0, LX/Jpe;->d:LX/0Px;

    move-object v0, v1

    .line 2738021
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2738022
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3OQ;

    .line 2738023
    instance-of v2, v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    invoke-virtual {v2}, LX/3OP;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2738024
    check-cast v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2738025
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2738026
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2738027
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2738028
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    .line 2738029
    new-instance v5, LX/DAC;

    iget-object v6, v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->a:Ljava/lang/String;

    iget-object v7, v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->b:Ljava/lang/String;

    iget v8, v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->c:I

    iget v9, v1, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;->d:I

    invoke-direct {v5, v6, v7, v8, v9}, LX/DAC;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    move-object v1, v5

    .line 2738030
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2738031
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2738032
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2738033
    new-instance v1, LX/Jpn;

    invoke-direct {v1, p0}, LX/Jpn;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2738034
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    .line 2738035
    sget-object v3, LX/Jpa;->MATCHED_CONTACT_FETCH:LX/Jpa;

    iput-object v3, v2, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    .line 2738036
    invoke-static {v2, v0}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a$redex0(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2738037
    iget-object v3, v2, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/JpZ;

    invoke-direct {v4, v2, v1}, LX/JpZ;-><init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Vd;)V

    iget-object v5, v2, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2738038
    iget-object v3, v2, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    if-eqz v3, :cond_3

    .line 2738039
    iget-object v3, v2, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    invoke-interface {v3}, LX/JpY;->a()V

    .line 2738040
    :cond_3
    return-void
.end method

.method public static o(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 3

    .prologue
    .line 2738153
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->h:LX/Jpc;

    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->q()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->r()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v2

    .line 2738154
    const p0, 0x7f082ea4

    invoke-static {v0, p0, v1, v2}, LX/Jpc;->a(LX/Jpc;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2738155
    return-void
.end method

.method public static p(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 3

    .prologue
    .line 2738150
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->h:LX/Jpc;

    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->q()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->r()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v2

    .line 2738151
    const p0, 0x7f082ea6

    invoke-static {v0, p0, v1, v2}, LX/Jpc;->a(LX/Jpc;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2738152
    return-void
.end method

.method private q()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 2738149
    new-instance v0, LX/Jpo;

    invoke-direct {v0, p0}, LX/Jpo;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    return-object v0
.end method

.method private r()Landroid/content/DialogInterface$OnDismissListener;
    .locals 1

    .prologue
    .line 2738148
    new-instance v0, LX/Jpp;

    invoke-direct {v0, p0}, LX/Jpp;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2738120
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/os/Bundle;)V

    .line 2738121
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->e:LX/Jpi;

    .line 2738122
    const/4 v1, 0x0

    .line 2738123
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2738124
    if-eqz p1, :cond_0

    .line 2738125
    const-string v1, "picker_mode_param"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2738126
    :cond_0
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    .line 2738127
    const-string v3, "com.facebook.fragment.BUNDLE_EXTRAS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 2738128
    if-eqz v2, :cond_1

    .line 2738129
    const-string v1, "picker_mode_param"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2738130
    :cond_1
    if-nez v1, :cond_2

    .line 2738131
    sget-object v1, LX/Jpg;->LOCAL:LX/Jpg;

    invoke-virtual {v1}, LX/Jpg;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2738132
    :cond_2
    invoke-static {v1}, LX/Jpg;->valueOf(Ljava/lang/String;)LX/Jpg;

    move-result-object v1

    move-object v1, v1

    .line 2738133
    new-instance p1, LX/Jph;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {v0}, LX/JqE;->b(LX/0QB;)LX/JqE;

    move-result-object v3

    check-cast v3, LX/JqE;

    invoke-direct {p1, v1, v2, v3}, LX/Jph;-><init>(LX/Jpg;Landroid/content/res/Resources;LX/JqE;)V

    .line 2738134
    move-object v0, p1

    .line 2738135
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    .line 2738136
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->d:LX/Jpf;

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    .line 2738137
    new-instance v3, LX/Jpe;

    invoke-static {v0}, LX/JqE;->b(LX/0QB;)LX/JqE;

    move-result-object v2

    check-cast v2, LX/JqE;

    invoke-direct {v3, v2, v1}, LX/Jpe;-><init>(LX/JqE;LX/Jph;)V

    .line 2738138
    move-object v0, v3

    .line 2738139
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738140
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->f:LX/Jpd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Jpd;->a(LX/0gc;)LX/Jpc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->h:LX/Jpc;

    .line 2738141
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    new-instance v1, LX/Jpj;

    invoke-direct {v1, p0}, LX/Jpj;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2738142
    iput-object v1, v0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    .line 2738143
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->b:LX/67a;

    new-instance v1, LX/67b;

    invoke-direct {v1, p0}, LX/67b;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 2738144
    iput-object v1, v0, LX/67a;->b:LX/67V;

    .line 2738145
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->b:LX/67a;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2738146
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->b:LX/67a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/67a;->a(I)Z

    .line 2738147
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 2738118
    sget-object v0, LX/Jpu;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/content/Intent;)V

    .line 2738119
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x2

    const/16 v3, 0x2a

    const v4, 0x46f911da

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2738099
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2738100
    invoke-static {p1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a(Landroid/os/Bundle;)LX/Jpa;

    move-result-object v4

    .line 2738101
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738102
    iget-object v5, v2, LX/Jpe;->d:LX/0Px;

    move-object v2, v5

    .line 2738103
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    .line 2738104
    iget-object v5, v2, LX/Jph;->a:LX/Jpg;

    move-object v2, v5

    .line 2738105
    sget-object v5, LX/Jpg;->LOCAL:LX/Jpg;

    if-ne v2, v5, :cond_3

    move v2, v1

    .line 2738106
    :goto_0
    if-nez v2, :cond_0

    sget-object v2, LX/Jpa;->LOCAL_CONTACT_FETCH:LX/Jpa;

    if-ne v4, v2, :cond_1

    :cond_0
    move v0, v1

    .line 2738107
    :cond_1
    if-eqz v0, :cond_4

    .line 2738108
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    invoke-virtual {v0}, LX/3Ne;->a()V

    .line 2738109
    new-instance v0, LX/Jpm;

    invoke-direct {v0, p0}, LX/Jpm;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2738110
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    .line 2738111
    sget-object v2, LX/Jpa;->LOCAL_CONTACT_FETCH:LX/Jpa;

    iput-object v2, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    .line 2738112
    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2738113
    iget-object v2, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/JpZ;

    invoke-direct {v4, v1, v0}, LX/JpZ;-><init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Vd;)V

    iget-object p0, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b:Ljava/util/concurrent/Executor;

    invoke-static {v2, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2738114
    :cond_2
    :goto_1
    const v0, 0x1234077f

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    :cond_3
    move v2, v0

    .line 2738115
    goto :goto_0

    .line 2738116
    :cond_4
    sget-object v0, LX/Jpa;->MATCHED_CONTACT_FETCH:LX/Jpa;

    if-ne v4, v0, :cond_2

    .line 2738117
    invoke-static {p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    goto :goto_1
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2738096
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onAttach(Landroid/content/Context;)V

    .line 2738097
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, LX/67a;->a(LX/0QB;)LX/67a;

    move-result-object v3

    check-cast v3, LX/67a;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b(LX/0QB;)Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    const-class v5, LX/Jpf;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Jpf;

    const-class v6, LX/Jpi;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Jpi;

    const-class p1, LX/Jpd;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/Jpd;

    invoke-static {v0}, LX/JqF;->b(LX/0QB;)LX/JqF;

    move-result-object v0

    check-cast v0, LX/JqF;

    iput-object v3, v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->b:LX/67a;

    iput-object v4, v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    iput-object v5, v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->d:LX/Jpf;

    iput-object v6, v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->e:LX/Jpi;

    iput-object p1, v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->f:LX/Jpd;

    iput-object v0, v2, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->g:LX/JqF;

    .line 2738098
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x291b2644

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738095
    const v1, 0x7f031512

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x66a68c66

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f24ec9e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738092
    invoke-super {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onDestroy()V

    .line 2738093
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-virtual {v1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a()V

    .line 2738094
    const/16 v1, 0x2b

    const v2, 0x694d55f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 2738089
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 2738090
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2738091
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2738071
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2738072
    const-string v0, "picker_mode_param"

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    .line 2738073
    iget-object v2, v1, LX/Jph;->a:LX/Jpg;

    move-object v1, v2

    .line 2738074
    invoke-virtual {v1}, LX/Jpg;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2738075
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2738076
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2738077
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738078
    iget-object v1, v0, LX/Jpe;->d:LX/0Px;

    move-object v4, v1

    .line 2738079
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2738080
    instance-of v6, v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    if-eqz v6, :cond_1

    .line 2738081
    check-cast v0, Lcom/facebook/messaging/sms/migration/SMSLocalContactRow;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2738082
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2738083
    :cond_1
    instance-of v6, v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    if-eqz v6, :cond_0

    .line 2738084
    check-cast v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2738085
    :cond_2
    const-string v0, "matched_contacts_param"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2738086
    const-string v0, "local_contacts_param"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2738087
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b(Landroid/os/Bundle;)V

    .line 2738088
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2738041
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2738042
    const-string v0, "matched_contacts_param"

    invoke-static {p0, p2, v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->a(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;Landroid/os/Bundle;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 2738043
    const-string v1, "local_contacts_param"

    invoke-static {p0, p2, v1}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->a(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;Landroid/os/Bundle;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 2738044
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p1

    invoke-virtual {p1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2738045
    new-instance v1, LX/3Ne;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f030d5f

    invoke-direct {v1, p1, p2}, LX/3Ne;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    .line 2738046
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    iget-object p1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    invoke-virtual {v1, p1}, LX/3Ne;->setAdapter(LX/3LH;)V

    .line 2738047
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    invoke-virtual {v1, v0}, LX/3Ne;->a(LX/0Px;)V

    .line 2738048
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    new-instance p1, LX/Jpk;

    invoke-direct {p1, p0}, LX/Jpk;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2738049
    iput-object p1, v1, LX/3Ne;->c:LX/3O4;

    .line 2738050
    const v1, 0x7f0d2f94

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const p1, 0x7f0d2f94

    iget-object p2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    invoke-static {v1, p1, p2}, LX/478;->a(Landroid/view/ViewGroup;ILandroid/view/View;)V

    .line 2738051
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->b:LX/67a;

    invoke-virtual {v0}, LX/67a;->f()LX/3u1;

    move-result-object v0

    .line 2738052
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738053
    iget p1, v2, LX/Jpe;->g:I

    move v2, p1

    .line 2738054
    iget-object p1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738055
    iget-object p2, p1, LX/Jpe;->d:LX/0Px;

    move-object p1, p2

    .line 2738056
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    invoke-virtual {v1, v2, p1}, LX/Jph;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3u1;->a(Ljava/lang/CharSequence;)V

    .line 2738057
    const/4 v1, 0x1

    move v1, v1

    .line 2738058
    invoke-virtual {v0, v1}, LX/3u1;->a(Z)V

    .line 2738059
    const v1, 0x7f020015

    invoke-virtual {v0, v1}, LX/3u1;->d(I)V

    .line 2738060
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2738061
    const v0, 0x7f0d2f96

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    .line 2738062
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738063
    iget p1, v2, LX/Jpe;->e:I

    move v2, p1

    .line 2738064
    iget-object p1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738065
    iget-object p2, p1, LX/Jpe;->d:LX/0Px;

    move-object p1, p2

    .line 2738066
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    invoke-virtual {v1, v2, p1}, LX/Jph;->b(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738067
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->j:Landroid/widget/TextView;

    new-instance v1, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;-><init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2738068
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0832

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->k:I

    .line 2738069
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0833

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->l:I

    .line 2738070
    return-void
.end method
