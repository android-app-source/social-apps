.class public Lcom/facebook/messaging/sms/migration/SMSUploadFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""


# instance fields
.field public d:LX/Ien;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/46x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/JqE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/JqF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738517
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 2738515
    sget-object v0, LX/Jpu;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/content/Intent;)V

    .line 2738516
    return-void
.end method


# virtual methods
.method public c()V
    .locals 1

    .prologue
    .line 2738512
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->i:LX/JqF;

    invoke-virtual {v0}, LX/JqF;->a()V

    .line 2738513
    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->k()V

    .line 2738514
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 2738473
    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->k()V

    .line 2738474
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2738509
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onAttach(Landroid/content/Context;)V

    .line 2738510
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;

    invoke-static {v0}, LX/Ien;->b(LX/0QB;)LX/Ien;

    move-result-object v3

    check-cast v3, LX/Ien;

    invoke-static {v0}, LX/46x;->b(LX/0QB;)LX/46x;

    move-result-object v4

    check-cast v4, LX/46x;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/JqE;->b(LX/0QB;)LX/JqE;

    move-result-object p1

    check-cast p1, LX/JqE;

    invoke-static {v0}, LX/JqF;->b(LX/0QB;)LX/JqF;

    move-result-object v0

    check-cast v0, LX/JqF;

    iput-object v3, v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->d:LX/Ien;

    iput-object v4, v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->e:LX/46x;

    iput-object v5, v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v6, v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    iput-object p1, v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->h:LX/JqE;

    iput-object v0, v2, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->i:LX/JqF;

    .line 2738511
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7b85b13f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738508
    const v1, 0x7f031357

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7a4ec340

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2756cd51

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738505
    invoke-super {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onStart()V

    .line 2738506
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/JqB;->a:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2738507
    const/16 v1, 0x2b

    const v2, 0x4a2e3325    # 2854089.2f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2738475
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2738476
    const v0, 0x7f0d2cba

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2738477
    const v1, 0x7f020f48

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2738478
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    const v1, 0x7f082e9a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2738479
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2738480
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738481
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    const v1, 0x7f082e9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2738482
    const v0, 0x7f0d2cbb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2738483
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738484
    new-instance v1, LX/Jq7;

    invoke-direct {v1, p0}, LX/Jq7;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2738485
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    const v1, 0x7f082e9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2738486
    const v0, 0x7f0d2cbc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2738487
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738488
    new-instance v1, LX/Jq8;

    invoke-direct {v1, p0}, LX/Jq8;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2738489
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    const v1, 0x7f082e9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2738490
    const v0, 0x7f0d2cbd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2738491
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2738492
    new-instance v1, Landroid/text/style/UnderlineSpan;

    invoke-direct {v1}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v2, v1, v4, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2738493
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738494
    new-instance v1, LX/Jq9;

    invoke-direct {v1, p0}, LX/Jq9;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2738495
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    const v1, 0x7f082e9b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2738496
    const v1, 0x7f0d1bb7

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2738497
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2738498
    iget-object v2, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->g:Landroid/content/res/Resources;

    const v3, 0x7f082e9c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2738499
    iget-object v3, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->h:LX/JqE;

    const-string v4, "{learn_more_link}"

    invoke-virtual {v3, v0, v4, v2}, LX/JqE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v2

    .line 2738500
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2738501
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v6, v5

    .line 2738502
    iget-object v5, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->e:LX/46x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0078

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    const v8, 0x7f0d2cba

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v8}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, LX/46x;->a(Landroid/view/View;ILjava/util/List;)V

    .line 2738503
    iget-object v5, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->e:LX/46x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0077

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    const v8, 0x7f0d02c4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const v9, 0x7f0d1bb7

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v8, v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    const v9, 0x7f0b0146

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const v10, 0x7f0b0148

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9, v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v9

    const v10, 0x7f0b0145

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const v11, 0x7f0b0147

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v10, v11}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    invoke-virtual/range {v5 .. v10}, LX/46x;->a(Landroid/view/View;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 2738504
    return-void
.end method
