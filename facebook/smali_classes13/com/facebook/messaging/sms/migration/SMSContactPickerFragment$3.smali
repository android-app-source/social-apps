.class public final Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 0

    .prologue
    .line 2737937
    iput-object p1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x94463e8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2737938
    sget-object v0, LX/Jpq;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    .line 2737939
    iget-object v2, v1, LX/Jph;->a:LX/Jpg;

    move-object v1, v2

    .line 2737940
    invoke-virtual {v1}, LX/Jpg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2737941
    :goto_0
    const v0, -0x26b3a1f7    # -3.595268E15f

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 2737942
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    goto :goto_0

    .line 2737943
    :pswitch_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2737944
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2737945
    iget-object v1, v0, LX/Jpe;->d:LX/0Px;

    move-object v5, v1

    .line 2737946
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2737947
    instance-of v1, v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    invoke-virtual {v1}, LX/3OP;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2737948
    check-cast v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737949
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2737950
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->g:LX/JqF;

    .line 2737951
    iget-object v7, v0, LX/JqF;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    .line 2737952
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2737953
    const-string v8, "target_ids"

    invoke-virtual {v10, v8, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2737954
    iget-object v8, v7, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->b:LX/0aG;

    const-string v9, "begin_journeys"

    sget-object v11, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v12, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static {v12}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v12

    const v13, 0x98ea817

    invoke-static/range {v8 .. v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v8

    invoke-interface {v8}, LX/1MF;->start()LX/1ML;

    .line 2737955
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment$3;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
