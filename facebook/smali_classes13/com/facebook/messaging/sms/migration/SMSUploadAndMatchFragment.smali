.class public Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;
.super Lcom/facebook/messaging/sms/migration/SMSUploadFragment;
.source ""


# instance fields
.field public b:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Jpd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Jpc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738518
    invoke-direct {p0}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;

    invoke-static {v2}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b(LX/0QB;)Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    const-class p0, LX/Jpd;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Jpd;

    iput-object v1, p1, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->b:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    iput-object v2, p1, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->c:LX/Jpd;

    return-void
.end method

.method public static e(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V
    .locals 4

    .prologue
    .line 2738519
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->j:LX/Jpc;

    new-instance v1, LX/Jq5;

    invoke-direct {v1, p0}, LX/Jq5;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V

    new-instance v2, LX/Jq6;

    invoke-direct {v2, p0}, LX/Jq6;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V

    .line 2738520
    new-instance v3, LX/31Y;

    iget-object p0, v0, LX/Jpc;->a:Landroid/content/Context;

    invoke-direct {v3, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const p0, 0x7f082ea8

    invoke-virtual {v3, p0, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v3

    const p0, 0x7f082eae

    invoke-virtual {v3, p0}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    const p0, 0x7f082eaf

    invoke-virtual {v3, p0}, LX/0ju;->b(I)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    .line 2738521
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 5

    .prologue
    .line 2738522
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->i:LX/JqF;

    invoke-virtual {v0}, LX/JqF;->a()V

    .line 2738523
    new-instance v0, LX/Jq4;

    invoke-direct {v0, p0}, LX/Jq4;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V

    .line 2738524
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->b:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    .line 2738525
    const/16 v2, 0xa

    .line 2738526
    sget-object v3, LX/Jpa;->COMBINED_FETCH:LX/Jpa;

    iput-object v3, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->g:LX/Jpa;

    .line 2738527
    invoke-static {v1, v2}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/JpV;

    invoke-direct {v4, v1}, LX/JpV;-><init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V

    iget-object p0, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, p0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2738528
    iget-object v3, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/JpZ;

    invoke-direct {v4, v1, v0}, LX/JpZ;-><init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Vd;)V

    iget-object p0, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2738529
    iget-object v3, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    if-eqz v3, :cond_0

    .line 2738530
    iget-object v3, v1, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    invoke-interface {v3}, LX/JpY;->a()V

    .line 2738531
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2738532
    sget-object v0, LX/Jpu;->b:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/content/Intent;)V

    .line 2738533
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x237cd2b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738534
    invoke-super {p0, p1}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2738535
    invoke-static {p1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a(Landroid/os/Bundle;)LX/Jpa;

    move-result-object v1

    sget-object v2, LX/Jpa;->COMBINED_FETCH:LX/Jpa;

    if-ne v1, v2, :cond_0

    .line 2738536
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->c()V

    .line 2738537
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x476bdfaf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2738538
    invoke-super {p0, p1}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->onAttach(Landroid/content/Context;)V

    .line 2738539
    const-class v0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2738540
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->c:LX/Jpd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Jpd;->a(LX/0gc;)LX/Jpc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->j:LX/Jpc;

    .line 2738541
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->b:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    new-instance v1, LX/Jq3;

    invoke-direct {v1, p0}, LX/Jq3;-><init>(Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;)V

    .line 2738542
    iput-object v1, v0, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->h:LX/JpY;

    .line 2738543
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3ea741fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2738544
    invoke-super {p0}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->onDestroy()V

    .line 2738545
    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->b:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-virtual {v1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->a()V

    .line 2738546
    const/16 v1, 0x2b

    const v2, -0x55629a60

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2738547
    invoke-super {p0, p1}, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2738548
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;->b:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b(Landroid/os/Bundle;)V

    .line 2738549
    return-void
.end method
