.class public Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;
.super LX/3OP;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public final d:Ljava/lang/String;

.field private e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2738385
    new-instance v0, LX/Jpy;

    invoke-direct {v0}, LX/Jpy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Jpz;)V
    .locals 1

    .prologue
    .line 2738386
    invoke-direct {p0}, LX/3OP;-><init>()V

    .line 2738387
    iget-object v0, p1, LX/Jpz;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2738388
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    .line 2738389
    iget-object v0, p1, LX/Jpz;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2738390
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->b:Ljava/lang/String;

    .line 2738391
    iget-object v0, p1, LX/Jpz;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 2738392
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->c:Landroid/net/Uri;

    .line 2738393
    iget-object v0, p1, LX/Jpz;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2738394
    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->d:Ljava/lang/String;

    .line 2738395
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2738396
    invoke-direct {p0}, LX/3OP;-><init>()V

    .line 2738397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    .line 2738398
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->b:Ljava/lang/String;

    .line 2738399
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->c:Landroid/net/Uri;

    .line 2738400
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->d:Ljava/lang/String;

    .line 2738401
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->e:Z

    .line 2738402
    return-void
.end method


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 2738403
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/3OQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2738404
    iput-boolean p1, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->e:Z

    .line 2738405
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2738406
    iget-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->e:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2738407
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2738408
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 2738409
    :cond_0
    const/4 v0, 0x0

    .line 2738410
    :goto_0
    return v0

    .line 2738411
    :cond_1
    check-cast p1, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;

    .line 2738412
    iget-object v0, p1, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2738413
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2738414
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2738415
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2738416
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2738417
    iget-object v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2738418
    iget-boolean v0, p0, Lcom/facebook/messaging/sms/migration/SMSMatchedContactRow;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2738419
    return-void
.end method
