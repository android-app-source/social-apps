.class public final Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/Jfc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/LinearLayout;

.field public d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

.field public e:Lcom/facebook/resources/ui/FbEditText;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Landroid/widget/ProgressBar;

.field public h:LX/4At;

.field public i:Landroid/view/MenuItem;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:LX/IZ9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2722197
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2722198
    return-void
.end method

.method public static d$redex0(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V
    .locals 2

    .prologue
    .line 2722202
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->g:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2722203
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->c:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2722204
    return-void
.end method

.method public static e(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V
    .locals 1

    .prologue
    .line 2722199
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->h:LX/4At;

    if-eqz v0, :cond_0

    .line 2722200
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->h:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2722201
    :cond_0
    return-void
.end method

.method public static k(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V
    .locals 2

    .prologue
    .line 2722127
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->i:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    .line 2722128
    :goto_0
    return-void

    .line 2722129
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->i:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 2722130
    iget p0, v0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    move v0, p0

    .line 2722131
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2722196
    const v0, 0x7f083bef

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2722194
    iput-object p1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->m:LX/IZ9;

    .line 2722195
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2722190
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2722191
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2722192
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    new-instance v1, LX/Jfc;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-direct {v1, v2, p1, v0}, LX/Jfc;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    move-object v2, v1

    check-cast v2, LX/Jfc;

    invoke-static {v3}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v3

    check-cast v3, LX/IZK;

    iput-object v2, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->a:LX/Jfc;

    iput-object v3, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->b:LX/IZK;

    .line 2722193
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2722205
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->j:Ljava/lang/String;

    .line 2722206
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2722187
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2722188
    const v0, 0x7f11002b

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2722189
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x44d4ca84

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2722186
    const v1, 0x7f0311de

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2c21784b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7a970346

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2722182
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroy()V

    .line 2722183
    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->a:LX/Jfc;

    .line 2722184
    iget-object v2, v1, LX/Jfc;->c:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2722185
    const/16 v1, 0x2b

    const v2, -0x50454fb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2722164
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3253

    if-ne v0, v1, :cond_0

    .line 2722165
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->a:LX/Jfc;

    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 2722166
    iget v3, v2, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    move v2, v3

    .line 2722167
    iget-object v3, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Jfe;

    invoke-direct {v4, p0}, LX/Jfe;-><init>(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722168
    new-instance v5, LX/4DF;

    invoke-direct {v5}, LX/4DF;-><init>()V

    .line 2722169
    const-string v6, "page_id"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722170
    move-object v5, v5

    .line 2722171
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 2722172
    const-string p0, "review_rating"

    invoke-virtual {v5, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2722173
    move-object v5, v5

    .line 2722174
    const-string v6, "review_text"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722175
    move-object v5, v5

    .line 2722176
    new-instance v6, LX/JfO;

    invoke-direct {v6}, LX/JfO;-><init>()V

    move-object v6, v6

    .line 2722177
    const-string p0, "input"

    invoke-virtual {v6, p0, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2722178
    invoke-virtual {v4}, LX/Jfe;->a()V

    .line 2722179
    iget-object v5, v0, LX/Jfc;->c:LX/1Ck;

    sget-object p0, LX/Jfb;->POST_REVIEW:LX/Jfb;

    iget-object p1, v0, LX/Jfc;->b:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance p1, LX/Jfa;

    invoke-direct {p1, v0, v4}, LX/Jfa;-><init>(LX/Jfc;LX/Jfe;)V

    invoke-virtual {v5, p0, v6, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2722180
    const/4 v0, 0x1

    .line 2722181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2722159
    const v0, 0x7f0d3253

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->i:Landroid/view/MenuItem;

    .line 2722160
    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->i:Landroid/view/MenuItem;

    iget-boolean v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->l:Z

    if-eqz v0, :cond_0

    const v0, 0x7f083bf5

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2722161
    invoke-static {p0}, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722162
    return-void

    .line 2722163
    :cond_0
    const v0, 0x7f083bf4

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2722152
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2722153
    const-string v0, "review_rating"

    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 2722154
    iget v2, v1, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    move v1, v2

    .line 2722155
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2722156
    const-string v0, "is_updated"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2722157
    const-string v0, "page_name"

    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722158
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2722132
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2722133
    const v0, 0x7f0d29f2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->c:Landroid/widget/LinearLayout;

    .line 2722134
    const v0, 0x7f0d29f3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ratingbar/BetterRatingBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 2722135
    const v0, 0x7f0d29f4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 2722136
    const v0, 0x7f0d29f5    # 1.87639E38f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2722137
    const v0, 0x7f0d29f6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->g:Landroid/widget/ProgressBar;

    .line 2722138
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    new-instance v1, LX/Jfd;

    invoke-direct {v1, p0}, LX/Jfd;-><init>(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(LX/8pS;)V

    .line 2722139
    if-eqz p2, :cond_0

    const-string v0, "review_rating"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2722140
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    const-string v1, "review_rating"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->setRating(I)V

    .line 2722141
    const-string v0, "is_updated"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->l:Z

    .line 2722142
    const-string v0, "page_name"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    .line 2722143
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x7f083bf3

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2722144
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f083bf6

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2722145
    :goto_0
    return-void

    .line 2722146
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->a:LX/Jfc;

    iget-object v1, p0, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->j:Ljava/lang/String;

    new-instance v2, LX/Jff;

    invoke-direct {v2, p0}, LX/Jff;-><init>(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722147
    new-instance v3, LX/JfS;

    invoke-direct {v3}, LX/JfS;-><init>()V

    move-object v3, v3

    .line 2722148
    const-string v4, "page_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2722149
    invoke-virtual {v2}, LX/Jff;->a()V

    .line 2722150
    iget-object v4, v0, LX/Jfc;->c:LX/1Ck;

    sget-object v5, LX/Jfb;->FETCH_REVIEW:LX/Jfb;

    iget-object p0, v0, LX/Jfc;->b:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    new-instance p0, LX/JfZ;

    invoke-direct {p0, v0, v2}, LX/JfZ;-><init>(LX/Jfc;LX/Jff;)V

    invoke-virtual {v4, v5, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2722151
    goto :goto_0
.end method
