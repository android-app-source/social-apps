.class public final Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2721858
    const-class v0, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2721857
    const-class v0, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2721855
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2721856
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2721853
    iget-object v0, p0, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;->e:Ljava/lang/String;

    .line 2721854
    iget-object v0, p0, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2721859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2721860
    invoke-direct {p0}, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2721861
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2721862
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2721863
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2721864
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2721850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2721851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2721852
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2721847
    new-instance v0, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;-><init>()V

    .line 2721848
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2721849
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2721845
    const v0, -0x653b6a01

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2721846
    const v0, 0x266f65

    return v0
.end method
