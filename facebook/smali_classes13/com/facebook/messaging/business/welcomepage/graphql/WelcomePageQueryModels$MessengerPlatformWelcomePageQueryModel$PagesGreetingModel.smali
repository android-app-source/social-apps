.class public final Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x219cf0ff
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2722886
    const-class v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2722887
    const-class v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2722888
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2722889
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2722890
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;->f:Ljava/lang/String;

    .line 2722891
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2722892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2722893
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722894
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2722895
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 2722896
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2722897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2722898
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2722899
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2722900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2722901
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2722902
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2722903
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;->e:Z

    .line 2722904
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2722905
    new-instance v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;-><init>()V

    .line 2722906
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2722907
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2722908
    const v0, 0x1ff2a88d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2722909
    const v0, 0x3fe7841d

    return v0
.end method
