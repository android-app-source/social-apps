.class public final Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6efeedc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2723072
    const-class v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2723058
    const-class v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2723059
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2723060
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723061
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2723062
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2723063
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723064
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723065
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBestDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723066
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723067
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723068
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723069
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private n()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHours"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2723070
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->i:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x4

    const v4, 0x6505315d

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->i:LX/3Sb;

    .line 2723071
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->i:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723077
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->j:Ljava/lang/String;

    .line 2723078
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723079
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723080
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2723075
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p:Ljava/util/List;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p:Ljava/util/List;

    .line 2723076
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private r()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerWelcomePageContextBanner"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723073
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723074
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->q:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723054
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r:Ljava/lang/String;

    .line 2723055
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723056
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->s:Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->s:Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    .line 2723057
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->s:Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    return-object v0
.end method

.method private u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723052
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723053
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723050
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u:Ljava/lang/String;

    .line 2723051
    iget-object v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u:Ljava/lang/String;

    return-object v0
.end method

.method private w()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723048
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723049
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->v:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResponsivenessContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 2723046
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2723047
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 2723008
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2723009
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2723010
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x43aeb34a

    invoke-static {v5, v4, v6}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2723011
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->l()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0x1a876b9c

    invoke-static {v6, v5, v7}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2723012
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->m()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x54dd1d34

    invoke-static {v7, v6, v8}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2723013
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->n()LX/2uF;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v7, v0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v7

    .line 2723014
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2723015
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    const v11, 0x594d3bd4

    invoke-static {v10, v9, v11}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2723016
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->q()LX/0Px;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 2723017
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    const v13, 0x5cf9bd60

    invoke-static {v12, v11, v13}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2723018
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->s()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2723019
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t()Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2723020
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u()LX/1vs;

    move-result-object v14

    iget-object v15, v14, LX/1vs;->a:LX/15i;

    iget v14, v14, LX/1vs;->b:I

    const v16, -0x6e392ff0

    move/from16 v0, v16

    invoke-static {v15, v14, v0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2723021
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->v()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2723022
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w()LX/1vs;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget v0, v0, LX/1vs;->b:I

    move/from16 v16, v0

    const v18, 0x5a2c5bd5

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 2723023
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->x()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    const v19, -0x530af3ed

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/messaging/responsiveness/graphql/FetchPageResponsivenessGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/messaging/responsiveness/graphql/FetchPageResponsivenessGraphQLModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2723024
    const/16 v18, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2723025
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2723026
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 2723027
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 2723028
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2723029
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2723030
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2723031
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 2723032
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 2723033
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->m:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 2723034
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 2723035
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2723036
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2723037
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 2723038
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 2723039
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 2723040
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 2723041
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 2723042
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2723043
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2723044
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2723045
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2722941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2722942
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2722943
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x43aeb34a

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2722944
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2722945
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722946
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->f:I

    move-object v1, v0

    .line 2722947
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2722948
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1a876b9c

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2722949
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2722950
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722951
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->g:I

    move-object v1, v0

    .line 2722952
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2722953
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x54dd1d34

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2722954
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2722955
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722956
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->h:I

    move-object v1, v0

    .line 2722957
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->n()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2722958
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->n()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2722959
    if-eqz v2, :cond_3

    .line 2722960
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722961
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->i:LX/3Sb;

    move-object v1, v0

    .line 2722962
    :cond_3
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2722963
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x594d3bd4

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2722964
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2722965
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722966
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->o:I

    move-object v1, v0

    .line 2722967
    :cond_4
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2722968
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2722969
    if-eqz v2, :cond_5

    .line 2722970
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722971
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->p:Ljava/util/List;

    move-object v1, v0

    .line 2722972
    :cond_5
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2722973
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5cf9bd60

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2722974
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2722975
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722976
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->q:I

    move-object v1, v0

    .line 2722977
    :cond_6
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t()Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2722978
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t()Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    .line 2722979
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t()Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 2722980
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722981
    iput-object v0, v1, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->s:Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel$PagesGreetingModel;

    .line 2722982
    :cond_7
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 2722983
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6e392ff0

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 2722984
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2722985
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722986
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t:I

    move-object v1, v0

    .line 2722987
    :cond_8
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 2722988
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5a2c5bd5

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 2722989
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2722990
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722991
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->v:I

    move-object v1, v0

    .line 2722992
    :cond_9
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->x()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_a

    .line 2722993
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x530af3ed

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/messaging/responsiveness/graphql/FetchPageResponsivenessGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/messaging/responsiveness/graphql/FetchPageResponsivenessGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 2722994
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->x()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2722995
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    .line 2722996
    iput v3, v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w:I

    move-object v1, v0

    .line 2722997
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2722998
    if-nez v1, :cond_b

    :goto_0
    return-object p0

    .line 2722999
    :catchall_0
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v0

    .line 2723000
    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v0

    .line 2723001
    :catchall_2
    move-exception v0

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v0

    .line 2723002
    :catchall_3
    move-exception v0

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v0

    .line 2723003
    :catchall_4
    move-exception v0

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v0

    .line 2723004
    :catchall_5
    move-exception v0

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    throw v0

    .line 2723005
    :catchall_6
    move-exception v0

    :try_start_e
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    throw v0

    .line 2723006
    :catchall_7
    move-exception v0

    :try_start_f
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    throw v0

    :cond_b
    move-object p0, v1

    .line 2723007
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2722940
    new-instance v0, LX/JgO;

    invoke-direct {v0, p1}, LX/JgO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2722917
    invoke-direct {p0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2722926
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2722927
    const/4 v0, 0x1

    const v1, -0x43aeb34a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->f:I

    .line 2722928
    const/4 v0, 0x2

    const v1, -0x1a876b9c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->g:I

    .line 2722929
    const/4 v0, 0x3

    const v1, 0x54dd1d34

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->h:I

    .line 2722930
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->k:Z

    .line 2722931
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->l:Z

    .line 2722932
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->m:Z

    .line 2722933
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->n:Z

    .line 2722934
    const/16 v0, 0xa

    const v1, 0x594d3bd4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->o:I

    .line 2722935
    const/16 v0, 0xc

    const v1, 0x5cf9bd60

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->q:I

    .line 2722936
    const/16 v0, 0xf

    const v1, -0x6e392ff0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->t:I

    .line 2722937
    const/16 v0, 0x11

    const v1, 0x5a2c5bd5

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->v:I

    .line 2722938
    const/16 v0, 0x12

    const v1, -0x530af3ed

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;->w:I

    .line 2722939
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2722924
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2722925
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2722923
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2722920
    new-instance v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$MessengerPlatformWelcomePageQueryModel;-><init>()V

    .line 2722921
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2722922
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2722919
    const v0, 0x59f50085

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2722918
    const v0, 0x252222

    return v0
.end method
