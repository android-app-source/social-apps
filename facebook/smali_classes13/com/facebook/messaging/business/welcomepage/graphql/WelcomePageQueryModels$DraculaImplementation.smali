.class public final Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2722727
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2722728
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2722729
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2722730
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2722731
    if-nez p1, :cond_0

    .line 2722732
    :goto_0
    return v1

    .line 2722733
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2722734
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2722735
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722736
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722737
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722738
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722739
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2722740
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722741
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722742
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722743
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722744
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2722745
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2722746
    const v2, -0x2351c208

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2722747
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722748
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722749
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2722750
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2722751
    const v2, 0x55c03ebf

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2722752
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722753
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722754
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2722755
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722756
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722757
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722758
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722759
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2722760
    :sswitch_5
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2722761
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v6

    .line 2722762
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2722763
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2722764
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2722765
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722766
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722767
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722768
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722769
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722770
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722771
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2722772
    const v2, -0x7ad90431

    const/4 v4, 0x0

    .line 2722773
    if-nez v0, :cond_1

    move v3, v4

    .line 2722774
    :goto_1
    move v0, v3

    .line 2722775
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 2722776
    const v3, -0x26174c3d

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2722777
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2722778
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722779
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2722780
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722781
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722782
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722783
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722784
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722785
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722786
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722787
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722788
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722789
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722790
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722791
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722792
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722793
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722794
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722795
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722796
    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2722797
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2722798
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2722799
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2722800
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2722801
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 2722802
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 2722803
    :goto_2
    if-ge v4, v5, :cond_3

    .line 2722804
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v6

    .line 2722805
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 2722806
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2722807
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 2722808
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ad90431 -> :sswitch_8
        -0x6e392ff0 -> :sswitch_a
        -0x43aeb34a -> :sswitch_0
        -0x26174c3d -> :sswitch_9
        -0x2351c208 -> :sswitch_3
        -0x1a876b9c -> :sswitch_1
        0x54dd1d34 -> :sswitch_2
        0x55c03ebf -> :sswitch_4
        0x594d3bd4 -> :sswitch_6
        0x5a2c5bd5 -> :sswitch_b
        0x5cf9bd60 -> :sswitch_7
        0x6505315d -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2722809
    if-nez p0, :cond_0

    move v0, v1

    .line 2722810
    :goto_0
    return v0

    .line 2722811
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2722812
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2722813
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2722814
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2722815
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2722816
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2722817
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2722818
    const/4 v7, 0x0

    .line 2722819
    const/4 v1, 0x0

    .line 2722820
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2722821
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2722822
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2722823
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2722824
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2722825
    new-instance v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2722826
    sparse-switch p2, :sswitch_data_0

    .line 2722827
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2722828
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2722829
    const v1, -0x2351c208

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2722830
    :goto_0
    :sswitch_1
    return-void

    .line 2722831
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2722832
    const v1, 0x55c03ebf

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2722833
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2722834
    const v1, -0x7ad90431

    .line 2722835
    if-eqz v0, :cond_0

    .line 2722836
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v3

    .line 2722837
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    .line 2722838
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2722839
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2722840
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2722841
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2722842
    const v1, -0x26174c3d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7ad90431 -> :sswitch_1
        -0x6e392ff0 -> :sswitch_1
        -0x43aeb34a -> :sswitch_1
        -0x26174c3d -> :sswitch_1
        -0x2351c208 -> :sswitch_2
        -0x1a876b9c -> :sswitch_1
        0x54dd1d34 -> :sswitch_0
        0x55c03ebf -> :sswitch_1
        0x594d3bd4 -> :sswitch_1
        0x5a2c5bd5 -> :sswitch_1
        0x5cf9bd60 -> :sswitch_3
        0x6505315d -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2722843
    if-eqz p1, :cond_0

    .line 2722844
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2722845
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    .line 2722846
    if-eq v0, v1, :cond_0

    .line 2722847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2722848
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2722726
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2722849
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2722850
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2722721
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2722722
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2722723
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2722724
    iput p2, p0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->b:I

    .line 2722725
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2722695
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2722720
    new-instance v0, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2722717
    iget v0, p0, LX/1vt;->c:I

    .line 2722718
    move v0, v0

    .line 2722719
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2722714
    iget v0, p0, LX/1vt;->c:I

    .line 2722715
    move v0, v0

    .line 2722716
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2722711
    iget v0, p0, LX/1vt;->b:I

    .line 2722712
    move v0, v0

    .line 2722713
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2722708
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2722709
    move-object v0, v0

    .line 2722710
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2722699
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2722700
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2722701
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2722702
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2722703
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2722704
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2722705
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2722706
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/welcomepage/graphql/WelcomePageQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2722707
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2722696
    iget v0, p0, LX/1vt;->c:I

    .line 2722697
    move v0, v0

    .line 2722698
    return v0
.end method
