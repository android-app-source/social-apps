.class public Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""

# interfaces
.implements LX/Jew;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Landroid/net/Uri;

.field private final l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

.field private final m:Lcom/facebook/user/model/User;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2721726
    new-instance v0, LX/JfH;

    invoke-direct {v0}, LX/JfH;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2721693
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2721694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->a:Ljava/lang/String;

    .line 2721695
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->b:Ljava/lang/String;

    .line 2721696
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->c:Ljava/lang/String;

    .line 2721697
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->j:Ljava/lang/String;

    .line 2721698
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->k:Landroid/net/Uri;

    .line 2721699
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2721700
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->m:Lcom/facebook/user/model/User;

    .line 2721701
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/user/model/User;)V
    .locals 2

    .prologue
    .line 2721702
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2721703
    invoke-virtual {p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v0

    .line 2721704
    invoke-virtual {p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->a:Ljava/lang/String;

    .line 2721705
    invoke-virtual {p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->b:Ljava/lang/String;

    .line 2721706
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->c:Ljava/lang/String;

    .line 2721707
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->j:Ljava/lang/String;

    .line 2721708
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->k:Landroid/net/Uri;

    .line 2721709
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->j()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v0

    invoke-static {v0}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2721710
    iput-object p4, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->m:Lcom/facebook/user/model/User;

    .line 2721711
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2721712
    sget-object v0, LX/DfY;->V2_MESSENGER_ADS_CLASSIC_UNIT:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2721713
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2721714
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721715
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721716
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721717
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721718
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->k:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2721719
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2721720
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->m:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2721721
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2721722
    sget-object v0, LX/Dfa;->V2_MESSENGER_ADS_CLASSIC_UNIT:LX/Dfa;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2721723
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 2721724
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->b:Ljava/lang/String;

    invoke-static {v0}, LX/1bi;->a(Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2721725
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
