.class public Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Jey;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JfG;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/common/callercontext/CallerContext;

.field public c:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/Jex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jex",
            "<",
            "Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Lcom/facebook/widget/text/BetterTextView;

.field private i:Lcom/facebook/widget/text/BetterTextView;

.field private j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2721760
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721761
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721758
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721759
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721739
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721740
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2721741
    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a:LX/0Ot;

    .line 2721742
    invoke-direct {p0}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a()V

    .line 2721743
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2721748
    const-class v0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2721749
    const v0, 0x7f030b1a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2721750
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2721751
    const v0, 0x7f0d1bf3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2721752
    const v0, 0x7f0d1bf4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2721753
    const v0, 0x7f0d1bf5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2721754
    const v0, 0x7f0d1bf6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2721755
    const v0, 0x7f0d1bf8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2721756
    invoke-direct {p0}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->b()V

    .line 2721757
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;

    const/16 v1, 0x269c

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->a:LX/0Ot;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2721745
    const v0, 0x7f0d1bf7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2721746
    new-instance v1, LX/JfJ;

    invoke-direct {v1, p0}, LX/JfJ;-><init>(Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2721747
    return-void
.end method


# virtual methods
.method public getInboxUnitItem()Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
    .locals 1

    .prologue
    .line 2721744
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->c:Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;

    return-object v0
.end method

.method public setFragmentManager(LX/0gc;)V
    .locals 0

    .prologue
    .line 2721737
    iput-object p1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->e:LX/0gc;

    .line 2721738
    return-void
.end method

.method public setListener(LX/Jex;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jex",
            "<",
            "Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2721735
    iput-object p1, p0, Lcom/facebook/messaging/business/inboxads/singleitem/MessengerInboxClassicAdsUnitView;->d:LX/Jex;

    .line 2721736
    return-void
.end method
