.class public Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2721472
    new-instance v0, LX/Jf5;

    invoke-direct {v0}, LX/Jf5;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2721480
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2721481
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2721482
    const-class v1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2721483
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;->a:LX/0Px;

    .line 2721484
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2721477
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)V

    .line 2721478
    iput-object p2, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;->a:LX/0Px;

    .line 2721479
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2721485
    sget-object v0, LX/DfY;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2721474
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2721475
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2721476
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2721473
    sget-object v0, LX/Dfa;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/Dfa;

    return-object v0
.end method
