.class public Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;
.super LX/Jf4;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JfG;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/Jf6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private i:Lcom/facebook/widget/text/BetterTextView;

.field private j:Lcom/facebook/widget/text/BetterTextView;

.field private k:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2721641
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721642
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721639
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721640
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721634
    invoke-direct {p0, p1, p2, p3}, LX/Jf4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721635
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2721636
    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->b:LX/0Ot;

    .line 2721637
    invoke-direct {p0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->a()V

    .line 2721638
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2721585
    const-class v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2721586
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030b1b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2721587
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2721588
    const v0, 0x7f0d1bf9

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2721589
    const v0, 0x7f0d1bfa

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2721590
    const v0, 0x7f0d1bfb

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2721591
    const v0, 0x7f0d1bfd

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2721592
    const v0, 0x7f0d1bfe

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2721593
    invoke-direct {p0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->b()V

    .line 2721594
    new-instance v0, LX/JfB;

    invoke-direct {v0, p0}, LX/JfB;-><init>(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2721595
    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;Landroid/view/LayoutInflater;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;",
            "Landroid/view/LayoutInflater;",
            "LX/0Ot",
            "<",
            "LX/JfG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2721633
    iput-object p1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->a:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->b:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/16 v2, 0x269c

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->a(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;Landroid/view/LayoutInflater;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2721630
    const v0, 0x7f0d1bfc

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2721631
    new-instance v1, LX/JfE;

    invoke-direct {v1, p0}, LX/JfE;-><init>(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2721632
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2721601
    instance-of v1, p1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2721602
    check-cast p1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    iput-object p1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721603
    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721604
    iget-object v2, v1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->j:Lcom/facebook/user/model/User;

    move-object v1, v2

    .line 2721605
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721606
    iget-object v2, v1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->j:Lcom/facebook/user/model/User;

    move-object v1, v2

    .line 2721607
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v1

    .line 2721608
    :goto_0
    if-eqz v1, :cond_0

    .line 2721609
    invoke-virtual {p0}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2657

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    .line 2721610
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2721611
    iget-object v0, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2721612
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2721613
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->i:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721614
    iget-object v2, v1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2721615
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2721616
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->j:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721617
    iget-object v2, v1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2721618
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2721619
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721620
    iget-object v2, v1, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->c:Landroid/net/Uri;

    move-object v1, v2

    .line 2721621
    iget-object v2, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2721622
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    .line 2721623
    iget-object v1, v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-object v0, v1

    .line 2721624
    if-eqz v0, :cond_2

    .line 2721625
    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->k:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2721626
    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2721627
    :goto_1
    return-void

    :cond_1
    move-object v1, v0

    .line 2721628
    goto :goto_0

    .line 2721629
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->k:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public getInboxUnitItem()Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
    .locals 1

    .prologue
    .line 2721600
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->d:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;

    return-object v0
.end method

.method public setFragmentManager(LX/0gc;)V
    .locals 0

    .prologue
    .line 2721598
    iput-object p1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->f:LX/0gc;

    .line 2721599
    return-void
.end method

.method public setListener(LX/Jf6;)V
    .locals 0

    .prologue
    .line 2721596
    iput-object p1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdsView;->e:LX/Jf6;

    .line 2721597
    return-void
.end method
