.class public Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;
.super Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2721564
    new-instance v0, LX/JfA;

    invoke-direct {v0}, LX/JfA;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2721547
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;-><init>(Landroid/os/Parcel;)V

    .line 2721548
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->a:Ljava/lang/String;

    .line 2721549
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->b:Ljava/lang/String;

    .line 2721550
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->c:Landroid/net/Uri;

    .line 2721551
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/user/model/User;)V
    .locals 6

    .prologue
    .line 2721558
    sget-object v3, LX/Jf3;->SINGLE_IMAGE:LX/Jf3;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/Jf3;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/user/model/User;)V

    .line 2721559
    invoke-virtual {p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v0

    .line 2721560
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->a:Ljava/lang/String;

    .line 2721561
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->b:Ljava/lang/String;

    .line 2721562
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->c:Landroid/net/Uri;

    .line 2721563
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2721565
    sget-object v0, LX/DfY;->V2_MESSENGER_ADS_SINGLE_IMAGE:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2721553
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->a(Landroid/os/Parcel;I)V

    .line 2721554
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721555
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721556
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxSingleImageAdItem;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2721557
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2721552
    sget-object v0, LX/Dfa;->V2_MESSENGER_ADS_SINGLE_IMAGE_ITEM:LX/Dfa;

    return-object v0
.end method
