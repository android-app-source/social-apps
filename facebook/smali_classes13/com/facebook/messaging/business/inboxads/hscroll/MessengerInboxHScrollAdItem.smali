.class public abstract Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""

# interfaces
.implements LX/Jew;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/Jf3;

.field private final c:Ljava/lang/String;

.field public final j:Lcom/facebook/user/model/User;

.field public final k:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2721438
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2721439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->a:Ljava/lang/String;

    .line 2721440
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Jf3;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->b:LX/Jf3;

    .line 2721441
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->c:Ljava/lang/String;

    .line 2721442
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->j:Lcom/facebook/user/model/User;

    .line 2721443
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2721444
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/Jf3;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;Lcom/facebook/user/model/User;)V
    .locals 2

    .prologue
    .line 2721445
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2721446
    invoke-virtual {p4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->l()Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;

    move-result-object v0

    .line 2721447
    invoke-virtual {p4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->a:Ljava/lang/String;

    .line 2721448
    iput-object p3, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->b:LX/Jf3;

    .line 2721449
    invoke-virtual {p4}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->c:Ljava/lang/String;

    .line 2721450
    iput-object p5, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->j:Lcom/facebook/user/model/User;

    .line 2721451
    invoke-virtual {v0}, Lcom/facebook/messaging/business/inboxads/graphql/InboxAdsQueryFragmentsModels$MessengerInboxAdDisplayInfoFragmentModel;->j()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v0

    invoke-static {v0}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2721452
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2721453
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2721454
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721455
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->b:LX/Jf3;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2721456
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2721457
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->j:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2721458
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2721459
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2721460
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 2721461
    iget-object v0, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->c:Ljava/lang/String;

    invoke-static {v0}, LX/1bi;->a(Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2721462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
