.class public Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/Jft;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/JgG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Jg1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Jg3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/JgM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:LX/Jfs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jfs",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2722686
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2722687
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;

    const-class v2, LX/Jft;

    invoke-interface {v6, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Jft;

    new-instance v3, LX/JgG;

    invoke-direct {v3}, LX/JgG;-><init>()V

    move-object v3, v3

    move-object v3, v3

    check-cast v3, LX/JgG;

    new-instance v0, LX/Jg1;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-direct {v0, v4, v5, p1}, LX/Jg1;-><init>(LX/0tX;LX/1Ck;LX/03V;)V

    move-object v4, v0

    check-cast v4, LX/Jg1;

    new-instance v0, LX/Jg3;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-direct {v0, v5, p0, p1}, LX/Jg3;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    move-object v5, v0

    check-cast v5, LX/Jg3;

    new-instance p0, LX/JgM;

    const/16 p1, 0x26d7

    invoke-static {v6, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-direct {p0, p1}, LX/JgM;-><init>(LX/0Or;)V

    move-object v6, p0

    check-cast v6, LX/JgM;

    iput-object v2, v1, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->a:LX/Jft;

    iput-object v3, v1, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->b:LX/JgG;

    iput-object v4, v1, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->c:LX/Jg1;

    iput-object v5, v1, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->d:LX/Jg3;

    iput-object v6, v1, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->e:LX/JgM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2722685
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2722673
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2722674
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2722675
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2722676
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2722677
    if-eqz p1, :cond_1

    .line 2722678
    const-string v0, "arg_station_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->f:Ljava/lang/String;

    .line 2722679
    const-string v0, "arg_station_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->g:Ljava/lang/String;

    .line 2722680
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2722681
    return-void

    .line 2722682
    :cond_1
    if-eqz v0, :cond_0

    .line 2722683
    const-string v1, "arg_station_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->f:Ljava/lang/String;

    .line 2722684
    const-string v1, "arg_station_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2722688
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "arg_station_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->f:Ljava/lang/String;

    .line 2722689
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "arg_station_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->g:Ljava/lang/String;

    .line 2722690
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2722691
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2722692
    return-void

    :cond_0
    move v0, v2

    .line 2722693
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2722694
    goto :goto_1
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 10

    .prologue
    .line 2722657
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2722658
    const v0, 0x7f11000d

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2722659
    const v0, 0x7f0d3212

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 2722660
    const v0, 0x7f0d1a99

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/RecyclerView;

    .line 2722661
    const v0, 0x7f0d1a98

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    .line 2722662
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->a:LX/Jft;

    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->b:LX/JgG;

    iget-object v2, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->c:LX/Jg1;

    iget-object v3, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->d:LX/Jg3;

    iget-object v4, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->e:LX/JgM;

    iget-object v8, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->f:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, LX/Jft;->a(LX/Jfu;LX/Jfy;LX/Jg3;LX/JgM;Landroid/support/v7/widget/RecyclerView;Landroid/widget/ProgressBar;Landroid/view/MenuItem;Ljava/lang/String;LX/0gc;)LX/Jfs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->h:LX/Jfs;

    .line 2722663
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x17f3aa2a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2722664
    const v1, 0x7f030a75

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6521bca7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2990092d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2722665
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroy()V

    .line 2722666
    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->h:LX/Jfs;

    .line 2722667
    iget-object v2, v1, LX/Jfs;->h:LX/Jfy;

    invoke-interface {v2}, LX/Jfy;->a()V

    .line 2722668
    const/16 v1, 0x2b

    const v2, 0x11dc04c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2722669
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2722670
    const-string v0, "arg_station_id"

    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722671
    const-string v0, "arg_station_name"

    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/manage/substations/ManageSubstationsFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722672
    return-void
.end method
