.class public Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2722576
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2722577
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->a()V

    .line 2722578
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2722579
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2722580
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->a()V

    .line 2722581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2722582
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2722583
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->a()V

    .line 2722584
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2722585
    const v0, 0x7f03140d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2722586
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2722587
    const v0, 0x7f0d1bdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->b:Landroid/widget/TextView;

    .line 2722588
    const v0, 0x7f0d2e11

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->c:Landroid/widget/TextView;

    .line 2722589
    return-void
.end method


# virtual methods
.method public setDescriptionView(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2722590
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2722591
    return-void
.end method

.method public setImageView(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2722592
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2722593
    return-void
.end method

.method public setNameView(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2722594
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManagePublisherItemView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2722595
    return-void
.end method
