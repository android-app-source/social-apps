.class public Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfoDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final isDeactivatedUser:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_messenger_only_deactivated_user"
    .end annotation
.end field

.field public final matchingFacebookUserDisplayName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matching_facebook_user_display_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final matchingFacebookUserId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matching_facebook_user_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final matchingFacebookUserPicSquare:Lcom/facebook/user/model/PicSquare;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matching_facebook_user_pic_square_json_string"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final viewerUserId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "viewer_user_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2745844
    const-class v0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfoDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2745845
    new-instance v0, LX/Jsp;

    invoke-direct {v0}, LX/Jsp;-><init>()V

    sput-object v0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2745846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745847
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->isDeactivatedUser:Z

    .line 2745848
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->viewerUserId:Ljava/lang/String;

    .line 2745849
    iput-object v1, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserId:Ljava/lang/String;

    .line 2745850
    iput-object v1, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserDisplayName:Ljava/lang/String;

    .line 2745851
    iput-object v1, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserPicSquare:Lcom/facebook/user/model/PicSquare;

    .line 2745852
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2745853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745854
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->isDeactivatedUser:Z

    .line 2745855
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->viewerUserId:Ljava/lang/String;

    .line 2745856
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserId:Ljava/lang/String;

    .line 2745857
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserDisplayName:Ljava/lang/String;

    .line 2745858
    const-class v0, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquare;

    iput-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserPicSquare:Lcom/facebook/user/model/PicSquare;

    .line 2745859
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/user/model/PicSquare;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/user/model/PicSquare;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745861
    iput-boolean p1, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->isDeactivatedUser:Z

    .line 2745862
    iput-object p2, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->viewerUserId:Ljava/lang/String;

    .line 2745863
    iput-object p3, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserId:Ljava/lang/String;

    .line 2745864
    iput-object p4, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserDisplayName:Ljava/lang/String;

    .line 2745865
    iput-object p5, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserPicSquare:Lcom/facebook/user/model/PicSquare;

    .line 2745866
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2745867
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2745868
    iget-boolean v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->isDeactivatedUser:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2745869
    iget-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->viewerUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2745870
    iget-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2745871
    iget-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2745872
    iget-object v0, p0, Lcom/facebook/messaging/zombification/model/PhoneReconfirmationInfo;->matchingFacebookUserPicSquare:Lcom/facebook/user/model/PicSquare;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2745873
    return-void
.end method
