.class public final Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/share/model/ShareItem;

.field public final b:Landroid/os/Bundle;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2736390
    new-instance v0, LX/Joy;

    invoke-direct {v0}, LX/Joy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2736385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736386
    const-class v0, Lcom/facebook/share/model/ShareItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ShareItem;

    iput-object v0, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->a:Lcom/facebook/share/model/ShareItem;

    .line 2736387
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->b:Landroid/os/Bundle;

    .line 2736388
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->c:Ljava/lang/String;

    .line 2736389
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2736384
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2736380
    iget-object v0, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->a:Lcom/facebook/share/model/ShareItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2736381
    iget-object v0, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2736382
    iget-object v0, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2736383
    return-void
.end method
