.class public final Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/share/model/ShareItem;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2736375
    new-instance v0, LX/Jox;

    invoke-direct {v0}, LX/Jox;-><init>()V

    sput-object v0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2736369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736370
    const-class v0, Lcom/facebook/share/model/ShareItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ShareItem;

    iput-object v0, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;->a:Lcom/facebook/share/model/ShareItem;

    .line 2736371
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2736374
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2736372
    iget-object v0, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;->a:Lcom/facebook/share/model/ShareItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2736373
    return-void
.end method
