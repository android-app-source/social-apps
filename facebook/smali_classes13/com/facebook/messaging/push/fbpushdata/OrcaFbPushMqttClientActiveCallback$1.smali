.class public final Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2XO;


# direct methods
.method public constructor <init>(LX/2XO;)V
    .locals 0

    .prologue
    .line 2736980
    iput-object p1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$1;->a:LX/2XO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2736981
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$1;->a:LX/2XO;

    .line 2736982
    iget-object v1, v0, LX/2XO;->c:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 2736983
    iget-object v1, v0, LX/2XO;->c:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2736984
    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/2XO;->a(LX/2XO;Z)V

    .line 2736985
    iget-object v1, v0, LX/2XO;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$2;

    invoke-direct {v2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$2;-><init>(LX/2XO;)V

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, v0, LX/2XO;->c:Ljava/util/concurrent/ScheduledFuture;

    .line 2736986
    return-void
.end method
