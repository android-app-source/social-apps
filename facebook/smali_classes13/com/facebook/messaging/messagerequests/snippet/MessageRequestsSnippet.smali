.class public Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733998
    new-instance v0, LX/JnQ;

    invoke-direct {v0}, LX/JnQ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;ILX/0Px;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2733999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734000
    iput p1, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->a:I

    .line 2734001
    iput p2, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->b:I

    .line 2734002
    iput-object p3, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->c:Ljava/lang/String;

    .line 2734003
    iput p4, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->d:I

    .line 2734004
    iput-object p5, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->e:LX/0Px;

    .line 2734005
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2734006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734007
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->a:I

    .line 2734008
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->b:I

    .line 2734009
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->c:Ljava/lang/String;

    .line 2734010
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->d:I

    .line 2734011
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->e:LX/0Px;

    .line 2734012
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2734013
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2734014
    iget v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2734015
    iget v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2734016
    iget-object v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2734017
    iget v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2734018
    iget-object v0, p0, Lcom/facebook/messaging/messagerequests/snippet/MessageRequestsSnippet;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2734019
    return-void
.end method
