.class public Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2736358
    new-instance v0, LX/Jov;

    invoke-direct {v0}, LX/Jov;-><init>()V

    sput-object v0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2736353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736354
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->a:Ljava/lang/String;

    .line 2736355
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->b:Ljava/lang/String;

    .line 2736356
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->c:Ljava/lang/String;

    .line 2736357
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2736348
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2736349
    iget-object v0, p0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2736350
    iget-object v0, p0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2736351
    iget-object v0, p0, Lcom/facebook/messaging/phoneconfirmation/prefs/PhoneNumberParam;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2736352
    return-void
.end method
