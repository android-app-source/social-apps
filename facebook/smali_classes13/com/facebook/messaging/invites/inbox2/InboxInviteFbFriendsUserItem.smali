.class public Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733876
    new-instance v0, LX/JnD;

    invoke-direct {v0}, LX/JnD;-><init>()V

    sput-object v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2733870
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2733871
    const-class v0, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    .line 2733872
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)V
    .locals 0

    .prologue
    .line 2733873
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2733874
    iput-object p3, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    .line 2733875
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2733869
    sget-object v0, LX/DfY;->V2_INVITE_FB_FRIENDS_ITEM:LX/DfY;

    return-object v0
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2733868
    sget-object v0, LX/Dfa;->V2_INVITE_FB_FRIENDS_ITEM:LX/Dfa;

    return-object v0
.end method
