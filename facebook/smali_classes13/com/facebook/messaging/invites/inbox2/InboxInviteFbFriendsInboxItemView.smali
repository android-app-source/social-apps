.class public Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;
.super Landroid/support/v7/widget/CardView;
.source ""


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Jn1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/facebook/user/tiles/UserTileView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/facebook/resources/ui/FbImageButton;

.field private h:LX/Jn7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2733802
    invoke-direct {p0, p1}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 2733803
    sget-object v0, LX/Jn7;->NOT_INVITED:LX/Jn7;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    .line 2733804
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a()V

    .line 2733805
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2733798
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2733799
    sget-object v0, LX/Jn7;->NOT_INVITED:LX/Jn7;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    .line 2733800
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a()V

    .line 2733801
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2733794
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733795
    sget-object v0, LX/Jn7;->NOT_INVITED:LX/Jn7;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    .line 2733796
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a()V

    .line 2733797
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2733783
    const-class v0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733784
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030994

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2733785
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/CardView;->setUseCompatPadding(Z)V

    .line 2733786
    const v0, 0x7f0d037b

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->c:Landroid/widget/TextView;

    .line 2733787
    const v0, 0x7f0d08c1

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->d:Landroid/widget/TextView;

    .line 2733788
    const v0, 0x7f0d0575

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->e:Lcom/facebook/user/tiles/UserTileView;

    .line 2733789
    const v0, 0x7f0d186f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->f:Landroid/widget/TextView;

    .line 2733790
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->f:Landroid/widget/TextView;

    new-instance v1, LX/Jn5;

    invoke-direct {v1, p0}, LX/Jn5;-><init>(Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733791
    const v0, 0x7f0d0b2c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    .line 2733792
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    new-instance v1, LX/Jn6;

    invoke-direct {v1, p0}, LX/Jn6;-><init>(Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733793
    return-void
.end method

.method private a(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2733768
    iget-object v0, p1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733769
    iget-object v1, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2733770
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->ax()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2733771
    iget-object v1, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->e:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0}, LX/8t9;->b(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2733772
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v1

    .line 2733773
    iget-object v2, v1, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2733774
    if-eqz v1, :cond_0

    .line 2733775
    iget-object v1, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    .line 2733776
    iget-object v2, v0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2733777
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2733778
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2733779
    :goto_0
    return-void

    .line 2733780
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2733781
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->e:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2733782
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a:Landroid/view/LayoutInflater;

    return-void
.end method

.method public static b(Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;)V
    .locals 1

    .prologue
    .line 2733765
    sget-object v0, LX/Jn7;->INVITED:LX/Jn7;

    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    .line 2733766
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->c()V

    .line 2733767
    return-void
.end method

.method private c()V
    .locals 0

    .prologue
    .line 2733762
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->d()V

    .line 2733763
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->e()V

    .line 2733764
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2733756
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    sget-object v1, LX/Jn7;->NOT_INVITED:LX/Jn7;

    if-ne v0, v1, :cond_0

    .line 2733757
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->f:Landroid/widget/TextView;

    const v1, 0x7f080685

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2733758
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->f:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2733759
    :goto_0
    return-void

    .line 2733760
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->f:Landroid/widget/TextView;

    const v1, 0x7f080686

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2733761
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2733752
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    sget-object v1, LX/Jn7;->NOT_INVITED:LX/Jn7;

    if-ne v0, v1, :cond_0

    .line 2733753
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    .line 2733754
    :goto_0
    return-void

    .line 2733755
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;Z)V
    .locals 1

    .prologue
    .line 2733747
    if-eqz p2, :cond_0

    sget-object v0, LX/Jn7;->INVITED:LX/Jn7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->h:LX/Jn7;

    .line 2733748
    invoke-direct {p0, p1}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->a(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)V

    .line 2733749
    invoke-direct {p0}, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->c()V

    .line 2733750
    return-void

    .line 2733751
    :cond_0
    sget-object v0, LX/Jn7;->NOT_INVITED:LX/Jn7;

    goto :goto_0
.end method

.method public setListener(LX/Jn1;)V
    .locals 0

    .prologue
    .line 2733745
    iput-object p1, p0, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsInboxItemView;->b:LX/Jn1;

    .line 2733746
    return-void
.end method
