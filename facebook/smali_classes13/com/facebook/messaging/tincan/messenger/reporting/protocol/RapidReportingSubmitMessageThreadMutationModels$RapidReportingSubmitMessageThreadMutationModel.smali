.class public final Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3f28b6ef
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2745071
    const-class v0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2745072
    const-class v0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2745073
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2745074
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2745075
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;->e:Ljava/lang/String;

    .line 2745076
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2745077
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2745078
    invoke-direct {p0}, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2745079
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2745080
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2745081
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2745082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2745083
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2745084
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2745085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2745086
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2745087
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2745088
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;->f:I

    .line 2745089
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2745090
    new-instance v0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;

    invoke-direct {v0}, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;-><init>()V

    .line 2745091
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2745092
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2745093
    const v0, -0x3e7e2953

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2745094
    const v0, 0x61174da

    return v0
.end method
