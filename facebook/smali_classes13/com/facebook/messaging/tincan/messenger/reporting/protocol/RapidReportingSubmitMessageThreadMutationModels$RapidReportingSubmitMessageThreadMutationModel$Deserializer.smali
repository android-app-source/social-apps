.class public final Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2745024
    const-class v0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2745025
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2745026
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2745027
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2745028
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2745029
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_6

    .line 2745030
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2745031
    :goto_0
    move v1, v2

    .line 2745032
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2745033
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2745034
    new-instance v1, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;

    invoke-direct {v1}, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;-><init>()V

    .line 2745035
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2745036
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2745037
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2745038
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2745039
    :cond_0
    return-object v1

    .line 2745040
    :cond_1
    const-string p0, "srt_job_id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2745041
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v4, v1

    move v1, v3

    .line 2745042
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_4

    .line 2745043
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2745044
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2745045
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v6, :cond_2

    .line 2745046
    const-string p0, "client_mutation_id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2745047
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2745048
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2745049
    :cond_4
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2745050
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2745051
    if-eqz v1, :cond_5

    .line 2745052
    invoke-virtual {v0, v3, v4, v2}, LX/186;->a(III)V

    .line 2745053
    :cond_5
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_6
    move v1, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
