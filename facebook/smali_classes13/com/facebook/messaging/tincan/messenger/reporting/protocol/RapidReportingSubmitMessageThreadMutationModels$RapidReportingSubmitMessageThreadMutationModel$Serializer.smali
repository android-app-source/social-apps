.class public final Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2745069
    const-class v0, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2745070
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2745055
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2745056
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2745057
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 2745058
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2745059
    invoke-virtual {v1, v0, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2745060
    if-eqz v2, :cond_0

    .line 2745061
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2745062
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2745063
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 2745064
    if-eqz v2, :cond_1

    .line 2745065
    const-string p0, "srt_job_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2745066
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2745067
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2745068
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2745054
    check-cast p1, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel$Serializer;->a(Lcom/facebook/messaging/tincan/messenger/reporting/protocol/RapidReportingSubmitMessageThreadMutationModels$RapidReportingSubmitMessageThreadMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
