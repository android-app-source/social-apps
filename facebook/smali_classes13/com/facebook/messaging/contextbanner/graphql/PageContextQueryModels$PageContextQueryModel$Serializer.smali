.class public final Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2726644
    const-class v0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;

    new-instance v1, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2726645
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2726646
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2726647
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2726648
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2726649
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2726650
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2726651
    if-eqz v2, :cond_0

    .line 2726652
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726653
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2726654
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2726655
    if-eqz v2, :cond_1

    .line 2726656
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726657
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2726658
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2726659
    if-eqz v2, :cond_2

    .line 2726660
    const-string v3, "is_verified_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726661
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2726662
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2726663
    if-eqz v2, :cond_8

    .line 2726664
    const-string v3, "messenger_context_banner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726665
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2726666
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2726667
    if-eqz v3, :cond_5

    .line 2726668
    const-string v4, "subtitles"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726669
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2726670
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 2726671
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2726672
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2726673
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2726674
    if-eqz p0, :cond_3

    .line 2726675
    const-string p2, "text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726676
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2726677
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2726678
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2726679
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2726680
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2726681
    if-eqz v3, :cond_7

    .line 2726682
    const-string v4, "title"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726683
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2726684
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2726685
    if-eqz v4, :cond_6

    .line 2726686
    const-string v5, "text"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726687
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2726688
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2726689
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2726690
    :cond_8
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2726691
    if-eqz v2, :cond_9

    .line 2726692
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726693
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2726694
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2726695
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2726696
    check-cast p1, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Serializer;->a(Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
