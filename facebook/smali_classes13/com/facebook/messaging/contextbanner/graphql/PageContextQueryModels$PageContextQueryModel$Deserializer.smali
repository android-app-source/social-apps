.class public final Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2726602
    const-class v0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;

    new-instance v1, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2726603
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2726604
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2726605
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2726606
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2726607
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 2726608
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2726609
    :goto_0
    move v1, v2

    .line 2726610
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2726611
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2726612
    new-instance v1, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$PageContextQueryModel;-><init>()V

    .line 2726613
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2726614
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2726615
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2726616
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2726617
    :cond_0
    return-object v1

    .line 2726618
    :cond_1
    const-string p0, "is_verified"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2726619
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v3

    .line 2726620
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_7

    .line 2726621
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2726622
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2726623
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 2726624
    const-string p0, "id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2726625
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2726626
    :cond_3
    const-string p0, "is_verified_page"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2726627
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_1

    .line 2726628
    :cond_4
    const-string p0, "messenger_context_banner"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2726629
    invoke-static {p1, v0}, LX/Jiq;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2726630
    :cond_5
    const-string p0, "name"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2726631
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2726632
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2726633
    :cond_7
    const/4 v10, 0x5

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2726634
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2726635
    if-eqz v4, :cond_8

    .line 2726636
    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 2726637
    :cond_8
    if-eqz v1, :cond_9

    .line 2726638
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 2726639
    :cond_9
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2726640
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2726641
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
