.class public final Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2726797
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2726798
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2726832
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2726833
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2726834
    if-nez p1, :cond_0

    .line 2726835
    :goto_0
    return v0

    .line 2726836
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2726837
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2726838
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2726839
    const v2, -0x15907c16

    const/4 v5, 0x0

    .line 2726840
    if-nez v1, :cond_1

    move v3, v5

    .line 2726841
    :goto_1
    move v1, v3

    .line 2726842
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2726843
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2726844
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2726845
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v1

    .line 2726846
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2726847
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2726848
    const v3, 0x1ec23282

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2726849
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2726850
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2726851
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2726852
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2726853
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2726854
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2726855
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2726856
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2726857
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2726858
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 2726859
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 2726860
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2726861
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2726862
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 2726863
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2726864
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 2726865
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x15907c16 -> :sswitch_1
        0x1ec23282 -> :sswitch_2
        0x67b0e476 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2726866
    new-instance v0, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2726874
    sparse-switch p2, :sswitch_data_0

    .line 2726875
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2726876
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2726877
    const v1, -0x15907c16

    .line 2726878
    if-eqz v0, :cond_0

    .line 2726879
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2726880
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2726881
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2726882
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2726883
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2726884
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2726885
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2726886
    const v1, 0x1ec23282

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x15907c16 -> :sswitch_2
        0x1ec23282 -> :sswitch_1
        0x67b0e476 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2726867
    if-eqz p1, :cond_0

    .line 2726868
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2726869
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;

    .line 2726870
    if-eq v0, v1, :cond_0

    .line 2726871
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2726872
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2726873
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2726825
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2726826
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2726827
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2726828
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2726829
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2726830
    iput p2, p0, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->b:I

    .line 2726831
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2726824
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2726823
    new-instance v0, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2726820
    iget v0, p0, LX/1vt;->c:I

    .line 2726821
    move v0, v0

    .line 2726822
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2726817
    iget v0, p0, LX/1vt;->c:I

    .line 2726818
    move v0, v0

    .line 2726819
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2726814
    iget v0, p0, LX/1vt;->b:I

    .line 2726815
    move v0, v0

    .line 2726816
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2726811
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2726812
    move-object v0, v0

    .line 2726813
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2726802
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2726803
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2726804
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2726805
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2726806
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2726807
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2726808
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2726809
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2726810
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2726799
    iget v0, p0, LX/1vt;->c:I

    .line 2726800
    move v0, v0

    .line 2726801
    return v0
.end method
