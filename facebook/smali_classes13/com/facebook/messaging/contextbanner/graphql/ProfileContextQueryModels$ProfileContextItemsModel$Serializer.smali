.class public final Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2726933
    const-class v0, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel;

    new-instance v1, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2726934
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2726932
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2726918
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2726919
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2726920
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2726921
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2726922
    if-eqz v2, :cond_0

    .line 2726923
    const-string p0, "specific_context_items"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726924
    invoke-static {v1, v2, p1, p2}, LX/Jiu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2726925
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2726926
    if-eqz v2, :cond_1

    .line 2726927
    const-string p0, "top_context_item"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2726928
    invoke-static {v1, v2, p1, p2}, LX/Jiu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2726929
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2726930
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2726931
    check-cast p1, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel$Serializer;->a(Lcom/facebook/messaging/contextbanner/graphql/ProfileContextQueryModels$ProfileContextItemsModel;LX/0nX;LX/0my;)V

    return-void
.end method
