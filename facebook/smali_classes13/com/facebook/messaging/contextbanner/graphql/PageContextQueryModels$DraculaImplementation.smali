.class public final Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2726595
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2726596
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2726593
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2726594
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2726561
    if-nez p1, :cond_0

    .line 2726562
    :goto_0
    return v0

    .line 2726563
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2726564
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2726565
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2726566
    const v2, -0x69fd1492

    const/4 v5, 0x0

    .line 2726567
    if-nez v1, :cond_1

    move v3, v5

    .line 2726568
    :goto_1
    move v1, v3

    .line 2726569
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2726570
    const v3, 0x725c9e44

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2726571
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2726572
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2726573
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2726574
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2726575
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2726576
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2726577
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2726578
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2726579
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2726580
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2726581
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2726582
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2726583
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2726584
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2726585
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v6

    .line 2726586
    if-nez v6, :cond_2

    const/4 v3, 0x0

    .line 2726587
    :goto_2
    if-ge v5, v6, :cond_3

    .line 2726588
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2726589
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 2726590
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2726591
    :cond_2
    new-array v3, v6, [I

    goto :goto_2

    .line 2726592
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69fd1492 -> :sswitch_1
        0x4cd7396a -> :sswitch_0
        0x725c9e44 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2726560
    new-instance v0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    .line 2726547
    sparse-switch p2, :sswitch_data_0

    .line 2726548
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2726549
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2726550
    const v1, -0x69fd1492

    .line 2726551
    if-eqz v0, :cond_0

    .line 2726552
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v3

    .line 2726553
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 2726554
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2726555
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2726556
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2726557
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2726558
    const v1, 0x725c9e44

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2726559
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x69fd1492 -> :sswitch_1
        0x4cd7396a -> :sswitch_0
        0x725c9e44 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2726541
    if-eqz p1, :cond_0

    .line 2726542
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2726543
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;

    .line 2726544
    if-eq v0, v1, :cond_0

    .line 2726545
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2726546
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2726540
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2726538
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2726539
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2726597
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2726598
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2726599
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2726600
    iput p2, p0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->b:I

    .line 2726601
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2726537
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2726536
    new-instance v0, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2726533
    iget v0, p0, LX/1vt;->c:I

    .line 2726534
    move v0, v0

    .line 2726535
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2726512
    iget v0, p0, LX/1vt;->c:I

    .line 2726513
    move v0, v0

    .line 2726514
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2726530
    iget v0, p0, LX/1vt;->b:I

    .line 2726531
    move v0, v0

    .line 2726532
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2726527
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2726528
    move-object v0, v0

    .line 2726529
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2726518
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2726519
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2726520
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2726521
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2726522
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2726523
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2726524
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2726525
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/contextbanner/graphql/PageContextQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2726526
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2726515
    iget v0, p0, LX/1vt;->c:I

    .line 2726516
    move v0, v0

    .line 2726517
    return v0
.end method
