.class public Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/Jnx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Jnt;

.field public d:Z

.field public e:Lcom/facebook/messaging/montage/model/art/BaseItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/Jns;

.field private final g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734630
    const-class v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2734631
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2734632
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734633
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2734634
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2734635
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2734636
    const-class v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2734637
    const v0, 0x7f030116

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2734638
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2698

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2734639
    new-instance v0, LX/Jns;

    invoke-direct {v0, p0}, LX/Jns;-><init>(Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;)V

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->f:LX/Jns;

    .line 2734640
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2734641
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->f:LX/Jns;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2734642
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-direct {v1, v4, v4}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2734643
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b269d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2734644
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/62a;

    invoke-direct {v2, v0, v0}, LX/62a;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2734645
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    invoke-static {v0}, LX/Jnx;->a(LX/0QB;)LX/Jnx;

    move-result-object v0

    check-cast v0, LX/Jnx;

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    return-void
.end method


# virtual methods
.method public setListener(LX/Jnt;)V
    .locals 0

    .prologue
    .line 2734646
    iput-object p1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->c:LX/Jnt;

    .line 2734647
    return-void
.end method

.method public setRecycledViewPool(LX/1YF;)V
    .locals 1

    .prologue
    .line 2734648
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setRecycledViewPool(LX/1YF;)V

    .line 2734649
    return-void
.end method

.method public setSelectedItem(Lcom/facebook/messaging/montage/model/art/BaseItem;)V
    .locals 3
    .param p1    # Lcom/facebook/messaging/montage/model/art/BaseItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734650
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->e:Lcom/facebook/messaging/montage/model/art/BaseItem;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2734651
    :cond_0
    :goto_0
    return-void

    .line 2734652
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->e:Lcom/facebook/messaging/montage/model/art/BaseItem;

    .line 2734653
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->f:LX/Jns;

    iget-object v1, v1, LX/Jns;->e:LX/Dhc;

    iget-object v1, v1, LX/Dhc;->b:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 2734654
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 2734655
    iput-object p1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->e:Lcom/facebook/messaging/montage/model/art/BaseItem;

    .line 2734656
    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->f:LX/Jns;

    invoke-virtual {v2, v1}, LX/1OM;->i_(I)V

    .line 2734657
    :goto_1
    if-eqz v0, :cond_0

    .line 2734658
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->f:LX/Jns;

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->f:LX/Jns;

    iget-object v2, v2, LX/Jns;->e:LX/Dhc;

    iget-object v2, v2, LX/Dhc;->b:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v1, v0}, LX/1OM;->i_(I)V

    goto :goto_0

    .line 2734659
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->e:Lcom/facebook/messaging/montage/model/art/BaseItem;

    goto :goto_1
.end method
