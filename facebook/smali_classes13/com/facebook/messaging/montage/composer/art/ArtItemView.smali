.class public Lcom/facebook/messaging/montage/composer/art/ArtItemView;
.super LX/4oJ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/IqF;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/photos/editing/LayerGroupLayout;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/IqE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IqH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/8mv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/Irs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/messaging/montage/model/art/ArtItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/Dhc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/Ird;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/Jnn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734496
    const-class v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a:Ljava/lang/String;

    .line 2734497
    const-class v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2734494
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2734495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2734492
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2734493
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2734472
    invoke-direct {p0, p1, p2, p3}, LX/4oJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2734473
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734474
    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->i:LX/0Ot;

    .line 2734475
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734476
    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->j:LX/0Ot;

    .line 2734477
    iput-boolean v4, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->q:Z

    .line 2734478
    const-class v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2734479
    const v0, 0x7f030112

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2734480
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2734481
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b269a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2699

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2734482
    :cond_0
    const v0, 0x7f0d05b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->c:LX/4ob;

    .line 2734483
    const v0, 0x7f0d05b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->d:LX/4ob;

    .line 2734484
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    .line 2734485
    iput-object p0, v0, LX/IqH;->d:LX/IqF;

    .line 2734486
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 2734487
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b269b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 2734488
    invoke-static {p0, v0, v0, v0, v0}, LX/4oJ;->a(LX/4oJ;FFFF)V

    .line 2734489
    invoke-virtual {p0, v4}, LX/4oJ;->setChildClippingEnabled(Z)V

    .line 2734490
    :goto_0
    return-void

    .line 2734491
    :cond_1
    invoke-virtual {p0, v4}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setClipToOutline(Z)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/montage/composer/art/ArtItemView;LX/IqE;LX/IqH;LX/1Ad;LX/8mv;LX/0Ot;LX/0Ot;LX/Irs;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/montage/composer/art/ArtItemView;",
            "LX/IqE;",
            "LX/IqH;",
            "LX/1Ad;",
            "LX/8mv;",
            "LX/0Ot",
            "<",
            "LX/1zC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;",
            "LX/Irs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2734471
    iput-object p1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->e:LX/IqE;

    iput-object p2, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    iput-object p3, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->g:LX/1Ad;

    iput-object p4, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->h:LX/8mv;

    iput-object p5, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->i:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->j:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->k:LX/Irs;

    return-void
.end method

.method private a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2734398
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2734399
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v1}, LX/IqH;->b()V

    .line 2734400
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->g:LX/1Ad;

    sget-object v2, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p1}, LX/8mv;->a(Lcom/facebook/stickers/model/Sticker;)[LX/1bf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    new-instance v2, LX/Jnk;

    invoke-direct {v2, p0, p1}, LX/Jnk;-><init>(Lcom/facebook/messaging/montage/composer/art/ArtItemView;Lcom/facebook/stickers/model/Sticker;)V

    invoke-virtual {v1, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2734401
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2734402
    invoke-static {p0, v3}, LX/4oZ;->a(Landroid/view/View;I)V

    .line 2734403
    invoke-virtual {p0, v4}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2734404
    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2734405
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2734406
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-static {v7}, LX/IqE;->b(LX/0QB;)LX/IqE;

    move-result-object v1

    check-cast v1, LX/IqE;

    invoke-static {v7}, LX/IqH;->a(LX/0QB;)LX/IqH;

    move-result-object v2

    check-cast v2, LX/IqH;

    invoke-static {v7}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v7}, LX/8mv;->b(LX/0QB;)LX/8mv;

    move-result-object v4

    check-cast v4, LX/8mv;

    const/16 v5, 0x12ac

    invoke-static {v7, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x11fa

    invoke-static {v7, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v8, LX/Irs;

    invoke-interface {v7, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Irs;

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Lcom/facebook/messaging/montage/composer/art/ArtItemView;LX/IqE;LX/IqH;LX/1Ad;LX/8mv;LX/0Ot;LX/0Ot;LX/Irs;)V

    return-void
.end method

.method private a(Ljava/util/List;LX/IqD;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;",
            "LX/IqD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2734453
    if-nez p1, :cond_0

    .line 2734454
    :goto_0
    return-void

    .line 2734455
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    if-nez v0, :cond_1

    .line 2734456
    invoke-direct {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->e()V

    .line 2734457
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2734458
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2734459
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    .line 2734460
    iget-object v1, v0, LX/Ird;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iqg;

    .line 2734461
    invoke-virtual {v0, v1}, LX/Ird;->c(LX/Iqg;)V

    .line 2734462
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2734463
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/ArtAsset;

    .line 2734464
    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v2}, LX/IqH;->b()V

    .line 2734465
    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->e:LX/IqE;

    iget-object v3, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->l:Lcom/facebook/messaging/montage/model/art/ArtItem;

    invoke-virtual {v2, v0, v3, p2}, LX/IqE;->a(Lcom/facebook/messaging/montage/model/art/ArtAsset;Lcom/facebook/messaging/montage/model/art/ArtItem;LX/IqD;)LX/Iqg;

    move-result-object v0

    .line 2734466
    new-instance v2, LX/Jnm;

    invoke-direct {v2, p0}, LX/Jnm;-><init>(Lcom/facebook/messaging/montage/composer/art/ArtItemView;)V

    invoke-virtual {v0, v2}, LX/Iqg;->a(LX/Iqq;)V

    .line 2734467
    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    invoke-virtual {v2, v0}, LX/Ird;->a(LX/Iqg;)V

    goto :goto_2

    .line 2734468
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->o:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->b()V

    .line 2734469
    const v0, 0x7f0200b8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setBackgroundResource(I)V

    .line 2734470
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/messaging/montage/model/art/ArtItem;LX/IqD;)V
    .locals 3

    .prologue
    .line 2734435
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2734436
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->g:LX/1Ad;

    sget-object v2, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    .line 2734437
    iget-object v1, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2734438
    if-eqz v1, :cond_1

    .line 2734439
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v1}, LX/IqH;->b()V

    .line 2734440
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->g:LX/1Ad;

    .line 2734441
    iget-object v2, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 2734442
    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    new-instance v2, LX/Jnl;

    invoke-direct {v2, p0, p1}, LX/Jnl;-><init>(Lcom/facebook/messaging/montage/composer/art/ArtItemView;Lcom/facebook/messaging/montage/model/art/ArtItem;)V

    invoke-virtual {v1, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2734443
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2734444
    const v1, 0x7f0200b8

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setBackgroundResource(I)V

    .line 2734445
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2734446
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2734447
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2734448
    :cond_0
    :goto_0
    return-void

    .line 2734449
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    move-object v0, v0

    .line 2734450
    if-eqz v0, :cond_0

    .line 2734451
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    move-object v0, v0

    .line 2734452
    invoke-direct {p0, v0, p2}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Ljava/util/List;LX/IqD;)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2734498
    new-instance v0, LX/Ird;

    invoke-direct {v0}, LX/Ird;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    .line 2734499
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->k:LX/Irs;

    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    invoke-virtual {v1, v0, v2}, LX/Irs;->a(Lcom/facebook/messaging/photos/editing/LayerGroupLayout;LX/Ird;)Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->o:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2734500
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->o:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(II)V

    .line 2734501
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->o:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    const/4 v1, 0x0

    .line 2734502
    iput-boolean v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->x:Z

    .line 2734503
    if-nez v1, :cond_0

    .line 2734504
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->w:Z

    .line 2734505
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->o:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a()V

    .line 2734506
    return-void
.end method


# virtual methods
.method public final a(LX/IqG;)V
    .locals 1

    .prologue
    .line 2734394
    sget-object v0, LX/IqG;->DONE:LX/IqG;

    if-ne p1, v0, :cond_0

    .line 2734395
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    if-eqz v0, :cond_0

    .line 2734396
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->n:LX/Ird;

    invoke-virtual {v0}, LX/Ird;->i()V

    .line 2734397
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/messaging/montage/model/art/ArtItem;LX/IqD;)V
    .locals 1

    .prologue
    .line 2734407
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->d()V

    .line 2734408
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/ArtItem;

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->l:Lcom/facebook/messaging/montage/model/art/ArtItem;

    .line 2734409
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    move-object v0, v0

    .line 2734410
    if-eqz v0, :cond_1

    .line 2734411
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    move-object v0, v0

    .line 2734412
    invoke-direct {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Lcom/facebook/stickers/model/Sticker;)V

    .line 2734413
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->a()V

    .line 2734414
    return-void

    .line 2734415
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->q:Z

    if-eqz v0, :cond_2

    .line 2734416
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    move-object v0, v0

    .line 2734417
    if-eqz v0, :cond_2

    .line 2734418
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->b(Lcom/facebook/messaging/montage/model/art/ArtItem;LX/IqD;)V

    goto :goto_0

    .line 2734419
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    move-object v0, v0

    .line 2734420
    if-eqz v0, :cond_0

    .line 2734421
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    move-object v0, v0

    .line 2734422
    invoke-direct {p0, v0, p2}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a(Ljava/util/List;LX/IqD;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2734423
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->e()Z

    move-result v0

    return v0
.end method

.method public getArtItem()Lcom/facebook/messaging/montage/model/art/ArtItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2734424
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->l:Lcom/facebook/messaging/montage/model/art/ArtItem;

    return-object v0
.end method

.method public getSection()LX/Dhc;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2734425
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->m:LX/Dhc;

    return-object v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x37878dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2734426
    invoke-super {p0, p1, p2, p3, p4}, LX/4oJ;->onSizeChanged(IIII)V

    .line 2734427
    new-instance v1, Lcom/facebook/messaging/montage/composer/art/ArtItemView$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/messaging/montage/composer/art/ArtItemView$1;-><init>(Lcom/facebook/messaging/montage/composer/art/ArtItemView;II)V

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->post(Ljava/lang/Runnable;)Z

    .line 2734428
    const/16 v1, 0x2d

    const v2, 0x7c7b574b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setArtPickerSection(LX/Dhc;)V
    .locals 0
    .param p1    # LX/Dhc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734429
    iput-object p1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->m:LX/Dhc;

    .line 2734430
    return-void
.end method

.method public setListener(LX/Jnn;)V
    .locals 0

    .prologue
    .line 2734431
    iput-object p1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->p:LX/Jnn;

    .line 2734432
    return-void
.end method

.method public setPreferThumbnailAssets(Z)V
    .locals 0

    .prologue
    .line 2734433
    iput-boolean p1, p0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->q:Z

    .line 2734434
    return-void
.end method
