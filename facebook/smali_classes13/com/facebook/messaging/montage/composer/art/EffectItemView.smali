.class public Lcom/facebook/messaging/montage/composer/art/EffectItemView;
.super LX/4oJ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field public final d:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/messaging/montage/model/art/EffectItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734684
    const-class v0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2734682
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2734683
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2734680
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2734681
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2734665
    invoke-direct {p0, p1, p2, p3}, LX/4oJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2734666
    const v0, 0x7f030111

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2734667
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2734668
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b269a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2699

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2734669
    :cond_0
    const v0, 0x7f0200b8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->setBackgroundResource(I)V

    .line 2734670
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2734671
    const v0, 0x7f0d05b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->b:Landroid/view/View;

    .line 2734672
    const v0, 0x7f0d05b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->c:Landroid/view/View;

    .line 2734673
    const v0, 0x7f0d05b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->d:LX/4ob;

    .line 2734674
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 2734675
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b269b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 2734676
    invoke-static {p0, v0, v0, v0, v0}, LX/4oJ;->a(LX/4oJ;FFFF)V

    .line 2734677
    invoke-virtual {p0, v4}, LX/4oJ;->setChildClippingEnabled(Z)V

    .line 2734678
    :goto_0
    return-void

    .line 2734679
    :cond_1
    invoke-virtual {p0, v4}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->setClipToOutline(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Jnw;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2734660
    iget-object v3, p0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->b:Landroid/view/View;

    sget-object v0, LX/Jnw;->NOT_STARTED:LX/Jnw;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2734661
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->c:Landroid/view/View;

    sget-object v3, LX/Jnw;->IN_PROGRESS:LX/Jnw;

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2734662
    return-void

    :cond_0
    move v0, v2

    .line 2734663
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2734664
    goto :goto_1
.end method
