.class public Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Jnw;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/1HI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734858
    const-class v0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2734855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734856
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b:Ljava/util/Map;

    .line 2734857
    return-void
.end method

.method public static b(Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;Lcom/facebook/messaging/montage/model/art/EffectAsset;)Z
    .locals 5

    .prologue
    .line 2734801
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2734802
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2734803
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->e:LX/1HI;

    invoke-virtual {v0, v1}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2734804
    const/4 v0, 0x1

    .line 2734805
    :goto_0
    return v0

    .line 2734806
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->e:LX/1HI;

    invoke-virtual {v0, v1}, LX/1HI;->d(Landroid/net/Uri;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    const v2, 0x533bed6a

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 2734807
    :catch_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->d:LX/03V;

    const-string v2, "ParticleAssetManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while trying to load image from disk: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2734808
    const/4 v0, 0x0

    goto :goto_0

    .line 2734809
    :catch_1
    goto :goto_1
.end method

.method private static d(Lcom/facebook/messaging/montage/model/art/EffectItem;)V
    .locals 2

    .prologue
    .line 2734851
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    move-object v0, v0

    .line 2734852
    if-nez v0, :cond_0

    .line 2734853
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The effect is not particles."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734854
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;
    .locals 5

    .prologue
    .line 2734834
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->d(Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734835
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2734836
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2734837
    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jnw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734838
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2734839
    :cond_0
    :try_start_1
    const/4 v1, 0x0

    .line 2734840
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    move-object v3, v0

    .line 2734841
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;

    .line 2734842
    invoke-static {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b(Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;Lcom/facebook/messaging/montage/model/art/EffectAsset;)Z

    move-result v0

    .line 2734843
    if-nez v0, :cond_2

    move v0, v1

    .line 2734844
    :goto_2
    move v0, v0

    .line 2734845
    if-eqz v0, :cond_1

    .line 2734846
    sget-object v0, LX/Jnw;->COMPLETED:LX/Jnw;

    goto :goto_0

    .line 2734847
    :cond_1
    sget-object v0, LX/Jnw;->NOT_STARTED:LX/Jnw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2734848
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2734849
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2734850
    :cond_3
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/montage/model/art/EffectItem;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/montage/model/art/EffectItem;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2734810
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->d(Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734811
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b:Ljava/util/Map;

    .line 2734812
    iget-object v1, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2734813
    sget-object v2, LX/Jnw;->IN_PROGRESS:LX/Jnw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734814
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2734815
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    move-object v3, v0

    .line 2734816
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;

    .line 2734817
    invoke-static {p0, v0}, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->b(Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;Lcom/facebook/messaging/montage/model/art/EffectAsset;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2734818
    const/4 v5, 0x0

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2734819
    :goto_1
    move-object v0, v5

    .line 2734820
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2734821
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2734822
    :cond_0
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2734823
    new-instance v1, LX/Jo0;

    .line 2734824
    iget-object v2, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2734825
    invoke-direct {v1, p0, v2}, LX/Jo0;-><init>(Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;Ljava/lang/String;)V

    .line 2734826
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2734827
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734828
    monitor-exit p0

    return-object v0

    .line 2734829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2734830
    :cond_1
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v5

    .line 2734831
    iget-object v6, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->e:LX/1HI;

    .line 2734832
    iget-object v7, v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    move-object v7, v7

    .line 2734833
    invoke-static {v7}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v7

    sget-object v8, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7, v8}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v6

    new-instance v7, LX/Jo1;

    invoke-direct {v7, v5}, LX/Jo1;-><init>(Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v8, p0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v6, v7, v8}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
