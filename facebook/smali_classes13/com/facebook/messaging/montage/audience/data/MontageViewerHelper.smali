.class public Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKO;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKQ;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKW;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKw;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL8;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734269
    const-class v0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->a:Ljava/lang/String;

    .line 2734270
    const-class v0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2734271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734272
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734273
    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->c:LX/0Ot;

    .line 2734274
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734275
    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->e:LX/0Ot;

    .line 2734276
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734277
    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->g:LX/0Ot;

    .line 2734278
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734279
    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->k:LX/0Ot;

    .line 2734280
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734281
    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->m:LX/0Ot;

    .line 2734282
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734283
    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->n:LX/0Ot;

    .line 2734284
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageViewerHelper;->o:Ljava/util/Map;

    .line 2734285
    return-void
.end method
