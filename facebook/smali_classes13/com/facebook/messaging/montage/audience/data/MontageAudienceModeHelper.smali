.class public Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0aG;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0Xp;

.field public final f:LX/0WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734258
    const-class v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Xp;LX/0WJ;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2734251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734252
    iput-object p1, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->b:LX/0aG;

    .line 2734253
    iput-object p2, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->c:Landroid/content/Context;

    .line 2734254
    iput-object p3, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->d:Ljava/util/concurrent/ExecutorService;

    .line 2734255
    iput-object p4, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->e:LX/0Xp;

    .line 2734256
    iput-object p5, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->f:LX/0WJ;

    .line 2734257
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;
    .locals 6

    .prologue
    .line 2734267
    new-instance v0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v4

    check-cast v4, LX/0Xp;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v5

    check-cast v5, LX/0WJ;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;-><init>(LX/0aG;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Xp;LX/0WJ;)V

    .line 2734268
    return-object v0
.end method


# virtual methods
.method public final a(ZLX/Jna;)V
    .locals 6
    .param p2    # LX/Jna;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734259
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2734260
    const-string v1, "montageAudienceMode"

    if-eqz p1, :cond_0

    const-string v0, "AUTO"

    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2734261
    iget-object v0, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->b:LX/0aG;

    const-string v1, "update_montage_audience_mode"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0xeea341b

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 2734262
    new-instance v1, LX/4At;

    iget-object v2, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->c:Landroid/content/Context;

    const v3, 0x7f082e62

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-interface {v0, v1}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    .line 2734263
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2734264
    new-instance v1, LX/Jne;

    invoke-direct {v1, p0, p1, p2}, LX/Jne;-><init>(Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;ZLX/Jna;)V

    iget-object v2, p0, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2734265
    return-void

    .line 2734266
    :cond_0
    const-string v0, "MANUAL"

    goto :goto_0
.end method
