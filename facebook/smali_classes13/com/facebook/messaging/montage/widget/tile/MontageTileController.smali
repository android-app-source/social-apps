.class public Lcom/facebook/messaging/montage/widget/tile/MontageTileController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/JoM;

.field private final a:Lcom/facebook/common/callercontext/CallerContext;

.field public final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final d:Landroid/content/res/Resources;

.field public e:LX/2OS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8jI;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/2Ly;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/2My;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/2OR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/JoH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3dt;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8mv;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Yb;

.field public s:LX/JoD;

.field public t:I

.field public u:I

.field public v:Z

.field private w:I

.field public x:Z

.field private y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2735270
    const-class v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    sput-object v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 2
    .param p1    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2735240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2735241
    const-class v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;

    const-string v1, "messenger_montage_thumbnail"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2735242
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735243
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->g:LX/0Ot;

    .line 2735244
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735245
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j:LX/0Ot;

    .line 2735246
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735247
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->o:LX/0Ot;

    .line 2735248
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735249
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->p:LX/0Ot;

    .line 2735250
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2735251
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->q:LX/0Ot;

    .line 2735252
    iput-object p1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2735253
    invoke-virtual {p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->d:Landroid/content/res/Resources;

    .line 2735254
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->w:I

    .line 2735255
    sget-object v0, LX/JoM;->NONE:LX/JoM;

    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    .line 2735256
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2735257
    :goto_0
    invoke-virtual {v0}, LX/1af;->c()I

    move-result v1

    if-nez v1, :cond_0

    .line 2735258
    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, LX/1af;->a(I)V

    .line 2735259
    :cond_0
    iget-object v1, v0, LX/1af;->c:LX/4Ab;

    move-object v1, v1

    .line 2735260
    if-nez v1, :cond_1

    .line 2735261
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->d:Landroid/content/res/Resources;

    const p1, 0x7f0b1f2d

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->a(LX/4Ab;)V

    .line 2735262
    :cond_1
    const/4 v1, 0x1

    .line 2735263
    iget-object p1, v0, LX/1af;->e:LX/1ap;

    invoke-virtual {p1, v1}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_4

    :goto_1
    move v1, v1

    .line 2735264
    if-nez v1, :cond_2

    .line 2735265
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->d:Landroid/content/res/Resources;

    const p1, 0x7f0a01cb

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2735266
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2735267
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2735268
    return-void

    .line 2735269
    :cond_3
    new-instance v0, LX/1Uo;

    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->d:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Landroid/net/Uri;)LX/1bf;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2735229
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Ljava/lang/String;)LX/33B;

    move-result-object v1

    .line 2735230
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 2735231
    move-object v0, v0

    .line 2735232
    invoke-static {}, LX/1bZ;->newBuilder()LX/1ba;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/1ba;->a(Z)LX/1ba;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/1ba;->c(Z)LX/1ba;

    move-result-object v1

    invoke-virtual {v1}, LX/1ba;->g()LX/1bZ;

    move-result-object v1

    .line 2735233
    iput-object v1, v0, LX/1bX;->e:LX/1bZ;

    .line 2735234
    move-object v0, v0

    .line 2735235
    iget v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    if-lez v1, :cond_0

    .line 2735236
    new-instance v1, LX/1o9;

    iget v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    iget v3, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 2735237
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 2735238
    move-object v0, v0

    .line 2735239
    :cond_0
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Ljava/lang/String;)LX/33B;
    .locals 10

    .prologue
    .line 2735096
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->n:LX/JoH;

    iget v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->w:I

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->z:Z

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/high16 v6, -0x1000000

    .line 2735097
    packed-switch v1, :pswitch_data_0

    .line 2735098
    :cond_0
    :goto_0
    :pswitch_0
    move-object v0, v3

    .line 2735099
    return-object v0

    .line 2735100
    :pswitch_1
    if-eqz v2, :cond_2

    .line 2735101
    iget-object v3, v0, LX/JoH;->b:LX/2My;

    invoke-virtual {v3}, LX/2My;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2735102
    new-instance v3, LX/JoF;

    const/4 v7, 0x2

    move-object v4, v0

    move-object v5, p1

    invoke-direct/range {v3 .. v8}, LX/JoF;-><init>(LX/JoH;Ljava/lang/String;IIZ)V

    goto :goto_0

    .line 2735103
    :cond_1
    new-instance v3, LX/JoF;

    invoke-direct {v3, v0, p1, v9}, LX/JoF;-><init>(LX/JoH;Ljava/lang/String;I)V

    goto :goto_0

    .line 2735104
    :cond_2
    new-instance v3, LX/JoF;

    const/4 v7, 0x3

    move-object v4, v0

    move-object v5, p1

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/JoF;-><init>(LX/JoH;Ljava/lang/String;IIZ)V

    goto :goto_0

    .line 2735105
    :pswitch_2
    if-eqz v2, :cond_0

    .line 2735106
    new-instance v3, LX/JoF;

    invoke-direct {v3, v0, p1, v8}, LX/JoF;-><init>(LX/JoH;Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Z)V
    .locals 2

    .prologue
    .line 2735220
    if-eqz p1, :cond_1

    .line 2735221
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    if-eqz v0, :cond_2

    .line 2735222
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2735223
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2735224
    :cond_0
    :goto_1
    return-void

    .line 2735225
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2735226
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    goto :goto_1

    .line 2735227
    :cond_2
    new-instance v0, LX/JoA;

    invoke-direct {v0, p0}, LX/JoA;-><init>(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    .line 2735228
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->h:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string p1, "com.facebook.orca.media.upload.PROCESS_MEDIA_TOTAL_PROGRESS"

    invoke-interface {v1, p1, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string p1, "com.facebook.orca.media.upload.MEDIA_UPLOAD_STATUS_CHANGED"

    invoke-interface {v1, p1, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->r:LX/0Yb;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Lcom/facebook/stickers/model/Sticker;)V
    .locals 8

    .prologue
    .line 2735197
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/8mv;->a(Lcom/facebook/stickers/model/Sticker;)[LX/1bf;

    move-result-object v0

    .line 2735198
    if-eqz v0, :cond_0

    iget v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    if-gtz v1, :cond_2

    .line 2735199
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2735200
    if-eqz v0, :cond_1

    array-length v1, v0

    if-eqz v1, :cond_1

    .line 2735201
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->i(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2735202
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2735203
    :goto_1
    return-void

    .line 2735204
    :cond_1
    sget-object v0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b:Ljava/lang/Class;

    const-string v1, "There is no uri associate with sticker %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2735205
    :cond_2
    array-length v1, v0

    new-array v3, v1, [LX/1bf;

    .line 2735206
    const/4 v1, 0x0

    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_4

    .line 2735207
    aget-object v2, v0, v1

    .line 2735208
    invoke-static {v2}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v4

    new-instance v5, LX/1o9;

    iget v6, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    iget v7, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    invoke-direct {v5, v6, v7}, LX/1o9;-><init>(II)V

    .line 2735209
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 2735210
    move-object v4, v4

    .line 2735211
    iget-object v5, v2, LX/1bf;->b:Landroid/net/Uri;

    move-object v5, v5

    .line 2735212
    if-nez v5, :cond_3

    const/4 v2, 0x0

    :goto_3
    invoke-static {p0, v2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Ljava/lang/String;)LX/33B;

    move-result-object v2

    .line 2735213
    iput-object v2, v4, LX/1bX;->j:LX/33B;

    .line 2735214
    move-object v2, v4

    .line 2735215
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    aput-object v2, v3, v1

    .line 2735216
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2735217
    :cond_3
    iget-object v5, v2, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v5

    .line 2735218
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_4
    move-object v0, v3

    .line 2735219
    goto :goto_0
.end method

.method private b(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2735191
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->B:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->B:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2735192
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2735193
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2735194
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2735195
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->B:Ljava/lang/Integer;

    .line 2735196
    :cond_1
    return-void
.end method

.method private b(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V
    .locals 12
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2735115
    if-nez p1, :cond_1

    .line 2735116
    const-string v2, ""

    iput-object v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->y:Ljava/lang/String;

    .line 2735117
    iput-boolean v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->z:Z

    .line 2735118
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2735119
    iput-object p3, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    .line 2735120
    invoke-static {p0, v1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Z)V

    .line 2735121
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    .line 2735122
    :cond_0
    :goto_0
    return-void

    .line 2735123
    :cond_1
    iget-object v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->y:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->z:Z

    if-ne p2, v2, :cond_2

    iget-object v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    if-eq v2, p3, :cond_0

    .line 2735124
    :cond_2
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->y:Ljava/lang/String;

    .line 2735125
    iput-boolean p2, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->z:Z

    .line 2735126
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    :cond_3
    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2735127
    iput-object p3, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    .line 2735128
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->C:LX/JoM;

    sget-object v2, LX/JoM;->PENDING:LX/JoM;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Z)V

    .line 2735129
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a01cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b(I)V

    .line 2735130
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->k:LX/2Ly;

    invoke-virtual {v0, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v0

    .line 2735131
    sget-object v1, LX/JoC;->a:[I

    invoke-virtual {v0}, LX/6eh;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2735132
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    .line 2735133
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->s:LX/JoD;

    invoke-interface {v0}, LX/JoD;->a()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2735134
    goto :goto_1

    .line 2735135
    :pswitch_0
    const/4 v1, 0x0

    .line 2735136
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/1Up;)V

    .line 2735137
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->e:LX/2OS;

    invoke-virtual {v0, p1}, LX/2OS;->e(Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v2

    .line 2735138
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2735139
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 2735140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2735141
    iget-object v2, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2735142
    if-eqz v2, :cond_5

    .line 2735143
    iget-object v3, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {p0, v3}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2735144
    iget-object v3, v2, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v3, :cond_5

    .line 2735145
    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {p0, v2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2735146
    :cond_5
    iget-object v2, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 2735147
    if-eqz v2, :cond_c

    iget-object v3, v2, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    if-eqz v3, :cond_c

    .line 2735148
    iget-object v2, v2, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    invoke-static {}, LX/1bZ;->newBuilder()LX/1ba;

    move-result-object v3

    const/4 p1, 0x1

    invoke-virtual {v3, p1}, LX/1ba;->a(Z)LX/1ba;

    move-result-object v3

    invoke-virtual {v3}, LX/1ba;->g()LX/1bZ;

    move-result-object v3

    .line 2735149
    iput-object v3, v2, LX/1bX;->e:LX/1bZ;

    .line 2735150
    move-object v2, v2

    .line 2735151
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 2735152
    :goto_4
    move-object v2, v2

    .line 2735153
    if-eqz v2, :cond_6

    .line 2735154
    iget-object v3, v2, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v3

    .line 2735155
    invoke-static {p0, v2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2735156
    :cond_6
    iget-object v2, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 2735157
    if-eqz v2, :cond_d

    .line 2735158
    iget-object v2, v2, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    .line 2735159
    :goto_5
    move-object v2, v2

    .line 2735160
    if-eqz v2, :cond_7

    .line 2735161
    iget-object v3, v2, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v3

    .line 2735162
    invoke-static {p0, v2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2735163
    :cond_7
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [LX/1bf;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/1bf;

    move-object v0, v1

    .line 2735164
    array-length v1, v0

    if-lez v1, :cond_b

    .line 2735165
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->i(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2735166
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2735167
    :goto_6
    goto/16 :goto_2

    .line 2735168
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->e:LX/2OS;

    invoke-virtual {v0, p1}, LX/2OS;->g(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/VideoAttachmentData;

    move-result-object v1

    .line 2735169
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/1Up;)V

    .line 2735170
    iget-object v0, v1, Lcom/facebook/messaging/attachments/VideoAttachmentData;->g:Landroid/net/Uri;

    if-eqz v0, :cond_8

    .line 2735171
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->i(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)LX/1Ad;

    move-result-object v0

    iget-object v1, v1, Lcom/facebook/messaging/attachments/VideoAttachmentData;->g:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2735172
    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2735173
    :cond_8
    goto/16 :goto_2

    .line 2735174
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v1, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 2735175
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dt;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 2735176
    if-nez v0, :cond_e

    .line 2735177
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8jI;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8jI;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2735178
    new-instance v2, LX/JoB;

    invoke-direct {v2, p0}, LX/JoB;-><init>(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2735179
    :goto_7
    goto/16 :goto_2

    .line 2735180
    :pswitch_3
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    iget v4, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    if-lez v4, :cond_9

    iget v4, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    if-gtz v4, :cond_f

    .line 2735181
    :cond_9
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    .line 2735182
    :goto_8
    goto/16 :goto_2

    :cond_a
    move v0, v1

    .line 2735183
    goto/16 :goto_3

    .line 2735184
    :cond_b
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    goto/16 :goto_6

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 2735185
    :cond_e
    invoke-static {p0, v0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a$redex0(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;Lcom/facebook/stickers/model/Sticker;)V

    goto :goto_7

    .line 2735186
    :cond_f
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->i(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)LX/1Ad;

    move-result-object v10

    const v4, 0x7f020f65

    invoke-static {v4}, LX/1bX;->a(I)LX/1bX;

    move-result-object v11

    iget-object v4, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->n:LX/JoH;

    iget-object v5, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    iget v7, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->t:I

    iget v8, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->u:I

    iget-boolean v9, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->z:Z

    invoke-virtual/range {v4 .. v9}, LX/JoH;->a(Landroid/content/Context;Ljava/lang/String;IIZ)LX/33B;

    move-result-object v4

    .line 2735187
    iput-object v4, v11, LX/1bX;->j:LX/33B;

    .line 2735188
    move-object v4, v11

    .line 2735189
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    invoke-virtual {v10, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    .line 2735190
    iget-object v5, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 2735110
    iget-boolean v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->v:Z

    if-eq v0, p1, :cond_0

    .line 2735111
    iput-boolean p1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->v:Z

    .line 2735112
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->s:LX/JoD;

    if-eqz v0, :cond_0

    .line 2735113
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->s:LX/JoD;

    invoke-interface {v0}, LX/JoD;->a()V

    .line 2735114
    :cond_0
    return-void
.end method

.method public static i(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)LX/1Ad;
    .locals 2

    .prologue
    .line 2735109
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    return-object v0
.end method

.method public static j(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V
    .locals 2

    .prologue
    .line 2735107
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2735108
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2735091
    invoke-static {p0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->j(Lcom/facebook/messaging/montage/widget/tile/MontageTileController;)V

    .line 2735092
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->y:Ljava/lang/String;

    .line 2735093
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->w:I

    .line 2735094
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b(I)V

    .line 2735095
    return-void
.end method

.method public final a(LX/DhQ;)V
    .locals 14

    .prologue
    .line 2735065
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->m:LX/2OR;

    .line 2735066
    if-eqz p1, :cond_2

    iget-object v4, p1, LX/DhQ;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v5, p1, LX/DhQ;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2735067
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2735068
    :cond_0
    const/4 v6, 0x0

    .line 2735069
    :goto_0
    move v4, v6

    .line 2735070
    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    move v1, v4

    .line 2735071
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->m:LX/2OR;

    .line 2735072
    if-nez p1, :cond_7

    .line 2735073
    const/4 v2, 0x0

    .line 2735074
    :goto_2
    move-object v2, v2

    .line 2735075
    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->x:Z

    .line 2735076
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->w:I

    .line 2735077
    invoke-direct {p0, v1}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b(Z)V

    .line 2735078
    sget-object v0, LX/JoM;->NONE:LX/JoM;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V

    .line 2735079
    return-void

    .line 2735080
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v3}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    iget-wide v6, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    .line 2735081
    invoke-static {v0}, LX/2OR;->a(LX/2OR;)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    move-wide v6, v8

    .line 2735082
    const/4 v9, 0x0

    .line 2735083
    move v8, v9

    :goto_4
    invoke-virtual {v5}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v10

    if-ge v8, v10, :cond_4

    .line 2735084
    invoke-virtual {v5, v8}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v10

    .line 2735085
    iget-wide v12, v10, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v11, v12, v6

    if-gtz v11, :cond_5

    .line 2735086
    :cond_4
    :goto_5
    move v6, v9

    .line 2735087
    goto :goto_0

    .line 2735088
    :cond_5
    invoke-static {v0, v10}, LX/2OR;->b(LX/2OR;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2735089
    const/4 v9, 0x1

    goto :goto_5

    .line 2735090
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_7
    iget-object v2, p1, LX/DhQ;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0, v2}, LX/2OR;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V
    .locals 1
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2735060
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->w:I

    .line 2735061
    iget-object v0, p0, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->l:LX/2My;

    invoke-virtual {v0}, LX/2My;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2735062
    invoke-direct {p0, p2}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b(Z)V

    .line 2735063
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/montage/widget/tile/MontageTileController;->b(Lcom/facebook/messaging/model/messages/Message;ZLX/JoM;)V

    .line 2735064
    return-void
.end method
