.class public Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734933
    new-instance v0, LX/Jo5;

    invoke-direct {v0}, LX/Jo5;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2734934
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2734935
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->a:I

    .line 2734936
    const-class v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    .line 2734937
    const-class v0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    .line 2734938
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2734939
    sget-object v0, LX/DfY;->V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2734940
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2734941
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2734942
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2734943
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2734944
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2734945
    sget-object v0, LX/Dfa;->V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/Dfa;

    return-object v0
.end method

.method public final e()J
    .locals 4

    .prologue
    .line 2734946
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    .line 2734947
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2734948
    iget v1, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->a:I

    invoke-interface {v0, v1}, LX/51h;->a(I)LX/51h;

    .line 2734949
    iget-object v1, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    if-eqz v1, :cond_0

    .line 2734950
    iget-object v1, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    iget-object v1, v1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/51h;->a(J)LX/51h;

    .line 2734951
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    if-eqz v1, :cond_1

    .line 2734952
    iget-object v1, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/51h;->a(J)LX/51h;

    .line 2734953
    :cond_1
    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2734954
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->a:I

    packed-switch v0, :pswitch_data_0

    .line 2734955
    invoke-super {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2734956
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734957
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    iget-object v1, v1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2734958
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734959
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->c:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->nm_()Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    move-result-object v0

    .line 2734960
    iget-object v1, v0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2734961
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
