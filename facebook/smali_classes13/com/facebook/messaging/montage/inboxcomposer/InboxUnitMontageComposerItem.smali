.class public Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734976
    new-instance v0, LX/Jo6;

    invoke-direct {v0}, LX/Jo6;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2734977
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2734978
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->a:J

    .line 2734979
    const-class v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    .line 2734980
    sget-object v0, Lcom/facebook/messaging/montage/inboxcomposer/InboxMontageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->c:LX/0Px;

    .line 2734981
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->j:LX/0Px;

    .line 2734982
    const-class v0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->k:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    .line 2734983
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2734975
    sget-object v0, LX/DfY;->V2_MONTAGE_COMPOSER_HEADER:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2734984
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2734985
    iget-wide v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2734986
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2734987
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->c:LX/0Px;

    .line 2734988
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2734989
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->j:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2734990
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->k:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2734991
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2734974
    sget-object v0, LX/Dfa;->V2_MONTAGE_COMPOSER_HEADER:LX/Dfa;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2734965
    if-ne p0, p1, :cond_1

    .line 2734966
    :cond_0
    :goto_0
    return v0

    .line 2734967
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2734968
    goto :goto_0

    .line 2734969
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;

    .line 2734970
    iget-wide v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->a:J

    iget-wide v4, p1, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    iget-object v3, p1, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->j:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->j:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->k:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->k:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2734971
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->l:I

    if-nez v0, :cond_0

    .line 2734972
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->b:Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->j:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->k:Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->l:I

    .line 2734973
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/InboxUnitMontageComposerItem;->l:I

    return v0
.end method
