.class public final Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/messages/Message;

.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final c:Z

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2734998
    new-instance v0, LX/Jo8;

    invoke-direct {v0}, LX/Jo8;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2734999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2735000
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2735001
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735002
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->c:Z

    .line 2735003
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    .line 2735004
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2735005
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2735006
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->hashCode()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2735007
    if-ne p0, p1, :cond_1

    .line 2735008
    :cond_0
    :goto_0
    return v0

    .line 2735009
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2735010
    goto :goto_0

    .line 2735011
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;

    .line 2735012
    iget-boolean v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->c:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->c:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_1
    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_2
    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_3
    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    if-nez v2, :cond_4

    goto :goto_1

    :cond_6
    iget-object v2, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v2, :cond_4

    goto :goto_2

    :cond_7
    iget-object v2, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    if-nez v2, :cond_4

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-nez v2, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2735013
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->f:I

    if-nez v0, :cond_0

    .line 2735014
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->f:I

    .line 2735015
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->f:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2735016
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2735017
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2735018
    iget-boolean v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2735019
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2735020
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2735021
    return-void
.end method
