.class public final Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2735022
    new-instance v0, LX/Jo7;

    invoke-direct {v0}, LX/Jo7;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2735030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2735031
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2735032
    sget-object v0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData$Item;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    .line 2735033
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2735029
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->hashCode()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2735034
    if-ne p0, p1, :cond_1

    .line 2735035
    :cond_0
    :goto_0
    return v0

    .line 2735036
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2735037
    goto :goto_0

    .line 2735038
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;

    .line 2735039
    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_1
    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v2, :cond_4

    goto :goto_1

    :cond_6
    iget-object v2, p1, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    if-nez v2, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2735026
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->c:I

    if-nez v0, :cond_0

    .line 2735027
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->c:I

    .line 2735028
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->c:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2735023
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2735024
    iget-object v0, p0, Lcom/facebook/messaging/montage/inboxcomposer/MontageInboxData;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2735025
    return-void
.end method
