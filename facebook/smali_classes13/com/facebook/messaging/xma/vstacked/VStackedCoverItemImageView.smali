.class public Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field private final c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2745715
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2745716
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->c:Landroid/graphics/Paint;

    .line 2745717
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 1

    .prologue
    .line 2745718
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2745719
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->c:Landroid/graphics/Paint;

    .line 2745720
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2745721
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2745722
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->c:Landroid/graphics/Paint;

    .line 2745723
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2745724
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745725
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->c:Landroid/graphics/Paint;

    .line 2745726
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2745727
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2745728
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a09ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2745729
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 2745730
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2745731
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2745732
    int-to-double v2, v0

    const-wide v4, 0x3ffe666666666666L    # 1.9

    div-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/messaging/xma/vstacked/VStackedCoverItemImageView;->setMeasuredDimension(II)V

    .line 2745733
    return-void
.end method
