.class public Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;
.super Lcom/facebook/messaging/xma/ui/XMALinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/Jso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2745662
    const-class v0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2745663
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745664
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745665
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745666
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745667
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745668
    const-class v0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2745669
    invoke-direct {p0}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->a()V

    .line 2745670
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2745671
    const v0, 0x7f031600

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2745672
    const v0, 0x7f0d317a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2745673
    const v0, 0x7f0d2e0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2745674
    const v0, 0x7f0d2e0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2745675
    const v0, 0x7f0d3177

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->g:LX/4ob;

    .line 2745676
    const v0, 0x7f0d3176

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->h:LX/4ob;

    .line 2745677
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;

    invoke-static {v0}, LX/Jso;->b(LX/0QB;)LX/Jso;

    move-result-object v0

    check-cast v0, LX/Jso;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->b:LX/Jso;

    return-void
.end method


# virtual methods
.method public final a(LX/6ll;)V
    .locals 1
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745678
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->h:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->setXMACallback(LX/6ll;)V

    .line 2745679
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V
    .locals 4

    .prologue
    .line 2745680
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0, p1}, LX/Jso;->a(Lcom/facebook/widget/text/BetterTextView;LX/5St;)V

    .line 2745681
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0, p1}, LX/Jso;->b(Lcom/facebook/widget/text/BetterTextView;LX/5St;)V

    .line 2745682
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2745683
    invoke-interface {p1}, LX/5St;->c()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2745684
    invoke-interface {p1}, LX/5St;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2745685
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2745686
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->b:LX/Jso;

    iget-object v1, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->g:LX/4ob;

    invoke-virtual {v0, v1, p1}, LX/Jso;->a(LX/4ob;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2745687
    iget-object v2, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->b:LX/Jso;

    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->g:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d3179

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->g:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    const v3, 0x7f0d3178

    invoke-virtual {v1, v3}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v2, v0, v1, p1}, LX/Jso;->a(Lcom/facebook/widget/text/BetterTextView;Landroid/widget/ImageView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    .line 2745688
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->h:LX/4ob;

    invoke-static {v0, p1}, LX/Jso;->b(LX/4ob;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    .line 2745689
    new-instance v0, LX/Jsi;

    invoke-direct {v0, p0, p1}, LX/Jsi;-><init>(Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/xma/vstacked/VStackedCompactItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2745690
    return-void

    .line 2745691
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
