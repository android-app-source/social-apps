.class public Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;
.super Lcom/facebook/messaging/xma/ui/XMALinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/Jso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2745627
    const-class v0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2745640
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745641
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745630
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745631
    const-class v0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2745632
    const v0, 0x7f0315fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2745633
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->setOrientation(I)V

    .line 2745634
    const v0, 0x7f0d3175

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2745635
    const v0, 0x7f0d2e0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2745636
    const v0, 0x7f0d2e0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2745637
    const v0, 0x7f0d3177

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->g:LX/4ob;

    .line 2745638
    const v0, 0x7f0d3176

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->h:LX/4ob;

    .line 2745639
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;

    invoke-static {p0}, LX/Jso;->b(LX/0QB;)LX/Jso;

    move-result-object p0

    check-cast p0, LX/Jso;

    iput-object p0, p1, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->b:LX/Jso;

    return-void
.end method


# virtual methods
.method public final a(LX/6ll;)V
    .locals 1
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745628
    iget-object v0, p0, Lcom/facebook/messaging/xma/vstacked/VStackedCommerceCoverItemView;->h:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->setXMACallback(LX/6ll;)V

    .line 2745629
    return-void
.end method
