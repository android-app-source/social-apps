.class public Lcom/facebook/messaging/protocol/WebServiceHandler;
.super LX/Dnx;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL2;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL5;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL6;",
            ">;"
        }
    .end annotation
.end field

.field private D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKk;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKl;",
            ">;"
        }
    .end annotation
.end field

.field private F:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL3;",
            ">;"
        }
    .end annotation
.end field

.field private G:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL7;",
            ">;"
        }
    .end annotation
.end field

.field private H:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKX;",
            ">;"
        }
    .end annotation
.end field

.field private I:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKh;",
            ">;"
        }
    .end annotation
.end field

.field private J:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendApiHandler;",
            ">;"
        }
    .end annotation
.end field

.field private K:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKc;",
            ">;"
        }
    .end annotation
.end field

.field private L:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKM;",
            ">;"
        }
    .end annotation
.end field

.field private M:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKu;",
            ">;"
        }
    .end annotation
.end field

.field private N:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKq;",
            ">;"
        }
    .end annotation
.end field

.field private O:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;"
        }
    .end annotation
.end field

.field private P:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FO7;",
            ">;"
        }
    .end annotation
.end field

.field private Q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JsP;",
            ">;"
        }
    .end annotation
.end field

.field private R:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;"
        }
    .end annotation
.end field

.field private S:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3NC;",
            ">;"
        }
    .end annotation
.end field

.field private T:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FLC;",
            ">;"
        }
    .end annotation
.end field

.field private U:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FFQ;",
            ">;"
        }
    .end annotation
.end field

.field private V:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKV;",
            ">;"
        }
    .end annotation
.end field

.field private W:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKT;",
            ">;"
        }
    .end annotation
.end field

.field private X:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3ND;",
            ">;"
        }
    .end annotation
.end field

.field private Y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3NB;",
            ">;"
        }
    .end annotation
.end field

.field private Z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKU;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LX/18V;

.field private final b:LX/6Po;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Mq;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8CH;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/Iuh;

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKg;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKf;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKL;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKi;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKN;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKP;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FLA;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FLB;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL9;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKv;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKR;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Oo;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKb;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKa;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL1;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/18V;LX/6Po;LX/0Or;LX/0Or;LX/2Mq;LX/0Or;LX/0Or;LX/Iuh;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMarkFolderSeenOverMqttGatekeeper;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/6Po;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Mq;",
            "LX/0Or",
            "<",
            "LX/8CH;",
            ">;",
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;",
            "LX/Iuh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2736694
    const-string v0, "WebServiceHandler"

    invoke-direct {p0, v0}, LX/Dnx;-><init>(Ljava/lang/String;)V

    .line 2736695
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736696
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->i:LX/0Ot;

    .line 2736697
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736698
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->j:LX/0Ot;

    .line 2736699
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736700
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    .line 2736701
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736702
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->l:LX/0Ot;

    .line 2736703
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736704
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->m:LX/0Ot;

    .line 2736705
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736706
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->n:LX/0Ot;

    .line 2736707
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736708
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->o:LX/0Ot;

    .line 2736709
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736710
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->p:LX/0Ot;

    .line 2736711
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736712
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->q:LX/0Ot;

    .line 2736713
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736714
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->r:LX/0Ot;

    .line 2736715
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736716
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->s:LX/0Ot;

    .line 2736717
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736718
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->t:LX/0Ot;

    .line 2736719
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736720
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->u:LX/0Ot;

    .line 2736721
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736722
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->v:LX/0Ot;

    .line 2736723
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736724
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->w:LX/0Ot;

    .line 2736725
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736726
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->x:LX/0Ot;

    .line 2736727
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736728
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->y:LX/0Ot;

    .line 2736729
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736730
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->z:LX/0Ot;

    .line 2736731
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736732
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->A:LX/0Ot;

    .line 2736733
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736734
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->B:LX/0Ot;

    .line 2736735
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736736
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->C:LX/0Ot;

    .line 2736737
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736738
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->D:LX/0Ot;

    .line 2736739
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736740
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->E:LX/0Ot;

    .line 2736741
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736742
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->F:LX/0Ot;

    .line 2736743
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736744
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->G:LX/0Ot;

    .line 2736745
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736746
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->H:LX/0Ot;

    .line 2736747
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736748
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->I:LX/0Ot;

    .line 2736749
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736750
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->J:LX/0Ot;

    .line 2736751
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736752
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->K:LX/0Ot;

    .line 2736753
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736754
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->L:LX/0Ot;

    .line 2736755
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736756
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->M:LX/0Ot;

    .line 2736757
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736758
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->N:LX/0Ot;

    .line 2736759
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736760
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    .line 2736761
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736762
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->P:LX/0Ot;

    .line 2736763
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736764
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Q:LX/0Ot;

    .line 2736765
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736766
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->R:LX/0Ot;

    .line 2736767
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736768
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->S:LX/0Ot;

    .line 2736769
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736770
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->T:LX/0Ot;

    .line 2736771
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736772
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->U:LX/0Ot;

    .line 2736773
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736774
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->V:LX/0Ot;

    .line 2736775
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736776
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->W:LX/0Ot;

    .line 2736777
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736778
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->X:LX/0Ot;

    .line 2736779
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736780
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Y:LX/0Ot;

    .line 2736781
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2736782
    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Z:LX/0Ot;

    .line 2736783
    iput-object p1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    .line 2736784
    iput-object p2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->b:LX/6Po;

    .line 2736785
    iput-object p3, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->c:LX/0Or;

    .line 2736786
    iput-object p4, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->d:LX/0Or;

    .line 2736787
    iput-object p5, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->e:LX/2Mq;

    .line 2736788
    iput-object p6, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->f:LX/0Or;

    .line 2736789
    iput-object p7, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->g:LX/0Or;

    .line 2736790
    iput-object p8, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->h:LX/Iuh;

    .line 2736791
    return-void
.end method

.method private a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736792
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736793
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    sget-object v1, LX/B9l;->GRAPH:LX/B9l;

    invoke-static {v1}, LX/B9o;->a(LX/B9l;)LX/B9o;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B9n;->a(LX/B9o;)V

    .line 2736794
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2736795
    return-object v0

    .line 2736796
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2736797
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    sget-object v2, LX/B9l;->GRAPH:LX/B9l;

    invoke-static {v1, v2}, LX/B9o;->a(Ljava/lang/Throwable;LX/B9l;)LX/B9o;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/B9n;->a(LX/B9o;)V

    .line 2736798
    throw v1
.end method

.method private a(LX/2VK;Lcom/facebook/messaging/service/model/ModifyThreadParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2736799
    iget-boolean v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    move v0, v0

    .line 2736800
    if-eqz v0, :cond_0

    .line 2736801
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "setThreadName"

    .line 2736802
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736803
    move-object v0, v0

    .line 2736804
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736805
    move-object v0, v0

    .line 2736806
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736807
    const-string p3, "setThreadName"

    .line 2736808
    :cond_0
    iget-boolean v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->f:Z

    move v0, v0

    .line 2736809
    if-eqz v0, :cond_1

    .line 2736810
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "setThreadImage"

    .line 2736811
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736812
    move-object v0, v0

    .line 2736813
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736814
    move-object v0, v0

    .line 2736815
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736816
    const-string p3, "setThreadImage"

    .line 2736817
    :cond_1
    iget-boolean v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->g:Z

    move v0, v0

    .line 2736818
    if-eqz v0, :cond_2

    .line 2736819
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "muteThread"

    .line 2736820
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736821
    move-object v0, v0

    .line 2736822
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736823
    move-object v0, v0

    .line 2736824
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736825
    const-string p3, "muteThread"

    .line 2736826
    :cond_2
    iget-boolean v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->n:Z

    move v0, v0

    .line 2736827
    if-eqz v0, :cond_3

    .line 2736828
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "setThreadEphemerality"

    .line 2736829
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736830
    move-object v0, v0

    .line 2736831
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736832
    move-object v0, v0

    .line 2736833
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736834
    const-string p3, "setThreadEphemerality"

    .line 2736835
    :cond_3
    iget-boolean v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    move v0, v0

    .line 2736836
    if-nez v0, :cond_4

    .line 2736837
    iget-boolean v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    move v0, v0

    .line 2736838
    if-eqz v0, :cond_5

    .line 2736839
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "setThreadTheme"

    .line 2736840
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736841
    move-object v0, v0

    .line 2736842
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736843
    move-object v0, v0

    .line 2736844
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736845
    const-string p3, "setThreadTheme"

    .line 2736846
    :cond_5
    iget-object v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    move-object v0, v0

    .line 2736847
    if-eqz v0, :cond_6

    .line 2736848
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "setThreadParticipantNickname"

    .line 2736849
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736850
    move-object v0, v0

    .line 2736851
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736852
    move-object v0, v0

    .line 2736853
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736854
    const-string p3, "setThreadParticipantNickname"

    .line 2736855
    :cond_6
    iget-object v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->r:LX/03R;

    move-object v0, v0

    .line 2736856
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2736857
    const-string v1, "changeJoinableMode"

    .line 2736858
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->V:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 2736859
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736860
    move-object v0, v0

    .line 2736861
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736862
    move-object v0, v0

    .line 2736863
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    move-object p3, v1

    .line 2736864
    :cond_7
    iget-object v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->q:LX/03R;

    move-object v0, v0

    .line 2736865
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2736866
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->W:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "changeApprovalMode"

    .line 2736867
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736868
    move-object v0, v0

    .line 2736869
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736870
    move-object v0, v0

    .line 2736871
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736872
    const-string p3, "changeApprovalMode"

    .line 2736873
    :cond_8
    iget-object v0, p2, Lcom/facebook/messaging/service/model/ModifyThreadParams;->s:Ljava/lang/String;

    move-object v0, v0

    .line 2736874
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2736875
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "changeGroupDescription"

    .line 2736876
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736877
    move-object v0, v0

    .line 2736878
    iput-object p3, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736879
    move-object v0, v0

    .line 2736880
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736881
    const-string p3, "changeGroupDescription"

    .line 2736882
    :cond_9
    return-object p3
.end method

.method private static a(Lcom/facebook/messaging/protocol/WebServiceHandler;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/protocol/WebServiceHandler;",
            "LX/0Ot",
            "<",
            "LX/FKg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FLA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FLB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Oo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/01T;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL4;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FL7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKh;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendApiHandler;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKq;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FO7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JsP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3NC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FLC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FFQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3ND;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3NB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FKU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2736883
    iput-object p1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->i:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->j:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->l:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->m:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->n:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->o:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->p:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->q:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->r:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->s:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->t:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->u:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->v:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->w:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->x:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->y:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->z:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->A:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->B:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->C:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->D:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->E:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->F:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->G:LX/0Ot;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->H:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->I:LX/0Ot;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->J:LX/0Ot;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->K:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->L:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->M:LX/0Ot;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->N:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->P:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Q:LX/0Ot;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->R:LX/0Ot;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->S:LX/0Ot;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->T:LX/0Ot;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->U:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->V:LX/0Ot;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->W:LX/0Ot;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->X:LX/0Ot;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Y:LX/0Ot;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Z:LX/0Ot;

    return-void
.end method

.method private b()Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2736884
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKl;

    invoke-virtual {v0}, LX/FKl;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2736885
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2736886
    if-eqz v0, :cond_0

    .line 2736887
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    sget-object v2, LX/B9l;->MQTT:LX/B9l;

    invoke-static {v2}, LX/B9o;->a(LX/B9l;)LX/B9o;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/B9n;->a(LX/B9o;)V

    .line 2736888
    :goto_0
    return-object v1

    .line 2736889
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    sget-object v2, LX/B9l;->MQTT:LX/B9l;

    .line 2736890
    iget-object v3, v1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v3, v3

    .line 2736891
    new-instance v4, LX/B9o;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct {v4, v5, v2, v3, v6}, LX/B9o;-><init>(Ljava/lang/Throwable;LX/B9l;Ljava/lang/String;Z)V

    move-object v2, v4

    .line 2736892
    invoke-virtual {v0, v2}, LX/B9n;->a(LX/B9o;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2736893
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2736894
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    sget-object v2, LX/B9l;->MQTT:LX/B9l;

    invoke-static {v1, v2}, LX/B9o;->a(Ljava/lang/Throwable;LX/B9l;)LX/B9o;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/B9n;->a(LX/B9o;)V

    .line 2736895
    throw v1
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/protocol/WebServiceHandler;
    .locals 47

    .prologue
    .line 2736896
    new-instance v2, Lcom/facebook/messaging/protocol/WebServiceHandler;

    invoke-static/range {p0 .. p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static/range {p0 .. p0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v4

    check-cast v4, LX/6Po;

    const/16 v5, 0x14ea

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x14e2

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v7

    check-cast v7, LX/2Mq;

    const/16 v8, 0x27d7

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x2680

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/Iuh;->a(LX/0QB;)LX/Iuh;

    move-result-object v10

    check-cast v10, LX/Iuh;

    invoke-direct/range {v2 .. v10}, Lcom/facebook/messaging/protocol/WebServiceHandler;-><init>(LX/18V;LX/6Po;LX/0Or;LX/0Or;LX/2Mq;LX/0Or;LX/0Or;LX/Iuh;)V

    .line 2736897
    const/16 v3, 0x2946

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2945

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xac0

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2932

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2948

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2934

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2936

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2963

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2964

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2962

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2954

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x2938

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x1a30

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x2941

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x2940

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x3df

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x295a

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x295d

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x295b

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x295e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x295f

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x294a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x294b

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x295c

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const/16 v27, 0x2960

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x293d

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x2947

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const/16 v30, 0x292b

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const/16 v31, 0x2942

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x2933

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x2953

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0x2951

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v34

    const/16 v35, 0x2a01

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v35

    const/16 v36, 0x2a00

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const/16 v37, 0x2a3e

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v37

    const/16 v38, 0x292d

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v38

    const/16 v39, 0xdbb

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    const/16 v40, 0x2965

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v40

    const/16 v41, 0x279b

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v41

    const/16 v42, 0x293b

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v42

    const/16 v43, 0x2939

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v43

    const/16 v44, 0xd8f

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v44

    const/16 v45, 0xdba

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v45

    const/16 v46, 0x293a

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v46

    invoke-static/range {v2 .. v46}, Lcom/facebook/messaging/protocol/WebServiceHandler;->a(Lcom/facebook/messaging/protocol/WebServiceHandler;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2736898
    return-object v2
.end method


# virtual methods
.method public final A(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736899
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736900
    const-string v1, "montageAudienceMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2736901
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v2, v0, v1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736902
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736903
    return-object v0
.end method

.method public final B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2736656
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736657
    sget-object v1, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;

    .line 2736658
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736659
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736660
    iget-wide v3, v0, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->b:J

    move-wide v0, v3

    .line 2736661
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 2736662
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Oo;

    sget-object v2, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    .line 2736663
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736664
    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, LX/6Oo;->a(LX/0Rf;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/6Ol;

    invoke-direct {v4, v0}, LX/6Ol;-><init>(LX/6Oo;)V

    invoke-static {v3, v4}, LX/2Ck;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2736665
    const v1, 0x3d6a4135

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2736666
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736904
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736905
    sget-object v1, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;

    .line 2736906
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736907
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736908
    return-object v0
.end method

.method public final D(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736909
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final E(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736910
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final F(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736911
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736912
    sget-object v1, Lcom/facebook/messaging/service/model/FetchGroupInviteLinkParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupInviteLinkParams;

    .line 2736913
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->K:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2736914
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final G(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736916
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final H(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736968
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736969
    sget-object v1, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;

    .line 2736970
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->L:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;

    .line 2736971
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final I(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736964
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736965
    sget-object v1, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;

    .line 2736966
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->M:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupResult;

    .line 2736967
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final J(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736960
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736961
    sget-object v1, Lcom/facebook/messaging/service/model/PostGameScoreParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/PostGameScoreParams;

    .line 2736962
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->N:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/PostGameScoreResult;

    .line 2736963
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final K(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2736954
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736955
    sget-object v1, Lcom/facebook/messaging/service/model/EditUsernameParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/EditUsernameParams;

    .line 2736956
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JsP;

    .line 2736957
    iget-object p0, v0, Lcom/facebook/messaging/service/model/EditUsernameParams;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2736958
    invoke-virtual {v1, v0}, LX/JsP;->b(Ljava/lang/String;)Lcom/facebook/messaging/service/model/EditUsernameResult;

    move-result-object v0

    .line 2736959
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final L(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2736948
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736949
    sget-object v1, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;

    .line 2736950
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FFQ;

    .line 2736951
    iget-object p0, v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, p0

    .line 2736952
    invoke-virtual {v1, v0}, LX/FFQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;

    move-result-object v0

    .line 2736953
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final M(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736947
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final N(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736946
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736940
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736941
    const-string v1, "fetchPinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2736942
    sget-object v1, LX/6iH;->STANDARD_GROUP:LX/6iH;

    invoke-static {v0, v1}, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;LX/6iH;)Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v1

    move-object v1, v1

    .line 2736943
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736944
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736945
    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final P(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736934
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736935
    const-string v1, "fetchPinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2736936
    sget-object v1, LX/6iH;->ROOM:LX/6iH;

    invoke-static {v0, v1}, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;LX/6iH;)Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v1

    move-object v1, v1

    .line 2736937
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736938
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736939
    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final Q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736915
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2736917
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x390

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2736918
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    const-string v1, "Messaging disabled."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2736919
    :goto_0
    return-object v0

    .line 2736920
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736921
    const-string v1, "logger_instance_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2736922
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->e:LX/2Mq;

    const p2, 0x540012

    .line 2736923
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0x18

    invoke-interface {v4, p2, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2736924
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v5, "GRAPH_QL"

    invoke-interface {v4, p2, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2736925
    const-string v1, "fetchThreadListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2736926
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2736927
    invoke-virtual {v1}, LX/6ek;->isMessageRequestFolders()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x18c

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2736928
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->P:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FO7;

    .line 2736929
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736930
    invoke-virtual {v1, v0, v2}, LX/FO7;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2736931
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736932
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736933
    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2736667
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x390

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2736668
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    const-string v1, "Messaging disabled."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2736669
    :goto_0
    return-object v0

    .line 2736670
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736671
    const-string v1, "fetchMoreThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    .line 2736672
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v1, v1

    .line 2736673
    invoke-virtual {v1}, LX/6ek;->isMessageRequestFolders()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x18c

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2736674
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->P:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FO7;

    .line 2736675
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736676
    invoke-virtual {v1, v0, v2}, LX/FO7;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2736677
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736678
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736679
    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2736680
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x390

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2736681
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    const-string v1, "Messaging disabled."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2736682
    :goto_0
    return-object v0

    .line 2736683
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736684
    const-string v1, "logger_instance_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2736685
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->e:LX/2Mq;

    const p2, 0x540011

    .line 2736686
    iget-object v3, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v4, 0x18

    invoke-interface {v3, p2, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2736687
    iget-object v3, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v4, "GRAPH_QL"

    invoke-interface {v3, p2, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2736688
    const-string v1, "fetchThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2736689
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736690
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736691
    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2736692
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->h:LX/Iuh;

    invoke-virtual {v1, v0}, LX/Iuh;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2736693
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736405
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736537
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736538
    const-string v1, "addMembersParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AddMembersParams;

    .line 2736539
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2736540
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "add-members"

    .line 2736541
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736542
    move-object v0, v0

    .line 2736543
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736544
    const-string v0, "addMembers"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736545
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736546
    return-object v0
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2736496
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736497
    const-string v1, "createGroupParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/CreateGroupParams;

    .line 2736498
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2736499
    const-string v2, "modifyThreadParams"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2736500
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v2}, LX/18V;->a()LX/2VK;

    move-result-object v3

    .line 2736501
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->H:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-static {v2, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    const-string v4, "create-group"

    .line 2736502
    iput-object v4, v2, LX/2Vk;->c:Ljava/lang/String;

    .line 2736503
    move-object v2, v2

    .line 2736504
    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {v3, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 2736505
    if-eqz v1, :cond_2

    .line 2736506
    iget-object v2, v1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2736507
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2736508
    iget-object v2, v1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 2736509
    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2736510
    new-instance v2, LX/6ih;

    invoke-direct {v2, v1}, LX/6ih;-><init>(Lcom/facebook/messaging/service/model/ModifyThreadParams;)V

    move-object v1, v2

    .line 2736511
    :goto_1
    iget-object v2, v0, Lcom/facebook/messaging/service/model/CreateGroupParams;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v2

    .line 2736512
    iget-boolean v2, v1, LX/6ih;->e:Z

    move v2, v2

    .line 2736513
    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 2736514
    invoke-virtual {v1, v0}, LX/6ih;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ih;

    .line 2736515
    :cond_0
    const-string v0, "{result=create-group:$.id}"

    .line 2736516
    iput-object v0, v1, LX/6ih;->b:Ljava/lang/String;

    .line 2736517
    move-object v0, v1

    .line 2736518
    invoke-virtual {v0}, LX/6ih;->u()Lcom/facebook/messaging/service/model/ModifyThreadParams;

    move-result-object v0

    .line 2736519
    const-string v1, "create-group"

    invoke-direct {p0, v3, v0, v1}, Lcom/facebook/messaging/protocol/WebServiceHandler;->a(LX/2VK;Lcom/facebook/messaging/service/model/ModifyThreadParams;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2736520
    new-instance v0, LX/6iM;

    invoke-direct {v0}, LX/6iM;-><init>()V

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2736521
    iput-object v2, v0, LX/6iM;->b:LX/0rS;

    .line 2736522
    move-object v2, v0

    .line 2736523
    const-string v0, "{result=create-group:$.id}"

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v0

    .line 2736524
    iput-object v0, v2, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 2736525
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v2}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v2

    invoke-static {v0, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 2736526
    const-string v2, "fetch-thread"

    .line 2736527
    iput-object v2, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736528
    move-object v0, v0

    .line 2736529
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736530
    move-object v0, v0

    .line 2736531
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v3, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736532
    const-string v0, "createGroup"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v3, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736533
    const-string v0, "fetch-thread"

    invoke-interface {v3, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2736534
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2736535
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 2736536
    :cond_2
    new-instance v1, LX/6ih;

    invoke-direct {v1}, LX/6ih;-><init>()V

    goto :goto_1
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2736487
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736488
    const-string v1, "createThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    .line 2736489
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->J:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/service/SendApiHandler;

    .line 2736490
    invoke-static {v1, v0}, Lcom/facebook/messaging/send/service/SendApiHandler;->b(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    move-object v1, v2

    .line 2736491
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2736492
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 2736493
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->R:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itm;

    .line 2736494
    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v3

    .line 2736495
    sget-object v3, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v1, v2, v0, v3}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v0

    throw v0
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736479
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x390

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2736480
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    const-string v1, "Messaging disabled."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2736481
    :goto_0
    return-object v0

    .line 2736482
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736483
    const-string v1, "fetchMoreMessagesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    .line 2736484
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736485
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736486
    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2736475
    :try_start_0
    invoke-super {p0, p1, p2}, LX/Dnx;->handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2736476
    :catch_0
    move-exception v1

    .line 2736477
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8CH;

    invoke-virtual {v0, v1}, LX/8CH;->a(Ljava/lang/Exception;)V

    .line 2736478
    throw v1
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2736456
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736457
    const-string v1, "removeMemberParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/RemoveMemberParams;

    .line 2736458
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2736459
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v3, "remove-members"

    .line 2736460
    iput-object v3, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 2736461
    move-object v1, v1

    .line 2736462
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v2, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 2736463
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->a:Z

    move v0, v1

    .line 2736464
    if-eqz v0, :cond_0

    .line 2736465
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    sget-object v1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch-pinned-threads"

    .line 2736466
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736467
    move-object v0, v0

    .line 2736468
    const-string v1, "remove-members"

    .line 2736469
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736470
    move-object v0, v0

    .line 2736471
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736472
    :cond_0
    const-string v0, "removeMember"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736473
    const-string v0, "fetch-pinned-threads"

    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2736474
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2736425
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736426
    const-string v1, "markThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2736427
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->T:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FLC;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2736428
    iget-boolean v4, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->b:Z

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/FLC;->g:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    .line 2736429
    goto :goto_1

    .line 2736430
    :cond_0
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736431
    return-object v0

    .line 2736432
    :goto_1
    iget-object v4, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ne v4, v2, :cond_1

    .line 2736433
    :goto_2
    if-eqz v2, :cond_2

    .line 2736434
    iget-object v2, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2736435
    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 2736436
    iget-object v4, v1, LX/FLC;->e:LX/3QL;

    sget-object p0, LX/FCY;->MQTT:LX/FCY;

    invoke-virtual {v4, v3, v2, p0}, LX/3QL;->a(LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;LX/FCY;)V

    .line 2736437
    iget-object v4, v1, LX/FLC;->f:LX/FKZ;

    invoke-virtual {v4, v3, v2}, LX/FKZ;->a(LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;)Z

    move-result v4

    move v3, v4

    .line 2736438
    if-nez v3, :cond_0

    .line 2736439
    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 2736440
    iget-object v4, v1, LX/FLC;->e:LX/3QL;

    sget-object p0, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {v4, v3, v2, p0}, LX/3QL;->a(LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;LX/FCY;)V

    .line 2736441
    iget-object v4, v1, LX/FLC;->a:LX/18V;

    invoke-static {v1, v3}, LX/FLC;->a(LX/FLC;LX/6iW;)LX/FKJ;

    move-result-object p0

    invoke-virtual {v4, p0, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736442
    goto :goto_0

    :cond_1
    move v2, v3

    .line 2736443
    goto :goto_2

    .line 2736444
    :cond_2
    iget-object v4, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 2736445
    iget-object v2, v1, LX/FLC;->a:LX/18V;

    invoke-virtual {v2}, LX/18V;->a()LX/2VK;

    move-result-object p0

    .line 2736446
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    iget-object v2, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 2736447
    iget-object v2, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2736448
    iget-object p1, v1, LX/FLC;->e:LX/3QL;

    sget-object p2, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {p1, v4, v2, p2}, LX/3QL;->a(LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;LX/FCY;)V

    .line 2736449
    invoke-static {v1, v4}, LX/FLC;->a(LX/FLC;LX/6iW;)LX/FKJ;

    move-result-object p1

    invoke-static {p1, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "mark-thread-"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, "-"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {v4}, LX/6iW;->getApiName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2736450
    iput-object p1, v2, LX/2Vk;->c:Ljava/lang/String;

    .line 2736451
    move-object v2, v2

    .line 2736452
    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {p0, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 2736453
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 2736454
    :cond_3
    const-string v2, "markMultipleThreads"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {p0, v2, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736455
    goto/16 :goto_0
.end method

.method public final k(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736420
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736421
    const-string v1, "blockUserParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/BlockUserParams;

    .line 2736422
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736423
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736424
    return-object v0
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2736407
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736408
    const-string v1, "deleteThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    .line 2736409
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v3, v1

    .line 2736410
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v4

    .line 2736411
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 2736412
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    new-instance v5, Lcom/facebook/messaging/service/model/DeleteThreadParams;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v5, v1}, Lcom/facebook/messaging/service/model/DeleteThreadParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-static {v0, v5}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "thread-key-"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2736413
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736414
    move-object v0, v0

    .line 2736415
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v4, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736416
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2736417
    :cond_0
    const-string v0, "deleteThreads"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736418
    const-string v0, "fetch-thread"

    invoke-interface {v4, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2736419
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final m(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736406
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736400
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736401
    const-string v1, "deleteMessagesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    .line 2736402
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736403
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736404
    return-object v0
.end method

.method public final o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736547
    const-string v0, "modifyThreadParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2736548
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v1

    .line 2736549
    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/messaging/protocol/WebServiceHandler;->a(LX/2VK;Lcom/facebook/messaging/service/model/ModifyThreadParams;Ljava/lang/String;)Ljava/lang/String;

    .line 2736550
    const-string v0, "modifyThread"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736551
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736552
    return-object v0
.end method

.method public final p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736553
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2736554
    invoke-direct {p0}, Lcom/facebook/messaging/protocol/WebServiceHandler;->b()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2736555
    :goto_0
    return-object v0

    .line 2736556
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/protocol/WebServiceHandler;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736557
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736558
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method

.method public final s(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736559
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736560
    const-string v1, "setSettingsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SetSettingsParams;

    .line 2736561
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->G:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736562
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2736563
    return-object v0
.end method

.method public final t(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2736564
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736565
    const-string v1, "searchThreadNameAndParticipantsParam"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;

    .line 2736566
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->X:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3ND;

    invoke-virtual {v1}, LX/3ND;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2736567
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->Y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3NB;

    invoke-virtual {v1, v0}, LX/3NB;->a(Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;)Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    .line 2736568
    :goto_0
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2736569
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->S:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3NC;

    invoke-virtual {v1, v0}, LX/3NC;->a(Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;)Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736570
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736571
    const-string v1, "fetchPinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2736572
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2736573
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2736574
    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736575
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736576
    const-string v1, "updatePinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;

    .line 2736577
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2736578
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "update-pinned-threads"

    .line 2736579
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736580
    move-object v0, v0

    .line 2736581
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736582
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    sget-object v1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch-pinned-threads"

    .line 2736583
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736584
    move-object v0, v0

    .line 2736585
    const-string v1, "update-pinned-threads"

    .line 2736586
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736587
    move-object v0, v0

    .line 2736588
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736589
    const-string v0, "updatePinnedThreads"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736590
    const-string v0, "fetch-pinned-threads"

    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2736591
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736592
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736593
    const-string v1, "addPinnedThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;

    .line 2736594
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2736595
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "add-pinned-thread"

    .line 2736596
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736597
    move-object v0, v0

    .line 2736598
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736599
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    sget-object v1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch-pinned-threads"

    .line 2736600
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736601
    move-object v0, v0

    .line 2736602
    const-string v1, "add-pinned-thread"

    .line 2736603
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736604
    move-object v0, v0

    .line 2736605
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736606
    const-string v0, "addPinnedThread"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736607
    const-string v0, "fetch-pinned-threads"

    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2736608
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2736609
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2736610
    const-string v1, "unpinThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UnpinThreadParams;

    .line 2736611
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2736612
    iget-object v1, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "unpin-thread"

    .line 2736613
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736614
    move-object v0, v0

    .line 2736615
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736616
    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    sget-object v1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch-pinned-threads"

    .line 2736617
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2736618
    move-object v0, v0

    .line 2736619
    const-string v1, "unpin-thread"

    .line 2736620
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2736621
    move-object v0, v0

    .line 2736622
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2736623
    const-string v0, "unpinThread"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2736624
    const-string v0, "fetch-pinned-threads"

    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2736625
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final y(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2736626
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2736627
    const-string v0, "threadKey"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2736628
    const-string v2, "attachment"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/attachments/OtherAttachmentData;

    .line 2736629
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736630
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736631
    const/4 v3, 0x0

    .line 2736632
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v4, 0x152

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2736633
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->O:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    iget-object v4, v1, Lcom/facebook/messaging/attachments/OtherAttachmentData;->e:Ljava/lang/String;

    iget-object v1, v1, Lcom/facebook/messaging/attachments/OtherAttachmentData;->f:Ljava/lang/String;

    .line 2736634
    iget-object v5, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v5

    .line 2736635
    new-instance v6, LX/5Z2;

    invoke-direct {v6}, LX/5Z2;-><init>()V

    move-object v6, v6

    .line 2736636
    new-instance v7, LX/4H3;

    invoke-direct {v7}, LX/4H3;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4H3;->a(Ljava/lang/String;)LX/4H3;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/4H3;->b(Ljava/lang/String;)LX/4H3;

    move-result-object v7

    .line 2736637
    const-string v8, "thread_msg_id"

    invoke-virtual {v6, v8, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2736638
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2736639
    iget-object v7, v2, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v7}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v7

    .line 2736640
    iput-object v7, v6, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2736641
    iput-object v5, v6, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2736642
    invoke-static {v2, v6}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2736643
    iget-object v7, v2, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2736644
    new-instance v7, LX/FOB;

    invoke-direct {v7, v2, v1}, LX/FOB;-><init>(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Ljava/lang/String;)V

    .line 2736645
    sget-object v8, LX/131;->INSTANCE:LX/131;

    move-object v8, v8

    .line 2736646
    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2736647
    const v1, 0x58eaf3f4

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2736648
    if-eqz v0, :cond_2

    .line 2736649
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2736650
    :goto_0
    if-nez v0, :cond_1

    .line 2736651
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    const-string v1, "Received a null URI for attachment."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2736652
    :goto_1
    return-object v0

    .line 2736653
    :cond_0
    iget-object v2, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->a:LX/18V;

    iget-object v0, p0, Lcom/facebook/messaging/protocol/WebServiceHandler;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v2, v0, v1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    goto :goto_0

    .line 2736654
    :cond_1
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_0
.end method

.method public final z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2736655
    new-instance v0, LX/Jp0;

    invoke-direct {v0, p1}, LX/Jp0;-><init>(LX/1qK;)V

    throw v0
.end method
