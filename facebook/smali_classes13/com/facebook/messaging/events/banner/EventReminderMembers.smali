.class public Lcom/facebook/messaging/events/banner/EventReminderMembers;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/events/banner/EventReminderMembers;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2727543
    new-instance v0, LX/JjL;

    invoke-direct {v0}, LX/JjL;-><init>()V

    sput-object v0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2727550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727551
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 2727552
    sget-object v0, Lcom/facebook/user/model/User;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    .line 2727553
    sget-object v0, Lcom/facebook/user/model/User;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->c:LX/0Px;

    .line 2727554
    sget-object v0, Lcom/facebook/user/model/User;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->d:LX/0Px;

    .line 2727555
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;LX/0Px;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2727556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727557
    iput-object p1, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 2727558
    iput-object p2, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    .line 2727559
    iput-object p3, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->c:LX/0Px;

    .line 2727560
    iput-object p4, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->d:LX/0Px;

    .line 2727561
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2727549
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2727544
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727545
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2727546
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2727547
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2727548
    return-void
.end method
