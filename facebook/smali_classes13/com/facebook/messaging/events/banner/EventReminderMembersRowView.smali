.class public Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/JjK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/25T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2727611
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2727612
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a()V

    .line 2727613
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727608
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2727609
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a()V

    .line 2727610
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727605
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2727606
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a()V

    .line 2727607
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2727599
    const-class v0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2727600
    const v0, 0x7f03050b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2727601
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->setOrientation(I)V

    .line 2727602
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->b()V

    .line 2727603
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->d()V

    .line 2727604
    return-void
.end method

.method private static a(Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;LX/JjK;LX/25T;)V
    .locals 0

    .prologue
    .line 2727598
    iput-object p1, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a:LX/JjK;

    iput-object p2, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->b:LX/25T;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    new-instance p1, LX/JjK;

    invoke-static {v1}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object v0

    check-cast v0, LX/FJw;

    invoke-direct {p1, v0}, LX/JjK;-><init>(LX/FJw;)V

    move-object v0, p1

    check-cast v0, LX/JjK;

    invoke-static {v1}, LX/25T;->a(LX/0QB;)LX/25T;

    move-result-object v1

    check-cast v1, LX/25T;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a(Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;LX/JjK;LX/25T;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2727595
    const v0, 0x7f0d0e51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2727596
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->c()V

    .line 2727597
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2727579
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    if-nez v0, :cond_0

    .line 2727580
    :goto_0
    return-void

    .line 2727581
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727582
    iget-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    move-object v0, v1

    .line 2727583
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2727584
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2727585
    if-lez v0, :cond_1

    .line 2727586
    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f013c

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727587
    :cond_1
    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727588
    iget-object v3, v2, Lcom/facebook/messaging/events/banner/EventReminderMembers;->c:LX/0Px;

    move-object v2, v3

    .line 2727589
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 2727590
    if-lez v2, :cond_3

    .line 2727591
    if-lez v0, :cond_2

    .line 2727592
    const-string v0, " \u22c5 "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727593
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f013d

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727594
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2727574
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->b:LX/25T;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2727575
    const v0, 0x7f0d0e52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2727576
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a:LX/JjK;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2727577
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->b:LX/25T;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2727578
    return-void
.end method


# virtual methods
.method public setMembers(Lcom/facebook/messaging/events/banner/EventReminderMembers;)V
    .locals 3

    .prologue
    .line 2727562
    iput-object p1, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727563
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->a:LX/JjK;

    .line 2727564
    iget-object v1, p1, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    move-object v1, v1

    .line 2727565
    iput-object v1, v0, LX/JjK;->c:LX/0Px;

    .line 2727566
    iget-object v1, p1, Lcom/facebook/messaging/events/banner/EventReminderMembers;->c:LX/0Px;

    move-object v1, v1

    .line 2727567
    iput-object v1, v0, LX/JjK;->d:LX/0Px;

    .line 2727568
    iget-object v1, p1, Lcom/facebook/messaging/events/banner/EventReminderMembers;->d:LX/0Px;

    move-object v1, v1

    .line 2727569
    iput-object v1, v0, LX/JjK;->e:LX/0Px;

    .line 2727570
    iget-object v1, v0, LX/JjK;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-object v2, v0, LX/JjK;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, v0, LX/JjK;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, LX/JjK;->b:I

    .line 2727571
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2727572
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->c()V

    .line 2727573
    return-void
.end method
