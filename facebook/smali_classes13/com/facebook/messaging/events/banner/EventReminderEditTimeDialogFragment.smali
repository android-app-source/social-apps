.class public Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JjT;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjy;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjx;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjj;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dde;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/Calendar;

.field public t:Lcom/facebook/messaging/events/banner/EventReminderParams;

.field public u:J

.field public v:LX/Jjb;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2727372
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2727373
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727374
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->n:LX/0Ot;

    .line 2727375
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727376
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->o:LX/0Ot;

    .line 2727377
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727378
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->p:LX/0Ot;

    .line 2727379
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727380
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->q:LX/0Ot;

    .line 2727381
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727382
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->r:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/messaging/events/banner/EventReminderParams;)Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;
    .locals 2

    .prologue
    .line 2727425
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727426
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2727427
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2727428
    const-string v1, "reminder_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2727429
    new-instance v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;-><init>()V

    .line 2727430
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2727431
    return-object v1

    .line 2727432
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;)V
    .locals 2

    .prologue
    .line 2727421
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->k:Ljava/lang/String;

    .line 2727422
    if-eqz v1, :cond_0

    .line 2727423
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dde;

    invoke-virtual {v0, v1}, LX/Dde;->a(Ljava/lang/String;)V

    .line 2727424
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;Z)V
    .locals 2

    .prologue
    .line 2727417
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->k:Ljava/lang/String;

    .line 2727418
    if-eqz v1, :cond_0

    .line 2727419
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dde;

    invoke-virtual {v0, v1, p1}, LX/Dde;->a(Ljava/lang/String;Z)V

    .line 2727420
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 2727415
    new-instance v0, LX/3uc;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e054a

    invoke-direct {v0, v1, v2}, LX/3uc;-><init>(Landroid/content/Context;I)V

    .line 2727416
    new-instance v1, LX/K9a;

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    new-instance v3, LX/JjC;

    invoke-direct {v3, p0, v0}, LX/JjC;-><init>(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;LX/3uc;)V

    invoke-direct {v1, v0, v2, v3}, LX/K9a;-><init>(Landroid/content/Context;Ljava/util/Calendar;LX/JjC;)V

    return-object v1
.end method

.method public final b(LX/0gc;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2727412
    invoke-virtual {p1}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2727413
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2727414
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x322460b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2727387
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2727388
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v3, p0

    check-cast v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0x2772

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v7, 0x2779

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2778

    invoke-static {p1, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2774

    invoke-static {p1, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v0, 0x26ff

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v4, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->m:LX/0SG;

    iput-object v5, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->n:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->o:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->p:LX/0Ot;

    iput-object v9, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->q:LX/0Ot;

    iput-object p1, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->r:LX/0Ot;

    .line 2727389
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2727390
    const-string v2, "reminder_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderParams;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2727391
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-static {v0}, Lcom/facebook/messaging/events/banner/EventReminderParams;->a(Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/JjW;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    const-string v3, "reminder_customization"

    .line 2727392
    iget-object v4, v0, LX/JjW;->l:Ljava/lang/String;

    iput-object v4, v0, LX/JjW;->n:Ljava/lang/String;

    .line 2727393
    iget-object v4, v0, LX/JjW;->m:Ljava/lang/String;

    iput-object v4, v0, LX/JjW;->o:Ljava/lang/String;

    .line 2727394
    iput-object v2, v0, LX/JjW;->l:Ljava/lang/String;

    .line 2727395
    iput-object v3, v0, LX/JjW;->m:Ljava/lang/String;

    .line 2727396
    move-object v0, v0

    .line 2727397
    invoke-virtual {v0}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2727398
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->w:Ljava/lang/String;

    .line 2727399
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->x:Ljava/lang/String;

    .line 2727400
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    .line 2727401
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->EVENT:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->y:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727402
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-wide v2, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 2727403
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-wide v2, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    iput-wide v2, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->u:J

    .line 2727404
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    iget-wide v2, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->u:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2727405
    :goto_1
    iput-boolean v6, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->z:Z

    .line 2727406
    const v0, 0x68d94009

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2727407
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->t:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    goto :goto_0

    .line 2727408
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->roll(II)V

    .line 2727409
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v6}, Ljava/util/Calendar;->set(II)V

    .line 2727410
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v6}, Ljava/util/Calendar;->set(II)V

    .line 2727411
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->s:Ljava/util/Calendar;

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v6}, Ljava/util/Calendar;->set(II)V

    goto :goto_1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2727383
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2727384
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->z:Z

    if-nez v0, :cond_0

    .line 2727385
    invoke-static {p0}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->a(Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;)V

    .line 2727386
    :cond_0
    return-void
.end method
