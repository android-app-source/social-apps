.class public Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2727931
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2727932
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727933
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;->m:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;

    const/16 p0, 0x2778

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;->m:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 2727934
    const-class v0, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2727935
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2727936
    const-string v1, "reminder_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2727937
    new-instance v2, LX/4me;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, LX/4me;-><init>(Landroid/content/Context;)V

    .line 2727938
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jjx;

    .line 2727939
    sget-object v3, LX/Jjw;->a:[I

    invoke-static {v1}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object v4

    invoke-virtual {v4}, LX/Jk2;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2727940
    const v3, 0x7f082dc1

    :goto_0
    move v1, v3

    .line 2727941
    invoke-virtual {v2, v1}, LX/4me;->setTitle(I)V

    .line 2727942
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jjx;

    .line 2727943
    sget-object v4, LX/Jjw;->a:[I

    invoke-static {v1}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object p1

    invoke-virtual {p1}, LX/Jk2;->ordinal()I

    move-result p1

    aget v4, v4, p1

    packed-switch v4, :pswitch_data_1

    .line 2727944
    const v4, 0x7f082dc4

    :goto_1
    move v1, v4

    .line 2727945
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2727946
    const/4 v3, -0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jjx;

    .line 2727947
    sget-object v5, LX/Jjw;->a:[I

    invoke-static {v1}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object p1

    invoke-virtual {p1}, LX/Jk2;->ordinal()I

    move-result p1

    aget v5, v5, p1

    packed-switch v5, :pswitch_data_2

    .line 2727948
    const v5, 0x7f082dc6

    :goto_2
    move v1, v5

    .line 2727949
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, LX/JjX;

    invoke-direct {v4, p0, v0}, LX/JjX;-><init>(Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;Lcom/facebook/messaging/events/banner/EventReminderParams;)V

    invoke-virtual {v2, v3, v1, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2727950
    const/4 v0, -0x2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f082dc8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, LX/JjY;

    invoke-direct {v3, p0}, LX/JjY;-><init>(Lcom/facebook/messaging/events/banner/EventReminderPromptCreationDialogFragment;)V

    invoke-virtual {v2, v0, v1, v3}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2727951
    return-object v2

    .line 2727952
    :pswitch_0
    const v3, 0x7f082dc2

    goto :goto_0

    .line 2727953
    :pswitch_1
    const v3, 0x7f082dc3

    goto :goto_0

    .line 2727954
    :pswitch_2
    const v4, 0x7f082dc5

    goto :goto_1

    .line 2727955
    :pswitch_3
    const v5, 0x7f082dc7

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
