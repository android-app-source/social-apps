.class public Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# instance fields
.field public A:Lcom/facebook/resources/ui/FbCheckedTextView;

.field public B:Lcom/facebook/resources/ui/FbCheckedTextView;

.field public C:Ljava/util/Calendar;

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/JjT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/Jjj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/JjO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/Jk1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/2Og;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/Jiz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DdT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

.field private q:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

.field public s:Lcom/facebook/messaging/events/banner/EventReminderParams;

.field private t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public u:Z

.field public v:Z

.field public w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

.field public x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

.field public y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

.field public z:Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2728176
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2728177
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728178
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->L:LX/0Ot;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2728179
    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->b()LX/3u1;

    move-result-object v1

    .line 2728180
    if-eqz v1, :cond_0

    .line 2728181
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728182
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v0, v2

    .line 2728183
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->CALL:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v0, v2, :cond_1

    const v0, 0x7f082db8

    :goto_0
    invoke-virtual {v1, v0}, LX/3u1;->b(I)V

    .line 2728184
    :cond_0
    return-void

    .line 2728185
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jjx;

    .line 2728186
    sget-object v2, LX/Jjw;->a:[I

    invoke-static {v0}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object p0

    invoke-virtual {p0}, LX/Jk2;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 2728187
    const v2, 0x7f082db5

    :goto_1
    move v0, v2

    .line 2728188
    goto :goto_0

    .line 2728189
    :pswitch_0
    const v2, 0x7f082db6

    goto :goto_1

    .line 2728190
    :pswitch_1
    const v2, 0x7f082db7

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;)V
    .locals 5

    .prologue
    .line 2728191
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2728192
    iget-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-object v0, v1

    .line 2728193
    if-ne v0, p1, :cond_0

    .line 2728194
    :goto_0
    return-void

    .line 2728195
    :cond_0
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 2728196
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->N:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2728197
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->q:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    .line 2728198
    invoke-virtual {v1, v0}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2728199
    iget-object v4, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->q:LX/0P1;

    invoke-virtual {v4, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 2728200
    :cond_2
    invoke-virtual {v2, v0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2728201
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->q:LX/0P1;

    .line 2728202
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->I:LX/JjO;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->q:LX/0P1;

    invoke-virtual {v0, v1, v2}, LX/JjO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0P1;)Lcom/facebook/messaging/events/banner/EventReminderMembers;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2728203
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->z:Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->setMembers(Lcom/facebook/messaging/events/banner/EventReminderMembers;)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2728171
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->J:LX/Jk1;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sget-object v1, LX/Jk0;->RELATIVE:LX/Jk0;

    invoke-virtual {v0, v2, v3, v1}, LX/Jk1;->a(JLX/Jk0;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2728172
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2728173
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-static {p1}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v2

    check-cast v2, LX/67X;

    invoke-static {p1}, LX/JjT;->b(LX/0QB;)LX/JjT;

    move-result-object v3

    check-cast v3, LX/JjT;

    invoke-static {p1}, LX/Jjj;->a(LX/0QB;)LX/Jjj;

    move-result-object v4

    check-cast v4, LX/Jjj;

    invoke-static {p1}, LX/JjO;->b(LX/0QB;)LX/JjO;

    move-result-object v5

    check-cast v5, LX/JjO;

    invoke-static {p1}, LX/Jk1;->b(LX/0QB;)LX/Jk1;

    move-result-object v6

    check-cast v6, LX/Jk1;

    invoke-static {p1}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v7

    check-cast v7, LX/2Og;

    const/16 v8, 0x2778

    invoke-static {p1, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p1}, LX/Jiz;->b(LX/0QB;)LX/Jiz;

    move-result-object v9

    check-cast v9, LX/Jiz;

    const/16 v10, 0x12cd

    invoke-static {p1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v0, 0x26ef

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->F:LX/67X;

    iput-object v3, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->G:LX/JjT;

    iput-object v4, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->H:LX/Jjj;

    iput-object v5, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->I:LX/JjO;

    iput-object v6, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->J:LX/Jk1;

    iput-object v7, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->K:LX/2Og;

    iput-object v8, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->L:LX/0Ot;

    iput-object v9, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->M:LX/Jiz;

    iput-object v10, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->N:LX/0Or;

    iput-object p1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->O:LX/0Or;

    .line 2728174
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->F:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2728175
    return-void
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 2728170
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->F:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2728077
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2728078
    const v0, 0x7f03050d

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->setContentView(I)V

    .line 2728079
    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "thread_event_reminder_model_extra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728080
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->H:LX/Jjj;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    invoke-virtual {v0, v1}, LX/Jjj;->a(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->v:Z

    .line 2728081
    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "thread_key_extra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2728082
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->K:LX/2Og;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728083
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2728084
    if-eqz v1, :cond_0

    if-nez v2, :cond_6

    :cond_0
    const/4 v3, 0x0

    :goto_1
    move-object v0, v3

    .line 2728085
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->q:LX/0P1;

    .line 2728086
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->I:LX/JjO;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->q:LX/0P1;

    invoke-virtual {v0, v1, v2}, LX/JjO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0P1;)Lcom/facebook/messaging/events/banner/EventReminderMembers;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2728087
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    invoke-static {v0, v1, v2}, LX/Jjj;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;Lcom/facebook/messaging/events/banner/EventReminderMembers;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->u:Z

    .line 2728088
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    .line 2728089
    if-nez p1, :cond_5

    .line 2728090
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728091
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2728092
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ""

    :goto_2
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    .line 2728093
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2728094
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728095
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    move-object v0, v1

    .line 2728096
    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    .line 2728097
    :goto_3
    invoke-static {}, Lcom/facebook/messaging/events/banner/EventReminderParams;->newBuilder()LX/JjW;

    move-result-object v0

    const-string v1, "messaging"

    const-string v2, "reminder_banner"

    .line 2728098
    iput-object v1, v0, LX/JjW;->n:Ljava/lang/String;

    .line 2728099
    iput-object v2, v0, LX/JjW;->o:Ljava/lang/String;

    .line 2728100
    move-object v0, v0

    .line 2728101
    const-string v1, "messaging"

    const-string v2, "event_reminder_settings"

    invoke-virtual {v0, v1, v2}, LX/JjW;->a(Ljava/lang/String;Ljava/lang/String;)LX/JjW;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->O:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdT;

    iget-object v2, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2728102
    invoke-virtual {v0, v2}, LX/DdT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    move v0, v3

    .line 2728103
    iput v0, v1, LX/JjW;->b:I

    .line 2728104
    move-object v0, v1

    .line 2728105
    invoke-virtual {v0}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->s:Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2728106
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->a()V

    .line 2728107
    const v0, 0x7f0d0e5b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    .line 2728108
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    const v1, 0x7f021011

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setGlyphImageResId(I)V

    .line 2728109
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2728110
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082db9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setPlaceholderText(Ljava/lang/String;)V

    .line 2728111
    :goto_4
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->v:Z

    if-eqz v0, :cond_8

    .line 2728112
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setEnabled(Z)V

    .line 2728113
    :goto_5
    const v0, 0x7f0d0e5a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    .line 2728114
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-static {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setText(Ljava/lang/CharSequence;)V

    .line 2728115
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    const v1, 0x7f020f81

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setGlyphImageResId(I)V

    .line 2728116
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->v:Z

    if-eqz v0, :cond_9

    .line 2728117
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setEnabled(Z)V

    .line 2728118
    :goto_6
    const/4 v3, 0x0

    .line 2728119
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728120
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v0, v1

    .line 2728121
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->EVENT:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->M:LX/Jiz;

    invoke-virtual {v0}, LX/Jiz;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2728122
    :cond_1
    :goto_7
    const v0, 0x7f0d0e5d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->z:Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    .line 2728123
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->u:Z

    if-eqz v0, :cond_e

    .line 2728124
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->z:Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->setMembers(Lcom/facebook/messaging/events/banner/EventReminderMembers;)V

    .line 2728125
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->z:Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->setVisibility(I)V

    .line 2728126
    :goto_8
    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2728127
    const v0, 0x7f0d0e5e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckedTextView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->A:Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 2728128
    const v0, 0x7f0d0e5f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckedTextView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->B:Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 2728129
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->u:Z

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->v:Z

    if-nez v0, :cond_10

    .line 2728130
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2728131
    iget-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-object v0, v1

    .line 2728132
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    if-ne v0, v1, :cond_f

    .line 2728133
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->A:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 2728134
    :cond_2
    :goto_9
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->A:Lcom/facebook/resources/ui/FbCheckedTextView;

    new-instance v1, LX/Jjf;

    invoke-direct {v1, p0}, LX/Jjf;-><init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2728135
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->B:Lcom/facebook/resources/ui/FbCheckedTextView;

    new-instance v1, LX/Jjg;

    invoke-direct {v1, p0}, LX/Jjg;-><init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2728136
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->A:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    .line 2728137
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->B:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    .line 2728138
    :goto_a
    return-void

    .line 2728139
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2728140
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2728141
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2728142
    goto/16 :goto_2

    .line 2728143
    :cond_5
    const-string v0, "event_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    .line 2728144
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    const-string v1, "event_timestamp"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2728145
    const-string v0, "event_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    goto/16 :goto_3

    :cond_6
    invoke-static {v0, v1}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2OQ;->c(Ljava/lang/String;)LX/0P1;

    move-result-object v3

    goto/16 :goto_1

    .line 2728146
    :cond_7
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 2728147
    :cond_8
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    new-instance v1, LX/Jja;

    invoke-direct {v1, p0}, LX/Jja;-><init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2728148
    goto/16 :goto_5

    .line 2728149
    :cond_9
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->x:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    new-instance v1, LX/Jjc;

    invoke-direct {v1, p0}, LX/Jjc;-><init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2728150
    goto/16 :goto_6

    .line 2728151
    :cond_a
    const v0, 0x7f0d0e5c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    .line 2728152
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setVisibility(I)V

    .line 2728153
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    const v1, 0x7f020ff5

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setGlyphImageResId(I)V

    .line 2728154
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2728155
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082dbe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setPlaceholderText(Ljava/lang/String;)V

    .line 2728156
    :goto_b
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->v:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->M:LX/Jiz;

    const/4 v1, 0x0

    .line 2728157
    invoke-virtual {v0}, LX/Jiz;->a()Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, v0, LX/Jiz;->a:LX/0Uh;

    const/16 p1, 0x66

    invoke-virtual {v2, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v1, 0x1

    :cond_b
    move v0, v1

    .line 2728158
    if-eqz v0, :cond_d

    .line 2728159
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    new-instance v1, LX/Jje;

    invoke-direct {v1, p0}, LX/Jje;-><init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2728160
    goto/16 :goto_7

    .line 2728161
    :cond_c
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    .line 2728162
    :cond_d
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->y:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setEnabled(Z)V

    goto/16 :goto_7

    .line 2728163
    :cond_e
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->z:Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/events/banner/EventReminderMembersRowView;->setVisibility(I)V

    goto/16 :goto_8

    .line 2728164
    :cond_f
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->r:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2728165
    iget-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-object v0, v1

    .line 2728166
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    if-ne v0, v1, :cond_2

    .line 2728167
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->B:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    goto/16 :goto_9

    .line 2728168
    :cond_10
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->A:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    .line 2728169
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->B:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    goto/16 :goto_a
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    .line 2728065
    iget-boolean v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->v:Z

    if-eqz v0, :cond_0

    .line 2728066
    const/4 v0, 0x0

    .line 2728067
    :goto_0
    return v0

    .line 2728068
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 2728069
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2728070
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jjx;

    .line 2728071
    sget-object v2, LX/Jjw;->a:[I

    invoke-static {v0}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object v3

    invoke-virtual {v3}, LX/Jk2;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2728072
    const v2, 0x7f082db3

    :goto_1
    move v0, v2

    .line 2728073
    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2728074
    new-instance v2, LX/Jjh;

    invoke-direct {v2, p0}, LX/Jjh;-><init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move v0, v1

    .line 2728075
    goto :goto_0

    .line 2728076
    :pswitch_0
    const v2, 0x7f082db4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2728061
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2728062
    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->finish()V

    .line 2728063
    const/4 v0, 0x1

    .line 2728064
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2728056
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2728057
    const-string v0, "event_timestamp"

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2728058
    const-string v0, "event_title"

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2728059
    const-string v0, "event_location"

    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2728060
    return-void
.end method
