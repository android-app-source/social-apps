.class public Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JjT;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjx;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjj;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/messaging/events/banner/EventReminderParams;

.field public q:Ljava/lang/String;

.field public r:LX/Jjd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2727276
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2727277
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727278
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->m:LX/0Ot;

    .line 2727279
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727280
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->n:LX/0Ot;

    .line 2727281
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727282
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->o:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;

    const/16 v2, 0x2772

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2778

    invoke-static {v1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x2774

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->m:LX/0Ot;

    iput-object v3, p1, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->n:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->o:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2727283
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1ef6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2727284
    new-instance v0, Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 2727285
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b1ef4

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setWidth(I)V

    .line 2727286
    const/16 v1, 0x4001

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setInputType(I)V

    .line 2727287
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2727288
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2727289
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 2727290
    :cond_0
    move-object v1, v0

    .line 2727291
    new-instance v0, LX/4me;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, LX/4me;-><init>(Landroid/content/Context;)V

    .line 2727292
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v4, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f082dbf

    :goto_0
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/4me;->setTitle(Ljava/lang/CharSequence;)V

    move v4, v2

    move v5, v3

    .line 2727293
    invoke-virtual/range {v0 .. v5}, LX/2EJ;->a(Landroid/view/View;IIII)V

    .line 2727294
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2727295
    const/4 v3, -0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082dcb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Jj5;

    invoke-direct {v5, p0, v1, v2}, LX/Jj5;-><init>(Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;Lcom/facebook/resources/ui/FbEditText;Landroid/content/Context;)V

    invoke-virtual {v0, v3, v4, v5}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2727296
    const/4 v2, -0x2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082dcc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Jj6;

    invoke-direct {v4, p0}, LX/Jj6;-><init>(Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;)V

    invoke-virtual {v0, v2, v3, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2727297
    invoke-virtual {v0}, LX/4me;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2727298
    return-object v0

    .line 2727299
    :cond_1
    const v4, 0x7f082dc0

    goto :goto_0
.end method

.method public final b(LX/0gc;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2727300
    invoke-virtual {p1}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2727301
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2727302
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xd87a6a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2727303
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2727304
    const-class v0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2727305
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2727306
    const-string v2, "reminder_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderParams;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2727307
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditLocationDialogFragment;->q:Ljava/lang/String;

    .line 2727308
    const/16 v0, 0x2b

    const v2, 0x7c493eca

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
