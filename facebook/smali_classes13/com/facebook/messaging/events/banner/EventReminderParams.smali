.class public Lcom/facebook/messaging/events/banner/EventReminderParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/events/banner/EventReminderParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:I

.field public final c:J

.field public final d:J

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2727905
    new-instance v0, LX/JjV;

    invoke-direct {v0}, LX/JjV;-><init>()V

    sput-object v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/JjW;)V
    .locals 2

    .prologue
    .line 2727906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727907
    iget-object v0, p1, LX/JjW;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727908
    iget v0, p1, LX/JjW;->b:I

    iput v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    .line 2727909
    iget-wide v0, p1, LX/JjW;->c:J

    iput-wide v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    .line 2727910
    iget-wide v0, p1, LX/JjW;->d:J

    iput-wide v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->d:J

    .line 2727911
    iget-object v0, p1, LX/JjW;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->e:Ljava/lang/String;

    .line 2727912
    iget-object v0, p1, LX/JjW;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->f:Ljava/lang/String;

    .line 2727913
    iget-object v0, p1, LX/JjW;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    .line 2727914
    iget-object v0, p1, LX/JjW;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    .line 2727915
    iget-object v0, p1, LX/JjW;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->i:Ljava/lang/String;

    .line 2727916
    iget-object v0, p1, LX/JjW;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    .line 2727917
    iget-object v0, p1, LX/JjW;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->k:Ljava/lang/String;

    .line 2727918
    iget-object v0, p1, LX/JjW;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    .line 2727919
    iget-object v0, p1, LX/JjW;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    .line 2727920
    iget-object v0, p1, LX/JjW;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    .line 2727921
    iget-object v0, p1, LX/JjW;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    .line 2727922
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2727887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727888
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727889
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    .line 2727890
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    .line 2727891
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->d:J

    .line 2727892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->e:Ljava/lang/String;

    .line 2727893
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    .line 2727894
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    .line 2727895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->i:Ljava/lang/String;

    .line 2727896
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    .line 2727897
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->k:Ljava/lang/String;

    .line 2727898
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    .line 2727899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    .line 2727900
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    .line 2727901
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    .line 2727902
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->f:Ljava/lang/String;

    .line 2727903
    return-void
.end method

.method public static a(Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/JjW;
    .locals 2

    .prologue
    .line 2727904
    new-instance v0, LX/JjW;

    invoke-direct {v0, p0}, LX/JjW;-><init>(Lcom/facebook/messaging/events/banner/EventReminderParams;)V

    return-object v0
.end method

.method public static newBuilder()LX/JjW;
    .locals 2

    .prologue
    .line 2727869
    new-instance v0, LX/JjW;

    invoke-direct {v0}, LX/JjW;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2727886
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2727870
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2727871
    iget v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2727872
    iget-wide v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2727873
    iget-wide v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2727874
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727875
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727876
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727877
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727878
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727879
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727880
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727881
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727882
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727883
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727884
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2727885
    return-void
.end method
