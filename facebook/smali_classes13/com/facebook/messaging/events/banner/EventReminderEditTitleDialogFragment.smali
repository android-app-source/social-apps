.class public Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JjT;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjx;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjj;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/messaging/events/banner/EventReminderParams;

.field public q:Ljava/lang/String;

.field private r:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:LX/JjZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2727455
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2727456
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727457
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->m:LX/0Ot;

    .line 2727458
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727459
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->n:LX/0Ot;

    .line 2727460
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2727461
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->o:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    const/16 v2, 0x2772

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2778

    invoke-static {v1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x2774

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->m:LX/0Ot;

    iput-object v3, p1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->n:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->o:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2727472
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1ef6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2727473
    new-instance v0, Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 2727474
    const/16 v1, 0x4001

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setInputType(I)V

    .line 2727475
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    .line 2727476
    const/4 v4, 0x0

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/16 v6, 0x64

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v1, v4

    .line 2727477
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 2727478
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2727479
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2727480
    iget-object v1, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 2727481
    :cond_0
    move-object v1, v0

    .line 2727482
    new-instance v0, LX/4me;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, LX/4me;-><init>(Landroid/content/Context;)V

    .line 2727483
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v4, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->r:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->CALL:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v4, v6, :cond_1

    const v4, 0x7f082dbd

    :goto_0
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/4me;->setTitle(Ljava/lang/CharSequence;)V

    move v4, v2

    move v5, v3

    .line 2727484
    invoke-virtual/range {v0 .. v5}, LX/2EJ;->a(Landroid/view/View;IIII)V

    .line 2727485
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2727486
    const/4 v3, -0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082dcb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/JjE;

    invoke-direct {v5, p0, v1, v2}, LX/JjE;-><init>(Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;Lcom/facebook/resources/ui/FbEditText;Landroid/content/Context;)V

    invoke-virtual {v0, v3, v4, v5}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2727487
    const/4 v2, -0x2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082dcc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/JjF;

    invoke-direct {v4, p0}, LX/JjF;-><init>(Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;)V

    invoke-virtual {v0, v2, v3, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2727488
    invoke-virtual {v0}, LX/4me;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2727489
    return-object v0

    .line 2727490
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Jjx;

    .line 2727491
    sget-object v6, LX/Jjw;->a:[I

    invoke-static {v4}, LX/Jjx;->k(LX/Jjx;)LX/Jk2;

    move-result-object p1

    invoke-virtual {p1}, LX/Jk2;->ordinal()I

    move-result p1

    aget v6, v6, p1

    packed-switch v6, :pswitch_data_0

    .line 2727492
    const v6, 0x7f082dba

    :goto_1
    move v4, v6

    .line 2727493
    goto :goto_0

    .line 2727494
    :pswitch_0
    const v6, 0x7f082dbb

    goto :goto_1

    .line 2727495
    :pswitch_1
    const v6, 0x7f082dbc

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x691b2916

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2727462
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2727463
    const-class v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2727464
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2727465
    const-string v2, "reminder_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/events/banner/EventReminderParams;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    .line 2727466
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->q:Ljava/lang/String;

    .line 2727467
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->r:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2727468
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderParams;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->s:Ljava/lang/String;

    .line 2727469
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->s:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    .line 2727470
    const v0, -0x4ab31842

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2727471
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->s:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->s:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x64

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
