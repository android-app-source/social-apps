.class public Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2728204
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2728205
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->a()V

    .line 2728206
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2728207
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2728208
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->a()V

    .line 2728209
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2728210
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2728211
    invoke-direct {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->a()V

    .line 2728212
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2728213
    const v0, 0x7f03050e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2728214
    const v0, 0x7f0d0e60

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2728215
    const v0, 0x7f0d0e61

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2728216
    return-void
.end method


# virtual methods
.method public setGlyphImageResId(I)V
    .locals 1

    .prologue
    .line 2728217
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2728218
    return-void
.end method

.method public setPlaceholderText(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2728219
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2728220
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2728221
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2728222
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2728223
    iget-object v0, p0, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2728224
    return-void
.end method
