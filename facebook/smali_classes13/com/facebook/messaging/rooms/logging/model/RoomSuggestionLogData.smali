.class public Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData$Surface;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2737039
    new-instance v0, LX/Jp7;

    invoke-direct {v0}, LX/Jp7;-><init>()V

    sput-object v0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2737032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737033
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->a:J

    .line 2737034
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->b:Ljava/lang/String;

    .line 2737035
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->c:I

    .line 2737036
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->d:I

    .line 2737037
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->e:Ljava/lang/String;

    .line 2737038
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2737025
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2737026
    iget-wide v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2737027
    iget-object v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2737028
    iget v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2737029
    iget v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2737030
    iget-object v0, p0, Lcom/facebook/messaging/rooms/logging/model/RoomSuggestionLogData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2737031
    return-void
.end method
