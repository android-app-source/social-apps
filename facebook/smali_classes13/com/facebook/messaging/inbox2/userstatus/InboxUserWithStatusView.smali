.class public Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Jid;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3Kv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733558
    const-class v0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;

    sput-object v0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2733555
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2733556
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a()V

    .line 2733557
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733552
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2733553
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a()V

    .line 2733554
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733549
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733550
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a()V

    .line 2733551
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2733521
    const-class v0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733522
    return-void
.end method

.method private static a(Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;LX/Jid;LX/3Kv;)V
    .locals 0

    .prologue
    .line 2733548
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a:LX/Jid;

    iput-object p2, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->b:LX/3Kv;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;

    new-instance p1, LX/Jid;

    const-class v0, Landroid/content/Context;

    invoke-interface {v1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p1, v0}, LX/Jid;-><init>(Landroid/content/Context;)V

    move-object v0, p1

    check-cast v0, LX/Jid;

    invoke-static {v1}, LX/3Kv;->b(LX/0QB;)LX/3Kv;

    move-result-object v1

    check-cast v1, LX/3Kv;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a(Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;LX/Jid;LX/3Kv;)V

    return-void
.end method

.method private b(Lcom/facebook/user/model/User;LX/Jke;ZZ)LX/3OO;
    .locals 7
    .param p2    # LX/Jke;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733538
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->b:LX/3Kv;

    sget-object v2, LX/3OH;->TOP_FRIENDS:LX/3OH;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v6, p4

    .line 2733539
    invoke-static {v0, v1, v4, v2}, LX/3Kv;->a(LX/3Kv;Lcom/facebook/user/model/User;ZLX/3OH;)LX/3OJ;

    move-result-object p0

    .line 2733540
    iput-boolean v5, p0, LX/3OJ;->G:Z

    .line 2733541
    move-object p0, p0

    .line 2733542
    iput-boolean v6, p0, LX/3OJ;->C:Z

    .line 2733543
    move-object p0, p0

    .line 2733544
    iput-object v3, p0, LX/3OJ;->w:LX/Jke;

    .line 2733545
    move-object p0, p0

    .line 2733546
    invoke-virtual {p0}, LX/3OJ;->a()LX/3OO;

    move-result-object p0

    move-object v0, p0

    .line 2733547
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;LX/Jke;ZZ)V
    .locals 4
    .param p1    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/Jke;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2733524
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->d:Lcom/facebook/user/model/User;

    .line 2733525
    if-eqz p1, :cond_2

    .line 2733526
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->b(Lcom/facebook/user/model/User;LX/Jke;ZZ)LX/3OO;

    move-result-object v0

    .line 2733527
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2733528
    if-eqz v1, :cond_1

    .line 2733529
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a:LX/Jid;

    invoke-virtual {v2, v0, v1}, LX/Jid;->a(LX/3OO;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 2733530
    if-eq v0, v1, :cond_0

    .line 2733531
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->removeViewAt(I)V

    .line 2733532
    invoke-virtual {p0, v0, v3}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->addView(Landroid/view/View;I)V

    .line 2733533
    :cond_0
    :goto_0
    return-void

    .line 2733534
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->a:LX/Jid;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/Jid;->a(LX/3OO;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 2733535
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2733536
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2733537
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->removeViewAt(I)V

    goto :goto_0
.end method

.method public getUser()Lcom/facebook/user/model/User;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2733523
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/userstatus/InboxUserWithStatusView;->d:Lcom/facebook/user/model/User;

    return-object v0
.end method
