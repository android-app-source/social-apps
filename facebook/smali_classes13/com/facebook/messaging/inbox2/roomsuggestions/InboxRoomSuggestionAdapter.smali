.class public Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Dct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Jmj;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/Dct",
        "<",
        "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field public static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public e:LX/FOK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/FKD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/Jme;

.field public h:LX/Jmf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733170
    sget-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_ITEM:LX/Dfa;

    invoke-virtual {v0}, LX/Dfa;->ordinal()I

    move-result v0

    sput v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->a:I

    .line 2733171
    sget-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_SEE_MORE:LX/Dfa;

    invoke-virtual {v0}, LX/Dfa;->ordinal()I

    move-result v0

    sput v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->b:I

    .line 2733172
    sget-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/Dfa;

    invoke-virtual {v0}, LX/Dfa;->ordinal()I

    move-result v0

    sput v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->c:I

    .line 2733173
    const-class v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2733167
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2733168
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2733169
    return-void
.end method

.method public static f(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)Z
    .locals 2

    .prologue
    .line 2733166
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->i(I)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v1, v1, LX/Jme;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)LX/Jmn;
    .locals 2

    .prologue
    .line 2733165
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v0, v0, LX/Jme;->a:LX/0Px;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jmn;

    return-object v0
.end method

.method public static h(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2733164
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->f:LX/FKD;

    invoke-virtual {v2}, LX/FKD;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v2, v2, LX/Jme;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private i(I)I
    .locals 1

    .prologue
    .line 2733163
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->f:LX/FKD;

    invoke-virtual {v0}, LX/FKD;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2733174
    invoke-static {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->h(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2733175
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v0, v0, LX/Jme;->c:Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;

    .line 2733176
    :goto_0
    move-object v0, v0

    .line 2733177
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v0

    return-wide v0

    .line 2733178
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->f(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2733179
    invoke-static {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)LX/Jmn;

    move-result-object v0

    iget-object v0, v0, LX/Jmn;->h:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomInboxItem;

    goto :goto_0

    .line 2733180
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v0, v0, LX/Jme;->b:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 2733134
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2733135
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->c:I

    if-ne p2, v1, :cond_0

    .line 2733136
    const v1, 0x7f0308f9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2733137
    new-instance v2, LX/JmY;

    invoke-direct {v2, p0}, LX/JmY;-><init>(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2733138
    new-instance v2, LX/JmZ;

    invoke-direct {v2, p0}, LX/JmZ;-><init>(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;)V

    .line 2733139
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733140
    new-instance p2, LX/Jmk;

    invoke-direct {p2, v1}, LX/Jmk;-><init>(Landroid/view/View;)V

    .line 2733141
    iget-object v1, p2, LX/Jmk;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733142
    move-object v0, p2

    .line 2733143
    :goto_0
    return-object v0

    .line 2733144
    :cond_0
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->a:I

    if-ne p2, v1, :cond_1

    .line 2733145
    const/4 p2, 0x1

    .line 2733146
    const v1, 0x7f0308fb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2733147
    new-instance v2, LX/Jmm;

    invoke-direct {v2, v1}, LX/Jmm;-><init>(Landroid/view/View;)V

    .line 2733148
    new-instance v3, LX/Jma;

    invoke-direct {v3, p0}, LX/Jma;-><init>(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2733149
    new-instance v3, LX/Jmb;

    invoke-direct {v3, p0, v1}, LX/Jmb;-><init>(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;Landroid/view/View;)V

    .line 2733150
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733151
    iget-object v4, v2, LX/Jmm;->o:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733152
    iget-object v3, v2, LX/Jmm;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v3, p2, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 2733153
    iget-object v2, v2, LX/Jmm;->p:Landroid/widget/ImageView;

    new-instance v3, LX/Jmc;

    invoke-direct {v3, p0, v1}, LX/Jmc;-><init>(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733154
    new-instance v2, LX/Jmm;

    invoke-direct {v2, v1}, LX/Jmm;-><init>(Landroid/view/View;)V

    move-object v0, v2

    .line 2733155
    goto :goto_0

    .line 2733156
    :cond_1
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->b:I

    if-ne p2, v1, :cond_2

    .line 2733157
    const v1, 0x7f0308fc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2733158
    new-instance v2, LX/Jml;

    invoke-direct {v2, v1}, LX/Jml;-><init>(Landroid/view/View;)V

    .line 2733159
    iget-object v1, v2, LX/Jml;->l:Lcom/facebook/uicontrib/fab/FabView;

    new-instance v3, LX/Jmd;

    invoke-direct {v3, p0}, LX/Jmd;-><init>(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;)V

    invoke-virtual {v1, v3}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2733160
    move-object v0, v2

    .line 2733161
    goto :goto_0

    .line 2733162
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2733097
    check-cast p1, LX/Jmj;

    .line 2733098
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2733099
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->c:I

    if-eq v0, v1, :cond_0

    .line 2733100
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->a:I

    if-ne v0, v1, :cond_1

    .line 2733101
    invoke-static {p0, p2}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)LX/Jmn;

    move-result-object v0

    check-cast p1, LX/Jmm;

    .line 2733102
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2733103
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2733104
    iget-object v2, p1, LX/Jmm;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v3, v0, LX/Jmn;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2733105
    iget-object v2, p1, LX/Jmm;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v3, 0x7f0f0134

    iget v4, v0, LX/Jmn;->d:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v0, LX/Jmn;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2733106
    iget-object v1, p1, LX/Jmm;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v2, 0x7f0e0c59

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2733107
    iget-object v1, p1, LX/Jmm;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v2, 0x7f0e0c5a

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2733108
    iget-object v1, p1, LX/Jmm;->m:Landroid/widget/TextView;

    const/4 v5, 0x0

    .line 2733109
    iget-object v2, v0, LX/Jmn;->f:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2733110
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2733111
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2733112
    :goto_0
    iget-object v1, p1, LX/Jmm;->n:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, v0, LX/Jmn;->j:I

    .line 2733113
    if-eqz v3, :cond_4

    :goto_1
    move v2, v3

    .line 2733114
    invoke-virtual {v1, v2}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->setPlaceholderColor(I)V

    .line 2733115
    iget-object v1, p1, LX/Jmm;->n:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    iget-object v2, v0, LX/Jmn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->setGroupName(Ljava/lang/String;)V

    .line 2733116
    iget-object v1, p1, LX/Jmm;->n:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    iget-object v2, v0, LX/Jmn;->c:Landroid/net/Uri;

    sget-object v3, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2733117
    :cond_0
    return-void

    .line 2733118
    :cond_1
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->b:I

    if-eq v0, v1, :cond_0

    .line 2733119
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown view type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2733120
    :cond_2
    iget-object v2, v0, LX/Jmn;->g:LX/0Px;

    iget-object v3, v0, LX/Jmn;->i:LX/JpA;

    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v8, 0x1

    const/4 p2, 0x0

    .line 2733121
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2733122
    const/4 v6, 0x0

    .line 2733123
    :goto_2
    move-object v2, v6

    .line 2733124
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2733125
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->e:LX/FOK;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, LX/Jmn;->f:LX/0Px;

    invoke-virtual {v2, v3, v1, v4}, LX/FOK;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;LX/0Px;)LX/FOI;

    move-result-object v2

    .line 2733126
    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2733127
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2733128
    :cond_3
    sget-object v6, LX/JpB;->a:[I

    invoke-virtual {v3}, LX/JpA;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 2733129
    const v6, 0x7f0f018e

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 2733130
    :pswitch_0
    const v6, 0x7f083b6b

    new-array v7, v8, [Ljava/lang/Object;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, p2

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 2733131
    :pswitch_1
    const v6, 0x7f083b69

    new-array v7, v8, [Ljava/lang/Object;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, p2

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 2733132
    :cond_4
    const v4, 0x7f010053

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v3, 0x7f0a019a

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v2, v4, v5}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v4

    move v3, v4

    .line 2733133
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(LX/1a1;)V
    .locals 4

    .prologue
    .line 2733065
    check-cast p1, LX/Jmj;

    .line 2733066
    invoke-super {p0, p1}, LX/1OM;->c(LX/1a1;)V

    .line 2733067
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 2733068
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->a:I

    if-ne v0, v1, :cond_1

    .line 2733069
    check-cast p1, LX/Jmm;

    iget-object v0, p1, LX/Jmm;->m:Landroid/widget/TextView;

    .line 2733070
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2733071
    array-length p0, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_1

    aget-object v1, v3, v2

    .line 2733072
    if-eqz v1, :cond_0

    instance-of p1, v1, LX/FOF;

    if-eqz p1, :cond_0

    .line 2733073
    check-cast v1, LX/FOF;

    invoke-interface {v1}, LX/FOF;->a()V

    .line 2733074
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2733075
    :cond_1
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 4

    .prologue
    .line 2733086
    check-cast p1, LX/Jmj;

    .line 2733087
    invoke-super {p0, p1}, LX/1OM;->d(LX/1a1;)V

    .line 2733088
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 2733089
    sget v1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->a:I

    if-ne v0, v1, :cond_1

    .line 2733090
    check-cast p1, LX/Jmm;

    iget-object v0, p1, LX/Jmm;->m:Landroid/widget/TextView;

    .line 2733091
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2733092
    array-length p0, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_1

    aget-object v1, v3, v2

    .line 2733093
    if-eqz v1, :cond_0

    instance-of p1, v1, LX/FOF;

    if-eqz p1, :cond_0

    .line 2733094
    check-cast v1, LX/FOF;

    invoke-interface {v1}, LX/FOF;->b()V

    .line 2733095
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2733096
    :cond_1
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2733080
    invoke-static {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->h(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2733081
    sget v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->c:I

    .line 2733082
    :goto_0
    return v0

    .line 2733083
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->f(Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2733084
    sget v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->a:I

    goto :goto_0

    .line 2733085
    :cond_1
    sget v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->b:I

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2733076
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v0, v0, LX/Jme;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 2733077
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    iget-object v0, v0, LX/Jme;->b:Lcom/facebook/messaging/inbox2/roomsuggestions/SuggestedRoomSeeMoreInboxItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 2733078
    return v0

    .line 2733079
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
