.class public Lcom/facebook/messaging/inbox2/roomsuggestions/CreateSuggestedRoomInboxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)V
    .locals 1

    .prologue
    .line 2733038
    sget-object v0, LX/Dfd;->CREATE_ROOM:LX/Dfd;

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;LX/Dfd;)V

    .line 2733039
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2733040
    sget-object v0, LX/DfY;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/DfY;

    return-object v0
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2733041
    sget-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/Dfa;

    return-object v0
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 2733042
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a()LX/DfY;

    move-result-object v1

    invoke-virtual {v1}, LX/DfY;->hashCode()I

    move-result v1

    invoke-interface {v0, v1}, LX/51h;->a(I)LX/51h;

    move-result-object v0

    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0
.end method
