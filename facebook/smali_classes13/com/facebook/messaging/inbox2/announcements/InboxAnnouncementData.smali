.class public Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730813
    new-instance v0, LX/Jl4;

    invoke-direct {v0}, LX/Jl4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2730806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730807
    const-class v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->a:Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

    .line 2730808
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->b:Ljava/lang/String;

    .line 2730809
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->c:Ljava/lang/String;

    .line 2730810
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->d:Ljava/lang/String;

    .line 2730811
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->e:Ljava/lang/String;

    .line 2730812
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730793
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->a:Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

    .line 2730794
    iput-object p2, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->b:Ljava/lang/String;

    .line 2730795
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->c:Ljava/lang/String;

    .line 2730796
    iput-object p4, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->d:Ljava/lang/String;

    .line 2730797
    iput-object p5, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->e:Ljava/lang/String;

    .line 2730798
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2730805
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2730799
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->a:Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2730800
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2730801
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2730802
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2730803
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2730804
    return-void
.end method
