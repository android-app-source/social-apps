.class public Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field public f:LX/Jl7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730860
    const-class v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2730861
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2730862
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->a()V

    .line 2730863
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2730839
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2730840
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->a()V

    .line 2730841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2730857
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2730858
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->a()V

    .line 2730859
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2730850
    const v0, 0x7f0308e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2730851
    const v0, 0x7f0d1708

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2730852
    const v0, 0x7f0d1709

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->c:Landroid/widget/TextView;

    .line 2730853
    const v0, 0x7f0d170a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->d:Landroid/widget/TextView;

    .line 2730854
    const v0, 0x7f0d170b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->e:Landroid/widget/TextView;

    .line 2730855
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->e:Landroid/widget/TextView;

    new-instance v1, LX/Jl8;

    invoke-direct {v1, p0}, LX/Jl8;-><init>(Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2730856
    return-void
.end method


# virtual methods
.method public setData(Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;)V
    .locals 3

    .prologue
    .line 2730844
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p1, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2730845
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2730846
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2730847
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2730848
    return-void

    .line 2730849
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementData;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public setListener(LX/Jl7;)V
    .locals 0
    .param p1    # LX/Jl7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730842
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/announcements/InboxAnnouncementUnitView;->f:LX/Jl7;

    .line 2730843
    return-void
.end method
