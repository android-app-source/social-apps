.class public Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:I

.field private c:I

.field public d:I
    .annotation build Landroid/support/annotation/IntRange;
    .end annotation
.end field

.field public e:LX/JmT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/view/View$OnClickListener;

.field private final h:Landroid/view/View$OnLongClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2732784
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2732785
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2732786
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    .line 2732787
    new-instance v0, LX/JmO;

    invoke-direct {v0, p0}, LX/JmO;-><init>(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->g:Landroid/view/View$OnClickListener;

    .line 2732788
    new-instance v0, LX/JmP;

    invoke-direct {v0, p0}, LX/JmP;-><init>(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->h:Landroid/view/View$OnLongClickListener;

    .line 2732789
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a()V

    .line 2732790
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2732758
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2732759
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2732760
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    .line 2732761
    new-instance v0, LX/JmO;

    invoke-direct {v0, p0}, LX/JmO;-><init>(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->g:Landroid/view/View$OnClickListener;

    .line 2732762
    new-instance v0, LX/JmP;

    invoke-direct {v0, p0}, LX/JmP;-><init>(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->h:Landroid/view/View$OnLongClickListener;

    .line 2732763
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a()V

    .line 2732764
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2732777
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732778
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2732779
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    .line 2732780
    new-instance v0, LX/JmO;

    invoke-direct {v0, p0}, LX/JmO;-><init>(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->g:Landroid/view/View$OnClickListener;

    .line 2732781
    new-instance v0, LX/JmP;

    invoke-direct {v0, p0}, LX/JmP;-><init>(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->h:Landroid/view/View$OnLongClickListener;

    .line 2732782
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a()V

    .line 2732783
    return-void
.end method

.method private a(I)Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;
    .locals 1

    .prologue
    .line 2732776
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2732770
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a:Landroid/view/LayoutInflater;

    .line 2732771
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b266f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2732772
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2670

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2732773
    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b:I

    .line 2732774
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b266e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->c:I

    .line 2732775
    return-void
.end method

.method private a(Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)V
    .locals 1

    .prologue
    .line 2732765
    invoke-virtual {p1, p2}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)V

    .line 2732766
    invoke-virtual {p1, p2}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->setTag(Ljava/lang/Object;)V

    .line 2732767
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2732768
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->h:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2732769
    return-void
.end method

.method private static b()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 2732791
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2732723
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2732724
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-ge v0, p1, :cond_0

    move v2, v1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2732725
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2732726
    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    .line 2732727
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getChildCount()I

    move-result v0

    :goto_2
    if-ge v0, p1, :cond_2

    .line 2732728
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a:Landroid/view/LayoutInflater;

    const v3, 0x7f030896

    invoke-virtual {v2, v3, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2732729
    invoke-static {}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2732730
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2732731
    :cond_2
    return-void
.end method


# virtual methods
.method public getItems()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2732732
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2732733
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getMeasuredWidth()I

    move-result v0

    iget v2, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    iget v3, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b:I

    mul-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 2732734
    iget v2, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    move v0, v1

    .line 2732735
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2732736
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2732737
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 2732738
    iget v3, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v4, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->c:I

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 2732739
    iget v3, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    div-int v3, v1, v3

    .line 2732740
    iget v4, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    rem-int v4, v1, v4

    .line 2732741
    iget v5, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->c:I

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 2732742
    iget v6, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b:I

    add-int/2addr v6, v0

    mul-int/2addr v4, v6

    .line 2732743
    iget v6, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->c:I

    mul-int/2addr v3, v6

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 2732744
    iget v5, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b:I

    add-int/2addr v5, v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 2732745
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2732746
    :cond_1
    iget v2, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v0, v2

    goto :goto_0

    .line 2732747
    :cond_2
    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param

    .prologue
    .line 2732748
    iput p1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->d:I

    .line 2732749
    return-void
.end method

.method public setItems(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2732750
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->f:LX/0Px;

    .line 2732751
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->b(I)V

    .line 2732752
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2732753
    invoke-direct {p0, v1}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a(I)Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;

    move-result-object v2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    invoke-direct {p0, v2, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->a(Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)V

    .line 2732754
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2732755
    :cond_0
    return-void
.end method

.method public setListener(LX/JmT;)V
    .locals 0
    .param p1    # LX/JmT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732756
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesPage;->e:LX/JmT;

    .line 2732757
    return-void
.end method
