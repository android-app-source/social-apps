.class public Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/JmJ;

.field public final b:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/messaging/model/threads/ThreadSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/8ue;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2732593
    new-instance v0, LX/JmH;

    invoke-direct {v0}, LX/JmH;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2732594
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2732595
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/JmJ;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    .line 2732596
    const-class v0, Lcom/facebook/user/model/User;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    .line 2732597
    const-class v0, Lcom/facebook/user/model/User;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    .line 2732598
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2732599
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8ue;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->k:LX/8ue;

    .line 2732600
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/JmJ;Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/8ue;)V
    .locals 0
    .param p2    # Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/8ue;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732601
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2732602
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    .line 2732603
    iput-object p4, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    .line 2732604
    iput-object p5, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    .line 2732605
    iput-object p6, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2732606
    iput-object p7, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->k:LX/8ue;

    .line 2732607
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2732608
    sget-object v0, LX/DfY;->V2_HORIZONTAL_TILE_ITEM:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2732609
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2732610
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2732611
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2732612
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2732613
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2732614
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->k:LX/8ue;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2732615
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2732616
    sget-object v0, LX/Dfa;->V2_HORIZONTAL_TILE_ITEM:LX/Dfa;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 2732617
    sget-object v0, LX/JmI;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {v1}, LX/JmJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2732618
    invoke-super {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->e()J

    move-result-wide v0

    :goto_0
    return-wide v0

    .line 2732619
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732620
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    .line 2732621
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2732622
    invoke-static {v0, v1}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 2732623
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732624
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    .line 2732625
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2732626
    invoke-static {v0, v1}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 2732627
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732628
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v1}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2732629
    sget-object v0, LX/JmI;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {v1}, LX/JmJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2732630
    invoke-super {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2732631
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    .line 2732633
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2732634
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2732635
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732636
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    .line 2732637
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2732638
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2732639
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
