.class public Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/FJv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/3Ky;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/widget/tiles/ThreadTileView;

.field private d:Lcom/facebook/messaging/ui/name/ThreadNameView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2732998
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2732999
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a()V

    .line 2733000
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2732959
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2732960
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a()V

    .line 2732961
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733007
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733008
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a()V

    .line 2733009
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2733001
    const-class v0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733002
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->setOrientation(I)V

    .line 2733003
    const v0, 0x7f030897

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2733004
    const v0, 0x7f0d083f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->c:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2733005
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->d:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2733006
    return-void
.end method

.method private static a(Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;LX/FJv;LX/3Ky;)V
    .locals 0

    .prologue
    .line 2732997
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a:LX/FJv;

    iput-object p2, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->b:LX/3Ky;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;

    invoke-static {v1}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v0

    check-cast v0, LX/FJv;

    invoke-static {v1}, LX/3Ky;->a(LX/0QB;)LX/3Ky;

    move-result-object v1

    check-cast v1, LX/3Ky;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a(Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;LX/FJv;LX/3Ky;)V

    return-void
.end method

.method private b(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)LX/8Vc;
    .locals 3

    .prologue
    .line 2732986
    sget-object v0, LX/JmW;->a:[I

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {v1}, LX/JmJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2732987
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported entity type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2732988
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732989
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->k:LX/8ue;

    if-nez v0, :cond_0

    .line 2732990
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a:LX/FJv;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v0, v1}, LX/FJv;->a(Lcom/facebook/user/model/User;)LX/8Vc;

    move-result-object v0

    .line 2732991
    :goto_0
    return-object v0

    .line 2732992
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a:LX/FJv;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    iget-object v2, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->k:LX/8ue;

    invoke-virtual {v0, v1, v2}, LX/FJv;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8Vc;

    move-result-object v0

    goto :goto_0

    .line 2732993
    :pswitch_1
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732994
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a:LX/FJv;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    invoke-virtual {v0, v1}, LX/FJv;->a(Lcom/facebook/user/model/User;)LX/8Vc;

    move-result-object v0

    goto :goto_0

    .line 2732995
    :pswitch_2
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732996
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->a:LX/FJv;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0, v1}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)LX/FON;
    .locals 11

    .prologue
    .line 2732962
    sget-object v0, LX/JmW;->a:[I

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {v1}, LX/JmJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2732963
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported entity type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->a:LX/JmJ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2732964
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732965
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->b:Lcom/facebook/user/model/User;

    const/4 v6, 0x0

    .line 2732966
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v4, v6

    .line 2732967
    :goto_0
    move-object v0, v4

    .line 2732968
    :goto_1
    return-object v0

    .line 2732969
    :pswitch_1
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732970
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->c:Lcom/facebook/user/model/User;

    const/4 v6, 0x0

    .line 2732971
    if-nez v0, :cond_2

    .line 2732972
    :goto_2
    move-object v0, v6

    .line 2732973
    goto :goto_1

    .line 2732974
    :pswitch_2
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732975
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->b:LX/3Ky;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0, v1}, LX/3Ky;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;

    move-result-object v0

    goto :goto_1

    .line 2732976
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v3

    .line 2732977
    new-instance v8, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2732978
    iget-object v4, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2732979
    invoke-direct {v8, v4, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2732980
    new-instance v4, LX/FOO;

    const/4 v5, 0x0

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    const-wide/16 v9, -0x1

    invoke-direct/range {v4 .. v10}, LX/FOO;-><init>(ZLjava/lang/String;LX/0Px;Lcom/facebook/messaging/model/messages/ParticipantInfo;J)V

    goto :goto_0

    .line 2732981
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v3

    .line 2732982
    new-instance v8, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2732983
    iget-object v4, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2732984
    invoke-direct {v8, v4, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2732985
    new-instance v4, LX/FOO;

    const/4 v5, 0x0

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    const-wide/16 v9, -0x1

    invoke-direct/range {v4 .. v10}, LX/FOO;-><init>(ZLjava/lang/String;LX/0Px;Lcom/facebook/messaging/model/messages/ParticipantInfo;J)V

    move-object v6, v4

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)V
    .locals 2

    .prologue
    .line 2732956
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->c:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->b(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)LX/8Vc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2732957
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->d:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/horizontaltiles/TileItemView;->c(Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;)LX/FON;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2732958
    return-void
.end method
