.class public Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2732808
    new-instance v0, LX/JmR;

    invoke-direct {v0}, LX/JmR;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2732809
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2732810
    sget-object v0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;->a:LX/0Px;

    .line 2732811
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2732812
    sget-object v0, LX/DfY;->V2_HORIZONTAL_TILES_UNIT_ITEM:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2732813
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2732814
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTilesUnitInboxItem;->a:LX/0Px;

    .line 2732815
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2732816
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2732817
    sget-object v0, LX/Dfa;->V2_HORIZONTAL_TILES_UNIT_ITEM:LX/Dfa;

    return-object v0
.end method
