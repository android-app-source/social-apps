.class public Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/Jmr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/fbui/facepile/FacepileView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Lcom/facebook/fbui/facepile/FacepileView;

.field private i:Lcom/facebook/widget/text/BetterTextView;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733356
    const-class v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2733354
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733355
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733352
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733353
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733349
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733350
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->a()V

    .line 2733351
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2733337
    const-class v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733338
    const v0, 0x7f031409

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2733339
    const v0, 0x7f0d2e0c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2733340
    const v0, 0x7f0d04e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2733341
    const v0, 0x7f0d2dfc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2733342
    const v0, 0x7f0d317d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->k:Landroid/widget/ImageView;

    .line 2733343
    const v0, 0x7f0d2e0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2733344
    const v0, 0x7f0d2e0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2733345
    const v0, 0x7f0d2e0f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->h:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2733346
    const v0, 0x7f0d2e10

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2733347
    const v0, 0x7f0d119d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->j:Landroid/widget/ImageButton;

    .line 2733348
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;

    new-instance p1, LX/Jmr;

    invoke-direct {p1}, LX/Jmr;-><init>()V

    invoke-static {v0}, LX/Jso;->b(LX/0QB;)LX/Jso;

    move-result-object v1

    check-cast v1, LX/Jso;

    iput-object v1, p1, LX/Jmr;->a:LX/Jso;

    move-object v0, p1

    check-cast v0, LX/Jmr;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSocialContextSubscriptionContentItemView;->b:LX/Jmr;

    return-void
.end method
