.class public Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/Jmp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733315
    const-class v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2733316
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733317
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733318
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733319
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733320
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733321
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->a()V

    .line 2733322
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2733323
    const-class v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733324
    const v0, 0x7f030c16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2733325
    const v0, 0x7f0d1a94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->c:LX/4ob;

    .line 2733326
    const v0, 0x7f0d2e0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2733327
    const v0, 0x7f0d2e0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2733328
    const v0, 0x7f0d3177

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->f:LX/4ob;

    .line 2733329
    const v0, 0x7f0d1dce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->g:LX/4ob;

    .line 2733330
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;

    invoke-static {v0}, LX/Jmp;->b(LX/0QB;)LX/Jmp;

    move-result-object v0

    check-cast v0, LX/Jmp;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxNonSocialContextSubscriptionContentItemView;->b:LX/Jmp;

    return-void
.end method
