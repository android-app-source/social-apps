.class public Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733360
    new-instance v0, LX/Jmq;

    invoke-direct {v0}, LX/Jmq;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2733361
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2733362
    const-class v0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->a:Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    .line 2733363
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->b:Ljava/lang/String;

    .line 2733364
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->c:LX/0Px;

    .line 2733365
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;Ljava/lang/String;LX/0Px;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;",
            "Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2733366
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2733367
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->a:Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    .line 2733368
    iput-object p4, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->b:Ljava/lang/String;

    .line 2733369
    iput-object p5, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->c:LX/0Px;

    .line 2733370
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2733371
    sget-object v0, LX/DfY;->V2_SUBSCRIPTION_CONTENT:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2733372
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2733373
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->a:Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2733374
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2733375
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2733376
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2733377
    sget-object v0, LX/Dfa;->V2_SUBSCRIPTION_CONTENT:LX/Dfa;

    return-object v0
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 2733378
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    .line 2733379
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2733380
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->a:Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    iget-object v1, v1, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->a:Ljava/lang/String;

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2733381
    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2733382
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/subscriptions/contents/InboxSubscriptionContentItem;->a:Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;

    iget-object v1, v1, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
