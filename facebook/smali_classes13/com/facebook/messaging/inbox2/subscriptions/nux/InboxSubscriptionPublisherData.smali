.class public Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733490
    new-instance v0, LX/Jmw;

    invoke-direct {v0}, LX/Jmw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2733482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2733483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->a:Ljava/lang/String;

    .line 2733484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->b:Ljava/lang/String;

    .line 2733485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->c:Ljava/lang/String;

    .line 2733486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->d:Ljava/lang/String;

    .line 2733487
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->e:Z

    .line 2733488
    return-void

    .line 2733489
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2733491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2733492
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->a:Ljava/lang/String;

    .line 2733493
    iput-object p2, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->b:Ljava/lang/String;

    .line 2733494
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->c:Ljava/lang/String;

    .line 2733495
    iput-object p4, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->d:Ljava/lang/String;

    .line 2733496
    iput-boolean p5, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->e:Z

    .line 2733497
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2733481
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2733474
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2733475
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2733476
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2733477
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2733478
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2733479
    return-void

    .line 2733480
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
