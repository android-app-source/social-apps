.class public Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733453
    new-instance v0, LX/Jmv;

    invoke-direct {v0}, LX/Jmv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2733454
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2733455
    const-class v0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;->a:Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    .line 2733456
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2733457
    sget-object v0, LX/DfY;->V2_SUBSCRIPTION_NUX:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2733458
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2733459
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;->a:Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2733460
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2733461
    sget-object v0, LX/Dfa;->V2_SUBSCRIPTION_NUX:LX/Dfa;

    return-object v0
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 2733462
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    .line 2733463
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2733464
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;->a:Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    .line 2733465
    iget-object v2, v1, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2733466
    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2733467
    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2733468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxItem;->a:Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;

    .line 2733469
    iget-object p0, v1, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionPublisherData;->d:Ljava/lang/String;

    move-object v1, p0

    .line 2733470
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
