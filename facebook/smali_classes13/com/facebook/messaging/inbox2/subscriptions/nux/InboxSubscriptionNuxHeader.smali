.class public Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733441
    new-instance v0, LX/Jmu;

    invoke-direct {v0}, LX/Jmu;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2733442
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2733443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;->a:Ljava/lang/String;

    .line 2733444
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2733445
    sget-object v0, LX/DfY;->V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2733446
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2733447
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/subscriptions/nux/InboxSubscriptionNuxHeader;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2733448
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2733449
    sget-object v0, LX/Dfa;->V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/Dfa;

    return-object v0
.end method
