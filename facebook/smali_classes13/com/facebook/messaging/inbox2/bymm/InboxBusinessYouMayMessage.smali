.class public Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

.field public final b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2731080
    new-instance v0, LX/JlO;

    invoke-direct {v0}, LX/JlO;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2731063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2731064
    const-class v0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    .line 2731065
    const-class v0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    .line 2731066
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->c:Z

    .line 2731067
    return-void

    .line 2731068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;)V
    .locals 1

    .prologue
    .line 2731075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2731076
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    .line 2731077
    iput-object p2, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    .line 2731078
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->c:Z

    .line 2731079
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2731074
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2731069
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2731070
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxUserItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2731071
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2731072
    return-void

    .line 2731073
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
