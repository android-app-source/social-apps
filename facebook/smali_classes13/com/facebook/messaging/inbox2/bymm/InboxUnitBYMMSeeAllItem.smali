.class public Lcom/facebook/messaging/inbox2/bymm/InboxUnitBYMMSeeAllItem;
.super Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/bymm/InboxUnitBYMMSeeAllItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItemList;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2731111
    new-instance v0, LX/JlR;

    invoke-direct {v0}, LX/JlR;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/bymm/InboxUnitBYMMSeeAllItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2731105
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;-><init>(Landroid/os/Parcel;)V

    .line 2731106
    const-class v0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItemList;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItemList;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxUnitBYMMSeeAllItem;->b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItemList;

    .line 2731107
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2731108
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;->a(Landroid/os/Parcel;I)V

    .line 2731109
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxUnitBYMMSeeAllItem;->b:Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItemList;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2731110
    return-void
.end method
