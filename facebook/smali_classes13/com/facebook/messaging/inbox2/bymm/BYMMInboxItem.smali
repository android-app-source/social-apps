.class public Lcom/facebook/messaging/inbox2/bymm/BYMMInboxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/bymm/BYMMInboxItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730903
    new-instance v0, LX/JlC;

    invoke-direct {v0}, LX/JlC;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2730900
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2730901
    const-class v0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxItem;->a:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    .line 2730902
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2730899
    sget-object v0, LX/DfY;->V2_BYMM:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2730896
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2730897
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxItem;->a:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2730898
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2730895
    sget-object v0, LX/Dfa;->V2_BYMM_PAGE:LX/Dfa;

    return-object v0
.end method
