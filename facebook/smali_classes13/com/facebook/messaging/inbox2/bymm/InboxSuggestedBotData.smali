.class public Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2731086
    new-instance v0, LX/JlP;

    invoke-direct {v0}, LX/JlP;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/JlQ;)V
    .locals 1

    .prologue
    .line 2731087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2731088
    iget-object v0, p1, LX/JlQ;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->a:Ljava/lang/String;

    .line 2731089
    iget-object v0, p1, LX/JlQ;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->b:Ljava/lang/String;

    .line 2731090
    iget-object v0, p1, LX/JlQ;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->c:Ljava/lang/String;

    .line 2731091
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2731092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2731093
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->a:Ljava/lang/String;

    .line 2731094
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->b:Ljava/lang/String;

    .line 2731095
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->c:Ljava/lang/String;

    .line 2731096
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2731097
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2731098
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2731099
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2731100
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2731101
    return-void
.end method
