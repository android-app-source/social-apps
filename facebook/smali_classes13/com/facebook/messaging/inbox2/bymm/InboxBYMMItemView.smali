.class public Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

.field private b:Lcom/facebook/user/tiles/UserTileView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2730975
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2730976
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->a()V

    .line 2730977
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730978
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2730979
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->a()V

    .line 2730980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730981
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2730982
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->a()V

    .line 2730983
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2730984
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->setOrientation(I)V

    .line 2730985
    const v0, 0x7f03021b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2730986
    const v0, 0x7f0d083f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->b:Lcom/facebook/user/tiles/UserTileView;

    .line 2730987
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->c:Landroid/widget/TextView;

    .line 2730988
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2730989
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2666

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2730990
    const-string v1, "\\s+"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 2730991
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 2730992
    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    .line 2730993
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->c:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 2730994
    :goto_0
    return-void

    .line 2730995
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->c:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;)V
    .locals 2

    .prologue
    .line 2730996
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->a:Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    .line 2730997
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->b:Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    iget-object v1, v1, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2730998
    iget-object v0, p1, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->a(Ljava/lang/String;)V

    .line 2730999
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMItemView;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;->a:Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;

    iget-object v1, v1, Lcom/facebook/messaging/inbox2/bymm/InboxSuggestedBotData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2731000
    return-void
.end method
