.class public Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730925
    new-instance v0, LX/JlE;

    invoke-direct {v0}, LX/JlE;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2730921
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2730922
    const-class v0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItem;->a:Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    .line 2730923
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2730924
    sget-object v0, LX/DfY;->V2_BYMM_VERTICAL:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2730918
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2730919
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/bymm/BYMMInboxVerticalItem;->a:Lcom/facebook/messaging/inbox2/bymm/InboxBusinessYouMayMessage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2730920
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2730917
    sget-object v0, LX/Dfa;->V2_BYMM_VERTICAL:LX/Dfa;

    return-object v0
.end method
