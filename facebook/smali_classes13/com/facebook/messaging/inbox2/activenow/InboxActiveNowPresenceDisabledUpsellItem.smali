.class public Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowPresenceDisabledUpsellItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowPresenceDisabledUpsellItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730761
    new-instance v0, LX/Jl0;

    invoke-direct {v0}, LX/Jl0;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowPresenceDisabledUpsellItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2730762
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2730763
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2730764
    sget-object v0, LX/DfY;->V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/DfY;

    return-object v0
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2730765
    sget-object v0, LX/Dfa;->V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/Dfa;

    return-object v0
.end method
