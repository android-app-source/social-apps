.class public Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;
.super Landroid/view/View;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jkq;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/user/model/UserKey;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/2CH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:LX/6lK;

.field private k:Z

.field private final l:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/Jkq;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/Jks;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private final p:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2730399
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2730400
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    .line 2730401
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    .line 2730402
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    .line 2730403
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->n:Ljava/util/LinkedList;

    .line 2730404
    new-instance v0, LX/Jkr;

    invoke-direct {v0, p0}, LX/Jkr;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->p:Ljava/util/Comparator;

    .line 2730405
    invoke-direct {p0, p2, v1, v1}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Landroid/util/AttributeSet;II)V

    .line 2730406
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2730515
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2730516
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    .line 2730517
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    .line 2730518
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    .line 2730519
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->n:Ljava/util/LinkedList;

    .line 2730520
    new-instance v0, LX/Jkr;

    invoke-direct {v0, p0}, LX/Jkr;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->p:Ljava/util/Comparator;

    .line 2730521
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Landroid/util/AttributeSet;II)V

    .line 2730522
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    .prologue
    .line 2730523
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 2730524
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    .line 2730525
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    .line 2730526
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    .line 2730527
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->n:Ljava/util/LinkedList;

    .line 2730528
    new-instance v0, LX/Jkr;

    invoke-direct {v0, p0}, LX/Jkr;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->p:Ljava/util/Comparator;

    .line 2730529
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Landroid/util/AttributeSet;II)V

    .line 2730530
    return-void
.end method

.method private a(Lcom/facebook/user/model/UserKey;)LX/Jkq;
    .locals 2

    .prologue
    .line 2730531
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jkq;

    .line 2730532
    if-nez v0, :cond_0

    .line 2730533
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->b(Lcom/facebook/user/model/UserKey;)LX/Jkq;

    move-result-object v0

    .line 2730534
    iget-object v1, v0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->c()V

    .line 2730535
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2730536
    :cond_0
    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 2730537
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2730538
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2730539
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2730540
    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Lcom/facebook/user/model/UserKey;)LX/Jkq;

    move-result-object v0

    .line 2730541
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2730542
    sget-object v2, LX/Jko;->a:[I

    iget-object v3, v0, LX/Jkq;->d:LX/Jkp;

    invoke-virtual {v3}, LX/Jkp;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2730543
    :cond_1
    :goto_1
    iget-object v2, v0, LX/Jkq;->d:LX/Jkp;

    sget-object v3, LX/Jkp;->GONE:LX/Jkp;

    if-eq v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 2730544
    if-nez v0, :cond_0

    .line 2730545
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2730546
    :cond_2
    return-void

    .line 2730547
    :pswitch_0
    invoke-static {v0}, LX/Jkq;->k(LX/Jkq;)F

    move-result v2

    .line 2730548
    cmpl-float v2, v2, v4

    if-ltz v2, :cond_1

    .line 2730549
    sget-object v2, LX/Jkp;->VISIBLE:LX/Jkp;

    iput-object v2, v0, LX/Jkq;->d:LX/Jkp;

    goto :goto_1

    .line 2730550
    :pswitch_1
    invoke-static {v0}, LX/Jkq;->k(LX/Jkq;)F

    move-result v2

    .line 2730551
    cmpl-float v2, v2, v4

    if-ltz v2, :cond_1

    .line 2730552
    sget-object v2, LX/Jkp;->GONE:LX/Jkp;

    iput-object v2, v0, LX/Jkq;->d:LX/Jkp;

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2730553
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    and-int/lit8 v0, v0, 0x70

    sparse-switch v0, :sswitch_data_0

    .line 2730554
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2730555
    :goto_0
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2730556
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    sub-int/2addr v0, v1

    .line 2730557
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int v0, v1, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2730558
    :goto_1
    return-void

    .line 2730559
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 2730560
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2730561
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingTop()I

    move-result v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 2730562
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 2730563
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2730564
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2730565
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2730566
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2730567
    return-void
.end method

.method private a(Landroid/util/AttributeSet;II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2730568
    const-class v1, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;

    invoke-static {v1, p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2730569
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->ActiveNowTickerView:[I

    invoke-virtual {v1, p1, v2, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2730570
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    .line 2730571
    const/16 v2, 0x2

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->h:I

    .line 2730572
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    .line 2730573
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2730574
    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Missing activeNowTickerTileSize attribute"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2730575
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2730576
    new-instance v1, LX/6lK;

    new-instance v2, LX/6lL;

    invoke-direct {v2, v0}, LX/6lL;-><init>(Landroid/content/res/Resources;)V

    iget v3, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    .line 2730577
    iput v3, v2, LX/6lL;->b:I

    .line 2730578
    move-object v2, v2

    .line 2730579
    const v3, 0x7f0a09a5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2730580
    iput v3, v2, LX/6lL;->c:I

    .line 2730581
    move-object v2, v2

    .line 2730582
    const v3, 0x7f0a09a6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2730583
    iput v3, v2, LX/6lL;->d:I

    .line 2730584
    move-object v2, v2

    .line 2730585
    const v3, 0x7f0b0804

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2730586
    iput v0, v2, LX/6lL;->e:I

    .line 2730587
    move-object v0, v2

    .line 2730588
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2730589
    iput-object v2, v0, LX/6lL;->f:Landroid/graphics/Typeface;

    .line 2730590
    move-object v0, v0

    .line 2730591
    new-instance v2, LX/6lM;

    invoke-direct {v2, v0}, LX/6lM;-><init>(LX/6lL;)V

    move-object v0, v2

    .line 2730592
    invoke-direct {v1, v0}, LX/6lK;-><init>(LX/6lM;)V

    iput-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->j:LX/6lK;

    .line 2730593
    return-void
.end method

.method private static a(Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;LX/0Or;Lcom/facebook/user/model/UserKey;LX/2CH;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;",
            "LX/0Or",
            "<",
            "LX/Jkq;",
            ">;",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/presence/PresenceManager;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2730594
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->b:Lcom/facebook/user/model/UserKey;

    iput-object p3, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->c:LX/2CH;

    iput-object p4, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->d:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;

    const/16 v0, 0x279d

    invoke-static {v2, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v2}, LX/41q;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-static {v2}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v1

    check-cast v1, LX/2CH;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0, v3, v0, v1, v2}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;LX/0Or;Lcom/facebook/user/model/UserKey;LX/2CH;Ljava/util/concurrent/ScheduledExecutorService;)V

    return-void
.end method

.method private b(Lcom/facebook/user/model/UserKey;)LX/Jkq;
    .locals 6

    .prologue
    .line 2730595
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jkq;

    .line 2730596
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    .line 2730597
    iput v2, v0, LX/Jkq;->c:I

    .line 2730598
    iget-object v3, v0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v4, v2, v5}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Landroid/content/Context;ZILX/8ug;)V

    .line 2730599
    iget-object v3, v0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-static {p1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(LX/8t9;)V

    .line 2730600
    iget-object v3, v0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    new-instance v4, LX/Jkn;

    invoke-direct {v4, v0}, LX/Jkn;-><init>(LX/Jkq;)V

    .line 2730601
    iput-object v4, v3, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    .line 2730602
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a09a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v0, LX/Jkq;->f:I

    .line 2730603
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a09a4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v0, LX/Jkq;->g:I

    .line 2730604
    invoke-virtual {v0, p0}, LX/Jkq;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2730605
    return-object v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2730511
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getLayoutDirection()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    packed-switch v0, :pswitch_data_0

    .line 2730512
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This view only supports left and right alignment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2730513
    :pswitch_1
    const/4 v0, 0x1

    .line 2730514
    :goto_0
    return v0

    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c()V
    .locals 6

    .prologue
    .line 2730496
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jks;

    .line 2730497
    iget-boolean v1, v0, LX/Jks;->b:Z

    if-eqz v1, :cond_0

    .line 2730498
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    const/4 v2, 0x0

    iget-object v3, v0, LX/Jks;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 2730499
    iget-object v0, v0, LX/Jks;->a:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Lcom/facebook/user/model/UserKey;)LX/Jkq;

    move-result-object v0

    .line 2730500
    iget-object v4, v0, LX/Jkq;->d:LX/Jkp;

    sget-object v5, LX/Jkp;->ENTERING:LX/Jkp;

    if-ne v4, v5, :cond_1

    .line 2730501
    :goto_0
    return-void

    .line 2730502
    :cond_0
    iget-object v0, v0, LX/Jks;->a:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Lcom/facebook/user/model/UserKey;)LX/Jkq;

    move-result-object v0

    .line 2730503
    iget-object v4, v0, LX/Jkq;->d:LX/Jkp;

    sget-object v5, LX/Jkp;->EXITING:LX/Jkp;

    if-ne v4, v5, :cond_2

    .line 2730504
    :goto_1
    goto :goto_0

    .line 2730505
    :cond_1
    sget-object v4, LX/Jkp;->ENTERING:LX/Jkp;

    iput-object v4, v0, LX/Jkq;->d:LX/Jkp;

    .line 2730506
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v0, LX/Jkq;->h:J

    .line 2730507
    invoke-virtual {v0}, LX/Jkq;->invalidateSelf()V

    goto :goto_0

    .line 2730508
    :cond_2
    sget-object v4, LX/Jkp;->EXITING:LX/Jkp;

    iput-object v4, v0, LX/Jkq;->d:LX/Jkp;

    .line 2730509
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v0, LX/Jkq;->h:J

    .line 2730510
    invoke-virtual {v0}, LX/Jkq;->invalidateSelf()V

    goto :goto_1
.end method

.method private c(Lcom/facebook/user/model/UserKey;)Z
    .locals 3

    .prologue
    .line 2730489
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2730490
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2730491
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jks;

    .line 2730492
    iget-object v2, v0, LX/Jks;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2, p1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v0, v0, LX/Jks;->b:Z

    if-nez v0, :cond_0

    .line 2730493
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 2730494
    const/4 v0, 0x1

    .line 2730495
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setActiveUsersAndSort(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2730483
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 2730484
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2730485
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v2}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2730486
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2730487
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->p:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2730488
    return-void
.end method


# virtual methods
.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x26199f9a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2730476
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2730477
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->k:Z

    .line 2730478
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jkq;

    .line 2730479
    iget-object v3, v0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v3}, Lcom/facebook/user/tiles/UserTileDrawableController;->d()V

    .line 2730480
    goto :goto_0

    .line 2730481
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2730482
    const v0, 0x61e9e557

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2730426
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2730427
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a()V

    .line 2730428
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2730429
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Landroid/graphics/Canvas;)V

    .line 2730430
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    if-gt v0, v1, :cond_2

    move v7, v6

    .line 2730431
    :goto_0
    if-eqz v7, :cond_f

    .line 2730432
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    add-int/lit8 v1, v1, -0x1

    mul-int v3, v0, v1

    move v4, v3

    .line 2730433
    :goto_1
    if-eqz v7, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v1, v0

    :goto_2
    move v5, v4

    move v4, v3

    move v3, v2

    .line 2730434
    :cond_0
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2730435
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2730436
    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Lcom/facebook/user/model/UserKey;)LX/Jkq;

    move-result-object v8

    .line 2730437
    iget v9, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    if-lt v2, v9, :cond_4

    .line 2730438
    iget-object v9, v8, LX/Jkq;->d:LX/Jkp;

    sget-object v10, LX/Jkp;->EXITING:LX/Jkp;

    if-ne v9, v10, :cond_10

    const/4 v9, 0x1

    :goto_4
    move v8, v9

    .line 2730439
    if-nez v8, :cond_1

    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->c(Lcom/facebook/user/model/UserKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2730440
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    :cond_2
    move v7, v2

    .line 2730441
    goto :goto_0

    .line 2730442
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 2730443
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2730444
    invoke-virtual {v8}, LX/Jkq;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    int-to-float v0, v4

    :goto_5
    const/4 v9, 0x0

    invoke-virtual {p1, v0, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2730445
    add-int/lit8 v0, v2, 0x1

    .line 2730446
    invoke-virtual {v8, p1}, LX/Jkq;->draw(Landroid/graphics/Canvas;)V

    .line 2730447
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2730448
    if-eqz v7, :cond_6

    .line 2730449
    iget v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v9, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v2, v9

    sub-int/2addr v4, v2

    .line 2730450
    int-to-float v2, v5

    iget v5, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v9, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v5, v9

    int-to-float v5, v5

    invoke-virtual {v8}, LX/Jkq;->e()F

    move-result v9

    mul-float/2addr v5, v9

    sub-float/2addr v2, v5

    float-to-int v5, v2

    .line 2730451
    :goto_6
    invoke-virtual {v8}, LX/Jkq;->g()Z

    move-result v2

    if-eqz v2, :cond_e

    move v2, v6

    :goto_7
    move v3, v2

    move v2, v0

    .line 2730452
    goto :goto_3

    .line 2730453
    :cond_5
    int-to-float v0, v5

    goto :goto_5

    .line 2730454
    :cond_6
    iget v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v9, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v2, v9

    add-int/2addr v4, v2

    .line 2730455
    int-to-float v2, v5

    iget v5, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v9, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v5, v9

    int-to-float v5, v5

    invoke-virtual {v8}, LX/Jkq;->e()F

    move-result v9

    mul-float/2addr v5, v9

    add-float/2addr v2, v5

    float-to-int v5, v2

    goto :goto_6

    .line 2730456
    :cond_7
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    sub-int/2addr v0, v2

    .line 2730457
    if-ne v0, v6, :cond_a

    .line 2730458
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Lcom/facebook/user/model/UserKey;)LX/Jkq;

    move-result-object v0

    .line 2730459
    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 2730460
    invoke-virtual {v0}, LX/Jkq;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_8
    move v3, v6

    .line 2730461
    :cond_8
    :goto_9
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2730462
    if-eqz v3, :cond_c

    .line 2730463
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->postInvalidate()V

    .line 2730464
    :cond_9
    :goto_a
    return-void

    .line 2730465
    :cond_a
    if-lez v0, :cond_8

    .line 2730466
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->o:I

    sub-int/2addr v0, v2

    .line 2730467
    if-nez v3, :cond_b

    .line 2730468
    add-int/lit8 v0, v0, 0x1

    .line 2730469
    :cond_b
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->j:LX/6lK;

    .line 2730470
    iput v0, v1, LX/6lK;->b:I

    .line 2730471
    invoke-virtual {v1}, LX/6lK;->invalidateSelf()V

    .line 2730472
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->j:LX/6lK;

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto :goto_9

    .line 2730473
    :cond_c
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2730474
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->c()V

    .line 2730475
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->postInvalidate()V

    goto :goto_a

    :cond_d
    move v6, v3

    goto :goto_8

    :cond_e
    move v2, v3

    goto :goto_7

    :cond_f
    move v3, v2

    move v4, v2

    goto/16 :goto_1

    :cond_10
    const/4 v9, 0x0

    goto/16 :goto_4
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2730419
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2730420
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 2730421
    :goto_0
    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    .line 2730422
    invoke-virtual {p0, v0, v1}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->setMeasuredDimension(II)V

    .line 2730423
    return-void

    .line 2730424
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->h:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 2730425
    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x5bdaed55

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2730412
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 2730413
    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2730414
    iget v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->h:I

    iput v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    .line 2730415
    :goto_0
    const v1, 0x75f8af

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 2730416
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingLeft()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2730417
    iget v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->e:I

    iget v3, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->f:I

    add-int/2addr v2, v3

    .line 2730418
    div-int/2addr v1, v2

    iput v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->g:I

    goto :goto_0
.end method

.method public setGravity(I)V
    .locals 1

    .prologue
    .line 2730408
    iget v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    if-eq v0, p1, :cond_0

    .line 2730409
    iput p1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->i:I

    .line 2730410
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowTickerView;->invalidate()V

    .line 2730411
    :cond_0
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2730407
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p1, LX/Jkq;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
