.class public Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/Iu3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2730171
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;

    invoke-static {v0}, LX/Iu3;->b(LX/0QB;)LX/Iu3;

    move-result-object v0

    check-cast v0, LX/Iu3;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;->p:LX/Iu3;

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2730172
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2730173
    instance-of v0, p1, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    if-eqz v0, :cond_0

    .line 2730174
    check-cast p1, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    .line 2730175
    new-instance v0, LX/Jkd;

    invoke-direct {v0, p0}, LX/Jkd;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;)V

    .line 2730176
    iput-object v0, p1, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->l:LX/Jkc;

    .line 2730177
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730178
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2730179
    invoke-static {p0, p0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2730180
    const v0, 0x7f0308e1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;->setContentView(I)V

    .line 2730181
    const v0, 0x7f083b58

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;->setTitle(I)V

    .line 2730182
    return-void
.end method
