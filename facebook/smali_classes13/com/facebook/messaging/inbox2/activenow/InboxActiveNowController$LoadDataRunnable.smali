.class public final Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:J

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Jkz;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/Jkz;JZ)V
    .locals 2

    .prologue
    .line 2730695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730696
    iput-wide p2, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->a:J

    .line 2730697
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->b:Ljava/lang/ref/WeakReference;

    .line 2730698
    iput-boolean p4, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->c:Z

    .line 2730699
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2730700
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jkz;

    .line 2730701
    iget-boolean v1, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->c:Z

    if-eqz v1, :cond_0

    .line 2730702
    const/4 v1, 0x0

    .line 2730703
    iput-object v1, v0, LX/Jkz;->y:Ljava/util/concurrent/ScheduledFuture;

    .line 2730704
    :cond_0
    if-eqz v0, :cond_1

    iget-wide v2, v0, LX/Jkz;->t:J

    iget-wide v4, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->a:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 2730705
    :cond_1
    :goto_0
    return-void

    .line 2730706
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$LoadDataRunnable;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2730707
    invoke-static {v0}, LX/Jkz;->e(LX/Jkz;)V

    goto :goto_0
.end method
