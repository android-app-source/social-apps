.class public Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:LX/Jkh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/3Kl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/2CH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsLightweightActionsActiveNowEntryPointEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Jkg;

.field public g:LX/3Lb;

.field private h:Landroid/support/v7/widget/RecyclerView;

.field private i:Landroid/support/v7/widget/Toolbar;

.field private j:Landroid/content/Context;

.field public k:J

.field public l:LX/Jkc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/JnI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:LX/3Mg;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2730285
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2730286
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->k:J

    .line 2730287
    new-instance v0, LX/Jki;

    invoke-direct {v0, p0}, LX/Jki;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->n:LX/3Mg;

    return-void
.end method

.method private static a(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;LX/Jkh;LX/3Kl;LX/2CH;LX/0Or;LX/0SG;LX/JnI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;",
            "LX/Jkh;",
            "LX/3Kl;",
            "Lcom/facebook/presence/PresenceManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0SG;",
            "LX/JnI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2730288
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->a:LX/Jkh;

    iput-object p2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->b:LX/3Kl;

    iput-object p3, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->c:LX/2CH;

    iput-object p4, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->e:LX/0SG;

    iput-object p6, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->m:LX/JnI;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    const-class v1, LX/Jkh;

    invoke-interface {v6, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Jkh;

    invoke-static {v6}, LX/3Kl;->a(LX/0QB;)LX/3Kl;

    move-result-object v2

    check-cast v2, LX/3Kl;

    invoke-static {v6}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v3

    check-cast v3, LX/2CH;

    const/16 v4, 0x151d

    invoke-static {v6, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v6}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v7, LX/JnI;

    invoke-interface {v6, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/JnI;

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->a(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;LX/Jkh;LX/3Kl;LX/2CH;LX/0Or;LX/0SG;LX/JnI;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730289
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2730290
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0104b3

    const v2, 0x7f0e0553

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    .line 2730291
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    const v2, 0x7f0e058b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    .line 2730292
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    invoke-static {p0, v1}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2730293
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2730294
    new-instance v2, LX/Jkg;

    invoke-direct {v2, v1, v0}, LX/Jkg;-><init>(Landroid/content/Context;Z)V

    .line 2730295
    move-object v0, v2

    .line 2730296
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->f:LX/Jkg;

    .line 2730297
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->f:LX/Jkg;

    new-instance v1, LX/Jkj;

    invoke-direct {v1, p0}, LX/Jkj;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V

    .line 2730298
    iput-object v1, v0, LX/Jkg;->e:LX/Jkj;

    .line 2730299
    return-void
.end method

.method public final b()LX/JnH;
    .locals 14

    .prologue
    .line 2730300
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->m:LX/JnI;

    .line 2730301
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 2730302
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2730303
    new-instance v3, LX/Jkm;

    invoke-direct {v3, p0}, LX/Jkm;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V

    .line 2730304
    new-instance v4, LX/JnH;

    .line 2730305
    new-instance v6, LX/JnE;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v5

    check-cast v5, LX/1zC;

    invoke-direct {v6, v5}, LX/JnE;-><init>(LX/1zC;)V

    .line 2730306
    move-object v5, v6

    .line 2730307
    check-cast v5, LX/JnE;

    invoke-static {v0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v6

    check-cast v6, LX/3Ec;

    invoke-static {v0}, LX/3Rl;->a(LX/0QB;)LX/3Rl;

    move-result-object v7

    check-cast v7, LX/3Rl;

    invoke-static {v0}, LX/FFh;->a(LX/0QB;)LX/FFh;

    move-result-object v8

    check-cast v8, LX/FFh;

    const/16 v9, 0x291f

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2923

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v11, v1

    move-object v12, v2

    move-object v13, v3

    invoke-direct/range {v4 .. v13}, LX/JnH;-><init>(LX/JnE;LX/3Ec;LX/3Rl;LX/FFh;LX/0Ot;LX/0Ot;LX/0gc;Landroid/view/View;LX/Jkm;)V

    .line 2730308
    move-object v0, v4

    .line 2730309
    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x14429f40

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2730310
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308e2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1d15e630

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x374ea211

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2730311
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2730312
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->g:LX/3Lb;

    invoke-virtual {v1}, LX/3Lb;->b()V

    .line 2730313
    iput-object v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2730314
    iput-object v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->i:Landroid/support/v7/widget/Toolbar;

    .line 2730315
    const/16 v1, 0x2b

    const v2, 0x4a9ceda8    # 5142228.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c648362

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2730316
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2730317
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->c:LX/2CH;

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->n:LX/3Mg;

    invoke-virtual {v1, v2}, LX/2CH;->b(LX/3Mg;)V

    .line 2730318
    const/16 v1, 0x2b

    const v2, 0x7974a736

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x606c0acc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2730319
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2730320
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->c:LX/2CH;

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->n:LX/3Mg;

    invoke-virtual {v1, v2}, LX/2CH;->a(LX/3Mg;)V

    .line 2730321
    const/16 v1, 0x2b

    const v2, -0xf96493c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2730322
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2730323
    const v0, 0x7f0d1702

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->i:Landroid/support/v7/widget/Toolbar;

    .line 2730324
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->i:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f083b58

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 2730325
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->i:Landroid/support/v7/widget/Toolbar;

    new-instance v1, LX/Jkk;

    invoke-direct {v1, p0}, LX/Jkk;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2730326
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->j:Landroid/content/Context;

    const v1, 0x7f01058d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0WH;->d(Landroid/content/Context;II)I

    move-result v0

    .line 2730327
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->i:Landroid/support/v7/widget/Toolbar;

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0vv;->f(Landroid/view/View;F)V

    .line 2730328
    const v0, 0x7f0d1703

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2730329
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->h:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2730330
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->f:LX/Jkg;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2730331
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->b:LX/3Kl;

    invoke-virtual {v0}, LX/3Kl;->b()LX/3Lb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->g:LX/3Lb;

    .line 2730332
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->g:LX/3Lb;

    new-instance v1, LX/Jkl;

    invoke-direct {v1, p0}, LX/Jkl;-><init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V

    .line 2730333
    iput-object v1, v0, LX/3Lb;->B:LX/3Mb;

    .line 2730334
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->g:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->a()V

    .line 2730335
    return-void
.end method
