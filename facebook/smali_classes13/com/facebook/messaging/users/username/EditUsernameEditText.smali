.class public Lcom/facebook/messaging/users/username/EditUsernameEditText;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:LX/JsA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/widget/EditText;

.field private e:Landroid/view/View;

.field public f:Landroid/widget/TextView;

.field private g:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2745166
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2745167
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g()V

    .line 2745168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2745163
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2745164
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g()V

    .line 2745165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2745160
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2745161
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g()V

    .line 2745162
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2745150
    const v0, 0x7f030d07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2745151
    invoke-virtual {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->a:I

    .line 2745152
    invoke-virtual {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->b:I

    .line 2745153
    const v0, 0x7f0d2091

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->f:Landroid/widget/TextView;

    .line 2745154
    const v0, 0x7f0d2090

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->e:Landroid/view/View;

    .line 2745155
    const v0, 0x7f0d208e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    .line 2745156
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    new-instance v1, LX/Js9;

    invoke-direct {v1, p0}, LX/Js9;-><init>(Lcom/facebook/messaging/users/username/EditUsernameEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2745157
    invoke-virtual {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->f()V

    .line 2745158
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->h()V

    .line 2745159
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2745130
    const v0, 0x7f0d208f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g:Landroid/widget/ProgressBar;

    .line 2745131
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2745132
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2745148
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2745149
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2745146
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->g:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2745147
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2745143
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2745144
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2745145
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2745140
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2745141
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2745142
    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2745139
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745135
    if-eqz p1, :cond_0

    .line 2745136
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2745137
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->d:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 2745138
    :cond_0
    return-void
.end method

.method public setUsernameAvailabilityListener(LX/JsA;)V
    .locals 0
    .param p1    # LX/JsA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745133
    iput-object p1, p0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->c:LX/JsA;

    .line 2745134
    return-void
.end method
