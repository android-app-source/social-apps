.class public final Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x25bcb6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2745419
    const-class v0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2745418
    const-class v0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2745416
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2745417
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745379
    iput-object p1, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->f:Ljava/lang/String;

    .line 2745380
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2745381
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2745382
    if-eqz v0, :cond_0

    .line 2745383
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2745384
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2745414
    iget-object v0, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->e:Ljava/lang/String;

    .line 2745415
    iget-object v0, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2745412
    iget-object v0, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->f:Ljava/lang/String;

    .line 2745413
    iget-object v0, p0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2745404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2745405
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2745406
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2745407
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2745408
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2745409
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2745410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2745411
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2745401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2745402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2745403
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2745400
    new-instance v0, LX/JsL;

    invoke-direct {v0, p1}, LX/JsL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2745399
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2745393
    const-string v0, "username"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2745394
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2745395
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2745396
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2745397
    :goto_0
    return-void

    .line 2745398
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2745390
    const-string v0, "username"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2745391
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;->a(Ljava/lang/String;)V

    .line 2745392
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2745387
    new-instance v0, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;

    invoke-direct {v0}, Lcom/facebook/messaging/users/username/graphql/EditUsernameMutationModels$EditUsernameMutationFieldsModel$UserModel;-><init>()V

    .line 2745388
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2745389
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2745386
    const v0, -0x282d958a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2745385
    const v0, 0x285feb

    return v0
.end method
