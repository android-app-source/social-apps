.class public Lcom/facebook/messaging/users/username/EditUsernameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/JsI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/JsQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:LX/JsP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

.field public l:LX/JsH;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public m:Landroid/view/MenuItem;

.field private n:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private p:Landroid/support/v7/widget/Toolbar;

.field public q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2745304
    const-string v0, "https://www.facebook.com/help/105399436216001"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2745296
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2745297
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745298
    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->c:LX/0Ot;

    .line 2745299
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745300
    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->e:LX/0Ot;

    .line 2745301
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2745302
    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->i:LX/0Ot;

    .line 2745303
    return-void
.end method

.method private a(Landroid/content/res/Resources;)Landroid/text/SpannableString;
    .locals 3

    .prologue
    .line 2745289
    new-instance v0, LX/47x;

    invoke-direct {v0, p1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2745290
    const v1, 0x7f0809a0

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2745291
    new-instance v1, LX/JsG;

    invoke-direct {v1, p0}, LX/JsG;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 2745292
    const-string v1, " "

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2745293
    const v1, 0x7f0809a1

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2745294
    invoke-virtual {v0}, LX/47x;->a()LX/47x;

    .line 2745295
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/users/username/EditUsernameFragment;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/JsI;LX/0Ot;LX/0Or;LX/JsQ;LX/JsP;LX/0Ot;LX/0kL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/users/username/EditUsernameFragment;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/JsI;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/JsQ;",
            "LX/JsP;",
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2745288
    iput-object p1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->b:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->c:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->d:LX/JsI;

    iput-object p4, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->e:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->f:LX/0Or;

    iput-object p6, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->g:LX/JsQ;

    iput-object p7, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->h:LX/JsP;

    iput-object p8, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->i:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->j:LX/0kL;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-static {v9}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    const/16 v2, 0x542

    invoke-static {v9, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    new-instance v4, LX/JsI;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {v4, v3}, LX/JsI;-><init>(LX/0Zb;)V

    move-object v3, v4

    check-cast v3, LX/JsI;

    const/16 v4, 0x259

    invoke-static {v9, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x12cb

    invoke-static {v9, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    new-instance v6, LX/JsQ;

    const/16 v7, 0x12cb

    invoke-static {v9, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct {v6, v7}, LX/JsQ;-><init>(LX/0Or;)V

    move-object v6, v6

    check-cast v6, LX/JsQ;

    invoke-static {v9}, LX/JsP;->b(LX/0QB;)LX/JsP;

    move-result-object v7

    check-cast v7, LX/JsP;

    const/16 v8, 0x27c5

    invoke-static {v9, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v9}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static/range {v0 .. v9}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->a(Lcom/facebook/messaging/users/username/EditUsernameFragment;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/JsI;LX/0Ot;LX/0Or;LX/JsQ;LX/JsP;LX/0Ot;LX/0kL;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/users/username/EditUsernameFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2745281
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2745282
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2745283
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->h:LX/JsP;

    .line 2745284
    iget-object v1, v0, LX/JsP;->a:LX/0tX;

    const/4 v2, 0x0

    invoke-static {v0, p1, v2}, LX/JsP;->a(LX/JsP;Ljava/lang/String;Z)LX/399;

    move-result-object v2

    sget-object v3, LX/3Fz;->a:LX/3Fz;

    invoke-virtual {v1, v2, v3}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2745285
    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2745286
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/JsE;

    invoke-direct {v1, p0}, LX/JsE;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    iget-object v2, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2745287
    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/users/username/EditUsernameFragment;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2745220
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2745221
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2745222
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2745223
    sget-object v0, Lcom/facebook/messaging/service/model/EditUsernameParams;->a:Ljava/lang/String;

    new-instance v1, Lcom/facebook/messaging/service/model/EditUsernameParams;

    invoke-direct {v1, p1}, Lcom/facebook/messaging/service/model/EditUsernameParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2745224
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "save_username"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x772e201a

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2745225
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/JsF;

    invoke-direct {v1, p0}, LX/JsF;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    iget-object v2, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2745226
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2745273
    const v0, 0x7f0d2092

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->p:Landroid/support/v7/widget/Toolbar;

    .line 2745274
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->p:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f08099c

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 2745275
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->p:Landroid/support/v7/widget/Toolbar;

    new-instance v1, LX/JsB;

    invoke-direct {v1, p0}, LX/JsB;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2745276
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->p:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f11002f

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->a(I)V

    .line 2745277
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->p:Landroid/support/v7/widget/Toolbar;

    new-instance v1, LX/JsC;

    invoke-direct {v1, p0}, LX/JsC;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    .line 2745278
    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->D:LX/3xb;

    .line 2745279
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->d()V

    .line 2745280
    return-void
.end method

.method public static synthetic d(Lcom/facebook/messaging/users/username/EditUsernameFragment;)I
    .locals 2

    .prologue
    .line 2745272
    iget v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->q:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->q:I

    return v0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 2745267
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->p:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0d3259

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->m:Landroid/view/MenuItem;

    .line 2745268
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2745269
    iget-object v1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->m:Landroid/view/MenuItem;

    const v2, 0x7f0e0574

    const v3, 0x7f020f95

    const v4, 0x7f0a00d5

    invoke-static {v0, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v0, v2, v3, v4}, LX/Hp3;->a(Landroid/content/Context;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2745270
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->m:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2745271
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2745257
    const v0, 0x7f0d2093

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/users/username/EditUsernameEditText;

    iput-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    .line 2745258
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    iget-object v1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->g:LX/JsQ;

    .line 2745259
    iget-object v2, v1, LX/JsQ;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2745260
    if-eqz v2, :cond_0

    .line 2745261
    iget-object v1, v2, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v2, v1

    .line 2745262
    :goto_0
    move-object v1, v2

    .line 2745263
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/users/username/EditUsernameEditText;->setText(Ljava/lang/String;)V

    .line 2745264
    iget-object v0, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k:Lcom/facebook/messaging/users/username/EditUsernameEditText;

    new-instance v1, LX/JsD;

    invoke-direct {v1, p0}, LX/JsD;-><init>(Lcom/facebook/messaging/users/username/EditUsernameFragment;)V

    .line 2745265
    iput-object v1, v0, Lcom/facebook/messaging/users/username/EditUsernameEditText;->c:LX/JsA;

    .line 2745266
    return-void

    :cond_0
    const-string v2, ""

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2745245
    const v0, 0x7f0d2094

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2745246
    iget-object v1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2745247
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->F:Z

    move v1, v2

    .line 2745248
    if-eqz v1, :cond_0

    .line 2745249
    const v1, 0x7f08099f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2745250
    :goto_0
    const v0, 0x7f0d2095

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2745251
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->a(Landroid/content/res/Resources;)Landroid/text/SpannableString;

    move-result-object v1

    .line 2745252
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2745253
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2745254
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 2745255
    return-void

    .line 2745256
    :cond_0
    const v1, 0x7f08099e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2745242
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2745243
    const-class v0, Lcom/facebook/messaging/users/username/EditUsernameFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2745244
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5630b8a7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2745241
    const v1, 0x7f030d08

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7b728c7a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x24b2f686

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2745237
    iget-object v1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2745238
    iget-object v1, p0, Lcom/facebook/messaging/users/username/EditUsernameFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2745239
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2745240
    const/16 v1, 0x2b

    const v2, -0x6f851589

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4b5aa4fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2745232
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2745233
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2745234
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2745235
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2745236
    const/16 v1, 0x2b

    const v2, -0x1207e3f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2745227
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2745228
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->c()V

    .line 2745229
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->e()V

    .line 2745230
    invoke-direct {p0}, Lcom/facebook/messaging/users/username/EditUsernameFragment;->k()V

    .line 2745231
    return-void
.end method
