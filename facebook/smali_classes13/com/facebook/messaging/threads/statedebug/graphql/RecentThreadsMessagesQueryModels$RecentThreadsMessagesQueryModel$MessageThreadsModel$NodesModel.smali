.class public final Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3cdcce85
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2744634
    const-class v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2744637
    const-class v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2744635
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2744636
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2744613
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2744614
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2744615
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2744616
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2744617
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2744618
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2744619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2744620
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2744621
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2744622
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2744623
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    .line 2744624
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2744625
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;

    .line 2744626
    iput-object v0, v1, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->e:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    .line 2744627
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2744628
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    .line 2744629
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2744630
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;

    .line 2744631
    iput-object v0, v1, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->f:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    .line 2744632
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2744633
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2744611
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->e:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    iput-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->e:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    .line 2744612
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->e:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$MessagesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2744608
    new-instance v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;-><init>()V

    .line 2744609
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2744610
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2744607
    const v0, -0xa9dee62

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2744606
    const v0, -0x2c24372f

    return v0
.end method

.method public final j()Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2744604
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->f:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    iput-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->f:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    .line 2744605
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;->f:Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    return-object v0
.end method
