.class public final Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x75633769
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2744671
    const-class v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2744670
    const-class v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2744668
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2744669
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2744660
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2744661
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2744662
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2744663
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2744664
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2744665
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2744666
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2744667
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2744672
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->e:Ljava/util/List;

    .line 2744673
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2744652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2744653
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2744654
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2744655
    if-eqz v1, :cond_0

    .line 2744656
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;

    .line 2744657
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->e:Ljava/util/List;

    .line 2744658
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2744659
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2744649
    new-instance v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;-><init>()V

    .line 2744650
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2744651
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2744648
    const v0, 0x14b9d421

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2744647
    const v0, 0x6cff0a72

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2744645
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->f:Ljava/lang/String;

    .line 2744646
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel;->f:Ljava/lang/String;

    return-object v0
.end method
