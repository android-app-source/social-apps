.class public final Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x38fa1931
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2744580
    const-class v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2744603
    const-class v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2744601
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2744602
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2744593
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2744594
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2744595
    invoke-virtual {p0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2744596
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2744597
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2744598
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2744599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2744600
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2744590
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2744591
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2744592
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2744588
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->e:Ljava/lang/String;

    .line 2744589
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2744585
    new-instance v0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;

    invoke-direct {v0}, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;-><init>()V

    .line 2744586
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2744587
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2744584
    const v0, -0x1a5eaa87

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2744583
    const v0, 0x358f5fce

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2744581
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->f:Ljava/lang/String;

    .line 2744582
    iget-object v0, p0, Lcom/facebook/messaging/threads/statedebug/graphql/RecentThreadsMessagesQueryModels$RecentThreadsMessagesQueryModel$MessageThreadsModel$NodesModel$ThreadKeyModel;->f:Ljava/lang/String;

    return-object v0
.end method
