.class public Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""

# interfaces
.implements LX/10X;


# static fields
.field public static final a:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;


# instance fields
.field public b:LX/CIm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Jv4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1sd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/8DZ;

.field public m:Landroid/preference/PreferenceScreen;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2749801
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "appUpdates/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2749802
    sput-object v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->a:LX/0Tn;

    const-string v1, "fb4a_auto_updates_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->i:LX/0Tn;

    .line 2749803
    sget-object v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->a:LX/0Tn;

    const-string v1, "fb4a_auto_update_notification_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->j:LX/0Tn;

    .line 2749804
    sget-object v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->a:LX/0Tn;

    const-string v1, "fb4a_auto_update_complete_notification_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->k:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2749800
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;LX/CIm;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/Jv4;LX/03V;LX/0Zb;LX/1sd;)V
    .locals 0

    .prologue
    .line 2749799
    iput-object p1, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->b:LX/CIm;

    iput-object p2, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->c:LX/0TD;

    iput-object p3, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->d:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->e:LX/Jv4;

    iput-object p5, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->f:LX/03V;

    iput-object p6, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->g:LX/0Zb;

    iput-object p7, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->h:LX/1sd;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;

    invoke-static {v7}, LX/CIm;->b(LX/0QB;)LX/CIm;

    move-result-object v1

    check-cast v1, LX/CIm;

    invoke-static {v7}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    new-instance v8, LX/Jv4;

    const-class v9, Landroid/content/Context;

    invoke-interface {v7, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {v7}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, LX/0TD;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v8 .. v13}, LX/Jv4;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/0TD;Ljava/util/concurrent/ExecutorService;)V

    move-object v4, v8

    check-cast v4, LX/Jv4;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v7}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v7}, LX/1sb;->b(LX/0QB;)LX/1sd;

    move-result-object v7

    check-cast v7, LX/1sd;

    invoke-static/range {v0 .. v7}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->a(Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;LX/CIm;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/Jv4;LX/03V;LX/0Zb;LX/1sd;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2749794
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2749795
    const-string v1, "application_name"

    invoke-virtual {p0}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2749796
    const-string v1, "appmanager_version"

    iget-object v2, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->h:LX/1sd;

    invoke-virtual {v2}, LX/1sd;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2749797
    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2749798
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2749791
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->a(Landroid/os/Bundle;)V

    .line 2749792
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->requestWindowFeature(I)Z

    .line 2749793
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2749777
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2749783
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2749784
    invoke-static {p0, p0}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2749785
    invoke-virtual {p0}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->m:Landroid/preference/PreferenceScreen;

    .line 2749786
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->b:LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2749787
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->m:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2749788
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->c:LX/0TD;

    new-instance v1, LX/Jv5;

    invoke-direct {v1, p0}, LX/Jv5;-><init>(Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2749789
    new-instance v1, LX/Jv6;

    invoke-direct {v1, p0}, LX/Jv6;-><init>(Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;)V

    iget-object v2, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2749790
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x46f4d838

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2749778
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStart()V

    .line 2749779
    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->b:LX/CIm;

    invoke-virtual {v1, p0}, LX/CIm;->b(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2749780
    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->b:LX/CIm;

    const v2, 0x7f083aed

    invoke-virtual {v1, v2}, LX/CIm;->a(I)V

    .line 2749781
    const-string v1, "app_update_settings_active"

    invoke-direct {p0, v1}, Lcom/facebook/oxygen/preloads/integration/appupdates/fb4a/AppUpdateSettingsActivity;->a(Ljava/lang/String;)V

    .line 2749782
    const/16 v1, 0x23

    const v2, 0x38bd30cc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
