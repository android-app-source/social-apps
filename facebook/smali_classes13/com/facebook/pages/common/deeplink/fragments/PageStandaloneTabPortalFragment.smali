.class public Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->PAGE_DEEPLINK_TAB_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HMa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HBf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2749940
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2749941
    return-void
.end method

.method public static synthetic a(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/9Y7;)V
    .locals 8

    .prologue
    .line 2749937
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->d:LX/HMa;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {p3}, LX/8A4;->a(LX/0Px;)Z

    move-result v7

    move-object v4, p2

    move-object v5, p5

    move-object v6, p4

    invoke-virtual/range {v0 .. v7}, LX/HMa;->a(Landroid/app/Activity;JLjava/lang/String;LX/9Y7;Ljava/lang/String;Z)V

    .line 2749938
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2749939
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;LX/JvD;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2749933
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    invoke-virtual {p1}, LX/JvD;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/HBf;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749934
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081778

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2749935
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error when fetching tab from deeplinking portal fragment: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749936
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2749942
    if-eqz p2, :cond_0

    .line 2749943
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081779

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2749944
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2749945
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2749946
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2749947
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2749922
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2749923
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-static {p1}, LX/HMa;->b(LX/0QB;)LX/HMa;

    move-result-object v6

    check-cast v6, LX/HMa;

    invoke-static {p1}, LX/HBf;->a(LX/0QB;)LX/HBf;

    move-result-object v7

    check-cast v7, LX/HBf;

    invoke-static {p1}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    const/16 v9, 0x259

    invoke-static {p1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x12c4

    invoke-static {p1, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x455

    invoke-static {p1, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v0, 0xc49

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a:LX/0tX;

    iput-object v4, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->b:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->c:LX/9XE;

    iput-object v6, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->d:LX/HMa;

    iput-object v7, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    iput-object v8, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->f:LX/0rq;

    iput-object v9, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->g:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->h:LX/0Ot;

    iput-object v11, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->i:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->j:LX/0Ot;

    .line 2749924
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2749925
    const-string v1, "id_or_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->k:Ljava/lang/String;

    .line 2749926
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2749927
    const-string v1, "tab_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->l:Ljava/lang/String;

    .line 2749928
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    iget-object v1, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->l:Ljava/lang/String;

    .line 2749929
    iget-object v3, v0, LX/HBf;->a:LX/0if;

    sget-object v4, LX/0ig;->ap:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 2749930
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "token:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/HBf;->b(LX/HBf;Ljava/lang/String;)V

    .line 2749931
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tab:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/HBf;->b(LX/HBf;Ljava/lang/String;)V

    .line 2749932
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x37ec6774

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2749901
    const v1, 0x7f030e4f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0xa24596

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x10a213c8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2749916
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2749917
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2749918
    if-eqz v0, :cond_0

    .line 2749919
    const v2, 0x7f081777

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2749920
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2749921
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x6cd111e9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749902
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2749903
    new-instance v0, LX/JvE;

    invoke-direct {v0}, LX/JvE;-><init>()V

    move-object v0, v0

    .line 2749904
    new-instance v1, LX/4Hm;

    invoke-direct {v1}, LX/4Hm;-><init>()V

    .line 2749905
    iget-object v2, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->k:Ljava/lang/String;

    .line 2749906
    const-string v3, "page_id_or_token"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749907
    iget-object v2, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->l:Ljava/lang/String;

    .line 2749908
    const-string v3, "tab_token"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749909
    const-string v2, "input_data"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2749910
    const-string v1, "profile_image_size"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0077

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2749911
    iget-object v1, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2749912
    iget-object v1, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->e:LX/HBf;

    .line 2749913
    iget-object v2, v1, LX/HBf;->a:LX/0if;

    sget-object v3, LX/0ig;->ap:LX/0ih;

    const-string p1, "redirect_fetch_start"

    invoke-virtual {v2, v3, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2749914
    iget-object v1, p0, Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;->b:LX/1Ck;

    const-string v2, "fetch_deeplink_tab_query"

    new-instance v3, LX/JvC;

    invoke-direct {v3, p0}, LX/JvC;-><init>(Lcom/facebook/pages/common/deeplink/fragments/PageStandaloneTabPortalFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2749915
    return-void
.end method
