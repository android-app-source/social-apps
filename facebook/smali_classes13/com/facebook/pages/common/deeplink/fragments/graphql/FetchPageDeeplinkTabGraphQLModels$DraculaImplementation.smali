.class public final Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2750024
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2750025
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2750001
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2750002
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2750003
    if-nez p1, :cond_0

    .line 2750004
    :goto_0
    return v0

    .line 2750005
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2750006
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2750007
    :sswitch_0
    const-class v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 2750008
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2750009
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2750010
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2750011
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2750012
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2750013
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2750014
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2750015
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2750016
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x49c28d8d -> :sswitch_1
        -0x1475178e -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2750017
    new-instance v0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2749990
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2749991
    if-eqz v0, :cond_0

    .line 2749992
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2749993
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2750018
    sparse-switch p2, :sswitch_data_0

    .line 2750019
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2750020
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2750021
    invoke-static {v0, p3}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 2750022
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x49c28d8d -> :sswitch_1
        -0x1475178e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2750023
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2749994
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2749995
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2749996
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2749997
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2749998
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2749999
    iput p2, p0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->b:I

    .line 2750000
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2749964
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2749965
    new-instance v0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2749966
    iget v0, p0, LX/1vt;->c:I

    .line 2749967
    move v0, v0

    .line 2749968
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2749969
    iget v0, p0, LX/1vt;->c:I

    .line 2749970
    move v0, v0

    .line 2749971
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2749972
    iget v0, p0, LX/1vt;->b:I

    .line 2749973
    move v0, v0

    .line 2749974
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2749975
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2749976
    move-object v0, v0

    .line 2749977
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2749978
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2749979
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2749980
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2749981
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2749982
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2749983
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2749984
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2749985
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2749986
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2749987
    iget v0, p0, LX/1vt;->c:I

    .line 2749988
    move v0, v0

    .line 2749989
    return v0
.end method
