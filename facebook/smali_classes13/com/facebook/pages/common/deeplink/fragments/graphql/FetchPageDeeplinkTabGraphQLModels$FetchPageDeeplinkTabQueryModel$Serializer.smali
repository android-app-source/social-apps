.class public final Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2750135
    const-class v0, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;

    new-instance v1, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2750136
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2750151
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2750138
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2750139
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 2750140
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2750141
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2750142
    if-eqz v2, :cond_0

    .line 2750143
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2750144
    invoke-static {v1, v2, p1, p2}, LX/JvI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2750145
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2750146
    if-eqz v2, :cond_1

    .line 2750147
    const-string v2, "tab_action_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2750148
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2750149
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2750150
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2750137
    check-cast p1, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel$Serializer;->a(Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
