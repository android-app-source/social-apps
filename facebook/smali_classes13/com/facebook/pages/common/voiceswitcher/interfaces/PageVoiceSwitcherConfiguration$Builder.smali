.class public final Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/2rw;


# instance fields
.field public b:LX/2rw;

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2750438
    const-class v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2750439
    new-instance v0, LX/JvY;

    invoke-direct {v0}, LX/JvY;-><init>()V

    .line 2750440
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    move-object v0, v0

    .line 2750441
    sput-object v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->a:LX/2rw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2750442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750443
    sget-object v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->a:LX/2rw;

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->b:LX/2rw;

    .line 2750444
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->d:Ljava/lang/String;

    .line 2750445
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;
    .locals 2

    .prologue
    .line 2750446
    new-instance v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;-><init>(Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;)V

    return-object v0
.end method

.method public setComposerTargetType(LX/2rw;)Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_target_type"
    .end annotation

    .prologue
    .line 2750447
    iput-object p1, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->b:LX/2rw;

    .line 2750448
    return-object p0
.end method

.method public setInitialSelectedId(Ljava/lang/String;)Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_selected_id"
    .end annotation

    .prologue
    .line 2750449
    iput-object p1, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->c:Ljava/lang/String;

    .line 2750450
    return-object p0
.end method

.method public setTargetId(Ljava/lang/String;)Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_id"
    .end annotation

    .prologue
    .line 2750451
    iput-object p1, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->d:Ljava/lang/String;

    .line 2750452
    return-object p0
.end method
