.class public Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2750510
    const-class v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    new-instance v1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2750511
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2750498
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2750504
    if-nez p0, :cond_0

    .line 2750505
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2750506
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2750507
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;->b(Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;LX/0nX;LX/0my;)V

    .line 2750508
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2750509
    return-void
.end method

.method private static b(Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2750500
    const-string v0, "composer_target_type"

    invoke-virtual {p0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->getComposerTargetType()LX/2rw;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2750501
    const-string v0, "initial_selected_id"

    invoke-virtual {p0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->getInitialSelectedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2750502
    const-string v0, "target_id"

    invoke-virtual {p0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->getTargetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2750503
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2750499
    check-cast p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;->a(Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
