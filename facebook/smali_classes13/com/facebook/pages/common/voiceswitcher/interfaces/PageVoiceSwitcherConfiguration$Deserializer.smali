.class public final Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2750456
    new-instance v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Deserializer;->a:Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2750457
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 2750458
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;
    .locals 1

    .prologue
    .line 2750454
    sget-object v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Deserializer;->a:Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;

    .line 2750455
    invoke-virtual {v0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->a()Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2750453
    invoke-static {p1, p2}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    move-result-object v0

    return-object v0
.end method
