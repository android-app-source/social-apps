.class public Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/2rw;

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2750497
    const-class v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2750496
    const-class v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2750495
    new-instance v0, LX/JvX;

    invoke-direct {v0}, LX/JvX;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2750488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750489
    invoke-static {}, LX/2rw;->values()[LX/2rw;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    .line 2750490
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 2750491
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    .line 2750492
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    .line 2750493
    return-void

    .line 2750494
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;)V
    .locals 1

    .prologue
    .line 2750483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2750484
    iget-object v0, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->b:LX/2rw;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rw;

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    .line 2750485
    iget-object v0, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    .line 2750486
    iget-object v0, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    .line 2750487
    return-void
.end method

.method public static newBuilder()Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;
    .locals 2

    .prologue
    .line 2750482
    new-instance v0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;

    invoke-direct {v0}, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2750481
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2750459
    if-ne p0, p1, :cond_1

    .line 2750460
    :cond_0
    :goto_0
    return v0

    .line 2750461
    :cond_1
    instance-of v2, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 2750462
    goto :goto_0

    .line 2750463
    :cond_2
    check-cast p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;

    .line 2750464
    iget-object v2, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    iget-object v3, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2750465
    goto :goto_0

    .line 2750466
    :cond_3
    iget-object v2, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2750467
    goto :goto_0

    .line 2750468
    :cond_4
    iget-object v2, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2750469
    goto :goto_0
.end method

.method public getComposerTargetType()LX/2rw;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_target_type"
    .end annotation

    .prologue
    .line 2750480
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    return-object v0
.end method

.method public getInitialSelectedId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_selected_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2750479
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_id"
    .end annotation

    .prologue
    .line 2750478
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2750477
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2750470
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->a:LX/2rw;

    invoke-virtual {v0}, LX/2rw;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2750471
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2750472
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2750473
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2750474
    return-void

    .line 2750475
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2750476
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/interfaces/PageVoiceSwitcherConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
