.class public Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->PAGES_VOICE_SWITCHER_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/JvR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/JvM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3iH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/JvQ;

.field private h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public i:LX/JvL;

.field public j:J

.field private final k:LX/JvO;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2750327
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2750328
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    .line 2750329
    new-instance v0, LX/JvO;

    invoke-direct {v0, p0}, LX/JvO;-><init>(Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->k:LX/JvO;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2750349
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2750350
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    const-class v3, LX/JvR;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/JvR;

    const-class v4, LX/JvM;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JvM;

    invoke-static {p1}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v5

    check-cast v5, LX/3iH;

    invoke-static {p1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v6

    check-cast v6, LX/0if;

    const/16 v7, 0x1430

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v0, 0x12c4

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->a:LX/JvR;

    iput-object v4, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->b:LX/JvM;

    iput-object v5, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->c:LX/3iH;

    iput-object v6, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    iput-object v7, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->e:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->f:LX/0Ot;

    .line 2750351
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->hashCode()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    .line 2750352
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v1, LX/0ig;->aq:LX/0ih;

    iget-wide v2, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;J)V

    .line 2750353
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->a:LX/JvR;

    iget-wide v2, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2750354
    new-instance v4, LX/JvQ;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    .line 2750355
    new-instance v7, LX/JvU;

    const/16 v8, 0x2c10

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x249b

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct {v7, v8, v9}, LX/JvU;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2750356
    move-object v7, v7

    .line 2750357
    check-cast v7, LX/JvU;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v8

    check-cast v8, LX/0if;

    move-object v9, p0

    move-object v10, v1

    invoke-direct/range {v4 .. v10}, LX/JvQ;-><init>(LX/1Ck;LX/0tX;LX/JvU;LX/0if;Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;Ljava/lang/Long;)V

    .line 2750358
    move-object v0, v4

    .line 2750359
    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->g:LX/JvQ;

    .line 2750360
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->g:LX/JvQ;

    .line 2750361
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2750362
    iput-object v1, v0, LX/JvQ;->g:Landroid/os/Bundle;

    .line 2750363
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->g:LX/JvQ;

    .line 2750364
    iget-object v4, v0, LX/JvQ;->g:Landroid/os/Bundle;

    const-string v5, "target_type_extra"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, LX/2rw;

    .line 2750365
    iget-object v6, v0, LX/JvQ;->d:LX/0if;

    sget-object v7, LX/0ig;->aq:LX/0ih;

    iget-wide v8, v0, LX/JvQ;->f:J

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v5, "target_type:"

    invoke-direct {v10, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v8, v9, v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 2750366
    if-eqz v4, :cond_1

    const/4 v5, 0x1

    :goto_1
    const-string v6, "Target type is invalid in voice switcher controller!"

    invoke-static {v5, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2750367
    iget-object v5, v0, LX/JvQ;->c:LX/JvU;

    .line 2750368
    sget-object v6, LX/JvT;->a:[I

    invoke-virtual {v4}, LX/2rw;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 2750369
    iget-object v6, v5, LX/JvU;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Jb3;

    :goto_2
    move-object v5, v6

    .line 2750370
    iget-object v6, v0, LX/JvQ;->g:Landroid/os/Bundle;

    invoke-interface {v5, v6}, LX/Jb3;->a(Landroid/os/Bundle;)LX/0zO;

    move-result-object v6

    .line 2750371
    if-nez v6, :cond_2

    .line 2750372
    iget-object v4, v0, LX/JvQ;->d:LX/0if;

    sget-object v5, LX/0ig;->aq:LX/0ih;

    iget-wide v6, v0, LX/JvQ;->f:J

    const-string v8, "null_request"

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 2750373
    :goto_3
    return-void

    .line 2750374
    :cond_0
    const-string v5, "null"

    goto :goto_0

    .line 2750375
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 2750376
    :cond_2
    iget-object v7, v0, LX/JvQ;->d:LX/0if;

    sget-object v8, LX/0ig;->aq:LX/0ih;

    iget-wide v10, v0, LX/JvQ;->f:J

    const-string v9, "fetch_start"

    invoke-virtual {v7, v8, v10, v11, v9}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750377
    iget-object v7, v0, LX/JvQ;->a:LX/1Ck;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "initial_fetch_"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/2rw;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v8, v0, LX/JvQ;->b:LX/0tX;

    invoke-virtual {v8, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    new-instance v8, LX/JvP;

    invoke-direct {v8, v0, v5}, LX/JvP;-><init>(LX/JvQ;LX/Jb3;)V

    invoke-virtual {v7, v4, v6, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_3

    .line 2750378
    :pswitch_0
    iget-object v6, v5, LX/JvU;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Jb3;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x57c593a1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750348
    const v1, 0x7f030eb2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5e74f0ad

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x30f1dcb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2750379
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2750380
    iget-object v1, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v2, LX/0ig;->aq:LX/0ih;

    iget-wide v4, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    invoke-virtual {v1, v2, v4, v5}, LX/0if;->c(LX/0ih;J)V

    .line 2750381
    const/16 v1, 0x2b

    const v2, -0x5374e640

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4331e1ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2750343
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2750344
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2750345
    if-eqz v0, :cond_0

    .line 2750346
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2750347
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x3e56fa59

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2750330
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2750331
    const v0, 0x7f0d23e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2750332
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->b:LX/JvM;

    iget-object v1, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->k:LX/JvO;

    .line 2750333
    new-instance v2, LX/JvL;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v3, v1}, LX/JvL;-><init>(LX/0Or;LX/JvO;)V

    .line 2750334
    move-object v0, v2

    .line 2750335
    iput-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->i:LX/JvL;

    .line 2750336
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->i:LX/JvL;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2750337
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2750338
    const v0, 0x7f0d23e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 2750339
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2750340
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2750341
    iget-object v0, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->d:LX/0if;

    sget-object v1, LX/0ig;->aq:LX/0ih;

    iget-wide v2, p0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->j:J

    const-string v4, "view_created"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750342
    return-void
.end method
