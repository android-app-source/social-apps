.class public Lcom/facebook/wearlistener/DataLayerListenerService;
.super LX/JtC;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/KA8;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/KAA;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/wearlistener/WearNodeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2778926
    const-class v0, Lcom/facebook/wearlistener/DataLayerListenerService;

    sput-object v0, Lcom/facebook/wearlistener/DataLayerListenerService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2778927
    invoke-direct {p0}, LX/JtC;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Iterable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2778928
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 2778929
    :try_start_0
    invoke-static {p0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, -0x55b0e9a5

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2778930
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2778931
    :goto_0
    return-void

    .line 2778932
    :catch_0
    move-exception v0

    .line 2778933
    :try_start_1
    sget-object v1, Lcom/facebook/wearlistener/DataLayerListenerService;->a:Ljava/lang/Class;

    const-string v4, "Operation failed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2778934
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 2778935
    :catch_1
    move-exception v0

    .line 2778936
    :try_start_2
    sget-object v1, Lcom/facebook/wearlistener/DataLayerListenerService;->a:Ljava/lang/Class;

    const-string v4, "Operation interrupted"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2778937
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/wearlistener/DataLayerListenerService;

    invoke-static {v0}, LX/KA5;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, LX/KA6;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v2

    invoke-static {v0}, LX/KA7;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/KA8;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/KAA;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/wearlistener/WearNodeListener;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2778938
    iput-object p1, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->b:Ljava/util/Set;

    .line 2778939
    iput-object p2, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->c:Ljava/util/Set;

    .line 2778940
    iput-object p3, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    .line 2778941
    return-void
.end method


# virtual methods
.method public final a(LX/KAn;)V
    .locals 2

    .prologue
    .line 2778942
    invoke-static {p0, p0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2778943
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    invoke-virtual {p1}, LX/4sY;->b()I

    .line 2778944
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->b:Ljava/util/Set;

    new-instance v1, LX/KA1;

    invoke-direct {v1, p0, p1}, LX/KA1;-><init>(Lcom/facebook/wearlistener/DataLayerListenerService;LX/KAn;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Iterable;)V

    .line 2778945
    return-void
.end method

.method public final a(LX/KAu;)V
    .locals 2

    .prologue
    .line 2778946
    invoke-static {p0, p0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2778947
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    .line 2778948
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->c:Ljava/util/Set;

    new-instance v1, LX/KA2;

    invoke-direct {v1, p0, p1}, LX/KA2;-><init>(Lcom/facebook/wearlistener/DataLayerListenerService;LX/KAu;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Iterable;)V

    .line 2778949
    return-void
.end method

.method public final a(LX/KAv;)V
    .locals 2

    .prologue
    .line 2778950
    invoke-static {p0, p0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2778951
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    .line 2778952
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    new-instance v1, LX/KA3;

    invoke-direct {v1, p0, p1}, LX/KA3;-><init>(Lcom/facebook/wearlistener/DataLayerListenerService;LX/KAv;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Iterable;)V

    .line 2778953
    return-void
.end method

.method public final b(LX/KAv;)V
    .locals 2

    .prologue
    .line 2778954
    invoke-static {p0, p0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2778955
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    .line 2778956
    iget-object v0, p0, Lcom/facebook/wearlistener/DataLayerListenerService;->d:Ljava/util/Set;

    new-instance v1, LX/KA4;

    invoke-direct {v1, p0, p1}, LX/KA4;-><init>(Lcom/facebook/wearlistener/DataLayerListenerService;LX/KAv;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/wearlistener/DataLayerListenerService;->a(Ljava/lang/Iterable;)V

    .line 2778957
    return-void
.end method
