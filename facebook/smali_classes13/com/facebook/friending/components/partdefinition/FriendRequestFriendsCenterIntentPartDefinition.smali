.class public Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JZh;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/17W;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2708902
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2708903
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2708904
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->b:LX/17W;

    .line 2708905
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;
    .locals 5

    .prologue
    .line 2708906
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    monitor-enter v1

    .line 2708907
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708908
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708909
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708910
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708911
    new-instance p0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-direct {p0, v3, v4}, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V

    .line 2708912
    move-object v0, p0

    .line 2708913
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708914
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708915
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2708917
    check-cast p2, LX/JZh;

    check-cast p3, LX/1Pq;

    .line 2708918
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2708919
    new-instance v1, LX/JZg;

    invoke-direct {v1, p0, p2, p3}, LX/JZg;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;LX/JZh;LX/1Pq;)V

    move-object v1, v1

    .line 2708920
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2708921
    const/4 v0, 0x0

    return-object v0
.end method
