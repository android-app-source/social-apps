.class public Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/JZr",
        "<TE;>;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2do;

.field private final g:LX/0xW;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2do;LX/0xW;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;",
            ">;",
            "LX/2do;",
            "LX/0xW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2709134
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2709135
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->a:LX/0Ot;

    .line 2709136
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->b:LX/0Ot;

    .line 2709137
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->c:LX/0Ot;

    .line 2709138
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->d:LX/0Ot;

    .line 2709139
    iput-object p5, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->e:LX/0Ot;

    .line 2709140
    iput-object p6, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->f:LX/2do;

    .line 2709141
    iput-object p7, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->g:LX/0xW;

    .line 2709142
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;
    .locals 11

    .prologue
    .line 2709143
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;

    monitor-enter v1

    .line 2709144
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709145
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709146
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709147
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709148
    new-instance v3, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;

    const/16 v4, 0xa6a

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xa6b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa6c

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x224e

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xa6d

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v9

    check-cast v9, LX/2do;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v10

    check-cast v10, LX/0xW;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2do;LX/0xW;)V

    .line 2709149
    move-object v0, v3

    .line 2709150
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709151
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709152
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Z
    .locals 2

    .prologue
    .line 2709154
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne p0, v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->PENDING:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2709155
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne p0, v2, :cond_2

    .line 2709156
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v2, v3, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 2709157
    :cond_1
    :goto_0
    return v0

    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eq p0, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne p0, v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2709158
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    const/4 v6, 0x0

    .line 2709159
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709160
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    .line 2709161
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2709162
    invoke-interface {v1}, LX/9uc;->cs()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v1

    .line 2709163
    sget-object v2, LX/JZn;->a:[I

    .line 2709164
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2709165
    invoke-interface {v3}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2709166
    :goto_0
    new-instance v1, LX/JZr;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v2, v3, v4}, LX/JZr;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    .line 2709167
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->f:LX/2do;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2709168
    return-object v1

    .line 2709169
    :pswitch_0
    invoke-static {v1, v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Z

    move-result v2

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->a:LX/0Ot;

    invoke-static {p1, v2, v3, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->g:LX/0xW;

    invoke-virtual {v3}, LX/0xW;->v()Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->d:LX/0Ot;

    invoke-virtual {v2, v3, v4, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    invoke-static {v1, v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->b(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->b:LX/0Ot;

    new-instance v5, LX/JZf;

    invoke-direct {v5, p2, v1, v6}, LX/JZf;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;I)V

    invoke-virtual {v2, v3, v4, v5}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_0

    .line 2709170
    :pswitch_1
    invoke-static {v1, v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Z

    move-result v2

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->c:LX/0Ot;

    invoke-static {p1, v2, v3, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    invoke-static {v1, v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->b(Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Z

    move-result v1

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;->e:LX/0Ot;

    new-instance v4, LX/JZj;

    invoke-direct {v4, p2, v6}, LX/JZj;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;I)V

    invoke-virtual {v2, v1, v3, v4}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2709171
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709172
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 2709173
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2709174
    invoke-interface {v1}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v1

    .line 2709175
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
