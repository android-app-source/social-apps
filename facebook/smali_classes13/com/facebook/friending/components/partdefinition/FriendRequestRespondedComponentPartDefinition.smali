.class public Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708932
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2708933
    return-void
.end method

.method private static a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2708934
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2708935
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    .line 2708936
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2708937
    check-cast p2, LX/1Pr;

    new-instance v1, LX/JZo;

    invoke-direct {v1, v0}, LX/JZo;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v1, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JZp;

    .line 2708938
    iget-object v1, v0, LX/JZp;->b:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-object v1, v1

    .line 2708939
    iget-boolean p1, v0, LX/JZp;->a:Z

    move v0, p1

    .line 2708940
    if-nez v0, :cond_2

    .line 2708941
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2708942
    :goto_0
    const/4 v1, 0x0

    .line 2708943
    new-instance v2, LX/Ja5;

    invoke-direct {v2}, LX/Ja5;-><init>()V

    .line 2708944
    sget-object p1, LX/Ja6;->b:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/Ja4;

    .line 2708945
    if-nez p1, :cond_0

    .line 2708946
    new-instance p1, LX/Ja4;

    invoke-direct {p1}, LX/Ja4;-><init>()V

    .line 2708947
    :cond_0
    invoke-static {p1, p0, v1, v1, v2}, LX/Ja4;->a$redex0(LX/Ja4;LX/1De;IILX/Ja5;)V

    .line 2708948
    move-object v2, p1

    .line 2708949
    move-object v1, v2

    .line 2708950
    move-object v1, v1

    .line 2708951
    iget-object v2, v1, LX/Ja4;->a:LX/Ja5;

    iput-object v0, v2, LX/Ja5;->a:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2708952
    iget-object v2, v1, LX/Ja4;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2708953
    move-object v0, v1

    .line 2708954
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2708955
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;
    .locals 4

    .prologue
    .line 2708956
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;

    monitor-enter v1

    .line 2708957
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708958
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708959
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708960
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708961
    new-instance p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2708962
    move-object v0, p0

    .line 2708963
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708964
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708965
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2708967
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-static {p1, p2, p3}, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2708968
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1, p2, p3}, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2708969
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2708970
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2708971
    if-eqz v0, :cond_0

    .line 2708972
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2708973
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2708974
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2708975
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2708976
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2708977
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2708978
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2708979
    return-object p1
.end method
