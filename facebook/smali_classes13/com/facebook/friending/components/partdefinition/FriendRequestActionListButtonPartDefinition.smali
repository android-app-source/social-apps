.class public Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JZe;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/resources/ui/FbButton;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/3UJ;

.field public final d:Landroid/content/res/Resources;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/3UJ;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2708852
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2708853
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->a:LX/23P;

    .line 2708854
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2708855
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->c:LX/3UJ;

    .line 2708856
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->d:Landroid/content/res/Resources;

    .line 2708857
    iput-object p5, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2708858
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;
    .locals 9

    .prologue
    .line 2708859
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    monitor-enter v1

    .line 2708860
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708861
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708862
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708863
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708864
    new-instance v3, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/3UJ;->b(LX/0QB;)LX/3UJ;

    move-result-object v6

    check-cast v6, LX/3UJ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;-><init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/3UJ;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2708865
    move-object v0, v3

    .line 2708866
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708867
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708868
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708869
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/1Pq;LX/JZe;Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pq;",
            ":",
            "LX/2kk;",
            ">(TE;",
            "LX/JZe;",
            "Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2708870
    move-object v0, p0

    check-cast v0, LX/2kk;

    iget-object v2, p1, LX/JZe;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v1, p1, LX/JZe;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2708871
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v3

    .line 2708872
    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v1

    invoke-static {v1}, LX/9vz;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/9vz;

    move-result-object v1

    .line 2708873
    iput-object p2, v1, LX/9vz;->co:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2708874
    move-object v1, v1

    .line 2708875
    invoke-virtual {v1}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 2708876
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    iget-object v2, p1, LX/JZe;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p0, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2708877
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2708878
    check-cast p2, LX/JZe;

    check-cast p3, LX/1Pq;

    .line 2708879
    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v0, p2, LX/JZe;->d:LX/2na;

    sget-object v2, LX/2na;->CONFIRM:LX/2na;

    if-ne v0, v2, :cond_0

    const v0, 0x7f080f83

    .line 2708880
    :goto_0
    iget-object v2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->a:LX/23P;

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->d:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v0, v2

    .line 2708881
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2708882
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2708883
    new-instance v1, LX/JZd;

    invoke-direct {v1, p0, p2, p3}, LX/JZd;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;LX/JZe;LX/1Pq;)V

    move-object v1, v1

    .line 2708884
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2708885
    const/4 v0, 0x0

    return-object v0

    .line 2708886
    :cond_0
    const v0, 0x7f080f81

    goto :goto_0
.end method
