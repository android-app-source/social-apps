.class public Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JZu;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ja1;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2do;

.field private final g:LX/1rj;

.field private final h:LX/0SI;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/2do;LX/1ri;LX/0SI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/JZu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ja1;",
            ">;",
            "LX/2do;",
            "LX/1ri;",
            "LX/0SI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709042
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2709043
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->d:LX/0Ot;

    .line 2709044
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->e:LX/0Ot;

    .line 2709045
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->f:LX/2do;

    .line 2709046
    iget-object v0, p5, LX/1ri;->a:LX/1rj;

    move-object v0, v0

    .line 2709047
    iput-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->g:LX/1rj;

    .line 2709048
    iput-object p6, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->h:LX/0SI;

    .line 2709049
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2709050
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709051
    invoke-interface {v0}, LX/9uc;->cs()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v1

    .line 2709052
    invoke-static {p2}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p3

    .line 2709053
    check-cast v0, LX/1Pr;

    new-instance v3, LX/JZo;

    invoke-direct {v3, v2}, LX/JZo;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JZp;

    .line 2709054
    iget-boolean v3, v0, LX/JZp;->a:Z

    move v3, v3

    .line 2709055
    if-eqz v3, :cond_0

    .line 2709056
    iget-object v1, v0, LX/JZp;->b:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-object v1, v1

    .line 2709057
    :cond_0
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2709058
    new-instance v5, LX/JZk;

    invoke-direct {v5, p0, v3, v4}, LX/JZk;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    move-object v3, v5

    .line 2709059
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2709060
    new-instance v5, LX/JZl;

    invoke-direct {v5, p0, v4}, LX/JZl;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;Ljava/lang/ref/WeakReference;)V

    move-object v4, v5

    .line 2709061
    iput-object v3, v0, LX/JZp;->c:LX/2Ip;

    .line 2709062
    iput-object v4, v0, LX/JZp;->d:LX/2hK;

    .line 2709063
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->f:LX/2do;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2709064
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->f:LX/2do;

    invoke-virtual {v0, v4}, LX/0b4;->a(LX/0b2;)Z

    .line 2709065
    sget-object v0, LX/JZm;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2709066
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2709067
    :pswitch_0
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v0

    .line 2709068
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ja1;

    const/4 v3, 0x0

    .line 2709069
    new-instance v4, LX/Ja0;

    invoke-direct {v4, v0}, LX/Ja0;-><init>(LX/Ja1;)V

    .line 2709070
    iget-object p0, v0, LX/Ja1;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JZz;

    .line 2709071
    if-nez p0, :cond_1

    .line 2709072
    new-instance p0, LX/JZz;

    invoke-direct {p0, v0}, LX/JZz;-><init>(LX/Ja1;)V

    .line 2709073
    :cond_1
    invoke-static {p0, p1, v3, v3, v4}, LX/JZz;->a$redex0(LX/JZz;LX/1De;IILX/Ja0;)V

    .line 2709074
    move-object v4, p0

    .line 2709075
    move-object v3, v4

    .line 2709076
    move-object v0, v3

    .line 2709077
    iget-object v3, v0, LX/JZz;->a:LX/Ja0;

    iput-object p2, v3, LX/Ja0;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709078
    iget-object v3, v0, LX/JZz;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2709079
    move-object v0, v0

    .line 2709080
    check-cast p3, LX/1Pr;

    .line 2709081
    iget-object v3, v0, LX/JZz;->a:LX/Ja0;

    iput-object p3, v3, LX/Ja0;->d:LX/1Pr;

    .line 2709082
    iget-object v3, v0, LX/JZz;->e:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2709083
    move-object v0, v0

    .line 2709084
    iget-object v3, v0, LX/JZz;->a:LX/Ja0;

    iput-object v2, v3, LX/Ja0;->e:Ljava/lang/String;

    .line 2709085
    iget-object v3, v0, LX/JZz;->e:Ljava/util/BitSet;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2709086
    move-object v0, v0

    .line 2709087
    invoke-interface {v1}, LX/9uc;->q()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 2709088
    iget-object v4, v0, LX/JZz;->a:LX/Ja0;

    iput-object v3, v4, LX/Ja0;->a:Ljava/lang/String;

    .line 2709089
    iget-object v4, v0, LX/JZz;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2709090
    move-object v0, v0

    .line 2709091
    invoke-interface {v1}, LX/9uc;->bH()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 2709092
    iget-object v3, v0, LX/JZz;->a:LX/Ja0;

    iput-object v1, v3, LX/Ja0;->b:Ljava/lang/String;

    .line 2709093
    iget-object v3, v0, LX/JZz;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2709094
    move-object v0, v0

    .line 2709095
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    move-object v0, v0

    .line 2709096
    goto :goto_0

    .line 2709097
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JZu;

    const/4 v1, 0x0

    .line 2709098
    new-instance v2, LX/JZt;

    invoke-direct {v2, v0}, LX/JZt;-><init>(LX/JZu;)V

    .line 2709099
    sget-object p0, LX/JZu;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JZs;

    .line 2709100
    if-nez p0, :cond_2

    .line 2709101
    new-instance p0, LX/JZs;

    invoke-direct {p0}, LX/JZs;-><init>()V

    .line 2709102
    :cond_2
    invoke-static {p0, p1, v1, v1, v2}, LX/JZs;->a$redex0(LX/JZs;LX/1De;IILX/JZt;)V

    .line 2709103
    move-object v2, p0

    .line 2709104
    move-object v1, v2

    .line 2709105
    move-object v0, v1

    .line 2709106
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2709107
    invoke-interface {v1}, LX/9uc;->r()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 2709108
    iget-object v2, v0, LX/JZs;->a:LX/JZt;

    iput-object v1, v2, LX/JZt;->a:Ljava/lang/String;

    .line 2709109
    iget-object v2, v0, LX/JZs;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2709110
    move-object v0, v0

    .line 2709111
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2709112
    invoke-interface {v1}, LX/9uc;->p()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    .line 2709113
    iget-object v2, v0, LX/JZs;->a:LX/JZt;

    iput-object v1, v2, LX/JZt;->b:Ljava/lang/String;

    .line 2709114
    move-object v0, v0

    .line 2709115
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    move-object v0, v0

    .line 2709116
    goto/16 :goto_0

    .line 2709117
    :pswitch_2
    const/4 v0, 0x0

    .line 2709118
    new-instance v1, LX/JZw;

    invoke-direct {v1}, LX/JZw;-><init>()V

    .line 2709119
    sget-object v2, LX/JZx;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JZv;

    .line 2709120
    if-nez v2, :cond_3

    .line 2709121
    new-instance v2, LX/JZv;

    invoke-direct {v2}, LX/JZv;-><init>()V

    .line 2709122
    :cond_3
    invoke-static {v2, p1, v0, v0, v1}, LX/JZv;->a$redex0(LX/JZv;LX/1De;IILX/JZw;)V

    .line 2709123
    move-object v1, v2

    .line 2709124
    move-object v0, v1

    .line 2709125
    move-object v0, v0

    .line 2709126
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2709127
    invoke-interface {v1}, LX/9uc;->bI()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 2709128
    iget-object v2, v0, LX/JZv;->a:LX/JZw;

    iput-object v1, v2, LX/JZw;->a:Ljava/lang/String;

    .line 2709129
    iget-object v2, v0, LX/JZv;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Ljava/util/BitSet;->set(I)V

    .line 2709130
    move-object v0, v0

    .line 2709131
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    move-object v0, v0

    .line 2709132
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;
    .locals 10

    .prologue
    .line 2709018
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;

    monitor-enter v1

    .line 2709019
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709020
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709021
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709022
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709023
    new-instance v3, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x2252

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2254

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v7

    check-cast v7, LX/2do;

    invoke-static {v0}, LX/1ri;->b(LX/0QB;)LX/1ri;

    move-result-object v8

    check-cast v8, LX/1ri;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v9

    check-cast v9, LX/0SI;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/2do;LX/1ri;LX/0SI;)V

    .line 2709024
    move-object v0, v3

    .line 2709025
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709026
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709027
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709028
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2709029
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709030
    if-eqz v0, :cond_0

    .line 2709031
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709032
    invoke-interface {v0}, LX/9uc;->cu()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2709033
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709034
    invoke-interface {v0}, LX/9uc;->cu()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2709035
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709036
    invoke-interface {v0}, LX/9uc;->cu()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2709037
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 6

    .prologue
    .line 2709038
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 2709039
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->h:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2709040
    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->g:LX/1rj;

    iget-object v2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->h:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    sget-object v3, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2709041
    :cond_0
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2709010
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2709009
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2709011
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709012
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2709013
    invoke-interface {v0}, LX/9uc;->cs()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v1

    .line 2709014
    if-eqz v1, :cond_2

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->PENDING:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eq p0, v1, :cond_0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eq p0, v1, :cond_0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne p0, v1, :cond_2

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move v1, p0

    .line 2709015
    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/9uc;->q()LX/174;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/9uc;->q()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/9uc;->r()LX/174;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/9uc;->r()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/9uc;->bH()LX/174;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/9uc;->bH()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/9uc;->bI()LX/174;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/9uc;->bI()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2709016
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709017
    return-object p1
.end method
