.class public Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/resources/ui/FbButton;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field private final b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

.field public final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(LX/23P;Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2709193
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2709194
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->a:LX/23P;

    .line 2709195
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    .line 2709196
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->c:Landroid/content/res/Resources;

    .line 2709197
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2709198
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;
    .locals 7

    .prologue
    .line 2709176
    const-class v1, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    monitor-enter v1

    .line 2709177
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709178
    sput-object v2, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709179
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709180
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709181
    new-instance p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;-><init>(LX/23P;Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2709182
    move-object v0, p0

    .line 2709183
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709184
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709185
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2709187
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2709188
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    const v1, 0x7f080fb5

    .line 2709189
    iget-object v2, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->a:LX/23P;

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->c:Landroid/content/res/Resources;

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 p3, 0x0

    invoke-virtual {v2, v3, p3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v1, v2

    .line 2709190
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2709191
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    new-instance v1, LX/JZh;

    sget-object v2, LX/5P0;->REQUESTS:LX/5P0;

    sget-object v3, LX/Cfc;->VIEW_FRIEND_REQUESTS_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v3}, LX/JZh;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/5P0;LX/Cfc;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2709192
    const/4 v0, 0x0

    return-object v0
.end method
