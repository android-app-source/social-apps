.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

.field private final b:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704280
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2704281
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

    .line 2704282
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    .line 2704283
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;
    .locals 5

    .prologue
    .line 2704284
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

    monitor-enter v1

    .line 2704285
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704286
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704287
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704288
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704289
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;)V

    .line 2704290
    move-object v0, p0

    .line 2704291
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704292
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704293
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2704295
    check-cast p2, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704296
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2704297
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2704298
    const/4 v0, 0x1

    return v0
.end method
