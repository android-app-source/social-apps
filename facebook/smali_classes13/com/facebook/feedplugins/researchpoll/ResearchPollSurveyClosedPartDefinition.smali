.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "LX/JXP;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

.field public final b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704184
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2704185
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    .line 2704186
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    .line 2704187
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;
    .locals 5

    .prologue
    .line 2704188
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    monitor-enter v1

    .line 2704189
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704190
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704191
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704192
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704193
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;)V

    .line 2704194
    move-object v0, p0

    .line 2704195
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704196
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704197
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704198
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2704199
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2704200
    check-cast p2, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    check-cast p3, LX/1Pq;

    .line 2704201
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704202
    new-instance v0, LX/JXE;

    invoke-direct {v0, p0, p2, p3}, LX/JXE;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyClosedPartDefinition;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Pq;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4a00045d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704203
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/JXP;

    .line 2704204
    invoke-virtual {p4, p2}, LX/JXP;->setCallToActionViewListener(Landroid/view/View$OnClickListener;)V

    .line 2704205
    const/16 v1, 0x1f

    const v2, 0x5f027e92

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2704206
    check-cast p1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704207
    const/4 v0, 0x1

    move v0, v0

    .line 2704208
    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2704209
    check-cast p4, LX/JXP;

    .line 2704210
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/JXP;->setCallToActionViewListener(Landroid/view/View$OnClickListener;)V

    .line 2704211
    return-void
.end method
