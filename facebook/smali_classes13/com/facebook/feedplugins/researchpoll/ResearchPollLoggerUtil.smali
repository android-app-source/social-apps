.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;


# instance fields
.field public final a:LX/0aG;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;

.field public final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0aG;LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2704091
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a:LX/0aG;

    .line 2704092
    const-class v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2704093
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->c:LX/0Zb;

    .line 2704094
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;
    .locals 5

    .prologue
    .line 2704095
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    if-nez v0, :cond_1

    .line 2704096
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    monitor-enter v1

    .line 2704097
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2704098
    if-eqz v2, :cond_0

    .line 2704099
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2704100
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;-><init>(LX/0aG;LX/0Zb;)V

    .line 2704101
    move-object v0, p0

    .line 2704102
    sput-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704103
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2704104
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2704105
    :cond_1
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    return-object v0

    .line 2704106
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2704107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2704108
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "research_poll_interaction"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "interaction_type"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "survey_id"

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2704109
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2704110
    return-void
.end method
