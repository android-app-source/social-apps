.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JXP;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704235
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2704236
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    .line 2704237
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;
    .locals 4

    .prologue
    .line 2704223
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

    monitor-enter v1

    .line 2704224
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704225
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704228
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;)V

    .line 2704229
    move-object v0, p0

    .line 2704230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2704234
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2704212
    check-cast p2, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    check-cast p3, LX/1Pq;

    const/4 v2, 0x0

    .line 2704213
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyOpenPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704214
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704215
    :goto_0
    return-object v2

    .line 2704216
    :cond_0
    check-cast p3, LX/1Pr;

    new-instance v0, LX/JXD;

    invoke-direct {v0, p2}, LX/JXD;-><init>(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JXB;

    .line 2704217
    const/4 v1, 0x1

    .line 2704218
    iput-boolean v1, v0, LX/JXB;->g:Z

    .line 2704219
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2704220
    check-cast p1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704221
    const/4 v0, 0x1

    move v0, v0

    .line 2704222
    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
