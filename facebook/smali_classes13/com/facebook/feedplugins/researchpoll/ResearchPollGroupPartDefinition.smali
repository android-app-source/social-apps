.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

.field private final b:Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

.field private final c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703975
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2703976
    iput-object p4, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    .line 2703977
    iput-object p3, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

    .line 2703978
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

    .line 2703979
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    .line 2703980
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;
    .locals 7

    .prologue
    .line 2703981
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;

    monitor-enter v1

    .line 2703982
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703983
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703984
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703985
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703986
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;)V

    .line 2703987
    move-object v0, p0

    .line 2703988
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2703992
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703993
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2703994
    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2703995
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->d:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2703996
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2703997
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->a:Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2703998
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;->c:Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveySelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2703999
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2704000
    const/4 v0, 0x1

    return v0
.end method
