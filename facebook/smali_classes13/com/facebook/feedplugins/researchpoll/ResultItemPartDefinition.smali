.class public Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/JXM;",
        "LX/JXN;",
        "LX/1PW;",
        "LX/JXO;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JXO;",
            ">;"
        }
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2704347
    new-instance v0, LX/JXL;

    invoke-direct {v0}, LX/JXL;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704348
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2704349
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;
    .locals 3

    .prologue
    .line 2704350
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    monitor-enter v1

    .line 2704351
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704352
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704353
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704354
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2704355
    new-instance v0, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;-><init>()V

    .line 2704356
    move-object v0, v0

    .line 2704357
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704358
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704359
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JXO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2704361
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2704362
    check-cast p2, LX/JXM;

    .line 2704363
    iget-object v0, p2, LX/JXM;->a:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-static {v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)LX/0Px;

    move-result-object v3

    .line 2704364
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 2704365
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    .line 2704366
    iget-object v0, p2, LX/JXM;->a:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->o()Ljava/lang/String;

    move-result-object v6

    .line 2704367
    iget-object v0, p2, LX/JXM;->a:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->m()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    move-result-object v0

    .line 2704368
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;->a()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 2704369
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2704370
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->m()Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;->a()I

    move-result v0

    .line 2704371
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, v4, v2

    .line 2704372
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    .line 2704373
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2704374
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;->a()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2704375
    :cond_1
    new-instance v0, LX/JXN;

    invoke-direct {v0, v5, v6, v4, v3}, LX/JXN;-><init>([Ljava/lang/String;Ljava/lang/String;[ILX/0Px;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2ca7c488

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704376
    check-cast p1, LX/JXM;

    check-cast p2, LX/JXN;

    check-cast p4, LX/JXO;

    .line 2704377
    iget-object v1, p2, LX/JXN;->b:Ljava/lang/String;

    .line 2704378
    iget-object v2, p4, LX/JXO;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2704379
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p2, LX/JXN;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2704380
    iget-object v2, p2, LX/JXN;->a:[Ljava/lang/String;

    aget-object v2, v2, v1

    iget-object v4, p2, LX/JXN;->c:[I

    aget v4, v4, v1

    .line 2704381
    iget-object p0, p4, LX/JXO;->b:[Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    aget-object p0, p0, v1

    .line 2704382
    invoke-virtual {p0, v2}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->setText(Ljava/lang/String;)V

    .line 2704383
    invoke-virtual {p0, v4}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->setProgress(I)V

    .line 2704384
    const/4 p3, 0x0

    invoke-virtual {p0, p3}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->setVisibility(I)V

    .line 2704385
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2704386
    :cond_0
    iget v1, p1, LX/JXM;->b:I

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 2704387
    iget-object v2, p4, LX/JXO;->b:[Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;

    aget-object v2, v2, v1

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->setVisibility(I)V

    .line 2704388
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2704389
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x34b5e216    # -1.324593E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
