.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final b:Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/2dq;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704160
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2704161
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2704162
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    .line 2704163
    iput-object p3, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2704164
    iput-object p4, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->d:LX/2dq;

    .line 2704165
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;
    .locals 7

    .prologue
    .line 2704149
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    monitor-enter v1

    .line 2704150
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704151
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704154
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v6

    check-cast v6, LX/2dq;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;)V

    .line 2704155
    move-object v0, p0

    .line 2704156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2704126
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2704139
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2704140
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 2704141
    check-cast v5, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704142
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704143
    iget-object v6, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;->d:LX/2dq;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v1, v2}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->I_()I

    move-result v2

    .line 2704144
    invoke-static {v5}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)LX/0Px;

    move-result-object v3

    .line 2704145
    invoke-static {v3}, LX/JXK;->a(LX/0Px;)I

    move-result v4

    .line 2704146
    new-instance p2, LX/JXC;

    invoke-direct {p2, p0, v3, v4, v5}, LX/JXC;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollResultsHScrollPartDefinition;LX/0Px;ILcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    move-object v3, p2

    .line 2704147
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704148
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4e09221f    # 5.7517869E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704131
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    const/4 v5, 0x0

    .line 2704132
    iget-object v4, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2704133
    check-cast v4, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704134
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->n()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    .line 2704135
    :goto_0
    if-eqz v4, :cond_2

    move v4, v5

    :goto_1
    invoke-virtual {p4, v4}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    .line 2704136
    const/16 v1, 0x1f

    const v2, 0x13922990

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move v4, v5

    .line 2704137
    goto :goto_0

    .line 2704138
    :cond_2
    const/16 v4, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2704127
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 2704128
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2704129
    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704130
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    invoke-static {v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
