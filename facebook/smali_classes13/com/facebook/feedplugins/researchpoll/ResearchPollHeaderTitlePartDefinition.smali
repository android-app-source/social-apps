.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feed/rows/views/ContentTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/1Uf;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704069
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2704070
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->a:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 2704071
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->b:LX/1Uf;

    .line 2704072
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;
    .locals 5

    .prologue
    .line 2704073
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    monitor-enter v1

    .line 2704074
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704075
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704076
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704077
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704078
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;-><init>(Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;LX/1Uf;)V

    .line 2704079
    move-object v0, p0

    .line 2704080
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704081
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704082
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704083
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2704084
    check-cast p2, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704085
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->a:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    new-instance v1, LX/JXA;

    invoke-direct {v1, p0, p2}, LX/JXA;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704086
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x76cf966b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704087
    check-cast p4, Lcom/facebook/feed/rows/views/ContentTextView;

    .line 2704088
    const v1, 0x7f0d0081

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feed/rows/views/ContentTextView;->setTag(ILjava/lang/Object;)V

    .line 2704089
    const/16 v1, 0x1f

    const v2, -0x7d08900e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
