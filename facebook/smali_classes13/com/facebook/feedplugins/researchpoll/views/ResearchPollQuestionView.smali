.class public Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2704415
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2704416
    invoke-direct {p0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->a()V

    .line 2704417
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2704421
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2704422
    invoke-direct {p0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->a()V

    .line 2704423
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2704418
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2704419
    invoke-direct {p0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->a()V

    .line 2704420
    return-void
.end method

.method private final a()V
    .locals 1

    .prologue
    .line 2704410
    const v0, 0x7f0311ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2704411
    const v0, 0x7f0d29da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->a:Landroid/widget/TextView;

    .line 2704412
    const v0, 0x7f0d29d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->b:Landroid/widget/TextView;

    .line 2704413
    const v0, 0x7f0d29d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    .line 2704414
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)V
    .locals 4

    .prologue
    .line 2704424
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2704425
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2704426
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2704427
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2704428
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2704429
    :cond_0
    return-void
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 2704390
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2704391
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setAnswers(LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2704392
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->removeAllViews()V

    .line 2704393
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2704394
    invoke-virtual {p1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2704395
    new-instance v3, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 2704396
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 2704397
    iget-object v4, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v4, v3}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 2704398
    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2704399
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 2704400
    goto :goto_0

    .line 2704401
    :cond_0
    return-void
.end method

.method public setOnAnswerClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 2704406
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2704407
    iget-object v2, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2704408
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2704409
    :cond_0
    return-void
.end method

.method public setQuestionHint(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2704402
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2704403
    return-void
.end method

.method public setQuestionText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2704404
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2704405
    return-void
.end method
