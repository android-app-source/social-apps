.class public Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2704449
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2704450
    invoke-direct {p0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->a()V

    .line 2704451
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2704446
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2704447
    invoke-direct {p0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->a()V

    .line 2704448
    return-void
.end method

.method private final a()V
    .locals 1

    .prologue
    .line 2704441
    const v0, 0x7f0311d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2704442
    const v0, 0x7f0d29dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->b:Landroid/widget/TextView;

    .line 2704443
    const v0, 0x7f0d29de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->a:Landroid/widget/TextView;

    .line 2704444
    const v0, 0x7f0d29df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->c:Landroid/widget/ProgressBar;

    .line 2704445
    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 2704436
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->c:Landroid/widget/ProgressBar;

    const-string v1, "progress"

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput p1, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2704437
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2704438
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2704439
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2704440
    return-void
.end method


# virtual methods
.method public setProgress(I)V
    .locals 2

    .prologue
    .line 2704430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2704431
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2704432
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->c(I)V

    .line 2704433
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2704434
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollResultItemView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2704435
    return-void
.end method
