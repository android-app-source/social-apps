.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        ">;",
        "LX/JX9;",
        "TE;",
        "LX/3V0;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final d:LX/1e4;

.field public final e:LX/1nG;

.field public final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final g:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2704016
    const-class v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1e4;LX/1nG;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704053
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2704054
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2704055
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 2704056
    iput-object p3, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->d:LX/1e4;

    .line 2704057
    iput-object p4, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->e:LX/1nG;

    .line 2704058
    iput-object p5, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2704059
    iput-object p6, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->g:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    .line 2704060
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;
    .locals 10

    .prologue
    .line 2704042
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    monitor-enter v1

    .line 2704043
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704044
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704045
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704046
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704047
    new-instance v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v6

    check-cast v6, LX/1e4;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v7

    check-cast v7, LX/1nG;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1e4;LX/1nG;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;)V

    .line 2704048
    move-object v0, v3

    .line 2704049
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704050
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704051
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704052
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2704041
    sget-object v0, LX/3V0;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2704027
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2704028
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2704029
    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 2704030
    iget-object v2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    sget-object v4, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v3, p2, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704031
    const v2, 0x7f0d0bde

    iget-object v3, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v4, LX/24b;

    sget-object v5, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v4, p2, v5}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2704032
    const v2, 0x7f0d1616

    iget-object v3, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->g:Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderTitlePartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2704033
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 2704034
    new-instance v3, LX/JX8;

    invoke-direct {v3, p0, v2}, LX/JX8;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 2704035
    invoke-static {v2}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v2}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    .line 2704036
    :goto_0
    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2704037
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->H()Ljava/lang/String;

    move-result-object v0

    .line 2704038
    iget-object v2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->d:LX/1e4;

    const-string v4, "only_me"

    invoke-virtual {v2, v4}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v2

    .line 2704039
    new-instance v4, LX/JX9;

    invoke-direct {v4, v3, v1, v0, v2}, LX/JX9;-><init>(Landroid/view/View$OnClickListener;Landroid/net/Uri;Ljava/lang/String;I)V

    return-object v4

    :cond_1
    move-object v2, v1

    .line 2704040
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x37e1a25b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704021
    check-cast p2, LX/JX9;

    check-cast p4, LX/3V0;

    .line 2704022
    iget-object v1, p2, LX/JX9;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3V0;->setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2704023
    iget-object v1, p2, LX/JX9;->b:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollHeaderPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, LX/3V0;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2704024
    iget-object v1, p2, LX/JX9;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p4, v1, v2}, LX/3V0;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2704025
    iget v1, p2, LX/JX9;->d:I

    invoke-virtual {p4, v1}, LX/3V0;->setSubtitleIcon(I)V

    .line 2704026
    const/16 v1, 0x1f

    const v2, -0x64b9bc95    # -1.639996E-22f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2704020
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2704017
    check-cast p4, LX/3V0;

    .line 2704018
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/3V0;->setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2704019
    return-void
.end method
