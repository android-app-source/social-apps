.class public Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JWH;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

.field public final b:Landroid/content/res/Resources;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition",
            "<",
            "LX/1PW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702687
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2702688
    iput-object p1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->a:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    .line 2702689
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->b:Landroid/content/res/Resources;

    .line 2702690
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2702691
    iput-object p4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 2702692
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLContactPoint;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2702682
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLContactPoint;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2702683
    :cond_0
    :goto_0
    return-object v0

    .line 2702684
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLContactPoint;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 2702685
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f020850

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2702686
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f020934

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3609cb28 -> :sswitch_0
        0x1c4e6237 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;
    .locals 7

    .prologue
    .line 2702660
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

    monitor-enter v1

    .line 2702661
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702662
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702663
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702664
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702665
    new-instance p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;-><init>(Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 2702666
    move-object v0, p0

    .line 2702667
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702668
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702669
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2702671
    check-cast p2, LX/JWH;

    .line 2702672
    iget-object v0, p2, LX/JWH;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->l()Lcom/facebook/graphql/model/GraphQLContactPoint;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLContactPoint;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2702673
    const v1, 0x7f0d13bb

    iget-object v2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->a:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    iget-object v3, p2, LX/JWH;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->l()Lcom/facebook/graphql/model/GraphQLContactPoint;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLContactPoint;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702674
    const v1, 0x7f0d13bb

    iget-object v2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-nez v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702675
    const v0, 0x7f0d13bc

    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/JWH;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-boolean v3, p2, LX/JWH;->b:Z

    .line 2702676
    if-eqz v3, :cond_1

    .line 2702677
    iget-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->b:Landroid/content/res/Resources;

    const p3, 0x7f0828a0

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 2702678
    :goto_1
    move-object v2, p2

    .line 2702679
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702680
    const/4 v0, 0x0

    return-object v0

    .line 2702681
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->k()Ljava/lang/String;

    move-result-object p2

    goto :goto_1
.end method
