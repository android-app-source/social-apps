.class public Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

.field private final c:Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702845
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2702846
    iput-object p1, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->a:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;

    .line 2702847
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    .line 2702848
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->c:Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

    .line 2702849
    iput-object p4, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->d:LX/0ad;

    .line 2702850
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;
    .locals 7

    .prologue
    .line 2702851
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;

    monitor-enter v1

    .line 2702852
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702853
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702856
    new-instance p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;-><init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;LX/0ad;)V

    .line 2702857
    move-object v0, p0

    .line 2702858
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702859
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702860
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2702862
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702863
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->d:LX/0ad;

    sget-short v1, LX/JW9;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702864
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->c:Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702865
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2702866
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->a:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702867
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/PaginatedPeopleYouMayInvitePartDefinition;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702868
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702869
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702870
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
