.class public Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/JWO;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JWp;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JWp;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

.field private final c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

.field private final d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/0lB;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2702844
    new-instance v0, LX/JWM;

    invoke-direct {v0}, LX/JWM;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;LX/0lB;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702826
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2702827
    iput-object p1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    .line 2702828
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

    .line 2702829
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    .line 2702830
    iput-object p4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->e:LX/0lB;

    .line 2702831
    iput-object p5, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2702832
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;
    .locals 9

    .prologue
    .line 2702833
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;

    monitor-enter v1

    .line 2702834
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702835
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702836
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702837
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702838
    new-instance v3, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lB;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;-><init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;LX/0lB;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2702839
    move-object v0, v3

    .line 2702840
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702841
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702842
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702843
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JWp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2702825
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2702811
    check-cast p2, LX/JWO;

    check-cast p3, LX/1Pq;

    .line 2702812
    new-instance v0, LX/JWN;

    iget-object v1, p2, LX/JWO;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/JWN;-><init>(Ljava/lang/String;Z)V

    .line 2702813
    check-cast p3, LX/1Pr;

    iget-object v1, p2, LX/JWO;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 2702814
    iget-object v0, p2, LX/JWO;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 2702815
    :try_start_0
    iget-object v2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->e:LX/0lB;

    invoke-virtual {v2, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2702816
    const-string v3, "ego_id"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2702817
    :goto_0
    move-object v4, v1

    .line 2702818
    const v0, 0x7f0d13b9

    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/JWO;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702819
    const v0, 0x7f0d13ba

    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->c:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingCredentialPartDefinition;

    new-instance v2, LX/JWH;

    iget-object v3, p2, LX/JWO;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-direct {v2, v3, v5}, LX/JWH;-><init>(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;Z)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702820
    const v0, 0x7f0d13bd

    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    new-instance v2, LX/JWG;

    iget-object v3, p2, LX/JWO;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iget-object v6, p2, LX/JWO;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-direct {v2, v3, v6, v4, v5}, LX/JWG;-><init>(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;Ljava/lang/String;Z)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702821
    const v6, 0x7f0d13be

    iget-object v7, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    new-instance v0, LX/JWK;

    iget-object v1, p2, LX/JWO;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iget-object v2, p2, LX/JWO;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v3, p2, LX/JWO;->c:LX/2dx;

    invoke-direct/range {v0 .. v5}, LX/JWK;-><init>(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;LX/2dx;Ljava/lang/String;Z)V

    invoke-interface {p1, v6, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2702822
    const/4 v0, 0x0

    return-object v0

    .line 2702823
    :cond_0
    :try_start_1
    const-string v3, "ego_id"

    invoke-virtual {v2, v3}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    .line 2702824
    :catch_0
    goto :goto_0
.end method
