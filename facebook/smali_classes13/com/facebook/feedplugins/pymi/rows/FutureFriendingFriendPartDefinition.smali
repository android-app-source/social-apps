.class public Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JWK;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/23P;

.field private final b:LX/17Q;

.field public final c:LX/0Zb;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/2dp;

.field public final f:Landroid/content/res/Resources;

.field private final g:Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;


# direct methods
.method public constructor <init>(LX/23P;LX/17Q;LX/0Zb;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2dp;Landroid/content/res/Resources;Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702746
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2702747
    iput-object p1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a:LX/23P;

    .line 2702748
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->b:LX/17Q;

    .line 2702749
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->c:LX/0Zb;

    .line 2702750
    iput-object p4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2702751
    iput-object p5, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->e:LX/2dp;

    .line 2702752
    iput-object p6, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->f:Landroid/content/res/Resources;

    .line 2702753
    iput-object p7, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->g:Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    .line 2702754
    iput-object p8, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->h:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    .line 2702755
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;
    .locals 12

    .prologue
    .line 2702735
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    monitor-enter v1

    .line 2702736
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702737
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702738
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702739
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702740
    new-instance v3, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/2dp;->b(LX/0QB;)LX/2dp;

    move-result-object v8

    check-cast v8, LX/2dp;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;-><init>(LX/23P;LX/17Q;LX/0Zb;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2dp;Landroid/content/res/Resources;Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;)V

    .line 2702741
    move-object v0, v3

    .line 2702742
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702743
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702744
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2702721
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a:LX/23P;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2702722
    check-cast p2, LX/JWK;

    check-cast p3, LX/1Pq;

    .line 2702723
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2702724
    new-instance v1, LX/JWJ;

    invoke-direct {v1, p0, p2, p3}, LX/JWJ;-><init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;LX/JWK;LX/1Pq;)V

    move-object v1, v1

    .line 2702725
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702726
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->h:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    iget-boolean v1, p2, LX/JWK;->e:Z

    .line 2702727
    new-instance v3, LX/2eV;

    if-eqz v1, :cond_0

    const v2, 0x7f02089f

    :goto_0
    iget-object v4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->f:Landroid/content/res/Resources;

    const p3, 0x7f0a00a4

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, v2, v4}, LX/2eV;-><init>(ILjava/lang/Integer;)V

    move-object v1, v3

    .line 2702728
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702729
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->g:Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    iget-boolean v1, p2, LX/JWK;->e:Z

    .line 2702730
    if-eqz v1, :cond_1

    .line 2702731
    new-instance v2, LX/2eW;

    iget-object v3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->f:Landroid/content/res/Resources;

    const v4, 0x7f080f7d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->f:Landroid/content/res/Resources;

    const p2, 0x7f080017

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2eW;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2702732
    :goto_1
    move-object v1, v2

    .line 2702733
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702734
    const/4 v0, 0x0

    return-object v0

    :cond_0
    const v2, 0x7f020b77

    goto :goto_0

    :cond_1
    new-instance v2, LX/2eW;

    iget-object v3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->f:Landroid/content/res/Resources;

    const v4, 0x7f080f7b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->f:Landroid/content/res/Resources;

    const p2, 0x7f080f7c

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;->a(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingFriendPartDefinition;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2eW;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
