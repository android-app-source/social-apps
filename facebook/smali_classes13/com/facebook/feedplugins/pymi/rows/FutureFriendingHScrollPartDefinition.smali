.class public Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final b:LX/1LV;

.field public final c:LX/JWD;

.field public final d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/2dq;

.field private final f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1LV;LX/JWD;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702789
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2702790
    iput-object p1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2702791
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->b:LX/1LV;

    .line 2702792
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->c:LX/JWD;

    .line 2702793
    iput-object p4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->d:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;

    .line 2702794
    iput-object p5, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->e:LX/2dq;

    .line 2702795
    iput-object p6, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    .line 2702796
    iput-object p7, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->g:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2702797
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;
    .locals 11

    .prologue
    .line 2702778
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    monitor-enter v1

    .line 2702779
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702780
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702781
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702782
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702783
    new-instance v3, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    invoke-static {v0}, LX/JWD;->a(LX/0QB;)LX/JWD;

    move-result-object v6

    check-cast v6, LX/JWD;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v8

    check-cast v8, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1LV;LX/JWD;Lcom/facebook/feedplugins/pymi/rows/FutureFriendingPagePartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;)V

    .line 2702784
    move-object v0, v3

    .line 2702785
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702786
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702787
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702788
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2702777
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2702766
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702767
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 2702768
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 2702769
    new-instance v6, LX/2dx;

    invoke-direct {v6}, LX/2dx;-><init>()V

    .line 2702770
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702771
    iget-object v7, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->g:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->e:LX/2dq;

    const/high16 v2, 0x439a0000    # 308.0f

    sget-object v3, LX/2eF;->a:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I_()I

    move-result v2

    .line 2702772
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r()LX/0Px;

    move-result-object v3

    .line 2702773
    new-instance v4, LX/JWL;

    invoke-direct {v4, p0, v3, v5, v6}, LX/JWL;-><init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;LX/0Px;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;LX/2dx;)V

    move-object v3, v4

    .line 2702774
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702775
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingHScrollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-interface {p1, v0, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702776
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702765
    const/4 v0, 0x1

    return v0
.end method
