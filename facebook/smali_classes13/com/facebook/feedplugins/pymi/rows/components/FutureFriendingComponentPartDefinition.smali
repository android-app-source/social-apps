.class public Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JWX;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JWX;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703171
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2703172
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->d:LX/JWX;

    .line 2703173
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->e:LX/1V0;

    .line 2703174
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2703175
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->d:LX/JWX;

    const/4 v1, 0x0

    .line 2703176
    new-instance v2, LX/JWW;

    invoke-direct {v2, v0}, LX/JWW;-><init>(LX/JWX;)V

    .line 2703177
    iget-object v3, v0, LX/JWX;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JWV;

    .line 2703178
    if-nez v3, :cond_0

    .line 2703179
    new-instance v3, LX/JWV;

    invoke-direct {v3, v0}, LX/JWV;-><init>(LX/JWX;)V

    .line 2703180
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JWV;->a$redex0(LX/JWV;LX/1De;IILX/JWW;)V

    .line 2703181
    move-object v2, v3

    .line 2703182
    move-object v1, v2

    .line 2703183
    move-object v0, v1

    .line 2703184
    iget-object v1, v0, LX/JWV;->a:LX/JWW;

    iput-object p3, v1, LX/JWW;->a:LX/1Pc;

    .line 2703185
    iget-object v1, v0, LX/JWV;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2703186
    move-object v0, v0

    .line 2703187
    iget-object v1, v0, LX/JWV;->a:LX/JWW;

    iput-object p2, v1, LX/JWW;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703188
    iget-object v1, v0, LX/JWV;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2703189
    move-object v0, v0

    .line 2703190
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2703191
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2703192
    iget-object v2, p0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;
    .locals 6

    .prologue
    .line 2703193
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

    monitor-enter v1

    .line 2703194
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703195
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703196
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703197
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703198
    new-instance p0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JWX;->a(LX/0QB;)LX/JWX;

    move-result-object v4

    check-cast v4, LX/JWX;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;-><init>(Landroid/content/Context;LX/JWX;LX/1V0;)V

    .line 2703199
    move-object v0, p0

    .line 2703200
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703201
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703202
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2703204
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2703205
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pymi/rows/components/FutureFriendingComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2703206
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2703207
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2703208
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
