.class public Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JWG;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field private final b:LX/17Q;

.field public final c:LX/0Zb;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field public final f:LX/JWD;

.field public final g:LX/2dj;

.field public final h:LX/2do;

.field public final i:Landroid/content/res/Resources;

.field private final j:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final k:Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition",
            "<",
            "LX/1PW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/23P;LX/17Q;LX/0Zb;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/JWD;LX/2dj;LX/2do;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702622
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2702623
    iput-object p1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->a:LX/23P;

    .line 2702624
    iput-object p2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->b:LX/17Q;

    .line 2702625
    iput-object p3, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->c:LX/0Zb;

    .line 2702626
    iput-object p4, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2702627
    iput-object p5, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->e:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2702628
    iput-object p6, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->f:LX/JWD;

    .line 2702629
    iput-object p7, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->g:LX/2dj;

    .line 2702630
    iput-object p8, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->h:LX/2do;

    .line 2702631
    iput-object p9, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->i:Landroid/content/res/Resources;

    .line 2702632
    iput-object p10, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->j:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2702633
    iput-object p11, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->k:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 2702634
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;
    .locals 15

    .prologue
    .line 2702635
    const-class v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    monitor-enter v1

    .line 2702636
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702637
    sput-object v2, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702638
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702639
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702640
    new-instance v3, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v8

    check-cast v8, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0}, LX/JWD;->a(LX/0QB;)LX/JWD;

    move-result-object v9

    check-cast v9, LX/JWD;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v10

    check-cast v10, LX/2dj;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v11

    check-cast v11, LX/2do;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v12

    check-cast v12, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;-><init>(LX/23P;LX/17Q;LX/0Zb;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/JWD;LX/2dj;LX/2do;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 2702641
    move-object v0, v3

    .line 2702642
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702643
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702644
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2702646
    check-cast p2, LX/JWG;

    .line 2702647
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2702648
    new-instance v1, LX/JWF;

    invoke-direct {v1, p0, p2}, LX/JWF;-><init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;LX/JWG;)V

    move-object v1, v1

    .line 2702649
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702650
    iget-object v0, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->j:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2702651
    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->a:LX/23P;

    iget-object v2, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->i:Landroid/content/res/Resources;

    const p3, 0x7f08289f

    invoke-virtual {v2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x0

    invoke-virtual {v1, v2, p3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v1, v1

    .line 2702652
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702653
    iget-object v1, p0, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->k:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    iget-boolean v0, p2, LX/JWG;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2702654
    const/4 v0, 0x0

    return-object v0

    .line 2702655
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
