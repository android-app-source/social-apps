.class public Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2707465
    const-class v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2707468
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2707469
    const v0, 0x7f03075d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2707470
    const v0, 0x7f0d13a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2707471
    const v0, 0x7f0d13a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2707472
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b25e5

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 2707473
    const v0, 0x7f0d13a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2707474
    const v0, 0x7f0d13a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2707475
    invoke-virtual {p0}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2707476
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2707477
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2707478
    move-object v1, v1

    .line 2707479
    const v2, 0x7f020bc4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v1

    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 2707480
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2707481
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f0a0996

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2707482
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2707483
    move-object v1, v1

    .line 2707484
    sget-object v2, LX/1Up;->b:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    const v2, 0x7f020bc5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2707485
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2707486
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 2707487
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0997

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 2707488
    return-void
.end method

.method public static b(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2707466
    invoke-virtual {p0}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0997

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v2, v2, v2, v0}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 2707467
    return-void
.end method
