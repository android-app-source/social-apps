.class public Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/1nu;

.field public final d:LX/17W;

.field public final e:LX/JYl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2707052
    const-class v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/1nu;LX/17W;LX/JYl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2707054
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->b:LX/0Zb;

    .line 2707055
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->c:LX/1nu;

    .line 2707056
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->d:LX/17W;

    .line 2707057
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->e:LX/JYl;

    .line 2707058
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;
    .locals 7

    .prologue
    .line 2707059
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;

    monitor-enter v1

    .line 2707060
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707061
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707062
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707063
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707064
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/JYl;->a(LX/0QB;)LX/JYl;

    move-result-object v6

    check-cast v6, LX/JYl;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;-><init>(LX/0Zb;LX/1nu;LX/17W;LX/JYl;)V

    .line 2707065
    move-object v0, p0

    .line 2707066
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707067
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707068
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707069
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2707070
    invoke-static {p1}, LX/JYy;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2707071
    const-string v0, ""

    .line 2707072
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2707073
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 2707074
    :goto_0
    invoke-static {p1}, LX/JYy;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v4

    .line 2707075
    if-eqz v4, :cond_0

    .line 2707076
    const/4 v0, 0x1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b2600    # 1.8496E38f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b2601

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    move v5, v1

    move-object v6, p0

    move-object v9, v3

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 2707077
    :cond_0
    return-object v5

    :cond_1
    move-object v5, v0

    goto :goto_0
.end method
