.class public Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/2g9;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/17W;

.field public final f:LX/1Uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2707330
    const-class v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/2g9;LX/0Or;LX/17W;LX/1Uf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/2g9;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/17W;",
            "LX/1Uf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2707332
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->b:LX/0Zb;

    .line 2707333
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->c:LX/2g9;

    .line 2707334
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->d:LX/0Or;

    .line 2707335
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->e:LX/17W;

    .line 2707336
    iput-object p5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->f:LX/1Uf;

    .line 2707337
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;
    .locals 9

    .prologue
    .line 2707338
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;

    monitor-enter v1

    .line 2707339
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707340
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707341
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707342
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707343
    new-instance v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v5

    check-cast v5, LX/2g9;

    const/16 v6, 0x509

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v8

    check-cast v8, LX/1Uf;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;-><init>(LX/0Zb;LX/2g9;LX/0Or;LX/17W;LX/1Uf;)V

    .line 2707344
    move-object v0, v3

    .line 2707345
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707346
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707347
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707348
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
