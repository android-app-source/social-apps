.class public Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/8yV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2706841
    const-class v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/8yV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2706843
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;->b:LX/8yV;

    .line 2706844
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;
    .locals 4

    .prologue
    .line 2706845
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;

    monitor-enter v1

    .line 2706846
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706847
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706848
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706849
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706850
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;

    invoke-static {v0}, LX/8yV;->a(LX/0QB;)LX/8yV;

    move-result-object v3

    check-cast v3, LX/8yV;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;-><init>(LX/8yV;)V

    .line 2706851
    move-object v0, p0

    .line 2706852
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706853
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepileComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706854
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706855
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
