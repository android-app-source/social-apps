.class public Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

.field private final c:LX/0Uh;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;LX/0Uh;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;",
            "Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707401
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2707402
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

    .line 2707403
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

    .line 2707404
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->c:LX/0Uh;

    .line 2707405
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->d:LX/0Ot;

    .line 2707406
    iput-object p5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->e:LX/0Ot;

    .line 2707407
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;
    .locals 9

    .prologue
    .line 2707440
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2707441
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707442
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707443
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707444
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707445
    new-instance v3, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xbc

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;LX/0Uh;LX/0Ot;LX/0Ot;)V

    .line 2707446
    move-object v0, v3

    .line 2707447
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707448
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707449
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2707436
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707437
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2707438
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2707439
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2707408
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2707409
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2707410
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2707411
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->c:LX/0Uh;

    const/16 v3, 0x4ba

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2707412
    :goto_0
    return v0

    :cond_0
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2707413
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2707414
    if-nez v0, :cond_2

    .line 2707415
    const-string v1, "no_attachment"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2707416
    :cond_1
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 2707417
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v5, "fundraiser_share_attachment_error"

    const-string p1, ", "

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-static {p1, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2707418
    :goto_2
    move v0, v1

    .line 2707419
    goto :goto_0

    .line 2707420
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2707421
    const-string v1, "no_target"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2707422
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    if-nez v1, :cond_4

    .line 2707423
    const-string v1, "no_campaign"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2707424
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2707425
    :cond_5
    const-string v1, "no_fundraiser_progress_text"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2707426
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2707427
    :cond_7
    const-string v1, "no_charity_text"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2707428
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2707429
    const-string v1, "no_fundraiser_title"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2707430
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_a

    .line 2707431
    const-string v1, "no_profile_picture"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2707432
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->n()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2707433
    const-string v1, "no_campaign_id"

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2707434
    :cond_b
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/BOe;->c(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    move v1, v3

    .line 2707435
    goto/16 :goto_2
.end method
