.class public Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V7;

.field private final e:LX/1V0;

.field private final f:LX/JYf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V7;LX/1V0;LX/JYf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706891
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2706892
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->d:LX/1V7;

    .line 2706893
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->e:LX/1V0;

    .line 2706894
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->f:LX/JYf;

    .line 2706895
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2706858
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->d:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->j()LX/1Ua;

    move-result-object v2

    const v3, 0x7f020aed

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 2706859
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->f:LX/JYf;

    const/4 v2, 0x0

    .line 2706860
    new-instance v3, LX/JYe;

    invoke-direct {v3, v1}, LX/JYe;-><init>(LX/JYf;)V

    .line 2706861
    sget-object v4, LX/JYf;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JYd;

    .line 2706862
    if-nez v4, :cond_0

    .line 2706863
    new-instance v4, LX/JYd;

    invoke-direct {v4}, LX/JYd;-><init>()V

    .line 2706864
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JYd;->a$redex0(LX/JYd;LX/1De;IILX/JYe;)V

    .line 2706865
    move-object v3, v4

    .line 2706866
    move-object v2, v3

    .line 2706867
    move-object v1, v2

    .line 2706868
    iget-object v2, v1, LX/JYd;->a:LX/JYe;

    iput-object p2, v2, LX/JYe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706869
    iget-object v2, v1, LX/JYd;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2706870
    move-object v1, v1

    .line 2706871
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2706872
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;
    .locals 7

    .prologue
    .line 2706880
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;

    monitor-enter v1

    .line 2706881
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706882
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706883
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706884
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706885
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/JYf;->a(LX/0QB;)LX/JYf;

    move-result-object v6

    check-cast v6, LX/JYf;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;-><init>(Landroid/content/Context;LX/1V7;LX/1V0;LX/JYf;)V

    .line 2706886
    move-object v0, p0

    .line 2706887
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706888
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706889
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706890
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2706896
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2706879
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2706873
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706874
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2706875
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2706876
    invoke-static {v0}, LX/JYp;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    .line 2706877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->my()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->my()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pC()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pC()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2706878
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2706856
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706857
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
