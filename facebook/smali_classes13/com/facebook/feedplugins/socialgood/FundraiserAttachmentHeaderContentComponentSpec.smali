.class public Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2707180
    const-class v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2707177
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->b:LX/1nu;

    .line 2707178
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->c:LX/0Ot;

    .line 2707179
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;IIZ)LX/1ne;
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 2707173
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    .line 2707174
    if-nez p4, :cond_0

    .line 2707175
    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f0a0998

    invoke-virtual {v0, v1}, LX/1ne;->l(I)LX/1ne;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, LX/1ne;->c(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->d(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->e(F)LX/1ne;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;
    .locals 5

    .prologue
    .line 2707162
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;

    monitor-enter v1

    .line 2707163
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707164
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707165
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707166
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707167
    new-instance v4, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;-><init>(LX/1nu;LX/0Ot;)V

    .line 2707168
    move-object v0, v4

    .line 2707169
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707170
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707171
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
