.class public Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/JYb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/JYb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706721
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2706722
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->d:LX/1V0;

    .line 2706723
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->e:LX/JYb;

    .line 2706724
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2706725
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2706726
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/JYy;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    .line 2706727
    iput v1, v0, LX/1UY;->d:F

    .line 2706728
    move-object v0, v0

    .line 2706729
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    move-object v1, v0

    .line 2706730
    :goto_0
    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 2706731
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2706732
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2706733
    invoke-static {v0}, LX/JYy;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-nez v4, :cond_3

    .line 2706734
    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f020bd1

    .line 2706735
    :goto_1
    move v0, v4

    .line 2706736
    const/4 v4, -0x1

    invoke-direct {v2, v3, v1, v0, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 2706737
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->e:LX/JYb;

    const/4 v1, 0x0

    .line 2706738
    new-instance v3, LX/JYa;

    invoke-direct {v3, v0}, LX/JYa;-><init>(LX/JYb;)V

    .line 2706739
    iget-object v4, v0, LX/JYb;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JYZ;

    .line 2706740
    if-nez v4, :cond_0

    .line 2706741
    new-instance v4, LX/JYZ;

    invoke-direct {v4, v0}, LX/JYZ;-><init>(LX/JYb;)V

    .line 2706742
    :cond_0
    invoke-static {v4, p1, v1, v1, v3}, LX/JYZ;->a$redex0(LX/JYZ;LX/1De;IILX/JYa;)V

    .line 2706743
    move-object v3, v4

    .line 2706744
    move-object v1, v3

    .line 2706745
    move-object v0, v1

    .line 2706746
    iget-object v1, v0, LX/JYZ;->a:LX/JYa;

    iput-object p2, v1, LX/JYa;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706747
    iget-object v1, v0, LX/JYZ;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2706748
    move-object v0, v0

    .line 2706749
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2706750
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->d:LX/1V0;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 2706751
    :cond_1
    sget-object v0, LX/1Ua;->m:LX/1Ua;

    move-object v1, v0

    goto :goto_0

    .line 2706752
    :cond_2
    const v4, 0x7f020bd0

    goto :goto_1

    .line 2706753
    :cond_3
    invoke-static {v0}, LX/JYy;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_4

    const v4, 0x7f020a46

    goto :goto_1

    :cond_4
    const v4, 0x7f020a41

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;
    .locals 6

    .prologue
    .line 2706754
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

    monitor-enter v1

    .line 2706755
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706756
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706757
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706758
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706759
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/JYb;->a(LX/0QB;)LX/JYb;

    move-result-object v5

    check-cast v5, LX/JYb;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/JYb;)V

    .line 2706760
    move-object v0, p0

    .line 2706761
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706762
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706763
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706764
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2706765
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2706766
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentBodyPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2706767
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2706768
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706769
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
