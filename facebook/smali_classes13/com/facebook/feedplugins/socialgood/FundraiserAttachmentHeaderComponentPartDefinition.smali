.class public Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/JYi;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2707045
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    .line 2707046
    iput v1, v0, LX/1UY;->d:F

    .line 2707047
    move-object v0, v0

    .line 2707048
    const/high16 v1, 0x41000000    # 8.0f

    .line 2707049
    iput v1, v0, LX/1UY;->b:F

    .line 2707050
    move-object v0, v0

    .line 2707051
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/JYi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707010
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2707011
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2707012
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->f:LX/JYi;

    .line 2707013
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2707030
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->d:LX/1Ua;

    const v3, 0x7f020a48

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 2707031
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->f:LX/JYi;

    const/4 v2, 0x0

    .line 2707032
    new-instance v3, LX/JYh;

    invoke-direct {v3, v1}, LX/JYh;-><init>(LX/JYi;)V

    .line 2707033
    sget-object v4, LX/JYi;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JYg;

    .line 2707034
    if-nez v4, :cond_0

    .line 2707035
    new-instance v4, LX/JYg;

    invoke-direct {v4}, LX/JYg;-><init>()V

    .line 2707036
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JYg;->a$redex0(LX/JYg;LX/1De;IILX/JYh;)V

    .line 2707037
    move-object v3, v4

    .line 2707038
    move-object v2, v3

    .line 2707039
    move-object v1, v2

    .line 2707040
    iget-object v2, v1, LX/JYg;->a:LX/JYh;

    iput-object p2, v2, LX/JYh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707041
    iget-object v2, v1, LX/JYg;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2707042
    move-object v1, v1

    .line 2707043
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2707044
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2707019
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2707020
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707021
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707022
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707023
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707024
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/JYi;->a(LX/0QB;)LX/JYi;

    move-result-object v5

    check-cast v5, LX/JYi;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/JYi;)V

    .line 2707025
    move-object v0, p0

    .line 2707026
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707027
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707028
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2707018
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2707017
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2707016
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2707014
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707015
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
