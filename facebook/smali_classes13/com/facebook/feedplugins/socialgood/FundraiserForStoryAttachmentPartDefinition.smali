.class public Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707355
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2707356
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

    .line 2707357
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;

    .line 2707358
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 2707359
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;

    monitor-enter v1

    .line 2707360
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707361
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707362
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707363
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707364
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;)V

    .line 2707365
    move-object v0, p0

    .line 2707366
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707367
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707368
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2707351
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707352
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2707353
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentFacepilePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2707354
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2707349
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707350
    invoke-static {p1}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
