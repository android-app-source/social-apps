.class public Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V7;

.field private final e:LX/1V0;

.field private final f:LX/JYo;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V7;LX/1V0;LX/JYo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707289
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2707290
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->d:LX/1V7;

    .line 2707291
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->e:LX/1V0;

    .line 2707292
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->f:LX/JYo;

    .line 2707293
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2707294
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->d:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->l()LX/1Ua;

    move-result-object v2

    const v3, 0x7f020bcf

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 2707295
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->f:LX/JYo;

    const/4 v2, 0x0

    .line 2707296
    new-instance v3, LX/JYn;

    invoke-direct {v3, v1}, LX/JYn;-><init>(LX/JYo;)V

    .line 2707297
    iget-object v4, v1, LX/JYo;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JYm;

    .line 2707298
    if-nez v4, :cond_0

    .line 2707299
    new-instance v4, LX/JYm;

    invoke-direct {v4, v1}, LX/JYm;-><init>(LX/JYo;)V

    .line 2707300
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JYm;->a$redex0(LX/JYm;LX/1De;IILX/JYn;)V

    .line 2707301
    move-object v3, v4

    .line 2707302
    move-object v2, v3

    .line 2707303
    move-object v1, v2

    .line 2707304
    iget-object v2, v1, LX/JYm;->a:LX/JYn;

    iput-object p2, v2, LX/JYn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707305
    iget-object v2, v1, LX/JYm;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2707306
    move-object v1, v1

    .line 2707307
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2707308
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;
    .locals 7

    .prologue
    .line 2707309
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

    monitor-enter v1

    .line 2707310
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707311
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707312
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707313
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707314
    new-instance p0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/JYo;->a(LX/0QB;)LX/JYo;

    move-result-object v6

    check-cast v6, LX/JYo;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V7;LX/1V0;LX/JYo;)V

    .line 2707315
    move-object v0, p0

    .line 2707316
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707317
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707318
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707319
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2707320
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2707321
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2707322
    invoke-static {v0}, LX/JYp;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2707323
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->mw()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->mz()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCharity;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2707324
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2707325
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2707326
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2707327
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2707328
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707329
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
