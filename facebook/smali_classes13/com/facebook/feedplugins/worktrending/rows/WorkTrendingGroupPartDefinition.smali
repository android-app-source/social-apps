.class public Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708781
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2708782
    iput-object p1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;->a:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    .line 2708783
    iput-object p2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;->b:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;

    .line 2708784
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;
    .locals 5

    .prologue
    .line 2708765
    const-class v1, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;

    monitor-enter v1

    .line 2708766
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708767
    sput-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708768
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708769
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708770
    new-instance p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;)V

    .line 2708771
    move-object v0, p0

    .line 2708772
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708773
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708774
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2708777
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2708778
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;->a:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2708779
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;->b:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2708780
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2708776
    const/4 v0, 0x1

    return v0
.end method
