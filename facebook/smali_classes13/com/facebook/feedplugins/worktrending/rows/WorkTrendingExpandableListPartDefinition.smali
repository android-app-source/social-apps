.class public Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;",
        ">;",
        "LX/JZV;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

.field private final b:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708748
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2708749
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->c:Z

    .line 2708750
    iput-object p1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->a:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    .line 2708751
    iput-object p2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->b:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

    .line 2708752
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;
    .locals 5

    .prologue
    .line 2708714
    const-class v1, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;

    monitor-enter v1

    .line 2708715
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708716
    sput-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708719
    new-instance p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;-><init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;)V

    .line 2708720
    move-object v0, p0

    .line 2708721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 2708725
    const/4 v0, 0x3

    if-le p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2708726
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v4, 0x0

    .line 2708727
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2708728
    check-cast v0, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;

    .line 2708729
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;->n()LX/0Px;

    move-result-object v6

    .line 2708730
    new-instance v7, LX/JZV;

    invoke-direct {v7, v0}, LX/JZV;-><init>(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;)V

    .line 2708731
    invoke-interface {p3, v7, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->c:Z

    .line 2708732
    iget-boolean v1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->c:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    .line 2708733
    :goto_0
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    if-le v2, v1, :cond_1

    move v2, v1

    :goto_1
    move v5, v4

    .line 2708734
    :goto_2
    if-ge v5, v2, :cond_3

    .line 2708735
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;

    .line 2708736
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    invoke-static {v3}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    add-int/lit8 v3, v2, -0x1

    if-ne v5, v3, :cond_2

    const/4 v3, 0x1

    .line 2708737
    :goto_3
    new-instance v8, LX/JZZ;

    invoke-direct {v8, v1, v3}, LX/JZZ;-><init>(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;Z)V

    .line 2708738
    iget-object v1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->a:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    invoke-virtual {p1, v1, v8}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2708739
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 2708740
    :cond_0
    const/4 v1, 0x3

    goto :goto_0

    .line 2708741
    :cond_1
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    move v2, v1

    goto :goto_1

    :cond_2
    move v3, v4

    .line 2708742
    goto :goto_3

    .line 2708743
    :cond_3
    new-instance v1, LX/JZX;

    iget-boolean v2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->c:Z

    new-instance v3, LX/JZU;

    invoke-direct {v3, p0, p3, v0, p2}, LX/JZU;-><init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;LX/1Pf;Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-direct {v1, v2, v3}, LX/JZX;-><init>(ZLX/JZU;)V

    .line 2708744
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2708745
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingExpandableListPartDefinition;->b:Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2708746
    :cond_4
    return-object v7
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2708747
    const/4 v0, 0x1

    return v0
.end method
