.class public Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704605
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2704606
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 2704607
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

    .line 2704608
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->c:LX/0ad;

    .line 2704609
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 2704575
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;

    monitor-enter v1

    .line 2704576
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704577
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704578
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704579
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704580
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;LX/0ad;)V

    .line 2704581
    move-object v0, p0

    .line 2704582
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704583
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704584
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704585
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2704604
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2704600
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2704601
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704602
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704603
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2704586
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2704587
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->c:LX/0ad;

    sget-short v3, LX/JXv;->i:S

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2704588
    if-nez v0, :cond_0

    move v0, v1

    .line 2704589
    :goto_0
    return v0

    .line 2704590
    :cond_0
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2704591
    if-nez v0, :cond_1

    move v0, v1

    .line 2704592
    goto :goto_0

    .line 2704593
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    .line 2704594
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 2704595
    goto :goto_0

    .line 2704596
    :cond_2
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    .line 2704597
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-ne v3, v2, :cond_3

    move v0, v2

    .line 2704598
    goto :goto_0

    .line 2704599
    :cond_3
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
