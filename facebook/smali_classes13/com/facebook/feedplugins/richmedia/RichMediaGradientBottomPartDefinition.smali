.class public Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/JXS;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704740
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;
    .locals 3

    .prologue
    .line 2704741
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;

    monitor-enter v1

    .line 2704742
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704743
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2704746
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;-><init>()V

    .line 2704747
    move-object v0, v0

    .line 2704748
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5e515790

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704752
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2704753
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2704754
    if-nez v1, :cond_0

    .line 2704755
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x1ec1ec68

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2704756
    :cond_0
    check-cast p4, LX/JXS;

    invoke-interface {p4}, LX/JXS;->f()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2704757
    if-nez p4, :cond_0

    .line 2704758
    :goto_0
    return-void

    .line 2704759
    :cond_0
    check-cast p4, LX/JXS;

    invoke-interface {p4}, LX/JXS;->g()V

    goto :goto_0
.end method
