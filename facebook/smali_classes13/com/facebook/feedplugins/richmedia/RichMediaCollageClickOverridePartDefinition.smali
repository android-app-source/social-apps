.class public Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/Aj7",
        "<",
        "LX/26M;",
        ">;",
        "LX/1Pf;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/2yJ;

.field public final b:LX/2yr;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/2yJ;LX/2yr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704647
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2704648
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->a:LX/2yJ;

    .line 2704649
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->b:LX/2yr;

    .line 2704650
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;
    .locals 5

    .prologue
    .line 2704651
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

    monitor-enter v1

    .line 2704652
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704653
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704656
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;

    invoke-static {v0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v3

    check-cast v3, LX/2yJ;

    invoke-static {v0}, LX/2yr;->b(LX/0QB;)LX/2yr;

    move-result-object v4

    check-cast v4, LX/2yr;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;-><init>(LX/2yJ;LX/2yr;)V

    .line 2704657
    move-object v0, p0

    .line 2704658
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704659
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704660
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2704634
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 2704635
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2704636
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2704637
    :goto_0
    return-object v0

    .line 2704638
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    .line 2704639
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2704640
    goto :goto_0

    .line 2704641
    :cond_1
    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    .line 2704642
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 2704643
    goto :goto_0

    .line 2704644
    :cond_2
    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->c:Ljava/lang/String;

    .line 2704645
    new-instance v0, LX/JXW;

    invoke-direct {v0, p0, v3, p3}, LX/JXW;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;LX/0Px;LX/1Pf;)V

    move-object v0, v0

    .line 2704646
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3d2b6a02

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704630
    check-cast p2, LX/Aj7;

    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 2704631
    iput-object p2, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 2704632
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->b:LX/2yr;

    invoke-virtual {p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2704633
    const/16 v1, 0x1f

    const v2, 0x1337f1f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2704618
    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 2704619
    const/4 v0, 0x0

    .line 2704620
    iput-object v0, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 2704621
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollageClickOverridePartDefinition;->b:LX/2yr;

    .line 2704622
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    if-eqz p0, :cond_2

    .line 2704623
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 2704624
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    if-eqz p1, :cond_0

    .line 2704625
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    invoke-interface {p1}, LX/1ca;->g()Z

    .line 2704626
    :cond_0
    iget-object p1, p0, LX/CHQ;->d:LX/8Ys;

    if-eqz p1, :cond_1

    .line 2704627
    iget-object p1, p0, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object p1, p1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    iget-object p2, p0, LX/CHQ;->d:LX/8Ys;

    invoke-interface {p2}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/8bW;->a(Ljava/lang/String;)V

    .line 2704628
    :cond_1
    const/4 p0, 0x0

    iput-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 2704629
    :goto_0
    return-void

    :cond_2
    goto :goto_0
.end method
