.class public Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

.field private final c:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

.field private final f:Z

.field private final g:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;Ljava/lang/Boolean;LX/0ad;)V
    .locals 1
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/feedplugins/richmedia/annotations/IsRichMediaAttachmentRenderingEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704545
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2704546
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    .line 2704547
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->c:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    .line 2704548
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;

    .line 2704549
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

    .line 2704550
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->f:Z

    .line 2704551
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

    .line 2704552
    iput-object p7, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->g:LX/0ad;

    .line 2704553
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;
    .locals 12

    .prologue
    .line 2704528
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;

    monitor-enter v1

    .line 2704529
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704530
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704531
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704532
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704533
    new-instance v3, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

    .line 2704534
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const/16 v10, 0x261

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, LX/0Uh;->a(IZ)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    move-object v9, v9

    .line 2704535
    check-cast v9, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;Ljava/lang/Boolean;LX/0ad;)V

    .line 2704536
    move-object v0, v3

    .line 2704537
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704538
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704539
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704540
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2704541
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2704542
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->c:Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2704543
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2704544
    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->f:Z

    return v0
.end method
