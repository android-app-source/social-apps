.class public Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JXo;",
        "LX/2oV;",
        "LX/1Pr;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1AV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705307
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2705308
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;->a:LX/1AV;

    .line 2705309
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;
    .locals 4

    .prologue
    .line 2705310
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;

    monitor-enter v1

    .line 2705311
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705312
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705313
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705314
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705315
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v3

    check-cast v3, LX/1AV;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;-><init>(LX/1AV;)V

    .line 2705316
    move-object v0, p0

    .line 2705317
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705318
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705319
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2705321
    check-cast p2, LX/JXo;

    check-cast p3, LX/1Pr;

    .line 2705322
    new-instance v0, LX/JXe;

    iget-object v1, p2, LX/JXo;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v0, v1}, LX/JXe;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    iget-object v1, p2, LX/JXo;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JXf;

    .line 2705323
    new-instance v1, LX/JXp;

    iget-object v2, p2, LX/JXo;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, LX/JXp;-><init>(Ljava/lang/String;LX/JXf;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x272442c2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2705324
    check-cast p2, LX/2oV;

    .line 2705325
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;->a:LX/1AV;

    invoke-virtual {v1, p4, p2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 2705326
    const/16 v1, 0x1f

    const v2, 0x661c6b36

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
