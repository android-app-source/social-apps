.class public Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JXq;",
        "LX/1PW;",
        "LX/JXn;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/2mn;

.field public final b:LX/0ad;

.field private final c:LX/0hB;


# direct methods
.method public constructor <init>(LX/2mn;LX/0ad;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705369
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2705370
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->a:LX/2mn;

    .line 2705371
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->b:LX/0ad;

    .line 2705372
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->c:LX/0hB;

    .line 2705373
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;
    .locals 6

    .prologue
    .line 2705358
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

    monitor-enter v1

    .line 2705359
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705360
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705361
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705362
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705363
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

    invoke-static {v0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v3

    check-cast v3, LX/2mn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;-><init>(LX/2mn;LX/0ad;LX/0hB;)V

    .line 2705364
    move-object v0, p0

    .line 2705365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2705374
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2705375
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2705376
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2705377
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2705378
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    move v1, v1

    .line 2705379
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->a:LX/2mn;

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v2}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    .line 2705380
    iget v0, v0, LX/3FO;->a:I

    .line 2705381
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->b:LX/0ad;

    sget-short v3, LX/JXv;->j:S

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 2705382
    if-eqz v2, :cond_0

    .line 2705383
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    .line 2705384
    :cond_0
    int-to-float v2, v0

    mul-float/2addr v2, v1

    float-to-int v2, v2

    move v1, v2

    .line 2705385
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v2, v2

    .line 2705386
    new-instance v3, LX/JXq;

    invoke-direct {v3, v0, v2, v0, v1}, LX/JXq;-><init>(IIII)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x70b2880

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2705347
    check-cast p2, LX/JXq;

    check-cast p4, LX/JXn;

    .line 2705348
    iget v4, p2, LX/JXq;->a:I

    iget v5, p2, LX/JXq;->b:I

    .line 2705349
    iget-object v6, p4, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 2705350
    if-nez v6, :cond_0

    .line 2705351
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2705352
    :cond_0
    iput v5, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2705353
    iput v4, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2705354
    iget-object v7, p4, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v7, v6}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2705355
    iget v4, p2, LX/JXq;->c:I

    iget v5, p2, LX/JXq;->d:I

    .line 2705356
    int-to-double v6, v4

    int-to-double v8, v5

    div-double/2addr v6, v8

    iput-wide v6, p4, LX/JXn;->j:D

    .line 2705357
    const/16 v1, 0x1f

    const v2, 0x726d9ec1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
