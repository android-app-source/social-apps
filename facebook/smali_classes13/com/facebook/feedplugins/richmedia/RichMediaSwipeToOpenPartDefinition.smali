.class public Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JXj;",
        "LX/JXf;",
        "TE;",
        "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2yr;

.field private final b:LX/17Y;

.field public final c:LX/0Zb;

.field public final d:LX/2yJ;

.field private final e:LX/0iQ;


# direct methods
.method public constructor <init>(LX/2yr;LX/17Y;LX/2yJ;LX/0Zb;LX/0iQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705133
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2705134
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->a:LX/2yr;

    .line 2705135
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->b:LX/17Y;

    .line 2705136
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->c:LX/0Zb;

    .line 2705137
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->d:LX/2yJ;

    .line 2705138
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->e:LX/0iQ;

    .line 2705139
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;
    .locals 9

    .prologue
    .line 2705140
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    monitor-enter v1

    .line 2705141
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705142
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705143
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705144
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705145
    new-instance v3, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    invoke-static {v0}, LX/2yr;->b(LX/0QB;)LX/2yr;

    move-result-object v4

    check-cast v4, LX/2yr;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v6

    check-cast v6, LX/2yJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0iQ;->a(LX/0QB;)LX/0iQ;

    move-result-object v8

    check-cast v8, LX/0iQ;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;-><init>(LX/2yr;LX/17Y;LX/2yJ;LX/0Zb;LX/0iQ;)V

    .line 2705146
    move-object v0, v3

    .line 2705147
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705148
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705149
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705150
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2705151
    check-cast p2, LX/JXj;

    check-cast p3, LX/1Pq;

    const/4 v2, 0x0

    .line 2705152
    iget-object v0, p2, LX/JXj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2705153
    iget-object v3, p2, LX/JXj;->b:Ljava/lang/String;

    .line 2705154
    if-eqz v3, :cond_0

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v2

    .line 2705155
    :goto_0
    return-object v1

    :cond_1
    move-object v0, p3

    .line 2705156
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2705157
    const-class v0, Landroid/app/Activity;

    invoke-static {v4, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2705158
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->a:LX/2yr;

    iget-object v6, p2, LX/JXj;->b:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2705159
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->b:LX/17Y;

    invoke-interface {v5, v4, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2705160
    check-cast p3, LX/1Pr;

    new-instance v4, LX/JXe;

    invoke-direct {v4, v1}, LX/JXe;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v4, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JXf;

    .line 2705161
    if-eqz v3, :cond_2

    if-nez v0, :cond_3

    :cond_2
    move-object v1, v2

    .line 2705162
    goto :goto_0

    .line 2705163
    :cond_3
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2705164
    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->e:LX/0iQ;

    invoke-virtual {v3, v0}, LX/0iQ;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    check-cast v0, LX/FAe;

    .line 2705165
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2705166
    invoke-virtual {v0, v2}, LX/FAe;->setArguments(Landroid/os/Bundle;)V

    .line 2705167
    invoke-virtual {v0}, LX/FAe;->k()V

    .line 2705168
    iput-object v0, v1, LX/JXf;->d:LX/FAe;

    .line 2705169
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6831620d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2705170
    check-cast p1, LX/JXj;

    check-cast p2, LX/JXf;

    check-cast p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    const/4 p3, 0x1

    .line 2705171
    if-nez p4, :cond_0

    .line 2705172
    :goto_0
    const/16 v1, 0x1f

    const v2, -0xdb80983

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2705173
    :cond_0
    iget-object v1, p2, LX/JXf;->d:LX/FAe;

    move-object v1, v1

    .line 2705174
    iput-object v1, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2705175
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->d:LX/2yJ;

    invoke-virtual {v1}, LX/2yJ;->a()LX/2yN;

    move-result-object v1

    iget-object v2, p1, LX/JXj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {v1, v2, p4}, LX/2yN;->a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2705176
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "rich_media_swipe_cancel"

    invoke-direct {v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2705177
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "rich_media_swipe_open"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2705178
    const-string v5, "tracking"

    iget-object v6, p1, LX/JXj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v6}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2705179
    invoke-virtual {v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2705180
    new-instance v5, LX/JXi;

    invoke-direct {v5, p0, v4, v1, v2}, LX/JXi;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2705181
    iput-object v5, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    .line 2705182
    invoke-virtual {p4}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->j()V

    .line 2705183
    iput-boolean p3, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    .line 2705184
    iget v1, p1, LX/JXj;->c:I

    .line 2705185
    iput v1, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->w:I

    .line 2705186
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2705187
    check-cast p2, LX/JXf;

    check-cast p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    .line 2705188
    if-nez p4, :cond_0

    .line 2705189
    :goto_0
    return-void

    .line 2705190
    :cond_0
    const/4 p1, 0x0

    .line 2705191
    iget-object v0, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    .line 2705192
    iput-object p1, v0, LX/1wz;->q:LX/4Ba;

    .line 2705193
    iget-object v0, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    .line 2705194
    iput-object p1, v0, LX/1wz;->r:LX/39D;

    .line 2705195
    iget-object v0, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    .line 2705196
    iput-object p1, v0, LX/1wz;->s:LX/39E;

    .line 2705197
    const/4 v0, 0x0

    .line 2705198
    iput-boolean v0, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    .line 2705199
    const/4 v0, 0x0

    .line 2705200
    iput-object v0, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2705201
    iget-object v0, p2, LX/JXf;->d:LX/FAe;

    move-object v0, v0

    .line 2705202
    invoke-virtual {v0}, LX/FAe;->j()V

    .line 2705203
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->e:LX/0iQ;

    invoke-virtual {v0}, LX/0iQ;->a()V

    .line 2705204
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->a:LX/2yr;

    .line 2705205
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    if-eqz p0, :cond_3

    .line 2705206
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 2705207
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    if-eqz p1, :cond_1

    .line 2705208
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    invoke-interface {p1}, LX/1ca;->g()Z

    .line 2705209
    :cond_1
    iget-object p1, p0, LX/CHQ;->d:LX/8Ys;

    if-eqz p1, :cond_2

    .line 2705210
    iget-object p1, p0, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object p1, p1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    iget-object p2, p0, LX/CHQ;->d:LX/8Ys;

    invoke-interface {p2}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/8bW;->a(Ljava/lang/String;)V

    .line 2705211
    :cond_2
    const/4 p0, 0x0

    iput-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 2705212
    :goto_1
    goto :goto_0

    :cond_3
    goto :goto_1
.end method
