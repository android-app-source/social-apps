.class public final Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JXd;


# direct methods
.method public constructor <init>(LX/JXd;)V
    .locals 0

    .prologue
    .line 2705020
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;->a:LX/JXd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2705021
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;->a:LX/JXd;

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;->a:LX/JXd;

    iget-object v1, v1, LX/JXd;->a:Landroid/view/View;

    invoke-static {v0, v1}, LX/JXd;->b(LX/JXd;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705022
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "rich_media_lightning_bolt_animation_half_done"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;->a:LX/JXd;

    iget-object v2, v2, LX/JXd;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2705023
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;->a:LX/JXd;

    iget-object v1, v1, LX/JXd;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2705024
    :cond_0
    return-void
.end method
