.class public Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1PV;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/JXT;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JXd;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:LX/0ad;

.field private final c:LX/0Zb;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/0Zb;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705091
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2705092
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2705093
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->b:LX/0ad;

    .line 2705094
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->c:LX/0Zb;

    .line 2705095
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;
    .locals 6

    .prologue
    .line 2705080
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;

    monitor-enter v1

    .line 2705081
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705082
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705083
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705084
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705085
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/0Zb;)V

    .line 2705086
    move-object v0, p0

    .line 2705087
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705088
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705089
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705090
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2705096
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/16 v3, 0x3e8

    .line 2705097
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2705098
    if-nez v0, :cond_0

    .line 2705099
    const/4 v0, 0x0

    .line 2705100
    :goto_0
    return-object v0

    .line 2705101
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->b:LX/0ad;

    sget v1, LX/JXv;->f:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v2

    .line 2705102
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->b:LX/0ad;

    sget v1, LX/JXv;->p:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v3

    .line 2705103
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->b:LX/0ad;

    sget v1, LX/JXv;->o:F

    const v4, 0x3f4ccccd    # 0.8f

    invoke-interface {v0, v1, v4}, LX/0ad;->a(FF)F

    move-result v4

    .line 2705104
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->b:LX/0ad;

    sget-char v1, LX/JXv;->n:C

    const-string v5, "https://lookaside.facebook.com/assets/126637881080823/"

    invoke-interface {v0, v1, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2705105
    new-instance v0, LX/JXd;

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v6, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->c:LX/0Zb;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/JXd;-><init>(Ljava/util/concurrent/ScheduledExecutorService;IIFLjava/lang/String;LX/0Zb;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x46cc5f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2705069
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/JXd;

    check-cast p3, LX/1Pr;

    .line 2705070
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2705071
    if-nez v1, :cond_0

    .line 2705072
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x7b75906e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2705073
    :cond_0
    new-instance v2, LX/JXe;

    invoke-direct {v2, v1}, LX/JXe;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JXf;

    .line 2705074
    iget-boolean v2, v1, LX/JXf;->c:Z

    move v2, v2

    .line 2705075
    if-eqz v2, :cond_1

    .line 2705076
    iput-object p4, p2, LX/JXd;->a:Landroid/view/View;

    .line 2705077
    iput-object v1, p2, LX/JXd;->b:LX/JXf;

    .line 2705078
    check-cast p3, LX/1PV;

    invoke-interface {p3, p2}, LX/1PV;->a(LX/5Oj;)V

    goto :goto_0

    .line 2705079
    :cond_1
    check-cast p4, LX/JXT;

    invoke-interface {p4}, LX/JXT;->h()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2705060
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/JXd;

    check-cast p3, LX/1Pr;

    .line 2705061
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2705062
    if-nez v0, :cond_0

    .line 2705063
    :goto_0
    return-void

    .line 2705064
    :cond_0
    invoke-virtual {p2}, LX/JXd;->b()V

    .line 2705065
    check-cast p3, LX/1PV;

    invoke-interface {p3, p2}, LX/1PV;->b(LX/5Oj;)V

    .line 2705066
    const/4 v0, 0x0

    .line 2705067
    iput-object v0, p2, LX/JXd;->a:Landroid/view/View;

    .line 2705068
    check-cast p4, LX/JXT;

    invoke-interface {p4}, LX/JXT;->h()V

    goto :goto_0
.end method
