.class public Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JXt;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/13l;

.field private final c:LX/1VK;

.field public final d:LX/17Y;

.field public final e:LX/0Zb;

.field private final f:LX/2yJ;

.field public final g:LX/2mt;

.field public final h:LX/2yr;


# direct methods
.method public constructor <init>(LX/13l;LX/1VK;LX/17Y;LX/2mt;LX/2yJ;LX/0Zb;LX/0Ot;LX/2yr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/13l;",
            "LX/1VK;",
            "LX/17Y;",
            "LX/2mt;",
            "LX/2yJ;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/2yr;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705523
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2705524
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->b:LX/13l;

    .line 2705525
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->c:LX/1VK;

    .line 2705526
    iput-object p7, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->a:LX/0Ot;

    .line 2705527
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->d:LX/17Y;

    .line 2705528
    iput-object p6, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->e:LX/0Zb;

    .line 2705529
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->f:LX/2yJ;

    .line 2705530
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->g:LX/2mt;

    .line 2705531
    iput-object p8, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->h:LX/2yr;

    .line 2705532
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/2oL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/2oL;"
        }
    .end annotation

    .prologue
    .line 2705512
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2705513
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2705514
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2705515
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 2705516
    if-nez v1, :cond_0

    .line 2705517
    const/4 v0, 0x0

    .line 2705518
    :goto_0
    return-object v0

    .line 2705519
    :cond_0
    new-instance v2, LX/2oK;

    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->c:LX/1VK;

    invoke-direct {v2, v1, v0, v3}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 2705520
    check-cast p2, LX/1Pr;

    .line 2705521
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2705522
    check-cast v0, LX/0jW;

    invoke-interface {p2, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oL;

    goto :goto_0
.end method

.method private a(LX/1aD;LX/JXt;LX/1Po;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/JXt;",
            "TE;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 2705496
    iget-object v0, p2, LX/JXt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2705497
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2705498
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2705499
    if-nez v2, :cond_1

    .line 2705500
    :cond_0
    :goto_0
    return-object v10

    .line 2705501
    :cond_1
    iget-object v1, p2, LX/JXt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2705502
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 2705503
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 2705504
    if-eqz v1, :cond_2

    .line 2705505
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 2705506
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 2705507
    iget-object v0, p2, LX/JXt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p0, v0, p3}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->a(Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/2oL;

    move-result-object v5

    .line 2705508
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->g:LX/2mt;

    iget-object v1, p2, LX/JXt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v7

    .line 2705509
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, LX/1Nt;

    new-instance v11, LX/3FZ;

    new-instance v0, LX/JXs;

    iget-object v3, p2, LX/JXt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v1}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v4

    iget-object v6, p2, LX/JXt;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v8, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->f:LX/2yJ;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, LX/JXs;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;LX/2oL;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;LX/2yJ;)V

    invoke-direct {v11, v0}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v9, v11}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object v1, v10

    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;
    .locals 9

    .prologue
    .line 2705510
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    invoke-static {p0}, LX/13l;->a(LX/0QB;)LX/13l;

    move-result-object v1

    check-cast v1, LX/13l;

    const-class v2, LX/1VK;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1VK;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-static {p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v4

    check-cast v4, LX/2mt;

    invoke-static {p0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v5

    check-cast v5, LX/2yJ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const/16 v7, 0x848

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/2yr;->b(LX/0QB;)LX/2yr;

    move-result-object v8

    check-cast v8, LX/2yr;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;-><init>(LX/13l;LX/1VK;LX/17Y;LX/2mt;LX/2yJ;LX/0Zb;LX/0Ot;LX/2yr;)V

    .line 2705511
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2705495
    check-cast p2, LX/JXt;

    check-cast p3, LX/1Po;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->a(LX/1aD;LX/JXt;LX/1Po;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x172eebb9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2705481
    check-cast p1, LX/JXt;

    .line 2705482
    if-nez p4, :cond_0

    .line 2705483
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x5ecab747

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2705484
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->g:LX/2mt;

    iget-object v2, p1, LX/JXt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    .line 2705485
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->h:LX/2yr;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {v2, p2, v1}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2705486
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->h:LX/2yr;

    .line 2705487
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    if-eqz p0, :cond_2

    .line 2705488
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 2705489
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    if-eqz p1, :cond_0

    .line 2705490
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    invoke-interface {p1}, LX/1ca;->g()Z

    .line 2705491
    :cond_0
    iget-object p1, p0, LX/CHQ;->d:LX/8Ys;

    if-eqz p1, :cond_1

    .line 2705492
    iget-object p1, p0, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object p1, p1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    iget-object p2, p0, LX/CHQ;->d:LX/8Ys;

    invoke-interface {p2}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/8bW;->a(Ljava/lang/String;)V

    .line 2705493
    :cond_1
    const/4 p0, 0x0

    iput-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 2705494
    :goto_0
    return-void

    :cond_2
    goto :goto_0
.end method
