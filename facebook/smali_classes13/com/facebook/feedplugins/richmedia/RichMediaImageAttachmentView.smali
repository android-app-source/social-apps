.class public Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/35q;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/4Ba;
.implements LX/39D;
.implements LX/39E;
.implements LX/0hY;
.implements LX/JXQ;
.implements LX/JXR;
.implements LX/JXS;
.implements LX/JXT;
.implements LX/JXU;


# static fields
.field private static final e:LX/0wT;


# instance fields
.field private A:LX/0f8;

.field private final B:Ljava/lang/Runnable;

.field private final C:Ljava/lang/Runnable;

.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1wz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:F

.field private final g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field public final h:Landroid/widget/TextView;

.field public final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field public k:LX/0wd;

.field public final l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final m:Landroid/widget/ImageView;

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/widget/ImageView;

.field private final p:Landroid/widget/ImageView;

.field private final q:Landroid/widget/ImageView;

.field public r:LX/68u;

.field public s:Landroid/animation/ValueAnimator;

.field public t:Landroid/view/View;

.field private u:Landroid/view/View;

.field public v:LX/FAe;

.field public w:I

.field public x:Z

.field public y:Z

.field public z:LX/JXi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2704906
    const-wide v0, 0x4071800000000000L    # 280.0

    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->e:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2704907
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2704908
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2704909
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2704910
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2704911
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2704912
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView$1;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->B:Ljava/lang/Runnable;

    .line 2704913
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView$2;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView$2;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->C:Ljava/lang/Runnable;

    .line 2704914
    const-class v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2704915
    const v0, 0x7f0311ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2704916
    const v0, 0x7f0d2a1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 2704917
    const v0, 0x7f0d2a25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->h:Landroid/widget/TextView;

    .line 2704918
    const v0, 0x7f0d2a24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->i:Landroid/widget/TextView;

    .line 2704919
    const v0, 0x7f0d2a1d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2704920
    const v0, 0x7f0d2a1e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    .line 2704921
    const v0, 0x7f0d1d07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->n:Landroid/widget/ImageView;

    .line 2704922
    const v0, 0x7f0d2a20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->o:Landroid/widget/ImageView;

    .line 2704923
    const v0, 0x7f0d2a22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->j:Landroid/widget/TextView;

    .line 2704924
    const v0, 0x7f0d2a23

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->p:Landroid/widget/ImageView;

    .line 2704925
    const v0, 0x7f0d2a1f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->q:Landroid/widget/ImageView;

    .line 2704926
    const v0, 0x7f0d2a21

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->u:Landroid/view/View;

    .line 2704927
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->A:LX/0f8;

    .line 2704928
    invoke-direct {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->l()V

    .line 2704929
    return-void
.end method

.method private a(F)V
    .locals 6

    .prologue
    .line 2704930
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v1

    .line 2704931
    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->x:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 2704932
    :goto_0
    iget-boolean v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->x:Z

    if-eqz v2, :cond_3

    .line 2704933
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    invoke-virtual {v2}, LX/FAe;->d()V

    .line 2704934
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    if-eqz v2, :cond_0

    .line 2704935
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    .line 2704936
    iget-object v3, v2, LX/JXi;->d:Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->c:LX/0Zb;

    iget-object v4, v2, LX/JXi;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2704937
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->A:LX/0f8;

    if-eqz v2, :cond_1

    .line 2704938
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->A:LX/0f8;

    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    invoke-interface {v2, v3}, LX/0f8;->a(LX/0hE;)V

    .line 2704939
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->k:LX/0wd;

    float-to-double v4, p1

    invoke-virtual {v2, v4, v5}, LX/0wd;->c(D)LX/0wd;

    .line 2704940
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->k:LX/0wd;

    new-instance v3, LX/JXc;

    invoke-direct {v3, p0}, LX/JXc;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2704941
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->k:LX/0wd;

    float-to-double v4, v1

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 2704942
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->k:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2704943
    return-void

    .line 2704944
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2704945
    :cond_3
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    if-eqz v2, :cond_1

    .line 2704946
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    .line 2704947
    iget-object v3, v2, LX/JXi;->d:Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->c:LX/0Zb;

    iget-object v4, v2, LX/JXi;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2704948
    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object v2

    check-cast v2, LX/1wz;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a:LX/1Ad;

    iput-object v2, p1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    iput-object v3, p1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->c:LX/0wW;

    iput-object p0, p1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->d:LX/0ad;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;ZFLandroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 2704949
    const/high16 v0, -0x40800000    # -1.0f

    mul-float/2addr v0, p2

    .line 2704950
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 2704951
    const/4 v2, 0x0

    div-float/2addr v0, v1

    sub-float v0, v3, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2704952
    if-eqz p1, :cond_0

    .line 2704953
    :goto_0
    invoke-virtual {p3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2704954
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->requestLayout()V

    .line 2704955
    return-void

    .line 2704956
    :cond_0
    sub-float v0, v3, v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;F)V
    .locals 2

    .prologue
    .line 2704957
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setX(F)V

    .line 2704958
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    if-eqz v0, :cond_0

    .line 2704959
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2704960
    iget-object v1, v0, LX/FAe;->a:Landroid/view/View;

    move-object v0, v1

    .line 2704961
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 2704962
    :cond_0
    return-void
.end method

.method private l()V
    .locals 8

    .prologue
    .line 2704982
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    sget-object v1, LX/31M;->LEFT:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    .line 2704983
    iput v1, v0, LX/1wz;->p:I

    .line 2704984
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->j()V

    .line 2704985
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->s:Landroid/animation/ValueAnimator;

    .line 2704986
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->s:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2704987
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->s:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2704988
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    .line 2704989
    const/4 v4, 0x1

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 2704990
    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v5, v6}, LX/68u;->a(FF)LX/68u;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    .line 2704991
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    new-instance v6, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v6}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v5, v6}, LX/68u;->a(Landroid/view/animation/Interpolator;)V

    .line 2704992
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    const/4 v6, -0x1

    .line 2704993
    iput v6, v5, LX/68u;->x:I

    .line 2704994
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    const-wide/16 v6, 0x5dc

    invoke-virtual {v5, v6, v7}, LX/68u;->a(J)LX/68u;

    .line 2704995
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    iget-object v6, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    .line 2704996
    iput-object v6, v5, LX/68u;->A:Ljava/lang/Object;

    .line 2704997
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    new-instance v6, LX/JXb;

    invoke-direct {v6, p0, v4}, LX/JXb;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;I)V

    invoke-virtual {v5, v6}, LX/68u;->a(LX/67q;)V

    .line 2704998
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->d:LX/0ad;

    sget-short v1, LX/JXv;->t:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2704999
    if-nez v0, :cond_0

    .line 2705000
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->i:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2705001
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->d:LX/0ad;

    sget-short v1, LX/JXv;->h:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2705002
    if-nez v0, :cond_1

    .line 2705003
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2705004
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->c:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->e:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide v2, 0x4082c00000000000L    # 600.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 2705005
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2705006
    move-object v0, v0

    .line 2705007
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->k:LX/0wd;

    .line 2705008
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->s:Landroid/animation/ValueAnimator;

    new-instance v1, LX/JXZ;

    invoke-direct {v1, p0}, LX/JXZ;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2705009
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->s:Landroid/animation/ValueAnimator;

    new-instance v1, LX/JXa;

    invoke-direct {v1, p0}, LX/JXa;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2705010
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2705011
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public final J_(I)V
    .locals 4

    .prologue
    .line 2704963
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->B:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2704964
    return-void
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2704890
    const/high16 v0, 0x44160000    # 600.0f

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a(F)V

    .line 2704891
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2704965
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a:LX/1Ad;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2704966
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2704967
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->C:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2704968
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 1

    .prologue
    .line 2704969
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a(F)V

    .line 2704970
    return-void
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 2704971
    const/4 v0, 0x1

    return v0
.end method

.method public final a(FFLX/31M;)Z
    .locals 2

    .prologue
    .line 2704972
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2704973
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    invoke-virtual {v0}, LX/FAe;->c()V

    .line 2704974
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    invoke-virtual {v0}, LX/FAe;->l()V

    .line 2704975
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2704976
    iget-object v1, v0, LX/FAe;->a:Landroid/view/View;

    move-object v0, v1

    .line 2704977
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 2704978
    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    return v0
.end method

.method public final a(LX/31M;II)Z
    .locals 1

    .prologue
    .line 2704979
    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2704980
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2704892
    sget-object v0, LX/31M;->LEFT:LX/31M;

    if-ne p3, v0, :cond_2

    .line 2704893
    iget v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 2704894
    iget v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    .line 2704895
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->w:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->x:Z

    if-nez v0, :cond_1

    .line 2704896
    iput-boolean v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->x:Z

    .line 2704897
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    if-eqz v0, :cond_1

    .line 2704898
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->z:LX/JXi;

    .line 2704899
    iget-object v1, v0, LX/JXi;->d:Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->c:LX/0Zb;

    iget-object p1, v0, LX/JXi;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v1, p1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2704900
    :cond_1
    iget v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    invoke-static {p0, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;F)V

    .line 2704901
    iget v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a$redex0(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;ZFLandroid/view/View;)V

    .line 2704902
    const/4 v0, 0x0

    iget v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->f:F

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2704903
    iget-object p1, v2, LX/FAe;->a:Landroid/view/View;

    move-object v2, p1

    .line 2704904
    invoke-static {p0, v0, v1, v2}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a$redex0(Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;ZFLandroid/view/View;)V

    .line 2704905
    :cond_2
    return-void
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 2704981
    const/4 v0, 0x0

    return v0
.end method

.method public final c(FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2704812
    iput-boolean v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->x:Z

    .line 2704813
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    invoke-virtual {v0}, LX/FAe;->c()V

    .line 2704814
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    invoke-virtual {v0}, LX/FAe;->l()V

    .line 2704815
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->v:LX/FAe;

    .line 2704816
    iget-object v1, v0, LX/FAe;->a:Landroid/view/View;

    move-object v0, v1

    .line 2704817
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 2704818
    const/high16 v0, 0x44160000    # 600.0f

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->a(F)V

    .line 2704819
    return v2
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2704820
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 2704821
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    .line 2704822
    iget-object v1, v0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 2704823
    iget-object v1, v0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2704824
    const/4 v1, 0x0

    iput-object v1, v0, LX/68u;->D:Ljava/util/ArrayList;

    .line 2704825
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704826
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704827
    :cond_1
    return-void
.end method

.method public final d(FF)V
    .locals 0

    .prologue
    .line 2704828
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2704829
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 2704830
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704831
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->r:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 2704832
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704833
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2704834
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704835
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2704836
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2704837
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704838
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704839
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704840
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020c0f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2704841
    if-eqz v0, :cond_0

    .line 2704842
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0990

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2704843
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->p:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2704844
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0991

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2704845
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->i:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2704846
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2704847
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704848
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2704849
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2704850
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704851
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704852
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2704853
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->i:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2704854
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2704855
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->C:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2704856
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2704857
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 2704858
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2704859
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2704805
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    .line 2704806
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 2704807
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    .line 2704808
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 2704809
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    .line 2704810
    iput-object p0, v0, LX/1wz;->s:LX/39E;

    .line 2704811
    return-void
.end method

.method public final nk_()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2704860
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->setPadding(IIII)V

    .line 2704861
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2704862
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2704863
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2704864
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 2704865
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 2704866
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 2704867
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2704868
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7ecf7660

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2704869
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 2704870
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    .line 2704871
    const/16 v0, 0x2d

    const v2, 0x2d37e68d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4d705ab3    # 2.52029744E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704872
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 2704873
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->t:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setX(F)V

    .line 2704874
    const/16 v1, 0x2d

    const v2, -0x5725cf9c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2704875
    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    if-eqz v0, :cond_0

    .line 2704876
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2704877
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x55a7ddb9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2704878
    iget-boolean v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->y:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 2704879
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x2b5be654

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2704880
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x18826013

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 2704881
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2704882
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2704883
    :cond_0
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 2704884
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2704885
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2704886
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->setVisibility(I)V

    .line 2704887
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->g:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2704888
    return-void

    .line 2704889
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
