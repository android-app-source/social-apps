.class public final Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JXd;


# direct methods
.method public constructor <init>(LX/JXd;)V
    .locals 0

    .prologue
    .line 2705012
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2705013
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v0, v0, LX/JXd;->a:Landroid/view/View;

    check-cast v0, LX/JXT;

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget v1, v1, LX/JXd;->h:I

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v2, v2, LX/JXd;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/JXT;->a(ILjava/lang/String;)V

    .line 2705014
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "rich_media_lightning_bolt_animation_start"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v2, v2, LX/JXd;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2705015
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v1, v1, LX/JXd;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2705016
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v0, v0, LX/JXd;->g:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v1, v1, LX/JXd;->f:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget v2, v2, LX/JXd;->h:I

    div-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2705017
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;->a:LX/JXd;

    iget-object v0, v0, LX/JXd;->b:LX/JXf;

    const/4 v1, 0x0

    .line 2705018
    iput-boolean v1, v0, LX/JXf;->c:Z

    .line 2705019
    return-void
.end method
