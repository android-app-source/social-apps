.class public Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/JXQ;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704514
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2704515
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2704516
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;
    .locals 4

    .prologue
    .line 2704517
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    monitor-enter v1

    .line 2704518
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704519
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704520
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704521
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2704522
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2704523
    move-object v0, p0

    .line 2704524
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704525
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704526
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704527
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2704507
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2704508
    const v0, 0x7f020a43

    .line 2704509
    sget-object v1, LX/1Ua;->e:LX/1Ua;

    .line 2704510
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    const/4 v5, -0x1

    invoke-direct {v3, v4, v1, v0, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2704511
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x227089c5    # 3.2599E-18f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704512
    check-cast p4, LX/JXQ;

    invoke-interface {p4}, LX/JXQ;->nk_()V

    .line 2704513
    const/16 v1, 0x1f

    const v2, 0x599bf57e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
