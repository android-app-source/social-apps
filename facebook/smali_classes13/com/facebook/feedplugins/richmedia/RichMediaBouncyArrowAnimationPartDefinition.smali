.class public Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/JXR;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2704554
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2704555
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;
    .locals 3

    .prologue
    .line 2704556
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;

    monitor-enter v1

    .line 2704557
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2704558
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2704559
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2704560
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2704561
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;-><init>()V

    .line 2704562
    move-object v0, v0

    .line 2704563
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2704564
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2704565
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2704566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x268de243

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2704567
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2704568
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2704569
    if-nez v1, :cond_0

    .line 2704570
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x17d45a55

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2704571
    :cond_0
    check-cast p4, LX/JXR;

    invoke-interface {p4}, LX/JXR;->e()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2704572
    if-nez p4, :cond_0

    .line 2704573
    :goto_0
    return-void

    .line 2704574
    :cond_0
    check-cast p4, LX/JXR;

    invoke-interface {p4}, LX/JXR;->d()V

    goto :goto_0
.end method
