.class public Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/JXU;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705236
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2705237
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;
    .locals 3

    .prologue
    .line 2705213
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    monitor-enter v1

    .line 2705214
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705215
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705216
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705217
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2705218
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;-><init>()V

    .line 2705219
    move-object v0, v0

    .line 2705220
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705221
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705222
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x66181dbf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2705224
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 2705225
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2705226
    if-nez v1, :cond_0

    .line 2705227
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x5b0273c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2705228
    :cond_0
    new-instance v2, LX/JXe;

    invoke-direct {v2, v1}, LX/JXe;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JXf;

    .line 2705229
    iget-boolean v2, v1, LX/JXf;->b:Z

    move v2, v2

    .line 2705230
    if-eqz v2, :cond_1

    .line 2705231
    check-cast p4, LX/JXU;

    const/16 v2, 0xbb8

    invoke-interface {p4, v2}, LX/JXU;->J_(I)V

    .line 2705232
    const/4 v2, 0x0

    .line 2705233
    iput-boolean v2, v1, LX/JXf;->b:Z

    .line 2705234
    goto :goto_0

    .line 2705235
    :cond_1
    check-cast p4, LX/JXU;

    invoke-interface {p4}, LX/JXU;->i()V

    goto :goto_0
.end method
