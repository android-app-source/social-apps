.class public Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

.field private final b:LX/JVy;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;LX/JVy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702419
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2702420
    iput-object p1, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->a:Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    .line 2702421
    iput-object p2, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->b:LX/JVy;

    .line 2702422
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;
    .locals 5

    .prologue
    .line 2702423
    const-class v1, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;

    monitor-enter v1

    .line 2702424
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702425
    sput-object v2, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702426
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702427
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702428
    new-instance p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    invoke-static {v0}, LX/JVy;->a(LX/0QB;)LX/JVy;

    move-result-object v4

    check-cast v4, LX/JVy;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;LX/JVy;)V

    .line 2702429
    move-object v0, p0

    .line 2702430
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702431
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702432
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2702434
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702435
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->a:Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->b:LX/JVy;

    invoke-virtual {v1, p2}, LX/JVy;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702436
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702437
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702438
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;->b:LX/JVy;

    invoke-virtual {v0, p1}, LX/JVy;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2702439
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
