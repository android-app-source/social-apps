.class public Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pu;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final d:LX/Gf3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gf3",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;

.field private final f:LX/JVy;

.field private final g:LX/03R;

.field private final h:LX/1DR;

.field private final i:LX/0ad;

.field private j:LX/1Ua;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Gf3;LX/1V0;LX/1V7;LX/JVy;LX/1DR;LX/0Uh;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702475
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2702476
    iput-object p2, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->d:LX/Gf3;

    .line 2702477
    iput-object p3, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->e:LX/1V0;

    .line 2702478
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {p4}, LX/1V7;->d()F

    move-result v1

    .line 2702479
    iput v1, v0, LX/1UY;->b:F

    .line 2702480
    move-object v0, v0

    .line 2702481
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->j:LX/1Ua;

    .line 2702482
    iput-object p5, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->f:LX/JVy;

    .line 2702483
    iput-object p6, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->h:LX/1DR;

    .line 2702484
    const/16 v0, 0x25b

    invoke-virtual {p7, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->g:LX/03R;

    .line 2702485
    iput-object p8, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->i:LX/0ad;

    .line 2702486
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pu;)LX/1X1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2702462
    new-instance v1, LX/1X6;

    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->j:LX/1Ua;

    invoke-direct {v1, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2702463
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->f:LX/JVy;

    invoke-virtual {v0, p2}, LX/JVy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2702464
    new-instance v3, LX/2dx;

    invoke-direct {v3}, LX/2dx;-><init>()V

    .line 2702465
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->i:LX/0ad;

    sget-short v4, LX/JVz;->a:S

    invoke-interface {v0, v4, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    move-object v0, p3

    .line 2702466
    check-cast v0, LX/1Pt;

    invoke-interface {v0}, LX/1Pt;->m()LX/1f9;

    move-result-object v5

    .line 2702467
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->d:LX/Gf3;

    invoke-virtual {v0, p1}, LX/Gf3;->c(LX/1De;)LX/Gf1;

    move-result-object v6

    .line 2702468
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702469
    check-cast v0, LX/25E;

    invoke-virtual {v6, v0}, LX/Gf1;->a(LX/25E;)LX/Gf1;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/Gf1;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Gf1;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/Gf1;->a(LX/1f9;)LX/Gf1;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->h:LX/1DR;

    invoke-virtual {v2}, LX/1DR;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LX/Gf1;->h(I)LX/Gf1;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/Gf1;->a(LX/2dx;)LX/Gf1;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/Gf1;->a(Z)LX/Gf1;

    move-result-object v0

    .line 2702470
    iget-object v2, v0, LX/Gf1;->a:LX/Gf2;

    iput-boolean v4, v2, LX/Gf2;->h:Z

    .line 2702471
    move-object v2, v0

    .line 2702472
    move-object v0, p3

    check-cast v0, LX/1Pp;

    invoke-virtual {v2, v0}, LX/Gf1;->a(LX/1Pp;)LX/Gf1;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2702473
    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2702474
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;
    .locals 12

    .prologue
    .line 2702451
    const-class v1, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;

    monitor-enter v1

    .line 2702452
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702453
    sput-object v2, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702454
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702455
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702456
    new-instance v3, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/Gf3;->a(LX/0QB;)LX/Gf3;

    move-result-object v5

    check-cast v5, LX/Gf3;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v7

    check-cast v7, LX/1V7;

    invoke-static {v0}, LX/JVy;->a(LX/0QB;)LX/JVy;

    move-result-object v8

    check-cast v8, LX/JVy;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v9

    check-cast v9, LX/1DR;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;-><init>(Landroid/content/Context;LX/Gf3;LX/1V0;LX/1V7;LX/JVy;LX/1DR;LX/0Uh;LX/0ad;)V

    .line 2702457
    move-object v0, v3

    .line 2702458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2702450
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pu;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pu;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2702449
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pu;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pu;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2702440
    const/4 v0, 0x0

    move v0, v0

    .line 2702441
    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2702447
    const/4 v0, 0x0

    move v0, v0

    .line 2702448
    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2702444
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 2702445
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->f:LX/JVy;

    invoke-virtual {v1, p1}, LX/JVy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2702446
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->g:LX/03R;

    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;->g:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2702442
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702443
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
