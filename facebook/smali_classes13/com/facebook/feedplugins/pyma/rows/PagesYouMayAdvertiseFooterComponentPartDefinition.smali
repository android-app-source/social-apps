.class public Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final d:LX/JW3;

.field public final e:LX/1V0;

.field public final f:LX/3j4;

.field private final g:LX/JVx;

.field private final h:LX/JVy;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JW3;LX/1V0;LX/3j4;LX/JVx;LX/JVy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702127
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2702128
    iput-object p2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->d:LX/JW3;

    .line 2702129
    iput-object p3, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->e:LX/1V0;

    .line 2702130
    iput-object p4, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->f:LX/3j4;

    .line 2702131
    iput-object p5, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->g:LX/JVx;

    .line 2702132
    iput-object p6, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->h:LX/JVy;

    .line 2702133
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2702099
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702100
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2702101
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->f:LX/3j4;

    .line 2702102
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702103
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object p1

    check-cast p3, LX/1Pq;

    .line 2702104
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702105
    check-cast v0, LX/0jW;

    invoke-virtual {v1, p1, p3, v0, p2}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    move-object v0, v0

    .line 2702106
    :goto_0
    return-object v0

    .line 2702107
    :cond_0
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->a:LX/1Ua;

    sget-object v2, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-direct {v0, p2, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2702108
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->d:LX/JW3;

    const/4 v2, 0x0

    .line 2702109
    new-instance v3, LX/JW2;

    invoke-direct {v3, v1}, LX/JW2;-><init>(LX/JW3;)V

    .line 2702110
    iget-object v4, v1, LX/JW3;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JW1;

    .line 2702111
    if-nez v4, :cond_1

    .line 2702112
    new-instance v4, LX/JW1;

    invoke-direct {v4, v1}, LX/JW1;-><init>(LX/JW3;)V

    .line 2702113
    :cond_1
    invoke-static {v4, p1, v2, v2, v3}, LX/JW1;->a$redex0(LX/JW1;LX/1De;IILX/JW2;)V

    .line 2702114
    move-object v3, v4

    .line 2702115
    move-object v2, v3

    .line 2702116
    move-object v1, v2

    .line 2702117
    iget-object v2, v1, LX/JW1;->a:LX/JW2;

    iput-object p3, v2, LX/JW2;->a:LX/1Pn;

    .line 2702118
    iget-object v2, v1, LX/JW1;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2702119
    move-object v1, v1

    .line 2702120
    iget-object v2, v1, LX/JW1;->a:LX/JW2;

    iput-object p2, v2, LX/JW2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702121
    iget-object v2, v1, LX/JW1;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2702122
    move-object v1, v1

    .line 2702123
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2702124
    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2702125
    move-object v0, v0

    .line 2702126
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;
    .locals 10

    .prologue
    .line 2702146
    const-class v1, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    monitor-enter v1

    .line 2702147
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702148
    sput-object v2, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702149
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702150
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702151
    new-instance v3, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/JW3;->a(LX/0QB;)LX/JW3;

    move-result-object v5

    check-cast v5, LX/JW3;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v7

    check-cast v7, LX/3j4;

    invoke-static {v0}, LX/JVx;->a(LX/0QB;)LX/JVx;

    move-result-object v8

    check-cast v8, LX/JVx;

    invoke-static {v0}, LX/JVy;->a(LX/0QB;)LX/JVy;

    move-result-object v9

    check-cast v9, LX/JVy;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/JW3;LX/1V0;LX/3j4;LX/JVx;LX/JVy;)V

    .line 2702152
    move-object v0, v3

    .line 2702153
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702154
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702155
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2702144
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2702145
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2702135
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702136
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2702137
    invoke-static {p1}, LX/JVy;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2702138
    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->g:LX/JVx;

    const-string v3, "PYMA footer has NULL ActionLink"

    invoke-virtual {v2, v0, v3}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    move v0, v1

    .line 2702139
    :goto_0
    return v0

    .line 2702140
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2702141
    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->g:LX/JVx;

    const-string v3, "PYMA footer has NULL title"

    invoke-virtual {v2, v0, v3}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    move v0, v1

    .line 2702142
    goto :goto_0

    .line 2702143
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702134
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
