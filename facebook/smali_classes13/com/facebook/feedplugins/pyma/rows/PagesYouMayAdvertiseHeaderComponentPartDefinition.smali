.class public Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements LX/1XH;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;TE;>;",
        "LX/1XH;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final d:LX/JW7;

.field public final e:LX/1V0;

.field public final f:LX/3j4;

.field private final g:LX/JVx;

.field private final h:LX/JVy;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JW7;LX/1V0;LX/3j4;LX/JVx;LX/JVy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702157
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2702158
    iput-object p2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->d:LX/JW7;

    .line 2702159
    iput-object p3, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2702160
    iput-object p4, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->f:LX/3j4;

    .line 2702161
    iput-object p5, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->g:LX/JVx;

    .line 2702162
    iput-object p6, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->h:LX/JVy;

    .line 2702163
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2702198
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702199
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2702200
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->f:LX/3j4;

    .line 2702201
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702202
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object p1

    check-cast p3, LX/1Pq;

    .line 2702203
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702204
    check-cast v0, LX/0jW;

    invoke-virtual {v1, p1, p3, v0, p2}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    move-object v0, v0

    .line 2702205
    :goto_0
    return-object v0

    .line 2702206
    :cond_0
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->i:LX/1Ua;

    sget-object v2, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    invoke-direct {v0, p2, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2702207
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->d:LX/JW7;

    const/4 v2, 0x0

    .line 2702208
    new-instance v3, LX/JW6;

    invoke-direct {v3, v1}, LX/JW6;-><init>(LX/JW7;)V

    .line 2702209
    iget-object v4, v1, LX/JW7;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JW5;

    .line 2702210
    if-nez v4, :cond_1

    .line 2702211
    new-instance v4, LX/JW5;

    invoke-direct {v4, v1}, LX/JW5;-><init>(LX/JW7;)V

    .line 2702212
    :cond_1
    invoke-static {v4, p1, v2, v2, v3}, LX/JW5;->a$redex0(LX/JW5;LX/1De;IILX/JW6;)V

    .line 2702213
    move-object v3, v4

    .line 2702214
    move-object v2, v3

    .line 2702215
    move-object v1, v2

    .line 2702216
    iget-object v2, v1, LX/JW5;->a:LX/JW6;

    iput-object p3, v2, LX/JW6;->a:LX/1Pn;

    .line 2702217
    iget-object v2, v1, LX/JW5;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2702218
    move-object v1, v1

    .line 2702219
    iget-object v2, v1, LX/JW5;->a:LX/JW6;

    iput-object p2, v2, LX/JW6;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702220
    iget-object v2, v1, LX/JW5;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2702221
    move-object v1, v1

    .line 2702222
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2702223
    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2702224
    move-object v0, v0

    .line 2702225
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;
    .locals 10

    .prologue
    .line 2702187
    const-class v1, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2702188
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702189
    sput-object v2, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702190
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702191
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702192
    new-instance v3, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/JW7;->a(LX/0QB;)LX/JW7;

    move-result-object v5

    check-cast v5, LX/JW7;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v7

    check-cast v7, LX/3j4;

    invoke-static {v0}, LX/JVx;->a(LX/0QB;)LX/JVx;

    move-result-object v8

    check-cast v8, LX/JVx;

    invoke-static {v0}, LX/JVy;->a(LX/0QB;)LX/JVy;

    move-result-object v9

    check-cast v9, LX/JVy;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/JW7;LX/1V0;LX/3j4;LX/JVx;LX/JVy;)V

    .line 2702193
    move-object v0, v3

    .line 2702194
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702195
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702196
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2702186
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2702185
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2702178
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702179
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2702180
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702181
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2702182
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->g:LX/JVx;

    const-string v2, "PYMA header hidden because of NULL Privacy Scope"

    invoke-virtual {v1, v0, v2}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    .line 2702183
    const/4 v0, 0x0

    .line 2702184
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2702165
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    if-nez v1, :cond_1

    .line 2702166
    :cond_0
    :goto_0
    return v0

    .line 2702167
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2702168
    iget-object v2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->h:LX/JVy;

    invoke-virtual {v2, v1, v0}, LX/JVy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2702169
    if-eqz v1, :cond_0

    .line 2702170
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702171
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 2702172
    if-eqz p2, :cond_2

    instance-of v2, p2, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_3

    .line 2702173
    :cond_2
    :goto_1
    move v0, v1

    .line 2702174
    goto :goto_0

    .line 2702175
    :cond_3
    if-eqz v0, :cond_2

    .line 2702176
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2702177
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702164
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
