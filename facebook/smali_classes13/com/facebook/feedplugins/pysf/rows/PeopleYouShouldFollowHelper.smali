.class public Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2dj;

.field private final f:LX/17Q;

.field public final g:LX/0Zb;

.field public final h:LX/0ti;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2703565
    const-class v0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0Ot;LX/2dj;LX/17Q;LX/0Zb;LX/0ti;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/2dj;",
            "LX/17Q;",
            "LX/0Zb;",
            "LX/0ti;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703567
    iput-object p1, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->b:LX/0Ot;

    .line 2703568
    iput-object p2, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->c:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2703569
    iput-object p3, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->d:LX/0Ot;

    .line 2703570
    iput-object p4, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->e:LX/2dj;

    .line 2703571
    iput-object p5, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->f:LX/17Q;

    .line 2703572
    iput-object p6, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->g:LX/0Zb;

    .line 2703573
    iput-object p7, p0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->h:LX/0ti;

    .line 2703574
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    .line 2703575
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 2703576
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2703577
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2703578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2703579
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2703580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2703581
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;
    .locals 11

    .prologue
    .line 2703582
    const-class v1, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;

    monitor-enter v1

    .line 2703583
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703584
    sput-object v2, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703585
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703586
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703587
    new-instance v3, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;

    const/16 v4, 0x6b9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    const/16 v6, 0x474

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v7

    check-cast v7, LX/2dj;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {v0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v10

    check-cast v10, LX/0ti;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;-><init>(LX/0Ot;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0Ot;LX/2dj;LX/17Q;LX/0Zb;LX/0ti;)V

    .line 2703588
    move-object v0, v3

    .line 2703589
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703590
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pysf/rows/PeopleYouShouldFollowHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703591
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703592
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
