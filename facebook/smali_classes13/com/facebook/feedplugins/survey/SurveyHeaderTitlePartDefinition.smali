.class public Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

.field public final b:LX/1Uf;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707811
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2707812
    iput-object p1, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->a:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 2707813
    iput-object p2, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->b:LX/1Uf;

    .line 2707814
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;
    .locals 5

    .prologue
    .line 2707815
    const-class v1, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    monitor-enter v1

    .line 2707816
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707817
    sput-object v2, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707818
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707819
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707820
    new-instance p0, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;-><init>(Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;LX/1Uf;)V

    .line 2707821
    move-object v0, p0

    .line 2707822
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707823
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707824
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2707826
    check-cast p2, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 2707827
    iget-object v0, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->a:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    new-instance v1, LX/JZ3;

    invoke-direct {v1, p0, p2}, LX/JZ3;-><init>(Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2707828
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x308a379c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2707829
    check-cast p4, Landroid/widget/TextView;

    .line 2707830
    const v1, 0x7f0d0081

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 2707831
    const/16 v1, 0x1f

    const v2, -0x7ebdd8dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2707832
    check-cast p4, Landroid/widget/TextView;

    .line 2707833
    const v0, 0x7f0d0081

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 2707834
    return-void
.end method
