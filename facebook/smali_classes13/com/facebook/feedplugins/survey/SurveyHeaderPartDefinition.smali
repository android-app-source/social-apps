.class public Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;",
        ">;",
        "LX/JZ1;",
        "TE;",
        "LX/3V0;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final d:LX/1e4;

.field private final e:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2707784
    const-class v0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1e4;Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707777
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2707778
    iput-object p1, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2707779
    iput-object p2, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 2707780
    iput-object p3, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->d:LX/1e4;

    .line 2707781
    iput-object p4, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->e:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    .line 2707782
    iput-object p5, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->f:Landroid/content/res/Resources;

    .line 2707783
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;
    .locals 9

    .prologue
    .line 2707766
    const-class v1, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;

    monitor-enter v1

    .line 2707767
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707768
    sput-object v2, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707769
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707770
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707771
    new-instance v3, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v6

    check-cast v6, LX/1e4;

    invoke-static {v0}, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1e4;Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;Landroid/content/res/Resources;)V

    .line 2707772
    move-object v0, v3

    .line 2707773
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707774
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707775
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2707785
    sget-object v0, LX/3V0;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2707734
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 2707735
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2707736
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 2707737
    iget-object v1, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2707738
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v4}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2707739
    const v1, 0x7f0d1616

    iget-object v2, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->e:Lcom/facebook/feedplugins/survey/SurveyHeaderTitlePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2707740
    iget-object v1, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->d:LX/1e4;

    const-string v2, "only_me"

    invoke-virtual {v1, v2}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v2

    .line 2707741
    iget-object v1, p0, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->f:Landroid/content/res/Resources;

    const v3, 0x7f081018

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2707742
    check-cast p3, LX/1Pr;

    new-instance v1, LX/JZ9;

    invoke-direct {v1, v0}, LX/JZ9;-><init>(Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JZ8;

    .line 2707743
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    .line 2707744
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2707745
    :goto_0
    new-instance v4, LX/JZ1;

    invoke-direct {v4, v1, v3, v2, v0}, LX/JZ1;-><init>(LX/JZ8;Ljava/lang/String;ILandroid/net/Uri;)V

    return-object v4

    .line 2707746
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3482c9fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2707748
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/JZ1;

    check-cast p4, LX/3V0;

    .line 2707749
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2707750
    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 2707751
    iget-object v2, p2, LX/JZ1;->d:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/feedplugins/survey/SurveyHeaderPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v2, v4}, LX/3V0;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2707752
    const/4 p1, 0x2

    const/4 p3, 0x1

    const/4 p0, 0x0

    const/4 v8, 0x0

    .line 2707753
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p2, LX/JZ1;->a:LX/JZ8;

    .line 2707754
    iget-object v4, v2, LX/JZ8;->a:LX/JZ7;

    move-object v2, v4

    .line 2707755
    sget-object v4, LX/JZ7;->COMPLETE:LX/JZ7;

    if-ne v2, v4, :cond_1

    .line 2707756
    :cond_0
    invoke-virtual {p4, v8, v8}, LX/3V0;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2707757
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x6f0d0207

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2707758
    :cond_1
    iget-object v2, p2, LX/JZ1;->a:LX/JZ8;

    .line 2707759
    iget v4, v2, LX/JZ8;->b:I

    move v2, v4

    .line 2707760
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    .line 2707761
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->m()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    move-result-object v5

    .line 2707762
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;->LINEAR:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    if-eq v5, v6, :cond_2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;->RANDOMIZED:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    if-ne v5, v6, :cond_3

    :cond_2
    const-string v5, "%s %d/%d "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p2, LX/JZ1;->b:Ljava/lang/String;

    aput-object v7, v6, p0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, p3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, p1

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2707763
    :goto_1
    invoke-virtual {p4, v2, v8}, LX/3V0;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2707764
    iget v2, p2, LX/JZ1;->c:I

    invoke-virtual {p4, v2}, LX/3V0;->setSubtitleIcon(I)V

    goto :goto_0

    .line 2707765
    :cond_3
    const-string v4, "%s %d"

    new-array v5, p1, [Ljava/lang/Object;

    iget-object v6, p2, LX/JZ1;->b:Ljava/lang/String;

    aput-object v6, v5, p0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, p3

    invoke-static {v4, v5}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2707747
    const/4 v0, 0x1

    return v0
.end method
