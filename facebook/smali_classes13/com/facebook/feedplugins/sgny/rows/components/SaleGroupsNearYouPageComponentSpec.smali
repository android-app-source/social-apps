.class public Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/JYA;

.field public final d:LX/JY7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2706460
    const-class v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/JYA;LX/JY7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2706462
    iput-object p1, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->b:LX/1nu;

    .line 2706463
    iput-object p2, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->c:LX/JYA;

    .line 2706464
    iput-object p3, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->d:LX/JY7;

    .line 2706465
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLProfile;LX/JY4;)LX/1Dh;
    .locals 7

    .prologue
    const/4 v0, 0x3

    const/16 v5, 0x8

    const/4 v3, 0x2

    .line 2706466
    iget-object v1, p3, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 2706467
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    .line 2706468
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2706469
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2706470
    :goto_0
    move-object v1, v2

    .line 2706471
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProfile;->x()I

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 2706472
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f020a3c

    invoke-interface {v0, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    .line 2706473
    iget-object v2, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->b:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0933

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0935

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    move-object v1, v2

    .line 2706474
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/JYV;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    .line 2706475
    :goto_1
    return-object v0

    .line 2706476
    :cond_0
    iget-object v1, p3, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    const/4 v2, 0x0

    const/4 p3, 0x3

    .line 2706477
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v4

    .line 2706478
    if-nez v4, :cond_5

    .line 2706479
    :cond_1
    :goto_2
    move-object v4, v2

    .line 2706480
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_2

    move v1, v0

    .line 2706481
    :goto_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f020a3c

    invoke-interface {v0, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 2706482
    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v1, :cond_3

    .line 2706483
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2706484
    iget-object v5, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0934

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0935

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    move-object v0, v5

    .line 2706485
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2706486
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 2706487
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_3

    .line 2706488
    :cond_3
    invoke-static {p1}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/JYV;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-object v0, v2

    .line 2706489
    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2706490
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->j()LX/0Px;

    move-result-object p2

    .line 2706491
    if-eqz p2, :cond_1

    .line 2706492
    invoke-static {p3}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 2706493
    const/4 v2, 0x0

    move v4, v2

    :goto_5
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    if-ge v4, v2, :cond_7

    .line 2706494
    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUser;

    .line 2706495
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2706496
    if-eqz v2, :cond_6

    .line 2706497
    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    .line 2706498
    if-eqz v2, :cond_6

    .line 2706499
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2706500
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, p3, :cond_7

    .line 2706501
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    :cond_7
    move-object v2, v6

    .line 2706502
    goto/16 :goto_2
.end method

.method private static a(LX/1De;)LX/1Di;
    .locals 5

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x1

    .line 2706503
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const v1, 0x7f020afc

    invoke-virtual {v0, v1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 2706504
    const v1, -0x1c78f93b

    const/4 v4, 0x0

    invoke-static {p0, v1, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2706505
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2, v3}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1, v3}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f0a058e

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;I)LX/1Di;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2706506
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ce

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a015d

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v7, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;
    .locals 6

    .prologue
    .line 2706507
    const-class v1, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;

    monitor-enter v1

    .line 2706508
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706509
    sput-object v2, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706510
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706511
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706512
    new-instance p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/JYA;->a(LX/0QB;)LX/JYA;

    move-result-object v4

    check-cast v4, LX/JYA;

    invoke-static {v0}, LX/JY7;->a(LX/0QB;)LX/JY7;

    move-result-object v5

    check-cast v5, LX/JY7;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;-><init>(LX/1nu;LX/JYA;LX/JY7;)V

    .line 2706513
    move-object v0, p0

    .line 2706514
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706515
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706516
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706517
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/JY4;)V
    .locals 14
    .param p1    # LX/JY4;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2706518
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouPageComponentSpec;->d:LX/JY7;

    iget-object v1, p1, LX/JY4;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iget-object v2, p1, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 2706519
    const-string v3, "sgny_xout"

    invoke-static {v0, v1, v2, v3}, LX/JY7;->a(LX/JY7;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;Ljava/lang/String;)V

    .line 2706520
    invoke-static {v2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    .line 2706521
    iget-object v4, v0, LX/JY7;->d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2706522
    invoke-static {v4, v3}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;)V

    .line 2706523
    iget-object v5, v4, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/189;

    .line 2706524
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2706525
    invoke-static {v1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    .line 2706526
    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, v11, :cond_1

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 2706527
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 2706528
    invoke-virtual {v9, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2706529
    :cond_0
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_0

    .line 2706530
    :cond_1
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2706531
    invoke-static {v1}, LX/4Yb;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/4Yb;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v9

    invoke-static {v9}, LX/4Yc;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;)LX/4Yc;

    move-result-object v9

    .line 2706532
    iput-object v7, v9, LX/4Yc;->b:LX/0Px;

    .line 2706533
    move-object v9, v9

    .line 2706534
    invoke-virtual {v9}, LX/4Yc;->a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v9

    .line 2706535
    iput-object v9, v8, LX/4Yb;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 2706536
    move-object v8, v8

    .line 2706537
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->w()LX/0Px;

    move-result-object v9

    invoke-static {v9, v3}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v9

    .line 2706538
    iput-object v9, v8, LX/4Yb;->h:LX/0Px;

    .line 2706539
    move-object v8, v8

    .line 2706540
    iget-object v9, v5, LX/189;->i:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    .line 2706541
    iput-wide v9, v8, LX/4Yb;->e:J

    .line 2706542
    move-object v8, v8

    .line 2706543
    invoke-virtual {v8}, LX/4Yb;->a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    move-result-object v8

    .line 2706544
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->I_()I

    move-result v9

    invoke-static {v7, v9}, LX/189;->a(II)I

    move-result v7

    invoke-static {v8, v7}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2706545
    move-object v5, v8

    .line 2706546
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2706547
    iget-object v6, v4, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    .line 2706548
    invoke-static {v6, v5}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2706549
    iget-object v6, v4, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v6, v5}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2706550
    :cond_2
    return-void
.end method
