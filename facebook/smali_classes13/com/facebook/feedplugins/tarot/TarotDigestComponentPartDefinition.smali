.class public Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JZC;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JZC;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708142
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2708143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->e:Z

    .line 2708144
    iput-object p2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->d:LX/JZC;

    .line 2708145
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2708115
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2708116
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2708117
    iget-object v1, p0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->d:LX/JZC;

    const/4 v2, 0x0

    .line 2708118
    new-instance v3, LX/JZB;

    invoke-direct {v3, v1}, LX/JZB;-><init>(LX/JZC;)V

    .line 2708119
    sget-object p2, LX/JZC;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JZA;

    .line 2708120
    if-nez p2, :cond_0

    .line 2708121
    new-instance p2, LX/JZA;

    invoke-direct {p2}, LX/JZA;-><init>()V

    .line 2708122
    :cond_0
    invoke-static {p2, p1, v2, v2, v3}, LX/JZA;->a$redex0(LX/JZA;LX/1De;IILX/JZB;)V

    .line 2708123
    move-object v3, p2

    .line 2708124
    move-object v2, v3

    .line 2708125
    move-object v1, v2

    .line 2708126
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2708127
    iget-object v2, v1, LX/JZA;->a:LX/JZB;

    iput-object v0, v2, LX/JZB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2708128
    iget-object v2, v1, LX/JZA;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708129
    move-object v0, v1

    .line 2708130
    iget-object v1, v0, LX/JZA;->a:LX/JZB;

    iput-object p3, v1, LX/JZB;->b:LX/1Pm;

    .line 2708131
    iget-object v1, v0, LX/JZA;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2708132
    move-object v0, v0

    .line 2708133
    const-string v1, "feed"

    .line 2708134
    iget-object v2, v0, LX/JZA;->a:LX/JZB;

    iput-object v1, v2, LX/JZB;->c:Ljava/lang/String;

    .line 2708135
    iget-object v2, v0, LX/JZA;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708136
    move-object v0, v0

    .line 2708137
    iget-boolean v1, p0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2708138
    iget-object v2, v0, LX/JZA;->a:LX/JZB;

    iput-object v1, v2, LX/JZB;->d:Ljava/lang/Boolean;

    .line 2708139
    iget-object v2, v0, LX/JZA;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708140
    move-object v0, v0

    .line 2708141
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;
    .locals 5

    .prologue
    .line 2708146
    const-class v1, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;

    monitor-enter v1

    .line 2708147
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708148
    sput-object v2, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708149
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708150
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708151
    new-instance p0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JZC;->a(LX/0QB;)LX/JZC;

    move-result-object v4

    check-cast v4, LX/JZC;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;-><init>(Landroid/content/Context;LX/JZC;)V

    .line 2708152
    move-object v0, p0

    .line 2708153
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708154
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708155
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2708114
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2708113
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2708107
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2708108
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2708109
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2708110
    invoke-static {v0}, LX/JZE;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTarotDigest;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2708111
    const/4 v0, 0x0

    .line 2708112
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
