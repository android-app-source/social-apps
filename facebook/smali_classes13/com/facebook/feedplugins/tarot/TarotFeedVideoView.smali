.class public Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;
.super LX/2oW;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2708438
    const-class v0, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2708439
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2708440
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2708441
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2708442
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2708443
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2708444
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    check-cast p0, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;

    const/16 p1, 0x259

    invoke-static {p3, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object p3, p0, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;->m:LX/0Ot;

    .line 2708445
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2708446
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2708447
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->l()Z

    .line 2708448
    new-instance v1, LX/Csy;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, p0, v3}, LX/Csy;-><init>(Landroid/content/Context;LX/2oW;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2708449
    new-instance v1, LX/Ctw;

    invoke-direct {v1, p1}, LX/Ctw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2708450
    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2708451
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 2708452
    sget-object v0, LX/04D;->TAROT_FEED:LX/04D;

    return-object v0
.end method

.method public getDefaultPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 2708453
    sget-object v0, LX/04G;->TAROT:LX/04G;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2708454
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
