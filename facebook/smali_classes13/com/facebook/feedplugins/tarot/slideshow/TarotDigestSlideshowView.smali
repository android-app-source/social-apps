.class public Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;
.super Landroid/widget/ViewFlipper;
.source ""


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/8bH;

.field public final e:LX/8bH;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2708655
    const-class v0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "tarot_story"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2708656
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2708657
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2708658
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2708659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->c:Ljava/util/List;

    .line 2708660
    new-instance v0, LX/JZS;

    invoke-direct {v0, p0}, LX/JZS;-><init>(Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->d:LX/8bH;

    .line 2708661
    new-instance v0, LX/JZT;

    invoke-direct {v0, p0}, LX/JZT;-><init>(Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->e:LX/8bH;

    .line 2708662
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->f:Ljava/util/List;

    .line 2708663
    const-class v0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2708664
    const v0, 0x7f0400f3

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2708665
    if-eqz v0, :cond_0

    .line 2708666
    iget-object p2, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->d:LX/8bH;

    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2708667
    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 2708668
    :cond_0
    const v0, 0x7f0400f4

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2708669
    if-eqz v0, :cond_1

    .line 2708670
    iget-object p2, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->e:LX/8bH;

    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2708671
    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 2708672
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;

    const-class p0, Landroid/content/Context;

    invoke-interface {v1, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p1, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final b(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2708673
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2708674
    new-instance v2, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;

    iget-object v3, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;-><init>(Landroid/content/Context;)V

    .line 2708675
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 2708676
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v4

    .line 2708677
    if-nez v4, :cond_2

    .line 2708678
    iget-object v4, v2, Lcom/facebook/feedplugins/tarot/TarotFeedVideoView;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string v7, "Tarot"

    const-string v8, "Invalid video URL: %s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v5

    invoke-static {v8, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 2708679
    :goto_1
    move v0, v4

    .line 2708680
    if-eqz v0, :cond_0

    .line 2708681
    iget-object v0, p0, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2708682
    invoke-virtual {p0, v2}, Lcom/facebook/feedplugins/tarot/slideshow/TarotDigestSlideshowView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2708683
    :cond_1
    return-void

    .line 2708684
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 2708685
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_4

    :cond_3
    move v4, v5

    .line 2708686
    goto :goto_1

    .line 2708687
    :cond_4
    new-instance v5, LX/Csx;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v10

    invoke-direct {v5, v7, v8, v9, v10}, LX/Csx;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2708688
    new-instance v7, LX/0P2;

    invoke-direct {v7}, LX/0P2;-><init>()V

    .line 2708689
    const-string v8, "CoverImageParamsKey"

    invoke-virtual {v7, v8, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2708690
    new-instance v5, LX/2oE;

    invoke-direct {v5}, LX/2oE;-><init>()V

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2708691
    iput-object v4, v5, LX/2oE;->a:Landroid/net/Uri;

    .line 2708692
    move-object v4, v5

    .line 2708693
    sget-object v5, LX/097;->FROM_STREAM:LX/097;

    .line 2708694
    iput-object v5, v4, LX/2oE;->e:LX/097;

    .line 2708695
    move-object v4, v4

    .line 2708696
    invoke-virtual {v4}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v4

    .line 2708697
    new-instance v5, LX/2oH;

    invoke-direct {v5}, LX/2oH;-><init>()V

    invoke-virtual {v5, v4}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v4

    invoke-virtual {v4}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v4

    .line 2708698
    new-instance v5, LX/2pZ;

    invoke-direct {v5}, LX/2pZ;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v8

    int-to-double v8, v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v10

    int-to-double v10, v10

    div-double/2addr v8, v10

    .line 2708699
    iput-wide v8, v5, LX/2pZ;->e:D

    .line 2708700
    move-object v5, v5

    .line 2708701
    iput-object v4, v5, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2708702
    move-object v4, v5

    .line 2708703
    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v4

    invoke-virtual {v4}, LX/2pZ;->b()LX/2pa;

    move-result-object v4

    .line 2708704
    invoke-virtual {v2, v6}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 2708705
    invoke-virtual {v2, v4}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2708706
    sget-object v4, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    invoke-virtual {v2, v6, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    move v4, v6

    .line 2708707
    goto/16 :goto_1
.end method
