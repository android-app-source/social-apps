.class public final Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JZK;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:I

.field public g:I

.field public h:Landroid/view/View$OnClickListener;

.field public i:Landroid/graphics/Typeface;

.field public j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

.field public k:Ljava/lang/Boolean;

.field public final synthetic l:LX/JZK;


# direct methods
.method public constructor <init>(LX/JZK;)V
    .locals 1

    .prologue
    .line 2708340
    iput-object p1, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->l:LX/JZK;

    .line 2708341
    move-object v0, p1

    .line 2708342
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2708343
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2708344
    const-string v0, "TarotDigestSimpleCoverComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2708345
    if-ne p0, p1, :cond_1

    .line 2708346
    :cond_0
    :goto_0
    return v0

    .line 2708347
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2708348
    goto :goto_0

    .line 2708349
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    .line 2708350
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2708351
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2708352
    if-eq v2, v3, :cond_0

    .line 2708353
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2708354
    goto :goto_0

    .line 2708355
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_4

    .line 2708356
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2708357
    goto :goto_0

    .line 2708358
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2708359
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2708360
    goto :goto_0

    .line 2708361
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2708362
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2708363
    goto :goto_0

    .line 2708364
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2708365
    :cond_f
    iget-wide v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->e:J

    iget-wide v4, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    .line 2708366
    goto :goto_0

    .line 2708367
    :cond_10
    iget v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->f:I

    iget v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->f:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 2708368
    goto :goto_0

    .line 2708369
    :cond_11
    iget v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->g:I

    iget v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->g:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 2708370
    goto/16 :goto_0

    .line 2708371
    :cond_12
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->h:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->h:Landroid/view/View$OnClickListener;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2708372
    goto/16 :goto_0

    .line 2708373
    :cond_14
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->h:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_13

    .line 2708374
    :cond_15
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->i:Landroid/graphics/Typeface;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->i:Landroid/graphics/Typeface;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->i:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 2708375
    goto/16 :goto_0

    .line 2708376
    :cond_17
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->i:Landroid/graphics/Typeface;

    if-nez v2, :cond_16

    .line 2708377
    :cond_18
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 2708378
    goto/16 :goto_0

    .line 2708379
    :cond_1a
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    if-nez v2, :cond_19

    .line 2708380
    :cond_1b
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->k:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->k:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2708381
    goto/16 :goto_0

    .line 2708382
    :cond_1c
    iget-object v2, p1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
