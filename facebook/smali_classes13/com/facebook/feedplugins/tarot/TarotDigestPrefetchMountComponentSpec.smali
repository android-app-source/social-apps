.class public Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/K8G;

.field public final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2708298
    const-class v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/K8G;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2708300
    iput-object p1, p0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->b:Landroid/content/Context;

    .line 2708301
    iput-object p2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->c:LX/K8G;

    .line 2708302
    iput-object p3, p0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->d:LX/0Uh;

    .line 2708303
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;
    .locals 6

    .prologue
    .line 2708304
    const-class v1, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;

    monitor-enter v1

    .line 2708305
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708306
    sput-object v2, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708307
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708308
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708309
    new-instance p0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/K8G;->b(LX/0QB;)LX/K8G;

    move-result-object v4

    check-cast v4, LX/K8G;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;-><init>(Landroid/content/Context;LX/K8G;LX/0Uh;)V

    .line 2708310
    move-object v0, p0

    .line 2708311
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708312
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestPrefetchMountComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708313
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708314
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
