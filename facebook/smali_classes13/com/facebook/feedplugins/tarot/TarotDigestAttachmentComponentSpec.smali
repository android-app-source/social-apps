.class public Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/JZF;

.field private final c:LX/8Yg;

.field private final d:LX/JZI;

.field private final e:LX/JZK;

.field private final f:LX/1DR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2707987
    const-class v0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/8Yg;LX/JZK;LX/JZF;LX/JZI;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2707989
    iput-object p1, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->c:LX/8Yg;

    .line 2707990
    iput-object p4, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->d:LX/JZI;

    .line 2707991
    iput-object p2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->e:LX/JZK;

    .line 2707992
    iput-object p3, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->b:LX/JZF;

    .line 2707993
    iput-object p5, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->f:LX/1DR;

    .line 2707994
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;
    .locals 9

    .prologue
    .line 2707995
    const-class v1, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;

    monitor-enter v1

    .line 2707996
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707997
    sput-object v2, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707998
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707999
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708000
    new-instance v3, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;

    invoke-static {v0}, LX/8Yg;->a(LX/0QB;)LX/8Yg;

    move-result-object v4

    check-cast v4, LX/8Yg;

    invoke-static {v0}, LX/JZK;->a(LX/0QB;)LX/JZK;

    move-result-object v5

    check-cast v5, LX/JZK;

    .line 2708001
    new-instance v8, LX/JZF;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v8, v6, v7}, LX/JZF;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 2708002
    move-object v6, v8

    .line 2708003
    check-cast v6, LX/JZF;

    invoke-static {v0}, LX/JZI;->a(LX/0QB;)LX/JZI;

    move-result-object v7

    check-cast v7, LX/JZI;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v8

    check-cast v8, LX/1DR;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;-><init>(LX/8Yg;LX/JZK;LX/JZF;LX/JZI;LX/1DR;)V

    .line 2708004
    move-object v0, v3

    .line 2708005
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708006
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708007
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708008
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2708009
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2708010
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2708011
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 2708012
    invoke-static {v0}, LX/JZE;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2708013
    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->b:LX/JZF;

    .line 2708014
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2708015
    :cond_0
    :goto_0
    return-void

    .line 2708016
    :cond_1
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2708017
    const-string p0, "tarot_story_ids"

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v3, p0, v0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2708018
    const-string p0, "tarot_click_source"

    invoke-virtual {v3, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2708019
    if-eqz v1, :cond_2

    .line 2708020
    const-string p0, "tracking_codes"

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2708021
    :cond_2
    iget-object p0, v2, LX/JZF;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, v2, LX/JZF;->a:Landroid/content/Context;

    invoke-interface {p0, v3, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;Ljava/lang/String;Ljava/lang/Boolean;)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2708022
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2708023
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2708024
    invoke-static {v0}, LX/JZE;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTarotDigest;

    move-result-object v3

    .line 2708025
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v0

    .line 2708026
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n()Ljava/lang/String;

    move-result-object v2

    .line 2708027
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    if-eqz v0, :cond_6

    .line 2708028
    iget-object v4, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->c:LX/8Yg;

    new-instance v5, LX/JZN;

    invoke-direct {v5, v0}, LX/JZN;-><init>(Lcom/facebook/graphql/model/GraphQLDocumentFontResource;)V

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v4, v0, v5, v6}, LX/8Yg;->a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;

    move-result-object v0

    .line 2708029
    if-eqz v0, :cond_6

    .line 2708030
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 2708031
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    iget-object v2, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->e:LX/JZK;

    const/4 v5, 0x0

    .line 2708032
    new-instance v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    invoke-direct {v6, v2}, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;-><init>(LX/JZK;)V

    .line 2708033
    sget-object v7, LX/JZK;->a:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/JZJ;

    .line 2708034
    if-nez v7, :cond_0

    .line 2708035
    new-instance v7, LX/JZJ;

    invoke-direct {v7}, LX/JZJ;-><init>()V

    .line 2708036
    :cond_0
    invoke-static {v7, p1, v5, v5, v6}, LX/JZJ;->a$redex0(LX/JZJ;LX/1De;IILcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;)V

    .line 2708037
    move-object v6, v7

    .line 2708038
    move-object v5, v6

    .line 2708039
    move-object v2, v5

    .line 2708040
    sget-object v5, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2708041
    iget-object v6, v2, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v5, v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2708042
    iget-object v6, v2, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2708043
    move-object v2, v2

    .line 2708044
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->aZ()Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->aZ()Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    move-result-object v5

    if-nez v5, :cond_7

    .line 2708045
    :cond_1
    const/4 v5, 0x0

    .line 2708046
    :goto_1
    move-object v5, v5

    .line 2708047
    iget-object v6, v2, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v5, v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->b:Ljava/lang/String;

    .line 2708048
    iget-object v6, v2, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2708049
    move-object v2, v2

    .line 2708050
    const/4 v5, 0x0

    .line 2708051
    if-nez v3, :cond_8

    .line 2708052
    :cond_2
    :goto_2
    move-object v5, v5

    .line 2708053
    iget-object v6, v2, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v5, v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->c:Ljava/lang/String;

    .line 2708054
    iget-object v6, v2, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2708055
    move-object v5, v2

    .line 2708056
    if-nez v3, :cond_4

    move-object v2, v1

    .line 2708057
    :goto_3
    iget-object v6, v5, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v2, v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->d:Ljava/lang/String;

    .line 2708058
    iget-object v6, v5, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2708059
    move-object v2, v5

    .line 2708060
    iget-object v5, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->f:LX/1DR;

    invoke-virtual {v5}, LX/1DR;->a()I

    move-result v5

    .line 2708061
    iget-object v6, v2, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput v5, v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->f:I

    .line 2708062
    iget-object v6, v2, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2708063
    move-object v2, v2

    .line 2708064
    iget-object v5, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->f:LX/1DR;

    invoke-virtual {v5}, LX/1DR;->a()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3fa00000    # 1.25f

    div-float/2addr v5, v6

    float-to-int v5, v5

    .line 2708065
    iget-object v6, v2, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput v5, v6, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->g:I

    .line 2708066
    iget-object v6, v2, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v7, 0x6

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2708067
    move-object v2, v2

    .line 2708068
    if-nez v3, :cond_5

    :goto_4
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, LX/JZJ;->a(J)LX/JZJ;

    move-result-object v1

    .line 2708069
    iget-object v2, v1, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v0, v2, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->i:Landroid/graphics/Typeface;

    .line 2708070
    iget-object v2, v1, LX/JZJ;->d:Ljava/util/BitSet;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Ljava/util/BitSet;->set(I)V

    .line 2708071
    move-object v0, v1

    .line 2708072
    iget-object v1, v0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v3, v1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->j:Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 2708073
    iget-object v1, v0, LX/JZJ;->d:Ljava/util/BitSet;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2708074
    move-object v0, v0

    .line 2708075
    iget-object v1, v0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object p5, v1, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->k:Ljava/lang/Boolean;

    .line 2708076
    iget-object v1, v0, LX/JZJ;->d:Ljava/util/BitSet;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2708077
    move-object v0, v0

    .line 2708078
    new-instance v1, LX/JZD;

    invoke-direct {v1, p0, p2}, LX/JZD;-><init>(Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2708079
    iget-object v2, v0, LX/JZJ;->a:Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedplugins/tarot/TarotDigestSimpleCoverComponent$TarotDigestSimpleCoverComponentImpl;->h:Landroid/view/View$OnClickListener;

    .line 2708080
    iget-object v2, v0, LX/JZJ;->d:Ljava/util/BitSet;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708081
    move-object v0, v0

    .line 2708082
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 2708083
    const v1, 0x68d801cf

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2708084
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    invoke-interface {v4, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/tarot/TarotDigestAttachmentComponentSpec;->d:LX/JZI;

    const/4 v2, 0x0

    .line 2708085
    new-instance v3, LX/JZH;

    invoke-direct {v3, v1}, LX/JZH;-><init>(LX/JZI;)V

    .line 2708086
    sget-object v4, LX/JZI;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JZG;

    .line 2708087
    if-nez v4, :cond_3

    .line 2708088
    new-instance v4, LX/JZG;

    invoke-direct {v4}, LX/JZG;-><init>()V

    .line 2708089
    :cond_3
    invoke-static {v4, p1, v2, v2, v3}, LX/JZG;->a$redex0(LX/JZG;LX/1De;IILX/JZH;)V

    .line 2708090
    move-object v3, v4

    .line 2708091
    move-object v2, v3

    .line 2708092
    move-object v1, v2

    .line 2708093
    iget-object v2, v1, LX/JZG;->a:LX/JZH;

    iput-object p3, v2, LX/JZH;->a:LX/1Pm;

    .line 2708094
    iget-object v2, v1, LX/JZG;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708095
    move-object v1, v1

    .line 2708096
    iget-object v2, v1, LX/JZG;->a:LX/JZH;

    iput-object p2, v2, LX/JZH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2708097
    iget-object v2, v1, LX/JZG;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708098
    move-object v1, v1

    .line 2708099
    iget-object v2, v1, LX/JZG;->a:LX/JZH;

    iput-object p4, v2, LX/JZH;->c:Ljava/lang/String;

    .line 2708100
    iget-object v2, v1, LX/JZG;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2708101
    move-object v1, v1

    .line 2708102
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->l()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/16 :goto_4

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->aZ()Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->a()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 2708103
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o()LX/0Px;

    move-result-object v6

    .line 2708104
    if-eqz v6, :cond_2

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2708105
    const/4 v5, 0x0

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLTarotCard;

    .line 2708106
    const/4 v6, 0x1

    invoke-static {v5, v6}, LX/JZE;->a(Lcom/facebook/graphql/model/GraphQLTarotCard;Z)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2
.end method
