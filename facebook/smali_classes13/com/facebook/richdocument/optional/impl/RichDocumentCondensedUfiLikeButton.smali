.class public Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;
.super Lcom/facebook/richdocument/view/widget/PressStateButton;
.source ""


# instance fields
.field public e:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2764163
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2764164
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2764161
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2764162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2764153
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/PressStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2764154
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->h:I

    .line 2764155
    const v0, -0x6e685d

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->i:I

    .line 2764156
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2764157
    const v0, 0x7f0208fc

    iget v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->i:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->f:Landroid/graphics/drawable/Drawable;

    .line 2764158
    const v0, 0x7f0208fc

    const v1, -0xa76f01

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->g:Landroid/graphics/drawable/Drawable;

    .line 2764159
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a()V

    .line 2764160
    return-void
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2764147
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->e:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2764148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->setIsLiked(Z)V

    .line 2764149
    const/4 v0, 0x1

    .line 2764150
    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->f:Z

    .line 2764151
    const v0, 0x7f081c4d

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(I)V

    .line 2764152
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 2764165
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2764166
    invoke-virtual {p0, p2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    .line 2764167
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->e:LX/0wM;

    return-void
.end method


# virtual methods
.method public setDefaultIconColor(I)V
    .locals 3

    .prologue
    .line 2764144
    iput p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->i:I

    .line 2764145
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->e:LX/0wM;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->f:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->i:I

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 2764146
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 1

    .prologue
    .line 2764141
    if-eqz p1, :cond_0

    sget-object v0, LX/1zt;->b:LX/1zt;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->setReaction(LX/1zt;)V

    .line 2764142
    return-void

    .line 2764143
    :cond_0
    sget-object v0, LX/1zt;->a:LX/1zt;

    goto :goto_0
.end method

.method public setReaction(LX/1zt;)V
    .locals 2

    .prologue
    .line 2764122
    iget v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->h:I

    .line 2764123
    iget v1, p1, LX/1zt;->e:I

    move v1, v1

    .line 2764124
    if-ne v0, v1, :cond_0

    .line 2764125
    :goto_0
    return-void

    .line 2764126
    :cond_0
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 2764127
    if-eqz v0, :cond_1

    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p1, v0, :cond_2

    .line 2764128
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->f:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->i:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2764129
    const v0, 0x7f081c4d

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(I)V

    .line 2764130
    :goto_1
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 2764131
    iput v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->h:I

    goto :goto_0

    .line 2764132
    :cond_2
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 2764133
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 2764134
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->g:Landroid/graphics/drawable/Drawable;

    const v1, -0xa76f01

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2764135
    const v0, 0x7f081c4d

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(I)V

    goto :goto_1

    .line 2764136
    :cond_3
    invoke-virtual {p1}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2764137
    iget v1, p1, LX/1zt;->g:I

    move v1, v1

    .line 2764138
    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2764139
    iget-object v0, p1, LX/1zt;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2764140
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
