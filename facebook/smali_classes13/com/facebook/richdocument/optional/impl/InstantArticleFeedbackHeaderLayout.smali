.class public Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CIg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/FrameLayout;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2762968
    const-class v0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;

    const-string v1, "native_article_story"

    const-string v2, "native_article_story"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2762969
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2762970
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2762971
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2762972
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->h:Z

    .line 2762973
    const-class v0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2762974
    const v0, 0x7f030121

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2762975
    const v0, 0x7f0d05cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->d:Landroid/widget/FrameLayout;

    .line 2762976
    const v0, 0x7f0d05ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2762977
    const v0, 0x7f0d05cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2762978
    const v0, 0x7f0d05d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2762979
    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->a(Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;)V

    .line 2762980
    return-void
.end method

.method public static a(Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2762981
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2762982
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->h:Z

    if-eqz v0, :cond_1

    .line 2762983
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->setLayoutDirection(I)V

    .line 2762984
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setLayoutDirection(I)V

    .line 2762985
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setTextDirection(I)V

    .line 2762986
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setLayoutDirection(I)V

    .line 2762987
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setTextDirection(I)V

    .line 2762988
    :cond_0
    :goto_0
    return-void

    .line 2762989
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->setLayoutDirection(I)V

    .line 2762990
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setLayoutDirection(I)V

    .line 2762991
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextDirection(I)V

    .line 2762992
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setLayoutDirection(I)V

    .line 2762993
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextDirection(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {p0}, LX/CIg;->a(LX/0QB;)LX/CIg;

    move-result-object p0

    check-cast p0, LX/CIg;

    iput-object v1, p1, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->a:LX/Cju;

    iput-object p0, p1, Lcom/facebook/richdocument/optional/impl/InstantArticleFeedbackHeaderLayout;->b:LX/CIg;

    return-void
.end method
