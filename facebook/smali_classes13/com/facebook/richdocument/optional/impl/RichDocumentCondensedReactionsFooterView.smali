.class public Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/20E;
.implements LX/20F;
.implements LX/1wK;
.implements LX/0xi;


# static fields
.field public static final f:LX/1Cz;


# instance fields
.field public a:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/20P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

.field private final h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

.field public final i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

.field public j:LX/20H;

.field public k:LX/21M;

.field private l:LX/0wd;

.field public m:LX/20z;

.field private n:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;

.field public o:LX/20I;

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2763714
    new-instance v0, LX/K2M;

    invoke-direct {v0}, LX/K2M;-><init>()V

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->f:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2763715
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763716
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763717
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763718
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->j:LX/20H;

    .line 2763719
    sget-object v0, LX/20I;->LIGHT:LX/20I;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->o:LX/20I;

    .line 2763720
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2763721
    const v0, 0x7f031203

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2763722
    const v0, 0x7f0d2a3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    .line 2763723
    const v0, 0x7f0d2939

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    .line 2763724
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    .line 2763725
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->e:LX/20P;

    invoke-virtual {v0, p0}, LX/20P;->a(LX/20E;)LX/20b;

    move-result-object v0

    .line 2763726
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2763727
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;LX/1zf;LX/20K;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/20P;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;",
            "LX/1zf;",
            "LX/20K;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/20P;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2763728
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a:LX/1zf;

    iput-object p2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->b:LX/20K;

    iput-object p3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->e:LX/20P;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-static {v5}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v1

    check-cast v1, LX/1zf;

    invoke-static {v5}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v2

    check-cast v2, LX/20K;

    const/16 v3, 0x3567

    invoke-static {v5, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v5}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-class v6, LX/20P;

    invoke-interface {v5, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/20P;

    invoke-static/range {v0 .. v5}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;LX/1zf;LX/20K;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/20P;)V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2763729
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->j:LX/20H;

    sget-object v1, LX/20H;->REACTIONS:LX/20H;

    if-eq v0, v1, :cond_0

    .line 2763730
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 2763731
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2763732
    const/4 v0, 0x0

    .line 2763733
    :goto_0
    return v0

    .line 2763734
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->b:LX/20K;

    invoke-virtual {v0, p0}, LX/20K;->a(LX/20F;)V

    .line 2763735
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->b:LX/20K;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->getReactionsDockAnchor()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/20K;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2763736
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2763737
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterAlpha(F)V

    .line 2763738
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 2763739
    return-void
.end method

.method private e()LX/0Ve;
    .locals 1

    .prologue
    .line 2763740
    new-instance v0, LX/K2N;

    invoke-direct {v0, p0}, LX/K2N;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;)V

    return-object v0
.end method

.method private e(LX/0wd;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    const/high16 v9, 0x42c80000    # 100.0f

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const/high16 v8, 0x42480000    # 50.0f

    .line 2763741
    iget-object v6, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v9

    sub-float v0, v8, v0

    div-float/2addr v0, v8

    invoke-virtual {v6, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterAlpha(F)V

    .line 2763742
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v2 .. v7}, LX/0xw;->a(DDD)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v9

    sub-float/2addr v1, v8

    div-float/2addr v1, v8

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 2763743
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v4

    if-ltz v0, :cond_0

    .line 2763744
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, v11}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterVisibility(I)V

    .line 2763745
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v10}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 2763746
    :goto_0
    return-void

    .line 2763747
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, v10}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterVisibility(I)V

    .line 2763748
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v11}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    goto :goto_0
.end method

.method private getRequestLayoutRunnable()Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;
    .locals 2

    .prologue
    .line 2763749
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->n:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;

    if-nez v0, :cond_0

    .line 2763750
    new-instance v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->n:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;

    .line 2763751
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->n:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;

    return-object v0
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2763757
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2763752
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a()V

    .line 2763753
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    if-eqz v0, :cond_0

    .line 2763754
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 2763755
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    .line 2763756
    return-void
.end method

.method public final a(LX/0wd;)V
    .locals 0

    .prologue
    .line 2763800
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->e(LX/0wd;)V

    .line 2763801
    return-void
.end method

.method public final a(LX/20H;Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 2763780
    sget-object v0, LX/K2O;->a:[I

    invoke-virtual {p1}, LX/20H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2763781
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->j:LX/20H;

    .line 2763782
    return-void

    .line 2763783
    :pswitch_0
    if-eqz p2, :cond_2

    .line 2763784
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    if-eqz v0, :cond_1

    .line 2763785
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2763786
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2763787
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 2763788
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, v4}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterVisibility(I)V

    .line 2763789
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 2763790
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->b()V

    goto :goto_1

    .line 2763791
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v4}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a(Z)V

    .line 2763792
    if-eqz p2, :cond_4

    .line 2763793
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    if-eqz v0, :cond_3

    .line 2763794
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2763795
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2763796
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 2763797
    :cond_4
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterVisibility(I)V

    .line 2763798
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v4}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 2763799
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->b()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 3

    .prologue
    .line 2763772
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->k:LX/21M;

    if-eqz v0, :cond_0

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 2763773
    :cond_0
    :goto_0
    return-void

    .line 2763774
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "reactions_like_up"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 2763775
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->k:LX/21M;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->e()LX/0Ve;

    move-result-object v1

    invoke-interface {v0, p0, p2, v1}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 2763776
    invoke-virtual {p0, p2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setReaction(LX/1zt;)V

    .line 2763777
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2763778
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getRequestLayoutRunnable()Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView$RequestLayoutRunnable;

    move-result-object v1

    const v2, -0x7a08377e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2763779
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 2763766
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    if-ne p1, v0, :cond_0

    .line 2763767
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820003

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2763768
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820004

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2763769
    sget-object v0, LX/20H;->REACTIONS:LX/20H;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(LX/20H;Z)V

    .line 2763770
    invoke-direct {p0, p2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(Landroid/view/MotionEvent;)Z

    .line 2763771
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2763802
    if-eqz p1, :cond_0

    sget-object v0, LX/20H;->REACTIONS:LX/20H;

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(LX/20H;Z)V

    .line 2763803
    return-void

    .line 2763804
    :cond_0
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 0

    .prologue
    .line 2763765
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2763763
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a(Z)V

    .line 2763764
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 2763680
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2763760
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setFooterVisibility(I)V

    .line 2763761
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 2763762
    return-void
.end method

.method public getDockTheme()LX/20I;
    .locals 1

    .prologue
    .line 2763759
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->o:LX/20I;

    return-object v0
.end method

.method public getInteractionLogger()LX/20z;
    .locals 1

    .prologue
    .line 2763758
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->m:LX/20z;

    return-object v0
.end method

.method public getMode()LX/20H;
    .locals 1

    .prologue
    .line 2763710
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->j:LX/20H;

    return-object v0
.end method

.method public getSupportedReactions()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2763711
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->p:LX/0Px;

    if-eqz v0, :cond_0

    .line 2763712
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->p:LX/0Px;

    .line 2763713
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->d()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2763662
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2763663
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(LX/20H;Z)V

    .line 2763664
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->b:LX/20K;

    invoke-virtual {v0}, LX/20K;->a()V

    .line 2763665
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2763683
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x4a32ac0b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2763682
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x2bf793eb

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 0

    .prologue
    .line 2763681
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2763676
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2763677
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    .line 2763678
    iput-object p1, v0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->c:Landroid/graphics/drawable/Drawable;

    .line 2763679
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 1

    .prologue
    .line 2763674
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setButtonContainerHeight(I)V

    .line 2763675
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 1

    .prologue
    .line 2763672
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setButtonWeights([F)V

    .line 2763673
    return-void
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763670
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setButtons(Ljava/util/Set;)V

    .line 2763671
    return-void
.end method

.method public setButtonsColor(I)V
    .locals 1

    .prologue
    .line 2763668
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setButtonsColor(I)V

    .line 2763669
    return-void
.end method

.method public setDockTheme(LX/20I;)V
    .locals 0

    .prologue
    .line 2763666
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->o:LX/20I;

    .line 2763667
    return-void
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 1

    .prologue
    .line 2763660
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setDownstateType(LX/1Wk;)V

    .line 2763661
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 2763684
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setEnabled(Z)V

    .line 2763685
    return-void
.end method

.method public setFadeStateSpring(LX/0wd;)V
    .locals 0

    .prologue
    .line 2763686
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->l:LX/0wd;

    .line 2763687
    invoke-virtual {p1, p0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2763688
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->e(LX/0wd;)V

    .line 2763689
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 1

    .prologue
    .line 2763690
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setHasCachedComments(Z)V

    .line 2763691
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 0

    .prologue
    .line 2763692
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 2763693
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setOnButtonClickedListener(LX/20Z;)V

    .line 2763694
    return-void
.end method

.method public setReaction(LX/1zt;)V
    .locals 1

    .prologue
    .line 2763695
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->i:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->setReaction(LX/1zt;)V

    .line 2763696
    return-void
.end method

.method public setReactionMutateListener(LX/21M;)V
    .locals 0

    .prologue
    .line 2763697
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->k:LX/21M;

    .line 2763698
    return-void
.end method

.method public setReactionsLogger(LX/20z;)V
    .locals 0

    .prologue
    .line 2763699
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->m:LX/20z;

    .line 2763700
    return-void
.end method

.method public setShowIcons(Z)V
    .locals 1

    .prologue
    .line 2763701
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setShowIcons(Z)V

    .line 2763702
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763703
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setSprings(Ljava/util/EnumMap;)V

    .line 2763704
    return-void
.end method

.method public setSupportedReactions(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763705
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->p:LX/0Px;

    .line 2763706
    return-void
.end method

.method public setTextVisibility(I)V
    .locals 1

    .prologue
    .line 2763707
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->g:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setTextVisibility(I)V

    .line 2763708
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 0

    .prologue
    .line 2763709
    return-void
.end method
