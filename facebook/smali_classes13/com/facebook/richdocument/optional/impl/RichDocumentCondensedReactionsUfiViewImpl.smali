.class public Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;
.super LX/CnR;
.source ""

# interfaces
.implements LX/Cjr;
.implements LX/CnL;
.implements LX/CnP;
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnR;",
        "LX/Cjr;",
        "LX/CnL;",
        "LX/CnP;",
        "LX/CnQ",
        "<",
        "LX/ClW;",
        ">;"
    }
.end annotation


# static fields
.field private static final J:Ljava/util/Set;

.field private static final K:Ljava/util/Set;

.field private static final L:Ljava/util/Set;

.field public static final q:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public C:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private D:LX/Cly;

.field private E:LX/CmD;

.field private F:LX/CmQ;

.field private G:Z

.field private H:Ljava/lang/String;

.field private final I:LX/K2U;

.field public M:Ljava/lang/String;

.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/20w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/20h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CjE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K1t;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/20i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/K1u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final r:Landroid/view/View;

.field private s:LX/K2Y;

.field public t:LX/ClW;

.field private u:LX/CnN;

.field public v:LX/CnO;

.field private w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

.field private x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

.field public y:Z

.field private z:LX/0wd;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2763939
    const-class v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->q:Ljava/lang/String;

    .line 2763940
    sget-object v0, LX/20X;->LIKE:LX/20X;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    sget-object v2, LX/20X;->SHARE:LX/20X;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->J:Ljava/util/Set;

    .line 2763941
    sget-object v0, LX/20X;->LIKE:LX/20X;

    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->K:Ljava/util/Set;

    .line 2763942
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2763943
    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->L:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2763944
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763945
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2763946
    invoke-direct {p0, p1, p2}, LX/CnR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763947
    new-instance v0, LX/CnO;

    invoke-direct {v0}, LX/CnO;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    .line 2763948
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2763949
    iput-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->G:Z

    .line 2763950
    new-instance v0, LX/K2U;

    invoke-direct {v0, p0}, LX/K2U;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->I:LX/K2U;

    .line 2763951
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2763952
    const v0, 0x7f03011b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2763953
    const v0, 0x7f0d05c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    .line 2763954
    const v0, 0x7f0d05c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    .line 2763955
    const v0, 0x7f0d05c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->r:Landroid/view/View;

    .line 2763956
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2763957
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->b:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2763958
    invoke-virtual {p0, v6}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setLayoutDirection(I)V

    .line 2763959
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->e:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4026000000000000L    # 11.0

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2763960
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 2763961
    move-object v0, v0

    .line 2763962
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->z:LX/0wd;

    .line 2763963
    new-instance v0, LX/K2Y;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a062c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, LX/K2Y;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->s:LX/K2Y;

    .line 2763964
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->M:Ljava/lang/String;

    .line 2763965
    return-void

    .line 2763966
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setLayoutDirection(I)V

    goto :goto_0
.end method

.method private static a(LX/CnN;LX/CnO;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 3

    .prologue
    .line 2763967
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 2763968
    iget-object v1, p0, LX/CnN;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2763969
    iput-object v1, v0, LX/170;->o:Ljava/lang/String;

    .line 2763970
    move-object v0, v0

    .line 2763971
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2763972
    iget v2, p0, LX/CnN;->f:I

    move v2, v2

    .line 2763973
    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2763974
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2763975
    move-object v0, v0

    .line 2763976
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2763977
    iget-object v1, p1, LX/CnO;->c:LX/21D;

    const-string v2, "richdocumentReactionsUfiView"

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2763978
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->H:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2763979
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->H:Ljava/lang/String;

    .line 2763980
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->P()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;LX/03V;LX/Crz;LX/Chi;LX/20K;LX/0wW;LX/20w;LX/1zf;LX/20h;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/20i;LX/K1u;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Crz;",
            "LX/Chi;",
            "LX/20K;",
            "LX/0wW;",
            "LX/20w;",
            "LX/1zf;",
            "LX/20h;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CjE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/K1t;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            "LX/K1u;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763981
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->b:LX/Crz;

    iput-object p3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->c:LX/Chi;

    iput-object p4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->d:LX/20K;

    iput-object p5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->e:LX/0wW;

    iput-object p6, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->f:LX/20w;

    iput-object p7, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->g:LX/1zf;

    iput-object p8, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->h:LX/20h;

    iput-object p9, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->i:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->j:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->l:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->n:LX/20i;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->o:LX/K1u;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->p:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 19

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;

    invoke-static/range {v17 .. v17}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static/range {v17 .. v17}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v3

    check-cast v3, LX/Crz;

    invoke-static/range {v17 .. v17}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v4

    check-cast v4, LX/Chi;

    invoke-static/range {v17 .. v17}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v5

    check-cast v5, LX/20K;

    invoke-static/range {v17 .. v17}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v6

    check-cast v6, LX/0wW;

    const-class v7, LX/20w;

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/20w;

    invoke-static/range {v17 .. v17}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v8

    check-cast v8, LX/1zf;

    invoke-static/range {v17 .. v17}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v9

    check-cast v9, LX/20h;

    const/16 v10, 0x3be

    move-object/from16 v0, v17

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x31e6

    move-object/from16 v0, v17

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3228

    move-object/from16 v0, v17

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3239

    move-object/from16 v0, v17

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x475

    move-object/from16 v0, v17

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v17 .. v17}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v15

    check-cast v15, LX/20i;

    invoke-static/range {v17 .. v17}, LX/K1u;->a(LX/0QB;)LX/K1u;

    move-result-object v16

    check-cast v16, LX/K1u;

    const/16 v18, 0x3216

    invoke-static/range {v17 .. v18}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v1 .. v17}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;LX/03V;LX/Crz;LX/Chi;LX/20K;LX/0wW;LX/20w;LX/1zf;LX/20h;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/20i;LX/K1u;LX/0Ot;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2763982
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CjE;

    new-instance v1, LX/K2P;

    invoke-direct {v1, p0}, LX/K2P;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    invoke-virtual {v0, p1, v1}, LX/CjE;->a(Ljava/lang/String;LX/2h1;)V

    .line 2763983
    return-void
.end method

.method public static a$redex0(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;LX/1zt;)V
    .locals 4

    .prologue
    .line 2763984
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v1

    invoke-static {v0, v1, p1}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v1

    .line 2763985
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->n:LX/20i;

    invoke-virtual {v2}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763986
    return-void
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 2763987
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->g()V

    .line 2763988
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 2763989
    :goto_0
    return-void

    .line 2763990
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    if-eqz v0, :cond_1

    .line 2763991
    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->o(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    .line 2763992
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    new-instance v1, LX/K2Q;

    invoke-direct {v1, p0}, LX/K2Q;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    invoke-virtual {v0, v1}, LX/CnK;->setReactorsOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2763993
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    new-instance v1, LX/K2R;

    invoke-direct {v1, p0}, LX/K2R;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    invoke-virtual {v0, v1}, LX/CnK;->setCommentOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2763994
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->I:LX/K2U;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->z:LX/0wd;

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->f:LX/20w;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    iget-object v7, v7, LX/CnO;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->g:LX/1zf;

    sget-object v6, LX/20I;->LIGHT:LX/20I;

    iget-object v7, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->g:LX/1zf;

    iget-object v8, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v7

    .line 2763995
    invoke-static {v1}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v5, v8}, LX/1zf;->a(I)LX/1zt;

    move-result-object v8

    .line 2763996
    iput-object v2, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->k:LX/21M;

    .line 2763997
    invoke-virtual {v0, v8}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setReaction(LX/1zt;)V

    .line 2763998
    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setFadeStateSpring(LX/0wd;)V

    .line 2763999
    invoke-virtual {v4, v8}, LX/20z;->a(LX/1zt;)V

    .line 2764000
    iput-object v4, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->m:LX/20z;

    .line 2764001
    iput-object v6, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->o:LX/20I;

    .line 2764002
    iput-object v7, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->p:LX/0Px;

    .line 2764003
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getFooterButtonOptions()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setButtons(Ljava/util/Set;)V

    .line 2764004
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setButtonWeights([F)V

    .line 2764005
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-virtual {v0, v9}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setShowIcons(Z)V

    .line 2764006
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    sget-object v1, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setTopDividerStyle(LX/1Wl;)V

    .line 2764007
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->l()LX/20Z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setOnButtonClickedListener(LX/20Z;)V

    .line 2764008
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->j()V

    .line 2764009
    iput-boolean v9, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->A:Z

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private g()V
    .locals 1

    .prologue
    .line 2764010
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 2764011
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->i()V

    .line 2764012
    :goto_0
    return-void

    .line 2764013
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-lez v0, :cond_2

    .line 2764014
    :cond_1
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->h()V

    goto :goto_0

    .line 2764015
    :cond_2
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->i()V

    goto :goto_0
.end method

.method public static getFeedbackLoggingparams(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .locals 2

    .prologue
    .line 2764109
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    iget-object v1, v1, LX/CnO;->a:Ljava/lang/String;

    .line 2764110
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 2764111
    move-object v0, v0

    .line 2764112
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    iget-object v1, v1, LX/CnO;->b:Ljava/lang/String;

    .line 2764113
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 2764114
    move-object v0, v0

    .line 2764115
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getTrackingCodes()LX/162;

    move-result-object v1

    .line 2764116
    iput-object v1, v0, LX/21A;->a:LX/162;

    .line 2764117
    move-object v0, v0

    .line 2764118
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    iget-object v1, v1, LX/CnO;->c:LX/21D;

    .line 2764119
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 2764120
    move-object v0, v0

    .line 2764121
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    return-object v0
.end method

.method private getFooterButtonOptions()Ljava/util/Set;
    .locals 2

    .prologue
    .line 2764016
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_0

    .line 2764017
    sget-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->K:Ljava/util/Set;

    .line 2764018
    :goto_0
    return-object v0

    .line 2764019
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_1

    .line 2764020
    sget-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->J:Ljava/util/Set;

    goto :goto_0

    .line 2764021
    :cond_1
    sget-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->L:Ljava/util/Set;

    goto :goto_0
.end method

.method private getTrackingCodes()LX/162;
    .locals 4

    .prologue
    .line 2764094
    const/4 v0, 0x0

    .line 2764095
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->c:LX/Chi;

    .line 2764096
    iget-object v2, v1, LX/Chi;->g:LX/0lF;

    move-object v1, v2

    .line 2764097
    instance-of v1, v1, LX/162;

    if-eqz v1, :cond_0

    .line 2764098
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->c:LX/Chi;

    .line 2764099
    iget-object v1, v0, LX/Chi;->g:LX/0lF;

    move-object v0, v1

    .line 2764100
    check-cast v0, LX/162;

    .line 2764101
    :cond_0
    if-nez v0, :cond_1

    .line 2764102
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 2764103
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2764104
    const-string v2, "node_id"

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->c:LX/Chi;

    .line 2764105
    iget-object p0, v3, LX/Chi;->c:Ljava/lang/String;

    move-object v3, p0

    .line 2764106
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2764107
    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2764108
    :cond_1
    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2764087
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setTextVisibility(I)V

    .line 2764088
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2764089
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2764090
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->setVisibility(I)V

    .line 2764091
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2764092
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->invalidate()V

    .line 2764093
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2764075
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setTextVisibility(I)V

    .line 2764076
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getFooterButtonOptions()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setButtons(Ljava/util/Set;)V

    .line 2764077
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setButtonWeights([F)V

    .line 2764078
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setShowIcons(Z)V

    .line 2764079
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    sget-object v1, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setTopDividerStyle(LX/1Wl;)V

    .line 2764080
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2764081
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2764082
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->setVisibility(I)V

    .line 2764083
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2764084
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->invalidate()V

    .line 2764085
    return-void

    .line 2764086
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2764073
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    new-instance v1, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl$4;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl$4;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->post(Ljava/lang/Runnable;)Z

    .line 2764074
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 2764063
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->A:Z

    if-nez v0, :cond_0

    .line 2764064
    :goto_0
    return-void

    .line 2764065
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->d:LX/20K;

    .line 2764066
    iget-object v2, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->j:LX/20H;

    move-object v2, v2

    .line 2764067
    sget-object v3, LX/20H;->REACTIONS:LX/20H;

    if-ne v2, v3, :cond_1

    .line 2764068
    invoke-virtual {v1}, LX/20K;->a()V

    .line 2764069
    sget-object v2, LX/20H;->DEFAULT:LX/20H;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(LX/20H;Z)V

    .line 2764070
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a()V

    .line 2764071
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->A:Z

    goto :goto_0

    .line 2764072
    :cond_1
    sget-object v2, LX/20H;->DEFAULT:LX/20H;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->a(LX/20H;Z)V

    goto :goto_1
.end method

.method private l()LX/20Z;
    .locals 1

    .prologue
    .line 2764062
    new-instance v0, LX/K2S;

    invoke-direct {v0, p0}, LX/K2S;-><init>(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    return-object v0
.end method

.method public static m(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V
    .locals 1

    .prologue
    .line 2764058
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->p()V

    .line 2764059
    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->o(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V

    .line 2764060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->y:Z

    .line 2764061
    return-void
.end method

.method public static n(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V
    .locals 4

    .prologue
    .line 2764052
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 2764053
    :goto_0
    return-void

    .line 2764054
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->g:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    .line 2764055
    :goto_1
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->I:LX/K2U;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/K2U;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 2764056
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setReaction(LX/1zt;)V

    goto :goto_0

    .line 2764057
    :cond_1
    sget-object v0, LX/1zt;->c:LX/1zt;

    goto :goto_1
.end method

.method public static o(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V
    .locals 2

    .prologue
    .line 2764050
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1}, LX/CnK;->setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2764051
    return-void
.end method

.method private p()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2764024
    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getFeedbackLoggingparams(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 2764025
    new-instance v1, LX/8qL;

    invoke-direct {v1}, LX/8qL;-><init>()V

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2764026
    iput-object v2, v1, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2764027
    move-object v1, v1

    .line 2764028
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    .line 2764029
    iput-object v2, v1, LX/8qL;->d:Ljava/lang/String;

    .line 2764030
    move-object v1, v1

    .line 2764031
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    .line 2764032
    iput-object v2, v1, LX/8qL;->e:Ljava/lang/String;

    .line 2764033
    move-object v1, v1

    .line 2764034
    const/4 v2, 0x1

    .line 2764035
    iput-boolean v2, v1, LX/8qL;->i:Z

    .line 2764036
    move-object v1, v1

    .line 2764037
    iput-boolean v3, v1, LX/8qL;->h:Z

    .line 2764038
    move-object v1, v1

    .line 2764039
    iput-boolean v3, v1, LX/8qL;->j:Z

    .line 2764040
    move-object v1, v1

    .line 2764041
    iput-object v0, v1, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2764042
    move-object v0, v1

    .line 2764043
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v2

    .line 2764044
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    .line 2764045
    iput-boolean v3, v0, LX/8qO;->a:Z

    .line 2764046
    move-object v0, v0

    .line 2764047
    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v3

    .line 2764048
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K1t;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->D:LX/Cly;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->E:LX/CmD;

    iget-object v6, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->F:LX/CmQ;

    iget-object v7, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->b:LX/Crz;

    invoke-virtual {v7}, LX/Crz;->a()Z

    move-result v7

    invoke-virtual/range {v0 .. v7}, LX/K1t;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;LX/Cly;LX/CmD;LX/CmQ;Z)Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;

    .line 2764049
    return-void
.end method

.method public static q(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V
    .locals 4

    .prologue
    .line 2764022
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->o:LX/K1u;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getFeedbackLoggingparams(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/K1u;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 2764023
    return-void
.end method

.method public static r(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;)V
    .locals 5

    .prologue
    .line 2763908
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    if-nez v0, :cond_0

    .line 2763909
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2763910
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoV;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x3ec

    invoke-virtual {v0, v2, v1, v3, v4}, LX/CoV;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2763911
    :goto_0
    return-void

    .line 2763912
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763913
    iget-object v1, v0, LX/CnN;->b:Landroid/app/Activity;

    move-object v0, v1

    .line 2763914
    if-eqz v0, :cond_1

    .line 2763915
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763916
    iget-object v2, v1, LX/CnN;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2763917
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    invoke-static {v2, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(LX/CnN;LX/CnO;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763918
    iget v4, v3, LX/CnN;->a:I

    move v3, v4

    .line 2763919
    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763920
    iget-object p0, v4, LX/CnN;->b:Landroid/app/Activity;

    move-object v4, p0

    .line 2763921
    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2763922
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763923
    iget-object v1, v0, LX/CnN;->c:Landroid/support/v4/app/Fragment;

    move-object v0, v1

    .line 2763924
    if-eqz v0, :cond_2

    .line 2763925
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763926
    iget-object v2, v1, LX/CnN;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2763927
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    invoke-static {v2, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(LX/CnN;LX/CnO;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763928
    iget v4, v3, LX/CnN;->a:I

    move v3, v4

    .line 2763929
    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763930
    iget-object p0, v4, LX/CnN;->c:Landroid/support/v4/app/Fragment;

    move-object v4, p0

    .line 2763931
    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2763932
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763933
    iget-object v2, v1, LX/CnN;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2763934
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    invoke-static {v2, v3}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(LX/CnN;LX/CnO;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static setFeedback(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 2763935
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763936
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->y:Z

    .line 2763937
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->f()V

    .line 2763938
    return-void
.end method

.method private setFeedbackOptions(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;)V
    .locals 0

    .prologue
    .line 2763851
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    :cond_0
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2763852
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2763853
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2763904
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763905
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2763906
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setVisibility(I)V

    .line 2763907
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 2763854
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2763855
    const/4 v0, 0x1

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2763856
    invoke-super {p0, p1}, LX/CnR;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2763857
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->G:Z

    if-eqz v0, :cond_0

    .line 2763858
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->s:LX/K2Y;

    invoke-virtual {v0, p0, p1}, LX/K2Y;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 2763859
    :cond_0
    return-void
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 2763860
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->t:LX/ClW;

    move-object v0, v0

    .line 2763861
    return-object v0
.end method

.method public getAnnotation()LX/ClW;
    .locals 1

    .prologue
    .line 2763862
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->t:LX/ClW;

    return-object v0
.end method

.method public getExtraPaddingBottom()I
    .locals 1

    .prologue
    .line 2763863
    const/4 v0, 0x0

    return v0
.end method

.method public getIsDirtyAndReset()Z
    .locals 2

    .prologue
    .line 2763864
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->y:Z

    .line 2763865
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->y:Z

    .line 2763866
    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x47da2896

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2763867
    invoke-super {p0}, LX/CnR;->onAttachedToWindow()V

    .line 2763868
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->f()V

    .line 2763869
    const/16 v1, 0x2d

    const v2, 0x746bbf9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2c33f19a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2763870
    invoke-super {p0}, LX/CnR;->onDetachedFromWindow()V

    .line 2763871
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->k()V

    .line 2763872
    const/16 v1, 0x2d

    const v2, 0x11a49e95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAnnotation(LX/ClW;)V
    .locals 1

    .prologue
    .line 2763873
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->t:LX/ClW;

    .line 2763874
    if-eqz p1, :cond_0

    .line 2763875
    iget-object v0, p1, LX/ClW;->a:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v0, v0

    .line 2763876
    invoke-direct {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->setFeedbackOptions(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;)V

    .line 2763877
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2763878
    if-eqz v0, :cond_0

    .line 2763879
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2763880
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->a(Ljava/lang/String;)V

    .line 2763881
    :cond_0
    return-void
.end method

.method public setButtonsColor(I)V
    .locals 1

    .prologue
    .line 2763849
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->x:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsFooterView;->setButtonsColor(I)V

    .line 2763850
    return-void
.end method

.method public setClipTokens(Z)V
    .locals 1

    .prologue
    .line 2763882
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    .line 2763883
    iput-boolean p1, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->i:Z

    .line 2763884
    return-void
.end method

.method public setComposerLaunchParams(LX/CnN;)V
    .locals 0

    .prologue
    .line 2763885
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->u:LX/CnN;

    .line 2763886
    return-void
.end method

.method public setCountsTextColor(I)V
    .locals 1

    .prologue
    .line 2763887
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->w:Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->setTextColor(I)V

    .line 2763888
    return-void
.end method

.method public setFeedbackHeaderAuthorByline(LX/CmD;)V
    .locals 0
    .param p1    # LX/CmD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763889
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->E:LX/CmD;

    .line 2763890
    return-void
.end method

.method public setFeedbackHeaderTitle(LX/Cly;)V
    .locals 0
    .param p1    # LX/Cly;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763891
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->D:LX/Cly;

    .line 2763892
    return-void
.end method

.method public setFeedbackLoggingParams(LX/162;)V
    .locals 0

    .prologue
    .line 2763893
    return-void
.end method

.method public setIsOverlay(Z)V
    .locals 0

    .prologue
    .line 2763894
    return-void
.end method

.method public setLogoInformation(LX/CmQ;)V
    .locals 0
    .param p1    # LX/CmQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763895
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->F:LX/CmQ;

    .line 2763896
    return-void
.end method

.method public setShareUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763897
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->H:Ljava/lang/String;

    .line 2763898
    return-void
.end method

.method public setShowShareButton(Z)V
    .locals 0

    .prologue
    .line 2763899
    return-void
.end method

.method public setShowTopDivider(Z)V
    .locals 0

    .prologue
    .line 2763900
    iput-boolean p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->G:Z

    .line 2763901
    return-void
.end method

.method public setUfiIdentificationParams(LX/CnO;)V
    .locals 0

    .prologue
    .line 2763902
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedReactionsUfiViewImpl;->v:LX/CnO;

    .line 2763903
    return-void
.end method
