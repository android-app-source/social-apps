.class public Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;
.super LX/8wv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Ct1;


# static fields
.field private static final z:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Lcom/facebook/common/callercontext/CallerContext;

.field private B:LX/Ctm;

.field private C:Z

.field public y:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2763488
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->z:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2763485
    invoke-direct {p0, p1}, LX/8wv;-><init>(Landroid/content/Context;)V

    .line 2763486
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->q()V

    .line 2763487
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2763482
    invoke-direct {p0, p1, p2}, LX/8wv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763483
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->q()V

    .line 2763484
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2763479
    invoke-direct {p0, p1, p2, p3}, LX/8wv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2763480
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->q()V

    .line 2763481
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v0

    check-cast v0, LX/Chi;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->y:LX/Chi;

    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2763468
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2763469
    new-instance v0, LX/Ctm;

    invoke-direct {v0, p0}, LX/Ctm;-><init>(LX/Ct1;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->B:LX/Ctm;

    .line 2763470
    sget-object v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->z:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->A:Lcom/facebook/common/callercontext/CallerContext;

    .line 2763471
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/ChJ;->b(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v0

    .line 2763472
    if-eqz v0, :cond_0

    .line 2763473
    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->A:Lcom/facebook/common/callercontext/CallerContext;

    .line 2763474
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/String;Lcom/facebook/spherical/photo/model/SphericalPhotoParams;)V
    .locals 3

    .prologue
    .line 2763462
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->A:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v1, LX/7Dj;->OTHER:LX/7Dj;

    invoke-virtual {p0, p4, v0, p3, v1}, LX/8wv;->a(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 2763463
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->setVisibility(I)V

    .line 2763464
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->B:LX/Ctm;

    int-to-float v1, p1

    int-to-float v2, p2

    div-float/2addr v1, v2

    .line 2763465
    iput v1, v0, LX/Ctm;->b:F

    .line 2763466
    invoke-virtual {p0}, LX/8wv;->h()V

    .line 2763467
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2763475
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->setVisibility(I)V

    .line 2763476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->C:Z

    .line 2763477
    invoke-virtual {p0}, LX/8wv;->j()V

    .line 2763478
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2763459
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->C:Z

    .line 2763460
    invoke-super {p0}, LX/8wv;->f()V

    .line 2763461
    return-void
.end method

.method public getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 2763456
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->B:LX/Ctm;

    .line 2763457
    iget p0, v0, LX/Ctm;->b:F

    move v0, p0

    .line 2763458
    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2763455
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 2763454
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->C:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 2763451
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->B:LX/Ctm;

    invoke-virtual {v0}, LX/Ctm;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 2763452
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, LX/8wv;->onMeasure(II)V

    .line 2763453
    return-void
.end method

.method public setCallerContext(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2763448
    if-eqz p1, :cond_0

    .line 2763449
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->A:Lcom/facebook/common/callercontext/CallerContext;

    .line 2763450
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2763447
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
